FROM morrisons/java8-base

MAINTAINER AutomationTeam "ayan.maity@morrisonsplc.co.uk"

RUN addgroup -g 1138 -S mwadmin && adduser -u 1584 -S mwadmin -G mwadmin -h /home/mwadmin

RUN set -e && \
    mkdir -p /app && \ 
	#mkdir -p /app/automation && \
	mkdir -p /app/bin && \
	mkdir -p /app/logs
    
RUN chmod -R 777 /app/
RUN chown -R mwadmin:mwadmin /app/
RUN chmod -R 777 /app/bin/
RUN chown -R mwadmin:mwadmin /app/bin/
RUN chmod -R 777 /app/logs/
RUN chown -R mwadmin:mwadmin /app/logs/


USER mwadmin
WORKDIR  /app/bin

#COPY .jar under target from host into the image
ADD ./target/selenium-docker.jar				/app/bin/selenium-docker.jar
ADD ./target/selenium-docker-tests.jar			/app/bin/selenium-docker-tests.jar
ADD ./target/libs								/app/bin/libs
ADD ./target/ChromeDriver						/app/bin/ChromeDriver
ADD ./target/Configuration						/app/bin/Configuration
ADD ./target/TestData							/app/bin/TestData
ADD ./target/Results							/app/bin/Results
ADD ./extent-config.xml							/app/bin/extent-config.xml
ADD ./target/log4j.properties					/app/bin/log4j.properties

#ADD suite file
ADD ./TestNG.xml								/app/bin/TestNG.xml

ENTRYPOINT /usr/bin/java -cp /app/bin/selenium-docker.jar:/app/bin/selenium-docker-tests.jar:/app/bin/libs/*:/app/bin/log4j.properties org.testng.TestNG /app/bin/TestNG.xml
#ENTRYPOINT java -jar selenium-docker-tests.jar
#ENTRYPOINT java -version
#CMD java -version > /app/logs/server.log 2>&1
#CMD java -cp ./selenium-docker.jar;./selenium-docker-tests.jar;./libs/* org.testng.TestNG ./TestNG.xml > /app/logs/server.log 2>&1
#ENTRYPOINT java -cp ./selenium-docker.jar;./selenium-docker-tests.jar;./libs/* org.testng.TestNG ./TestNG.xml
