package Additional_Functions;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

import Utilities.*;

public class Batch_JobRun {
	

public static boolean  RunJob (String link,String userName,String userPwd,String TestCaseNo,String ResultPath,String JobName) {
	
	Boolean  result=false;
	 try {

	       // Runtime.getRuntime().exec( "C:/UFTJOB/MyTest.vbs" );
		
	      Process p=  Runtime.getRuntime().exec( "cscript C:/UFT_Scripts_Exec_Source/Appworx_Job_Run/Appworx_BatchParam_WS1.vbs"+" "+link+","+userName+","+userPwd+","+TestCaseNo+","+ResultPath+","+JobName);
	      
	      System.out.println("Waiting for UFT Script Execution to be completed ...");
	        try {
				p.waitFor();
				
				result=true;
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
}
	 
	 
	 catch (Exception ex) {
		 
		 System.out.println("Problem occurred during batch run...");
		 result=false;

	    }
	 
	 
	 finally{
    	   
    	return result;   
    	   
       }
	
	
}

public static boolean  RunJob_SITEnv (String link,String userName,String userPwd,String TestCaseNo,String ResultPath,String JobName) {
	
	Boolean  result=false;
	 try {

	       // Runtime.getRuntime().exec( "C:/UFTJOB/MyTest.vbs" );
		
	      Process p=  Runtime.getRuntime().exec( "cscript C:/UFT_Scripts_Exec_Source/Appworx_Job_Run/Appworx_Batch_SIT.vbs"+" "+link+","+userName+","+userPwd+","+TestCaseNo+","+ResultPath+","+JobName);
	      
	      System.out.println("Waiting for UFT Script Execution to be completed ...");
	        try {
				p.waitFor();
				
				result=true;
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
}
	 
	 
	 catch (Exception ex) {
		 
		 System.out.println("Problem occurred during batch run...");
		 result=false;

	    }
	 
	 
	 finally{
    	   
    	return result;   
    	   
       }
	
	
}


}
