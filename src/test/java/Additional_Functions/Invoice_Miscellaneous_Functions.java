package Additional_Functions;

import java.io.IOException;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.poi.xwpf.usermodel.XWPFRun;

import java.io.File;
import java.io.IOException;

import jxl.Sheet;
import jxl.Workbook;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Utilities.*;


import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;


public class Invoice_Miscellaneous_Functions {
	
	
public static boolean OrderIdGeneration(String TestDataPath, String SheetName, String tcid, String Resultpath,XWPFRun xwpfRun, ExtentTest logger) throws IOException {
		
		boolean res = false;
		int r = 0;
		
	    try {
	    	int rows = 0;
			
	    	Workbook wrk1 = Workbook.getWorkbook(new File(TestDataPath));
	    	
			Sheet sheet1 = wrk1.getSheet(SheetName); 
	         
			rows = sheet1.getRows();
			
			for(r=1; r<rows; r++) {
	
				long currentDateSec = (System.currentTimeMillis() / 1000);
				 excelCellValueWrite.writeValueToCell(String.valueOf(currentDateSec), r, 16, TestDataPath, SheetName);

				 Thread.sleep(1200);
			}
			
			wrk1.close();
			
			res = true;
	        	  
	       
		} catch (Exception e) {
			// TODO: handle exception
			
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during Order ID generation ", "Due to :"+e,Resultpath);
	        
			res = false;
			return res;
		}
	    
	    finally{
	}
	    return res;
	}



public static String GetCallquantityTypeFromWebPortal(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String ItemId, String tcid, String Resultpath,XWPFRun xwpfRun, ExtentTest logger) throws IOException {
	
	String res = null;
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        //https://sit-api.morrisons.com/wholesaleorder/v1/customers/mccolls/catalogues/main/items/101743031?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36&filtertype=maptype&filtervalue=PIN
        
        HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/mccolls/catalogues/main/items/" + ItemId + "?" + ApiKey+"&filtertype=maptype&filtervalue=PIN");
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Item ID is " + ItemId);
        utilityFileWriteOP.writeToLog(tcid, "Searching the attributes for ", "Item ID "+ItemId,Resultpath);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",Resultpath,xwpfRun,"");
        
        response = httpclient.execute(target, httpget);
        logger.log(LogStatus.INFO, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),Resultpath,xwpfRun, "");
        logger.log(LogStatus.INFO, "Response code for Item search in Catalogue displayed is: "+response.getStatusLine().getStatusCode() +" for item ID : "+ItemId);
        String jsonresponse = EntityUtils.toString(response.getEntity());
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,Resultpath);
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString(),Resultpath,xwpfRun,"");
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        System.out.println(jarray.size());
        for(int i=0; i<jarray.size(); i++) {
        	
        	jsonobject = jarray.get(i).getAsJsonObject();
	
	        
		    JsonArray jarraylookups = jsonobject.getAsJsonArray("lookups");
		    
		    HashMap<String, Long> mapofquantityType = new HashMap<String, Long>();
		    ArrayList<String> listoflookupkeys = new ArrayList<String>();
		    ArrayList<Long> listoflookupstartvalues = new ArrayList<Long>();

		    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

		    
		    for(int lookupsindex=0; lookupsindex<jarraylookups.size(); lookupsindex++) {
		    	
		    	JsonObject jsonobjectlookupsBody = jarraylookups.get(lookupsindex).getAsJsonObject();
	        	
	        	String lookupsJsonKey = jsonobjectlookupsBody.get("type").toString().replaceAll("^\"|\"$", "");
	        	
	        	JsonArray jarraylookupsBody = jsonobjectlookupsBody.getAsJsonArray("values");
		        
	        	if(lookupsJsonKey.equals("SINGLE-PICK")) {
	        		
	        		
	        		for(int lookupsBodyindex=0; lookupsBodyindex<jarraylookupsBody.size(); lookupsBodyindex++) {
		        		
				        JsonObject jsonobjectlookupsBodyValue = jarraylookupsBody.get(lookupsBodyindex).getAsJsonObject();

				        String lookupsJsonvalue = jsonobjectlookupsBodyValue.get("value").toString().replaceAll("^\"|\"$", "");
				        String lookupsJsonstartdate = jsonobjectlookupsBodyValue.get("start").toString().replaceAll("^\"|\"$", "");
				       System.out.println(lookupsJsonvalue + " " + lookupsJsonstartdate);
				        Date date = formatter.parse(lookupsJsonstartdate.replaceAll("Z$", "+0000"));
				    	
				    	long startdatemilli = date.getTime();
				        
				        mapofquantityType.put(lookupsJsonvalue, startdatemilli);
		        		
		        	}
	        	
 	        		
	        	}
	    	
		    }
		    
		    for(String str: mapofquantityType.keySet()) {
		    	
		    	listoflookupkeys.add(str);
		    	listoflookupstartvalues.add(mapofquantityType.get(str));
		    }

		    long latestdatemilli = 0; 
		    
		    for(int startdateindex=0; startdateindex<listoflookupstartvalues.size(); startdateindex++) {
		    			    	
		    	if(listoflookupstartvalues.get(startdateindex) > latestdatemilli) {
		    		
		    		latestdatemilli = listoflookupstartvalues.get(startdateindex);
		    		
		    	}
		    	
		    }
		    
		    String finalSinglePickkey = null;
		    String quantityTypeFromWebPortal = null;
		    
		    for(int mapofquantityTypeindex=0; mapofquantityTypeindex<mapofquantityType.size(); mapofquantityTypeindex++) {
		    	
		    	if( (mapofquantityType.get(listoflookupkeys.get(mapofquantityTypeindex))) == latestdatemilli) {
		    		
		    		finalSinglePickkey = listoflookupkeys.get(mapofquantityTypeindex);
		    	}
		    	
		    }
		    
		    System.out.println("latest date in milli "+latestdatemilli);
		    
		    if(finalSinglePickkey.equals("Y")) {
		    	
		    	quantityTypeFromWebPortal = "EA";
		    }
		    
		    if(finalSinglePickkey.equals("N")) {
		    	
		    	quantityTypeFromWebPortal = "CS";
		    }
		    res = quantityTypeFromWebPortal;
			
        }
        	  
       
	} catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e,Resultpath,xwpfRun,"");
        
		res = null;
		return res;
	}
    
    finally{
        
        response.close();
}
    return res;
	
}

public static String GetCallquantityTypeFromWebPortal(String CatalogueURL,String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String ItemId, String tcid, String Resultpath,XWPFRun xwpfRun, ExtentTest logger) throws IOException {
	
	String res = null;
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        //https://sit-api.morrisons.com/wholesaleorder/v1/customers/mccolls/catalogues/main/items/101743031?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36&filtertype=maptype&filtervalue=PIN
        
        HttpGet httpget = new HttpGet(CatalogueURL+ ItemId + "?" + ApiKey+"&filtertype=maptype&filtervalue=PIN");

        
//        HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/sandpiper/catalogues/main/items/" + ItemId + "?" + ApiKey+"&filtertype=maptype&filtervalue=PIN");
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Item ID is " + ItemId);
        utilityFileWriteOP.writeToLog(tcid, "Searching the attributes for ", "Item ID "+ItemId,Resultpath);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",Resultpath);
        
     /*   for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
        	
        	Thread.sleep(regularWaitingTime);*/
        
        response = httpclient.execute(target, httpget);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),Resultpath);
        
        String jsonresponse = EntityUtils.toString(response.getEntity());
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,Resultpath);
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString(),Resultpath);
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        System.out.println(jarray.size());
        for(int i=0; i<jarray.size(); i++) {
        	
        	jsonobject = jarray.get(i).getAsJsonObject();
	
	        
		    JsonArray jarraylookups = jsonobject.getAsJsonArray("lookups");
		    
		    HashMap<String, Long> mapofquantityType = new HashMap<String, Long>();
		    ArrayList<String> listoflookupkeys = new ArrayList<String>();
		    ArrayList<Long> listoflookupstartvalues = new ArrayList<Long>();

		    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

		    
		    for(int lookupsindex=0; lookupsindex<jarraylookups.size(); lookupsindex++) {
		    	
		    	JsonObject jsonobjectlookupsBody = jarraylookups.get(lookupsindex).getAsJsonObject();
	        	
	        	String lookupsJsonKey = jsonobjectlookupsBody.get("type").toString().replaceAll("^\"|\"$", "");
	        	
	        	JsonArray jarraylookupsBody = jsonobjectlookupsBody.getAsJsonArray("values");
		        
	        	if(lookupsJsonKey.equals("SINGLE-PICK")) {
	        		
	        		
	        		for(int lookupsBodyindex=0; lookupsBodyindex<jarraylookupsBody.size(); lookupsBodyindex++) {
		        		
				        JsonObject jsonobjectlookupsBodyValue = jarraylookupsBody.get(lookupsBodyindex).getAsJsonObject();

				        String lookupsJsonvalue = jsonobjectlookupsBodyValue.get("value").toString().replaceAll("^\"|\"$", "");
				        String lookupsJsonstartdate = jsonobjectlookupsBodyValue.get("start").toString().replaceAll("^\"|\"$", "");
				       System.out.println(lookupsJsonvalue + " " + lookupsJsonstartdate);
				        Date date = formatter.parse(lookupsJsonstartdate.replaceAll("Z$", "+0000"));
				    	
				    	long startdatemilli = date.getTime();
				        
				        mapofquantityType.put(lookupsJsonvalue, startdatemilli);
		        		
		        	}
	        	
 	        		
	        	}
	    	
		    }
		    
		    for(String str: mapofquantityType.keySet()) {
		    	
		    	listoflookupkeys.add(str);
		    	listoflookupstartvalues.add(mapofquantityType.get(str));
		    }

		    long latestdatemilli = 0; 
		    
		    for(int startdateindex=0; startdateindex<listoflookupstartvalues.size(); startdateindex++) {
		    			    	
		    	if(listoflookupstartvalues.get(startdateindex) > latestdatemilli) {
		    		
		    		latestdatemilli = listoflookupstartvalues.get(startdateindex);
		    		
		    	}
		    	
		    }
		    
		    String finalSinglePickkey = null;
		    String quantityTypeFromWebPortal = null;
		    
		    for(int mapofquantityTypeindex=0; mapofquantityTypeindex<mapofquantityType.size(); mapofquantityTypeindex++) {
		    	
		    	if( (mapofquantityType.get(listoflookupkeys.get(mapofquantityTypeindex))) == latestdatemilli) {
		    		
		    		finalSinglePickkey = listoflookupkeys.get(mapofquantityTypeindex);
		    	}
		    	
		    }
		    
		    System.out.println("latest date in milli "+latestdatemilli);
		    
		    if(finalSinglePickkey.equals("Y")) {
		    	
		    	quantityTypeFromWebPortal = "EA";
		    }
		    
		    if(finalSinglePickkey.equals("N")) {
		    	
		    	quantityTypeFromWebPortal = "CS";
		    }
		    
		    
		    //String itemDetailsbody = "{\"quantityType\": \"" + quantityTypeFromWebPortal + "\",\"itemId\": \"" + ItemId +"\",\"itemAlternateId\": {\"clientId\": \"" + mapofMaps.get("CLIENTID") +"\",\"barcodeEan\": \"" + mapofMaps.get("EAN") +"\",\"skuPin\": \"" + mapofMaps.get("PIN") +"\"},\"priceOrderedAmount\": " + mapofPrices.get("WSP") + ",\"priceOrderedCurrency\": \"" + priceOrderedCurrency + "\",\"itemBaseType\": \"" + itemBaseType + "\",\"itemLineId\": \"" + itemLineId + "\",\"quantityOrdered\": " + quantityOrdered + ",\"priceOrderedTaxRate\": " + mapofPrices.get("VAT") + ",\"itemDescription\": \"" + description + "\",\"itemCaseSize\": " + mapofPrices.get("CS").substring(0, mapofPrices.get("CS").indexOf(".")) + "}";
		    	    
		    //System.out.println(itemDetailsbody);	    
		    	     
		    res = quantityTypeFromWebPortal;
			
			
        	
        }
        	  
       
	} catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e,Resultpath);
        
		res = null;
		return res;
	}
    
    finally{
        
        response.close();
        
}
    
    return res;
    
	
}




public static String GetCallItemDetailsFromWebPortal(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String ItemId, String itemBaseType, String quantityType, String itemLineId, String quantityOrdered, String tcid, String Resultpath,XWPFRun xwpfRun, ExtentTest logger) throws IOException {
	
	String res = null;
	
	int MaxwaitingTime = 60000;
	
	int regularWaitingTime = 10000;
	
	HashMap<String, String> mapofMaps = new HashMap<String, String>();
	HashMap<String, String> mapofPrices = new HashMap<String, String>();

	String priceOrderedCurrency = null;
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        //https://sit-api.morrisons.com/wholesaleorder/v1/customers/mccolls/catalogues/main/items/101743031?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36&filtertype=maptype&filtervalue=PIN
        
        HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/mccolls/catalogues/main/items/" + ItemId + "?" + ApiKey+"&filtertype=maptype&filtervalue=PIN");
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Item ID is " + ItemId);
        utilityFileWriteOP.writeToLog(tcid, "Searching the attributes for ", "Item ID "+ItemId,Resultpath);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",Resultpath,xwpfRun,"");
        logger.log(LogStatus.INFO, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        
     /*   for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
        	
        	Thread.sleep(regularWaitingTime);*/
        
        response = httpclient.execute(target, httpget);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),Resultpath,xwpfRun,"");
        
        logger.log(LogStatus.INFO, "Response code for item attributes is displayed as: "+response.getStatusLine().getStatusCode() +"for item id:"+ItemId);
        String jsonresponse = EntityUtils.toString(response.getEntity());
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,Resultpath);
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString(),Resultpath,xwpfRun,"");
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        System.out.println(jarray.size());
        for(int i=0; i<jarray.size(); i++) {
        	
        	jsonobject = jarray.get(i).getAsJsonObject();
        	
        	String description = jsonobject.get("description").toString().replaceAll("^\"|\"$", "");
        	
        	//String mapsJsonBody = jsonobject.get("maps").toString().replaceAll("^\"|\"$", "");
	        JsonArray jarrayMaps = jsonobject.getAsJsonArray("maps");
	        
	        for(int mapsindex=0; mapsindex<jarrayMaps.size(); mapsindex++) {
	        	
	        	JsonObject jsonobjectMapsBody = jarrayMaps.get(mapsindex).getAsJsonObject();
	        	
	        	String mapJsonKey = jsonobjectMapsBody.get("type").toString().replaceAll("^\"|\"$", "");
	        	
		        JsonArray jarrayMapsBody = jsonobjectMapsBody.getAsJsonArray("values");
		        
		        JsonObject jsonobjectMapsBodyValue = jarrayMapsBody.get(0).getAsJsonObject();
		        
		        String mapJsonvalue = jsonobjectMapsBodyValue.get("value").toString().replaceAll("^\"|\"$", "");
		        
		        mapofMaps.put(mapJsonKey, mapJsonvalue);

	        	
	        }
	        
		    JsonArray jarrayPrices = jsonobject.getAsJsonArray("prices");

	        
	        for(int pricesindex=0; pricesindex<jarrayPrices.size(); pricesindex++) {
	        	
	        	JsonObject jsonobjectPricesBody = jarrayPrices.get(pricesindex).getAsJsonObject();
	        	
	        	String pricesJsonKey = jsonobjectPricesBody.get("type").toString().replaceAll("^\"|\"$", "");
	        	
		        JsonArray jarrayPricesBody = jsonobjectPricesBody.getAsJsonArray("values");
		        
		        JsonObject jsonobjectPricesBodyValue = jarrayPricesBody.get(0).getAsJsonObject();
		        
		        String pricesJsonvalue = jsonobjectPricesBodyValue.get("value").toString().replaceAll("^\"|\"$", "");
		        
		        mapofPrices.put(pricesJsonKey, pricesJsonvalue);
		        
		        if(pricesJsonKey.equals("WSP")) {
		        	
			        priceOrderedCurrency = jsonobjectPricesBodyValue.get("currency").toString().replaceAll("^\"|\"$", "");
			        mapofPrices.put("priceOrderedCurrency", priceOrderedCurrency);
		        }
	        	
	        }
	        
	       	//fetching  priceOrderedCurrency
		    
		    System.out.println(mapofMaps.size());
		    System.out.println(mapofPrices.size());
		    
		    String itemDetailsbody = "{\"quantityType\": \"" + quantityType + "\",\"itemId\": \"" + ItemId +"\",\"itemAlternateId\": {\"clientId\": \"" + mapofMaps.get("CLIENTID") +"\",\"barcodeEan\": \"" + mapofMaps.get("EAN") +"\",\"skuPin\": \"" + mapofMaps.get("PIN") +"\"},\"priceOrderedAmount\": " + mapofPrices.get("WSP") + ",\"priceOrderedCurrency\": \"" + priceOrderedCurrency + "\",\"itemBaseType\": \"" + itemBaseType + "\",\"itemLineId\": \"" + itemLineId + "\",\"quantityOrdered\": " + quantityOrdered + ",\"priceOrderedTaxRate\": " + mapofPrices.get("VAT") + ",\"itemDescription\": \"" + description + "\",\"itemCaseSize\": " + mapofPrices.get("CS").substring(0, mapofPrices.get("CS").indexOf(".")) + "}";
		    	    
		    System.out.println(itemDetailsbody);	    
		    	     
		    res = itemDetailsbody;
        }
        	  
    	
	} catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e,Resultpath);
        
		res = null;
		return res;
	}
    
    finally{
        
        response.close();
}
    
    return res;
}


public static String GetCallVatAmountFromWebPortal(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String ItemId, String tcid, String Resultpath,XWPFRun xwpfRun, ExtentTest logger) throws IOException {
	
	String res = null;
	
	String EBSVatDetails = null;
	
	String pricesJsonvalue = null;
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        //https://sit-api.morrisons.com/wholesaleorder/v1/customers/mccolls/catalogues/main/items/101743031?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36&filtertype=maptype&filtervalue=PIN
        
        HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/mccolls/catalogues/main/items/" + ItemId + "?" + ApiKey+"&filtertype=maptype&filtervalue=PIN");
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Item ID is " + ItemId);
        utilityFileWriteOP.writeToLog(tcid, "Searching the attributes for ", "ItemId "+ItemId,Resultpath,xwpfRun,"");
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",Resultpath,xwpfRun,"");
        
        response = httpclient.execute(target, httpget);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),Resultpath,xwpfRun,"");
        
        String jsonresponse = EntityUtils.toString(response.getEntity());
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,Resultpath,xwpfRun,"");
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString(),Resultpath,xwpfRun,"");
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        System.out.println(jarray.size());
        for(int i=0; i<jarray.size(); i++) {
        	
        	jsonobject = jarray.get(i).getAsJsonObject();
	        
        	JsonArray jarrayprices = jsonobject.getAsJsonArray("prices");
		    
		    for(int pricesindex=0; pricesindex<jarrayprices.size(); pricesindex++) {
		    	
		    	JsonObject jsonobjectpricesBody = jarrayprices.get(pricesindex).getAsJsonObject();
	        	
	        	String pricesJsonKey = jsonobjectpricesBody.get("type").toString().replaceAll("^\"|\"$", "");
	        	
	        	JsonArray jarraypricesBody = jsonobjectpricesBody.getAsJsonArray("values");
		        
        		
	        		if(pricesJsonKey.equals("VAT")) {
	        			
	        			
	        			for(int pricesBodyindex=0; pricesBodyindex<jarraypricesBody.size(); pricesBodyindex++) {
			        		
					        JsonObject jsonobjectpricesBodyValue = jarraypricesBody.get(pricesBodyindex).getAsJsonObject();

					        pricesJsonvalue = jsonobjectpricesBodyValue.get("value").toString().replaceAll("^\"|\"$", "");
					     
			        	}	        		
	        	}	        	
		    }
		   
		    Double vatRatevalueDouble = Double.parseDouble(pricesJsonvalue);
		    
		    
		    int vatRateInt = vatRatevalueDouble.intValue();
		    
		    if( (vatRateInt <= 5) && (vatRateInt > 0) ) {

		    	EBSVatDetails = "LOWER";
		    }
		    else

		    	if(vatRateInt >= 20) {

		    		EBSVatDetails = "STANDARD 20";
		    	}
		    	else
		    		if(vatRateInt == 0) {
		    			EBSVatDetails = "ZERO";
		    		}
		    		else
		    		{
		    			EBSVatDetails = "MIXED";
		    		}
		    
		    
		   	     
		    res = EBSVatDetails;
			
			
        	
        }
        	  
       
	} catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e,Resultpath);
        
		res = null;
		return res;
	}
    
    finally{
        
        response.close();
        
}
    
    return res;
    
	
}



public static String GetCallVatAmountFromWebPortal(String CatalogueURL,String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String ItemId, String tcid, String Resultpath,XWPFRun xwpfRun, ExtentTest logger) throws IOException {
	
	String res = null;
	
	String EBSVatDetails = null;
	
	String pricesJsonvalue = null;
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        //https://sit-api.morrisons.com/wholesaleorder/v1/customers/mccolls/catalogues/main/items/101743031?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36&filtertype=maptype&filtervalue=PIN
        HttpGet httpget = new HttpGet(CatalogueURL+ ItemId + "?" + ApiKey+"&filtertype=maptype&filtervalue=PIN");
//        HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/mccolls/catalogues/main/items/" + ItemId + "?" + ApiKey+"&filtertype=maptype&filtervalue=PIN");
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Item ID is " + ItemId);
        utilityFileWriteOP.writeToLog(tcid, "Searching the attributes for ", "ItemId "+ItemId,Resultpath,xwpfRun,"");
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",Resultpath,xwpfRun,"");
        
        response = httpclient.execute(target, httpget);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),Resultpath,xwpfRun,"");
        
        String jsonresponse = EntityUtils.toString(response.getEntity());
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,Resultpath,xwpfRun,"");
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString(),Resultpath,xwpfRun,"");
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        System.out.println(jarray.size());
        for(int i=0; i<jarray.size(); i++) {
        	
        	jsonobject = jarray.get(i).getAsJsonObject();
	        
        	JsonArray jarrayprices = jsonobject.getAsJsonArray("prices");
		    
		    for(int pricesindex=0; pricesindex<jarrayprices.size(); pricesindex++) {
		    	
		    	JsonObject jsonobjectpricesBody = jarrayprices.get(pricesindex).getAsJsonObject();
	        	
	        	String pricesJsonKey = jsonobjectpricesBody.get("type").toString().replaceAll("^\"|\"$", "");
	        	
	        	JsonArray jarraypricesBody = jsonobjectpricesBody.getAsJsonArray("values");
		        
        		
	        		if(pricesJsonKey.equals("VAT")) {
	        			
	        			
	        			for(int pricesBodyindex=0; pricesBodyindex<jarraypricesBody.size(); pricesBodyindex++) {
			        		
					        JsonObject jsonobjectpricesBodyValue = jarraypricesBody.get(pricesBodyindex).getAsJsonObject();

					        pricesJsonvalue = jsonobjectpricesBodyValue.get("value").toString().replaceAll("^\"|\"$", "");
					     
			        	}	        		
	        	}	        	
		    }
		   
		    Double vatRatevalueDouble = Double.parseDouble(pricesJsonvalue);
		    
		    
		    int vatRateInt = vatRatevalueDouble.intValue();
		    
		    if( (vatRateInt <= 5) && (vatRateInt > 0) ) {

		    	EBSVatDetails = "LOWER";
		    }
		    else

		    	if(vatRateInt >= 20) {

		    		EBSVatDetails = "STANDARD 20";
		    	}
		    	else
		    		if(vatRateInt == 0) {
		    			EBSVatDetails = "ZERO";
		    		}
		    		else
		    		{
		    			EBSVatDetails = "MIXED";
		    		}
		    
		    
		   	     
		    res = EBSVatDetails;
			
			
        	
        }
        	  
       
	} catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e,Resultpath);
        
		res = null;
		return res;
	}
    
    finally{
        
        response.close();
        
}
    
    return res;
    
	
}


public static boolean PostCallOrderCreationGeneralSeperateItemDetails(String DriverExcelPath, String ItemDetailsSheetName, String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String MessageType, String ShipToLocationId, String OrderCount,String ShipToDeliverAt, String AllOrderAttributes, String AllMessageAttributes, String AllShipToAddressAttributes, String AllBillToAddressAttributes, String AllBillToTaxAddressAttributes, String AllItemAttributes, String TestKeyword, String tcid,String ResultPath,XWPFRun xwfprun, ExtentTest logger) throws IOException {
	
	boolean res = false;
	
	//String ItemID[] = AllItemIDs.split(",");
	//String QuantityOrdered[] = AllQuantitiesOrdered.split(",");
	String OrderAttributes[] = AllOrderAttributes.split(",");
	String MessageAttributes[] = AllMessageAttributes.split(",");
	String ShipToAddressAttributes[] = AllShipToAddressAttributes.split(",");
	String BillToAddressAttributes[] = AllBillToAddressAttributes.split(",");
	String BillToTaxAddressAttributes[] = AllBillToTaxAddressAttributes.split(",");
	//String ItemAttributes[] = AllItemAttributes.split(",");
	
	String ItemDetails = "";
	String Keyword = null;
	String Items = null;
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    int r = 0;
    int occurances = 0;
    
    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpPost httppost = new HttpPost(UrlTail + "?" + ApiKey);
        
        httppost.setConfig(config);
        
        httppost.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Executing request " + httppost.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httppost.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwfprun,"");
        logger.log (LogStatus.INFO, "Executing request " + httppost.getRequestLine() + " to " + target + " via " + proxy);
        
        Workbook wrk1 = Workbook.getWorkbook(new File(DriverExcelPath));
		
		Sheet sheet1 = wrk1.getSheet(ItemDetailsSheetName);
		
		int rows = sheet1.getRows();
		//int cols = sheet1.getColumns();
		
		for(r=1; r<rows; r++) {
			
			Keyword = sheet1.getCell(0, r).getContents().trim();
			
			if(occurances>0){
		          
		    	if(!(TestKeyword.contentEquals(Keyword))){

			    	break;
			    	
		    	}

			 }
			
			if(Keyword.equalsIgnoreCase(TestKeyword)) {
				  
				 occurances=occurances+1;
			
			Items = sheet1.getCell(1, r).getContents().trim();
			
			String[] AllItemsInItemDetails = Items.split(",");
			
			//System.out.println("Number of ItemIDs " + ItemID.length);
			System.out.println("Items with details " + AllItemsInItemDetails.length);
			
			for(int c=2; c< 2 + AllItemsInItemDetails.length; c++) {
				
				String ItemDetailsJson = sheet1.getCell(c, r).getContents().trim();
				
				ItemDetails = ItemDetails + ItemDetailsJson;
	        	
	        	if(c!=AllItemsInItemDetails.length + 1)
	        		ItemDetails = ItemDetails + ",";
			}
			}
			
		}
        
        //String json = "{ \"orders\": {\"orderId\": \"387429-02\", \"orderBuyerPartyId\": \"5013546056078\", \"orderSellerPartyId\": \"5013546229809\", \"orderReferenceCode\": \"Local-Order-3607-20-20170314\",\"messageId\": \"FS-20170811-133400\", \"messageSenderPartyId\": \"5013546056078\", \"messageRecipientPartyId\": \"5013546229809\", \"messageType\": \"MOR\", \"messageCreatedAt\": \"2017-08-11T13:34:00Z\",\"shipToPartyId\": \"MOR-HEEL\", \"shipToLocationId\": \"542\", \"shipToAddressName\": \"Palmer Harvey\", \"shipToAddressLine1\": \"Maxted Road\", \"shipToAddressLine2\": \"--\", \"shipToAddressCity\": \"Hemel Hempstead\", \"shipToAddressState\": \"Herts\", \"shipToAddressPostCode\": \"HP2 7DX\", \"shipToAddressCountryCode\": \"UK\",\"shipToDeliverAt\": \"2017-03-15\", \"shipToDeliverLatestAt\": \"2017-03-15\",\"billToPartyId\": \"5013546056078\", \"billToAddressName\": \"PALMER AND HARVEY MCLANE\", \"billToAddressLine1\": \"P&H HOUSE\", \"billToAddressLine2\": \"DAVIGDOR ROAD\", \"billToAddressCity\": \"HOVE\", \"billToAddressState\": \"EAST SUSSEX\", \"billToAddressPostCode\": \"BN3 1RE\", \"billToAddressCountryCode\": \"UK\",\"billToTaxId\": \"GB232169089\", \"billToTaxAddressName\": \"PALMER AND HARVEY MCLANE\", \"billToTaxAddressLine1\": \"P&H HOUSE\", \"billToTaxAddressLine2\": \"DAVIGDOR ROAD\", \"billToTaxAddressCity\": \"HOVE\", \"billToTaxAddressState\": \"EAST SUSSEX\", \"billToTaxAddressPostCode\": \"BN3 1RE\", \"billToTaxAddressCountryCode\": \"UK\"}, \"items\": [{\"itemId\": \"104466391\", \"itemLineId\": 1, \"itemBaseType\": \"skuMin\", \"itemDescription\": \"M Pork Sausage Rolls 6s\",\"itemAlternateId\": { \"skuMin\": \"104466391\",\"skuLegacy\": \"0617707\", \"phId\": \"53238\" },\"quantityType\": \"CS\", \"quantityOrdered\": 1,\"priceOrderedCurrency\": \"GBP\", \"priceOrderedAmount\": 1.75, \"priceOrderedTaxRate\": 0},{\"itemId\": \"105462210\", \"itemLineId\": 2, \"itemBaseType\": \"skuMin\", \"itemDescription\": \"M Banana Flav Milk 1L\",\"itemAlternateId\": { \"skuMin\": \"105462210\",\"skuLegacy\": \"0403674\", \"phId\": \"53241\" },\"quantityType\": \"CS\", \"quantityOrdered\": 1,\"priceOrderedCurrency\": \"GBP\", \"priceOrderedAmount\": 1.29, \"priceOrderedTaxRate\": 0}] }";
       
        //{\"itemId\": \"" + ItemID[0] + "\",\"itemLineId\": 1,\"itemBaseType\": \"skuMin\",\"itemAlternateId\": {\"skuMin\": \"M104593859\",\"skuPin\": \"P104593867\",\"skuLegacy\": \"L0472851\",\"barcodeAsin\": \"ASIN29384845\",\"barcodeEan\": \"EAN3928445\"},\"itemDescription\": \"Heinz Baked Beans\",\"quantityType\": \"unit\",\"quantityOrdered\": " + QuantityOrdered[0] + ",\"priceOrderedCurrency\": \"GBP\",\"priceOrderedAmount\": 1.00,\"priceOrderedTaxRate\": 20.00},{\"itemId\": \"" + ItemID[1] + "\",\"itemLineId\": 2,\"itemBaseType\": \"skuMin\",\"itemAlternateId\": {\"skuMin\": \"M104593859\",\"skuPin\": \"P103201067\",\"skuLegacy\": \"L0419068\",\"barcodeAsin\": \"ASIN29383445\",\"barcodeEan\": \"EAN39234545\"},\"itemDescription\": \"Walkers Crips\",\"quantityType\": \"unit\",\"quantityOrdered\": " + QuantityOrdered[1] + ",\"priceOrderedCurrency\": \"GBP\",\"priceOrderedAmount\": 2.00,\"priceOrderedTaxRate\": 5.00}
        
        
       // *******JSON updated S*******************
        //String json = "{  \"orders\":{\"orderId\": \"" + OrderID + "\",\"orderBuyerPartyId\": \"" + OrderAttributes[0] + "\",\"orderSellerPartyId\": \"" + OrderAttributes[1] + "\",\"orderReferenceCode\": \"" + OrderAttributes[2] + "\",\"messageId\": \"" + MessageAttributes[0] + "\",\"messageSenderPartyId\": \"" + MessageAttributes[1] + "\",\"messageRecipientPartyId\": \"" + MessageAttributes[2] + "\",\"messageType\": \"" + MessageType + "\",\"messageCreatedAt\": \"" + MessageAttributes[3] + "\",\"shipToPartyId\": \"" + ShipToAddressAttributes[0] + "\",\"shipToLocationId\": \"" + ShipToLocationId + "\",\"shipToAddressName\": \"" + ShipToAddressAttributes[1] + "\",\"shipToAddressLine1\": \"" + ShipToAddressAttributes[2] + "\",\"shipToAddressLine2\": \"" + ShipToAddressAttributes[3] + "\",\"shipToAddressCity\": \"" + ShipToAddressAttributes[4] + "\",\"shipToAddressState\": \"" + ShipToAddressAttributes[5] + "\",\"shipToAddressPostCode\": \"" + ShipToAddressAttributes[6] + "\",\"shipToAddressCountryCode\": \"" + ShipToAddressAttributes[7] + "\",\"shipToDeliverAt\": \"" + ShipToDeliverAt + "\",\"shipToDeliverLatestAt\": \"" + ShipToAddressAttributes[8] + "\",\"shipFromLocationId\": \"" + ShipToAddressAttributes[9] + "\",\"shipFromAddressName\": \"" + ShipToAddressAttributes[10] + "\",\"billToPartyId\": \"" + BillToAddressAttributes[0] + "\",\"billToAddressName\": \"" + BillToAddressAttributes[1] + "\",\"billToAddressLine1\": \"" + BillToAddressAttributes[2] + "\",\"billToAddressLine2\": \"" + BillToAddressAttributes[3] + "\",\"billToAddressCity\": \"" + BillToAddressAttributes[4] + "\",\"billToAddressState\": \"" + BillToAddressAttributes[5] + "\",\"billToAddressPostCode\": \"" + BillToAddressAttributes[6] + "\",\"billToAddressCountryCode\": \"" + BillToAddressAttributes[7] + "\",\"billToTaxId\": \"" + BillToTaxAddressAttributes[0] + "\",\"billToTaxAddressName\": \"" + BillToTaxAddressAttributes[1] + "\",\"billToTaxAddressLine1\": \"" + BillToTaxAddressAttributes[2] + "\",\"billToTaxAddressLine2\": \"" + BillToTaxAddressAttributes[3] + "\",\"billToTaxAddressCity\": \"" + BillToTaxAddressAttributes[4] + "\",\"billToTaxAddressState\": \"" + BillToTaxAddressAttributes[5] + "\",\"billToTaxAddressPostCode\": \"" + BillToTaxAddressAttributes[6] + "\",\"billToTaxAddressCountryCode\": \"" + BillToTaxAddressAttributes[7] + "\"},\"items\": [" + ItemDetails + "]}";
        String json = "{  \"orders\":{\"orderId\": \"" + OrderID + "\",\"shipToLocationId\": \"" + ShipToLocationId + "\",\"orderCount\": \"" +OrderCount + "\",\"shipToPartyId\": \"" + ShipToAddressAttributes[0] + "\",\"orderSellerPartyId\": \"" + OrderAttributes[1] + "\",\"billToAddressName\": \"" + BillToAddressAttributes[1] + "\",\"shipToAddressLine1\": \"" + ShipToAddressAttributes[2] + "\",\"shipToAddressCity\": \"" + ShipToAddressAttributes[4] + "\",\"shipToAddressLine2\": \"" + ShipToAddressAttributes[3] + "\",\"billToTaxAddressCity\": \"" + BillToTaxAddressAttributes[4] + "\",\"orderReferenceCode\": \"" + OrderAttributes[2] + "\",\"messageType\": \"" + MessageType + "\",\"billToPartyId\": \"" + BillToAddressAttributes[0] + "\",\"shipToAddressPostCode\": \"" + ShipToAddressAttributes[6] + "\",\"billToAddressLine1\": \"" + BillToTaxAddressAttributes[2] + "\",\"billToTaxAddressPostCode\": \"" + BillToTaxAddressAttributes[6] + "\",\"billToAddressLine2\": \"" + BillToAddressAttributes[3] + "\",\"billToAddressPostCode\": \"" + BillToAddressAttributes[6] + "\",\"messageSenderPartyId\": \"" + MessageAttributes[1] + "\",\"shipToAddressState\": \"" + ShipToAddressAttributes[5] + "\",\"shipToDeliverAt\": \"" + ShipToDeliverAt + "\",\"shipToAddressName\": \"" + ShipToAddressAttributes[1] + "\",\"messageCreatedAt\": \"" + MessageAttributes[3] + "\",\"billToAddressState\": \"" + BillToAddressAttributes[5] + "\",\"billToTaxId\": \"" + BillToTaxAddressAttributes[0] + "\",\"messageId\": \"" + MessageAttributes[0] + "\",\"billToAddressCountryCode\": \"" + BillToAddressAttributes[7] + "\",\"billToTaxAddressState\": \"" + BillToTaxAddressAttributes[5] + "\",\"billToTaxAddressCountryCode\": \"" + BillToTaxAddressAttributes[7] + "\",\"messageRecipientPartyId\": \"" + MessageAttributes[2] + "\",\"shipToAddressCountryCode\": \"" + ShipToAddressAttributes[7] + "\",\"orderBuyerPartyId\": \"" + OrderAttributes[0] + "\",\"billToTaxAddressLine1\": \"" + BillToTaxAddressAttributes[2] + "\",\"shipToDeliverLatestAt\": \"" + ShipToAddressAttributes[8] + "\",\"billToTaxAddressLine2\": \"" + BillToTaxAddressAttributes[3] + "\",\"billToAddressCity\": \"" + BillToAddressAttributes[4] + "\",\"billToTaxAddressName\": \"" + BillToTaxAddressAttributes[1] + "\"},\"items\": [" + ItemDetails + "]}";
        
//        String json = "{\"orders\":{\"orderId\": \"" + OrderID + "\",\"shipToLocationId\": \"" + ShipToLocationId + "\",\"orderCount\": \"" +OrderCount + "\",\"shipToPartyId\": \"" + ShipToAddressAttributes[0] + "\",\"orderSellerPartyId\": \"" + OrderAttributes[1] + "\",\"billToAddressName\": \"" + BillToAddressAttributes[1] + "\",\"shipToAddressLine1\": \"" + ShipToAddressAttributes[2] + "\",\"shipToAddressCity\": \"" + ShipToAddressAttributes[4] + "\",\"shipToAddressLine2\": \"" + ShipToAddressAttributes[3] + "\",\"billToTaxAddressCity\": \"" + BillToTaxAddressAttributes[4] + "\",\"orderReferenceCode\": \"" + OrderReferenceCode + "\",\"messageType\": \"" + MessageType + "\",\"billToPartyId\": \"" + BillToAddressAttributes[0] + "\",\"shipToAddressPostCode\": \"" + ShipToAddressAttributes[6] + "\",\"billToAddressLine1\": \"" + BillToTaxAddressAttributes[2] + "\",\"billToTaxAddressPostCode\": \"" + BillToTaxAddressAttributes[6] + "\",\"billToAddressLine2\": \"" + BillToAddressAttributes[3] + "\",\"billToAddressPostCode\": \"" + BillToAddressAttributes[6] + "\",\"messageSenderPartyId\": \"" + MessageAttributes[1] + "\",\"shipToAddressState\": \"" + ShipToAddressAttributes[5] + "\",\"shipToDeliverAt\": \"" + ShipToDeliverAt + "\",\"shipToAddressName\": \"" + ShipToAddressAttributes[1] + "\",\"messageCreatedAt\": \"" + MessageAttributes[3] + "\",\"billToAddressState\": \"" + BillToAddressAttributes[5] + "\",\"billToTaxId\": \"" + BillToTaxAddressAttributes[0] + "\",\"messageId\": \"" + MessageAttributes[0] + "\",\"billToAddressCountryCode\": \"" + BillToAddressAttributes[7] + "\",\"billToTaxAddressState\": \"" + BillToTaxAddressAttributes[5] + "\",\"billToTaxAddressCountryCode\": \"" + BillToTaxAddressAttributes[7] + "\",\"messageRecipientPartyId\": \"" + MessageAttributes[2] + "\",\"shipToAddressCountryCode\": \"" + ShipToAddressAttributes[7] + "\",\"orderBuyerPartyId\": \"" + OrderAttributes[0] + "\",\"billToTaxAddressLine1\": \"" + BillToTaxAddressAttributes[2] + "\",\"shipToDeliverLatestAt\": \"" + ShipToAddressAttributes[8] + "\",\"billToTaxAddressLine2\": \"" + BillToTaxAddressAttributes[3] + "\",\"billToAddressCity\": \"" + BillToAddressAttributes[4] + "\",\"billToTaxAddressName\": \"" + BillToTaxAddressAttributes[1] + "\"},\"items\": [" + ItemDetails + "]}";

        //String json = "{  \"orders\":{\"orderId\": \"" + OrderID + "\",\"shipToLocationId\": \"" + ShipToLocationId + "\",\"shipToPartyId\": \"" + ShipToAddressAttributes[0] + "\",\"orderSellerPartyId\": \"" + OrderAttributes[1] + "\",\"billToAddressName\": \"" + BillToAddressAttributes[1] + "\",\"shipToAddressLine1\": \"" + ShipToAddressAttributes[2] + "\",\"shipToAddressCity\": \"" + ShipToAddressAttributes[4] + "\",\"shipToAddressLine2\": \"" + ShipToAddressAttributes[3] + "\",\"billToTaxAddressCity\": \"" + BillToTaxAddressAttributes[4] + "\",\"orderReferenceCode\": \"" + OrderAttributes[2] + "\",\"messageType\": \"" + MessageType + "\",\"billToPartyId\": \"" + BillToAddressAttributes[0] + "\",\"shipToAddressPostCode\": \"" + ShipToAddressAttributes[6] + "\",\"billToAddressLine1\": \"" + BillToTaxAddressAttributes[2] + "\",\"billToTaxAddressPostCode\": \"" + BillToTaxAddressAttributes[6] + "\",\"billToAddressLine2\": \"" + BillToAddressAttributes[3] + "\",\"billToAddressPostCode\": \"" + BillToAddressAttributes[6] + "\",\"messageSenderPartyId\": \"" + MessageAttributes[1] + "\",\"shipToAddressState\": \"" + ShipToAddressAttributes[5] + "\",\"shipToDeliverAt\": \"" + ShipToDeliverAt + "\",\"shipToAddressName\": \"" + ShipToAddressAttributes[1] + "\",\"messageCreatedAt\": \"" + MessageAttributes[3] + "\",\"billToAddressState\": \"" + BillToAddressAttributes[5] + "\",\"billToTaxId\": \"" + BillToTaxAddressAttributes[0] + "\",\"messageId\": \"" + MessageAttributes[0] + "\",\"billToAddressCountryCode\": \"" + BillToAddressAttributes[7] + "\",\"billToTaxAddressState\": \"" + BillToTaxAddressAttributes[5] + "\",\"billToTaxAddressCountryCode\": \"" + BillToTaxAddressAttributes[7] + "\",\"messageRecipientPartyId\": \"" + MessageAttributes[2] + "\",\"shipToAddressCountryCode\": \"" + ShipToAddressAttributes[7] + "\",\"orderBuyerPartyId\": \"" + OrderAttributes[0] + "\",\"billToTaxAddressLine1\": \"" + BillToTaxAddressAttributes[2] + "\",\"shipToDeliverLatestAt\": \"" + ShipToAddressAttributes[8] + "\",\"billToTaxAddressLine2\": \"" + BillToTaxAddressAttributes[3] + "\",\"billToAddressCity\": \"" + BillToAddressAttributes[4] + "\",\"billToTaxAddressName\": \"" + BillToTaxAddressAttributes[1] + "\",\"billToTaxAddressPostCode\": \"" + BillToTaxAddressAttributes[6] + "\",\"billToTaxAddressCountryCode\": \"" + BillToTaxAddressAttributes[7] + "\"},\"items\": [" + ItemDetails + "]}";

        
        // httppost.setEntity(new StringEntity(json, "UTF-8"));
        
        System.out.println(json);
        
        httppost.setEntity(new StringEntity(json));
		
       // httppost.setHeader("apikey", "l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httppost.setHeader("Accept", "application/json");
		httppost.setHeader("Content-type", "application/json");
		
		utilityFileWriteOP.writeToLog(tcid, "Sending the JSON ", "Body :- "+json,ResultPath,xwfprun,"");
		response = httpclient.execute(target, httppost);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code is ", "Displayed as: "+response.getStatusLine().getStatusCode(),ResultPath,xwfprun,"");

        System.out.println(EntityUtils.toString(response.getEntity()));
        utilityFileWriteOP.writeToLog(tcid, "Response Body is ", "Body :- "+EntityUtils.toString(response.getEntity()),ResultPath,xwfprun,"");
    
        //response.toString();
		if(response.getStatusLine().getStatusCode() == 201){
			logger.log(LogStatus.PASS, "Response code for Order is : "+response.getStatusLine().getStatusCode());
			res = true;
		}
			
		else{
			logger.log(LogStatus.FAIL, "Response code for Order is : "+response.getStatusLine().getStatusCode());
			res=false;
		}
			
		
        
	} catch (Exception e) {
		// TODO: handle exception
		System.out.println(e);
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during POST call ", "Due to :"+e);
        
		res = false;
		return res;
		
	}
    
    finally{
        
        response.close();
}
    
    return res;
	
	
}


public static boolean GetOrderStatusValidation(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String tcid,String ResultPath, XWPFRun xwpfrun,ExtentTest logger) throws IOException {
	
	boolean res = false;
	
	
	String [] shipStatusArrey=statusCurrentValue.split(",");
	
	statusCurrentValue=shipStatusArrey[0];

	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "?" + ApiKey);
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        
        
        for (int j=0;j<120;j++){
        
        
       Thread.sleep(5000);
        	
        	
        System.out.println("Order ID is " + OrderID);
        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");

        response = httpclient.execute(target, httpget);

        
        
      //  System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        
        
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");
        
        String jsonresponse = EntityUtils.toString(response.getEntity());
        
        
        
        
      //  System.out.println(jsonresponse);
        
        
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath,xwpfrun,"");
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        JsonArray jarray = jsonobject.getAsJsonArray("orders");
        
        JsonObject jsonobject1=jarray.get(0).getAsJsonObject();
       
        
     //   JsonArray jarray = jsonobject.getAsJsonArray("items");
      //  System.out.println(jarray.size());
     //   for(int i=0; i<jarray.size(); i++) {
        
      //  System.out.println(jsonobject.toString());
        
        
        //utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
        
       // JsonArray jarray = jsonobject.getAsJsonArray("orders");
        
        
    //  System.out.println(jarray);
       
    
	        
        	
        //String statusValidationResult = jsonobject.get("statusValidation").toString();
	        
	        String statusCurrent = jsonobject1.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
	        
	        //System.out.println("StatusValidation Result is " + statusValidationResult);

	        if(statusCurrent.equals(statusCurrentValue)) {
	        	res = true;
				utilityFileWriteOP.writeToLog(tcid, "Orders StatusCurrent is "+statusCurrent, "Success",ResultPath,xwpfrun,"");
				
				
				System.out.println("Orders statusCurrent is " +statusCurrent);
				
			     break;
			
	        }
	        
	        else{
	        	res = false;
				utilityFileWriteOP.writeToLog(tcid, "Orders StatusCurrent is "+statusCurrent,"FAIL",ResultPath,xwpfrun,"");
				System.out.println("statusCurrent is " +statusCurrent);
				
				continue;
	        	
	        }
        	
       
        }
        
      
    	
	}  //end of try
    
    
    
    catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
        
		res = false;
		
		
		//return res;
	}
    
    finally{
        
     response.close();
     
     return res;
        
}
    
    
    
	
}




public static boolean GetOtherValuesValidation(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String InvoiceId,String totalAmt, String taxAmt,String tcid,String ResultPath, XWPFRun xwpfrun,ExtentTest logger) throws IOException {
	
	boolean res = false;
	
	
	String [] shipStatusArrey=statusCurrentValue.split(",");
	
	statusCurrentValue=shipStatusArrey[0];

	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "?" + ApiKey);
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        
        
        for (int j=0;j<120;j++){
        
        
       Thread.sleep(5000);
        	
        	
        System.out.println("Order ID is " + OrderID);
        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");

        response = httpclient.execute(target, httpget);

        
        
      //  System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        
        
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");
        
        String jsonresponse = EntityUtils.toString(response.getEntity());
        
        
        
        
      //  System.out.println(jsonresponse);
        
        
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath,xwpfrun,"");
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        JsonArray jarray = jsonobject.getAsJsonArray("orders");
        
        JsonObject jsonobject1=jarray.get(0).getAsJsonObject();
       
        //String statusValidationResult = jsonobject.get("statusValidation").toString();
	        
	        String statusCurrent = jsonobject1.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
	        
	        String billToId = jsonobject1.get("billToId").toString().replaceAll("^\"|\"$", "");
	        
	        String billToTotalNetAmount = jsonobject1.get("billToTotalNetAmount").toString().replaceAll("^\"|\"$", "");
	        
	        String billToTotalTaxAmount = jsonobject1.get("billToTotalTaxAmount").toString().replaceAll("^\"|\"$", "");
	        
	        System.out.println("Outside loop");
	        
	        System.out.println("Orders statusCurrent is " +statusCurrent);
			System.out.println("Bil to Invoice ID is : " +billToId);
			System.out.println("Bill to Total Amount is " +billToTotalNetAmount);
			System.out.println("billToTotalTaxAmount:  " +billToTotalTaxAmount);
			
			
			System.out.println("Orders statusCurrent is " +statusCurrentValue);
			System.out.println("Bil to Invoice ID is : " +InvoiceId);
			System.out.println("Bill to Total Amount is " +totalAmt);
			System.out.println("billToTotalTaxAmount:  " +taxAmt);
			
	        
	        //System.out.println("StatusValidation Result is " + statusValidationResult);

	        if(statusCurrent.equals(statusCurrentValue)  && (InvoiceId.contains(billToId) && totalAmt.contains(billToTotalNetAmount) && taxAmt.contains(billToTotalTaxAmount))) {
	        	res = true;
				utilityFileWriteOP.writeToLog(tcid, "Orders StatusCurrent is "+statusCurrent, "Success",ResultPath,xwpfrun,"");
				
			
				System.out.println("Orders statusCurrent is " +statusCurrent);
				System.out.println("Bil to Invoice ID is : " +billToId);
				System.out.println("Bill to Toral Amount is " +billToTotalNetAmount);
				System.out.println("billToTotalTaxAmount:  " +billToTotalTaxAmount);
				
			     break;
			
	        }
	        
	        else{
	        	res = false;
				utilityFileWriteOP.writeToLog(tcid, "Orders StatusCurrent is "+statusCurrent,"FAIL",ResultPath,xwpfrun,"");
				System.out.println("statusCurrent is " +statusCurrent);
				
	        	
	        }
        	
       
        }
        
      
    	
	}  //end of try
    
    
    
    catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
        
		res = false;
		
		
		//return res;
	}
    
    finally{
        
     response.close();
     
     return res;
        
}
    
    
    
	
}



public static boolean GetCallItemStatusValidation(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String tcid,String ResultPath,XWPFRun xwpfrun,ExtentTest logger) throws IOException {
	
	boolean res = false;
	
	int MaxwaitingTime = 60000;
	
	int regularwaitTime = 10000;
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Order ID is " + OrderID);
        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");
        
        for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
        	
        	Thread.sleep(regularwaitTime);
        
        response = httpclient.execute(target, httpget);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");
        
        String jsonresponse = EntityUtils.toString(response.getEntity());
        
        
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath,xwpfrun,"");
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        
        int stCnt=jarray.size();
        
        int mtchCnt=0;
        
        System.out.println(jarray.size());
        for(int i=0; i<jarray.size(); i++) {
        	
        	jsonobject = jarray.get(i).getAsJsonObject();
	        String statusValidationResult = jsonobject.get("statusValidation").toString();
	        
	        String statusCurrent = jsonobject.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
	        
	        System.out.println("StatusValidation Result is " + statusValidationResult);
	        
	        
	        
	        utilityFileWriteOP.writeToLog(tcid, "StatusValidation String is ",statusValidationResult,ResultPath,xwpfrun,"");
	        
	        
	        
	        int countOne = statusValidationResult.split("1").length - 1;
	        
	        if(countOne == 10) {
	        	res = true;
	        	System.out.println("Item Status validation Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is successful ", "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				System.out.println("Count of 1 is 10");
	       
	        
	        }
	        else {
	        	
	        	res = false;
				
	        	
	        	utilityFileWriteOP.writeToLog(tcid, "Item StatusValiadtion Failed ", "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));

	        	//break;
	        }
	        
	        if(statusCurrent.equals(statusCurrentValue)) {
	        	res = true;
				utilityFileWriteOP.writeToLog(tcid, "Item StatusCurrent is "+statusCurrent, "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath,xwpfrun,"");
				System.out.println("Item statusCurrent is " +statusCurrent);
				
				mtchCnt=mtchCnt+1;
				
				
			//break;
	        }
	        
	        else{
	        	res = false;
				utilityFileWriteOP.writeToLog(tcid, "Item StatusCurrent is "+statusCurrent, "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath,xwpfrun,"");
				
				
				System.out.println("Item statusCurrent is " +statusCurrent);
	        	
	        }
        	
        }
        	  
       
        if(mtchCnt==stCnt) {
        	break;
        
        }
        
      }
    	
} 
    
    
catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
        
		res = false;
		
		
	}
    
    finally{
        
        response.close();
        
        return res;
        
    }

}

public static boolean ShipCallOrderShipment_New(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String MessageType, String ShipToLocationId, String ShipToDeliverAt, String AllItemIDs, String AllQuantitiesOrdered, String tcid,String ResultPath,XWPFRun xwfprun,ExtentTest logger) throws IOException {
	
	boolean res = false;
	
	String ItemID[] = AllItemIDs.split(",");
	String QuantityOrdered[] = AllQuantitiesOrdered.split(",");
	
	String ItemDetails = "";
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpPost httppostship = new HttpPost("/wholesale/v1/customers/mccolls/shipments" + "/" + OrderID + "?" + ApiKey);
        
        httppostship.setConfig(config);
        
        httppostship.addHeader(AuthorizationKey, AuthorizationValue);
        
        
        System.out.println("Item length is "+ItemID.length);
        
        /*
        
{
   "id": "2019280022110320",
   "params": {
       "source": "dhl"
   },
   "items": [
       {
           "itemId": "104017384",
           "itemLineId": "1",
           "itemBaseType": "skuPIN",
           "quantityShipped": 10,
           "quantityType": "CS",
           "containerId": "39906351",
           "despatchAt": "2019-02-22T10:51:00Z",
           "quantityOperator": "add"
       }  
        

        
        */
        //*****To be updated******
        System.out.println("Executing request " + httppostship.getRequestLine() + " to " + target + " via " + proxy);
        
        for(int i = 1; i<= ItemID.length; i++) {
        	
        	ItemDetails = ItemDetails + "{\"itemId\": \"" + ItemID[i-1] + "\",\"itemLineId\":\""+i+"\",\"itemBaseType\":\"skuPIN\",\"quantityShipped\": " + QuantityOrdered[i-1] + ",\"quantityType\":\"CS\",\"containerId\": \"39906351\",\"despatchAt\": \"2019-02-22T10:51:00Z\",\"quantityOperator\": \"add\"}";

        	if(i!=ItemID.length)
        		ItemDetails = ItemDetails + ",";
        }
        
//        String json = "{\"id\": \""+OrderID+"\",\"params\": {\"label\": {\"print\": true,\"copies\": 1,\"device\": \"prnt-138493\"}},\"items\": [" + ItemDetails + "]}";
      
      String json = "{\"id\": \""+OrderID+"\",\"params\": {\"source\": \"dhl\"},\"items\": [" + ItemDetails + "]}";

        System.out.println(json);		  
        httppostship.setEntity(new StringEntity(json));
        
        httppostship.setHeader("Accept", "application/json");
		httppostship.setHeader("Content-type", "application/json");
		
		utilityFileWriteOP.writeToLog(tcid, "Sending the JSON ", "Body :- "+json,ResultPath,xwfprun,"");
		response = httpclient.execute(target, httppostship);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code is ", "Displayed as: "+response.getStatusLine().getStatusCode(),ResultPath,xwfprun,"");

        String responsebody = EntityUtils.toString(response.getEntity());
        System.out.println(responsebody);
        
        utilityFileWriteOP.writeToLog(tcid, "Response Body is ", "Body :- "+responsebody,ResultPath,xwfprun,"");
        
        if(response.getStatusLine().getStatusCode() == 202)
			res = true;
		else
			res=false;
        
	} catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
		System.out.println(e);
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during POST call ", "Due to :"+e);
        
		res = false;
		return res;
	}
    finally{
    	response.close();
    }
    
    return res;
}


public static boolean ShipCallOrderShipment_New(String customer,String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String MessageType, String ShipToLocationId, String ShipToDeliverAt, String AllItemIDs, String AllQuantitiesOrdered, String tcid,String ResultPath,XWPFRun xwfprun,ExtentTest logger) throws IOException {
	
	boolean res = false;
	
	String ItemID[] = AllItemIDs.split(",");
	String QuantityOrdered[] = AllQuantitiesOrdered.split(",");
	
	String ItemDetails = "";
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
//        HttpPost httppostship = new HttpPost("/wholesale/v1/customers/mccolls/shipments" + "/" + OrderID + "?" + ApiKey);
        
        HttpPost httppostship = new HttpPost("/wholesale/v1/customers/"+customer+"/shipments" + "/" + OrderID + "?" + ApiKey);
        
        httppostship.setConfig(config);
        
        httppostship.addHeader(AuthorizationKey, AuthorizationValue);
        
        
        System.out.println("Item length is "+ItemID.length);
        
        /*
        
{
   "id": "2019280022110320",
   "params": {
       "source": "dhl"
   },
   "items": [
       {
           "itemId": "104017384",
           "itemLineId": "1",
           "itemBaseType": "skuPIN",
           "quantityShipped": 10,
           "quantityType": "CS",
           "containerId": "39906351",
           "despatchAt": "2019-02-22T10:51:00Z",
           "quantityOperator": "add"
       }  
        

        
        */
        //*****To be updated******
        System.out.println("Executing request " + httppostship.getRequestLine() + " to " + target + " via " + proxy);
        
        for(int i = 1; i<= ItemID.length; i++) {
        	
        	ItemDetails = ItemDetails + "{\"itemId\": \"" + ItemID[i-1] + "\",\"itemLineId\":\""+i+"\",\"itemBaseType\":\"skuPIN\",\"quantityShipped\": " + QuantityOrdered[i-1] + ",\"quantityType\":\"CS\",\"containerId\": \"39906351\",\"despatchAt\": \"2019-02-22T10:51:00Z\",\"quantityOperator\": \"add\"}";

        	if(i!=ItemID.length)
        		ItemDetails = ItemDetails + ",";
        }
        
//        String json = "{\"id\": \""+OrderID+"\",\"params\": {\"label\": {\"print\": true,\"copies\": 1,\"device\": \"prnt-138493\"}},\"items\": [" + ItemDetails + "]}";
      
      String json = "{\"id\": \""+OrderID+"\",\"params\": {\"source\": \"dhl\"},\"items\": [" + ItemDetails + "]}";

        System.out.println(json);		  
        httppostship.setEntity(new StringEntity(json));
        
        httppostship.setHeader("Accept", "application/json");
		httppostship.setHeader("Content-type", "application/json");
		
		utilityFileWriteOP.writeToLog(tcid, "Sending the JSON ", "Body :- "+json,ResultPath,xwfprun,"");
		response = httpclient.execute(target, httppostship);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code is ", "Displayed as: "+response.getStatusLine().getStatusCode(),ResultPath,xwfprun,"");

        String responsebody = EntityUtils.toString(response.getEntity());
        System.out.println(responsebody);
        
        utilityFileWriteOP.writeToLog(tcid, "Response Body is ", "Body :- "+responsebody,ResultPath,xwfprun,"");
        
        if(response.getStatusLine().getStatusCode() == 202)
			res = true;
		else
			res=false;
        
	} catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
		System.out.println(e);
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during POST call ", "Due to :"+e);
        
		res = false;
		return res;
	}
    finally{
    	response.close();
    }
    
    return res;
}


public static boolean ShipCallOrderShipment_NewClosure(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String MessageType, String ShipToLocationId, String ShipToDeliverAt, String AllItemIDs, String AllQuantitiesOrdered, String tcid,String ResultPath,XWPFRun xwfprun,ExtentTest logger) throws IOException {
	
	boolean res = false;
	
	String ItemID[] = AllItemIDs.split(",");
	String QuantityOrdered[] = AllQuantitiesOrdered.split(",");
	
	String ItemDetails = "";
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        
        HttpPost httppostship = new HttpPost("/wholesale/v1/customers/mccolls/shipments" + "/" + OrderID + "?" + ApiKey);
        
        httppostship.setConfig(config);
        
        httppostship.addHeader(AuthorizationKey, AuthorizationValue);
        
        
        System.out.println("Item length is "+ItemID.length);
        
        /*
        
{
  "id": "127014412222222110",
  "params": {
    "source": "dhl"
  },
  "items": [
    {
      "itemId": "COMPLETE",
      "itemLineId": "999",
      "itemBaseType": "skuPIN",
      "quantityShipped": 999,
      "quantityType": "CS",
      "containerId": "CONTAINER",
      "quantityOperator": "add"
    }
    
  ]
} 
        */
        //*****To be updated******
        System.out.println("Executing request " + httppostship.getRequestLine() + " to " + target + " via " + proxy);
        logger.log(LogStatus.INFO, "Executing request " + httppostship.getRequestLine() + " to " + target + " via " + proxy);
        
        for(int i = 1; i<= ItemID.length; i++) {
        	
        	        	
        	ItemDetails = ItemDetails + "{\"itemId\": \"COMPLETE\",\"itemLineId\":\"999\",\"itemBaseType\":\"skuPIN\",\"quantityShipped\": \"999\",\"quantityType\":\"CS\",\"containerId\": \"CONTAINER\",\"quantityOperator\": \"add\"}";

        	if(i!=ItemID.length)
        		ItemDetails = ItemDetails + ",";
        }
        
//        String json = "{\"id\": \""+OrderID+"\",\"params\": {\"label\": {\"print\": true,\"copies\": 1,\"device\": \"prnt-138493\"}},\"items\": [" + ItemDetails + "]}";
      
        String OrderID1 =OrderID+1;
        
        System.out.println("Updated order id while closure"+OrderID1);
        
      String json = "{\"id\": \""+OrderID1+"\",\"params\": {\"source\": \"dhl\"},\"items\": [" + ItemDetails + "]}";
        
        System.out.println(json);		  
        httppostship.setEntity(new StringEntity(json));
        
        httppostship.setHeader("Accept", "application/json");
		httppostship.setHeader("Content-type", "application/json");
		
		utilityFileWriteOP.writeToLog(tcid, "Sending the JSON ", "Body :- "+json,ResultPath,xwfprun,"");
		response = httpclient.execute(target, httppostship);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        logger.log(LogStatus.INFO, "Response code displayed as: "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code is ", "Displayed as: "+response.getStatusLine().getStatusCode(),ResultPath,xwfprun,"");

        String responsebody = EntityUtils.toString(response.getEntity());
        System.out.println(responsebody);
        
        utilityFileWriteOP.writeToLog(tcid, "Response Body is ", "Body :- "+responsebody,ResultPath,xwfprun,"");
        
        if(response.getStatusLine().getStatusCode() == 202)
			res = true;
		else
			res=false;
        
	} catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
		System.out.println(e);
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during POST call ", "Due to :"+e);
        
		res = false;
		return res;
	}
    finally{
    	response.close();
    }
    
    return res;
}



public static boolean ShipCallOrderShipment_NewClosure(String customer,String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String MessageType, String ShipToLocationId, String ShipToDeliverAt, String AllItemIDs, String AllQuantitiesOrdered, String tcid,String ResultPath,XWPFRun xwfprun,ExtentTest logger) throws IOException {
	
	boolean res = false;
	
	String ItemID[] = AllItemIDs.split(",");
	String QuantityOrdered[] = AllQuantitiesOrdered.split(",");
	
	String ItemDetails = "";
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        
        HttpPost httppostship = new HttpPost("/wholesale/v1/customers/"+customer+"/shipments" + "/" + OrderID + "?" + ApiKey);
        
        httppostship.setConfig(config);
        
        httppostship.addHeader(AuthorizationKey, AuthorizationValue);
        
        
        System.out.println("Item length is "+ItemID.length);
        
        /*
        
{
  "id": "127014412222222110",
  "params": {
    "source": "dhl"
  },
  "items": [
    {
      "itemId": "COMPLETE",
      "itemLineId": "999",
      "itemBaseType": "skuPIN",
      "quantityShipped": 999,
      "quantityType": "CS",
      "containerId": "CONTAINER",
      "quantityOperator": "add"
    }
    
  ]
} 
        */
        //*****To be updated******
        System.out.println("Executing request " + httppostship.getRequestLine() + " to " + target + " via " + proxy);
        logger.log(LogStatus.INFO, "Executing request " + httppostship.getRequestLine() + " to " + target + " via " + proxy);
        
        for(int i = 1; i<= ItemID.length; i++) {
        	
        	        	
        	ItemDetails = ItemDetails + "{\"itemId\": \"COMPLETE\",\"itemLineId\":\"999\",\"itemBaseType\":\"skuPIN\",\"quantityShipped\": \"999\",\"quantityType\":\"CS\",\"containerId\": \"CONTAINER\",\"quantityOperator\": \"add\"}";

        	if(i!=ItemID.length)
        		ItemDetails = ItemDetails + ",";
        }
        
//        String json = "{\"id\": \""+OrderID+"\",\"params\": {\"label\": {\"print\": true,\"copies\": 1,\"device\": \"prnt-138493\"}},\"items\": [" + ItemDetails + "]}";
      
        String OrderID1 =OrderID+1;
        
        System.out.println("Updated order id while closure"+OrderID1);
        
      String json = "{\"id\": \""+OrderID1+"\",\"params\": {\"source\": \"dhl\"},\"items\": [" + ItemDetails + "]}";
        
        System.out.println(json);		  
        httppostship.setEntity(new StringEntity(json));
        
        httppostship.setHeader("Accept", "application/json");
		httppostship.setHeader("Content-type", "application/json");
		
		utilityFileWriteOP.writeToLog(tcid, "Sending the JSON ", "Body :- "+json,ResultPath,xwfprun,"");
		response = httpclient.execute(target, httppostship);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        logger.log(LogStatus.INFO, "Response code displayed as: "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code is ", "Displayed as: "+response.getStatusLine().getStatusCode(),ResultPath,xwfprun,"");

        String responsebody = EntityUtils.toString(response.getEntity());
        System.out.println(responsebody);
        
        utilityFileWriteOP.writeToLog(tcid, "Response Body is ", "Body :- "+responsebody,ResultPath,xwfprun,"");
        
        if(response.getStatusLine().getStatusCode() == 202)
			res = true;
		else
			res=false;
        
	} catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
		System.out.println(e);
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during POST call ", "Due to :"+e);
        
		res = false;
		return res;
	}
    finally{
    	response.close();
    }
    
    return res;
}


public static boolean GetCallItemStatusValidation(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String tcid,XWPFRun xwpfRun, ExtentTest logger) throws IOException {
	
	boolean res = false;
	
	int MaxwaitingTime = 60000;
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Order ID is " + OrderID);
        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        logger.log(LogStatus.INFO, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done");
        
       	
        	Thread.sleep(5000);
        	
        	
        	for (int i=0;i<10;i++){
        
        response = httpclient.execute(target, httpget);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode());
        logger.log(LogStatus.INFO, "Response code for item attributes is displayed as: "+response.getStatusLine().getStatusCode());
        
        String jsonresponse = EntityUtils.toString(response.getEntity());
        
        
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse);
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        System.out.println(jarray.size());
        
        
        int ItemCount=jarray.size();
        
        int resCount=0;
        
        for(int k=0; k<jarray.size(); k++) {
        	
        	jsonobject = jarray.get(k).getAsJsonObject();
	       
        	String statusValidationResult = jsonobject.get("statusValidation").toString();
	        
	        String statusCurrent = jsonobject.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
	        
	        System.out.println("StatusValidation Result is " + statusValidationResult);
	        
	        int countOne = statusValidationResult.split("1").length - 1;
	        
	        if(countOne == 7) {
	        	res = true;
	        	System.out.println("Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is successful ", "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				System.out.println("Count of 1 is 7");
	        }
	        else {
	        	res = false;
				utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion Failed ", "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));

	        	//break;
	        }
	        
	        if(statusCurrent.equals(statusCurrentValue)) {
	        	res = true;
				utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrentValue, "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				System.out.println("statusCurrent is " +statusCurrentValue);
				
				resCount=resCount+1;
				
	        }
	        
	        else{
	        	res = false;
				utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrentValue, "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				System.out.println("statusCurrent is " +statusCurrentValue);
	        	//break;
	        }
        }
        	  
        if(ItemCount==resCount) {
        	
        	res=true;
        	break;
        }
        
      }
    	
	} catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
        
		res = false;
		
	}
    finally{
        
        response.close();
        return res;
        
}
}




public static String checkTotalAmountInPending(String SFTPHostName, int SFTPPort, String SFTPUserName, String SFTPPassword, String SFTPPath, String LocalPath, String OrderID, String tcid, String ResultPath) throws IOException {
	
	JSch jsch = new JSch();
	Session session = null;
	BufferedReader br = null;
	
	double sumOfAmount = 0;
	
	String customerId = null;
	String orderId = null;
	String itemId = null;
	String itemLineId = null;
	String itemDescription = null;
	String quantityType = null;
	String quantityShipped = null;
	String priceConfirmedCurrency = null;
	String priceConfirmedAmount  = null;
	String priceConfirmedTaxRate  = null;
	String shipToLocationRef  = null;
	String invoiceId = null;
	String billToId_invoicenumber  = null;
	String billToNetAmount = null;
	String billToTaxAmount = null;
	String billToAmount = null;

	
	
	try {
		
		session = jsch.getSession(SFTPUserName, SFTPHostName, SFTPPort);
		session.setConfig("StrictHostKeyChecking", "no");
		session.setPassword(SFTPPassword);
		session.connect();
		
		Channel channel = session.openChannel("sftp");
		channel.connect();
		
		ChannelSftp sftpchannel = (ChannelSftp) channel;
		
	/*	sftpchannel.cd("cd..;");
		sftpchannel.cd("cd..;");*/
		
		
		sftpchannel.cd(SFTPPath);
		
		System.out.println("The Current path is " + sftpchannel.pwd());
		
		Vector<ChannelSftp.LsEntry> list = sftpchannel.ls("*.csv");
		
		String reqfilename="w-mcc-"+OrderID;
		
		//String AllpriceConfirmedDetails[] = PriceConfirmedDetails.split(",");
		
		for(ChannelSftp.LsEntry entry: list) {
			
			
			String filename = entry.getFilename();
			
			System.out.println("The current file is " + filename);
			
			if(filename.contains(reqfilename))
			{
				
				System.out.println("To be used file is "+filename);
				System.out.println(LocalPath+filename);
				sftpchannel.get(filename, LocalPath + filename);//Copy file from WinSCP to local

					//Read from local
					br = new BufferedReader(new FileReader(LocalPath + filename));
					//System.out.println(lastmodifiedfilename +" "+ lastmodifiedfiletime);
					
					String CurrentLine = null;
					int linecount = 0;
					
					while((CurrentLine = br.readLine()) != null) {
					
						
						//Split the Current line in comma seperated values
						String[] strarr = CurrentLine.split(",");
						
						//extracting details from file
						customerId = strarr[0];
						orderId = strarr[1];
						itemId = strarr[2];
						itemLineId = strarr[3];
						itemDescription = strarr[4];
						quantityType = strarr[5];
						quantityShipped = strarr[6];
						priceConfirmedCurrency = strarr[7];
						priceConfirmedAmount = strarr[8];
						priceConfirmedTaxRate = strarr[9];
						shipToLocationRef = strarr[10];
						billToId_invoicenumber = strarr[11];
						
						//invoiceId = strarr[12];
						
						billToNetAmount = strarr[12];
						billToTaxAmount = strarr[13];
						billToAmount = strarr[14];
						
						double TotalPriceEachItem = Double.parseDouble(billToNetAmount);
						
						//System.out.println("billToNetAmount is "+billToNetAmount);
						
						System.out.println("sum for item "+linecount+" is "+billToNetAmount);
						sumOfAmount = sumOfAmount + TotalPriceEachItem;

						linecount++;
					
					}
					
				
				br.close();
				//FileUtils.cleanDirectory(new File(LocalPath)); // Delete all downloaded files from local directory
				
				
			
				utilityFileWriteOP.writeToLog(tcid, "The csv file "+reqfilename, "Exists in wholesale_ebs/in/pending folder", ResultPath);
				
				break;
			}
			
			
				
			}
			
		sftpchannel.exit();
		
		session.disconnect();
			
		
		
	} catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
		System.out.println(e);
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during Raised file check in wholesale_ebs/in/pending folder ", "Due to :"+e, ResultPath);
		//String.valueOf(sumOfAmount) = null;
		return null;
	}
	
	
	

	finally{
		System.out.println("sending back "+ String.valueOf(sumOfAmount));
	
	}
	
	return String.valueOf(sumOfAmount);
}



public static String checkTotalVATAmountInPending(String SFTPHostName, int SFTPPort, String SFTPUserName, String SFTPPassword, String SFTPPath, String LocalPath, String OrderID, String tcid, String ResultPath) throws IOException {
	
	JSch jsch = new JSch();
	Session session = null;
	BufferedReader br = null;
	
	double sumOfVATAmount = 0;
	
	String customerId = null;
	String orderId = null;
	String itemId = null;
	String itemLineId = null;
	String itemDescription = null;
	String quantityType = null;
	String quantityShipped = null;
	String priceConfirmedCurrency = null;
	String priceConfirmedAmount  = null;
	String priceConfirmedTaxRate  = null;
	String shipToLocationRef  = null;
	String invoiceId = null;
	String billToId_invoicenumber  = null;
	String billToNetAmount = null;
	String billToTaxAmount = null;
	String billToAmount = null;

	
	
	try {
		
		session = jsch.getSession(SFTPUserName, SFTPHostName, SFTPPort);
		session.setConfig("StrictHostKeyChecking", "no");
		session.setPassword(SFTPPassword);
		session.connect();
		
		Channel channel = session.openChannel("sftp");
		channel.connect();
		
		ChannelSftp sftpchannel = (ChannelSftp) channel;
		
	/*	sftpchannel.cd("cd..;");
		sftpchannel.cd("cd..;");*/
		
		
		sftpchannel.cd(SFTPPath);
		
		System.out.println("The Current path is " + sftpchannel.pwd());
		
		Vector<ChannelSftp.LsEntry> list = sftpchannel.ls("*.csv");
		
		String reqfilename="w-mcc-"+OrderID;
		
		//String AllpriceConfirmedDetails[] = PriceConfirmedDetails.split(",");
		
		for(ChannelSftp.LsEntry entry: list) {
			
			
			String filename = entry.getFilename();
			
			System.out.println("The current file is " + filename);
			
			if(filename.contains(reqfilename))
			{
				
				System.out.println("To be used file is "+filename);
				System.out.println(LocalPath+filename);
				sftpchannel.get(filename, LocalPath + filename);//Copy file from WinSCP to local

					//Read from local
					br = new BufferedReader(new FileReader(LocalPath + filename));
					//System.out.println(lastmodifiedfilename +" "+ lastmodifiedfiletime);
					
					String CurrentLine = null;
					int linecount = 0;
					
					while((CurrentLine = br.readLine()) != null) {
					
						
						//Split the Current line in comma seperated values
						String[] strarr = CurrentLine.split(",");
						
						//extracting details from file
						customerId = strarr[0];
						orderId = strarr[1];
						itemId = strarr[2];
						itemLineId = strarr[3];
						itemDescription = strarr[4];
						quantityType = strarr[5];
						quantityShipped = strarr[6];
						priceConfirmedCurrency = strarr[7];
						priceConfirmedAmount = strarr[8];
						priceConfirmedTaxRate = strarr[9];
						shipToLocationRef = strarr[10];
						billToId_invoicenumber = strarr[11];
						
						//invoiceId = strarr[12];
						
						billToNetAmount = strarr[12];
						billToTaxAmount = strarr[13];
						billToAmount = strarr[14];
					/*	
						double TotalPriceEachItem = Double.parseDouble(billToNetAmount);
						
						//System.out.println("billToNetAmount is "+billToNetAmount);
						
						System.out.println("sum for item "+linecount+" is "+billToNetAmount);
						sumOfAmount = sumOfAmount + TotalPriceEachItem;*/
						
						double TotalVATEachItem = Double.parseDouble(billToTaxAmount);
						
						//System.out.println("billToNetAmount is "+billToNetAmount);
						
						System.out.println("sum for item "+linecount+" is "+billToTaxAmount);
						sumOfVATAmount = sumOfVATAmount + TotalVATEachItem;

						linecount++;
					
					}
					
				
				br.close();
				//FileUtils.cleanDirectory(new File(LocalPath)); // Delete all downloaded files from local directory
				
				
			
				utilityFileWriteOP.writeToLog(tcid, "The csv file "+reqfilename, "Exists in wholesale_ebs/in/pending folder", ResultPath);
				
				break;
			}
			
			
				
			}
			
		sftpchannel.exit();
		
		session.disconnect();
			
		
		
	} catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
		System.out.println(e);
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during Raised file check in wholesale_ebs/in/pending folder ", "Due to :"+e, ResultPath);
		//String.valueOf(sumOfAmount) = null;
		return null;
	}
	
	
	

	finally{
		System.out.println("sending back "+ String.valueOf(sumOfVATAmount));
	
	}
	
	return String.valueOf(sumOfVATAmount);
}


public static String GetCallItemDetailsFromWebPortal(String CatalogueURL,String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String ItemId, String itemBaseType, String quantityType, String itemLineId, String quantityOrdered, String tcid, String Resultpath,XWPFRun xwpfRun, ExtentTest logger) throws IOException {
	
	String res = null;
	
	int MaxwaitingTime = 60000;
	
	int regularWaitingTime = 10000;
	
	HashMap<String, String> mapofMaps = new HashMap<String, String>();
	HashMap<String, String> mapofPrices = new HashMap<String, String>();

	String priceOrderedCurrency = null;
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        //https://sit-api.morrisons.com/wholesaleorder/v1/customers/mccolls/catalogues/main/items/101743031?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36&filtertype=maptype&filtervalue=PIN
        
        HttpGet httpget = new HttpGet(CatalogueURL+ ItemId + "?" + ApiKey+"&filtertype=maptype&filtervalue=PIN");

        
//        HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/sandpiper/catalogues/main/items/" + ItemId + "?" + ApiKey+"&filtertype=maptype&filtervalue=PIN");
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Item ID is " + ItemId);
        utilityFileWriteOP.writeToLog(tcid, "Searching the attributes for ", "Item ID "+ItemId,Resultpath);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",Resultpath);
        
     /*   for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
        	
        	Thread.sleep(regularWaitingTime);*/
        
        response = httpclient.execute(target, httpget);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),Resultpath);
        
        String jsonresponse = EntityUtils.toString(response.getEntity());
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,Resultpath);
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString(),Resultpath);
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        System.out.println(jarray.size());
        for(int i=0; i<jarray.size(); i++) {
        	
        	jsonobject = jarray.get(i).getAsJsonObject();
        	
        	String description = jsonobject.get("description").toString().replaceAll("^\"|\"$", "");
        	
        	//String mapsJsonBody = jsonobject.get("maps").toString().replaceAll("^\"|\"$", "");
	        JsonArray jarrayMaps = jsonobject.getAsJsonArray("maps");
	        
	        for(int mapsindex=0; mapsindex<jarrayMaps.size(); mapsindex++) {
	        	
	        	JsonObject jsonobjectMapsBody = jarrayMaps.get(mapsindex).getAsJsonObject();
	        	
	        	String mapJsonKey = jsonobjectMapsBody.get("type").toString().replaceAll("^\"|\"$", "");
	        	
		        JsonArray jarrayMapsBody = jsonobjectMapsBody.getAsJsonArray("values");
		        
		        JsonObject jsonobjectMapsBodyValue = jarrayMapsBody.get(0).getAsJsonObject();
		        
		        String mapJsonvalue = jsonobjectMapsBodyValue.get("value").toString().replaceAll("^\"|\"$", "");
		        
		        mapofMaps.put(mapJsonKey, mapJsonvalue);

	        	
	        }
	        
		    JsonArray jarrayPrices = jsonobject.getAsJsonArray("prices");

	        
	        for(int pricesindex=0; pricesindex<jarrayPrices.size(); pricesindex++) {
	        	
	        	JsonObject jsonobjectPricesBody = jarrayPrices.get(pricesindex).getAsJsonObject();
	        	
	        	String pricesJsonKey = jsonobjectPricesBody.get("type").toString().replaceAll("^\"|\"$", "");
	        	
		        JsonArray jarrayPricesBody = jsonobjectPricesBody.getAsJsonArray("values");
		        
		        JsonObject jsonobjectPricesBodyValue = jarrayPricesBody.get(0).getAsJsonObject();
		        
		        String pricesJsonvalue = jsonobjectPricesBodyValue.get("value").toString().replaceAll("^\"|\"$", "");
		        
		        mapofPrices.put(pricesJsonKey, pricesJsonvalue);
		        
		        if(pricesJsonKey.equals("WSP")) {
		        	
			        priceOrderedCurrency = jsonobjectPricesBodyValue.get("currency").toString().replaceAll("^\"|\"$", "");
			        mapofPrices.put("priceOrderedCurrency", priceOrderedCurrency);

		        	
		        }

	        	
	        }
	        
	       	//fetching  priceOrderedCurrency
	        
	        
		    
		    System.out.println(mapofMaps.size());
		    System.out.println(mapofPrices.size());
		    
		    String itemDetailsbody = "{\"quantityType\": \"" + quantityType + "\",\"itemId\": \"" + ItemId +"\",\"itemAlternateId\": {\"clientId\": \"" + mapofMaps.get("CLIENTID") +"\",\"barcodeEan\": \"" + mapofMaps.get("EAN") +"\",\"skuPin\": \"" + mapofMaps.get("PIN") +"\"},\"priceOrderedAmount\": " + mapofPrices.get("WSP") + ",\"priceOrderedCurrency\": \"" + priceOrderedCurrency + "\",\"itemBaseType\": \"" + itemBaseType + "\",\"itemLineId\": \"" + itemLineId + "\",\"quantityOrdered\": " + quantityOrdered + ",\"priceOrderedTaxRate\": " + mapofPrices.get("VAT") + ",\"itemDescription\": \"" + description + "\",\"itemCaseSize\": " + mapofPrices.get("CS").substring(0, mapofPrices.get("CS").indexOf(".")) + "}";
		    	    
		    System.out.println(itemDetailsbody);	    
		    	     
		    res = itemDetailsbody;
			
			
        	
        }
        	  
       
        
       // }
    	
	} catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e,Resultpath);
        
		res = null;
		return res;
	}
    
    finally{
        
        response.close();
        
}
    
    return res;
    
	
}



}
