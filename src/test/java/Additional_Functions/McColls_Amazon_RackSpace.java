package Additional_Functions;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import Utilities.*;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import jxl.Sheet;
import jxl.Workbook;
















import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.poi.ss.formula.functions.Index;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

public class McColls_Amazon_RackSpace {
	
	
	


	public static boolean AWSLogOut (String UserName,WebDriver driver, WebDriverWait wait, String TC_no , String Resultpath ,  String Screenshotpath,XWPFRun xwpfrun) 
	{
		boolean res = false;
		try
		{
			String MainPg_title = "Fanatical Support for AWS";
			String currentWindow = driver.getWindowHandle();  //will keep current window to switch back
			System.out.println("Current Title: "+driver.getTitle());
			for(String winHandle : driver.getWindowHandles())
			{
				if (driver.switchTo().window(winHandle).getTitle().equals(MainPg_title))  //Search for the desired Window
				{


					System.out.println("Entered to this block working correctly");
					break;

				} 
				else 
				{

					driver.switchTo().window(currentWindow);
				} 
			}



			//wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@class='rs-dropdown-title' and contains(text(),' "+ UserName + "' )]")));
			driver.findElement(By.xpath("//div[@class='phx-dropdown-title' and contains(text(),'"+UserName.toUpperCase()+"')]")).click();


			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@href='/aws/logout' and @data-pilot-tracking-id='logout']")));
			driver.findElement(By.xpath("//a[@href='/aws/logout' and @data-pilot-tracking-id='logout']")).click();
			Thread.sleep(3000);
			//Take Screenshot
			String ClickonLogout=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";   
			TakeScreenShot.saveScreenShot(ClickonLogout, driver);
			utilityFileWriteOP.writeToLog(TC_no, "Logout Button Clicked", "Done!!!",Resultpath);
			Thread.sleep(5000);

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='username']")));

			String AfterLogout_WP1=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";

			TakeScreenShot.saveScreenShot(AfterLogout_WP1, driver);

			if(driver.findElement(By.xpath("//input[@id='username']")).isDisplayed())
			{
				res=true;
			}


			if(res)
			{
				utilityFileWriteOP.writeToLog(TC_no, "Logout", "Successfull!!!",Resultpath,xwpfrun);
				System.out.println("Logout-Pass");
			}
			if(!res)
			{
				utilityFileWriteOP.writeToLog(TC_no, "Cannot Logout", "Failed to Logout!!!",Resultpath,xwpfrun);
				System.out.println("Logout-Fail");
			}


			return res;


		}

		catch (Exception e)
		{
			res=false;
			System.out.println("The error is "+e);
			utilityFileWriteOP.writeToLog(TC_no, "Problem ocuurred for"+e, "while logging out from AWS",Resultpath);
			return res;
		}

		finally
		{
			return res;
		}
	}

	/*
	 * Method Name:aws_login
	 * Functionality Description:Function To login AWS
	 * Created on:08/01/2020
	 * Created By:Morrisons
	 */
	
	public static boolean aws_login(WebDriver driver, WebDriverWait wait, String tcid, String un, String pw ,String Resultpath,XWPFRun xwpfrun, ExtentTest logger)
	{

		boolean res=false;
		try
		{	
			Resultpath=System.getProperty("user.dir")+"/"+Resultpath;
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='username']")));
			utilityFileWriteOP.writeToLog(tcid, "Login Page of AWS Rackspace", "Displayed!!!",Resultpath,xwpfrun);
			String awsDynamoDB_sc=Resultpath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	    
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc, driver);
			logger.log(LogStatus.PASS, "Login Page of AWS Rackspace is displayed");
			logger.log(LogStatus.PASS, logger.addScreenCapture(awsDynamoDB_sc));
			driver.findElement(By.xpath("//input[@id='username']")).sendKeys(un);
			utilityFileWriteOP.writeToLog(tcid, "Username", "Entered!!!",Resultpath,xwpfrun);
			logger.log(LogStatus.PASS, "Username is Entered");
			driver.findElement(By.xpath("//input[ @id='password']")).sendKeys(pw);
			utilityFileWriteOP.writeToLog(tcid, "Password", "Entered!!!",Resultpath,xwpfrun);
			String awsDynamoDB_sc1=Resultpath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc1, driver);
			logger.log(LogStatus.PASS, "Password is enetered");
			logger.log(LogStatus.PASS, logger.addScreenCapture(awsDynamoDB_sc1));
			driver.findElement(By.xpath("//a[@id='signin_button']")).click();
			utilityFileWriteOP.writeToLog(tcid, "Login Button", "Clicked!!!",Resultpath,xwpfrun);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()='AWS Management Console']")));
			String awsDynamoDB_sc2=Resultpath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc2, driver);
			
			if(driver.findElement(By.xpath("//span[text()='AWS Management Console']")).isDisplayed())
			{
				res=true;
				logger.log(LogStatus.PASS, "AWS Home Page is displayed");
				logger.log(LogStatus.PASS, logger.addScreenCapture(awsDynamoDB_sc2));
				utilityFileWriteOP.writeToLog(tcid, "Home Page", "Displayed!!!",Resultpath,xwpfrun);
				utilityFileWriteOP.writeToLog(tcid, "Home Page", "Displayed!!!",Resultpath);
			}else{
				logger.log(LogStatus.FAIL, "AWS Home Page is NOT displayed");
				logger.log(LogStatus.FAIL, logger.addScreenCapture(awsDynamoDB_sc2));
				utilityFileWriteOP.writeToLog(tcid, "Home Page", "Not Displayed!!!",Resultpath,xwpfrun);
				utilityFileWriteOP.writeToLog(tcid, "Home Page", "Not Displayed!!!",Resultpath);
				res=false;
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			String awsDynamoDB_sc=Resultpath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	   
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc, driver);
			res=false;
			logger.log(LogStatus.FAIL, e);
			logger.log(LogStatus.FAIL, logger.addScreenCapture(awsDynamoDB_sc));
			utilityFileWriteOP.writeToLog(tcid, "A problem occurred while logging in AWS Rackspace", "Due to "+e,Resultpath);
		}
		finally
		{
			return res;
		}

	
	}
	
	/*
	 * Method navigateToS3
	 * Functionality Description:Function To Navigate thorugh AWS
	 * Created on:08/01/2020
	 * Created By:Morrisons
	 */

	
	public static boolean navigateToS3(WebDriver driver, WebDriverWait wait, String tcid,String ResultPath,XWPFRun xwpfRun, ExtentTest logger)
	{
		boolean res=false;
		
		try
		{
			driver.findElement(By.xpath("//div[@id='nav-logo']")).click();
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='search-box-input']")));
			driver.findElement(By.xpath("//*[@id='search-box-input']")).sendKeys("s3");
			/*wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[contains(@id,'search-box-item-0-title') and text()='S3']")));
			driver.findElement(By.xpath("//div[contains(@id,'search-box-item-0-title') and text()='S3']")).click();*/
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(@class,'awsui-select-option-filtering-match-highlight') and text()='S3']")));
			driver.findElement(By.xpath("//span[contains(@class,'awsui-select-option-filtering-match-highlight') and text()='S3']")).click();
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@region-container='text' and text()='Create bucket']")));
			String AfterLogin11=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(AfterLogin11, driver);
			utilityFileWriteOP.writeToLog(tcid, "S3 search page", "Displayed!!!",ResultPath,xwpfRun);
			logger.log(LogStatus.PASS, "S3 search page is displayed!!!");
			logger.log(LogStatus.PASS, logger.addScreenCapture(AfterLogin11));
			if(driver.findElement(By.xpath("//span[@region-container='text' and text()='Create bucket']")).isDisplayed())
				res=true;
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
			String AfterLogin=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(AfterLogin, driver);
			res=false;
			utilityFileWriteOP.writeToLog(tcid, "A problem occurred while navigating through AWS S3", "Due to "+e,ResultPath,xwpfRun);
			logger.log(LogStatus.FAIL, "A problem occurred while navigating through AWS S3");
			logger.log(LogStatus.FAIL, logger.addScreenCapture(AfterLogin));
		}
		finally
		{
			return res;
		}
	}
	
	
	public static boolean validateFileExistenceInPending(WebDriver driver, WebDriverWait wait, String searchKey, String jobid, String tcid,String ResultPath, XWPFRun xwpfRun, ExtentTest logger)
	{
		boolean res=false;
		
		try
		{
			
			Thread.sleep(5000);	
			driver.findElement(By.xpath("//input[@placeholder='Search for buckets' and @type='text']")).sendKeys(searchKey);
			utilityFileWriteOP.writeToLog(tcid, "Search field is populated with ", "Keyword "+searchKey,ResultPath,xwpfRun,"");
			
			String awsDynamoDB_sc1=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc1, driver);
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(@class,'list-view-item-name') and text()='"+searchKey+"']")));
			utilityFileWriteOP.writeToLog(tcid, "Link related to keyword "+searchKey, "Appeared!!!",ResultPath,xwpfRun);
			logger.log(LogStatus.PASS, "Link related to keyword "+searchKey+" is displayed");
			logger.log(LogStatus.PASS, logger.addScreenCapture(awsDynamoDB_sc1));
			
			String awsDynamoDB_sc2=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc2, driver);
			
			driver.findElement(By.xpath("//a[contains(@class,'list-view-item-name') and text()='"+searchKey+"']")).click();
			utilityFileWriteOP.writeToLog(tcid, "Link related to keyword "+searchKey, "Clicked!!!",ResultPath,xwpfRun);
			logger.log(LogStatus.PASS, "Link related to keyword "+searchKey+" is Clicked");
			logger.log(LogStatus.PASS, logger.addScreenCapture(awsDynamoDB_sc2));
			
			driver.findElement(By.xpath("//input[@placeholder='Type a prefix and press Enter to search. Press ESC to clear.' and @type='text']")).clear();
			driver.findElement(By.xpath("//input[@placeholder='Type a prefix and press Enter to search. Press ESC to clear.' and @type='text']")).sendKeys(jobid);
			
			
			Robot rb = new Robot();			
			rb.keyPress(KeyEvent.VK_ENTER);
			Thread.sleep(1500);
			
			
		
			System.out.println(jobid);
			if(driver.findElement(By.xpath("//a[contains(@class,'list-view-item-name object-name') and contains(text(),'"+jobid+"') and contains(text(),'json')]")).isDisplayed())
				{
					res=true;
					utilityFileWriteOP.writeToLog(tcid, "The file "+jobid+".json", "Exists in pending folder!!!",ResultPath,xwpfRun);
					System.out.println("File found");
					logger.log(LogStatus.PASS,  "The file "+jobid+".json", "Exists in pending folder");
					
				}
		
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
			String awsDynamoDB_sc=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc, driver);
			res=false;
			logger.log(LogStatus.FAIL, "A problem occurred while downloading from AWS S3");
			logger.log(LogStatus.FAIL, logger.addScreenCapture(awsDynamoDB_sc));
			utilityFileWriteOP.writeToLog(tcid, "A problem occurred while navigating through AWS S3 bucket", "Due to "+e,ResultPath,xwpfRun);
		}
		
		
		return res;
		
	}
	
	
	public static String getLatestFileName(String filePath)
	{
		String res=null;
		try
		{
			String[] filenames=new File(filePath).list();
			
			
			
			long lastmodified = 0;

			String lastmodifiedfilename = null;

			for(int i = 0; i<filenames.length; i++) {

			File currentfile = new File(filePath + "/" + filenames[i]);

			Date currentfilelastmodifieddate = new Date(currentfile.lastModified());

			long currentfilelastmodifiedmilliseconds = currentfilelastmodifieddate.getTime();

			if(currentfilelastmodifiedmilliseconds > lastmodified) {

			lastmodified = currentfilelastmodifiedmilliseconds;

			lastmodifiedfilename = filenames[i];

			}

			}

			//System.out.println(lastmodifiedfilename);

			res= lastmodifiedfilename;
			
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			return res;
		}
	}
	
	public static boolean validateDynamoDB(WebDriver driver, WebDriverWait wait, String tcid, String searchKey, String customerName, String orderID, String validationSheetPath,String ResultPath)
	{
		boolean res=false;
		try
		{
			String awsDynamoDB_sc=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc, driver);
			
			driver.findElement(By.xpath("//input[@type='text' and @placeholder='Filter by table name']")).sendKeys(searchKey);
			Utilities.utilityFileWriteOP.writeToLog(tcid, "Dynamo DB Filter Field", "Populated with "+searchKey);
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(text(),'"+searchKey+"')]")));
			Utilities.utilityFileWriteOP.writeToLog(tcid, "Search results for "+searchKey, "Found!!!",ResultPath);
			
			String awsDynamoDB_sc1=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc1, driver);
			
			driver.findElement(By.xpath("//a[contains(text(),'wholesale.core.orderHeaders')]")).click();
			driver.findElement(By.xpath("//a[contains(text(),'wholesale.core.orderHeaders')]")).click();
			Utilities.utilityFileWriteOP.writeToLog(tcid, "Link of sit.xxwmm.wholesale.core.orderHeaders ", "Clicked!!!",ResultPath);
			
			String awsDynamoDB_sc2=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc2, driver);
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[contains(@class,'gwt-Label') and contains(text(),'wholesale.core.orderHeaders')]")));
			Utilities.utilityFileWriteOP.writeToLog(tcid, "Page of sit.xxwmm.wholesale.core.orderHeaders ", "Appeared!!!",ResultPath);
			
			String awsDynamoDB_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc3, driver);
			
			driver.findElement(By.xpath("//div[@class='gwt-Label' and text()='Items']")).click();
			Utilities.utilityFileWriteOP.writeToLog(tcid, "Item tab of Link of sit.xxwmm.wholesale.core.orderHeaders Page", "Clicked!!!",ResultPath);
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//select[contains(@class,'gwt-ListBox') and contains(@id,'scan-query-operation-type-drop-down')]")));
			Utilities.utilityFileWriteOP.writeToLog(tcid, "Item Page of sit.xxwmm.wholesale.core.orderHeaders ", "Appeared!!!",ResultPath);
			
			String awsDynamoDB_sc4=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc4, driver);
			
			driver.findElement(By.xpath("//select[contains(@class,'gwt-ListBox') and contains(@id,'scan-query-operation-type-drop-down')]")).click();
			Utilities.utilityFileWriteOP.writeToLog(tcid, "Scan/Query drop down list of Link of sit.xxwmm.wholesale.core.orderHeaders Page", "Clicked!!!",ResultPath);
			
			driver.findElement(By.xpath("//select[contains(@class,'gwt-ListBox') and contains(@id,'scan-query-operation-type-drop-down')]/option[@value='Query']")).click();
			Utilities.utilityFileWriteOP.writeToLog(tcid, "Query option in the drop down list of Link of sit.xxwmm.wholesale.core.orderHeaders Page", "Clicked!!!",ResultPath);
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[contains(@class,'gwt-TextBox') and contains(@id,'scan-query-primary-sort-key-attribute-value-text-field')]")));
			Utilities.utilityFileWriteOP.writeToLog(tcid, "Search field for query in Item Page of sit.xxwmm.wholesale.core.orderHeaders ", "Appeared!!!",ResultPath);
			
			String awsDynamoDB_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc5, driver);
			
			driver.findElement(By.xpath("//input[contains(@class,'gwt-TextBox') and contains(@id,'scan-query-primary-sort-key-attribute-value-text-field')]")).sendKeys(customerName+orderID);
			Utilities.utilityFileWriteOP.writeToLog(tcid, "Search field for query in Item Page of sit.xxwmm.wholesale.core.orderHeaders ", "Populated with "+customerName+orderID);
			
			String awsDynamoDB_sc6=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc6, driver);
			
			driver.findElement(By.xpath("//button[contains(@class,'gwt-Button') and contains(@id,'scan-query-start-button')]")).click();
			Utilities.utilityFileWriteOP.writeToLog(tcid, "Search button for query in Item Page of sit.xxwmm.wholesale.core.orderHeaders ", "Clicked!!!",ResultPath);
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(text(),'"+customerName+orderID+"')]")));
			Utilities.utilityFileWriteOP.writeToLog(tcid, "The order detail row for "+customerName+orderID, "Appeared!!!",ResultPath);
			
			String awsDynamoDB_sc7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc7, driver);
			
			/*List<WebElement> orderHeaderParams=driver.findElements(By.xpath("//tr/th[@class='GBVW1WSFY']/div/div"));
			List<WebElement> orderParams=driver.findElements(By.xpath("//tr[@class='GBVW1WSPX']/td/div/div/div"));*/
			
			List<WebElement> orderParams=driver.findElements(By.xpath("//table/tbody/tr/td/div/div/div"));
			
			
			//Below code validates the data of OrderHeader table
			Workbook workbook=Workbook.getWorkbook(new File(validationSheetPath));
			Sheet order=workbook.getSheet("Order");
			Sheet item=workbook.getSheet("Item");
			int i=1;
			for(WebElement element:orderParams)
			{
				if(element.getText().equals(""))
					continue;
				System.out.print("    "+element.getText()+"    ");
				String value=order.getCell(i, 1).getContents().trim();
				if(!value.equals(element.getText().trim()))
					{res=false;
					
					Actions action=new Actions(driver);
					action.moveToElement(element);
					String awsDynamoDB_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
					TakeScreenShot.saveScreenShot(awsDynamoDB_sc8, driver);
					
					Utilities.utilityFileWriteOP.writeToLog(tcid, "The details of "+order.getCell(i, 0).getContents().trim(), "capture from validation excel is "+value);
					Utilities.utilityFileWriteOP.writeToLog(tcid, "The details of "+order.getCell(i, 0).getContents().trim(), "Captured from Dynamo DB is "+element.getText().trim());
					Utilities.utilityFileWriteOP.writeToLog(tcid, "The details of "+order.getCell(i, 0).getContents().trim(), "Did not match!!!",ResultPath);
					break;
					}
				else
				{
					Actions action=new Actions(driver);
					action.moveToElement(element);
					String awsDynamoDB_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
					TakeScreenShot.saveScreenShot(awsDynamoDB_sc9, driver);
					Utilities.utilityFileWriteOP.writeToLog(tcid, "The details of "+order.getCell(i, 0).getContents().trim(), "Matched!!!",ResultPath);
					res=true;
					i++;
				}
				
			}
			
			//Below code for validation in OrderItems table
			driver.findElement(By.xpath("//a[contains(text(),'wholesale.core.orderItems')]")).click();
			driver.findElement(By.xpath("//a[contains(text(),'wholesale.core.orderItems')]")).click();
			Utilities.utilityFileWriteOP.writeToLog(tcid, "Link of sit.xxwmm.wholesale.core.orderItems ", "Clicked!!!",ResultPath);
			
			String awsDynamoDB_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc9, driver);
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[contains(@class,'gwt-Label') and contains(text(),'wholesale.core.orderItems')]")));
			Utilities.utilityFileWriteOP.writeToLog(tcid, "Page of sit.xxwmm.wholesale.core.orderItems ", "Appeared!!!",ResultPath);
			
			String awsDynamoDB_sc10=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc10, driver);
			
			driver.findElement(By.xpath("//div[@class='gwt-Label' and text()='Items']")).click();
			Utilities.utilityFileWriteOP.writeToLog(tcid, "Item tab of Link of sit.xxwmm.wholesale.core.orderorderItems Page", "Clicked!!!",ResultPath);
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//select[contains(@class,'gwt-ListBox') and contains(@id,'scan-query-operation-type-drop-down')]")));
			Utilities.utilityFileWriteOP.writeToLog(tcid, "Item Page of sit.xxwmm.wholesale.core.orderItems ", "Appeared!!!",ResultPath);
			
			String awsDynamoDB_sc11=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc11, driver);
			
			driver.findElement(By.xpath("//select[contains(@class,'gwt-ListBox') and contains(@id,'scan-query-operation-type-drop-down')]")).click();
			Utilities.utilityFileWriteOP.writeToLog(tcid, "Scan/Query drop down list of Link of sit.xxwmm.wholesale.core.orderorderItems Page", "Clicked!!!",ResultPath);
			
			driver.findElement(By.xpath("//select[contains(@class,'gwt-ListBox') and contains(@id,'scan-query-operation-type-drop-down')]/option[@value='Query']")).click();
			Utilities.utilityFileWriteOP.writeToLog(tcid, "Query option in the drop down list of Link of sit.xxwmm.wholesale.core.orderorderItems Page", "Clicked!!!",ResultPath);
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[contains(@class,'gwt-TextBox') and contains(@id,'scan-query-primary-sort-key-attribute-value-text-field')]")));
			Utilities.utilityFileWriteOP.writeToLog(tcid, "Search field for query in Item Page of sit.xxwmm.wholesale.core.orderorderItems ", "Appeared!!!",ResultPath);
			
			String awsDynamoDB_sc12=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc12, driver);
		
			driver.findElement(By.xpath("//input[contains(@class,'gwt-TextBox') and contains(@id,'scan-query-primary-sort-key-attribute-value-text-field')]")).sendKeys(customerName+orderID);
			Utilities.utilityFileWriteOP.writeToLog(tcid, "Search field for query in Item Page of sit.xxwmm.wholesale.core.orderItems ", "Populated with "+customerName+orderID);
			
			String awsDynamoDB_sc13=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc13, driver);
			
			driver.findElement(By.xpath("//button[contains(@class,'gwt-Button') and contains(@id,'scan-query-start-button')]")).click();
			Utilities.utilityFileWriteOP.writeToLog(tcid, "Search button for query in Item Page of sit.xxwmm.wholesale.core.orderorderItems ", "Clicked!!!",ResultPath);
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(text(),'"+customerName+orderID+"')]")));
			Utilities.utilityFileWriteOP.writeToLog(tcid, "The order detail row for "+customerName+orderID, "Appeared!!!",ResultPath);
			
			String awsDynamoDB_sc14=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc14, driver);
			
			
			List<WebElement> items=driver.findElements(By.xpath("//a[contains(text(),'"+customerName+orderID+"')]"));
			//System.out.println("The number of items present in Dynamo DB is "+items.size());
			
			for(int j=1;j<item.getRows();j++)
			{
				String itemID=item.getCell(1, j).getContents().trim();
				
				driver.findElement(By.xpath("//input[contains(@class,'gwt-TextBox GBVW1WSGK')]")).clear();
				Utilities.utilityFileWriteOP.writeToLog(tcid, "Items Search field for query in Item Page of sit.xxwmm.wholesale.core.orderorderItems ", "Cleared!!!",ResultPath);
				
				driver.findElement(By.xpath("//input[contains(@class,'gwt-TextBox GBVW1WSGK')]")).sendKeys(itemID);
				Utilities.utilityFileWriteOP.writeToLog(tcid, "Items Search field for query in Item Page of sit.xxwmm.wholesale.core.orderorderItems ", "Populated with "+itemID);
				
				String awsDynamoDB_sc15=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(awsDynamoDB_sc15, driver);
				
				driver.findElement(By.xpath("//button[contains(@class,'gwt-Button') and contains(@id,'scan-query-start-button')]")).click();
				Utilities.utilityFileWriteOP.writeToLog(tcid, "Search button for query in Item Page of sit.xxwmm.wholesale.core.orderorderItems ", "Clicked!!!",ResultPath);
				
				Thread.sleep(1000);
				List<WebElement> itemParams=driver.findElements(By.xpath("//table/tbody/tr/td/div/div/div"));
				
				int k=1;
				for(WebElement element:itemParams)
				{
					//System.out.println();
					//System.out.println("The k is now "+k);
					//System.out.println("The j is now "+j);
					
					if(element.getText().equals(""))
					{
						//System.out.println("Got a blank value in list");
						continue;
					}
					System.out.print("    "+element.getText()+"    ");
					String value=item.getCell(k, j).getContents().trim();
					if(!value.equals(element.getText().trim()))
					{
						if(k!=4)
						{
							Actions action=new Actions(driver);
							action.moveToElement(element);
							String awsDynamoDB_sc16=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
							TakeScreenShot.saveScreenShot(awsDynamoDB_sc16, driver);
							
							res=false;
						Utilities.utilityFileWriteOP.writeToLog(tcid, "The details of "+item.getCell(k, 0).getContents().trim(), "capture from validation excel is "+value);
						Utilities.utilityFileWriteOP.writeToLog(tcid, "The details of "+item.getCell(k, 0).getContents().trim(), "Captured from Dynamo DB is "+element.getText().trim());
						Utilities.utilityFileWriteOP.writeToLog(tcid, "The details of "+item.getCell(k, 0).getContents().trim(), "Did not match!!!",ResultPath);
						break;
						}
						else
						{
							Actions action=new Actions(driver);
							action.moveToElement(element);
							String awsDynamoDB_sc16=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
							TakeScreenShot.saveScreenShot(awsDynamoDB_sc16, driver);
							
							k++;
						}
					}
					else
					{
						Actions action=new Actions(driver);
						action.moveToElement(element);
						String awsDynamoDB_sc16=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
						TakeScreenShot.saveScreenShot(awsDynamoDB_sc16, driver);
						
						Utilities.utilityFileWriteOP.writeToLog(tcid, "The details of "+item.getCell(k, 0).getContents().trim(), "Matched!!!",ResultPath);
						res=true;
						k++;
					}
					
				}
				
				
				
			}
			
			
			
			
			
		}
		catch(Exception e)
		{
			
			String awsDynamoDB_sc16=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc16, driver);
			
			
			e.printStackTrace();
			res=false;
			Utilities.utilityFileWriteOP.writeToLog(tcid, "A problem occurred while validating in DynamoDB", "Due to "+e);
		
		}
		finally
		{
			return res;
		}
	}
	
	
	public static boolean validateFileExistenceInS3Folder(WebDriver driver, WebDriverWait wait, String searchKey, String ssourceFilePath, String foldernames, String tcid,String ResultPath)
	{
		boolean res=false;
		
		try
		{
			String[] folder=foldernames.split("->");
			//System.out.println(ssourceFilePath);
			driver.findElement(By.xpath("//input[@placeholder='Search for buckets' and @type='text']")).sendKeys(searchKey);
			Utilities.utilityFileWriteOP.writeToLog(tcid, "Search field is populated with ", "Keyword "+searchKey);
			
			String awsDynamoDB_sc1=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc1, driver);
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(@class,'list-view-item-name') and text()='"+searchKey+"']")));
			Utilities.utilityFileWriteOP.writeToLog(tcid, "Link related to keyword "+searchKey, "Appeared!!!",ResultPath);
			
			String awsDynamoDB_sc2=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc2, driver);
			
			driver.findElement(By.xpath("//a[contains(@class,'list-view-item-name') and text()='"+searchKey+"']")).click();
			Utilities.utilityFileWriteOP.writeToLog(tcid, "Link related to keyword "+searchKey, "Clicked!!!",ResultPath);
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(@class,'list-view-item-name object-name') and text()=' "+folder[0]+" ']")));
			Utilities.utilityFileWriteOP.writeToLog(tcid, "Inbound folder ", "Appeared!!!",ResultPath);
			
			String awsDynamoDB_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc3, driver);
			
			
			for(int foldercount=0;foldercount<folder.length;foldercount++)
			{
				try {
					driver.findElement(By.xpath("//a[contains(@class,'list-view-item-name object-name') and text()=' "+folder[foldercount]+" ']")).click();
					Utilities.utilityFileWriteOP.writeToLog(tcid, "Inbound folder ", "Clicked!!!",ResultPath);
					
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(@class,'list-view-item-name object-name') and text()=' "+folder[foldercount+1]+" ']")));
					Utilities.utilityFileWriteOP.writeToLog(tcid, "MasterData folder ", "Appeared!!!",ResultPath);
					
					String awsDynamoDB_sc4=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
					TakeScreenShot.saveScreenShot(awsDynamoDB_sc4, driver);
				} catch (Exception e) {

					Utilities.utilityFileWriteOP.writeToLog(tcid, "Inside of "+folder[foldercount]+" folder ", "Appeared!!!",ResultPath);
				}
			}
			
			String latestfiletimestamp=getLatestFileName(ssourceFilePath);
			//System.out.println(latestfiletimestamp);
			String awsDynamoDB_sc7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc7, driver);
			
			if(driver.findElement(By.xpath("//a[contains(@class,'list-view-item-name object-name') and contains(text(),'"+latestfiletimestamp+"')]")).isDisplayed())
				{
					res=true;
					Utilities.utilityFileWriteOP.writeToLog(tcid, "The file "+latestfiletimestamp+".dat", "Exists in pending folder!!!",ResultPath);
				}
		}
		catch(Exception e)
		{
			String awsDynamoDB_sc=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc, driver);
			res=false;
			Utilities.utilityFileWriteOP.writeToLog(tcid, "A problem occurred while navigating through AWS S3 bucket", "Due to "+e);
		}
		finally
		{
			return res;
		}
	}
	
	
	public static String GetCallJobFileListedStatus(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String JobID, String tcid,String ResultPath, int r, String DriverSheetPath,String SheetName) throws IOException {
	      
	       String res = "notFound";
	       System.out.println("In Getcall Job");
	      
	       CloseableHttpResponse response=null;
	   
	    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	      
	    credsProvider.setCredentials(
	              new AuthScope(ProxyHostName, ProxyPort),
	              new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials
	 
	    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();
	 
	    try {
	             
	       HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
	        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
	       
	        RequestConfig config = RequestConfig.custom()
	                .setProxy(proxy)
	                .build();
	       
	        HttpGet httpget = new HttpGet(UrlTail + JobID + "/status?" + ApiKey);

	        httpget.setConfig(config);
	       
	        httpget.addHeader(AuthorizationKey, AuthorizationValue);
	       
	        
	        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Job ID "+JobID,ResultPath);
	        
	        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath);
	       
	        
	       
	             
	              response = httpclient.execute(target, httpget);
	           
	              utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath);
	           
	            String jsonresponse = EntityUtils.toString(response.getEntity());
	            System.out.println(jsonresponse);
	            utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath);
	          
	            JsonElement jsonelement = new JsonParser().parse(jsonresponse);
	            JsonObject jsonobject = jsonelement.getAsJsonObject();
	           
	            JsonArray jarray = jsonobject.getAsJsonArray("workflows");
	       
	           
	            for(int i=1; i<= jarray.size(); i++) 
	            {
	             
	              JsonObject jsonobjectEachWorkflow = jarray.get(i-1).getAsJsonObject();
	                String targetSystem = jsonobjectEachWorkflow.get("taskCurrentState").toString().replaceAll("^\"|\"$", "");
	               
	                if(targetSystem.equals("orderfilelisted")) {
	             
	               
	                  String TaskCurrentStatus = jsonobjectEachWorkflow.get("taskCurrentState").toString().replaceAll("^\"|\"$", "");
	                   System.out.println(TaskCurrentStatus);
	                     if(TaskCurrentStatus!=null) 
	                     {
	                           res = TaskCurrentStatus;
	                        

	                           break;
	                     }
	                    
	                     else
	                     {
	                           res = "not Found";

	                           break;
	                     }
	               
	              }
	               
	            } }
	            catch (Exception e) 
	            {
	             
	              e.printStackTrace();
	              utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e,ResultPath);
	       
	              res = "not found";
	              return res;
	            }
	                	   
	    finally{
	       
	        response.close();
	       
	    }
	   
	    return res;
	   
	      
	}
	
	
	
	
	
	public static String GetCallJobFileReceivedStatus(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String JobID, String tcid,String ResultPath, int r, String DriverSheetPath,String SheetName) throws IOException {
	      
	       String res = null;
	       System.out.println("In Getcall Job");
	      
	       CloseableHttpResponse response=null;
	   
	    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	      
	    credsProvider.setCredentials(
	              new AuthScope(ProxyHostName, ProxyPort),
	              new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials
	 
	    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();
	 
	    try {
	             
	       HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
	        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
	       
	        RequestConfig config = RequestConfig.custom()
	                .setProxy(proxy)
	                .build();
	       
	
	        
	        
	        HttpGet httpget = new HttpGet(UrlTail + JobID + "/status?" + ApiKey);

	        httpget.setConfig(config);
	       
	        httpget.addHeader(AuthorizationKey, AuthorizationValue);
	       
	        
	        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Job ID "+JobID,ResultPath);
	        
	        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath);
	       
	        
	       
	             
	         response = httpclient.execute(target, httpget);
	           
	              utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath);
	           
	            String jsonresponse = EntityUtils.toString(response.getEntity());
	            
	            
	            
	            
	            System.out.println("Final Json response is "+jsonresponse);
	           
	            
	            
	            
	            utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath);
	          
	            JsonElement jsonelement = new JsonParser().parse(jsonresponse);
	            JsonObject jsonobject = jsonelement.getAsJsonObject();
	           
	            res=jsonobject.get("jobCurrentState").toString();
	           
	       
	      
	       } catch (Exception e) {
	             
	              e.printStackTrace();
	              utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e,ResultPath);
	       
	              res = null;
	              return res;
	       }
	   
	    finally{
	       
	        response.close();
	       
	}
	   
	    return res;
	   
	      
	}
	
	
	
	
	
	public static String GetCallJobStatusExternalFileNameinNas(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String JobID, String tcid,String ResultPath, int r, String DriverSheetPath,String SheetName, XWPFRun xwpfRun) throws IOException {
	      
	       String res = null;
	       System.out.println("In Getcall Job");
	      
	       CloseableHttpResponse response=null;
	   
	    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	      
	    credsProvider.setCredentials(
	              new AuthScope(ProxyHostName, ProxyPort),
	              new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials
	 
	    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();
	 
	    try {
	             
	       HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
	        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
	       
	        RequestConfig config = RequestConfig.custom()
	                .setProxy(proxy)
	                .build();
	       
	        HttpGet httpget = new HttpGet(UrlTail + JobID + "/status?" + ApiKey);

	        httpget.setConfig(config);
	       
	        httpget.addHeader(AuthorizationKey, AuthorizationValue);
	       
	        
	        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Job ID "+JobID,ResultPath, xwpfRun,"");
	        
	        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfRun, "");

	              response = httpclient.execute(target, httpget);
	           
	              utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath, xwpfRun, "");
	           
	            String jsonresponse = EntityUtils.toString(response.getEntity());
	            System.out.println(jsonresponse);
	            utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath, xwpfRun,"");
	           
	          
	            
	            JsonElement jsonelement = new JsonParser().parse(jsonresponse);
	            JsonObject jsonobject = jsonelement.getAsJsonObject();
	           
	          
	           
	            JsonArray jarray = jsonobject.getAsJsonArray("workflows");
	            System.out.println(jarray);
	           
	            for(int i=1; i<= jarray.size(); i++) {
	             
	              JsonObject jsonobjectEachWorkflow = jarray.get(i-1).getAsJsonObject();
	                String targetSystem = jsonobjectEachWorkflow.get("targetSystem").toString().replaceAll("^\"|\"$", "");
	               
	                if(targetSystem.equals("MainFrame")) {
	                    
	                     String jsonfileName = jsonobjectEachWorkflow.get("fileName").toString().replaceAll("^\"|\"$", "");
	                    System.out.println(jsonfileName);
	                     if(jsonfileName!=null) 
	                     {
	                           res = jsonfileName;
	                           
	                         utilityFileWriteOP.writeToLog(tcid, "The FileName for JobID "+JobID, "in NAS is "+jsonfileName,ResultPath,xwpfRun,"");
	                         
	                           break;
	                     }
	                    
	                     else
	                     {
	                           res = null;
	                           utilityFileWriteOP.writeToLog(tcid, "The FileName for JobID "+JobID, "in NAS is "+jsonfileName,ResultPath,xwpfRun,"");
	                       
	                        
	                           break;
	                     }
	                }
	            }
	            
	            
	            
	            utilityFileWriteOP.writeToLog(tcid, "The FileName is ", "Displayed as "+res,ResultPath, xwpfRun, "");
	           
	        
	            //response.toString();
	            /*if(result.equals("\"shipped\"")) {
	              res = true;
	              //break;
	            }*/
	           
	       // }
	       
	        
	      
	       } catch (Exception e) {
	             
	              e.printStackTrace();
	              utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e,ResultPath,xwpfRun,"");
	       
	              res = null;
	              return res;
	       }
	   
	    finally{
	       
	        response.close();
	       
	}
	   
	    return res;
	   
	      
	}
	
	
	
	
	
	public static boolean RelexOutboundNasValidationFiles(String NasPath, String FileNameinNAS, String OrderID, String tcid,String ResultPath) throws IOException {
	      
	       JSch jsch = new JSch();
	       Session session = null;
	       BufferedReader br = null;
	      
	       boolean OrderIdValidation = true;
	      
	       String Result = null;
	       int datflag = 0;
	       int trgflag = 0;
	      
	       int count = 0;
	      
	       String[] AllOrderIDs = OrderID.split(",");
	      
	       try {
	             
	              session = jsch.getSession("systcs7y", "temus06x", 22);
	              session.setConfig("StrictHostKeyChecking", "no");
	              session.setPassword("lj8Rncpx");
	              session.connect();
	             
	              Channel channel = session.openChannel("sftp");
	              channel.connect();
	             
	              ChannelSftp sftpchannel = (ChannelSftp) channel;
	             
	       /*     sftpchannel.cd("cd..;");
	              sftpchannel.cd("cd..;");*/
	             
	             
	              sftpchannel.cd(NasPath);
	             
	              //System.out.println("The Current path is " + sftpchannel.pwd());
	             
	              Vector<ChannelSftp.LsEntry> listdat = sftpchannel.ls("*.dat");
	             
	              Vector<ChannelSftp.LsEntry> listtrg = sftpchannel.ls("*.trg");
	             
	              for(ChannelSftp.LsEntry entry: listtrg) {
	                    
	                     String filename = entry.getFilename();
	                    
	                     //System.out.println("The current file is " + filename);
	                     //System.out.println("Testing for file " + FileNameinNAS.substring(0, FileNameinNAS.length() - 4) + ".trg");
	                     if(filename.equals(FileNameinNAS.substring(0, FileNameinNAS.length() - 4) + ".trg")) {
	                          
	                           //System.out.println("trg file found");
	                           OrderIdValidation = OrderIdValidation && true;
	                           trgflag = 1;
	                           break;
	                     }
	                    
	                    
	              }
	             
	              if(trgflag != 1) {
	                     //System.out.println("trg file not found");
	                     OrderIdValidation = OrderIdValidation && false;
	              }
	             
	              for(ChannelSftp.LsEntry entry: listdat) {
	                    
	                     String filename = entry.getFilename();
	                    
	                     //System.out.println("The current file is " + filename);
	                    
	                     if(filename.equals(FileNameinNAS)) {
	                           //System.out.println("dat file found");
	                           sftpchannel.get(filename, "S:/WebService Automation/McColls/TestData/S3DAT/" + filename);//Copy file from WinSCP to local
	                          
	                           //Read from local
	                           br = new BufferedReader(new FileReader("S:/WebService Automation/McColls/TestData/S3DAT/" + filename));
	                          
	                          
	                           String CurrentLine = null;
	                          
	                           while((CurrentLine = br.readLine()) != null) {
	                                 
	                                  //Split the Current line in comma seperated values
	                                  String[] strarr = CurrentLine.split("\\|");
	                                  //System.out.println(strarr[1]);
	                                  //System.out.println(AllOrderIDs[count]);
	                                  if(strarr[1].substring(5).equals(AllOrderIDs[count])) {
	                                        
	                                         OrderIdValidation = OrderIdValidation && true;
	                                         //System.out.println("The Order id after trimming first five digits is " + strarr[1].substring(5));
	                                         utilityFileWriteOP.writeToLog(tcid, "Successfully matched for OrderID ", AllOrderIDs[count]);
	                                        
	                                        
	                                        
	                                  }
	                                 
	                                  else{
	                                        
	                                         OrderIdValidation = OrderIdValidation && false;
	                                         //System.out.println("The Order id is " + strarr[1]);
	                                         utilityFileWriteOP.writeToLog(tcid, "Failed match for OrderID ", AllOrderIDs[count]);
	 
	                                  }
	                                 
	                          
	                                count++; 
	                           }
	                          
	                          
	                           datflag = 1;
	                          
	                     }
	                    
	                    
	                    
	                    
	                    
	                     if(datflag == 1) {
	                          
	                           br.close();
	                           break;
	                          
	                     }
	                    
	                    
	      
	                    
	              }
	             
	              if(datflag != 1) {
	                     //System.out.println("dat file not found");
	              }
	             
	              br.close();
	              sftpchannel.exit();
	             
	              session.disconnect();
	             
	      
	             
	             
	      
	             
	             
	       } catch (Exception e) {
	              // TODO: handle exception
	              e.printStackTrace();
	              //System.out.println(e);
	              utilityFileWriteOP.writeToLog(tcid, "Error occurred during Order ID Validation ", "Due to :"+e);
	              OrderIdValidation = false;
	              return OrderIdValidation;
	       }
	      
	      
	      
	 
	      
	       return OrderIdValidation;

	   
	}
	
	
	
	
	
	
	
	
	public static boolean RelexOutboundNasoutboundValidationFiles(String NasPath, String FileNameinNAS, String OrderID, String tcid,String ResultPath) throws IOException {
	      
	       JSch jsch = new JSch();
	       Session session = null;
	       BufferedReader br = null;
	      
	       boolean OrderIdValidation = true;
	      
	       String Result = null;
	       int datflag = 0;
	       int trgflag = 0;
	      
	       int count = 0;
	      
	       String[] AllOrderIDs = OrderID.split(",");
	      
	       try {
	             
	              session = jsch.getSession("mwadmin", "temus06x", 22);
	              session.setConfig("StrictHostKeyChecking", "no");
	              session.setPassword("2Bchanged");
	              session.connect();
	             
	              Channel channel = session.openChannel("sftp");
	              channel.connect();
	             
	              ChannelSftp sftpchannel = (ChannelSftp) channel;
	             
	       /*     sftpchannel.cd("cd..;");
	              sftpchannel.cd("cd..;");*/
	             
	             
	              sftpchannel.cd(NasPath);
	             
	              //System.out.println("The Current path is " + sftpchannel.pwd());
	             
	              Vector<ChannelSftp.LsEntry> listdat = sftpchannel.ls("*.dat");
	             
	              //Vector<ChannelSftp.LsEntry> listtrg = sftpchannel.ls("*.trg");
	             /*
	              for(ChannelSftp.LsEntry entry: listtrg) {
	                    
	                     String filename = entry.getFilename();
	                    
	                     //System.out.println("The current file is " + filename);
	                     //System.out.println("Testing for file " + FileNameinNAS.substring(0, FileNameinNAS.length() - 4) + ".trg");
	                     if(filename.equals(FileNameinNAS.substring(0, FileNameinNAS.length() - 4) + ".trg")) {
	                          
	                           //System.out.println("trg file found");
	                           OrderIdValidation = OrderIdValidation && true;
	                           trgflag = 1;
	                           break;
	                     }
	                    
	                    
	              }
	             
	              if(trgflag != 1) {
	                     //System.out.println("trg file not found");
	                     OrderIdValidation = OrderIdValidation && false;
	              }
	             */
	              for(ChannelSftp.LsEntry entry: listdat) {
	                    
	                     String filename = entry.getFilename();
	                    
	                     //System.out.println("The current file is " + filename);
	                    
	                     if(filename.equals(FileNameinNAS)) {
	                           //System.out.println("dat file found");
	                           sftpchannel.get(filename, "C:/Auto/Files" + filename);//Copy file from WinSCP to local
	                          
	                           //Read from local
	                           br = new BufferedReader(new FileReader("C:/Auto/Files" + filename));
	                          
	                          
	                           String CurrentLine = null;
	                          
	                           while((CurrentLine = br.readLine()) != null) {
	                                 
	                                  //Split the Current line in comma seperated values
	                                  String[] strarr = CurrentLine.split("\\s+");
	                                  //System.out.println(strarr[0]);
	                                  //System.out.println(AllOrderIDs[count]);
	                                  if(strarr[0].substring(5,20).equals(AllOrderIDs[count])) {
	                                        
	                                         OrderIdValidation = OrderIdValidation && true;
	                                         //System.out.println("The Order id after trimming first five digits is " + strarr[0].substring(5,20));
	                                         utilityFileWriteOP.writeToLog(tcid, "Successfully matched for OrderID ", AllOrderIDs[count]);
	                                        
	                                        
	                                        
	                                  }
	                                 
	                                  else{
	                                        
	                                         OrderIdValidation = OrderIdValidation && false;
	                                         //System.out.println("The Order id is " + strarr[1]);
	                                         utilityFileWriteOP.writeToLog(tcid, "Failed match for OrderID ", AllOrderIDs[count]);
	 
	                                  }
	                                 
	                          
	                                count++; 
	                           }
	                          
	                          
	                           datflag = 1;
	                          
	                     }
	                    
	                    
	                    
	                    
	                    
	                     if(datflag == 1) {
	                          
	                           br.close();
	                           break;
	                          
	                     }
	                    
	                    
	      
	                    
	              }
	             
	              if(datflag != 1) {
	                     //System.out.println("dat file not found");
	              }
	             
	              br.close();
	              sftpchannel.exit();
	             
	              session.disconnect();
	             
	      
	             
	             
	      
	             
	             
	       } catch (Exception e) {
	              // TODO: handle exception
	              e.printStackTrace();
	              //System.out.println(e);
	              utilityFileWriteOP.writeToLog(tcid, "Error occurred during Order ID Validation ", "Due to :"+e);
	              OrderIdValidation = false;
	              return OrderIdValidation;
	       }
	      
	      
	      
	 
	      
	       return OrderIdValidation;
	
	}
	public static boolean validateFilesinS3(WebDriver driver, WebDriverWait wait, String searchKey, String filename, String tcid,String ResultPath, XWPFRun xwpfRun)
	{
		boolean res=false;
		
		try
		{
			
			
			driver.findElement(By.xpath("//input[@placeholder='Search for buckets' and @type='text']")).sendKeys(searchKey);
			
			String AfterLogin=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(AfterLogin, driver);
			utilityFileWriteOP.writeToLog(tcid, "Search field is populated with ", "Keyword "+searchKey,ResultPath,xwpfRun,"");
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(@class,'list-view-item-name') and text()='"+searchKey+"']")));
			
			
			String AfterLogin1=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(AfterLogin1, driver);
			utilityFileWriteOP.writeToLog(tcid, "Link related to keyword "+searchKey, "Appeared!!!",ResultPath,xwpfRun);
			
			driver.findElement(By.xpath("//a[contains(@class,'list-view-item-name') and text()='"+searchKey+"']")).click();
			utilityFileWriteOP.writeToLog(tcid, "Link related to keyword "+searchKey, "Clicked!!!",ResultPath,xwpfRun,"");
			
			String Full="R"+filename.substring(0, 39);
			//System.out.println(Full);
			
			
			driver.findElement(By.xpath("//input[@placeholder='Type a prefix and press Enter to search. Press ESC to clear.' and @type='text']")).sendKeys(Full);
			
			
			Robot rb = new Robot();			
			rb.keyPress(KeyEvent.VK_ENTER);
			Thread.sleep(500);
			
			
			
			
			if(driver.findElement(By.xpath("//a[contains(@class,'list-view-item-name object-name') and contains(text(),'"+Full+"')]")).isDisplayed())
				{
					res=true;
					String AfterLogin2=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
					TakeScreenShot.saveScreenShot(AfterLogin2, driver);
					utilityFileWriteOP.writeToLog(tcid, "The file "+filename+".dat", "Exists in pending folder!!!",ResultPath,xwpfRun);
				}
			driver.findElement(By.xpath("//*[@id='nav-usernameMenu']/div[2]")).click();
			driver.findElement(By.xpath("//*[@id='aws-console-logout']")).click();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			String AfterLogin=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(AfterLogin, driver);
			res=false;
			utilityFileWriteOP.writeToLog(tcid, "A problem occurred while navigating through AWS S3 bucket", "Due to "+e,ResultPath, xwpfRun,"");
		}
		
		
		return res;
		
	}
	
	public static String downloadFilefromS3(WebDriver driver, WebDriverWait wait, String searchKey, String jobid, String downloadpath, String tcid,String ResultPath, XWPFRun xwpfRun){
		String res=null;
		try{
			WebElement jsonfile=driver.findElement(By.xpath("//a[contains(text(),'"+jobid+"')]"));
			wait.until(ExpectedConditions.elementToBeClickable(jsonfile));
			jsonfile.click();

			WebElement downloadbutton=driver.findElement(By.xpath("//*[@id='download']//span[text()='Download']"));
			wait.until(ExpectedConditions.elementToBeClickable(downloadbutton));
			downloadbutton.click();
//			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			Thread.sleep(20000);
			String folderName = downloadpath; // Give your folderName
			File[] listFiles = new File(folderName).listFiles();

			for (int i = 0; i < listFiles.length; i++) {

			    if (listFiles[i].isFile()) {
			        String fileName = listFiles[i].getName();
			        if (fileName.startsWith(jobid)
			                && fileName.endsWith(".json")) {
			            res=fileName;
			            System.out.println("fileName:"+fileName);
			            
			            break;
			        }else
			        	res=null;
			    }
			}
		}catch(Exception e)
		{
			e.printStackTrace();
			String AfterLogin=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(AfterLogin, driver);
			res=null;
			utilityFileWriteOP.writeToLog(tcid, "A problem occurred while downloading json from AWS S3 bucket", "Due to "+e,ResultPath, xwpfRun,"");
		}
		
		
		return res;
		
		
	}
	
	public static boolean Ecs_task_runner_job_trigger(WebDriver driver,WebDriverWait wait, String TC_no, String searchkey, String taskname , String Resultpath ,XWPFRun xwpfrun, ExtentTest logger)
	{
		{
			String[] tasknames= taskname.split(",");
			boolean res=false;
			try{
				//Click on AWS button
				
				
				Resultpath=System.getProperty("user.dir")+"/"+Resultpath;
				
				
				driver.findElement(By.xpath("//div[@id='nav-logo']")).click();
				
				//Waiting for AWS Services Page
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='search-box-input']")));
				
				if(!(driver.findElement(By.xpath("//input[@id='search-box-input']")).isDisplayed())){
					String SearchInputBox=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
					TakeScreenShot.saveScreenShot(SearchInputBox, driver);
//					logger.log(LogStatus.FAIL, "Search Input box not displayed");
//					logger.log(LogStatus.FAIL,logger.addScreenCapture(SearchInputBox));
					res =false;
					System.out.println("NOT Found");
					throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					
				}else
					System.out.println("Found");
				
				boolean imgres1=driver.findElement(By.xpath("//input[@id='search-box-input']")).isDisplayed();
				System.out.println(imgres1);
				Thread.sleep(10000);
				if(imgres1)
				{

					//utilityFileWriteOP.writeToLog(TC_no, "AWS Services Page", "Found!!!",Resultpath,xwpfrun);

					// Take Screenshot
					String AWSServicePage_WP1=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
					TakeScreenShot.saveScreenShot(AWSServicePage_WP1, driver);
					
					driver.findElement(By.xpath("//input[@id='search-box-input']")).sendKeys("ECS");
					utilityFileWriteOP.writeToLog(TC_no, "ECS Keyword", "Entered!!!",Resultpath);
//					logger.log(LogStatus.PASS, "Lambda Keyword is Entered!!!");
//					logger.log(LogStatus.PASS, logger.addScreenCapture(AWSServicePage_WP1));
					driver.findElement(By.xpath("//span[@class='awsui-select-option-filtering-match-highlight' and contains(text(), 'ECS')]")).click();
					utilityFileWriteOP.writeToLog(TC_no, "ECS Option ", "Clicked!!!",Resultpath);
					//Waiting for ECS Functions Search Page
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(text(),'Task Definitions')]")));
					utilityFileWriteOP.writeToLog(TC_no, "ECS Search Page", "Found!!!",Resultpath);
					// Take Screenshot
					String LambdaFnSearch_WP1=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
					TakeScreenShot.saveScreenShot(LambdaFnSearch_WP1, driver);
					//loop for the number of jobs to run
					
					
					
					
					for(int i=0;i<tasknames.length;i++){

						//click on task definitions				
						driver.findElement(By.xpath("//a[contains(text(),'Task Definitions')]")).click();
						Thread.sleep(2000);
						wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[@class='ecs-header-text ng-binding' and contains(text(),'Task Definitions')]")));

						driver.findElement(By.xpath("//span[@class='awsui-select-value' and contains(text(),'50')]")).click();
						WebElement element2 = driver.findElement(By.xpath("//li[@title='100' and contains(text(),'100')]"));
						JavascriptExecutor executor2 = (JavascriptExecutor)driver;
						executor2.executeScript("arguments[0].click();", element2);		
						//loop
						boolean taskvisible =false;

						driver.findElement(By.xpath("//input[@placeholder ='Filter in this page' and @type='text']")).sendKeys(tasknames[i]);

						utilityFileWriteOP.writeToLog(TC_no, "Task name: "+tasknames[i], "Entered!!!",Resultpath);
//						logger.log(LogStatus.INFO, "Task name: "+tasknames[i]+" Entered!!!");

						int count=0;

//						driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
						Thread.sleep(10000);

						while(taskvisible!=true){

							try{



								taskvisible= driver.findElement(By.xpath("//a[contains(text(),'"+tasknames[i]+"')]")).isDisplayed();


							}



							catch(Exception e){
								System.out.println("Visibilty of task"+taskvisible);
								driver.findElement(By.xpath("//i[@class='awsui-icon angle-right']")).click();

								count++;
								if(count>15)
									break;
							}



						}
						System.out.println("task found");
						driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
						driver.findElement(By.xpath("(//a[ contains(text(),'"+tasknames[i]+"')])[1]")).click();
						driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
						driver.findElement(By.xpath("(//a[ contains(text(),'"+tasknames[i]+"')])[2]")).click();
						driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
						wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='btn dropdown-label ng-binding' and contains(text(),'Actions')]")));
						driver.findElement(By.xpath("//div[@class='btn dropdown-label ng-binding' and contains(text(),'Actions')]")).click();
						driver.findElement(By.xpath("//a[contains(text(),'Run Task')]")).click();
						wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[@class='ecs-header-text ng-binding' and contains(text(),'Run Task')]")));
						// Take Screenshot
						String RunTask_TC002_REG_WP2=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
						TakeScreenShot.saveScreenShot(RunTask_TC002_REG_WP2, driver);
						utilityFileWriteOP.writeToLog(TC_no, "Run Task Screen", "Opens!!!",Resultpath);
						logger.log(LogStatus.PASS, "Run Task Screen is displayed.");
						logger.log(LogStatus.PASS,logger.addScreenCapture(RunTask_TC002_REG_WP2));
						Thread.sleep(2000);
						driver.findElement(By.xpath("//div[@class='ecs-table-row-content ng-isolate-scope' and @items='clustersDropDown']")).click();
						utilityFileWriteOP.writeToLog(TC_no,"Click on Cluster Drop Down","List Expanded",Resultpath);
						Thread.sleep(2000);
						// Take Screenshot
						String ClusterDropDown_TC002_REG_WP2=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
						TakeScreenShot.saveScreenShot(ClusterDropDown_TC002_REG_WP2, driver);
						WebElement element = driver.findElement(By.xpath("//a[contains(text(),'ecs-euw1-n-wholesale-vpc001-general-001')]"));
						JavascriptExecutor executor = (JavascriptExecutor)driver;
						executor.executeScript("arguments[0].click();", element);
						System.out.println("Selected cluster");
						utilityFileWriteOP.writeToLog(TC_no,"nonprod-ECS-ECSCluster-W68V9ZHCS399","option selected",Resultpath,xwpfrun);
//						logger.log(LogStatus.PASS, "nonprod-ECS-ECSCluster-W68V9ZHCS399 option selected");
//						logger.log(LogStatus.PASS,logger.addScreenCapture(ClusterDropDown_TC002_REG_WP2));
						Thread.sleep(2000);
						// Take Screenshot
						String ClusterSelection_TC002_REG_WP2=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
						TakeScreenShot.saveScreenShot(ClusterSelection_TC002_REG_WP2, driver);
						WebElement element1 = driver.findElement(By.xpath("//span[contains(text(),'Run Task')]"));
						executor.executeScript("arguments[0].click();", element1);
						System.out.println("Clicked Run Task");
						String TaskTriggered=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
						TakeScreenShot.saveScreenShot(TaskTriggered, driver);
						utilityFileWriteOP.writeToLog(TC_no, "RunTask Button clicked", "Passed!!!",Resultpath,xwpfrun);
//						logger.log(LogStatus.PASS, "RunTask Button clicked");
//						logger.log(LogStatus.PASS,logger.addScreenCapture(TaskTriggered));
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[contains(text(),'Created tasks successfully')]")));
						//  Take Screenshot
						String TaskSuccess=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
						TakeScreenShot.saveScreenShot(TaskSuccess, driver);
						System.out.println("Transfer Request task Triggered Successfully");
//						logger.log(LogStatus.PASS, "Transfer Request task Triggered Successfully");
//						logger.log(LogStatus.PASS,logger.addScreenCapture(TaskSuccess));
						Thread.sleep(1000);
					}
					
					
					utilityFileWriteOP.writeToLog(TC_no, "Transfer Request task triggered succesfully, moving to previous tab", "Passed!!!",Resultpath,xwpfrun);   
					
					res=true; 		
					
					
					return res;
				}
				
				else
				{
					
					
					String SearchInputBox=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
					TakeScreenShot.saveScreenShot(SearchInputBox, driver);
//					logger.log(LogStatus.FAIL, "Search Input box not displayed");
//					logger.log(LogStatus.FAIL,logger.addScreenCapture(SearchInputBox));
					res =false;
					
					throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					
					
				}

			}
			
			
			
			
			catch (Exception e) 
			{	
				e.printStackTrace();
				res=false;
				System.out.println("The error is "+e);
				String Webelementnotfound=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
				TakeScreenShot.saveScreenShot(Webelementnotfound, driver);
				logger.log(LogStatus.FAIL, "Search Input box not displayed");
				logger.log(LogStatus.FAIL,logger.addScreenCapture(Webelementnotfound));
				utilityFileWriteOP.writeToLog(TC_no, "Problem ocuurred for "+e, "Triggering Daily Load",Resultpath);
				return res;
			}

			finally 
			{
				return res;
			}


		}

	}

	public static boolean Store_Entry_Ecs_task_job_trigger(WebDriver driver,WebDriverWait wait, String TC_no, String searchkey, String taskname , String Resultpath ,XWPFRun xwpfrun, ExtentTest logger)
	{
		{
			String[] tasknames= taskname.split(",");
			boolean res=false;
			try{
				//Click on AWS button
				
				
				Resultpath=System.getProperty("user.dir")+"/"+Resultpath;
				
				
				driver.findElement(By.xpath("//div[@id='nav-logo']")).click();
				
				//Waiting for AWS Services Page
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='search-box-input']")));
				
				if(!(driver.findElement(By.xpath("//input[@id='search-box-input']")).isDisplayed())){
					String SearchInputBox=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
					TakeScreenShot.saveScreenShot(SearchInputBox, driver);
//					logger.log(LogStatus.FAIL, "Search Input box not displayed");
//					logger.log(LogStatus.FAIL,logger.addScreenCapture(SearchInputBox));
					res =false;
					System.out.println("NOT Found");
					throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					
				}else
					System.out.println("Found");
				
				boolean imgres1=driver.findElement(By.xpath("//input[@id='search-box-input']")).isDisplayed();
				System.out.println(imgres1);
				Thread.sleep(10000);
				if(imgres1)
				{

					//utilityFileWriteOP.writeToLog(TC_no, "AWS Services Page", "Found!!!",Resultpath,xwpfrun);

					// Take Screenshot
					String AWSServicePage_WP1=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
					TakeScreenShot.saveScreenShot(AWSServicePage_WP1, driver);
					
					driver.findElement(By.xpath("//input[@id='search-box-input']")).sendKeys("ECS");
					utilityFileWriteOP.writeToLog(TC_no, "ECS Keyword", "Entered!!!",Resultpath);
//					logger.log(LogStatus.PASS, "Lambda Keyword is Entered!!!");
//					logger.log(LogStatus.PASS, logger.addScreenCapture(AWSServicePage_WP1));
					driver.findElement(By.xpath("//span[@class='awsui-select-option-filtering-match-highlight' and contains(text(), 'ECS')]")).click();
					utilityFileWriteOP.writeToLog(TC_no, "ECS Option ", "Clicked!!!",Resultpath);
					//Waiting for ECS Functions Search Page
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(text(),'Task Definitions')]")));
					utilityFileWriteOP.writeToLog(TC_no, "ECS Search Page", "Found!!!",Resultpath);
					// Take Screenshot
					String LambdaFnSearch_WP1=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
					TakeScreenShot.saveScreenShot(LambdaFnSearch_WP1, driver);
					//loop for the number of jobs to run
					
					
					
					
					for(int i=0;i<tasknames.length;i++){

						//click on task definitions				
						driver.findElement(By.xpath("//a[contains(text(),'Task Definitions')]")).click();
						Thread.sleep(2000);
						wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[@class='ecs-header-text ng-binding' and contains(text(),'Task Definitions')]")));

						driver.findElement(By.xpath("//span[@class='awsui-select-value' and contains(text(),'50')]")).click();
						WebElement element2 = driver.findElement(By.xpath("//li[@title='100' and contains(text(),'100')]"));
						JavascriptExecutor executor2 = (JavascriptExecutor)driver;
						executor2.executeScript("arguments[0].click();", element2);		
						//loop
						boolean taskvisible =false;

						driver.findElement(By.xpath("//input[@placeholder ='Filter in this page' and @type='text']")).sendKeys(tasknames[i]);

						utilityFileWriteOP.writeToLog(TC_no, "Task name: "+tasknames[i], "Entered!!!",Resultpath);
//						logger.log(LogStatus.INFO, "Task name: "+tasknames[i]+" Entered!!!");

						int count=0;

//						driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
						Thread.sleep(10000);

						while(taskvisible!=true){

							try{



								taskvisible= driver.findElement(By.xpath("//a[contains(text(),'"+tasknames[i]+"')]")).isDisplayed();


							}



							catch(Exception e){
								System.out.println("Visibilty of task"+taskvisible);
								driver.findElement(By.xpath("//i[@class='awsui-icon angle-right']")).click();

								count++;
								if(count>15)
									break;
							}



						}
						System.out.println("task found");
						driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
						driver.findElement(By.xpath("(//a[ contains(text(),'"+tasknames[i]+"')])[1]")).click();
						driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
						driver.findElement(By.xpath("(//a[ contains(text(),'"+tasknames[i]+"')])[2]")).click();
						driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
						wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='btn dropdown-label ng-binding' and contains(text(),'Actions')]")));
						driver.findElement(By.xpath("//div[@class='btn dropdown-label ng-binding' and contains(text(),'Actions')]")).click();
						driver.findElement(By.xpath("//a[contains(text(),'Run Task')]")).click();
						wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[@class='ecs-header-text ng-binding' and contains(text(),'Run Task')]")));
						// Take Screenshot
						String RunTask_TC002_REG_WP2=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
						TakeScreenShot.saveScreenShot(RunTask_TC002_REG_WP2, driver);
						utilityFileWriteOP.writeToLog(TC_no, "Run Task Screen", "Opens!!!",Resultpath);
						logger.log(LogStatus.PASS, "Run Task Screen is displayed.");
						logger.log(LogStatus.PASS,logger.addScreenCapture(RunTask_TC002_REG_WP2));
						Thread.sleep(2000);
						driver.findElement(By.xpath("//span[contains(text(),'Switch to launch type')]")).click();
						Thread.sleep(4000);
						driver.findElement(By.xpath("//div[@class='ecs-table-row-content ng-isolate-scope' and @items='clustersDropDown']")).click();
						utilityFileWriteOP.writeToLog(TC_no,"Click on Cluster Drop Down","List Expanded",Resultpath);
						Thread.sleep(2000);
						// Take Screenshot
						
						String ClusterDropDown_TC002_REG_WP2=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
						TakeScreenShot.saveScreenShot(ClusterDropDown_TC002_REG_WP2, driver);
						WebElement element = driver.findElement(By.xpath("//a[contains(text(),'nonprod-ECS-ECSCluster-W68V9ZHCS399')]"));
						JavascriptExecutor executor = (JavascriptExecutor)driver;
						executor.executeScript("arguments[0].click();", element);
						System.out.println("Selected cluster");
						utilityFileWriteOP.writeToLog(TC_no,"nonprod-ECS-ECSCluster-W68V9ZHCS399","option selected",Resultpath,xwpfrun);
//						logger.log(LogStatus.PASS, "nonprod-ECS-ECSCluster-W68V9ZHCS399 option selected");
//						logger.log(LogStatus.PASS,logger.addScreenCapture(ClusterDropDown_TC002_REG_WP2));
						Thread.sleep(2000);
						// Take Screenshot
						String ClusterSelection_TC002_REG_WP2=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
						TakeScreenShot.saveScreenShot(ClusterSelection_TC002_REG_WP2, driver);
						WebElement element1 = driver.findElement(By.xpath("//span[contains(text(),'Run Task')]"));
						executor.executeScript("arguments[0].click();", element1);
						System.out.println("Clicked Run Task");
						String TaskTriggered=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
						TakeScreenShot.saveScreenShot(TaskTriggered, driver);
						utilityFileWriteOP.writeToLog(TC_no, "RunTask Button clicked", "Passed!!!",Resultpath,xwpfrun);
//						logger.log(LogStatus.PASS, "RunTask Button clicked");
//						logger.log(LogStatus.PASS,logger.addScreenCapture(TaskTriggered));
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[contains(text(),'Created tasks successfully')]")));
						//  Take Screenshot
						String TaskSuccess=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
						TakeScreenShot.saveScreenShot(TaskSuccess, driver);
						System.out.println("Transfer Request task Triggered Successfully");
//						logger.log(LogStatus.PASS, "Transfer Request task Triggered Successfully");
//						logger.log(LogStatus.PASS,logger.addScreenCapture(TaskSuccess));
						Thread.sleep(1000);
					}
					
					
					utilityFileWriteOP.writeToLog(TC_no, "Transfer Request task triggered succesfully, moving to previous tab", "Passed!!!",Resultpath,xwpfrun);   
					
					res=true; 		
					
					
					return res;
				}
				
				else
				{
					
					
					String SearchInputBox=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
					TakeScreenShot.saveScreenShot(SearchInputBox, driver);
//					logger.log(LogStatus.FAIL, "Search Input box not displayed");
//					logger.log(LogStatus.FAIL,logger.addScreenCapture(SearchInputBox));
					res =false;
					
					throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					
					
				}

			}
			
			
			
			
			catch (Exception e) 
			{	
				e.printStackTrace();
				res=false;
				System.out.println("The error is "+e);
				String Webelementnotfound=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
				TakeScreenShot.saveScreenShot(Webelementnotfound, driver);
				logger.log(LogStatus.FAIL, "Search Input box not displayed");
				logger.log(LogStatus.FAIL,logger.addScreenCapture(Webelementnotfound));
				utilityFileWriteOP.writeToLog(TC_no, "Problem ocuurred for "+e, "Triggering Daily Load",Resultpath);
				return res;
			}

			finally 
			{
				return res;
			}


		}

	}













	public static boolean validateSQS(WebDriver driver, WebDriverWait wait,	String TC_no, String SQSsearchkey,String OrderID, String s3username, String s3password, String resultpath, XWPFRun xwpfRun, ExtentTest logger) {
		boolean res= false;
		try{
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='search-box-input']")));
			boolean imgres1=driver.findElement(By.xpath("//input[@id='search-box-input']")).isDisplayed();
			if(imgres1)
			{
				utilityFileWriteOP.writeToLog(TC_no, "AWS Services Page", "Found!!!",resultpath);
				// Take Screenshot
				String AWSServicePage_WP1=resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
				TakeScreenShot.saveScreenShot(AWSServicePage_WP1, driver);
				//Enter Lambda keyword in the search window
				driver.findElement(By.xpath("//input[@id='search-box-input']")).sendKeys("SQS");
				utilityFileWriteOP.writeToLog(TC_no, "SQS Keyword", "Entered!!!",resultpath,xwpfRun);
				logger.log(LogStatus.PASS, "SQS Keyword is Entered!!!");
				logger.log(LogStatus.PASS, logger.addScreenCapture(AWSServicePage_WP1));
				driver.findElement(By.xpath("//span[@class='awsui-select-option-filtering-match-highlight' and contains(text(), 'SQS')]")).click();
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(text(),'Create New Queue')]")));
				driver.findElement(By.xpath("//input[@class='gwt-TextBox']")).sendKeys(SQSsearchkey);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[contains(text(),'"+SQSsearchkey+"')]")));
				
				driver.findElement(By.xpath("//div[contains(text(),'"+SQSsearchkey+"')]")).click();
				driver.findElement(By.xpath("//button[contains(text(),'Queue Actions')]")).click();
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[@id='gwt-uid-209']")));
				driver.findElement(By.xpath("//td[@id='gwt-uid-209']")).click();
				String AWSServicePage_WP2=resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
				TakeScreenShot.saveScreenShot(AWSServicePage_WP2, driver);
				utilityFileWriteOP.writeToLog(TC_no, "SQS search Keyword", "Entered!!!",resultpath,xwpfRun);
				logger.log(LogStatus.PASS, "SQS search Keyword is Entered!!!");
				logger.log(LogStatus.PASS, logger.addScreenCapture(AWSServicePage_WP2));
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(text(),'Start Polling for Messages')]")));
				//Start Polling for Messages
				for(int i=0;i<10;i++){
					driver.findElement(By.xpath("//button[contains(text(),'Start Polling for Messages')]")).click();
//					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='gwt-Label GDRRJ23CHWE'][contains(text(),'100%')]")));
					Wait<WebDriver> wait2 = new FluentWait<WebDriver>(driver)
						    .withTimeout(60, TimeUnit.SECONDS)
						    .pollingEvery(10, TimeUnit.SECONDS)
						    .ignoring(NoSuchElementException.class);
					 wait2.until(new ExpectedCondition<Boolean>() {
					        @Override
					        public Boolean apply(WebDriver webDriver) {
					            try {
					                final WebElement webElement = webDriver.findElement(By.xpath("//div[@class='gwt-Label GDRRJ23CHWE']"));
					                return webElement.getText().equals("100%");
					            } catch (final StaleElementReferenceException e) {
					                return false;
					            }
					        }
					    });
					String pollingPercent=driver.findElement(By.xpath("//div[@class='gwt-Label GDRRJ23CHWE']")).getText();
					if(pollingPercent.contains("100%")){
						try{
							List<WebElement> Messages=driver.findElements(By.xpath("((//table[@class='GDRRJ23CFR'])[2]/tbody)[1]/tr"));
							if(Messages.size()<1){
								
								continue;
							}
							AWSServicePage_WP2=resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
							TakeScreenShot.saveScreenShot(AWSServicePage_WP2, driver);
							logger.log(LogStatus.INFO, "Total number SQS messages:"+Messages.size());
							logger.log(LogStatus.INFO, logger.addScreenCapture(AWSServicePage_WP2));
							String rwString="((//table[@class='GDRRJ23CFR'])[2]/tbody)[1]/tr",temprwString;
							System.out.println("Total number SQS messages:"+Messages.size());
							for(int lstelemnt=1;lstelemnt<=Messages.size();lstelemnt++){
								temprwString="("+rwString+"["+lstelemnt+"]//td)[9]//button";
								driver.findElement(By.xpath(temprwString)).click();
								wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(text(),'Message Details')]")));
								String MessageBody=driver.findElement(By.xpath("//pre[@class='GDRRJ23CGDF']")).getText();
								if(MessageBody.contains(OrderID) && MessageBody.contains("retail") && MessageBody.contains("create")){
									res=true;
									driver.findElement(By.xpath("//button[contains(@class,'GDRRJ23CHDF')]")).click();
									break;
								}else{
									res=false;
									driver.findElement(By.xpath("//button[contains(@class,'GDRRJ23CHDF')]")).click();
								}
								
							}
							/*if(res==false)
								continue;
							else */if(res=true){
								
								try{
									driver.findElement(By.xpath("//button[contains(@class,'GDRRJ23CHDF')]")).click();
								}catch(Exception e){}
								driver.findElement(By.xpath("//button[@class='GDRRJ23CFE GDRRJ23CD GDRRJ23CO GDRRJ23CL'][contains(text(),'Close')]")).click();
								break;
							}
//							if(res==true){
//								break;
//							}
							return res;
						}catch(Exception e){
							e.printStackTrace();
							res=false;
							return res;
						}
					}
				}
				try{
					driver.findElement(By.xpath("//button[contains(@class,'GDRRJ23CHDF')]")).click();
				}catch(Exception e){}
				try{
					driver.findElement(By.xpath("//button[@class='GDRRJ23CFE GDRRJ23CD GDRRJ23CO GDRRJ23CL'][contains(text(),'Close')]")).click();
				}catch(Exception e){}
			}
		}catch(Exception e){
			e.printStackTrace();
			res=false;
			return res;
		}
		try{
		driver.findElement(By.xpath("//div[@id='nav-logo']")).click();
		}catch(Exception e){}
		return res;
		
	}	
	
	public static boolean GetCloudWatchLogsError(WebDriver driver, WebDriverWait wait,String taskName,  String tcid,String ResultPath, XWPFRun xwpfRun)
	{
		boolean res=false;
		
	try
	{	
		driver.findElement(By.xpath("//div[contains(text(),'Services')]")).click();
		utilityFileWriteOP.writeToLog(tcid, "Clicked On services", "Passed!!!",ResultPath,xwpfRun,"");
		driver.findElement(By.xpath("(//span[contains(text(),'CloudWatch')])[1]")).click();
		utilityFileWriteOP.writeToLog(tcid, "Clicked On CloudWatch", "Passed!!!",ResultPath,xwpfRun,"");

		driver.findElement(By.xpath("//*[@id='gwt-debug-logsLink']")).click();
		utilityFileWriteOP.writeToLog(tcid, "Clicked On Logs", "Passed!!!",ResultPath,xwpfRun,"");
		
		driver.findElement(By.xpath("//input[@id='gwt-debug-prefixFilterTextBox']")).sendKeys(taskName);
		driver.findElement(By.xpath("//input[@id='gwt-debug-prefixFilterTextBox']")).sendKeys(Keys.ENTER);
		Thread.sleep(3000);
		driver.findElement(By.xpath("//a[contains(text(),'"+taskName+"')]")).click();
		
		driver.findElement(By.xpath("//*[@id='gwt-debug-dataTable']/tbody[1]/tr[1]/td[2]/div/a")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("(//div[@class='awsui-radio-button-styled-button'])[2]")).click();
		String ErrorText=driver.findElement(By.xpath("//div[@class='cwdb-log-viewer-table-body']")).getText();
		System.out.println(ErrorText);
			String AfterLogin=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			
			TakeScreenShot.saveScreenShot(AfterLogin, driver);
			utilityFileWriteOP.writeToLog(tcid, "Error found in Cloud watch logs", " ",ResultPath,xwpfRun);
			utilityFileWriteOP.writeToLog(tcid, "Error found in Cloud watch logs", "Error is "+ ErrorText,ResultPath,xwpfRun,"");
			
			Thread.sleep(5000);
			res=true;
	
	}
	catch(Exception e)
	{
		e.printStackTrace();
		String AfterLogin=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin, driver);
		res=false;
		utilityFileWriteOP.writeToLog(tcid, "A problem occurred while navigating through AWS S3", "Due to "+e,ResultPath);
	}
	finally
	{
		return res;
	}
}














	public static boolean validateSQScustomizedfields(WebDriver driver, WebDriverWait wait,	String TC_no, String SQSsearchkey,ArrayList<String> fieldstobevalidate, String s3username, String s3password, String resultpath, XWPFRun xwpfRun, ExtentTest logger) {
		boolean res= false;
		try{
			driver.findElement(By.xpath("//div[@id='nav-logo']")).click();
			
			//Waiting for AWS Services Page
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='search-box-input']")));
			boolean imgres1=driver.findElement(By.xpath("//input[@id='search-box-input']")).isDisplayed();
			if(imgres1)
			{
				utilityFileWriteOP.writeToLog(TC_no, "AWS Services Page", "Found!!!",resultpath);
				// Take Screenshot
				String AWSServicePage_WP1=resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
				TakeScreenShot.saveScreenShot(AWSServicePage_WP1, driver);
				//Enter Lambda keyword in the search window
				driver.findElement(By.xpath("//input[@id='search-box-input']")).sendKeys("SQS");
				utilityFileWriteOP.writeToLog(TC_no, "SQS Keyword", "Entered!!!",resultpath,xwpfRun);
				logger.log(LogStatus.PASS, "SQS Keyword is Entered!!!");
				logger.log(LogStatus.PASS, logger.addScreenCapture(AWSServicePage_WP1));
				driver.findElement(By.xpath("//span[@class='awsui-select-option-filtering-match-highlight' and contains(text(), 'SQS')]")).click();
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(text(),'Create New Queue')]")));
				driver.findElement(By.xpath("//input[@class='gwt-TextBox']")).sendKeys(SQSsearchkey);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[contains(text(),'"+SQSsearchkey+"')]")));
				
				driver.findElement(By.xpath("//div[contains(text(),'"+SQSsearchkey+"')]")).click();
				driver.findElement(By.xpath("//button[contains(text(),'Queue Actions')]")).click();
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[@id='gwt-uid-209']")));
				driver.findElement(By.xpath("//td[@id='gwt-uid-209']")).click();
				String AWSServicePage_WP2=resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
				TakeScreenShot.saveScreenShot(AWSServicePage_WP2, driver);
				utilityFileWriteOP.writeToLog(TC_no, "SQS search Keyword", "Entered!!!",resultpath,xwpfRun);
				logger.log(LogStatus.PASS, "SQS search Keyword is Entered!!!");
				logger.log(LogStatus.PASS, logger.addScreenCapture(AWSServicePage_WP2));
				
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(text(),'Start Polling for Messages')]")));
				//Start Polling for Messages
				for(int i=0;i<5;i++){
					driver.findElement(By.xpath("//button[contains(text(),'Start Polling for Messages')]")).click();
					Thread.sleep(2000);
					
					try{
						String MainWindow=driver.getWindowHandle();
						Alert alert = driver.switchTo().alert();
						String alertMessage= driver.switchTo().alert().getText();
						AWSServicePage_WP1=resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
						
						logger.log(LogStatus.INFO, "Alert message displayed: "+alertMessage);
						
						alert.accept();
						
						driver.switchTo().window(MainWindow);
					}catch(Exception e){}
//					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='gwt-Label GDRRJ23CHWE'][contains(text(),'100%')]")));
					Wait<WebDriver> wait2 = new FluentWait<WebDriver>(driver)
						    .withTimeout(60, TimeUnit.SECONDS)
						    .pollingEvery(10, TimeUnit.SECONDS)
						    .ignoring(NoSuchElementException.class);
					 wait2.until(new ExpectedCondition<Boolean>() {
					        @Override
					        public Boolean apply(WebDriver webDriver) {
					            try {
					                final WebElement webElement = webDriver.findElement(By.xpath("//div[@class='gwt-Label GDRRJ23CHWE']"));
					                return webElement.getText().equals("100%");
					            } catch (final StaleElementReferenceException e) {
					                return false;
					            }
					        }
					    });
					String pollingPercent=driver.findElement(By.xpath("//div[@class='gwt-Label GDRRJ23CHWE']")).getText();
					if(pollingPercent.contains("100%")){
						try{
							List<WebElement> Messages=driver.findElements(By.xpath("((//table[@class='GDRRJ23CFR'])[2]/tbody)[1]/tr"));
							if(Messages.size()<1){
								
								continue;
							}
							AWSServicePage_WP2=resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
							TakeScreenShot.saveScreenShot(AWSServicePage_WP2, driver);
							logger.log(LogStatus.INFO, "Total number SQS messages:"+Messages.size());
							logger.log(LogStatus.INFO, logger.addScreenCapture(AWSServicePage_WP2));
							String rwString="((//table[@class='GDRRJ23CFR'])[2]/tbody)[1]/tr",temprwString;
							System.out.println("Total number SQS messages:"+Messages.size());
							for(int lstelemnt=1;lstelemnt<=Messages.size();lstelemnt++){
								temprwString="("+rwString+"["+lstelemnt+"]//td)[9]//button";
								driver.findElement(By.xpath(temprwString)).click();
								Thread.sleep(2000);
								/*try{
									driver.findElement(By.xpath(temprwString)).click();
								}catch(Exception e){
									temprwString=rwString+"["+lstelemnt+"]";
									driver.findElement(By.xpath(temprwString)).click();
								}*/
								
								System.out.println("temprwString: "+temprwString);
								wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(text(),'Message Details')]")));
								String MessageBody=driver.findElement(By.xpath("//pre[@class='GDRRJ23CGDF']")).getText();
								AWSServicePage_WP2=resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
								TakeScreenShot.saveScreenShot(AWSServicePage_WP2, driver);
								logger.log(LogStatus.INFO, "Displayed SQS message: "+MessageBody);
								logger.log(LogStatus.INFO, logger.addScreenCapture(AWSServicePage_WP2));
								for(String fieldValue:fieldstobevalidate){
									System.out.println("Expected: "+fieldValue);
									System.out.println("MessageBody: "+MessageBody);
									
									if(MessageBody.contains(fieldValue)){
										res=true;
										logger.log(LogStatus.PASS, fieldValue+" is present in SQS message");
//										driver.findElement(By.xpath("//button[contains(@class,'GDRRJ23CHDF')]")).click();
										
									}else{
										res=false;
										break;
//										driver.findElement(By.xpath("//button[contains(@class,'GDRRJ23CHDF')]")).click();
									}
									
									/*try{
										driver.findElement(By.xpath("//button[contains(@class,'GDRRJ23CHDF')]")).click();
										
									}catch(Exception e){}*/
									
								}
/*								if(res){
									break;
								}*/
								if(res==false)
									continue;
								else if(res=true){
									
									try{
										driver.findElement(By.xpath("//button[contains(@class,'GDRRJ23CHDF')]")).click();
									}catch(Exception e){}
									try{
										driver.findElement(By.xpath("//button[@class='GDRRJ23CFE GDRRJ23CD GDRRJ23CO GDRRJ23CL'][contains(text(),'Close')]")).click();
									}catch(Exception e){}
									
									break;
								}
							}
							if(res==false){
								logger.log(LogStatus.WARNING, fieldstobevalidate.get(0)+" is not present in SQS message");
							}
							try{
								driver.findElement(By.xpath("//button[contains(@class,'GDRRJ23CHDF')]")).click();
							}catch(Exception e){}
							try{
							driver.findElement(By.xpath("//button[@class='GDRRJ23CFE GDRRJ23CD GDRRJ23CO GDRRJ23CL'][contains(text(),'Close')]")).click();
							}catch(Exception e){}
//							if(res==true){
//								break;
//							}
							return res;
						}catch(Exception e){
							e.printStackTrace();
							res=false;
							return res;
						}
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			res=false;
			return res;
		}
		driver.findElement(By.xpath("//div[@id='nav-logo']")).click();
		return res;
		
	}	
	
	public static boolean UploadFile(WebDriver driver, WebDriverWait wait,String FilePath, String Filename,String searchKey, String tcid,String ResultPath, XWPFRun xwpfRun, ExtentTest logger)
	{
		boolean res=false;
		
	try
	{	

		driver.findElement(By.xpath("//input[@placeholder='Search for buckets' and @type='text']")).sendKeys(searchKey);
//		utilityFileWriteOP.writeToLog(tcid, "Search field is populated with ", "Keyword "+searchKey,ResultPath,xwpfRun,"");
		
		String awsDynamoDB_sc1=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(awsDynamoDB_sc1, driver);
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(@class,'list-view-item-name') and text()='"+searchKey+"']")));
		utilityFileWriteOP.writeToLog(tcid, "Link related to keyword "+searchKey, "Appeared!!!",ResultPath,xwpfRun);
		logger.log(LogStatus.PASS, "Link related to keyword "+searchKey+" is displayed");
		logger.log(LogStatus.PASS, logger.addScreenCapture(awsDynamoDB_sc1));
		
		String awsDynamoDB_sc2=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(awsDynamoDB_sc2, driver);
		
		driver.findElement(By.xpath("//a[contains(@class,'list-view-item-name') and text()='"+searchKey+"']")).click();
		utilityFileWriteOP.writeToLog(tcid, "Link related to keyword "+searchKey, "Clicked!!!",ResultPath,xwpfRun);
		logger.log(LogStatus.PASS, "Link related to keyword "+searchKey+" is Clicked");
		logger.log(LogStatus.PASS, logger.addScreenCapture(awsDynamoDB_sc2));
		System.out.println("within upload file");
		
		//ErithAutomationProjectDemo\\test_files
		driver.findElement(By.xpath("(//span[contains(text(),'Upload')])[1]")).click();
		driver.findElement(By.xpath("//input[@id='uploadInput']")).sendKeys(FilePath+Filename);
		driver.findElement(By.xpath("(//span[contains(text(),'Upload')])[2]")).click();
		
		Thread.sleep(2000);
		
		String AfterLogin=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
	    TakeScreenShot.saveScreenShot(AfterLogin, driver);
		logger.log(LogStatus.PASS, "Uploaded File Name is " + Filename);
		logger.log(LogStatus.INFO,logger.addScreenCapture(AfterLogin));
		Utilities.utilityFileWriteOP.writeToLog(tcid, " File Uploaded", "File Name is " + Filename,ResultPath,xwpfRun);
		
		Thread.sleep(2000);
		
		res=true;
		
		
	}
	catch(Exception e)
	{
		e.printStackTrace();
		Thread.sleep(2000);
		String AfterLogin=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
	    TakeScreenShot.saveScreenShot(AfterLogin, driver);
		res=false;
		Utilities.utilityFileWriteOP.writeToLog(tcid, "A problem occurred while uploading file", "Due to "+e,ResultPath,xwpfRun);
		logger.log(LogStatus.PASS, "A problem occurred while uploading file due to "+e);
		logger.log(LogStatus.INFO,logger.addScreenCapture(AfterLogin));
	}
	
	finally
	{
		return res;
	}
}


	public static boolean Ecs_task_runner_job_trigger_test(WebDriver driver,WebDriverWait wait, String TC_no, String searchkey, String taskname , String Resultpath , String Screenshotpath,XWPFRun xwpfrun, ExtentTest logger)
	{
		{

			boolean res=false;
			try{
				//Click on AWS button
				
				
				Resultpath=System.getProperty("user.dir")+"/"+Resultpath;
				
				
				driver.findElement(By.xpath("//div[@id='nav-logo']")).click();
				
				//Waiting for AWS Services Page
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='search-box-input']")));
				
				if(!(driver.findElement(By.xpath("//input[@id='search-box-input']")).isDisplayed())){
					String SearchInputBox=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
					TakeScreenShot.saveScreenShot(SearchInputBox, driver);
//					logger.log(LogStatus.FAIL, "Search Input box not displayed");
//					logger.log(LogStatus.FAIL,logger.addScreenCapture(SearchInputBox));
					res =false;
					System.out.println("NOT Found");
					throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					
				}else
					System.out.println("Found");
				
				boolean imgres1=driver.findElement(By.xpath("//input[@id='search-box-input']")).isDisplayed();
				System.out.println(imgres1);
				Thread.sleep(10000);
				if(imgres1)
				{

					//utilityFileWriteOP.writeToLog(TC_no, "AWS Services Page", "Found!!!",Resultpath,xwpfrun);

					// Take Screenshot
					String AWSServicePage_WP1=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
					TakeScreenShot.saveScreenShot(AWSServicePage_WP1, driver);
					
					driver.findElement(By.xpath("//input[@id='search-box-input']")).sendKeys("ECS");
					utilityFileWriteOP.writeToLog(TC_no, "ECS Keyword", "Entered!!!",Resultpath);
//					logger.log(LogStatus.PASS, "Lambda Keyword is Entered!!!");
//					logger.log(LogStatus.PASS, logger.addScreenCapture(AWSServicePage_WP1));
					driver.findElement(By.xpath("//span[@class='awsui-select-option-filtering-match-highlight' and contains(text(), 'ECS')]")).click();
					utilityFileWriteOP.writeToLog(TC_no, "ECS Option ", "Clicked!!!",Resultpath);
					//Waiting for ECS Functions Search Page
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(text(),'Task Definitions')]")));
					utilityFileWriteOP.writeToLog(TC_no, "ECS Search Page", "Found!!!",Resultpath);
					// Take Screenshot
					String LambdaFnSearch_WP1=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
					TakeScreenShot.saveScreenShot(LambdaFnSearch_WP1, driver);
					//loop for the number of jobs to run
					
					
					
					
					

						//click on task definitions				
						driver.findElement(By.xpath("//a[contains(text(),'Task Definitions')]")).click();
						Thread.sleep(2000);
						wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[@class='ecs-header-text ng-binding' and contains(text(),'Task Definitions')]")));

						driver.findElement(By.xpath("//span[@class='awsui-select-value' and contains(text(),'50')]")).click();
						WebElement element2 = driver.findElement(By.xpath("//li[@title='100' and contains(text(),'100')]"));
						JavascriptExecutor executor2 = (JavascriptExecutor)driver;
						executor2.executeScript("arguments[0].click();", element2);		
						//loop
						boolean taskvisible =false;

						driver.findElement(By.xpath("//input[@placeholder ='Filter in this page' and @type='text']")).sendKeys("catalogue-alignment-engine-AWSTask-sit");

						utilityFileWriteOP.writeToLog(TC_no, "Task name: ", "Entered!!!",Resultpath);
//						logger.log(LogStatus.INFO, "Task name: "+tasknames[i]+" Entered!!!");

						int count=0;

//						driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
						Thread.sleep(10000);

						while(taskvisible!=true){

							try{



								taskvisible= driver.findElement(By.xpath("//a[contains(text(),'catalogue-alignment-engine-AWSTask-sit')]")).isDisplayed();


							}



							catch(Exception e){
								System.out.println("Visibilty of task"+taskvisible);
								driver.findElement(By.xpath("//i[@class='awsui-icon angle-right']")).click();

								count++;
								if(count>15)
									break;
							}



						}
						System.out.println("task found");
						driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
						driver.findElement(By.xpath("//a[ contains(text(),'catalogue-alignment-engine-AWSTask-sit'])")).click();
						driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
						driver.findElement(By.xpath("(//a[ contains(text(),'catalogue-alignment-engine-AWSTask-sit')])[1]")).click();
						driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
						wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='btn dropdown-label ng-binding' and contains(text(),'Actions')]")));
						driver.findElement(By.xpath("//div[@class='btn dropdown-label ng-binding' and contains(text(),'Actions')]")).click();
						driver.findElement(By.xpath("//a[contains(text(),'Run Task')]")).click();
						wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[@class='ecs-header-text ng-binding' and contains(text(),'Run Task')]")));
						// Take Screenshot
						String RunTask_TC002_REG_WP2=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
						TakeScreenShot.saveScreenShot(RunTask_TC002_REG_WP2, driver);
						utilityFileWriteOP.writeToLog(TC_no, "Run Task Screen", "Opens!!!",Resultpath);
						logger.log(LogStatus.PASS, "Run Task Screen is displayed.");
						logger.log(LogStatus.PASS,logger.addScreenCapture(RunTask_TC002_REG_WP2));
						Thread.sleep(2000);
						driver.findElement(By.xpath("//div[@class='ecs-table-row-content ng-isolate-scope' and @items='clustersDropDown']")).click();
						utilityFileWriteOP.writeToLog(TC_no,"Click on Cluster Drop Down","List Expanded",Resultpath);
						Thread.sleep(2000);
						// Take Screenshot
						String ClusterDropDown_TC002_REG_WP2=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
						TakeScreenShot.saveScreenShot(ClusterDropDown_TC002_REG_WP2, driver);
						WebElement element = driver.findElement(By.xpath("//a[contains(text(),'ecs-euw1-n-wholesale-vpc001-general-001')]"));
						JavascriptExecutor executor = (JavascriptExecutor)driver;
						executor.executeScript("arguments[0].click();", element);
						System.out.println("Selected cluster");
						utilityFileWriteOP.writeToLog(TC_no,"nonprod-ECS-ECSCluster-W68V9ZHCS399","option selected",Resultpath,xwpfrun);
//						logger.log(LogStatus.PASS, "nonprod-ECS-ECSCluster-W68V9ZHCS399 option selected");
//						logger.log(LogStatus.PASS,logger.addScreenCapture(ClusterDropDown_TC002_REG_WP2));
						Thread.sleep(2000);
						// Take Screenshot
						String ClusterSelection_TC002_REG_WP2=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
						TakeScreenShot.saveScreenShot(ClusterSelection_TC002_REG_WP2, driver);
						WebElement element1 = driver.findElement(By.xpath("//span[contains(text(),'Run Task')]"));
						executor.executeScript("arguments[0].click();", element1);
						System.out.println("Clicked Run Task");
						String TaskTriggered=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
						TakeScreenShot.saveScreenShot(TaskTriggered, driver);
						utilityFileWriteOP.writeToLog(TC_no, "RunTask Button clicked", "Passed!!!",Resultpath,xwpfrun);
//						logger.log(LogStatus.PASS, "RunTask Button clicked");
//						logger.log(LogStatus.PASS,logger.addScreenCapture(TaskTriggered));
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[contains(text(),'Created tasks successfully')]")));
						//  Take Screenshot
						String TaskSuccess=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
						TakeScreenShot.saveScreenShot(TaskSuccess, driver);
						System.out.println("Transfer Request task Triggered Successfully");
//						logger.log(LogStatus.PASS, "Transfer Request task Triggered Successfully");
//						logger.log(LogStatus.PASS,logger.addScreenCapture(TaskSuccess));
						Thread.sleep(1000);
					
					
					
					utilityFileWriteOP.writeToLog(TC_no, "Transfer Request task triggered succesfully, moving to previous tab", "Passed!!!",Resultpath,xwpfrun);   
					
					res=true; 		
					
					
					return res;
				}
				
				else
				{
					
					
					String SearchInputBox=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
					TakeScreenShot.saveScreenShot(SearchInputBox, driver);
//					logger.log(LogStatus.FAIL, "Search Input box not displayed");
//					logger.log(LogStatus.FAIL,logger.addScreenCapture(SearchInputBox));
					res =false;
					
					throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					
					
				}

			}
			
			
			
			
			catch (Exception e) 
			{	
				e.printStackTrace();
				res=false;
				System.out.println("The error is "+e);
				String Webelementnotfound=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
				TakeScreenShot.saveScreenShot(Webelementnotfound, driver);
				logger.log(LogStatus.FAIL, "Search Input box not displayed");
				logger.log(LogStatus.FAIL,logger.addScreenCapture(Webelementnotfound));
				utilityFileWriteOP.writeToLog(TC_no, "Problem ocuurred for "+e, "Triggering Daily Load",Resultpath);
				return res;
			}

			finally 
			{
				return res;
			}


		}

	}
	/*
	 * Method Name: Ecs_task_runner_job_trigger_new
	 * Functionality Description:Function To trigger ECS task in AWS
	 * Created on:08/01/2020
	 * Created By:Morrisons
	 */

	public static boolean Ecs_task_runner_job_trigger_new(WebDriver driver,WebDriverWait wait, String TC_no,  String Resultpath , String Screenshotpath,XWPFRun xwpfrun, ExtentTest logger)
	{
		
			boolean res=false;
			try{
				
				Resultpath=System.getProperty("user.dir")+"/"+Resultpath;
				//Waiting for AWS Services Page
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='search-box-input']")));
					driver.findElement(By.xpath("//input[@id='search-box-input']")).sendKeys("ECS");
					utilityFileWriteOP.writeToLog(TC_no, "ECS Keyword", "Entered!!!",Resultpath,xwpfrun);

					driver.findElement(By.xpath("//span[@class='awsui-select-option-filtering-match-highlight' and contains(text(), 'ECS')]")).click();
					utilityFileWriteOP.writeToLog(TC_no, "ECS Option ", "Clicked!!!",Resultpath,xwpfrun);
					//Waiting for ECS Functions Search Page
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(text(),'Task Definitions')]")));
					utilityFileWriteOP.writeToLog(TC_no, "ECS Search Page", "Found!!!",Resultpath,xwpfrun);
					// Take Screenshot
					String LambdaFnSearch_WP1=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
					TakeScreenShot.saveScreenShot(LambdaFnSearch_WP1, driver);
					//loop for the number of jobs to run
					
						//click on task definitions				
						driver.findElement(By.xpath("//a[contains(text(),'Task Definitions')]")).click();
						
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='awsui-textfield-0']")));
						driver.findElement(By.xpath("//input[@id='awsui-textfield-0']")).click();
						Thread.sleep(500);
						driver.findElement(By.xpath("//input[@id='awsui-textfield-0']")).sendKeys("catalogue-alignment-engine-AWSTask-sit");
							
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(text(),'catalogue-alignment-engine-AWSTask-sit')]")));
						
						driver.findElement(By.xpath("//a[contains(text(),'catalogue-alignment-engine-AWSTask-sit')]")).click();
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(text(),'catalogue-alignment-engine-AWSTask-sit')]")));
						
						driver.findElement(By.xpath("(//a[contains(text(),'catalogue-alignment-engine-AWSTask-sit')])[1]")).click();
						Thread.sleep(500);
						
						driver.findElement(By.xpath("(//a[contains(text(),'catalogue-alignment-engine-AWSTask-sit')])[1]")).click();
						
						
						wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='btn dropdown-label ng-binding' and contains(text(),'Actions')]")));
						driver.findElement(By.xpath("//div[@class='btn dropdown-label ng-binding' and contains(text(),'Actions')]")).click();
						Thread.sleep(2000);
						driver.findElement(By.xpath("//a[contains(text(),'Run Task')]")).click();
						wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[@class='ecs-header-text ng-binding' and contains(text(),'Run Task')]")));
					
						driver.findElement(By.xpath("//div[@class='awsui-control-group-control']//span[@class='ecs-capacity-provider-switch-type ng-binding ng-scope']")).click();
						String RunTask_TC002_REG_WP2=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
						TakeScreenShot.saveScreenShot(RunTask_TC002_REG_WP2, driver);
						utilityFileWriteOP.writeToLog(TC_no, "Run Task Screen", "Opens!!!",Resultpath,xwpfrun);
						logger.log(LogStatus.PASS, "Run Task Screen is displayed.");
						logger.log(LogStatus.PASS,logger.addScreenCapture(RunTask_TC002_REG_WP2));
						Thread.sleep(2000);
						driver.findElement(By.xpath("//div[@class='ecs-table-row-content ng-isolate-scope' and @items='clustersDropDown']")).click();
						utilityFileWriteOP.writeToLog(TC_no,"Click on Cluster Drop Down","List Expanded",Resultpath,xwpfrun);
						Thread.sleep(2000);
						// Take Screenshot
						String ClusterDropDown_TC002_REG_WP2=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
						TakeScreenShot.saveScreenShot(ClusterDropDown_TC002_REG_WP2, driver);
						WebElement element = driver.findElement(By.xpath("//a[contains(text(),'nonprod-ECS-ECSCluster-W68V9ZHCS399')]"));
						JavascriptExecutor executor = (JavascriptExecutor)driver;
						executor.executeScript("arguments[0].click();", element);
						System.out.println("Selected cluster");
						utilityFileWriteOP.writeToLog(TC_no,"nonprod-ECS-ECSCluster-W68V9ZHCS399","option selected",Resultpath,xwpfrun);
//						
						Thread.sleep(2000);
						// Take Screenshot
						String ClusterSelection_TC002_REG_WP2=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
						TakeScreenShot.saveScreenShot(ClusterSelection_TC002_REG_WP2, driver);
						Robot robot =new Robot();
						robot.keyPress(KeyEvent.VK_PAGE_DOWN);
						WebElement element1 = driver.findElement(By.xpath("//span[contains(text(),'Run Task')]"));
						executor.executeScript("arguments[0].click();", element1);
						System.out.println("Clicked Run Task");
						String TaskTriggered=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
						TakeScreenShot.saveScreenShot(TaskTriggered, driver);
						utilityFileWriteOP.writeToLog(TC_no, "RunTask Button clicked", "Passed!!!",Resultpath,xwpfrun);
//						logger.log(LogStatus.PASS, "RunTask Button clicked");
//						logger.log(LogStatus.PASS,logger.addScreenCapture(TaskTriggered));
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[contains(text(),'Created tasks successfully')]")));
						//  Take Screenshot
						String TaskSuccess=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
						TakeScreenShot.saveScreenShot(TaskSuccess, driver);
						System.out.println("Transfer Request task Triggered Successfully");
//						logger.log(LogStatus.PASS, "Transfer Request task Triggered Successfully");
//						logger.log(LogStatus.PASS,logger.addScreenCapture(TaskSuccess));
						Thread.sleep(1000);
						if(driver.findElement(By.xpath("//div[contains(text(),'Created tasks successfully')]")).isDisplayed()){
							res=true;
						}else
							res=false;
						
			    	}catch(Exception e){
			    		e.printStackTrace();
			    		res=false;
			    	}finally{
			    		return res;
			    	}
			    }

	
	
	
}
