package Additional_Functions;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.opencsv.CSVIterator;
import com.opencsv.CSVParser;
import com.opencsv.CSVReader;
import com.relevantcodes.extentreports.ExtentTest;

public class ReadItemCatalogue {
	
	static String[] columnnames=null;
	static Map<String, String> mapforsingleitem = new HashMap<String, String>();
	
	/*
	 * Method Name: readcatalogue
	 * Functionality Description:Map to read the catalogue header and value against them
	 * Created on:08/01/2020
	 * Created By:Morrisons
	 */
	
	public static Map<String, String> readCatalogue(String filePath, String item) throws IOException
	{
	File file = new File(filePath);
	FileReader csvFile = new FileReader(file);
	CSVReader csvreader = new CSVReader(csvFile);
	boolean eof= false;
	int counter =0 ;
	int mincolumnindex= 0;
	while(!eof)
	{
//		System.out.println("counter: "+counter);
		if(counter == 0)
		{
		columnnames = csvreader.readNext();
		for(String l : columnnames)
		{
			System.out.println(l);
			if(l.equals("maps:MIN"))
			{
				break;
			}
			mincolumnindex++;
		}
		
		}
		else
		{
			String[] line= csvreader.readNext();
			if(line[mincolumnindex].contains(item))
			{
				for(int count=0;count<line.length;count++)
				{
					mapforsingleitem.put(columnnames[count], line[count]);
					System.out.println(columnnames[count] +" : "+line[count]);
				}
				eof=true;
			}
			
		}
		counter++;
	}
	csvreader.close();
	csvFile.close();
	
	
	return mapforsingleitem;
	}
	
	
	/*
	 * Method Name: readExportCatalogue
	 * Functionality Description:Map to read the catalogue Export header and value against them
	 * Created on:08/01/2020
	 * Created By:Morrisons
	 */
	
	public static Map<String, String> readExportCatalogue(String filePath, String item) throws IOException
	{
	File file = new File(filePath);
	FileReader csvFile = new FileReader(file);
	CSVReader csvreader = new CSVReader(csvFile);
	boolean eof= false;
	int counter =0 ;
	int mincolumnindex= 0;
	while(!eof)
	{
//		System.out.println("counter: "+counter);
		if(counter == 0)
		{
		columnnames = csvreader.readNext();
		for(String l : columnnames)
		{
			System.out.println(l);
			if(l.equals("maps:PIN"))
			{
				break;
			}
			mincolumnindex++;
		}
		
		}
		else
		{
			String[] line= csvreader.readNext();
			if(line[mincolumnindex].contains(item))
			{
				for(int count=0;count<columnnames.length;count++)
				{
					mapforsingleitem.put(columnnames[count], line[count]);
					System.out.println(columnnames[count] +" : "+line[count]);
				}
				eof=true;
			}
			
		}
		counter++;
	}
	csvreader.close();
	csvFile.close();
	
	
	return mapforsingleitem;
	}
	
	
	
	/*
	 * Method Name: readPromoCatalogue
	 * Functionality Description:Map to read the Promotion catalogue header and value against them
	 * Created on:08/01/2020
	 * Created By:Morrisons
	 */
	
	public static Map<String, String> readPromoCatalogue(String filePath, String item) throws IOException
	{
	File file = new File(filePath);
	FileReader csvFile = new FileReader(file);
	CSVReader csvreader = new CSVReader(csvFile);
	boolean eof= false;
	int counter =0 ;
	int mincolumnindex= 0;
	while(!eof)
	{
		System.out.println("counter: "+counter);
		if(counter == 0)
		{
		columnnames = csvreader.readNext();
		for(String l : columnnames)
		{
			System.out.println(l);
			if(l.equals("id:PIN"))
			{
				break;
			}
			mincolumnindex++;
		}
		
		}
		else
		{
			String[] line= csvreader.readNext();
			if(line[mincolumnindex].contains(item))
			{
				for(int count=0;count<line.length;count++)
				{
					mapforsingleitem.put(columnnames[count], line[count]);
					System.out.println(columnnames[count] +" : "+line[count]);
				}
				eof=true;
			}
			
		}
		counter++;
	}
	csvreader.close();
	csvFile.close();
	
	
	return mapforsingleitem;
	}
	
	
	
	
	/*
	 * Method Name: readBulkCatalogueHeader
	 * Functionality Description:Bulk Catalogue Header validation
	 * Created on:08/01/2020
	 * Created By:Morrisons
	 */

	
	public static boolean readBulkCatalogueHeader(WebDriver driver, WebDriverWait wait, String tcid,String errorfilepath, String Headermessage,String ResultPath,XWPFRun xwpfRun, ExtentTest logger) throws IOException
	{
		boolean result = false;
	File file = new File(errorfilepath);
	FileReader csvFile = new FileReader(file);
	CSVReader csvreader = new CSVReader(csvFile);
	boolean eof= false;
	try {
		outerloop : while(!eof)
		{
			columnnames = csvreader.readNext();
			for(String l : columnnames)
			{
				System.out.println(l);
				if(l.contains(Headermessage))
				{
					System.out.println("File contain data with PROPER ERROR details");
					result = true;
					break outerloop;
				}
			}
			
		}
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	csvreader.close();
	csvFile.close();
	
	
	return result;
	
	}
	
	
	/*
	 * Method Name: mapComparator
	 * Functionality Description:Map compare between two files or data match with portal
	 * Created on:08/01/2020
	 * Created By:Morrisons
	 */
	public static boolean mapComparator(HashMap<String,String> src, HashMap<String, String> dest) throws IOException
	{
		boolean res=false;
		Iterator<HashMap.Entry<String, String>> itr = src.entrySet().iterator(); 
		  
        while(itr.hasNext()) 
        { 
             Map.Entry<String, String> entry = itr.next(); 
             System.out.println("Source Key = " + entry.getKey() +  
                                 ",Source Value = " + entry.getValue()); 
             Iterator<HashMap.Entry<String, String>> itrDest = src.entrySet().iterator();
             while(itrDest.hasNext()) 
             { 
            	 
            	 Map.Entry<String, String> entryDest = itr.next(); 
            	 if(entryDest.getKey().contains("maps:MIN") && entry.getKey().contains("maps:MIN")){
            		if(!(entryDest.getValue().contains(entry.getValue().trim())))
            			continue;
            	 }else
            		 continue;
            	 System.out.println("Destination Key = " + entryDest.getKey() +  
                         ",Destination Value = " + entryDest.getValue());
            	 
             }
        } 
		return res;
		
	}
	
}
