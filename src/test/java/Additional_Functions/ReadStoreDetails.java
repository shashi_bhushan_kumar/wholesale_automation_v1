package Additional_Functions;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.opencsv.CSVIterator;
import com.opencsv.CSVParser;
import com.opencsv.CSVReader;
import com.relevantcodes.extentreports.ExtentTest;

public class ReadStoreDetails {
	
	static String[] columnnames=null;
	static Map<String, String> mapforsinglestore = new HashMap<String, String>();
	
	
	public static Map<String,HashMap<String,String>> amazonReadPromoCSV(String filePath, String itemid) throws IOException
	{
		HashMap<String,HashMap<String,String>> amazonPromoCSVComp= new HashMap<String,HashMap<String,String >>();
		HashMap<String,String> amazonPromoCSVCompTemp= new HashMap<String,String>();
		File file = new File(filePath);
		FileReader csvFile = new FileReader(file);
		CSVReader csvreader = new CSVReader(csvFile);
		boolean eof= false;
		int counter =0 ;
		int mincolumnindex= 0;
		while(!eof)
		{
			System.out.println("counter: "+counter);
			if(counter == 0)
			{
			columnnames = csvreader.readNext();
			for(String l : columnnames)
			{
				System.out.println(l);
				if(l.equals("id:PIN"))
				{
					break;
				}
				mincolumnindex++;
			}
			
			}
			else
			{
				String[] line= csvreader.readNext();
				if(line[mincolumnindex].contains(itemid))
				{
					System.out.println("line.length: "+line.length);
					for(int count=0;count<columnnames.length;count++)
					{
						amazonPromoCSVCompTemp.put(columnnames[count].trim(), line[count].trim());
						amazonPromoCSVComp.put(itemid, amazonPromoCSVCompTemp);
						System.out.println("Contains Item ID: "+columnnames[count] +" : "+line[count]);
					}
					
					eof=true;
				}
				
			}
			counter++;
		}
		csvreader.close();
		csvFile.close();
		
		return amazonPromoCSVComp;
	}
	
	public static Map<String,HashMap<String,String>> amazonReadPromoReportCSV(String filePath, String itemid) throws IOException
	{
		HashMap<String,HashMap<String,String>> amazonPromoCSVComp= new HashMap<String,HashMap<String,String >>();
		HashMap<String,String> amazonPromoCSVCompTemp= new HashMap<String,String>();
		File file = new File(filePath);
		FileReader csvFile = new FileReader(file);
		CSVReader csvreader = new CSVReader(csvFile);
		boolean eof= false;
		int counter =0 ;
		int mincolumnindex= 0;
		while(!eof)
		{
			System.out.println("counter: "+counter);
			if(counter == 0)
			{
			columnnames = csvreader.readNext();
			for(String l : columnnames)
			{
				System.out.println(l);
				if(l.equals(" Item ID"))
				{
					break;
				}
				mincolumnindex++;
			}
			
			}
			else
			{
				String[] line= csvreader.readNext();
				if(line[mincolumnindex].contains(itemid))
				{
					System.out.println("line.length: "+line.length);
					String price="";
					String range="";
					for(int count=0;count<columnnames.length;count++)
						
					{	
						
						if(columnnames[count].contains(" Item ID")){
							amazonPromoCSVCompTemp.put("id:PIN", line[count]);
							
						}
						if(columnnames[count].contains("Customer")){
							amazonPromoCSVCompTemp.put("customer", line[count]);
						}
						if(columnnames[count].contains("Price(PWSP)")){
							price=line[count];
						}
						if(columnnames[count].contains("Range")){
							range=line[count];
							if(range.contains("ENG")){
								amazonPromoCSVCompTemp.put("prices:PWSPENG:GBP", price);
							}else if(range.contains("SCO")){
								amazonPromoCSVCompTemp.put("prices:PWSPSCO:GBP", price);
							}else if(range.contains("WAL")){
								amazonPromoCSVCompTemp.put("prices:PWSPWAL:GBP", price);
							}
						}
						if(columnnames[count].contains("Start Date")){
							amazonPromoCSVCompTemp.put("START_DATE", line[count]);
						}
						if(columnnames[count].contains("End Date")){
							amazonPromoCSVCompTemp.put("END_DATE", line[count]);
						}
					}
					
					eof=true;
				}
				
			}
			counter++;
		}
		csvreader.close();
		csvFile.close();
		amazonPromoCSVComp.put(itemid, amazonPromoCSVCompTemp);
		System.out.println("amazonPromoCSVCompTemp :"+amazonPromoCSVCompTemp);
		System.out.println("amazonPromoCSVComp :"+amazonPromoCSVComp);
		return amazonPromoCSVComp;
	}
	
	static Map<String, String> readStoreDtl(String filePath, String storeid) throws IOException
	{
	File file = new File(filePath);
	FileReader csvFile = new FileReader(file);
	CSVReader csvreader = new CSVReader(csvFile);
	boolean eof= false;
	int counter =0 ;
	int mincolumnindex= 0;
	while(!eof)
	{
		System.out.println("counter: "+counter);
		if(counter == 0)
		{
		columnnames = csvreader.readNext();
		for(String l : columnnames)
		{
			System.out.println(l);
			if(l.equals("STOREID"))
			{
				break;
			}
			mincolumnindex++;
		}
		
		}
		else
		{
			String[] line= csvreader.readNext();
			if(line[mincolumnindex].contains(storeid))
			{
				System.out.println("line.length: "+line.length);
				for(int count=0;count<columnnames.length;count++)
				{
					mapforsinglestore.put(columnnames[count], line[count]);
					System.out.println("Contains Store ID: "+columnnames[count] +" : "+line[count]);
				}
				eof=true;
			}
			
		}
		counter++;
	}
	csvreader.close();
	csvFile.close();
	
	
	return mapforsinglestore;
	}
	

	static Map<String, String> readPromoItemDtl(String filePath, String itemid) throws IOException
	{
	File file = new File(filePath);
	FileReader csvFile = new FileReader(file);
	CSVReader csvreader = new CSVReader(csvFile);
	boolean eof= false;
	int counter =0 ;
	int mincolumnindex= 0;
	while(!eof)
	{
		System.out.println("counter: "+counter);
		if(counter == 0)
		{
		columnnames = csvreader.readNext();
		for(String l : columnnames)
		{
			System.out.println(l);
			if(l.equals("id:PIN"))
			{
				break;
			}
			mincolumnindex++;
		}
		
		}
		else
		{
			String[] line= csvreader.readNext();
			if(line[mincolumnindex].contains(itemid))
			{
				System.out.println("line.length: "+line.length);
				for(int count=0;count<columnnames.length;count++)
				{
					mapforsinglestore.put(columnnames[count], line[count]);
					System.out.println("Contains Item ID: "+columnnames[count] +" : "+line[count]);
				}
				eof=true;
			}
			
		}
		counter++;
	}
	csvreader.close();
	csvFile.close();
	
	
	return mapforsinglestore;
	}
	

	static Map<String, String> readOtherPromoItemDtl(String filePath, String itemid) throws IOException
	{
	File file = new File(filePath);
	FileReader csvFile = new FileReader(file);
	CSVReader csvreader = new CSVReader(csvFile);
	boolean eof= false;
	int counter =0 ;
	int mincolumnindex= 0;
	while(!eof)
	{
		System.out.println("counter: "+counter);
		if(counter == 0)
		{
		columnnames = csvreader.readNext();
		for(String l : columnnames)
		{
			System.out.println(l);
			if(l.equals(" Item ID"))
			{
				break;
			}
			mincolumnindex++;
		}
		
		}
		else
		{
			String[] line= csvreader.readNext();
			if(line[mincolumnindex].contains(itemid))
			{
				System.out.println("line.length: "+line.length);
//				for(int count=0;count<line.length;count++)
				for(int count=0;count<columnnames.length;count++)
				{
					mapforsinglestore.put(columnnames[count].trim(), line[count].trim());
					System.out.println("Contains Item ID: "+columnnames[count] +" : "+line[count]);
				}
				eof=true;
			}
			
		}
		counter++;
	}
	csvreader.close();
	csvFile.close();
	
	
	return mapforsinglestore;
	}
	
	
	
	public static boolean readBulkStoreHeader(WebDriver driver, WebDriverWait wait, String tcid,String errorfilepath, String Headermessage,String ResultPath,XWPFRun xwpfRun, ExtentTest logger) throws IOException
	{
		boolean result = false;
	File file = new File(errorfilepath);
	FileReader csvFile = new FileReader(file);
	CSVReader csvreader = new CSVReader(csvFile);
	boolean eof= false;
	try {
		outerloop : while(!eof)
		{
			columnnames = csvreader.readNext();
			for(String l : columnnames)
			{
				System.out.println(l);
				if(l.contains(Headermessage))
				{
					System.out.println("File contain data with PROPER ERROR details");
					result = true;
					break outerloop;
				}
			}
			
		}
	} catch (Exception e) {
		// TODO Auto-generated catch block
		//e.printStackTrace();
	}
	csvreader.close();
	csvFile.close();
	
	
	return result;
	}
	
	
	public static String lastStoreFile(String directorypath,String customer) {
		File dir = new File(directorypath);
		File[] files = dir.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.contains(customer);
				
			}
		});
		long lastMod = Long.MIN_VALUE;
		String choice = null;
		for (File file : files) {
			if (file.lastModified() > lastMod) {
				choice = file.getName();
				lastMod = file.lastModified();
			}
		}
		return choice;
	}
	
	
	
}
