package Additional_Functions;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.time.temporal.ChronoUnit;

import Utilities.*;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;






































import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.poi.ss.formula.functions.Index;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import freemarker.template.utility.DateUtil;

public class WholeSale_Upload_Functions {
	
	

	
	public static boolean upload_Promotion_AllCustomer(WebDriver driver, WebDriverWait wait, String tcid,String customer,String errorfilepath,String filepath,String ResultPath,XWPFRun xwpfRun, ExtentTest logger){
	    	boolean res=false;
	    	
	    	try{
	    		
	    		ResultPath=System.getProperty("user.dir")+"/"+ResultPath;

	    		driver.findElement(By.xpath("//img[@alt='"+customer+"']")).click();
	    		Thread.sleep(5000);
	    		
	    		logger.log(LogStatus.PASS, "Sales order page is displayed for :"+customer+"");
  				String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
  				TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
  				utilityFileWriteOP.writeToLog(tcid, "Required customer is", "clicked!!!",ResultPath,xwpfRun);
  				logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc5));
				
  				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//img[@src='app/images/IconCatalogueManagement.svg']")));
				driver.findElement(By.xpath("//img[@src='app/images/IconCatalogueManagement.svg']")).click();
				
				logger.log(LogStatus.PASS, "Catalogue management icon is clicked in sales order page of customer :"+customer);
  				String wholesale_sc6=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
  				TakeScreenShot.saveScreenShot(wholesale_sc6, driver);
  				utilityFileWriteOP.writeToLog(tcid, "clicked on ", "Catalogue management icon!!!",ResultPath,xwpfRun);
  				logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc6));
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h5[contains(text(),'Upload Promotions')]")));
				driver.findElement(By.xpath("//h5[contains(text(),'Upload Promotions')]")).click();
				
				logger.log(LogStatus.PASS, "Upload Promotionns  is clicked for customer :"+customer);
  				String wholesale_sc7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
  				TakeScreenShot.saveScreenShot(wholesale_sc7, driver);
  				utilityFileWriteOP.writeToLog(tcid, "Upload Promotionns  is clicked for customer :"+customer, "PASS",ResultPath,xwpfRun);
  				logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc7));
				
				
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h4[contains(text(),'uploaded promotion files')]")));
			
				
				if(driver.findElement(By.xpath("//button[contains(text(),'Upload new Promotion file')]")).isDisplayed())
				{
		 WebElement Cat=driver.findElement(By.xpath("//button[contains(text(),'Upload new Promotion file')]"));
		       Cat.click();
		       
		       Thread.sleep(2000);
		       	String wholesale_sc4=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc4, driver);
				logger.log(LogStatus.PASS, "Upload new Promtion");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc4));
				utilityFileWriteOP.writeToLog(tcid, "Upload new Promtion", "PASS",ResultPath,xwpfRun);
		       
				}
				
				
		        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='fullRangeUploadFrm']/ng2-file-input/div/div[2]/button")));
		        System.out.println("Filepath :"+errorfilepath);
		        
		        WebElement upload=driver.findElement(By.xpath("//div[@class='ng2-file-input']/input[@type='file']"));
		        upload.sendKeys(errorfilepath);
		        Thread.sleep(2000);
		        logger.log(LogStatus.PASS, "Error message is displayed if user trying to upload files other than .CSV");
  				String wholesale_sc15=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
  				TakeScreenShot.saveScreenShot(wholesale_sc15, driver);
  				utilityFileWriteOP.writeToLog(tcid, "Promotion file is not uploaded if file is not .CSV ", "PASS",ResultPath,xwpfRun);
  				logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc15));
				
  				try{
  				Thread.sleep(2000);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'Please select CSV File only')]")));
  				if(driver.findElement(By.xpath("//span[contains(text(),'Please select CSV File only')]")).isDisplayed()){
  					System.out.println("INSIDE IF");
  					driver.findElement(By.xpath("//button[contains(text(),'Ok')]")).click();	
				}
	            Thread.sleep(2000);  
				}catch(Exception e){}
		        WebElement upload1=driver.findElement(By.xpath("//div[@class='ng2-file-input']/input[@type='file']"));
		        
		        upload1.sendKeys(filepath);
		        logger.log(LogStatus.PASS, "Now Promotion file with .CSV format uploaded for customer :"+customer);
  				String wholesale_sc100=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
  				TakeScreenShot.saveScreenShot(wholesale_sc100, driver);
  				utilityFileWriteOP.writeToLog(tcid, "Promotion file uploaded !!!", "PASS",ResultPath,xwpfRun);
  				logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc100));
				
		        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@type='button' and text()='Submit']")));	             
	          
	              driver.findElement(By.xpath("//button[@type='button' and text()='Submit']")).click();
	              Thread.sleep(2000);
	              driver.findElement(By.xpath("//*[@id='rangeFileUploadBox']/div/div/div[2]/div/div[2]/button")).click();
	            
					for(int j = 0; j < 32; j++)
					{
						try{
							wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("((//table[@class='table table-bordered table-striped margin-bottom-0']//tr)[2]//td)[5][contains(text(),'Upload Completed')]")));
							break;
						}catch(Exception e)
						{
							driver.navigate().refresh();
//							driver.findElement(By.xpath("//button[@type='submit'][contains(text(),'VIEW')]")).click();
							
						}
					}
					String pattern = "ddMMyyyy";
		            String dateInString =new SimpleDateFormat(pattern).format(new Date());
		            String flnme=driver.findElement(By.xpath("//table[@class='table table-bordered table-striped margin-bottom-0']//tbody/tr/td[2]")).getText();
		            System.out.println("File name in table viewed is"+flnme);
		            if(driver.findElement(By.xpath("//table[@class='table table-bordered table-striped margin-bottom-0']//tbody/tr/td[2]")).getText().contains("-"+customer+"-"+dateInString))
		            {
		            	System.out.println("Promotion file name validated");
		            	res=true;
		            }
		            else
		            {
		            	System.out.println("Promotion file name not validated");
		            	res=false;
		            }
		            Thread.sleep(2000);
	              driver.findElement(By.xpath("((//table[@class='table table-bordered table-striped margin-bottom-0']//tr)[2]//td)[4]")).click();
					
					
					logger.log(LogStatus.PASS, " Newly promotion Store file contain name as : *-"+customer+"-ddmmyyyyyhhmmss.csv is downloaded successfully");
      				String wholesale_sc11=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
      				TakeScreenShot.saveScreenShot(wholesale_sc11, driver);
      				utilityFileWriteOP.writeToLog(tcid, "Newly promotion Store file", "downloaded!!!",ResultPath,xwpfRun);
      				logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc11));
      				
					
		    	}catch(Exception e){
		    		e.printStackTrace();
		    		res=false;
		        		
	    	}finally{
	    		return res;
	    	}
	    }

	

	public static boolean view_Report_Promotion_AllCustomer(String startDate,String endDate,String user,WebDriver driver, WebDriverWait wait, String tcid,String customer,String ResultPath,XWPFRun xwpfRun, ExtentTest logger){
	    	boolean res=false;
	    	
	    	try{
	    		
	    		ResultPath=System.getProperty("user.dir")+"/"+ResultPath;

	    		driver.findElement(By.xpath("//img[@alt='"+customer+"']")).click();
	    		Thread.sleep(5000);
	    		
	    		logger.log(LogStatus.PASS, "Sales order page is displayed for :"+customer+"");
  				String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
  				TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
  				utilityFileWriteOP.writeToLog(tcid, "Required customer is", "clicked!!!",ResultPath,xwpfRun);
  				logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc5));
				
  				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//img[@src='app/images/IconCatalogueManagement.svg']")));
				driver.findElement(By.xpath("//img[@src='app/images/IconCatalogueManagement.svg']")).click();
				
				logger.log(LogStatus.PASS, "Catalogue management icon is clicked in sales order page of customer :"+customer);
  				String wholesale_sc6=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
  				TakeScreenShot.saveScreenShot(wholesale_sc6, driver);
  				utilityFileWriteOP.writeToLog(tcid, "clicked on ", "Catalogue management icon!!!",ResultPath,xwpfRun);
  				logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc6));
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@id='sticky1']//span[text()='Reports']")));
				driver.findElement(By.xpath("//div[@id='sticky1']//span[text()='Reports']")).click();
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@id='search-order-border']//h4[text()='Select Report Type']")));
				logger.log(LogStatus.PASS, "View Report for Uploaded Promotion :"+customer);
  				String wholesale_sc7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
  				TakeScreenShot.saveScreenShot(wholesale_sc7, driver);
  				utilityFileWriteOP.writeToLog(tcid, "View Report for Uploaded Promotion :"+customer, "PASS",ResultPath,xwpfRun);
  				logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc7));
			
  			
		        Select catType= new Select( driver.findElement(By.xpath("//div[@id='search-order-border']//select[@id='catalogueType']")));
		        catType.selectByVisibleText("Promotion Report");
		       
		        Thread.sleep(2000);
		        logger.log(LogStatus.PASS, "Report Type as Promotion selected sucessfully");
  				String wholesale_sc15=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
  				TakeScreenShot.saveScreenShot(wholesale_sc15, driver);
  				utilityFileWriteOP.writeToLog(tcid, "Report Type as Promotion selected sucessfully", "PASS",ResultPath,xwpfRun);
  				logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc15));
  				
		        WebElement requestNewReport=driver.findElement(By.xpath("//div[@id='search-order-border']//button[@id='requestNewReport']"));

		        requestNewReport.click();
		        Thread.sleep(2000);
		        logger.log(LogStatus.PASS, "Request for new Promotion report of customer :"+customer);
  				String wholesale_sc100=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
  				TakeScreenShot.saveScreenShot(wholesale_sc100, driver);
  				utilityFileWriteOP.writeToLog(tcid, "Request for new Promotion report of customer"+customer, "PASS",ResultPath,xwpfRun);
  				logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc100));
  				
  				selectStartDate(startDate,driver,wait,ResultPath,tcid,xwpfRun,logger);
					Thread.sleep(2000);
  				selectEndDate(endDate,driver,wait,ResultPath,tcid,xwpfRun,logger);
  				Thread.sleep(2000);
					logger.log(LogStatus.PASS, " Start Date and End Date is selected successfully");
      				String wholesale_sc11=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
      				TakeScreenShot.saveScreenShot(wholesale_sc11, driver);
      				utilityFileWriteOP.writeToLog(tcid, "Start Date and End Date is selected successfully", "PASS",ResultPath,xwpfRun);
      				logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc11));
      				
      				driver.findElement(By.xpath("//form[@name='dealSearch']//button[@id='adv-search-desk' and contains(text(),'SUBMIT REQUEST')]")).isDisplayed();	
      				driver.findElement(By.xpath("//form[@name='dealSearch']//button[@id='adv-search-desk' and contains(text(),'SUBMIT REQUEST')]")).click();
      				Thread.sleep(1000);
      				driver.findElement(By.xpath("//span[contains(text(),'File will be generated as')]")).isDisplayed();
      				
      				String promofile=driver.findElement(By.xpath("(//div[@class='modal-content'])[3]//div[2]//span")).getText();
      				

      				logger.log(LogStatus.PASS, " File generation request placed successfully ");
      				String wholesale_sc21=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
      				TakeScreenShot.saveScreenShot(wholesale_sc21, driver);
      				utilityFileWriteOP.writeToLog(tcid, "File generation request placed successfully", "PASS",ResultPath,xwpfRun);
      				logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc21));

      				driver.findElement(By.xpath("//h5[contains(text(),'Report')]//button[@id='close-popup']")).isDisplayed();
      				driver.findElement(By.xpath("//h5[contains(text(),'Report')]//button[@id='close-popup']")).click();

      				Thread.sleep(5000);

      				String[] promo=promofile.split("_");
      				
      				System.out.println("First 1"+promo[0]);
      				System.out.println("First 2"+promo[1]);
      				System.out.println("First 3"+promo[2]);
      				
      				
    		        Select catType1= new Select( driver.findElement(By.xpath("//div[@id='search-order-border']//select[@id='catalogueType']")));
    		        catType1.selectByVisibleText("Promotion Report");
    		        Thread.sleep(2000);
    		        
    		        logger.log(LogStatus.PASS, " Selection of the report type for viewing");
      				String wholesale_sc22=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
      				TakeScreenShot.saveScreenShot(wholesale_sc22, driver);
      				utilityFileWriteOP.writeToLog(tcid, "Selection of the report type for viewing", "PASS",ResultPath,xwpfRun);
      				logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc22));
    		        driver.findElement(By.xpath("//div[@id='search-order-border']//button[contains(text(),'VIEW')]")).click();
    		        Thread.sleep(9000);

    		        for(int i=1;i<10;i++){
    		        	
    		        	try{
    		        	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@id='exportable']//tr["+i+"]//td)[2]"))).getText().contains(promo[2]);
    		        	
    		        	  String prm=driver.findElement(By.xpath("(//div[@id='exportable']//tr["+i+"]//td)[2]")).getText();
    		        	
    		        	  System.out.println("Counter is:"+i);
    		        	  System.out.println("String get is :"+prm);
    		        	  System.out.println("String to be find is "+promo[2]);
    		        	
    		        	Thread.sleep(2000);
    		        	  if(prm.contains(promo[2])){
    		        		  System.out.println("in If condition");
    		        		  boolean request=driver.findElement(By.xpath("(//div[@id='exportable']//tr//td)[5][contains(text(),'"+user+"')]")).isDisplayed();
    		        		  driver.findElement(By.xpath("(//div[@id='exportable']//tr//td)[6]")).click();
    							Thread.sleep(12000);
    							logger.log(LogStatus.PASS, " New generated Promotion file downloaded successfully");
    		      				String wholesale_sc19=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
    		      				TakeScreenShot.saveScreenShot(wholesale_sc19, driver);
    		      				utilityFileWriteOP.writeToLog(tcid, "New generated Promotion file downloaded successfull", "PASS",ResultPath,xwpfRun);
    		      				logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc19));
    		      				res=true;
    		        		break;
    		        	}}
    		        	
    		        	 catch(Exception e){
    		        		  System.out.println("Expected file is not displayed or present in iteration "+i);
    		        		  driver.navigate().refresh();
    		        		  Select catType3= new Select( driver.findElement(By.xpath("//div[@id='search-order-border']//select[@id='catalogueType']")));
    		    		        catType3.selectByVisibleText("Promotion Report");
    		    		        Thread.sleep(2000);
    		    		        driver.findElement(By.xpath("//div[@id='search-order-border']//button[contains(text(),'VIEW')]")).click();
    		    		        Thread.sleep(2000);
    		        		 
    		        	  }
    		        	
    		        }
    		        
      				
		    	}catch(Exception e){
		    		e.printStackTrace();
		    		res=false;
		        		
	    	}finally{
	    		return res;
	    	}
	    }

	
	public static boolean selectStartDate(String startDate,WebDriver driver,WebDriverWait wait,String ResultPath,String tcid,XWPFRun xwpfRun, ExtentTest logger)
	{
	       boolean res=false;
	       String PageSc=null;
	                 try{
	                   	         

	              wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//i[@id='dealstartdateinput']")));
	              System.out.println("Calendar Found!!!");
	              driver.findElement(By.xpath("//i[@id='dealstartdateinput']")).click();
	              Thread.sleep(1000);
	             System.out.println("From date sheet"+startDate);
	               String[] start= startDate.split("/");
	               System.out.println("Date in array "+start);System.out.println("after split 0"+start[0]);
	               System.out.println("after split 1"+start[1]);
	               System.out.println("after split 2"+start[2]);
	               
	                     String day=start[0];
	       				String[] monthList={"January","February","March","April","May","June","July","August","September","October","November","December"};
	       				
	       				int monthIndex= Integer.parseInt(start[1]);
	       				
	       				String month=monthList[monthIndex-1];
	       				String year=start[2];
	       				System.out.println("Before Parse"+year);
	       				int y= Integer.parseInt(year);
	       				System.out.println("Year"+y);
	       				System.out.println("The Date is : "+day+" "+month+" "+year);
	       				Thread.sleep(1000);   
	  	              wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//button[contains(@id,'datepicker')])[4]")));
			
	                 System.out.println(" Internal Found!!!");
	                                                  
	                 driver.findElement(By.xpath("(//button[contains(@id,'datepicker')])[4]")).click();
	                 Thread.sleep(1000);
	                 
	                 int yr = Calendar.getInstance().get(Calendar.YEAR);
	                 System.out.println("Calendar Year"+yr);
	                 Thread.sleep(1000);
	                 
	                   
	                 for(int i=0;i<(yr-y);i++)						  
	                	 driver.findElement(By.xpath("//button[@class='btn btn-default btn-sm pull-left']")).click();
	                 System.out.println("Left Pane button");
	                 Thread.sleep(1000);
	                
	                 driver.findElement(By.xpath("//button[contains(@class,'btn btn-default')]/span[text()='"+month+"']")).click();
	                 System.out.println("Month is:"+month);
	                 
	                 Thread.sleep(1000);
	 				try
	 				{
	 					
	 					 System.out.println("TRY1");
	 					 System.out.println("Day is:"+day);
	 					try{
	 						
	 						int index=0;
	 						int dd=Integer.valueOf(day);
	 						/*if(dd==1)
	 						{
	 							index=4;
	 						}
	 						else if((dd>=3) && (dd<=9))
	 						{
	 							index=2;
	 						}
	 						
	 						else if(dd==2  || (dd>=10) || (dd<=31)){
	 							index=1;
	 							
	 						}
	 						*/	WebElement element=driver.findElement(By.xpath("//label[contains(text(),'Start Date')]/../div[2]/datepicker/datepicker-inner/div/daypicker/table/tbody/tr/td/button/span[contains(text(),'"+dd+"')]"));
	 							 
	 		                    JavascriptExecutor executor5 = (JavascriptExecutor)driver;
	 		                    executor5.executeScript("arguments[0].click()", element);
	 		                    Thread.sleep(1000);
	 						}
	 						
	 					catch (Exception e){
	 							
	 							System.out.println("Exception "+e);
	 							
	 						}
	 					 
	                    System.out.println("Click 1");
	                    }
					catch(Exception e)
					{
						e.printStackTrace();
						System.out.println("TRY1 Exception");
						
						
					}
	                 }
	 				catch(Exception e){
	 					e.printStackTrace();
						System.out.println("Main TRY Exception");
	 				}
	          return res;      
	}
	
	

	public static boolean selectEndDate(String endDate,WebDriver driver,WebDriverWait wait,String ResultPath,String tcid,XWPFRun xwpfRun, ExtentTest logger)
	{
	       boolean res=false;
	       String PageSc=null;
	                 try{
	                   	         

	              wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//i[@id='dealstartdateinput']")));
	              System.out.println("Calendar Found!!!");
	              driver.findElement(By.xpath("//i[@id='dealenddateinput']")).click();
	              Thread.sleep(1000);
	             System.out.println("From date sheet"+endDate);
	               String[] end= endDate.split("/");
	               System.out.println("Date in array "+end);System.out.println("after split 0"+end[0]);
	               System.out.println("after split 1"+end[1]);
	               System.out.println("after split 2"+end[2]);
	               
	                     String day=end[0];
	       				String[] monthList={"January","February","March","April","May","June","July","August","September","October","November","December"};
	       				
	       				int monthIndex= Integer.parseInt(end[1]);
	       				
	       				String month=monthList[monthIndex-1];
	       				String year=end[2];
	       				System.out.println("Before Parse"+year);
	       				int y= Integer.parseInt(year);
	       				System.out.println("Year"+y);
	       				System.out.println("The Date is : "+day+" "+month+" "+year);
	       				Thread.sleep(1000);   
	  	              wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//button[contains(@id,'datepicker')])[5]")));
			
	                 System.out.println(" Internal Found!!!");
	                                                  
	                 driver.findElement(By.xpath("(//button[contains(@id,'datepicker')])[5]")).click();
	                 Thread.sleep(2000);
	                 
	                 int yr = Calendar.getInstance().get(Calendar.YEAR);
	                 System.out.println("Calendar Year"+yr);
	                 Thread.sleep(1000);
	                 
	                
	                 for(int i=0;i<(yr-y);i++)						  
	                	 driver.findElement(By.xpath("//button[@class='btn btn-default btn-sm pull-left']")).click();
	                 System.out.println("Left Pane button");
	                 Thread.sleep(1000);
	                
	                 driver.findElement(By.xpath("//button[contains(@class,'btn btn-default')]/span[text()='"+month+"']")).click();
	                 System.out.println("Month is:"+month);
	                 
	                 Thread.sleep(1000);
	 				try
	 				{
	 					
	 					 System.out.println("TRY1");
	 					 System.out.println("Day is:"+day);
	 					try{
	 						int dd=Integer.valueOf(day);
	 						WebElement element=driver.findElement(By.xpath("//label[contains(text(),'End Date')]/../div[2]/datepicker/datepicker-inner/div/daypicker/table/tbody/tr/td/button[@class='btn btn-sm btn-default']/span[contains(text(),'"+dd+"')]"));
	 							 
	 		                    JavascriptExecutor executor5 = (JavascriptExecutor)driver;
	 		                    executor5.executeScript("arguments[0].click()", element);
	 		                    Thread.sleep(1000);
	 		                  
	 		                   
	 		                  WebElement element1= driver.findElement(By.xpath("//i[@id='dealenddateinput']"));
	 							 
	 		                    JavascriptExecutor executor6 = (JavascriptExecutor)driver;
	 		                    executor6.executeScript("arguments[0].click()", element1);
	 		                    Thread.sleep(1000);
	 		                   JavascriptExecutor executor7 = (JavascriptExecutor)driver;
	 		                    executor7.executeScript("arguments[0].click()", element1);
	 						}
	 						
	 					catch (Exception e){
	 							
	 							System.out.println("Exception "+e);
	 							
	 						}
	                    System.out.println("Click 1");
	                    }
					catch(Exception e)
					{
						e.printStackTrace();
						System.out.println("TRY1 Exception");
						
						
					}
	                 }
	 				catch(Exception e){
	 					e.printStackTrace();
						System.out.println("Main TRY Exception");
	 				}
	          return res;      
	}
	
	
	
	
	
	/*
	 * Method Name: promocatalogueitemvalidationamazon
	 * Functionality Description:Catlalogue Item validation with portal and successfully uploaded CSV file
	 * Created on:08/01/2020
	 * Created By:Morrisons
	 */
	public static boolean promocatalogueitemvalidationamazon(WebDriver driver, WebDriverWait wait, String tcid,String filename,String customer,String itemid, String ResultPath,XWPFRun xwpfRun, ExtentTest logger){
		boolean res=true;
		try{
			
			ResultPath=System.getProperty("user.dir")+"/"+ResultPath;
			
			driver.findElement(By.xpath("//img[@src='app/images/IconCatalogueManagement.svg']")).click();
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='csr']")));
			driver.findElement(By.xpath("//*[@id='csr']")).click();

			String wholesale_sc2=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc2, driver);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc2));
			utilityFileWriteOP.writeToLog(tcid, "Catalogue management", "Clicked!!!",ResultPath,xwpfRun);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='search-order-border']/form/div[1]/label")));

			for(int catSelection=1;catSelection<4;catSelection++){
				
				driver.navigate().refresh();
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//select[@name='catalogueType']")));
				
			WebElement selectElement = driver.findElement(By.xpath("//select[@name='catalogueType']"));
			Select select = new Select(selectElement);
//			select.selectByValue(cataloguetype);
			select.selectByIndex(catSelection);

			logger.log(LogStatus.PASS, "Catalogue type selection for customer : '"+customer+"' is suceesful");
			String wholesale_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc3, driver);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc3));
			utilityFileWriteOP.writeToLog(tcid, "Catalogue type ", "selected!!!",ResultPath,xwpfRun);

			
			String[] ItemId = itemid.split(",");

			

			
			//***************************************************


			for(String eachItemid:ItemId) {
				
				driver.findElement(By.xpath("//input[@id='catalogueCode']")).clear();
				driver.findElement(By.xpath("//input[@id='catalogueCode']")).sendKeys(eachItemid);

			
				logger.log(LogStatus.PASS, "Required ProductID is Entered");
				String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
				logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc5));

				Thread.sleep(2000);
				driver.findElement(By.xpath("//button[@class='btn pull-right mrgn-r-2 search-button adv-search-btn form-control']")).click();

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='exportable']/table/tbody/tr[1]/td[1]")));
				driver.findElement(By.xpath("//*[@id='exportable']/table/tbody/tr[1]/td[1]")).click();
				Thread.sleep(2000);
				ArrayList<String> tabs_windows = new ArrayList<String> (driver.getWindowHandles());
				driver.switchTo().window(tabs_windows.get(1));
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h2[contains(text(),'PRODUCT DETAILS')]")));

				Actions action = new Actions(driver);
				WebElement tableElement1=driver.findElement(By.xpath("//h2[contains(text(),'Product Identifiers')]")); 
				action.moveToElement(tableElement1);
				action.perform();
				Thread.sleep(500);
				
				System.out.println("The path to read file is :"+filename);
				Map<String, String> columnVals=Additional_Functions.ReadItemCatalogue.readPromoCatalogue(filename,eachItemid);

				String mapPIN=columnVals.get("id:PIN").toString().trim();
				if((mapPIN.length()>0)){
					String uiPIN=driver.findElement(By.xpath("(//div[@id='itemCodeMapping']//table//span[contains(text(),'PIN')]//ancestor::tr//td)[2]//span")).getText().trim();
					
				/*	
					WebElement pin=driver.findElement(By.xpath("(//div[@id='itemCodeMapping']//table//span[contains(text(),'PIN')]//ancestor::tr//td)[2]//span"));
					JavascriptExecutor js = (JavascriptExecutor) driver;
					js.executeScript("arguments[0].scrollIntoView(true);", pin);*/
					
					JavascriptExecutor js = (JavascriptExecutor) driver;  
					js.executeScript("window.scrollBy(0,100)");
					
					
					if(uiPIN.equals(mapPIN)){
						System.out.println(" PIN PASS");
						logger.log(LogStatus.PASS, "Item number validated successfully for the item:"+eachItemid);
						String wholesale_sc6=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
						TakeScreenShot.saveScreenShot(wholesale_sc6, driver);
						logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc6));
						res=res && true;
					}else{
						System.out.println("PIN FAIL");
						logger.log(LogStatus.FAIL, "Item number validation failed");
						res=res && false;
					}
				}

				if(catSelection==1){

				String mapWSP=columnVals.get("prices:PWSPENG:GBP").toString().trim();
				if((mapWSP.length()>0)){
					String uiWSP=driver.findElement(By.xpath("((//div[@id='itemPriceInfo']//table//span[contains(text(),'WSP')]//ancestor::tr//td)[2]//span)[2]")).getText().trim();

					if(uiWSP.contains(mapWSP) || mapWSP.contains(uiWSP)){
						System.out.println("mapWSP"+mapWSP);

						System.out.println("uiWSP"+uiWSP);
						System.out.println("prices:WSP:GBP PASS");
						logger.log(LogStatus.PASS, "Price change for WSP England validated successfully for the item :"+eachItemid);
						String wholesale_sc6=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
						TakeScreenShot.saveScreenShot(wholesale_sc6, driver);
						logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc6));
						res=res && true;
					}else{
						System.out.println("mapWSP"+mapWSP);

						System.out.println("uiWSP"+uiWSP);
						System.out.println("prices:WSP:GBP FAIL");
						res=res && false;
					}
				}
				}
				else if(catSelection==2){
					String mapSCOWSP=columnVals.get("prices:PWSPSCO:GBP").toString().trim();
					if((mapSCOWSP.length()>0)){
						String uiWSP=driver.findElement(By.xpath("((//div[@id='itemPriceInfo']//table//span[contains(text(),'WSP')]//ancestor::tr//td)[2]//span)[2]")).getText().trim();

						if(uiWSP.contains(mapSCOWSP) || mapSCOWSP.contains(uiWSP)){
							System.out.println("mapWSP"+mapSCOWSP);

							System.out.println("uiWSP"+uiWSP);
							System.out.println("prices:WSPSCO:GBP PASS");
							logger.log(LogStatus.PASS, "Price change for WSP Scotland validated successfully for the item :"+eachItemid);
							String wholesale_sc6=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
							TakeScreenShot.saveScreenShot(wholesale_sc6, driver);
							logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc6));
							res=res && true;
						}else{
							System.out.println("mapWSP"+mapSCOWSP);

							System.out.println("uiWSP"+uiWSP);
							System.out.println("prices:WSPSCO:GBP FAIL");
							res=res && false;
						}
					
				}
								
				}
				
				else{
					String mapWALWSP=columnVals.get("prices:PWSPWAL:GBP").toString().trim();
					if((mapWALWSP.length()>0)){
						String uiWSP=driver.findElement(By.xpath("((//div[@id='itemPriceInfo']//table//span[contains(text(),'WSP')]//ancestor::tr//td)[2]//span)[2]")).getText().trim();

						if(uiWSP.contains(mapWALWSP) || mapWALWSP.contains(uiWSP)){
							System.out.println("mapWSP"+mapWALWSP);

							System.out.println("uiWSP"+uiWSP);
							System.out.println("prices:WSPWAL:GBP PASS");
							logger.log(LogStatus.PASS, "Price change for WSP Wales validated successfully for the item :"+eachItemid);
							String wholesale_sc6=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
							TakeScreenShot.saveScreenShot(wholesale_sc6, driver);
							logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc6));
							res=res && true;
						}else{
							System.out.println("mapWSP"+mapWALWSP);

							System.out.println("uiWSP"+uiWSP);
							System.out.println("prices:WSPWAL:GBP FAIL");
							res=res && false;
						}
				}
				}
				String effDate=columnVals.get("START_DATE").toString().trim();
				String [] effectiveDate=effDate.split("-");
				
				if((effDate.length()>0)){
					String date=driver.findElement(By.xpath("(//div[@id='itemPriceInfo']//table//span[contains(text(),'WSP')]//ancestor::tr//td)[3]//span")).getText().trim();
					String [] dateEff=date.split("/");
					
					WebElement ddmmyy=driver.findElement(By.xpath("(//div[@id='itemPriceInfo']//table//span[contains(text(),'WSP')]//ancestor::tr//td)[3]//span"));
					JavascriptExecutor js = (JavascriptExecutor) driver;
					js.executeScript("arguments[0].scrollIntoView(true);", ddmmyy);
					
//					if(date.contains(effDate) || effDate.contains(date)){
					if(effectiveDate[2].equalsIgnoreCase(dateEff[0]) && (effectiveDate[1].equalsIgnoreCase(dateEff[1])) ){
						System.out.println("effDate"+effDate);

						System.out.println("date"+date);
						System.out.println("date PASS");
						logger.log(LogStatus.PASS, "Promotion effective date validated successfully for the item :"+eachItemid);
						String wholesale_sc6=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
						TakeScreenShot.saveScreenShot(wholesale_sc6, driver);
						logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc6));
						res=res && true;
					}else{
						System.out.println("effDate"+effDate);

						System.out.println("date"+date);
						System.out.println("date FAIL");
						res=res && false;
					}
				}

				driver.close();
				ArrayList<String> tabs_windows1 = new ArrayList<String> (driver.getWindowHandles());
				driver.switchTo().window(tabs_windows1.get(0));
				
				}
			}

			System.out.println("After all if else result is:"+res);

			if(res){
				logger.log(LogStatus.PASS, " Promotion Item details validated sucessfully");
				return res;
				
				
			}
			else{
				
				System.out.println("Item details not validated");
				logger.log(LogStatus.FAIL, "Item details not validated");
				String wholesale_sc15=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc15, driver);
				logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc15));
				return res;
			}

		}catch(Exception e){
			e.printStackTrace();
			res=false;
		}finally{
			return res;
		}
	}
	
	
	
	/*
	 * Method Name: promocatalogueitemvalidationCustomer
	 * Functionality Description:Catlalogue Item validation with portal and successfully uploaded CSV file
	 * Created on:08/01/2020
	 * Created By:Morrisons
	 */
	public static boolean promocatalogueitemvalidationCustomer(WebDriver driver, WebDriverWait wait, String tcid,String filename,String customer,String cataloguetype,String itemid, String ResultPath,XWPFRun xwpfRun, ExtentTest logger){
		boolean res=true;
		try{
			
			ResultPath=System.getProperty("user.dir")+"/"+ResultPath;
			
			String[] ItemId = itemid.split(",");
			
			driver.findElement(By.xpath("//img[@src='app/images/IconCatalogueManagement.svg']")).click();
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='csr']")));
			driver.findElement(By.xpath("//*[@id='csr']")).click();

			String wholesale_sc2=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc2, driver);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc2));
			utilityFileWriteOP.writeToLog(tcid, "Catalogue management", "Clicked!!!",ResultPath,xwpfRun);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='search-order-border']/form/div[1]/label")));

			for(String eachItemid:ItemId) {
			
				
				driver.navigate().refresh();
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//select[@name='catalogueType']")));
				
			WebElement selectElement = driver.findElement(By.xpath("//select[@name='catalogueType']"));
			Select select = new Select(selectElement);
			select.selectByIndex(1);


			logger.log(LogStatus.PASS, "Catalogue type selection for customer : '"+customer+"' is sucessful");
			String wholesale_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc3, driver);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc3));
			utilityFileWriteOP.writeToLog(tcid, "Catalogue type ", "selected!!!",ResultPath,xwpfRun);

				
				driver.findElement(By.xpath("//input[@id='catalogueCode']")).clear();
				driver.findElement(By.xpath("//input[@id='catalogueCode']")).sendKeys(eachItemid);

			
				logger.log(LogStatus.PASS, "Required ProductID is Entered");
				String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
				logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc5));

				Thread.sleep(2000);
				driver.findElement(By.xpath("//button[@class='btn pull-right mrgn-r-2 search-button adv-search-btn form-control']")).click();

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='exportable']/table/tbody/tr[1]/td[1]")));
				driver.findElement(By.xpath("//*[@id='exportable']/table/tbody/tr[1]/td[1]")).click();
				Thread.sleep(2000);
				ArrayList<String> tabs_windows = new ArrayList<String> (driver.getWindowHandles());
				driver.switchTo().window(tabs_windows.get(1));
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h2[contains(text(),'PRODUCT DETAILS')]")));

				Actions action = new Actions(driver);
				WebElement tableElement1=driver.findElement(By.xpath("//h2[contains(text(),'Product Identifiers')]")); 
				action.moveToElement(tableElement1);
				action.perform();
				Thread.sleep(500);
				
				System.out.println("The path to read file is :"+filename);
				Map<String, String> columnVals=Additional_Functions.ReadItemCatalogue.readPromoCatalogue(filename,eachItemid);

				String mapPIN=columnVals.get("id:PIN").toString().trim();
				if((mapPIN.length()>0)){
					String uiPIN=driver.findElement(By.xpath("(//div[@id='itemCodeMapping']//table//span[contains(text(),'PIN')]//ancestor::tr//td)[2]//span")).getText().trim();
					
				/*	
					WebElement pin=driver.findElement(By.xpath("(//div[@id='itemCodeMapping']//table//span[contains(text(),'PIN')]//ancestor::tr//td)[2]//span"));
					JavascriptExecutor js = (JavascriptExecutor) driver;
					js.executeScript("arguments[0].scrollIntoView(true);", pin);*/
					
					JavascriptExecutor js = (JavascriptExecutor) driver;  
					js.executeScript("window.scrollBy(0,100)");
					
					
					if(uiPIN.equals(mapPIN)){
						System.out.println(" PIN PASS");
						logger.log(LogStatus.PASS, "Item number validated successfully  for Item:"+eachItemid);
						String wholesale_sc6=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
						TakeScreenShot.saveScreenShot(wholesale_sc6, driver);
						logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc6));
						res=res && true;
					}else{
						System.out.println("PIN FAIL");
						logger.log(LogStatus.FAIL, "Item number validation failed");
						res=res && false;
					}
				}

				

				String mapWSP=columnVals.get("prices:PWSCP:GBP").toString().trim();
				if((mapWSP.length()>0)){
					String uiWSP=driver.findElement(By.xpath("(//div[@id='itemPriceInfo']//table//span[contains(text(),'WSCP')]//ancestor::tr//td)[2]//span")).getText().trim();

					if(uiWSP.contains(mapWSP) || mapWSP.contains(uiWSP)){
						System.out.println("mapWSP"+mapWSP);

						System.out.println("uiWSP"+uiWSP);
						System.out.println("prices:WSCP:GBP PASS");
						logger.log(LogStatus.PASS, "Price change for WSCP validated successfully for the item :"+eachItemid);
						String wholesale_sc6=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
						TakeScreenShot.saveScreenShot(wholesale_sc6, driver);
						logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc6));
						res=res && true;
					}else{
						System.out.println("mapWSP"+mapWSP);

						System.out.println("uiWSP"+uiWSP);
						System.out.println("prices:WSP:GBP FAIL");
						res=res && false;
					}
				}
				
				
				String effDate=columnVals.get("START_DATE").toString().trim();
				String [] effectiveDate=effDate.split("-");
				
				if((effDate.length()>0)){
					String date=driver.findElement(By.xpath("(//div[@id='itemPriceInfo']//table//span[contains(text(),'WSCP')]//ancestor::tr//td)[3]//span")).getText().trim();
					String [] dateEff=date.split("/");
					
					WebElement ddmmyy=driver.findElement(By.xpath("(//div[@id='itemPriceInfo']//table//span[contains(text(),'WSCP')]//ancestor::tr//td)[3]//span"));
					JavascriptExecutor js = (JavascriptExecutor) driver;
					js.executeScript("arguments[0].scrollIntoView(true);", ddmmyy);
					
//					if(date.contains(effDate) || effDate.contains(date)){
					if(effectiveDate[2].equalsIgnoreCase(dateEff[0]) && (effectiveDate[1].equalsIgnoreCase(dateEff[1])) ){
						System.out.println("effDate"+effDate);

						System.out.println("date"+date);
						System.out.println("date PASS");
						logger.log(LogStatus.PASS, "Promotion effective date validated successfully for the item :"+eachItemid);
						String wholesale_sc6=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
						TakeScreenShot.saveScreenShot(wholesale_sc6, driver);
						logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc6));
						res=res && true;
					}else{
						System.out.println("effDate"+effDate);

						System.out.println("date"+date);
						System.out.println("date FAIL");
						res=res && false;
					}
				}

				driver.close();
				ArrayList<String> tabs_windows1 = new ArrayList<String> (driver.getWindowHandles());
				driver.switchTo().window(tabs_windows1.get(0));
				
				}
		

			System.out.println("After all if else result is:"+res);

			if(res){
				logger.log(LogStatus.PASS, " Promotion Item details validated sucessfully");
				return res;
				
				
			}
			else{
				
				System.out.println("Item details not validated");
				logger.log(LogStatus.FAIL, "Item details not validated");
				String wholesale_sc15=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc15, driver);
				logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc15));
				return res;
			}

		}catch(Exception e){
			e.printStackTrace();
			res=false;
		}finally{
			return res;
		}
	}
	
	
	    
	    
}