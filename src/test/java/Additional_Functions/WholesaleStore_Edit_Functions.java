package Additional_Functions;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.time.temporal.ChronoUnit;

import Utilities.*;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.poi.ss.formula.functions.Index;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import freemarker.template.utility.DateUtil;

public class WholesaleStore_Edit_Functions {

	public static boolean store_Edit_Effective_Immediate(String customer,String storeid,String category,String deliveryDay_0,String editSupplyDepot,WebDriver driver, WebDriverWait wait, String tcid,String ResultPath,XWPFRun xwpfRun, ExtentTest logger){
		boolean res=false;
		try{


			ResultPath=System.getProperty("user.dir")+"/"+ResultPath;

			Thread.sleep(4000);

			String currentWindow=driver.getWindowHandle();
			Set<String> windows=driver.getWindowHandles();

			for(String window:windows)
			{
				if(!window.equals(currentWindow))
				{
					driver.switchTo().window(window);
					//utilityFileWriteOP.writeToLog(tcid, "AWS Console Window", "Switched!!!",ResultPath, xwpfRun,"");
					break;
				}
			}

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@class='edit-text' and contains(text(),'Edit')]")));
			driver.findElement(By.xpath("//span[@class='edit-text' and contains(text(),'Edit')]")).isDisplayed();
			driver.findElement(By.xpath("//span[@class='edit-text' and contains(text(),'Edit')]")).click();
			String wholesale_sc1=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc1, driver);
			logger.log(LogStatus.PASS, "Store details in edit mode displayed");
			logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc1));
			utilityFileWriteOP.writeToLog(tcid, "Store details in edit mode displayed", "PASS",ResultPath,xwpfRun);


			driver.findElement(By.xpath("//label[@for='effectiveImmediatelyId']")).click();
			Thread.sleep(2000);

			String wholesale_sc12=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc12, driver);
			logger.log(LogStatus.PASS, "Make Effective Immediately pop up displayed successfully");
			logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc12));
			utilityFileWriteOP.writeToLog(tcid, "Make Effective Immediately pop up displayed successfully", "PASS",ResultPath,xwpfRun);

			
			driver.findElement(By.xpath("//div[@id='immediatelyModalPageLevel']/div/div/div[2]/div/button[@type='button' and contains(text(),'Yes')]")).click();
			Thread.sleep(1000);
			String wholesale_sc2=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc2, driver);
			logger.log(LogStatus.PASS, "Checked the Make Effective Immediately successfully");
			logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc2));
			utilityFileWriteOP.writeToLog(tcid, "Checked the Make Effective Immediately successfully", "PASS",ResultPath,xwpfRun);


			if(customer.equalsIgnoreCase("amazon")){

				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).click();
				Thread.sleep(2000);
				String wholesale_sc7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc7, driver);
				logger.log(LogStatus.PASS, "Delivery Opportunities details displayed successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc7));
				utilityFileWriteOP.writeToLog(tcid, "Delivery Opportunities details displayed successfully", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//button[@id='addnewButton' and @type='button' and contains(text(),'Add New Row')]")).isDisplayed();
				driver.findElement(By.xpath("//button[@id='addnewButton' and @type='button' and contains(text(),'Add New Row')]")).click();

				driver.findElement(By.xpath("//div[@id='deliveryDaysAddGrpId0']/input")).click();
				driver.findElement(By.xpath("//div[@id='deliveryDaysAddGrpId0']/input")).sendKeys(deliveryDay_0);

				driver.findElement(By.xpath("//h4[contains(text(),'Historical and Future')]")).click();

				String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
				logger.log(LogStatus.PASS, "Delivery Opportunities details details to be update");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc8));
				utilityFileWriteOP.writeToLog(tcid, "Delivery Opportunities details details to be update", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).isDisplayed();
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).click();
				Thread.sleep(2000);

				driver.findElement(By.xpath("//div[contains(text(),'Store details updated successfully')]")).isDisplayed();

				String wholesale_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc9, driver);
				logger.log(LogStatus.PASS, "Store details updated successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc9));
				utilityFileWriteOP.writeToLog(tcid, "Store details updated successfully", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//h5[contains(text(),'Edit Store')]/button[@type='button' and contains(text(),'✖') and @id='close-popup']")).click();

				res=true;
			}

			else{

				String arr[]=category.split(",");

				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).click();
				Thread.sleep(2000);
				String wholesale_sc7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc7, driver);
				logger.log(LogStatus.PASS, "Supplying Depot details displayed successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc7));
				utilityFileWriteOP.writeToLog(tcid, "Supplying Depot details displayed successfully", "PASS",ResultPath,xwpfRun);

				Select depotType = new Select(driver.findElement(By.xpath("//select[@name='supplyDepotType']")));
				depotType.selectByVisibleText(editSupplyDepot);
				Thread.sleep(2000);

				String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
				logger.log(LogStatus.PASS, "Supplying Depot details updated successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc8));
				utilityFileWriteOP.writeToLog(tcid, "Supplying Depot details updated successfully", "PASS",ResultPath,xwpfRun);

				//******Update Delivering Opportunities OPS
				System.out.println("OPS update start");
				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).click();
				Thread.sleep(2000);
				String wholesale_sc11=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc11, driver);
				logger.log(LogStatus.PASS, "Delivery Opportunities details displayed successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc11));
				utilityFileWriteOP.writeToLog(tcid, "Delivery Opportunities details displayed successfully", "PASS",ResultPath,xwpfRun);

				for(int i=0;i<arr.length;i++){

					driver.findElement(By.xpath("//table[@class='custom-borderd-table table table-bordered table-striped table-hover search-result-table text-nowrap viewchanges-table current-opp-table marginzero']//td[contains(text(),'"+arr[i]+"')]")).click();
				
				driver.findElement(By.xpath("//button[@id='addnewButton' and @type='button' and contains(text(),'Add New Row')]")).isDisplayed();
				driver.findElement(By.xpath("//button[@id='addnewButton' and @type='button' and contains(text(),'Add New Row')]")).click();
				
				/*driver.findElement(By.xpath("//div[@id='deliveryDaysAddGrpId0']/input")).click();
				driver.findElement(By.xpath("//div[@id='deliveryDaysAddGrpId0']/input")).sendKeys(deliveryDay_0);*/
				driver.findElement(By.xpath("(//table[@id='deliveryOpportunitiesTable']//td[2])/div/input")).click();
				driver.findElement(By.xpath("(//table[@id='deliveryOpportunitiesTable']//td[2])/div/input")).sendKeys(deliveryDay_0);

				driver.findElement(By.xpath("//h4[contains(text(),'Historical and Future')]")).click();

				String wholesale_sc13=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc13, driver);
				logger.log(LogStatus.PASS, "Delivery Opportunities details details to be update");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc13));
				utilityFileWriteOP.writeToLog(tcid, "Delivery Opportunities details details to be update", "PASS",ResultPath,xwpfRun);
				}
				
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).isDisplayed();
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).click();
				Thread.sleep(2000);

				driver.findElement(By.xpath("//div[contains(text(),'Store details updated successfully')]")).isDisplayed();

				String wholesale_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc9, driver);
				logger.log(LogStatus.PASS, "Store details updated successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc9));
				utilityFileWriteOP.writeToLog(tcid, "Store details updated successfully", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//h5[contains(text(),'Edit Store')]/button[@type='button' and contains(text(),'✖') and @id='close-popup']")).click();
				res=true;

			}


		}

		catch(Exception e){
			e.printStackTrace();
			String wholesale_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc9, driver);
			logger.log(LogStatus.FAIL, "Error occured while store edit effective immediately");
			logger.log(LogStatus.FAIL, logger.addScreenCapture(wholesale_sc9));
			utilityFileWriteOP.writeToLog(tcid, "Error occured while store edit effective immediately", "FAIL",ResultPath,xwpfRun);
			res=false;
		}finally{
			return res;
		}
	}

	public static boolean store_Edit_Without_Eff_Immedt(String customer,String storeid,String category,String deliveryDay_0,String editSupplyDepot,WebDriver driver, WebDriverWait wait, String tcid,String ResultPath,XWPFRun xwpfRun, ExtentTest logger){
		boolean res=false;
		try{


			ResultPath=System.getProperty("user.dir")+"/"+ResultPath;

			Thread.sleep(4000);

			String currentWindow=driver.getWindowHandle();
			Set<String> windows=driver.getWindowHandles();

			for(String window:windows)
			{
				if(!window.equals(currentWindow))
				{
					driver.switchTo().window(window);
					//utilityFileWriteOP.writeToLog(tcid, "AWS Console Window", "Switched!!!",ResultPath, xwpfRun,"");
					break;
				}
			}

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@class='edit-text' and contains(text(),'Edit')]")));
			driver.findElement(By.xpath("//span[@class='edit-text' and contains(text(),'Edit')]")).isDisplayed();
			driver.findElement(By.xpath("//span[@class='edit-text' and contains(text(),'Edit')]")).click();
			String wholesale_sc1=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc1, driver);
			logger.log(LogStatus.PASS, "Store details in edit mode displayed");
			logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc1));
			utilityFileWriteOP.writeToLog(tcid, "Store details in edit mode displayed", "PASS",ResultPath,xwpfRun);

			
			if(customer.equalsIgnoreCase("amazon")){

				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).click();
				Thread.sleep(2000);
				String wholesale_sc7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc7, driver);
				logger.log(LogStatus.PASS, "Delivery Opportunities details displayed successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc7));
				utilityFileWriteOP.writeToLog(tcid, "Delivery Opportunities details displayed successfully", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//button[@id='addnewButton' and @type='button' and contains(text(),'Add New Row')]")).isDisplayed();
				driver.findElement(By.xpath("//button[@id='addnewButton' and @type='button' and contains(text(),'Add New Row')]")).click();

				driver.findElement(By.xpath("//div[@id='deliveryDaysAddGrpId0']/input")).click();
				driver.findElement(By.xpath("//div[@id='deliveryDaysAddGrpId0']/input")).sendKeys(deliveryDay_0);

				driver.findElement(By.xpath("//h4[contains(text(),'Historical and Future')]")).click();

				String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
				logger.log(LogStatus.PASS, "Delivery Opportunities details details to be update");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc8));
				utilityFileWriteOP.writeToLog(tcid, "Delivery Opportunities details details to be update", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).isDisplayed();
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).click();
				Thread.sleep(2000);

				driver.findElement(By.xpath("//div[contains(text(),'Store details updated successfully')]")).isDisplayed();

				String wholesale_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc9, driver);
				logger.log(LogStatus.PASS, "Store details updated successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc9));
				utilityFileWriteOP.writeToLog(tcid, "Store details updated successfully", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//h5[contains(text(),'Edit Store')]/button[@type='button' and contains(text(),'✖') and @id='close-popup']")).click();

				res=true;
			}

			else{

				String arr[]=category.split(",");

				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).click();
				Thread.sleep(2000);
				String wholesale_sc7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc7, driver);
				logger.log(LogStatus.PASS, "Supplying Depot details displayed successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc7));
				utilityFileWriteOP.writeToLog(tcid, "Supplying Depot details displayed successfully", "PASS",ResultPath,xwpfRun);

				Select depotType = new Select(driver.findElement(By.xpath("//select[@name='supplyDepotType']")));
				depotType.selectByVisibleText(editSupplyDepot);
				Thread.sleep(2000);

				String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
				logger.log(LogStatus.PASS, "Supplying Depot details updated successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc8));
				utilityFileWriteOP.writeToLog(tcid, "Supplying Depot details updated successfully", "PASS",ResultPath,xwpfRun);

				//******Update Delivering Opportunities OPS
				System.out.println("OPS update start");
				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).click();
				Thread.sleep(2000);
				String wholesale_sc11=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc11, driver);
				logger.log(LogStatus.PASS, "Delivery Opportunities details displayed successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc11));
				utilityFileWriteOP.writeToLog(tcid, "Delivery Opportunities details displayed successfully", "PASS",ResultPath,xwpfRun);

				for(int i=0;i<arr.length;i++){

					driver.findElement(By.xpath("//table[@class='custom-borderd-table table table-bordered table-striped table-hover search-result-table text-nowrap viewchanges-table current-opp-table marginzero']//td[contains(text(),'"+arr[i]+"')]")).click();
				
				driver.findElement(By.xpath("//button[@id='addnewButton' and @type='button' and contains(text(),'Add New Row')]")).isDisplayed();
				driver.findElement(By.xpath("//button[@id='addnewButton' and @type='button' and contains(text(),'Add New Row')]")).click();
				
				driver.findElement(By.xpath("(//table[@id='deliveryOpportunitiesTable']//td[2])/div/input")).click();
				driver.findElement(By.xpath("(//table[@id='deliveryOpportunitiesTable']//td[2])/div/input")).sendKeys(deliveryDay_0);

				driver.findElement(By.xpath("//h4[contains(text(),'Historical and Future')]")).click();

				String wholesale_sc13=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc13, driver);
				logger.log(LogStatus.PASS, "Delivery Opportunities details details to be update");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc13));
				utilityFileWriteOP.writeToLog(tcid, "Delivery Opportunities details details to be update", "PASS",ResultPath,xwpfRun);
				}
				
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).isDisplayed();
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).click();
				Thread.sleep(2000);

				driver.findElement(By.xpath("//div[contains(text(),'Store details updated successfully')]")).isDisplayed();

				String wholesale_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc9, driver);
				logger.log(LogStatus.PASS, "Store details updated successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc9));
				utilityFileWriteOP.writeToLog(tcid, "Store details updated successfully", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//h5[contains(text(),'Edit Store')]/button[@type='button' and contains(text(),'✖') and @id='close-popup']")).click();
				res=true;

			}


		}

		catch(Exception e){
			e.printStackTrace();
			String wholesale_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc9, driver);
			logger.log(LogStatus.FAIL, "Error occured while store edit without effective immediately");
			logger.log(LogStatus.FAIL, logger.addScreenCapture(wholesale_sc9));
			utilityFileWriteOP.writeToLog(tcid, "Error occured while store edit without effective immediately", "FAIL",ResultPath,xwpfRun);
			res=false;
		}finally{
			return res;
		}
	}

	

	public static boolean store_Edit_Supply_Depot(String storeid,String editSupplyDepot,WebDriver driver, WebDriverWait wait, String tcid,String ResultPath,XWPFRun xwpfRun, ExtentTest logger){
		boolean res=false;
		try{

			ResultPath=System.getProperty("user.dir")+"/"+ResultPath;

			Thread.sleep(4000);

			String currentWindow=driver.getWindowHandle();
			Set<String> windows=driver.getWindowHandles();

			for(String window:windows)
			{
				if(!window.equals(currentWindow))
				{
					driver.switchTo().window(window);
					//utilityFileWriteOP.writeToLog(tcid, "AWS Console Window", "Switched!!!",ResultPath, xwpfRun,"");
					break;
				}
			}

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@class='edit-text' and contains(text(),'Edit')]")));
			driver.findElement(By.xpath("//span[@class='edit-text' and contains(text(),'Edit')]")).isDisplayed();
			driver.findElement(By.xpath("//span[@class='edit-text' and contains(text(),'Edit')]")).click();
			String wholesale_sc1=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc1, driver);
			logger.log(LogStatus.PASS, "Store details in edit mode displayed");
			logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc1));
			utilityFileWriteOP.writeToLog(tcid, "Store details in edit mode displayed", "PASS",ResultPath,xwpfRun);


			driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).isDisplayed();
			driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).click();
			Thread.sleep(2000);
			String wholesale_sc7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc7, driver);
			logger.log(LogStatus.PASS, "Supplying Depot details displayed successfully");
			logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc7));
			utilityFileWriteOP.writeToLog(tcid, "Supplying Depot details displayed successfully", "PASS",ResultPath,xwpfRun);


			Select depotType = new Select(driver.findElement(By.xpath("//select[@name='supplyDepotType']")));
			depotType.selectByVisibleText(editSupplyDepot);
			Thread.sleep(2000);

			String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
			logger.log(LogStatus.PASS, "Supplying Depot details updated successfully");
			logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc8));
			utilityFileWriteOP.writeToLog(tcid, "Supplying Depot details updated successfully", "PASS",ResultPath,xwpfRun);

			driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).isDisplayed();
			driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).click();
			Thread.sleep(2000);

			driver.findElement(By.xpath("//div[contains(text(),'Store details updated successfully')]")).isDisplayed();

			String wholesale_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc9, driver);
			logger.log(LogStatus.PASS, "Store details updated successfully");
			logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc9));
			utilityFileWriteOP.writeToLog(tcid, "Store details updated successfully", "PASS",ResultPath,xwpfRun);
			driver.findElement(By.xpath("//h5[contains(text(),'Edit Store')]/button[@type='button' and contains(text(),'✖') and @id='close-popup']")).click();

			res=true;

		}

		catch(Exception e){
			e.printStackTrace();
			String wholesale_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc9, driver);
			logger.log(LogStatus.FAIL, "Error occured while store creation");
			logger.log(LogStatus.FAIL, logger.addScreenCapture(wholesale_sc9));
			utilityFileWriteOP.writeToLog(tcid, "Error occured while store creation", "FAIL",ResultPath,xwpfRun);
			res=false;
		}finally{
			return res;
		}

	}


	public static boolean store_Validate_SuppDepot_Day2(String storeid,String editSupplyDepot,WebDriver driver, WebDriverWait wait, String tcid,String ResultPath,XWPFRun xwpfRun, ExtentTest logger){
		boolean res=false;
		try{

			ResultPath=System.getProperty("user.dir")+"/"+ResultPath;

			Thread.sleep(4000);

			String currentWindow=driver.getWindowHandle();
			Set<String> windows=driver.getWindowHandles();

			for(String window:windows)
			{
				if(!window.equals(currentWindow))
				{
					driver.switchTo().window(window);
					//utilityFileWriteOP.writeToLog(tcid, "AWS Console Window", "Switched!!!",ResultPath, xwpfRun,"");
					break;
				}
			}

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@class='edit-text' and contains(text(),'Edit')]")));
			driver.findElement(By.xpath("//span[@class='edit-text' and contains(text(),'Edit')]")).isDisplayed();

			driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).isDisplayed();
			driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).click();
			Thread.sleep(2000);
			String wholesale_sc7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc7, driver);
			logger.log(LogStatus.PASS, "Supplying Depot details displayed successfully");
			logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc7));
			utilityFileWriteOP.writeToLog(tcid, "Supplying Depot details displayed successfully", "PASS",ResultPath,xwpfRun);

			String suppDepot=driver.findElement(By.xpath("//span[text()='Supplying Depot ']/..")).getText().trim();
			System.out.println("Supplying depot  at page is:"+suppDepot);

			suppDepot = suppDepot.substring(18);
			System.out.println("Substring1 "+suppDepot);

			System.out.println("Supplying depot is:"+suppDepot);
			System.out.println("Last day updated Supplying depot is:"+editSupplyDepot);
			if(suppDepot.equalsIgnoreCase(editSupplyDepot)){

				String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
				logger.log(LogStatus.PASS, "Supplying Depot details validated successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc8));
				utilityFileWriteOP.writeToLog(tcid, "Supplying Depot details validated successfully", "PASS",ResultPath,xwpfRun);
				res=true;

			}

			else{
				String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
				logger.log(LogStatus.FAIL, "Supplying Depot details validattion failed");
				logger.log(LogStatus.FAIL, logger.addScreenCapture(wholesale_sc8));
				utilityFileWriteOP.writeToLog(tcid, "Supplying Depot details validation failed", "PASS",ResultPath,xwpfRun);
				res=false;
			}

		}

		catch(Exception e){
			e.printStackTrace();
			String wholesale_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc9, driver);
			logger.log(LogStatus.FAIL, "Error occured while supply depot validation");
			logger.log(LogStatus.FAIL, logger.addScreenCapture(wholesale_sc9));
			utilityFileWriteOP.writeToLog(tcid, "Error occured while supply depot validation", "FAIL",ResultPath,xwpfRun);
			res=false;
		}finally{
			return res;
		}

	}



	public static boolean store_Edit_OPS(String customer,String storeid,String category,String deliveryDay_0,String effectiveday_0,String deliveryDay_1,String effectiveday_1,WebDriver driver, WebDriverWait wait, String tcid,String ResultPath,XWPFRun xwpfRun, ExtentTest logger){
		boolean res=false;
		try{

			ResultPath=System.getProperty("user.dir")+"/"+ResultPath;

			Thread.sleep(4000);

			String currentWindow=driver.getWindowHandle();
			Set<String> windows=driver.getWindowHandles();

			for(String window:windows)
			{
				if(!window.equals(currentWindow))
				{
					driver.switchTo().window(window);
					//utilityFileWriteOP.writeToLog(tcid, "AWS Console Window", "Switched!!!",ResultPath, xwpfRun,"");
					break;
				}
			}

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@class='edit-text' and contains(text(),'Edit')]")));
			driver.findElement(By.xpath("//span[@class='edit-text' and contains(text(),'Edit')]")).isDisplayed();
			driver.findElement(By.xpath("//span[@class='edit-text' and contains(text(),'Edit')]")).click();
			String wholesale_sc1=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc1, driver);
			logger.log(LogStatus.PASS, "Store details in edit mode displayed");
			logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc1));
			utilityFileWriteOP.writeToLog(tcid, "Store details in edit mode displayed", "PASS",ResultPath,xwpfRun);

			driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).isDisplayed();
			driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).click();
			Thread.sleep(2000);
			String wholesale_sc7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc7, driver);
			logger.log(LogStatus.PASS, "Delivery Opportunities details displayed successfully");
			logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc7));
			utilityFileWriteOP.writeToLog(tcid, "Delivery Opportunities details displayed successfully", "PASS",ResultPath,xwpfRun);


			if(customer.equalsIgnoreCase("amazon"))

				//***********Amazon customer
			{

				driver.findElement(By.xpath("//button[@id='addnewButton' and @type='button' and contains(text(),'Add New Row')]")).isDisplayed();
				driver.findElement(By.xpath("//button[@id='addnewButton' and @type='button' and contains(text(),'Add New Row')]")).click();

				driver.findElement(By.xpath("//div[@id='deliveryDaysAddGrpId0']/input")).click();
				driver.findElement(By.xpath("//div[@id='deliveryDaysAddGrpId0']/input")).sendKeys(deliveryDay_0);

				pickDateFromCalendar(driver,wait,effectiveday_0);
				System.out.println("Date selected for first calendar");
				//						driver.findElement(By.xpath("//input[@id='Histcal_ALL0']")).sendKeys(effectiveday_0);
				driver.findElement(By.xpath("//h4[contains(text(),'Historical and Future')]")).click();
				driver.findElement(By.xpath("//button[@id='addnewButton' and @type='button' and contains(text(),'Add New Row')]")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("//div[@id='deliveryDaysAddGrpId0']/input")).click();
				driver.findElement(By.xpath("//div[@id='deliveryDaysAddGrpId0']/input")).sendKeys(deliveryDay_1);

				pickDateFromCalendar(driver,wait,effectiveday_1);
				System.out.println("Date selected for second calendar");
				driver.findElement(By.xpath("//h4[contains(text(),'Historical and Future')]")).click();
				Thread.sleep(2000);
				String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
				logger.log(LogStatus.PASS, "Delivery Opportunities details details to be update");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc8));
				utilityFileWriteOP.writeToLog(tcid, "Delivery Opportunities details details to be update", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).isDisplayed();
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).click();
				Thread.sleep(2000);

				driver.findElement(By.xpath("//div[contains(text(),'Store details updated successfully')]")).isDisplayed();

				String wholesale_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc9, driver);
				logger.log(LogStatus.PASS, "Store details updated successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc9));
				utilityFileWriteOP.writeToLog(tcid, "Store details updated successfully", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//h5[contains(text(),'Edit Store')]/button[@type='button' and contains(text(),'✖') and @id='close-popup']")).click();
				res=true;


			}

			else{

				//***********Harvest customer
				{
					String arr[]=category.split(",");

					for(int i=0;i<arr.length;i++){

						driver.findElement(By.xpath("//table[@class='custom-borderd-table table table-bordered table-striped table-hover search-result-table text-nowrap viewchanges-table current-opp-table marginzero']//td[contains(text(),'"+arr[i]+"')]")).click();

						driver.findElement(By.xpath("//button[@id='addnewButton' and @type='button' and contains(text(),'Add New Row')]")).isDisplayed();
						driver.findElement(By.xpath("//button[@id='addnewButton' and @type='button' and contains(text(),'Add New Row')]")).click();

						driver.findElement(By.xpath("//div[@id='deliveryDaysAddGrpId0']/input")).click();
						driver.findElement(By.xpath("//div[@id='deliveryDaysAddGrpId0']/input")).sendKeys(deliveryDay_0);

						pickDateFromCalendar_Category(driver,wait,effectiveday_0,arr[i]);
						System.out.println("Date selected for first calendar");
						String wholesale_sc10=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
						TakeScreenShot.saveScreenShot(wholesale_sc10, driver);
						logger.log(LogStatus.PASS, "Delivery Opportunities details details to be update");
						logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc10));
						utilityFileWriteOP.writeToLog(tcid, "Delivery Opportunities details details to be update", "PASS",ResultPath,xwpfRun);
						//						driver.findElement(By.xpath("//input[@id='Histcal_ALL0']")).sendKeys(effectiveday_0);
						driver.findElement(By.xpath("//h4[contains(text(),'Historical and Future')]")).click();
						driver.findElement(By.xpath("//button[@id='addnewButton' and @type='button' and contains(text(),'Add New Row')]")).click();
						Thread.sleep(2000);
						driver.findElement(By.xpath("//div[@id='deliveryDaysAddGrpId0']/input")).click();
						driver.findElement(By.xpath("//div[@id='deliveryDaysAddGrpId0']/input")).sendKeys(deliveryDay_1);

						pickDateFromCalendar_Category(driver,wait,effectiveday_1,arr[i]);
						System.out.println("Date selected for second calendar");
						driver.findElement(By.xpath("//h4[contains(text(),'Historical and Future')]")).click();
						Thread.sleep(2000);
						String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
						TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
						logger.log(LogStatus.PASS, "Delivery Opportunities details details to be update");
						logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc8));
						utilityFileWriteOP.writeToLog(tcid, "Delivery Opportunities details details to be update", "PASS",ResultPath,xwpfRun);
					}
					driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).isDisplayed();
					driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).click();
					Thread.sleep(2000);

					driver.findElement(By.xpath("//div[contains(text(),'Store details updated successfully')]")).isDisplayed();

					String wholesale_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
					TakeScreenShot.saveScreenShot(wholesale_sc9, driver);
					logger.log(LogStatus.PASS, "Store details updated successfully");
					logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc9));
					utilityFileWriteOP.writeToLog(tcid, "Store details updated successfully", "PASS",ResultPath,xwpfRun);
					driver.findElement(By.xpath("//h5[contains(text(),'Edit Store')]/button[@type='button' and contains(text(),'✖') and @id='close-popup']")).click();
					res=true;


				}

			}

		}

		catch(Exception e){
			e.printStackTrace();
			String wholesale_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc9, driver);
			logger.log(LogStatus.FAIL, "Error occured while store creation");
			logger.log(LogStatus.FAIL, logger.addScreenCapture(wholesale_sc9));
			utilityFileWriteOP.writeToLog(tcid, "Error occured while store creation", "FAIL",ResultPath,xwpfRun);
			res=false;
		}finally{
			return res;
		}

	}



	public static boolean store_Validate_OPS_Day2(String customer,String storeid,String category,String deliveryDay_0,String effectiveday_0,WebDriver driver, WebDriverWait wait, String tcid,String ResultPath,XWPFRun xwpfRun, ExtentTest logger){
		boolean res=false;
		try{

			ResultPath=System.getProperty("user.dir")+"/"+ResultPath;

			Thread.sleep(4000);

			String currentWindow=driver.getWindowHandle();
			Set<String> windows=driver.getWindowHandles();

			for(String window:windows)
			{
				if(!window.equals(currentWindow))
				{
					driver.switchTo().window(window);
					//utilityFileWriteOP.writeToLog(tcid, "AWS Console Window", "Switched!!!",ResultPath, xwpfRun,"");
					break;
				}
			}

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@class='edit-text' and contains(text(),'Edit')]")));
			driver.findElement(By.xpath("//span[@class='edit-text' and contains(text(),'Edit')]")).isDisplayed();

			driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).isDisplayed();
			driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).click();
			Thread.sleep(2000);
			String wholesale_sc7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc7, driver);
			logger.log(LogStatus.PASS, "Delivery Opportunities details displayed successfully");
			logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc7));
			utilityFileWriteOP.writeToLog(tcid, "Delivery Opportunities details displayed successfully", "PASS",ResultPath,xwpfRun);

			if(customer.equalsIgnoreCase("amazon"))

			{
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("window.scrollBy(0,400)");
				boolean del=driver.findElement(By.xpath("//table[@class='custom-borderd-table table table-bordered table-striped table-hover search-result-table text-nowrap viewchanges-table current-opp-table marginzero']//td[2][contains(text(),'"+deliveryDay_0+"')]")).isDisplayed();

				boolean eff=driver.findElement(By.xpath("//table[@class='custom-borderd-table table table-bordered table-striped table-hover search-result-table text-nowrap viewchanges-table current-opp-table marginzero']//td[11][contains(text(),'"+effectiveday_0+"')]")).isDisplayed();


				if(del && eff){

					String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
					TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
					logger.log(LogStatus.PASS, "OPS updated details validated successfully");
					logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc8));
					utilityFileWriteOP.writeToLog(tcid, "OPS updated details validated successfully", "PASS",ResultPath,xwpfRun);
					res=true;

				}

				else{
					String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
					TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
					logger.log(LogStatus.FAIL, "Supplying Depot details validattion failed");
					logger.log(LogStatus.FAIL, logger.addScreenCapture(wholesale_sc8));
					utilityFileWriteOP.writeToLog(tcid, "Supplying Depot details validation failed", "PASS",ResultPath,xwpfRun);
					res=false;
				}

			}

			else{

				String arr[]=category.split(",");

				for(int i=0;i<arr.length;i++){

					driver.findElement(By.xpath("((//h4[text()='Current Opportunity']//parent::div//following-sibling::div//table)[1]//td[text()='"+arr[i]+"']//parent::tr//td)[2]")).click();
					String delDay=driver.findElement(By.xpath("((//h4[text()='Current Opportunity']//parent::div//following-sibling::div//table)[1]//td[text()='"+arr[i]+"']//parent::tr//td)[2]")).getText();
					Thread.sleep(2000);
					System.out.println("Delivery day retrived is:"+delDay);
					System.out.println("Delivery day data sheet:"+deliveryDay_0);
					String effDate=driver.findElement(By.xpath("((//h4[text()='Current Opportunity']//parent::div//following-sibling::div//table)[1]//td[text()='"+arr[i]+"']//parent::tr//td)[11]")).getText();
					Thread.sleep(2000);
					System.out.println("Effective date retrived is:"+effDate);
					System.out.println("Effective date data sheetis:"+effectiveday_0);
					if(delDay.equalsIgnoreCase(deliveryDay_0) && effDate.equalsIgnoreCase(effectiveday_0)){

						JavascriptExecutor js = (JavascriptExecutor) driver;
						js.executeScript("window.scrollBy(0,400)");
						System.out.println("Inside true");
						String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
						TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
						logger.log(LogStatus.PASS, "Delivery Opportunities details validated successfully");
						logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc8));
						utilityFileWriteOP.writeToLog(tcid, "Delivery Opportunities details validated successfully", "PASS",ResultPath,xwpfRun);
						res=true;
					}

					else{

						JavascriptExecutor js = (JavascriptExecutor) driver;
						js.executeScript("window.scrollBy(0,400)");
						System.out.println("Inside false");
						String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
						TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
						logger.log(LogStatus.FAIL, "Delivery Opportunities details validation failed");
						logger.log(LogStatus.FAIL, logger.addScreenCapture(wholesale_sc8));
						utilityFileWriteOP.writeToLog(tcid, "Delivery Opportunities details validatfailed", "FAIL",ResultPath,xwpfRun);
						res=false;
					}


				}

			}
		}
		catch(Exception e){
			e.printStackTrace();
			String wholesale_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc9, driver);
			logger.log(LogStatus.FAIL, "Error occured while OPS validation");
			logger.log(LogStatus.FAIL, logger.addScreenCapture(wholesale_sc9));
			utilityFileWriteOP.writeToLog(tcid, "Error occured while OPS validation", "FAIL",ResultPath,xwpfRun);
			res=false;
		}finally{
			return res;
		}

	}



	public static boolean store_Validate_Effective_Immediate(String customer,String storeid,String category,String editSupplyDepot,String deliveryDay_0,WebDriver driver, WebDriverWait wait, String tcid,String ResultPath,XWPFRun xwpfRun, ExtentTest logger){
		boolean res=false;
		try{

			ResultPath=System.getProperty("user.dir")+"/"+ResultPath;

			Thread.sleep(4000);

			String currentWindow=driver.getWindowHandle();
			Set<String> windows=driver.getWindowHandles();

			for(String window:windows)
			{
				if(!window.equals(currentWindow))
				{
					driver.switchTo().window(window);
					//utilityFileWriteOP.writeToLog(tcid, "AWS Console Window", "Switched!!!",ResultPath, xwpfRun,"");
					break;
				}
			}

			if(customer.equalsIgnoreCase("amazon"))

			{

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@class='edit-text' and contains(text(),'Edit')]")));
				driver.findElement(By.xpath("//span[@class='edit-text' and contains(text(),'Edit')]")).isDisplayed();

				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).click();
				Thread.sleep(2000);
				String wholesale_sc7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc7, driver);
				logger.log(LogStatus.PASS, "Delivery Opportunities details displayed successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc7));
				utilityFileWriteOP.writeToLog(tcid, "Delivery Opportunities details displayed successfully", "PASS",ResultPath,xwpfRun);
				
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("window.scrollBy(0,400)");
				boolean del=driver.findElement(By.xpath("//table[@class='custom-borderd-table table table-bordered table-striped table-hover search-result-table text-nowrap viewchanges-table current-opp-table marginzero']//td[2][contains(text(),'"+deliveryDay_0+"')]")).isDisplayed();

				if(del){

					String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
					TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
					logger.log(LogStatus.PASS, "OPS updated details validated successfully");
					logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc8));
					utilityFileWriteOP.writeToLog(tcid, "OPS updated details validated successfully", "PASS",ResultPath,xwpfRun);
					res=true;

				}

				else{
					
					String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
					TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
					logger.log(LogStatus.FAIL, "OPS details validattion failed");
					logger.log(LogStatus.FAIL, logger.addScreenCapture(wholesale_sc8));
					utilityFileWriteOP.writeToLog(tcid, "OPS details validation failed", "PASS",ResultPath,xwpfRun);
					res=false;
				}

			}

			else{
				
				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).click();
				Thread.sleep(2000);
				String wholesale_sc7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc7, driver);
				logger.log(LogStatus.PASS, "Supplying Depot details displayed successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc7));
				utilityFileWriteOP.writeToLog(tcid, "Supplying Depot details displayed successfully", "PASS",ResultPath,xwpfRun);


				String suppDepot=driver.findElement(By.xpath("//span[text()='Supplying Depot ']/..")).getText().trim();
				System.out.println("Supplying depot  at page is:"+suppDepot);

				suppDepot = suppDepot.substring(18);
				System.out.println("Substring1 "+suppDepot);

				System.out.println("Supplying depot is:"+suppDepot);
				System.out.println("Last day updated Supplying depot is:"+editSupplyDepot);
				if(suppDepot.equalsIgnoreCase(editSupplyDepot)){

					String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
					TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
					logger.log(LogStatus.PASS, "Supplying Depot details validated successfully");
					logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc8));
					utilityFileWriteOP.writeToLog(tcid, "Supplying Depot details validated successfully", "PASS",ResultPath,xwpfRun);
					res=true;

				}

				else{
					String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
					TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
					logger.log(LogStatus.FAIL, "Supplying Depot details validattion failed");
					logger.log(LogStatus.FAIL, logger.addScreenCapture(wholesale_sc8));
					utilityFileWriteOP.writeToLog(tcid, "Supplying Depot details validation failed", "PASS",ResultPath,xwpfRun);
					res=false;
				}

				String arr[]=category.split(",");


				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).click();
				Thread.sleep(2000);
				String wholesale_sc17=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc17, driver);
				logger.log(LogStatus.PASS, "Delivery Opportunities details displayed successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc17));
				utilityFileWriteOP.writeToLog(tcid, "Delivery Opportunities details displayed successfully", "PASS",ResultPath,xwpfRun);

				
				for(int i=0;i<arr.length;i++){

					driver.findElement(By.xpath("((//h4[text()='Current Opportunity']//parent::div//following-sibling::div//table)[1]//td[text()='"+arr[i]+"']//parent::tr//td)[2]")).click();
					String delDay=driver.findElement(By.xpath("((//h4[text()='Current Opportunity']//parent::div//following-sibling::div//table)[1]//td[text()='"+arr[i]+"']//parent::tr//td)[2]")).getText();
					Thread.sleep(2000);
					System.out.println("Delivery day retrived is:"+delDay);
					System.out.println("Delivery day data sheet:"+deliveryDay_0);
					
					if(delDay.equalsIgnoreCase(deliveryDay_0)){

						JavascriptExecutor js = (JavascriptExecutor) driver;
						js.executeScript("window.scrollBy(0,300)");
						System.out.println("Inside true");
						String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
						TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
						logger.log(LogStatus.PASS, "Delivery Opportunities details validated successfully");
						logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc8));
						utilityFileWriteOP.writeToLog(tcid, "Delivery Opportunities details validated successfully", "PASS",ResultPath,xwpfRun);
						res=true;
					}

					else{

						JavascriptExecutor js = (JavascriptExecutor) driver;
						js.executeScript("window.scrollBy(0,400)");
						System.out.println("Inside false");
						String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
						TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
						logger.log(LogStatus.FAIL, "Delivery Opportunities details validation failed");
						logger.log(LogStatus.FAIL, logger.addScreenCapture(wholesale_sc8));
						utilityFileWriteOP.writeToLog(tcid, "Delivery Opportunities details validatfailed", "FAIL",ResultPath,xwpfRun);
						res=false;
					}


				}

			}
		}
		catch(Exception e){
			e.printStackTrace();
			String wholesale_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc9, driver);
			logger.log(LogStatus.FAIL, "Error occured while OPS validation");
			logger.log(LogStatus.FAIL, logger.addScreenCapture(wholesale_sc9));
			utilityFileWriteOP.writeToLog(tcid, "Error occured while OPS validation", "FAIL",ResultPath,xwpfRun);
			res=false;
		}finally{
			return res;
		}

	}



	public static boolean store_Validate_Without_Eff_Imm(String customer,String storeid,String category,String editSupplyDepot,String deliveryDay_0,WebDriver driver, WebDriverWait wait, String tcid,String ResultPath,XWPFRun xwpfRun, ExtentTest logger){
		boolean res=false;
		try{

			ResultPath=System.getProperty("user.dir")+"/"+ResultPath;

			Thread.sleep(4000);

			String currentWindow=driver.getWindowHandle();
			Set<String> windows=driver.getWindowHandles();

			for(String window:windows)
			{
				if(!window.equals(currentWindow))
				{
					driver.switchTo().window(window);
					//utilityFileWriteOP.writeToLog(tcid, "AWS Console Window", "Switched!!!",ResultPath, xwpfRun,"");
					break;
				}
			}

			if(customer.equalsIgnoreCase("amazon"))

			{

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@class='edit-text' and contains(text(),'Edit')]")));
				driver.findElement(By.xpath("//span[@class='edit-text' and contains(text(),'Edit')]")).isDisplayed();

				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).click();
				Thread.sleep(2000);
				String wholesale_sc7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc7, driver);
				logger.log(LogStatus.PASS, "Delivery Opportunities details displayed successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc7));
				utilityFileWriteOP.writeToLog(tcid, "Delivery Opportunities details displayed successfully", "PASS",ResultPath,xwpfRun);
				
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("window.scrollBy(0,400)");
				boolean del=driver.findElement(By.xpath("//table[@class='custom-borderd-table table table-bordered table-striped table-hover search-result-table text-nowrap viewchanges-table current-opp-table marginzero']//td[2][contains(text(),'"+deliveryDay_0+"')]")).isDisplayed();

				if(del){

					String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
					TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
					logger.log(LogStatus.PASS, "OPS updated details validated successfully");
					logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc8));
					utilityFileWriteOP.writeToLog(tcid, "OPS updated details validated successfully", "PASS",ResultPath,xwpfRun);
					res=true;

				}

				else{
					
					String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
					TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
					logger.log(LogStatus.FAIL, "OPS details validattion failed");
					logger.log(LogStatus.FAIL, logger.addScreenCapture(wholesale_sc8));
					utilityFileWriteOP.writeToLog(tcid, "OPS details validation failed", "PASS",ResultPath,xwpfRun);
					res=false;
				}

			}

			else{
				
				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).click();
				Thread.sleep(2000);
				String wholesale_sc7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc7, driver);
				logger.log(LogStatus.PASS, "Supplying Depot details displayed successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc7));
				utilityFileWriteOP.writeToLog(tcid, "Supplying Depot details displayed successfully", "PASS",ResultPath,xwpfRun);


				String suppDepot=driver.findElement(By.xpath("//span[text()='Supplying Depot ']/..")).getText().trim();
				System.out.println("Supplying depot  at page is:"+suppDepot);

				suppDepot = suppDepot.substring(18);
				System.out.println("Substring1 "+suppDepot);

				System.out.println("Supplying depot is:"+suppDepot);
				System.out.println("Last day updated Supplying depot is:"+editSupplyDepot);
				if(suppDepot.equalsIgnoreCase(editSupplyDepot)){

					String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
					TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
					logger.log(LogStatus.PASS, "Supplying Depot details validated successfully");
					logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc8));
					utilityFileWriteOP.writeToLog(tcid, "Supplying Depot details validated successfully", "PASS",ResultPath,xwpfRun);
					res=true;

				}

				else{
					String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
					TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
					logger.log(LogStatus.FAIL, "Supplying Depot details validattion failed");
					logger.log(LogStatus.FAIL, logger.addScreenCapture(wholesale_sc8));
					utilityFileWriteOP.writeToLog(tcid, "Supplying Depot details validation failed", "PASS",ResultPath,xwpfRun);
					res=false;
				}

				String arr[]=category.split(",");


				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).click();
				Thread.sleep(2000);
				String wholesale_sc17=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc17, driver);
				logger.log(LogStatus.PASS, "Delivery Opportunities details displayed successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc17));
				utilityFileWriteOP.writeToLog(tcid, "Delivery Opportunities details displayed successfully", "PASS",ResultPath,xwpfRun);

				
				for(int i=0;i<arr.length;i++){

					driver.findElement(By.xpath("((//h4[text()='Current Opportunity']//parent::div//following-sibling::div//table)[1]//td[text()='"+arr[i]+"']//parent::tr//td)[2]")).click();
					String delDay=driver.findElement(By.xpath("((//h4[text()='Current Opportunity']//parent::div//following-sibling::div//table)[1]//td[text()='"+arr[i]+"']//parent::tr//td)[2]")).getText();
					Thread.sleep(2000);
					System.out.println("Delivery day retrived is:"+delDay);
					System.out.println("Delivery day data sheet:"+deliveryDay_0);
					
					if(delDay.equalsIgnoreCase(deliveryDay_0)){

						JavascriptExecutor js = (JavascriptExecutor) driver;
						js.executeScript("window.scrollBy(0,300)");
						System.out.println("Inside true");
						String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
						TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
						logger.log(LogStatus.PASS, "Delivery Opportunities details validated successfully");
						logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc8));
						utilityFileWriteOP.writeToLog(tcid, "Delivery Opportunities details validated successfully", "PASS",ResultPath,xwpfRun);
						res=true;
					}

					else{

						JavascriptExecutor js = (JavascriptExecutor) driver;
						js.executeScript("window.scrollBy(0,400)");
						System.out.println("Inside false");
						String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
						TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
						logger.log(LogStatus.FAIL, "Delivery Opportunities details validation failed");
						logger.log(LogStatus.FAIL, logger.addScreenCapture(wholesale_sc8));
						utilityFileWriteOP.writeToLog(tcid, "Delivery Opportunities details validatfailed", "FAIL",ResultPath,xwpfRun);
						res=false;
					}


				}

			}
		}
		catch(Exception e){
			e.printStackTrace();
			String wholesale_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc9, driver);
			logger.log(LogStatus.FAIL, "Error occured while OPS validation");
			logger.log(LogStatus.FAIL, logger.addScreenCapture(wholesale_sc9));
			utilityFileWriteOP.writeToLog(tcid, "Error occured while OPS validation", "FAIL",ResultPath,xwpfRun);
			res=false;
		}finally{
			return res;
		}

	}

	
	
	
	
	public static boolean pickDateFromCalendar(WebDriver driver, WebDriverWait wait, String date)
	{
		boolean res = false;
		String[] dates = date.split("/");
		String dd= dates[0];String mm= dates[1];String yyyy= dates[2];
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='Histcal_ALL0']")));
		driver.findElement(By.xpath("//input[@id='Histcal_ALL0']")).click();
		switch(mm)
		{
		case "01": mm = "Jan"; break;
		case "02": mm = "Feb"; break;
		case "03": mm = "Mar"; break;
		case "04": mm = "Apr"; break;
		case "05": mm = "May"; break;
		case "06": mm = "Jun"; break;
		case "07": mm = "Jul"; break;
		case "08": mm = "Aug"; break;
		case "09": mm = "Sep"; break;
		case "10": mm = "Oct"; break;
		case "11": mm = "Nov"; break;
		case "12": mm = "Dec"; break;
		}
		String monthshown = driver.findElement(By.xpath("(//th[@class='month'])[1]")).getText();
		while(true)
		{

			if(monthshown.contains(mm) || monthshown.contains(yyyy))
			{
				driver.findElement(By.xpath("(//table[@class='table-condensed'])[1]//td[contains(text(),'"+dd+"')]")).click();

				break;
			}
			else
			{
				driver.findElement(By.xpath("(//i[@class='fa fa-chevron-right glyphicon glyphicon-chevron-right'])[1]")).click();
			}
		}


		return res;
	}



	public static boolean pickDateFromCalendar_Category(WebDriver driver, WebDriverWait wait, String date,String category)
	{
		boolean res = false;
		String[] dates = date.split("/");
		String dd= dates[0];String mm= dates[1];String yyyy= dates[2];
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='Histcal_"+category+"0']")));
		driver.findElement(By.xpath("//input[@id='Histcal_"+category+"0']")).click();
		switch(mm)
		{
		case "01": mm = "Jan"; break;
		case "02": mm = "Feb"; break;
		case "03": mm = "Mar"; break;
		case "04": mm = "Apr"; break;
		case "05": mm = "May"; break;
		case "06": mm = "Jun"; break;
		case "07": mm = "Jul"; break;
		case "08": mm = "Aug"; break;
		case "09": mm = "Sep"; break;
		case "10": mm = "Oct"; break;
		case "11": mm = "Nov"; break;
		case "12": mm = "Dec"; break;
		}
		String monthshown = driver.findElement(By.xpath("(//th[@class='month'])[1]")).getText();
		while(true)
		{

			if(monthshown.contains(mm) || monthshown.contains(yyyy))
			{
				driver.findElement(By.xpath("(//table[@class='table-condensed'])[1]//td[contains(text(),'"+dd+"')]")).click();

				break;
			}
			else
			{
				driver.findElement(By.xpath("(//i[@class='fa fa-chevron-right glyphicon glyphicon-chevron-right'])[1]")).click();
			}
		}


		return res;
	}


	public static boolean pickExportCalendarDate(WebDriver driver, WebDriverWait wait, String date) throws Exception
	{
		boolean res = false;
		String[] dates = date.split("/");
		String dd= dates[0];String mm= dates[1];String yyyy= dates[2];
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@id='orderdateinput']")));
		driver.findElement(By.xpath("//span[@id='orderdateinput']")).click();
		switch(mm)
		{
		case "01": mm = "Jan"; break;
		case "02": mm = "Feb"; break;
		case "03": mm = "Mar"; break;
		case "04": mm = "Apr"; break;
		case "05": mm = "May"; break;
		case "06": mm = "Jun"; break;
		case "07": mm = "Jul"; break;
		case "08": mm = "Aug"; break;
		case "09": mm = "Sep"; break;
		case "10": mm = "Oct"; break;
		case "11": mm = "Nov"; break;
		case "12": mm = "Dec"; break;
		}
		String monthshown = driver.findElement(By.xpath("(//th[@class='month'])[1]")).getText();
		while(true)
		{

			if(monthshown.contains(mm) || monthshown.contains(yyyy))
			{
				try{
					
					if(driver.findElement(By.xpath("(//table[@class='table-condensed'])[1]//td[@class='today active start-date active end-date available']")).getText().equals(dd)) {
						driver.findElement(By.xpath("(//table[@class='table-condensed'])[1]//td[@class='today active start-date active end-date available']")).click();
					}else{
						driver.findElement(By.xpath("//table[@class='table-condensed'][1]//td[@class='available' and contains(text(),'"+dd+"')]")).click();		
					}
						
				//driver.findElement(By.xpath("(//table[@class='table-condensed'])[1]//td[@class='off available' and text()='"+dd+"']")).isDisplayed();
				break;
				}
				catch (Exception e){
					/*if
					driver.findElement(By.xpath("((//table[@class='table-condensed'])[1]//td[contains(@class,'available') and contains(text(),'"+dd+"')])[2]")).click();
					
					break;*/
				}
			}
			else
			{
				driver.findElement(By.xpath("//i[@class='glyphicon glyphicon-calendar cust-calender']")).click();
			}
		}

		return res;
	}




}