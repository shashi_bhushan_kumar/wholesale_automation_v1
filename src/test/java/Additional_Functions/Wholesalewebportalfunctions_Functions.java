package Additional_Functions;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.time.temporal.ChronoUnit;

import Utilities.*;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;







































import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.poi.ss.formula.functions.Index;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import freemarker.template.utility.DateUtil;

public class Wholesalewebportalfunctions_Functions {

	/*
	 * Method Name:WholeSale Login
	 * Functionality Description:WholeSale Login
	 * Created on:08/01/2020
	 * Created By:Morrisons
	 */


	public static boolean wholesale_login(WebDriver driver, WebDriverWait wait, String tcid, String un, String pw,String Resultpath,XWPFRun xwpfrun, ExtentTest logger)
	{

		boolean res=false;
		try
		{	
			Resultpath=System.getProperty("user.dir")+"/"+Resultpath;

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='username']")));
			utilityFileWriteOP.writeToLog(tcid, "Login Page of wholesale webportal", "Displayed!!!",Resultpath);
			String wholesale_sc=Resultpath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	    
			TakeScreenShot.saveScreenShot(wholesale_sc, driver);
			logger.log(LogStatus.PASS, "Login Page of wholesale webportal is displayed");
			logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc));

			driver.findElement(By.xpath("//input[@id='username']")).sendKeys(un);
			utilityFileWriteOP.writeToLog(tcid, "Username", "Entered!!!",Resultpath);
			logger.log(LogStatus.PASS, "Username is Entered");

			driver.findElement(By.xpath("//input[@id='password']")).sendKeys(pw);
			utilityFileWriteOP.writeToLog(tcid, "Password", "Entered!!!",Resultpath);
			String wholesale_sc1=Resultpath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc1, driver);
			logger.log(LogStatus.PASS, "Password is enetered");
			logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc1));

			driver.findElement(By.xpath("//button[@id='submit']")).click();
			utilityFileWriteOP.writeToLog(tcid, "Login Button", "Clicked!!!",Resultpath);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h3[contains(text(),'SELECT CUSTOMER')]")));
			String wholesale_sc2=Resultpath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc2, driver);

			if(driver.findElement(By.xpath("//h3[contains(text(),'SELECT CUSTOMER')]")).isDisplayed())
			{
				res=true;
				logger.log(LogStatus.PASS, "Order enquiry page for required Customer is displayed");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc2));
				utilityFileWriteOP.writeToLog(tcid, "Home Page", "Displayed!!!",Resultpath);
				utilityFileWriteOP.writeToLog(tcid, "Home Page", "Displayed!!!",Resultpath);
			}else{
				logger.log(LogStatus.FAIL, "Order enquiry page for required Customer is NOT displayed");
				logger.log(LogStatus.FAIL, logger.addScreenCapture(wholesale_sc2));
				utilityFileWriteOP.writeToLog(tcid, "Home Page", "Not Displayed!!!",Resultpath);
				utilityFileWriteOP.writeToLog(tcid, "Home Page", "Not Displayed!!!",Resultpath);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			String awsDynamoDB_sc=Resultpath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	   
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc, driver);
			res=false;
			logger.log(LogStatus.FAIL, e);
			logger.log(LogStatus.FAIL, logger.addScreenCapture(awsDynamoDB_sc));
			utilityFileWriteOP.writeToLog(tcid, "A problem occurred while navigating to order enquiry page", "Due to "+e,Resultpath);
		}
		finally
		{
			return res;
		}


	}

	
	/*
	 * Method Name: webPortal_login
	 * Functionality Description: Login function for Wholesale Webportal
	 * Created on:08/01/2020
	 * Created By:Morrisons
	 */

	public static boolean webPortal_login(WebDriver driver, WebDriverWait wait, String tcid, String un, String pw,String Resultpath,XWPFRun xwpfrun, ExtentTest logger)
	{

		boolean res=false;
		try
		{	
			Resultpath=System.getProperty("user.dir")+"/"+Resultpath;

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='username']")));
			utilityFileWriteOP.writeToLog(tcid, "Login Page of wholesale webportal", "Displayed!!!",Resultpath);
			String wholesale_sc=Resultpath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	    
			TakeScreenShot.saveScreenShot(wholesale_sc, driver);
			logger.log(LogStatus.PASS, "Login Page of wholesale webportal is displayed");
			logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc));

			driver.findElement(By.xpath("//input[@id='username']")).sendKeys(un);
			utilityFileWriteOP.writeToLog(tcid, "Username", "Entered!!!",Resultpath);
			logger.log(LogStatus.PASS, "Username is Entered");

			driver.findElement(By.xpath("//input[@id='password']")).sendKeys(pw);
			utilityFileWriteOP.writeToLog(tcid, "Password", "Entered!!!",Resultpath);
			String wholesale_sc1=Resultpath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc1, driver);
			logger.log(LogStatus.PASS, "Password is entered");
			logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc1));

			driver.findElement(By.xpath("//button[@id='submit']")).click();
			utilityFileWriteOP.writeToLog(tcid, "Login Button", "Clicked!!!",Resultpath);

			Thread.sleep(2000);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h3[contains(text(),'SELECT CUSTOMER')]")));
			String wholesale_sc2=Resultpath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc2, driver);
			
			if(driver.findElement(By.xpath("//h3[contains(text(),'SELECT CUSTOMER')]")).isDisplayed())
			{
				Thread.sleep(4000);
				res=true;
				String wholesale_sc31=Resultpath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";
				TakeScreenShot.saveScreenShot(wholesale_sc31, driver);
				logger.log(LogStatus.PASS, "Home page is displayed!!");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc31));
				utilityFileWriteOP.writeToLog(tcid, "Home Page", "Displayed!!!",Resultpath,xwpfrun);
			
			}else{
				String wholesale_sc31=Resultpath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";
				logger.log(LogStatus.FAIL, "Home page NOT displayed");
				TakeScreenShot.saveScreenShot(wholesale_sc31, driver);
				logger.log(LogStatus.FAIL, logger.addScreenCapture(wholesale_sc31));
				utilityFileWriteOP.writeToLog(tcid, "Home Page", "Not Displayed!!!",Resultpath,xwpfrun);
				
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			String awsDynamoDB_sc=Resultpath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	   
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc, driver);
			res=false;
			logger.log(LogStatus.FAIL, e);
			logger.log(LogStatus.FAIL, logger.addScreenCapture(awsDynamoDB_sc));
			utilityFileWriteOP.writeToLog(tcid, "A problem occurred while navigating to order enquiery page", "Due to "+e,Resultpath);
		}
		finally
		{
			return res;
		}


	}

	
	/*
	 * Method Name: navigatetocustomer
	 * Functionality Description:Navigate to order Inquiry and order details
	 * Created on:08/01/2020
	 * Created By:Morrisons
	 */

	public static boolean navigatetocustomer(WebDriver driver, WebDriverWait wait, String tcid,String Customer,String orderid, String ResultPath,XWPFRun xwpfRun, ExtentTest logger){
		boolean res=false;
		try{

			ResultPath=System.getProperty("user.dir")+"/"+ResultPath;

			driver.findElement(By.xpath("//img[@alt='"+Customer+"']")).click();
			utilityFileWriteOP.writeToLog(tcid, "Required Customer", "Clicked!!!",ResultPath,xwpfRun);
			driver.findElement(By.xpath("//h5[contains(text(),'Order Enquiry')]")).click();
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h4[contains(text(),'Sales Orders')]")));
			String wholesale_sc2=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc2, driver);
			driver.findElement(By.xpath("//input[@id='orderno']")).sendKeys(orderid);
			utilityFileWriteOP.writeToLog(tcid, "OrderId", "Entered!!!",ResultPath,xwpfRun);
			logger.log(LogStatus.PASS, "Orderid is Entered");
			driver.findElement(By.xpath("//i[@class='glyphicon glyphicon-search search-icon']")).click();
			String wholesale_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc3, driver);
			driver.findElement(By.xpath("(//td[text()='"+orderid+"'])[2]")).click();
			Thread.sleep(2000);
			String wholesale_sc4=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc4, driver);
			ArrayList<String> tabs_windows = new ArrayList<String> (driver.getWindowHandles());
			System.out.println("Number of tabs: "+tabs_windows.size());
			driver.switchTo().window(tabs_windows.get(1));
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//i[@id='order_Qnt_Info']")));

			driver.findElement(By.xpath("//i[@id='order_Qnt_Info']")).click();
			System.out.println(driver.getCurrentUrl());
			Thread.sleep(2000);
			Robot robot =new Robot();
			robot.keyPress(KeyEvent.VK_PAGE_DOWN);
			Thread.sleep(2000);
			//wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h4[contains(text(),'Ordered Lines')]")));
			driver.findElement(By.xpath("//i[@id='trans_info']")).click();
			Thread.sleep(2000);
			robot.keyPress(KeyEvent.VK_PAGE_UP);
			Thread.sleep(2000);

			//wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h4[contains(text(),'Message ID')]")));

			driver.findElement(By.xpath("//i[@id='ship_add']")).click();
			Thread.sleep(2000);
			robot.keyPress(KeyEvent.VK_PAGE_DOWN);
			Thread.sleep(2000);
			//wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h4[contains(text(),'Post Code')]")));
			driver.findElement(By.xpath("//i[@id='bill_add']")).click();
			Thread.sleep(2000);
			robot.keyPress(KeyEvent.VK_PAGE_UP);
			Thread.sleep(2000);
			//wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h4[contains(text(),'Party ID')]")));
			driver.findElement(By.xpath("//i[@id='invoice_info']")).click();
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h4[contains(text(),'Ordered Lines')]")));
			Thread.sleep(2000);
			driver.findElement(By.xpath("//*[@id='orderDetailHeader']/div/div[1]/button[2]")).click();

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='orderDetailHeader']/div[2]/div[1]/button")));
			if(driver.findElement(By.xpath("//*[@id='orderDetailHeader']/div[2]/div[1]/button")).isDisplayed()){
				res=true;
			}else
				res=false;
			//				driver.switchTo().defaultContent();
		}catch(Exception e){
			e.printStackTrace();
			res=false;
		}finally{
			return res;
		}
	}


	/*
	 * Method Name: advancesearchorder
	 * Functionality Description:Navigate to order Inquiry and Advance search for order details
	 * Created on:08/01/2020
	 * Created By:Morrisons
	 */
	
	public static boolean advancesearchorder(String date, WebDriver driver, WebDriverWait wait, String tcid,String Customer,String orderid, String ResultPath,XWPFRun xwpfRun, ExtentTest logger){
		boolean res=false;
		try{

			ResultPath=System.getProperty("user.dir")+"/"+ResultPath;

			driver.findElement(By.xpath("//img[@alt='"+Customer+"']")).click();


			logger.log(LogStatus.PASS, "Clicked on '"+Customer+"'");
			String wholesale_sc1=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc1, driver);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc1));
			utilityFileWriteOP.writeToLog(tcid, "Required Customer", "Clicked!!!",ResultPath,xwpfRun);

			driver.findElement(By.xpath("//h5[contains(text(),'Order Enquiry')]")).click();


			logger.log(LogStatus.PASS, "User  navigated to Sales order page successfully for '"+Customer+"'");
			String wholesale_sc2=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc2, driver);
			utilityFileWriteOP.writeToLog(tcid, "Sales order page", "displayed!!!",ResultPath,xwpfRun);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc2));

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h4[contains(text(),'Sales Orders')]")));

			wait.until(ExpectedConditions.elementToBeClickable(By.id("advSearchIcon"))).isDisplayed();
			driver.findElement(By.id("advSearchIcon")).click();


			logger.log(LogStatus.PASS, "Advanced button , Advance Search Criteria for Sales Orders displayed successfully for  ");
			String wholesale_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc3, driver);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc3));
			utilityFileWriteOP.writeToLog(tcid, "Advance search page is", "displayed!!!",ResultPath,xwpfRun);

			JavascriptExecutor js = (JavascriptExecutor) driver;  
			js.executeScript("window.scrollBy(0,1000)");

			driver.findElement(By.xpath("//*[@id='adv-search-desk']")).click();
			Thread.sleep(500);
			if(driver.findElement(By.xpath("//strong[contains(text(),'Select atleast 1 field from Order Date')]")).isDisplayed()){
				res=true;
			}else
				res=false;

			logger.log(LogStatus.PASS, "error message displayed successfully after clicking on Submit button without entering any field");
			String wholesale_sc30=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc30, driver);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc30));
			utilityFileWriteOP.writeToLog(tcid, "Error message is", "displayed!!!",ResultPath,xwpfRun);

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//i[@id='orderdateinput'])[2]")));
			driver.findElement(By.xpath("(//i[@id='orderdateinput'])[2]")).click();

			Thread.sleep(200);



			String[] dmy= date.split("/");
			System.out.println("date: "+date);



			String day=dmy[0];
			System.out.println("Day: "+day);
			String[] monthList={"January","February","March","April","May","June","July","August","September","October","November","December"};

			int monthIndex= Integer.parseInt(dmy[1]);
			System.out.println("monthIndex: "+monthIndex);
			String month=monthList[monthIndex-1];
			System.out.println("month: "+month);
			String year=dmy[2];
			long y= Integer.parseInt(year);
			System.out.println("year: "+year);
			System.out.println("y: "+y);
			//System.out.println("The Date is : "+day+" "+month+" "+year);
			Thread.sleep(2000);



			driver.findElement(By.xpath("((//datepicker[@id='orderdateinput'])[2]//table//th)[2]//strong")).click();
			Thread.sleep(500);

			int yr = Calendar.getInstance().get(Calendar.YEAR);
			System.out.println("yr: "+yr);


			for(int i=0;i<(yr-y);i++)
				driver.findElement(By.xpath("//button[@class='btn btn-default btn-sm pull-left']")).click();
			Thread.sleep(500);



			driver.findElement(By.xpath("//span[contains(text(),'"+month+"')]")).click();
			System.out.println(day);

			try
			{
				Thread.sleep(5000);
				System.out.println("TRY1");




				WebElement element5=driver.findElement(By.xpath("(//datepicker[@id='orderdateinput'])[2]//table//td//span[contains(text(),'"+day+"')]"));

				JavascriptExecutor executor5 = (JavascriptExecutor)driver;
				executor5.executeScript("arguments[0].click()", element5);

				logger.log(LogStatus.PASS, "Order date "+date+" is entered for "+Customer);
				String wholesale_sc4=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc4, driver);
				utilityFileWriteOP.writeToLog(tcid, "Order date is", "entered!!!",ResultPath,xwpfRun);
				logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc4));
				Thread.sleep(500);


			}
			catch(WebDriverException e)
			{
				e.printStackTrace();
				System.out.println("TRY1");
				Thread.sleep(5000);


				WebElement element5=driver.findElement(By.xpath("(.//*[@class='btn btn-sm btn-default']/span[contains(text(),'"+day+"')])[2]"));

				JavascriptExecutor executor5 = (JavascriptExecutor)driver;
				executor5.executeScript("arguments[0].click()", element5);





				System.out.println("Click 2");

			}

			Thread.sleep(5000);
			res=true;

			return res;
		}
		catch(Exception e)
		{
			res=false;



			//wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id="search-order-border"]/div[7]/div[1]/p")));
			if(driver.findElement(By.id("advSearchIcon")).isDisplayed()){
				res=true;
			}else
				res=false;

			e.printStackTrace();
			//				driver.switchTo().defaultContent();
		}
		return res;

	}
	
	/*
	 * Method Name: advancesearchinvoice
	 * Functionality Description: Advance search for Invoice
	 * Created on:08/01/2020
	 * Created By:Morrisons
	 */
	
	public static boolean advancesearchinvoice(String invoicedate,WebDriver driver, WebDriverWait wait, String tcid,String Customer, String ResultPath,XWPFRun xwpfRun, ExtentTest logger){
		boolean res=false;
		try{

			JavascriptExecutor js = (JavascriptExecutor) driver;  


			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//i[@id='invoicedateinput'])[2]")));
			driver.findElement(By.xpath("(//i[@id='invoicedateinput'])[2]")).click();

			String[] dmy= invoicedate.split("/");

			String day=dmy[0];
			System.out.println("Day: "+invoicedate);
			String[] monthList={"January","February","March","April","May","June","July","August","September","October","November","December"};

			int monthIndex= Integer.parseInt(dmy[1]);
			System.out.println("monthIndex: "+monthIndex);
			String month=monthList[monthIndex-1];
			System.out.println("month: "+month);
			String year=dmy[2];
			long y= Integer.parseInt(year);
			System.out.println("year: "+year);
			System.out.println("y: "+y);


			driver.findElement(By.xpath("((//datepicker[@id='invoicedateinput'])[2]//table//th)[2]//strong")).click();
			Thread.sleep(500);

			int yr = Calendar.getInstance().get(Calendar.YEAR);
			System.out.println("yr: "+yr);


			for(int i=0;i<(yr-y);i++)
				driver.findElement(By.xpath("//button[@class='btn btn-default btn-sm pull-left']")).click();
			Thread.sleep(500);

			driver.findElement(By.xpath("//span[contains(text(),'"+month+"')]")).click();
			System.out.println(day);

			try
			{
				Thread.sleep(2000);
				System.out.println("TRY1");

				WebElement element5=driver.findElement(By.xpath("(//datepicker[@id='invoicedateinput'])[2]//table//td//span[contains(text(),'"+day+"')]"));

				JavascriptExecutor executor5 = (JavascriptExecutor)driver;
				executor5.executeScript("arguments[0].click()", element5);


				logger.log(LogStatus.PASS, "Invoice date "+invoicedate+" is entered for "+Customer);
				String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
				utilityFileWriteOP.writeToLog(tcid, "Invoice date is", "entered!!!",ResultPath,xwpfRun);
				logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc5));

				Thread.sleep(2000);
				try{
					
					boolean shipto=driver.findElement(By.xpath("//div[@class='col-xs-12 col-sm-12 form-group']//input[@id='shipto']")).isEnabled();
					boolean shipfrom = driver.findElement(By.xpath("(//input[@class='form-control ng-untouched ng-pristine' ])[6]")).isEnabled();
					boolean invoiceId = driver.findElement(By.xpath("//div[@class='col-xs-12 col-sm-12 form-group']//input[@id='invoiceId']")).isEnabled();
					boolean status = driver.findElement(By.xpath("//select[@id='status']")).isEnabled();
					
					System.out.println("Inside try block");
					logger.log(LogStatus.PASS, "after providing two criteria rest all criteria are disabled");
					String wholesale_sc6=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
					TakeScreenShot.saveScreenShot(wholesale_sc6, driver);
					utilityFileWriteOP.writeToLog(tcid, "after providing two criteria rest all criteria are", "disabled!!!",ResultPath,xwpfRun);
					logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc6));
					
					System.out.println("shipto: "+shipto);
					System.out.println("shipfrom: "+shipfrom);
					System.out.println("invoiceId: "+invoiceId);
					System.out.println("status: "+status);

					if(shipto==false && shipfrom==false && invoiceId==false && status==false)
					{
						logger.log(LogStatus.PASS, "after providing two criteria rest all criteria are disabled");
						wholesale_sc6=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
						TakeScreenShot.saveScreenShot(wholesale_sc6, driver);
						utilityFileWriteOP.writeToLog(tcid, "after providing two criteria rest all criteria are", "disabled!!!",ResultPath,xwpfRun);
						logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc6));

						res=true;
						System.out.println("TRUE");
					}else{
						res=false;
						System.out.println("FALSE");
					}

				}
				catch(Exception e)
				{
					e.printStackTrace();
					System.out.println("Inside Code");
					Thread.sleep(5000);
					res=false;
				}



				driver.findElement(By.xpath("//*[@id='adv-search-desk']")).click();
				Thread.sleep(5000);

				js.executeScript("window.scrollBy(0,1000)");


				driver.findElement(By.xpath("//table[@class='custom-borderd-table table table-bordered table-striped search-result-table text-nowrap']//tbody//tr[1]//td[1]")).isDisplayed();

				logger.log(LogStatus.PASS, "All the orders with entered orderdate and invoicedate are displayed");
				String wholesale_sc6=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc6, driver);
				utilityFileWriteOP.writeToLog(tcid, "All the orders with entered orderdate and invoicedate are", "displayed!!!",ResultPath,xwpfRun);
				logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc6));

				js.executeScript("window.scrollBy(0,-900)");

				driver.findElement(By.xpath("//table[@class='custom-borderd-table table table-bordered table-striped search-result-table text-nowrap']//tbody//tr[1]//td[1]")).click();

				logger.log(LogStatus.PASS, "Selected Order detail displayed for "+Customer);
				String wholesale_sc7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc7, driver);
				utilityFileWriteOP.writeToLog(tcid, "All the orders with entered orderdate and invoicedate are", "displayed!!!",ResultPath,xwpfRun);
				logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc7)); 

				Thread.sleep(2000);
				ArrayList<String> tabs_windows = new ArrayList<String> (driver.getWindowHandles());
				driver.switchTo().window(tabs_windows.get(tabs_windows.size()-1));
				//wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//i[@id='order_Qnt_Info']")));

				System.out.println("Number of tabs: "+tabs_windows.size());
				Thread.sleep(2000);

				driver.findElement(By.xpath("//i[@id='order_Qnt_Info']")).isDisplayed();
				driver.findElement(By.xpath("//i[@id='order_Qnt_Info']")).click();

				logger.log(LogStatus.PASS, "Details of Order quantity information");
				String wholesale_sc60=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc60, driver);
				logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc60));
				utilityFileWriteOP.writeToLog(tcid, "Order quantity information", "displayed!!!",ResultPath,xwpfRun);
				System.out.println(driver.getCurrentUrl());
				Thread.sleep(2000);
				Robot robot =new Robot();
				robot.keyPress(KeyEvent.VK_PAGE_DOWN);
				Thread.sleep(2000);

				//wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h4[contains(text(),'Ordered Lines')]")));
				driver.findElement(By.xpath("//i[@id='trans_info']")).isDisplayed();
				driver.findElement(By.xpath("//i[@id='trans_info']")).click();

				logger.log(LogStatus.PASS, "Transmisson information is displayed");
				String wholesale_sc70=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc70, driver);
				logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc70));
				utilityFileWriteOP.writeToLog(tcid, "Transmisson information", "displayed!!!",ResultPath,xwpfRun);
				Thread.sleep(2000);
				robot.keyPress(KeyEvent.VK_PAGE_UP);
				Thread.sleep(2000);

				driver.findElement(By.xpath("//i[@id='bill_add']")).isDisplayed();
				driver.findElement(By.xpath("//i[@id='bill_add']")).click();

				logger.log(LogStatus.PASS, "Billing address is displayed");
				String wholesale_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc9, driver);
				logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc9));
				utilityFileWriteOP.writeToLog(tcid, "Billing address", "displayed!!!",ResultPath,xwpfRun);
				Thread.sleep(2000);
				//robot.keyPress(KeyEvent.VK_PAGE_UP);
				Thread.sleep(2000);
				driver.findElement(By.xpath("//i[@id='invoice_info']")).isDisplayed();
				driver.findElement(By.xpath("//i[@id='invoice_info']")).click();

				logger.log(LogStatus.PASS, "Invoice information is displayed");
				String wholesale_sc10=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc10, driver);
				logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc10));
				utilityFileWriteOP.writeToLog(tcid, "Invoice information", "displayed!!!",ResultPath,xwpfRun);


				Thread.sleep(2000);
				driver.findElement(By.xpath("//*[@id='orderDetailHeader']/div/div[1]/button[2]")).isDisplayed();
				driver.findElement(By.xpath("//*[@id='orderDetailHeader']/div/div[1]/button[2]")).click();

				logger.log(LogStatus.PASS, "Order tracker details displayed");
				String wholesale_sc11=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc11, driver);
				logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc11));
				utilityFileWriteOP.writeToLog(tcid, "Order tracker details", "displayed!!!",ResultPath,xwpfRun);

				System.out.println("Click 2");
			}
			catch(WebDriverException e)
			{
				e.printStackTrace();
				System.out.println("TRY1");
				Thread.sleep(5000);

				WebElement element5=driver.findElement(By.xpath("(.//*[@class='btn btn-sm btn-default']/span[contains(text(),'"+day+"')])[2]"));

				JavascriptExecutor executor5 = (JavascriptExecutor)driver;
				executor5.executeScript("arguments[0].click()", element5);

				System.out.println("Click 2");

			}

			Thread.sleep(5000);
			res=true;

			return res;
		}
		catch(Exception e)
		{
			res=false;

			e.printStackTrace();
			//				driver.switchTo().defaultContent();
		}
		return res;

	}
	
	/*
	 * Method Name: navigate_Customer
	 * Functionality Description:Navigate to specific Customer and Store id to be inquired with details validation
	 * Created on:08/01/2020
	 * Created By:Morrisons
	 */


	public static boolean navigate_Customer(WebDriver driver, WebDriverWait wait, String tcid,String Customer,String storeid, String ResultPath,XWPFRun xwpfRun, ExtentTest logger){
		boolean res=false;
		try{

			ResultPath=System.getProperty("user.dir")+"/"+ResultPath;

			driver.findElement(By.xpath("//img[@alt='"+Customer+"']")).click();
			utilityFileWriteOP.writeToLog(tcid, "Required Customer", "Clicked!!!",ResultPath);
			driver.findElement(By.xpath("//span[@class='menu-product-name-inline dis-inlineblc padd-top-5 text-left']")).click();
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h5[contains(text(),'Store Enquiry')]")));

			driver.findElement(By.xpath("//h5[contains(text(),'Store Enquiry')]")).click();
			driver.findElement(By.xpath("//h4[contains(text(),'Store Enquiry')]")).isDisplayed();
			Thread.sleep(2000);
			String wholesale_sc2=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc2, driver);
			logger.log(LogStatus.PASS, "Store Enquiry page displayed successfully");
			logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc2));
			utilityFileWriteOP.writeToLog(tcid, "Store Enquiry page displayed successfully", "PASS",ResultPath,xwpfRun);

			driver.findElement(By.xpath("//h4[contains(text(),'Store Enquiry')]")).isDisplayed();

			for(int i=0;i<6;i++){
				try{
					System.out.println("Loop count"+i);
					WebElement strtxt=wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("(//tr[@class='order-number-table cursor-pointer']/td[contains(text(),'"+storeid+"')])[2]"))));

					if(strtxt.getText().equalsIgnoreCase(storeid)){
						System.out.println("Inside IF");
						driver.findElement(By.xpath("(//tr[@class='order-number-table cursor-pointer']/td[contains(text(),'"+storeid+"')])[2]")).isDisplayed();
						driver.findElement(By.xpath("(//tr[@class='order-number-table cursor-pointer']/td[contains(text(),'"+storeid+"')])[2]")).click();

						String wholesale_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
						TakeScreenShot.saveScreenShot(wholesale_sc3, driver);
						logger.log(LogStatus.PASS, "Store number "+storeid+" is been displayed in page");
						logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc3));
						utilityFileWriteOP.writeToLog(tcid, "Store number "+storeid+" is been displayed in page", "PASS",ResultPath,xwpfRun);
						res=true;
						break;
					}
				}
				catch(Exception e){
					System.out.println("Scroll count"+i);
					JavascriptExecutor js = (JavascriptExecutor) driver;
					Thread.sleep(2000);
					((JavascriptExecutor)driver).executeScript("scroll(0,3000)");
					Thread.sleep(4000);
				}

			}

			//				
		}catch(Exception e){
			e.printStackTrace();
			res=false;
		}finally{
			return res;
		}
	}

	
	/*
	 * Method Name: navigate_Portal_Customer
	 * Functionality Description:Is to navigate in Webportal to specific customer
	 * Created on:08/01/2020
	 * Created By:Morrisons
	 */


	public static boolean navigate_Portal_Customer(WebDriver driver, WebDriverWait wait, String tcid,String Customer,String storeid, String ResultPath,XWPFRun xwpfRun, ExtentTest logger){
		boolean res=false;
		try{

			ResultPath=System.getProperty("user.dir")+"/"+ResultPath;

			driver.findElement(By.xpath("//img[@alt='"+Customer+"']")).click();
			utilityFileWriteOP.writeToLog(tcid, "Required Customer", "Clicked!!!",ResultPath);
			utilityFileWriteOP.writeToLog(tcid, "Required Customer", "Clicked!!!",ResultPath,xwpfRun);
			driver.findElement(By.xpath("//span[@class='menu-product-name-inline dis-inlineblc padd-top-5 text-left']")).isDisplayed();
			//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h5[contains(text(),'Store Enquiry')]")));

			//				driver.findElement(By.xpath("//h5[contains(text(),'Store Enquiry')]")).click();
			String wholesale_sc2=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc2, driver);
			utilityFileWriteOP.writeToLog(tcid, "Navigated successfully to Customer page", "PASS",ResultPath,xwpfRun);
			logger.log(LogStatus.PASS, "Navigated successfully to Customer page");
			logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc2));
			utilityFileWriteOP.writeToLog(tcid, "Navigated successfully to Customer page", "PASS",ResultPath,xwpfRun);


			res=true;

			//				driver.switchTo().defaultContent();
		}catch(Exception e){
			e.printStackTrace();
			res=false;
		}finally{
			return res;
		}
	}



	public static boolean verify_Tabs(String storeid,WebDriver driver, WebDriverWait wait, String tcid,String ResultPath,XWPFRun xwpfRun, ExtentTest logger){
		boolean res=false;
		try{

			ResultPath=System.getProperty("user.dir")+"/"+ResultPath;

			Thread.sleep(4000);

			String currentWindow=driver.getWindowHandle();
			Set<String> windows=driver.getWindowHandles();

			for(String window:windows)
			{
				if(!window.equals(currentWindow))
				{
					driver.switchTo().window(window);
					//utilityFileWriteOP.writeToLog(tcid, "AWS Console Window", "Switched!!!",ResultPath, xwpfRun,"");
					break;
				}
			}



			driver.findElement(By.xpath("//span[text()='Store ID']/..")).isDisplayed();
			/*	    		String strid=driver.findElement(By.xpath("//span[text()='Store ID']//parent::div")).getText();
			 */	    		String strid=driver.findElement(By.xpath("//span[text()='Store ID']/..")).getText().trim();
			 System.out.println(strid);

			 //strid = strid.substring(11);
			 strid = strid.substring(strid.indexOf(":")+1).trim();
			 System.out.println("Substring1 "+strid);

			 Thread.sleep(4000);
			 if(strid.equalsIgnoreCase(storeid)){
				 System.out.println("Store id matches with the input"+strid+"Input Store ID"+storeid);
				 driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).isDisplayed();
				 driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).click();
				 Thread.sleep(4000);
				 wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='bold' and contains(text(),'Area Code')]")));

				 driver.findElement(By.xpath("//div[@class='bold' and contains(text(),'Area Code')]")).isDisplayed();
				 String wholesale_sc2=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				 TakeScreenShot.saveScreenShot(wholesale_sc2, driver);
				 logger.log(LogStatus.PASS, "Main Details content displayed");
				 logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc2));
				 utilityFileWriteOP.writeToLog(tcid, "Main Details content displayed", "PASS",ResultPath,xwpfRun);

				 
				 driver.findElement(By.xpath("//a[contains(text(),'Address')]")).isDisplayed();
				 driver.findElement(By.xpath("//a[contains(text(),'Address')]")).click();
				 Thread.sleep(4000);
				 wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='bold' and contains(text(),'Contact Phone')]")));
				 driver.findElement(By.xpath("//div[@class='bold' and contains(text(),'Contact Phone')]")).click();
				 driver.findElement(By.xpath("//a[contains(text(),'Address')]")).isDisplayed();
				 String wholesale_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				 TakeScreenShot.saveScreenShot(wholesale_sc3, driver);
				 logger.log(LogStatus.PASS, "Address details dsiplayed successfully");
				 logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc3));
				 utilityFileWriteOP.writeToLog(tcid, "Address details dsiplayed successfully", "PASS",ResultPath,xwpfRun);


				 driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).isDisplayed();
				 driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).click();
				 Thread.sleep(4000);
				 wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@class='bold' and contains(text(),'Supplying Depot ')]")));
				 driver.findElement(By.xpath("//span[@class='bold' and contains(text(),'Supplying Depot ')]")).click();
				 driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).isDisplayed();
				 String wholesale_sc4=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				 TakeScreenShot.saveScreenShot(wholesale_sc4, driver);
				 logger.log(LogStatus.PASS, "Supplying Depot details displayed successfully");
				 logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc4));
				 utilityFileWriteOP.writeToLog(tcid, "Supplying Depot details displayed successfully", "PASS",ResultPath,xwpfRun);

				 driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).isDisplayed();
				 driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).click();
				 Thread.sleep(4000);
				 wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h4[@class='orders-heading' and contains(text(),'Current Opportunity')]")));
				 driver.findElement(By.xpath("//h4[@class='orders-heading' and contains(text(),'Current Opportunity')]")).click();
				 driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).isDisplayed();
		
				 String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				 TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
				 logger.log(LogStatus.PASS, "Delivering Opportunities displayed successfully");
				 logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc5));
				 utilityFileWriteOP.writeToLog(tcid, "Delivering Opportunities displayed successfully", "PASS",ResultPath,xwpfRun);

				 res=true;
			 }

			 else{

				 String wholesale_sc4=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				 TakeScreenShot.saveScreenShot(wholesale_sc4, driver);
				 logger.log(LogStatus.FAIL, "No text displayed");
				 utilityFileWriteOP.writeToLog(tcid, "No text displayed", "FAIL",ResultPath,xwpfRun);

				 System.out.println("No text displayed");
				 res=false;

			 }
			 //				driver.switchTo().defaultContent();
		}catch(Exception e){
			e.printStackTrace();
			res=false;
		}finally{
			return res;
		}
	}

	/*
	 * Method Name: store_New_Entry
	 * Functionality Description: Store creation New Entry for all customer using Switch case
	 * Created on:08/01/2020
	 * Created By:Morrisons
	 */
	
	public static boolean store_New_Entry(String customer,String storename,String storeid,String tradingname,String merchantid,String AddressLine1,String EmailId,WebDriver driver, WebDriverWait wait, String tcid,String ResultPath,XWPFRun xwpfRun, ExtentTest logger){
		boolean res=false;
		try{

			ResultPath=System.getProperty("user.dir")+"/"+ResultPath;

			Thread.sleep(4000);

			driver.findElement(By.xpath("//span[@class='menu-product-name-inline dis-inlineblc padd-top-5 text-left']")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("//h5[contains(text(),'Store  New Entry')]")).isDisplayed();
			driver.findElement(By.xpath("//h5[contains(text(),'Store  New Entry')]")).click();
			Thread.sleep(5000);

			String currentWindow=driver.getWindowHandle();
			Set<String> windows=driver.getWindowHandles();

			for(String window:windows)
			{
				if(!window.equals(currentWindow))
				{
					driver.switchTo().window(window);
					//utilityFileWriteOP.writeToLog(tcid, "AWS Console Window", "Switched!!!",ResultPath, xwpfRun,"");
					break;
				}
			}

			switch (customer){
			case "amazon":

				//***********Amazon customer
			{
				driver.findElement(By.xpath("//input[@id='storeNameId']")).click();

				String wholesale_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc3, driver);
				logger.log(LogStatus.PASS, "Store New entry page displayed");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc3));
				utilityFileWriteOP.writeToLog(tcid, "Store New entry page displayed", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//input[@id='storeNameId']")).sendKeys(storename);

				driver.findElement(By.xpath("//input[@id='storeID']")).sendKeys(storeid);

				driver.findElement(By.xpath("//input[@id='tradingNameId']")).sendKeys(tradingname);


				driver.findElement(By.xpath("//input[@id='merchantId']")).sendKeys(merchantid);

				Select availability = new Select(driver.findElement(By.xpath("//select[@id='availability']")));
				availability.selectByVisibleText("ACTIVE");

				String wholesale_sc4=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc4, driver);
				logger.log(LogStatus.PASS, "All details filled up for Store details");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc4));
				utilityFileWriteOP.writeToLog(tcid, "All details filled up for Store details", "PASS",ResultPath,xwpfRun);


				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).click();


				Select regionCode = new Select(driver.findElement(By.xpath("//select[@id='storeRegionP']")));
				regionCode.selectByVisibleText("England");
				Thread.sleep(2000);

				Select status = new Select(driver.findElement(By.xpath("//select[@id='statusId']")));
				status.selectByVisibleText("rollout");
				Thread.sleep(2000);
				String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
				logger.log(LogStatus.PASS, "All details filled up for main details");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc5));
				utilityFileWriteOP.writeToLog(tcid, "All details filled up for main details", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Address')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Address')]")).click();
				Thread.sleep(2000);

				driver.findElement(By.xpath("//input[@id='addressLine1']")).click();
				driver.findElement(By.xpath("//input[@id='addressLine1']")).sendKeys(AddressLine1);

				String wholesale_sc6=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc6, driver);
				logger.log(LogStatus.PASS, "Address details filled successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc6));
				utilityFileWriteOP.writeToLog(tcid, "Address details filled successfully", "PASS",ResultPath,xwpfRun);



				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).click();
				Thread.sleep(2000);


				String wholesale_sc7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc7, driver);
				logger.log(LogStatus.PASS, "Supplying Depot details filled up successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc7));
				utilityFileWriteOP.writeToLog(tcid, "Supplying Depot details filled up successfully", "PASS",ResultPath,xwpfRun);


				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).click();
				Thread.sleep(2000);

				driver.findElement(By.xpath("//input[@id='Histval_0']")).sendKeys("YYYYYYN");

				String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
				logger.log(LogStatus.PASS, "Delivery days details filled up successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc8));
				utilityFileWriteOP.writeToLog(tcid, "Delivery days details filled up successfully", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).isDisplayed();
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).click();

				Thread.sleep(3000);
				driver.findElement(By.xpath("//div[contains(text(),'Store added')]")).isDisplayed();

				String wholesale_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc9, driver);
				logger.log(LogStatus.PASS, "New Store created successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc9));
				utilityFileWriteOP.writeToLog(tcid, "New Store created successfully", "PASS",ResultPath,xwpfRun);


				driver.findElement(By.xpath("//button[@type='button' and contains(text(),'✖')]")).click();

				res=true;

				break;
			}
			case "harvest":
				//**********************Harvest Customer

			{
				driver.findElement(By.xpath("//input[@id='storeNameId']")).click();

				String wholesale_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc3, driver);
				logger.log(LogStatus.PASS, "Store New entry page displayed");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc3));
				utilityFileWriteOP.writeToLog(tcid, "Store New entry page displayed", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//input[@id='storeNameId']")).sendKeys(storename);

				driver.findElement(By.xpath("//input[@id='storeID']")).sendKeys(storeid);


				driver.findElement(By.xpath("//input[@id='tradingNameId']")).sendKeys(tradingname);


				Select availability = new Select(driver.findElement(By.xpath("//select[@id='availability']")));
				availability.selectByVisibleText("ACTIVE");

				String wholesale_sc4=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc4, driver);
				logger.log(LogStatus.PASS, "All details filled up for Store details");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc4));
				utilityFileWriteOP.writeToLog(tcid, "All details filled up for Store details", "PASS",ResultPath,xwpfRun);


				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).click();


				Select allocation = new Select(driver.findElement(By.xpath("//select[@id='storeAllocationP']")));
				allocation.selectByVisibleText("P2333");
				Thread.sleep(2000);

				Select status = new Select(driver.findElement(By.xpath("//select[@id='statusId']")));
				status.selectByVisibleText("rollout");
				Thread.sleep(2000);
				String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
				logger.log(LogStatus.PASS, "All details filled up for main details");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc5));
				utilityFileWriteOP.writeToLog(tcid, "All details filled up for main details", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//a[contains(text(),'Address')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Address')]")).click();
				Thread.sleep(2000);

				driver.findElement(By.xpath("//input[@id='addressLine1']")).click();
				driver.findElement(By.xpath("//input[@id='addressLine1']")).sendKeys(AddressLine1);


				driver.findElement(By.xpath("//input[@id='postcodeId']")).click();
				driver.findElement(By.xpath("//input[@id='postcodeId']")).sendKeys("NN172AF");

				String wholesale_sc6=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc6, driver);
				logger.log(LogStatus.PASS, "Address details filled successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc6));
				utilityFileWriteOP.writeToLog(tcid, "Address details filled successfully", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).click();
				Thread.sleep(2000);
				Select depotType = new Select(driver.findElement(By.xpath("//select[@name='supplyDepotType']")));
				depotType.selectByVisibleText("Bury");
				Thread.sleep(2000);

				String wholesale_sc7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc7, driver);
				logger.log(LogStatus.PASS, "Supplying Depot details filled up successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc7));
				utilityFileWriteOP.writeToLog(tcid, "Supplying Depot details filled up successfully", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("//h4[@class='orders-heading' and contains(text(),'Current Opportunity')]")).click();

				driver.findElement(By.xpath("//input[@id='Histval_0']")).sendKeys("YYYYYYN");
				Thread.sleep(1000);

				driver.findElement(By.xpath("//input[@id='Histval_1']")).sendKeys("YYYYYYN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histval_2']")).sendKeys("YYYYYNN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histval_3']")).sendKeys("YYYYNYN");
				Thread.sleep(1000);
				String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
				logger.log(LogStatus.PASS, "Delivery days details filled up successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc8));
				utilityFileWriteOP.writeToLog(tcid, "Delivery days details filled up successfully", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).isDisplayed();
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).click();

				Thread.sleep(3000);
				driver.findElement(By.xpath("//div[contains(text(),'Store added')]")).isDisplayed();

				String wholesale_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc9, driver);
				logger.log(LogStatus.PASS, "New Store created successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc9));
				utilityFileWriteOP.writeToLog(tcid, "New Store created successfully", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//button[@type='button' and contains(text(),'✖')]")).click();

				res=true;

				break;
			}

			case "mccolls":
				//**********************McColls Customer

			{
				driver.findElement(By.xpath("//input[@id='storeNameId']")).click();

				String wholesale_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc3, driver);
				logger.log(LogStatus.PASS, "Store New entry page displayed");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc3));
				utilityFileWriteOP.writeToLog(tcid, "Store New entry page displayed", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//input[@id='storeNameId']")).sendKeys(storename);

				driver.findElement(By.xpath("//input[@id='storeID']")).sendKeys(storeid);

				driver.findElement(By.xpath("//input[@id='tradingNameId']")).sendKeys(tradingname);


				Select availability = new Select(driver.findElement(By.xpath("//select[@id='availability']")));
				availability.selectByVisibleText("ACTIVE");


				String wholesale_sc4=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc4, driver);
				logger.log(LogStatus.PASS, "All details filled up for Store details");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc4));
				utilityFileWriteOP.writeToLog(tcid, "All details filled up for Store details", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).click();


				Select allocation = new Select(driver.findElement(By.xpath("//select[@id='storeAllocationP']")));
				allocation.selectByVisibleText("P2333");
				Thread.sleep(2000);

				Select status = new Select(driver.findElement(By.xpath("//select[@id='statusId']")));
				status.selectByVisibleText("rollout");
				Thread.sleep(2000);
				String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
				logger.log(LogStatus.PASS, "All details filled up for main details");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc5));
				utilityFileWriteOP.writeToLog(tcid, "All details filled up for main details", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//a[contains(text(),'Address')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Address')]")).click();
				Thread.sleep(2000);

				driver.findElement(By.xpath("//input[@id='addressLine1']")).click();
				driver.findElement(By.xpath("//input[@id='addressLine1']")).sendKeys(AddressLine1);


				driver.findElement(By.xpath("//input[@id='postcodeId']")).click();
				driver.findElement(By.xpath("//input[@id='postcodeId']")).sendKeys("SS7 4EN");

				String wholesale_sc6=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc6, driver);
				logger.log(LogStatus.PASS, "Address details filled successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc6));
				utilityFileWriteOP.writeToLog(tcid, "Address details filled successfully", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).click();
				Thread.sleep(2000);
				Select depotType = new Select(driver.findElement(By.xpath("//select[@name='supplyDepotType']")));
				depotType.selectByVisibleText("Bury");
				Thread.sleep(2000);

				String wholesale_sc7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc7, driver);
				logger.log(LogStatus.PASS, "Supplying Depot details filled up successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc7));
				utilityFileWriteOP.writeToLog(tcid, "Supplying Depot details filled up successfully", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("//h4[@class='orders-heading' and contains(text(),'Current Opportunity')]")).click();

				driver.findElement(By.xpath("//input[@id='Histval_0']")).sendKeys("YYYYYYN");
				Thread.sleep(1000);

				driver.findElement(By.xpath("//input[@id='Histval_1']")).sendKeys("YYYYYYN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histval_2']")).sendKeys("YYYYYNN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histval_3']")).sendKeys("YYYYNYN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histval_4']")).sendKeys("YYYYNYN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histval_5']")).sendKeys("YYYYYYN");
				Thread.sleep(1000);

				String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
				logger.log(LogStatus.PASS, "Delivery days details filled up successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc8));
				utilityFileWriteOP.writeToLog(tcid, "Delivery days details filled up successfully", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).isDisplayed();
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).click();

				Thread.sleep(3000);
				driver.findElement(By.xpath("//div[contains(text(),'Store added')]")).isDisplayed();

				String wholesale_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc9, driver);
				logger.log(LogStatus.PASS, "New Store created successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc9));
				utilityFileWriteOP.writeToLog(tcid, "New Store created successfully", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//button[@type='button' and contains(text(),'✖')]")).click();

				res=true;

				break;
			}

			case "mcdly":
				//**********************McDaily Customer

			{
				driver.findElement(By.xpath("//input[@id='storeNameId']")).click();

				String wholesale_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc3, driver);
				logger.log(LogStatus.PASS, "Store New entry page displayed");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc3));
				utilityFileWriteOP.writeToLog(tcid, "Store New entry page displayed", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//input[@id='storeNameId']")).sendKeys(storename);

				driver.findElement(By.xpath("//input[@id='storeID']")).sendKeys(storeid);

				driver.findElement(By.xpath("//input[@id='tradingNameId']")).sendKeys(tradingname);


				Select availability = new Select(driver.findElement(By.xpath("//select[@id='availability']")));
				availability.selectByVisibleText("ACTIVE");


				String wholesale_sc4=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc4, driver);
				logger.log(LogStatus.PASS, "All details filled up for Store details");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc4));
				utilityFileWriteOP.writeToLog(tcid, "All details filled up for Store details", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).click();


				Select allocation = new Select(driver.findElement(By.xpath("//select[@id='storeAllocationP']")));
				allocation.selectByVisibleText("P20000");
				Thread.sleep(2000);

				Select status = new Select(driver.findElement(By.xpath("//select[@id='statusId']")));
				status.selectByVisibleText("rollout");
				Thread.sleep(2000);
				String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
				logger.log(LogStatus.PASS, "All details filled up for main details");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc5));
				utilityFileWriteOP.writeToLog(tcid, "All details filled up for main details", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//a[contains(text(),'Address')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Address')]")).click();
				Thread.sleep(2000);

				driver.findElement(By.xpath("//input[@id='addressLine1']")).click();
				driver.findElement(By.xpath("//input[@id='addressLine1']")).sendKeys(AddressLine1);


				driver.findElement(By.xpath("//input[@id='postcodeId']")).click();
				driver.findElement(By.xpath("//input[@id='postcodeId']")).sendKeys("JE2 4TA");

				String wholesale_sc6=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc6, driver);
				logger.log(LogStatus.PASS, "Address details filled successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc6));
				utilityFileWriteOP.writeToLog(tcid, "Address details filled successfully", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).click();
				Thread.sleep(2000);
				Select depotType = new Select(driver.findElement(By.xpath("//select[@name='supplyDepotType']")));
				depotType.selectByVisibleText("Bury");
				Thread.sleep(2000);

				String wholesale_sc7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc7, driver);
				logger.log(LogStatus.PASS, "Supplying Depot details filled up successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc7));
				utilityFileWriteOP.writeToLog(tcid, "Supplying Depot details filled up successfully", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("//h4[@class='orders-heading' and contains(text(),'Current Opportunity')]")).click();

				driver.findElement(By.xpath("//input[@id='Histval_0']")).sendKeys("YYYYYYN");
				Thread.sleep(1000);

				driver.findElement(By.xpath("//input[@id='Histval_1']")).sendKeys("YYYYYYN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histval_2']")).sendKeys("YYYYYNN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histval_3']")).sendKeys("YYYYNYN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histval_4']")).sendKeys("YYYYNYN");
				Thread.sleep(1000);


				String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
				logger.log(LogStatus.PASS, "Delivery days details filled up successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc8));
				utilityFileWriteOP.writeToLog(tcid, "Delivery days details filled up successfully", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).isDisplayed();
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).click();

				Thread.sleep(3000);
				driver.findElement(By.xpath("//div[contains(text(),'Store added')]")).isDisplayed();

				String wholesale_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc9, driver);
				logger.log(LogStatus.PASS, "New Store created successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc9));
				utilityFileWriteOP.writeToLog(tcid, "New Store created successfully", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//button[@type='button' and contains(text(),'✖')]")).click();

				res=true;

				break;
			}

			case "mpk":
				//**********************MPK Customer

			{
				driver.findElement(By.xpath("//input[@id='storeNameId']")).click();

				String wholesale_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc3, driver);
				logger.log(LogStatus.PASS, "Store New entry page displayed");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc3));
				utilityFileWriteOP.writeToLog(tcid, "Store New entry page displayed", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//input[@id='storeNameId']")).sendKeys(storename);

				driver.findElement(By.xpath("//input[@id='storeID']")).sendKeys(storeid);

				driver.findElement(By.xpath("//input[@id='tradingNameId']")).sendKeys(tradingname);


				Select availability = new Select(driver.findElement(By.xpath("//select[@id='availability']")));
				availability.selectByVisibleText("ACTIVE");


				String wholesale_sc4=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc4, driver);
				logger.log(LogStatus.PASS, "All details filled up for Store details");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc4));
				utilityFileWriteOP.writeToLog(tcid, "All details filled up for Store details", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).click();


				Select allocation = new Select(driver.findElement(By.xpath("//select[@id='storeAllocationP']")));
				allocation.selectByVisibleText("P20000");
				Thread.sleep(2000);

				Select status = new Select(driver.findElement(By.xpath("//select[@id='statusId']")));
				status.selectByVisibleText("rollout");
				Thread.sleep(2000);
				String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
				logger.log(LogStatus.PASS, "All details filled up for main details");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc5));
				utilityFileWriteOP.writeToLog(tcid, "All details filled up for main details", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//a[contains(text(),'Address')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Address')]")).click();
				Thread.sleep(2000);

				driver.findElement(By.xpath("//input[@id='addressLine1']")).click();
				driver.findElement(By.xpath("//input[@id='addressLine1']")).sendKeys(AddressLine1);


				driver.findElement(By.xpath("//input[@id='postcodeId']")).click();
				driver.findElement(By.xpath("//input[@id='postcodeId']")).sendKeys("NG25 0BW");

				String wholesale_sc6=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc6, driver);
				logger.log(LogStatus.PASS, "Address details filled successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc6));
				utilityFileWriteOP.writeToLog(tcid, "Address details filled successfully", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).click();
				Thread.sleep(2000);
				Select depotType = new Select(driver.findElement(By.xpath("//select[@name='supplyDepotType']")));
				depotType.selectByVisibleText("Bury");
				Thread.sleep(2000);

				String wholesale_sc7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc7, driver);
				logger.log(LogStatus.PASS, "Supplying Depot details filled up successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc7));
				utilityFileWriteOP.writeToLog(tcid, "Supplying Depot details filled up successfully", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("//h4[@class='orders-heading' and contains(text(),'Current Opportunity')]")).click();

				driver.findElement(By.xpath("//input[@id='Histval_0']")).sendKeys("YYYYYYN");
				Thread.sleep(1000);

				driver.findElement(By.xpath("//input[@id='Histval_1']")).sendKeys("YYYYYYN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histval_2']")).sendKeys("YYYYYNN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histval_3']")).sendKeys("YYYYNYN");
				Thread.sleep(1000);



				String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
				logger.log(LogStatus.PASS, "Delivery days details filled up successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc8));
				utilityFileWriteOP.writeToLog(tcid, "Delivery days details filled up successfully", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).isDisplayed();
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).click();

				Thread.sleep(3000);
				driver.findElement(By.xpath("//div[contains(text(),'Store added')]")).isDisplayed();

				String wholesale_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc9, driver);
				logger.log(LogStatus.PASS, "New Store created successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc9));
				utilityFileWriteOP.writeToLog(tcid, "New Store created successfully", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//button[@type='button' and contains(text(),'✖')]")).click();

				res=true;

				break;
			}

			case "rontec":
				//**********************Rontec Customer

			{
				driver.findElement(By.xpath("//input[@id='storeNameId']")).click();

				String wholesale_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc3, driver);
				logger.log(LogStatus.PASS, "Store New entry page displayed");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc3));
				utilityFileWriteOP.writeToLog(tcid, "Store New entry page displayed", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//input[@id='storeNameId']")).sendKeys(storename);

				driver.findElement(By.xpath("//input[@id='storeID']")).sendKeys(storeid);

				driver.findElement(By.xpath("//input[@id='tradingNameId']")).sendKeys(tradingname);


				Select availability = new Select(driver.findElement(By.xpath("//select[@id='availability']")));
				availability.selectByVisibleText("ACTIVE");


				String wholesale_sc4=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc4, driver);
				logger.log(LogStatus.PASS, "All details filled up for Store details");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc4));
				utilityFileWriteOP.writeToLog(tcid, "All details filled up for Store details", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).click();


				Select allocation = new Select(driver.findElement(By.xpath("//select[@id='storeAllocationP']")));
				allocation.selectByVisibleText("P20000");
				Thread.sleep(2000);

				Select status = new Select(driver.findElement(By.xpath("//select[@id='statusId']")));
				status.selectByVisibleText("rollout");
				Thread.sleep(2000);
				String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
				logger.log(LogStatus.PASS, "All details filled up for main details");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc5));
				utilityFileWriteOP.writeToLog(tcid, "All details filled up for main details", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//a[contains(text(),'Address')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Address')]")).click();
				Thread.sleep(2000);

				driver.findElement(By.xpath("//input[@id='addressLine1']")).click();
				driver.findElement(By.xpath("//input[@id='addressLine1']")).sendKeys(AddressLine1);

				driver.findElement(By.xpath("//input[@id='contactEmailId']")).click();
				driver.findElement(By.xpath("//input[@id='contactEmailId']")).sendKeys(EmailId);

				driver.findElement(By.xpath("//input[@id='postcodeId']")).click();
				driver.findElement(By.xpath("//input[@id='postcodeId']")).sendKeys("TTT TTT");

				String wholesale_sc6=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc6, driver);
				logger.log(LogStatus.PASS, "Address details filled successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc6));
				utilityFileWriteOP.writeToLog(tcid, "Address details filled successfully", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).click();
				Thread.sleep(2000);
				Select depotType = new Select(driver.findElement(By.xpath("//select[@name='supplyDepotType']")));
				depotType.selectByVisibleText("Allington");
				Thread.sleep(2000);

				String wholesale_sc7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc7, driver);
				logger.log(LogStatus.PASS, "Supplying Depot details filled up successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc7));
				utilityFileWriteOP.writeToLog(tcid, "Supplying Depot details filled up successfully", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("//h4[@class='orders-heading' and contains(text(),'Current Opportunity')]")).click();

				driver.findElement(By.xpath("//input[@id='Histval_0']")).sendKeys("YYYYYYN");
				Thread.sleep(1000);

				driver.findElement(By.xpath("//input[@id='Histval_1']")).sendKeys("YYYYYYN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histval_2']")).sendKeys("YYYYYNN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histval_3']")).sendKeys("YYYYNYN");
				Thread.sleep(1000);


				String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
				logger.log(LogStatus.PASS, "Delivery days details filled up successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc8));
				utilityFileWriteOP.writeToLog(tcid, "Delivery days details filled up successfully", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).isDisplayed();
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).click();

				Thread.sleep(3000);
				driver.findElement(By.xpath("//div[contains(text(),'Store added')]")).isDisplayed();

				String wholesale_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc9, driver);
				logger.log(LogStatus.PASS, "New Store created successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc9));
				utilityFileWriteOP.writeToLog(tcid, "New Store created successfully", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//button[@type='button' and contains(text(),'✖')]")).click();

				res=true;

				break;
			}

			case "rontec-b":
				//**********************Rontec B Customer

			{
				driver.findElement(By.xpath("//input[@id='storeNameId']")).click();

				String wholesale_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc3, driver);
				logger.log(LogStatus.PASS, "Store New entry page displayed");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc3));
				utilityFileWriteOP.writeToLog(tcid, "Store New entry page displayed", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//input[@id='storeNameId']")).sendKeys(storename);

				driver.findElement(By.xpath("//input[@id='storeID']")).sendKeys(storeid);

				driver.findElement(By.xpath("//input[@id='tradingNameId']")).sendKeys(tradingname);


				Select availability = new Select(driver.findElement(By.xpath("//select[@id='availability']")));
				availability.selectByVisibleText("ACTIVE");


				String wholesale_sc4=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc4, driver);
				logger.log(LogStatus.PASS, "All details filled up for Store details");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc4));
				utilityFileWriteOP.writeToLog(tcid, "All details filled up for Store details", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).click();


				Select allocation = new Select(driver.findElement(By.xpath("//select[@id='storeAllocationP']")));
				allocation.selectByVisibleText("P30000");
				Thread.sleep(2000);

				Select status = new Select(driver.findElement(By.xpath("//select[@id='statusId']")));
				status.selectByVisibleText("rollout");
				Thread.sleep(2000);
				String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
				logger.log(LogStatus.PASS, "All details filled up for main details");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc5));
				utilityFileWriteOP.writeToLog(tcid, "All details filled up for main details", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//a[contains(text(),'Address')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Address')]")).click();
				Thread.sleep(2000);

				driver.findElement(By.xpath("//input[@id='addressLine1']")).click();
				driver.findElement(By.xpath("//input[@id='addressLine1']")).sendKeys(AddressLine1);


				driver.findElement(By.xpath("//input[@id='contactEmailId']")).click();
				driver.findElement(By.xpath("//input[@id='contactEmailId']")).sendKeys(EmailId);

				driver.findElement(By.xpath("//input[@id='postcodeId']")).click();
				driver.findElement(By.xpath("//input[@id='postcodeId']")).sendKeys("NN1 4GD");

				String wholesale_sc6=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc6, driver);
				logger.log(LogStatus.PASS, "Address details filled successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc6));
				utilityFileWriteOP.writeToLog(tcid, "Address details filled successfully", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).click();
				Thread.sleep(2000);
				Select depotType = new Select(driver.findElement(By.xpath("//select[@name='supplyDepotType']")));
				depotType.selectByVisibleText("Allington");
				Thread.sleep(2000);

				String wholesale_sc7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc7, driver);
				logger.log(LogStatus.PASS, "Supplying Depot details filled up successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc7));
				utilityFileWriteOP.writeToLog(tcid, "Supplying Depot details filled up successfully", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("//h4[@class='orders-heading' and contains(text(),'Current Opportunity')]")).click();

				driver.findElement(By.xpath("//input[@id='Histval_0']")).sendKeys("YYYYYYN");
				Thread.sleep(1000);

				driver.findElement(By.xpath("//input[@id='Histval_1']")).sendKeys("YYYYYYN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histval_2']")).sendKeys("YYYYYNN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histval_3']")).sendKeys("YYYYNYN");
				Thread.sleep(1000);


				String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
				logger.log(LogStatus.PASS, "Delivery days details filled up successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc8));
				utilityFileWriteOP.writeToLog(tcid, "Delivery days details filled up successfully", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).isDisplayed();
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).click();

				Thread.sleep(3000);
				driver.findElement(By.xpath("//div[contains(text(),'Store added')]")).isDisplayed();

				String wholesale_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc9, driver);
				logger.log(LogStatus.PASS, "New Store created successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc9));
				utilityFileWriteOP.writeToLog(tcid, "New Store created successfully", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//button[@type='button' and contains(text(),'✖')]")).click();

				res=true;

				break;
			}


			case "sandpiper":
				//**********************Sandpiper Customer

			{
				driver.findElement(By.xpath("//input[@id='storeNameId']")).click();

				String wholesale_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc3, driver);
				logger.log(LogStatus.PASS, "Store New entry page displayed");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc3));
				utilityFileWriteOP.writeToLog(tcid, "Store New entry page displayed", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//input[@id='storeNameId']")).sendKeys(storename);

				driver.findElement(By.xpath("//input[@id='storeID']")).sendKeys(storeid);

				driver.findElement(By.xpath("//input[@id='tradingNameId']")).sendKeys(tradingname);

				Select availability = new Select(driver.findElement(By.xpath("//select[@id='availability']")));
				availability.selectByVisibleText("ACTIVE");


				String wholesale_sc4=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc4, driver);
				logger.log(LogStatus.PASS, "All details filled up for Store details");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc4));
				utilityFileWriteOP.writeToLog(tcid, "All details filled up for Store details", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).click();


				Select allocation = new Select(driver.findElement(By.xpath("//select[@id='storeAllocationP']")));
				allocation.selectByVisibleText("P20000");
				Thread.sleep(2000);

				Select status = new Select(driver.findElement(By.xpath("//select[@id='statusId']")));
				status.selectByVisibleText("rollout");
				Thread.sleep(2000);
				String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
				logger.log(LogStatus.PASS, "All details filled up for main details");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc5));
				utilityFileWriteOP.writeToLog(tcid, "All details filled up for main details", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//a[contains(text(),'Address')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Address')]")).click();
				Thread.sleep(2000);

				driver.findElement(By.xpath("//input[@id='addressLine1']")).click();
				driver.findElement(By.xpath("//input[@id='addressLine1']")).sendKeys(AddressLine1);


				driver.findElement(By.xpath("//input[@id='postcodeId']")).click();
				driver.findElement(By.xpath("//input[@id='postcodeId']")).sendKeys("GY7 9RS");

				Select port = new Select(driver.findElement(By.xpath("//select[@id='storePort_id']")));
				port.selectByVisibleText("St Helier");

				String wholesale_sc6=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc6, driver);
				logger.log(LogStatus.PASS, "Address details filled successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc6));
				utilityFileWriteOP.writeToLog(tcid, "Address details filled successfully", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).click();
				Thread.sleep(2000);
				Select depotType = new Select(driver.findElement(By.xpath("//select[@name='supplyDepotType']")));
				depotType.selectByVisibleText("Bury");
				Thread.sleep(2000);

				String wholesale_sc7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc7, driver);
				logger.log(LogStatus.PASS, "Supplying Depot details filled up successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc7));
				utilityFileWriteOP.writeToLog(tcid, "Supplying Depot details filled up successfully", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("//h4[@class='orders-heading' and contains(text(),'Current Opportunity')]")).click();

				driver.findElement(By.xpath("//input[@id='Histval_0']")).sendKeys("YYYYYYN");
				Thread.sleep(1000);

				driver.findElement(By.xpath("//input[@id='Histval_1']")).sendKeys("YYYYYYN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histval_2']")).sendKeys("YYYYYNN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histval_3']")).sendKeys("YYYYNYN");
				Thread.sleep(1000);


				String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
				logger.log(LogStatus.PASS, "Delivery days details filled up successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc8));
				utilityFileWriteOP.writeToLog(tcid, "Delivery days details filled up successfully", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).isDisplayed();
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).click();

				Thread.sleep(3000);
				driver.findElement(By.xpath("//div[contains(text(),'Store added')]")).isDisplayed();

				String wholesale_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc9, driver);
				logger.log(LogStatus.PASS, "New Store created successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc9));
				utilityFileWriteOP.writeToLog(tcid, "New Store created successfully", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//button[@type='button' and contains(text(),'✖')]")).click();

				res=true;

				break;
			}
			default: System.out.println("unknown test");
			}


		}

		catch(Exception e){
			e.printStackTrace();
			String wholesale_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc9, driver);
			logger.log(LogStatus.FAIL, "Error occured while store creation");
			logger.log(LogStatus.FAIL, logger.addScreenCapture(wholesale_sc9));
			utilityFileWriteOP.writeToLog(tcid, "Error occured while store creation", "FAIL",ResultPath,xwpfRun);
			res=false;
		}finally{
			return res;
		}
	}




	public static boolean store_New_Entry_Immediate(String customer,String storename,String storeid,String tradingname,String merchantid,String AddressLine1,String EmailId,WebDriver driver, WebDriverWait wait, String tcid,String ResultPath,XWPFRun xwpfRun, ExtentTest logger){
		boolean res=false;
		try{

			ResultPath=System.getProperty("user.dir")+"/"+ResultPath;

			Thread.sleep(4000);

			driver.findElement(By.xpath("//span[@class='menu-product-name-inline dis-inlineblc padd-top-5 text-left']")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("//h5[contains(text(),'Store  New Entry')]")).isDisplayed();
			driver.findElement(By.xpath("//h5[contains(text(),'Store  New Entry')]")).click();
			Thread.sleep(5000);

			String currentWindow=driver.getWindowHandle();
			Set<String> windows=driver.getWindowHandles();

			for(String window:windows)
			{
				if(!window.equals(currentWindow))
				{
					driver.switchTo().window(window);
					//utilityFileWriteOP.writeToLog(tcid, "AWS Console Window", "Switched!!!",ResultPath, xwpfRun,"");
					break;
				}
			}

			switch (customer){
			case "amazon":

				//***********Amazon customer
			{
				driver.findElement(By.xpath("//input[@id='storeNameId']")).click();

				String wholesale_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc3, driver);
				logger.log(LogStatus.PASS, "Store New entry page displayed");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc3));
				utilityFileWriteOP.writeToLog(tcid, "Store New entry page displayed", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//input[@id='storeNameId']")).sendKeys(storename);

				driver.findElement(By.xpath("//input[@id='storeID']")).sendKeys(storeid);

				driver.findElement(By.xpath("//label[@for='effectiveImmediatelyId']")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("//div[@id='immediatelyModalPageLevel']/div/div/div[2]/div/button[@type='button' and contains(text(),'Yes')]")).click();
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='tradingNameId']")).sendKeys(tradingname);


				driver.findElement(By.xpath("//input[@id='merchantId']")).sendKeys(merchantid);

				Select availability = new Select(driver.findElement(By.xpath("//select[@id='availability']")));
				availability.selectByVisibleText("ACTIVE");

				String wholesale_sc4=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc4, driver);
				logger.log(LogStatus.PASS, "All details filled up for Store details");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc4));
				utilityFileWriteOP.writeToLog(tcid, "All details filled up for Store details", "PASS",ResultPath,xwpfRun);


				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).click();


				Select regionCode = new Select(driver.findElement(By.xpath("//select[@id='storeRegionP']")));
				regionCode.selectByVisibleText("England");
				Thread.sleep(2000);

				Select status = new Select(driver.findElement(By.xpath("//select[@id='statusId']")));
				status.selectByVisibleText("rollout");
				Thread.sleep(2000);
				String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
				logger.log(LogStatus.PASS, "All details filled up for main details");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc5));
				utilityFileWriteOP.writeToLog(tcid, "All details filled up for main details", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Address')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Address')]")).click();
				Thread.sleep(2000);

				driver.findElement(By.xpath("//input[@id='addressLine1']")).click();
				driver.findElement(By.xpath("//input[@id='addressLine1']")).sendKeys(AddressLine1);

				String wholesale_sc6=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc6, driver);
				logger.log(LogStatus.PASS, "Address details filled successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc6));
				utilityFileWriteOP.writeToLog(tcid, "Address details filled successfully", "PASS",ResultPath,xwpfRun);



				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).click();
				Thread.sleep(2000);


				String wholesale_sc7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc7, driver);
				logger.log(LogStatus.PASS, "Supplying Depot details displayed successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc7));
				utilityFileWriteOP.writeToLog(tcid, "Supplying Depot details displayed successfully", "PASS",ResultPath,xwpfRun);


				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).click();
				Thread.sleep(2000);

				driver.findElement(By.xpath("//input[@id='Histval_0']")).sendKeys("YYYYYYN");

				String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
				logger.log(LogStatus.PASS, "Delivery days details filled up successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc8));
				utilityFileWriteOP.writeToLog(tcid, "Delivery days details filled up successfully", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).isDisplayed();
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).click();

				Thread.sleep(3000);
				driver.findElement(By.xpath("//div[contains(text(),'Store added')]")).isDisplayed();

				String wholesale_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc9, driver);
				logger.log(LogStatus.PASS, "New Store created successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc9));
				utilityFileWriteOP.writeToLog(tcid, "New Store created successfully", "PASS",ResultPath,xwpfRun);


				driver.findElement(By.xpath("//button[@type='button' and contains(text(),'✖')]")).click();

				res=true;

				break;
			}
			case "harvest":
				//**********************Harvest Customer

			{
				driver.findElement(By.xpath("//input[@id='storeNameId']")).click();

				String wholesale_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc3, driver);
				logger.log(LogStatus.PASS, "Store New entry page displayed");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc3));
				utilityFileWriteOP.writeToLog(tcid, "Store New entry page displayed", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//input[@id='storeNameId']")).sendKeys(storename);

				driver.findElement(By.xpath("//input[@id='storeID']")).sendKeys(storeid);

				driver.findElement(By.xpath("//label[@for='effectiveImmediatelyId']")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("//div[@id='immediatelyModalPageLevel']/div/div/div[2]/div/button[@type='button' and contains(text(),'Yes')]")).click();
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='tradingNameId']")).sendKeys(tradingname);


				Select availability = new Select(driver.findElement(By.xpath("//select[@id='availability']")));
				availability.selectByVisibleText("ACTIVE");


				String wholesale_sc4=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc4, driver);
				logger.log(LogStatus.PASS, "All details filled up for Store details");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc4));
				utilityFileWriteOP.writeToLog(tcid, "All details filled up for Store details", "PASS",ResultPath,xwpfRun);


				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).click();


				Select allocation = new Select(driver.findElement(By.xpath("//select[@id='storeAllocationP']")));
				allocation.selectByVisibleText("P2333");
				Thread.sleep(2000);

				Select status = new Select(driver.findElement(By.xpath("//select[@id='statusId']")));
				status.selectByVisibleText("rollout");
				Thread.sleep(2000);
				String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
				logger.log(LogStatus.PASS, "All details filled up for main details");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc5));
				utilityFileWriteOP.writeToLog(tcid, "All details filled up for main details", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//a[contains(text(),'Address')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Address')]")).click();
				Thread.sleep(2000);

				driver.findElement(By.xpath("//input[@id='addressLine1']")).click();
				driver.findElement(By.xpath("//input[@id='addressLine1']")).sendKeys(AddressLine1);


				driver.findElement(By.xpath("//input[@id='postcodeId']")).click();
				driver.findElement(By.xpath("//input[@id='postcodeId']")).sendKeys("NN172AE");

				String wholesale_sc6=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc6, driver);
				logger.log(LogStatus.PASS, "Address details filled successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc6));
				utilityFileWriteOP.writeToLog(tcid, "Address details filled successfully", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).click();
				Thread.sleep(2000);
				Select depotType = new Select(driver.findElement(By.xpath("//select[@name='supplyDepotType']")));
				depotType.selectByVisibleText("Bury");
				Thread.sleep(2000);

				String wholesale_sc7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc7, driver);
				logger.log(LogStatus.PASS, "Supplying Depot details displayed successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc7));
				utilityFileWriteOP.writeToLog(tcid, "Supplying Depot details displayed successfully", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("//h4[@class='orders-heading' and contains(text(),'Current Opportunity')]")).click();

				driver.findElement(By.xpath("//input[@id='Histval_0']")).sendKeys("YYYYYYN");
				Thread.sleep(1000);

				driver.findElement(By.xpath("//input[@id='Histval_1']")).sendKeys("YYYYYYN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histval_2']")).sendKeys("YYYYYNN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histval_3']")).sendKeys("YYYYNYN");
				Thread.sleep(1000);
				String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
				logger.log(LogStatus.PASS, "Delivery days details filled up successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc8));
				utilityFileWriteOP.writeToLog(tcid, "Delivery days details filled up successfully", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).isDisplayed();
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).click();

				Thread.sleep(3000);
				driver.findElement(By.xpath("//div[contains(text(),'Store added')]")).isDisplayed();

				String wholesale_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc9, driver);
				logger.log(LogStatus.PASS, "New Store created successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc9));
				utilityFileWriteOP.writeToLog(tcid, "New Store created successfully", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//button[@type='button' and contains(text(),'✖')]")).click();

				res=true;

				break;
			}

			case "mccolls":
				//**********************McColls Customer

			{
				driver.findElement(By.xpath("//input[@id='storeNameId']")).click();

				String wholesale_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc3, driver);
				logger.log(LogStatus.PASS, "Store New entry page displayed");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc3));
				utilityFileWriteOP.writeToLog(tcid, "Store New entry page displayed", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//input[@id='storeNameId']")).sendKeys(storename);

				driver.findElement(By.xpath("//input[@id='storeID']")).sendKeys(storeid);

				driver.findElement(By.xpath("//label[@for='effectiveImmediatelyId']")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("//div[@id='immediatelyModalPageLevel']/div/div/div[2]/div/button[@type='button' and contains(text(),'Yes')]")).click();
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='tradingNameId']")).sendKeys(tradingname);


				Select availability = new Select(driver.findElement(By.xpath("//select[@id='availability']")));
				availability.selectByVisibleText("ACTIVE");


				String wholesale_sc4=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc4, driver);
				logger.log(LogStatus.PASS, "All details filled up for Store details");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc4));
				utilityFileWriteOP.writeToLog(tcid, "All details filled up for Store details", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).click();


				Select allocation = new Select(driver.findElement(By.xpath("//select[@id='storeAllocationP']")));
				allocation.selectByVisibleText("P2333");
				Thread.sleep(2000);

				Select status = new Select(driver.findElement(By.xpath("//select[@id='statusId']")));
				status.selectByVisibleText("rollout");
				Thread.sleep(2000);
				String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
				logger.log(LogStatus.PASS, "All details filled up for main details");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc5));
				utilityFileWriteOP.writeToLog(tcid, "All details filled up for main details", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//a[contains(text(),'Address')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Address')]")).click();
				Thread.sleep(2000);

				driver.findElement(By.xpath("//input[@id='addressLine1']")).click();
				driver.findElement(By.xpath("//input[@id='addressLine1']")).sendKeys(AddressLine1);


				driver.findElement(By.xpath("//input[@id='postcodeId']")).click();
				driver.findElement(By.xpath("//input[@id='postcodeId']")).sendKeys("SS7 4EN");

				String wholesale_sc6=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc6, driver);
				logger.log(LogStatus.PASS, "Address details filled successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc6));
				utilityFileWriteOP.writeToLog(tcid, "Address details filled successfully", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).click();
				Thread.sleep(2000);
				Select depotType = new Select(driver.findElement(By.xpath("//select[@name='supplyDepotType']")));
				depotType.selectByVisibleText("Bury");
				Thread.sleep(2000);

				String wholesale_sc7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc7, driver);
				logger.log(LogStatus.PASS, "Supplying Depot details displayed successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc7));
				utilityFileWriteOP.writeToLog(tcid, "Supplying Depot details displayed successfully", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("//h4[@class='orders-heading' and contains(text(),'Current Opportunity')]")).click();

				driver.findElement(By.xpath("//input[@id='Histval_0']")).sendKeys("YYYYYYN");
				Thread.sleep(1000);

				driver.findElement(By.xpath("//input[@id='Histval_1']")).sendKeys("YYYYYYN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histval_2']")).sendKeys("YYYYYNN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histval_3']")).sendKeys("YYYYNYN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histval_4']")).sendKeys("YYYYNYN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histval_5']")).sendKeys("YYYYYYN");
				Thread.sleep(1000);

				String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
				logger.log(LogStatus.PASS, "Delivery days details filled up successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc8));
				utilityFileWriteOP.writeToLog(tcid, "Delivery days details filled up successfully", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).isDisplayed();
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).click();

				Thread.sleep(3000);
				driver.findElement(By.xpath("//div[contains(text(),'Store added')]")).isDisplayed();

				String wholesale_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc9, driver);
				logger.log(LogStatus.PASS, "New Store created successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc9));
				utilityFileWriteOP.writeToLog(tcid, "New Store created successfully", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//button[@type='button' and contains(text(),'✖')]")).click();

				res=true;

				break;
			}

			case "mcdly":
				//**********************McDaily Customer

			{
				driver.findElement(By.xpath("//input[@id='storeNameId']")).click();

				String wholesale_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc3, driver);
				logger.log(LogStatus.PASS, "Store New entry page displayed");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc3));
				utilityFileWriteOP.writeToLog(tcid, "Store New entry page displayed", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//input[@id='storeNameId']")).sendKeys(storename);

				driver.findElement(By.xpath("//input[@id='storeID']")).sendKeys(storeid);

				driver.findElement(By.xpath("//label[@for='effectiveImmediatelyId']")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("//div[@id='immediatelyModalPageLevel']/div/div/div[2]/div/button[@type='button' and contains(text(),'Yes')]")).click();
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='tradingNameId']")).sendKeys(tradingname);


				Select availability = new Select(driver.findElement(By.xpath("//select[@id='availability']")));
				availability.selectByVisibleText("ACTIVE");


				String wholesale_sc4=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc4, driver);
				logger.log(LogStatus.PASS, "All details filled up for Store details");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc4));
				utilityFileWriteOP.writeToLog(tcid, "All details filled up for Store details", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).click();


				Select allocation = new Select(driver.findElement(By.xpath("//select[@id='storeAllocationP']")));
				allocation.selectByVisibleText("P20000");
				Thread.sleep(2000);

				Select status = new Select(driver.findElement(By.xpath("//select[@id='statusId']")));
				status.selectByVisibleText("rollout");
				Thread.sleep(2000);
				String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
				logger.log(LogStatus.PASS, "All details filled up for main details");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc5));
				utilityFileWriteOP.writeToLog(tcid, "All details filled up for main details", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//a[contains(text(),'Address')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Address')]")).click();
				Thread.sleep(2000);

				driver.findElement(By.xpath("//input[@id='addressLine1']")).click();
				driver.findElement(By.xpath("//input[@id='addressLine1']")).sendKeys(AddressLine1);


				driver.findElement(By.xpath("//input[@id='postcodeId']")).click();
				driver.findElement(By.xpath("//input[@id='postcodeId']")).sendKeys("JE2 4TA");

				String wholesale_sc6=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc6, driver);
				logger.log(LogStatus.PASS, "Address details filled successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc6));
				utilityFileWriteOP.writeToLog(tcid, "Address details filled successfully", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).click();
				Thread.sleep(2000);
				Select depotType = new Select(driver.findElement(By.xpath("//select[@name='supplyDepotType']")));
				depotType.selectByVisibleText("Bury");
				Thread.sleep(2000);

				String wholesale_sc7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc7, driver);
				logger.log(LogStatus.PASS, "Supplying Depot details displayed successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc7));
				utilityFileWriteOP.writeToLog(tcid, "Supplying Depot details displayed successfully", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("//h4[@class='orders-heading' and contains(text(),'Current Opportunity')]")).click();

				driver.findElement(By.xpath("//input[@id='Histval_0']")).sendKeys("YYYYYYN");
				Thread.sleep(1000);

				driver.findElement(By.xpath("//input[@id='Histval_1']")).sendKeys("YYYYYYN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histval_2']")).sendKeys("YYYYYNN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histval_3']")).sendKeys("YYYYNYN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histval_4']")).sendKeys("YYYYNYN");
				Thread.sleep(1000);


				String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
				logger.log(LogStatus.PASS, "Delivery days details filled up successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc8));
				utilityFileWriteOP.writeToLog(tcid, "Delivery days details filled up successfully", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).isDisplayed();
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).click();

				Thread.sleep(3000);
				driver.findElement(By.xpath("//div[contains(text(),'Store added')]")).isDisplayed();

				String wholesale_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc9, driver);
				logger.log(LogStatus.PASS, "New Store created successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc9));
				utilityFileWriteOP.writeToLog(tcid, "New Store created successfully", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//button[@type='button' and contains(text(),'✖')]")).click();

				res=true;

				break;
			}

			case "mpk":
				//**********************MPK Customer

			{
				driver.findElement(By.xpath("//input[@id='storeNameId']")).click();

				String wholesale_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc3, driver);
				logger.log(LogStatus.PASS, "Store New entry page displayed");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc3));
				utilityFileWriteOP.writeToLog(tcid, "Store New entry page displayed", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//input[@id='storeNameId']")).sendKeys(storename);

				driver.findElement(By.xpath("//input[@id='storeID']")).sendKeys(storeid);

				driver.findElement(By.xpath("//label[@for='effectiveImmediatelyId']")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("//div[@id='immediatelyModalPageLevel']/div/div/div[2]/div/button[@type='button' and contains(text(),'Yes')]")).click();
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='tradingNameId']")).sendKeys(tradingname);


				Select availability = new Select(driver.findElement(By.xpath("//select[@id='availability']")));
				availability.selectByVisibleText("ACTIVE");


				String wholesale_sc4=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc4, driver);
				logger.log(LogStatus.PASS, "All details filled up for Store details");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc4));
				utilityFileWriteOP.writeToLog(tcid, "All details filled up for Store details", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).click();


				Select allocation = new Select(driver.findElement(By.xpath("//select[@id='storeAllocationP']")));
				allocation.selectByVisibleText("P20000");
				Thread.sleep(2000);

				Select status = new Select(driver.findElement(By.xpath("//select[@id='statusId']")));
				status.selectByVisibleText("rollout");
				Thread.sleep(2000);
				String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
				logger.log(LogStatus.PASS, "All details filled up for main details");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc5));
				utilityFileWriteOP.writeToLog(tcid, "All details filled up for main details", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//a[contains(text(),'Address')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Address')]")).click();
				Thread.sleep(2000);

				driver.findElement(By.xpath("//input[@id='addressLine1']")).click();
				driver.findElement(By.xpath("//input[@id='addressLine1']")).sendKeys(AddressLine1);


				driver.findElement(By.xpath("//input[@id='postcodeId']")).click();
				driver.findElement(By.xpath("//input[@id='postcodeId']")).sendKeys("NG25 0BW");

				String wholesale_sc6=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc6, driver);
				logger.log(LogStatus.PASS, "Address details filled successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc6));
				utilityFileWriteOP.writeToLog(tcid, "Address details filled successfully", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).click();
				Thread.sleep(2000);
				Select depotType = new Select(driver.findElement(By.xpath("//select[@name='supplyDepotType']")));
				depotType.selectByVisibleText("Bury");
				Thread.sleep(2000);

				String wholesale_sc7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc7, driver);
				logger.log(LogStatus.PASS, "Supplying Depot details displayed successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc7));
				utilityFileWriteOP.writeToLog(tcid, "Supplying Depot details displayed successfully", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("//h4[@class='orders-heading' and contains(text(),'Current Opportunity')]")).click();

				driver.findElement(By.xpath("//input[@id='Histval_0']")).sendKeys("YYYYYYN");
				Thread.sleep(1000);

				driver.findElement(By.xpath("//input[@id='Histval_1']")).sendKeys("YYYYYYN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histval_2']")).sendKeys("YYYYYNN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histval_3']")).sendKeys("YYYYNYN");
				Thread.sleep(1000);



				String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
				logger.log(LogStatus.PASS, "Delivery days details filled up successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc8));
				utilityFileWriteOP.writeToLog(tcid, "Delivery days details filled up successfully", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).isDisplayed();
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).click();

				Thread.sleep(3000);
				driver.findElement(By.xpath("//div[contains(text(),'Store added')]")).isDisplayed();

				String wholesale_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc9, driver);
				logger.log(LogStatus.PASS, "New Store created successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc9));
				utilityFileWriteOP.writeToLog(tcid, "New Store created successfully", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//button[@type='button' and contains(text(),'✖')]")).click();

				res=true;

				break;
			}

			case "rontec":
				//**********************Rontec Customer

			{
				driver.findElement(By.xpath("//input[@id='storeNameId']")).click();

				String wholesale_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc3, driver);
				logger.log(LogStatus.PASS, "Store New entry page displayed");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc3));
				utilityFileWriteOP.writeToLog(tcid, "Store New entry page displayed", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//input[@id='storeNameId']")).sendKeys(storename);

				driver.findElement(By.xpath("//input[@id='storeID']")).sendKeys(storeid);

				driver.findElement(By.xpath("//label[@for='effectiveImmediatelyId']")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("//div[@id='immediatelyModalPageLevel']/div/div/div[2]/div/button[@type='button' and contains(text(),'Yes')]")).click();
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='tradingNameId']")).sendKeys(tradingname);


				Select availability = new Select(driver.findElement(By.xpath("//select[@id='availability']")));
				availability.selectByVisibleText("ACTIVE");


				String wholesale_sc4=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc4, driver);
				logger.log(LogStatus.PASS, "All details filled up for Store details");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc4));
				utilityFileWriteOP.writeToLog(tcid, "All details filled up for Store details", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).click();


				Select allocation = new Select(driver.findElement(By.xpath("//select[@id='storeAllocationP']")));
				allocation.selectByVisibleText("P20000");
				Thread.sleep(2000);

				Select status = new Select(driver.findElement(By.xpath("//select[@id='statusId']")));
				status.selectByVisibleText("rollout");
				Thread.sleep(2000);
				String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
				logger.log(LogStatus.PASS, "All details filled up for main details");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc5));
				utilityFileWriteOP.writeToLog(tcid, "All details filled up for main details", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//a[contains(text(),'Address')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Address')]")).click();
				Thread.sleep(2000);

				driver.findElement(By.xpath("//input[@id='addressLine1']")).click();
				driver.findElement(By.xpath("//input[@id='addressLine1']")).sendKeys(AddressLine1);

				driver.findElement(By.xpath("//input[@id='contactEmailId']")).click();
				driver.findElement(By.xpath("//input[@id='contactEmailId']")).sendKeys(EmailId);

				driver.findElement(By.xpath("//input[@id='postcodeId']")).click();
				driver.findElement(By.xpath("//input[@id='postcodeId']")).sendKeys("TTT TTT");

				String wholesale_sc6=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc6, driver);
				logger.log(LogStatus.PASS, "Address details filled successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc6));
				utilityFileWriteOP.writeToLog(tcid, "Address details filled successfully", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).click();
				Thread.sleep(2000);
				Select depotType = new Select(driver.findElement(By.xpath("//select[@name='supplyDepotType']")));
				depotType.selectByVisibleText("Allington");
				Thread.sleep(2000);

				String wholesale_sc7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc7, driver);
				logger.log(LogStatus.PASS, "Supplying Depot details displayed successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc7));
				utilityFileWriteOP.writeToLog(tcid, "Supplying Depot details displayed successfully", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("//h4[@class='orders-heading' and contains(text(),'Current Opportunity')]")).click();

				driver.findElement(By.xpath("//input[@id='Histval_0']")).sendKeys("YYYYYYN");
				Thread.sleep(1000);

				driver.findElement(By.xpath("//input[@id='Histval_1']")).sendKeys("YYYYYYN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histval_2']")).sendKeys("YYYYYNN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histval_3']")).sendKeys("YYYYNYN");
				Thread.sleep(1000);


				String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
				logger.log(LogStatus.PASS, "Delivery days details filled up successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc8));
				utilityFileWriteOP.writeToLog(tcid, "Delivery days details filled up successfully", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).isDisplayed();
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).click();

				Thread.sleep(3000);
				driver.findElement(By.xpath("//div[contains(text(),'Store added')]")).isDisplayed();

				String wholesale_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc9, driver);
				logger.log(LogStatus.PASS, "New Store created successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc9));
				utilityFileWriteOP.writeToLog(tcid, "New Store created successfully", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//button[@type='button' and contains(text(),'✖')]")).click();

				res=true;

				break;
			}

			case "rontec-b":
				//**********************Rontec B Customer

			{
				driver.findElement(By.xpath("//input[@id='storeNameId']")).click();

				String wholesale_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc3, driver);
				logger.log(LogStatus.PASS, "Store New entry page displayed");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc3));
				utilityFileWriteOP.writeToLog(tcid, "Store New entry page displayed", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//input[@id='storeNameId']")).sendKeys(storename);

				driver.findElement(By.xpath("//input[@id='storeID']")).sendKeys(storeid);

				driver.findElement(By.xpath("//label[@for='effectiveImmediatelyId']")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("//div[@id='immediatelyModalPageLevel']/div/div/div[2]/div/button[@type='button' and contains(text(),'Yes')]")).click();
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='tradingNameId']")).sendKeys(tradingname);


				Select availability = new Select(driver.findElement(By.xpath("//select[@id='availability']")));
				availability.selectByVisibleText("ACTIVE");


				String wholesale_sc4=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc4, driver);
				logger.log(LogStatus.PASS, "All details filled up for Store details");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc4));
				utilityFileWriteOP.writeToLog(tcid, "All details filled up for Store details", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).click();


				Select allocation = new Select(driver.findElement(By.xpath("//select[@id='storeAllocationP']")));
				allocation.selectByVisibleText("P30000");
				Thread.sleep(2000);

				Select status = new Select(driver.findElement(By.xpath("//select[@id='statusId']")));
				status.selectByVisibleText("rollout");
				Thread.sleep(2000);
				String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
				logger.log(LogStatus.PASS, "All details filled up for main details");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc5));
				utilityFileWriteOP.writeToLog(tcid, "All details filled up for main details", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//a[contains(text(),'Address')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Address')]")).click();
				Thread.sleep(2000);

				driver.findElement(By.xpath("//input[@id='addressLine1']")).click();
				driver.findElement(By.xpath("//input[@id='addressLine1']")).sendKeys(AddressLine1);


				driver.findElement(By.xpath("//input[@id='contactEmailId']")).click();
				driver.findElement(By.xpath("//input[@id='contactEmailId']")).sendKeys(EmailId);

				driver.findElement(By.xpath("//input[@id='postcodeId']")).click();
				driver.findElement(By.xpath("//input[@id='postcodeId']")).sendKeys("NN1 4GD");

				String wholesale_sc6=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc6, driver);
				logger.log(LogStatus.PASS, "Address details filled successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc6));
				utilityFileWriteOP.writeToLog(tcid, "Address details filled successfully", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).click();
				Thread.sleep(2000);
				Select depotType = new Select(driver.findElement(By.xpath("//select[@name='supplyDepotType']")));
				depotType.selectByVisibleText("Allington");
				Thread.sleep(2000);

				String wholesale_sc7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc7, driver);
				logger.log(LogStatus.PASS, "Supplying Depot details displayed successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc7));
				utilityFileWriteOP.writeToLog(tcid, "Supplying Depot details displayed successfully", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("//h4[@class='orders-heading' and contains(text(),'Current Opportunity')]")).click();

				driver.findElement(By.xpath("//input[@id='Histval_0']")).sendKeys("YYYYYYN");
				Thread.sleep(1000);

				driver.findElement(By.xpath("//input[@id='Histval_1']")).sendKeys("YYYYYYN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histval_2']")).sendKeys("YYYYYNN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histval_3']")).sendKeys("YYYYNYN");
				Thread.sleep(1000);


				String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
				logger.log(LogStatus.PASS, "Delivery days details filled up successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc8));
				utilityFileWriteOP.writeToLog(tcid, "Delivery days details filled up successfully", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).isDisplayed();
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).click();

				Thread.sleep(3000);
				driver.findElement(By.xpath("//div[contains(text(),'Store added')]")).isDisplayed();

				String wholesale_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc9, driver);
				logger.log(LogStatus.PASS, "New Store created successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc9));
				utilityFileWriteOP.writeToLog(tcid, "New Store created successfully", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//button[@type='button' and contains(text(),'✖')]")).click();

				res=true;

				break;
			}


			case "sandpiper":
				//**********************Sandpiper Customer

			{
				driver.findElement(By.xpath("//input[@id='storeNameId']")).click();

				String wholesale_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc3, driver);
				logger.log(LogStatus.PASS, "Store New entry page displayed");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc3));
				utilityFileWriteOP.writeToLog(tcid, "Store New entry page displayed", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//input[@id='storeNameId']")).sendKeys(storename);

				driver.findElement(By.xpath("//input[@id='storeID']")).sendKeys(storeid);

				driver.findElement(By.xpath("//label[@for='effectiveImmediatelyId']")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("//div[@id='immediatelyModalPageLevel']/div/div/div[2]/div/button[@type='button' and contains(text(),'Yes')]")).click();
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='tradingNameId']")).sendKeys(tradingname);


				Select availability = new Select(driver.findElement(By.xpath("//select[@id='availability']")));
				availability.selectByVisibleText("ACTIVE");


				String wholesale_sc4=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc4, driver);
				logger.log(LogStatus.PASS, "All details filled up for Store details");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc4));
				utilityFileWriteOP.writeToLog(tcid, "All details filled up for Store details", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).click();


				Select allocation = new Select(driver.findElement(By.xpath("//select[@id='storeAllocationP']")));
				allocation.selectByVisibleText("P20000");
				Thread.sleep(2000);

				Select status = new Select(driver.findElement(By.xpath("//select[@id='statusId']")));
				status.selectByVisibleText("rollout");
				Thread.sleep(2000);
				String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
				logger.log(LogStatus.PASS, "All details filled up for main details");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc5));
				utilityFileWriteOP.writeToLog(tcid, "All details filled up for main details", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//a[contains(text(),'Address')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Address')]")).click();
				Thread.sleep(2000);

				driver.findElement(By.xpath("//input[@id='addressLine1']")).click();
				driver.findElement(By.xpath("//input[@id='addressLine1']")).sendKeys(AddressLine1);


				driver.findElement(By.xpath("//input[@id='postcodeId']")).click();
				driver.findElement(By.xpath("//input[@id='postcodeId']")).sendKeys("GY7 9RS");

				Select port = new Select(driver.findElement(By.xpath("//select[@id='storePort_id']")));
				port.selectByVisibleText("St Helier");

				String wholesale_sc6=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc6, driver);
				logger.log(LogStatus.PASS, "Address details filled successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc6));
				utilityFileWriteOP.writeToLog(tcid, "Address details filled successfully", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).click();
				Thread.sleep(2000);
				Select depotType = new Select(driver.findElement(By.xpath("//select[@name='supplyDepotType']")));
				depotType.selectByVisibleText("Bury");
				Thread.sleep(2000);

				String wholesale_sc7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc7, driver);
				logger.log(LogStatus.PASS, "Supplying Depot details displayed successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc7));
				utilityFileWriteOP.writeToLog(tcid, "Supplying Depot details displayed successfully", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("//h4[@class='orders-heading' and contains(text(),'Current Opportunity')]")).click();

				driver.findElement(By.xpath("//input[@id='Histval_0']")).sendKeys("YYYYYYN");
				Thread.sleep(1000);

				driver.findElement(By.xpath("//input[@id='Histval_1']")).sendKeys("YYYYYYN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histval_2']")).sendKeys("YYYYYNN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histval_3']")).sendKeys("YYYYNYN");
				Thread.sleep(1000);


				String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
				logger.log(LogStatus.PASS, "Delivery days details filled up successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc8));
				utilityFileWriteOP.writeToLog(tcid, "Delivery days details filled up successfully", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).isDisplayed();
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).click();

				Thread.sleep(3000);
				driver.findElement(By.xpath("//div[contains(text(),'Store added')]")).isDisplayed();

				String wholesale_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc9, driver);
				logger.log(LogStatus.PASS, "New Store created successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc9));
				utilityFileWriteOP.writeToLog(tcid, "New Store created successfully", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//button[@type='button' and contains(text(),'✖')]")).click();

				res=true;

				break;
			}
			default: System.out.println("unknown test");
			}


		}

		catch(Exception e){
			e.printStackTrace();
			String wholesale_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc9, driver);
			logger.log(LogStatus.FAIL, "Error occured while store creation");
			logger.log(LogStatus.FAIL, logger.addScreenCapture(wholesale_sc9));
			utilityFileWriteOP.writeToLog(tcid, "Error occured while store creation", "FAIL",ResultPath,xwpfRun);
			res=false;
		}finally{
			return res;
		}
	}


	public static boolean store_Entry_Future_Day(String customer,String storename,String storeid,String tradingname,String merchantid,String AddressLine1,String EmailId,String effectiveDate,WebDriver driver, WebDriverWait wait, String tcid,String ResultPath,XWPFRun xwpfRun, ExtentTest logger){
		boolean res=false;
		try{

			ResultPath=System.getProperty("user.dir")+"/"+ResultPath;

			Thread.sleep(4000);

			driver.findElement(By.xpath("//span[@class='menu-product-name-inline dis-inlineblc padd-top-5 text-left']")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("//h5[contains(text(),'Store  New Entry')]")).isDisplayed();
			driver.findElement(By.xpath("//h5[contains(text(),'Store  New Entry')]")).click();
			Thread.sleep(5000);

			String currentWindow=driver.getWindowHandle();
			Set<String> windows=driver.getWindowHandles();

			for(String window:windows)
			{
				if(!window.equals(currentWindow))
				{
					driver.switchTo().window(window);
					//utilityFileWriteOP.writeToLog(tcid, "AWS Console Window", "Switched!!!",ResultPath, xwpfRun,"");
					break;
				}
			}

			switch (customer){
			case "amazon":

				//***********Amazon customer
			{
				driver.findElement(By.xpath("//input[@id='storeNameId']")).click();

				String wholesale_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc3, driver);
				logger.log(LogStatus.PASS, "Store New entry page displayed");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc3));
				utilityFileWriteOP.writeToLog(tcid, "Store New entry page displayed", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//input[@id='storeNameId']")).sendKeys(storename);

				driver.findElement(By.xpath("//input[@id='storeID']")).sendKeys(storeid);

				driver.findElement(By.xpath("//input[@id='tradingNameId']")).sendKeys(tradingname);
				driver.findElement(By.xpath("//input[@id='merchantId']")).sendKeys(merchantid);

				Select availability = new Select(driver.findElement(By.xpath("//select[@id='availability']")));
				availability.selectByVisibleText("ACTIVE");

				String wholesale_sc4=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc4, driver);
				logger.log(LogStatus.PASS, "All details filled up for Store details");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc4));
				utilityFileWriteOP.writeToLog(tcid, "All details filled up for Store details", "PASS",ResultPath,xwpfRun);


				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).click();


				Select regionCode = new Select(driver.findElement(By.xpath("//select[@id='storeRegionP']")));
				regionCode.selectByVisibleText("England");
				Thread.sleep(2000);

				Select status = new Select(driver.findElement(By.xpath("//select[@id='statusId']")));
				status.selectByVisibleText("rollout");
				Thread.sleep(2000);
				String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
				logger.log(LogStatus.PASS, "All details filled up for main details");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc5));
				utilityFileWriteOP.writeToLog(tcid, "All details filled up for main details", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Address')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Address')]")).click();
				Thread.sleep(2000);

				driver.findElement(By.xpath("//input[@id='addressLine1']")).click();
				driver.findElement(By.xpath("//input[@id='addressLine1']")).sendKeys(AddressLine1);

				String wholesale_sc6=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc6, driver);
				logger.log(LogStatus.PASS, "Address details filled successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc6));
				utilityFileWriteOP.writeToLog(tcid, "Address details filled successfully", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).click();
				Thread.sleep(2000);

				String wholesale_sc7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc7, driver);
				logger.log(LogStatus.PASS, "Supplying Depot details displayed successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc7));
				utilityFileWriteOP.writeToLog(tcid, "Supplying Depot details displayed successfully", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).click();
				Thread.sleep(2000);

				driver.findElement(By.xpath("//input[@id='Histval_0']")).sendKeys("YYYYYYN");

				driver.findElement(By.xpath("//input[@id='Histcal_0']")).clear();
				driver.findElement(By.xpath("//input[@id='Histcal_0']")).sendKeys(effectiveDate);

				String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
				logger.log(LogStatus.PASS, "Delivery days details filled up successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc8));
				utilityFileWriteOP.writeToLog(tcid, "Delivery days details filled up successfully", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).isDisplayed();
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).click();

				Thread.sleep(3000);
				driver.findElement(By.xpath("//div[contains(text(),'Store added')]")).isDisplayed();

				String wholesale_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc9, driver);
				logger.log(LogStatus.PASS, "New Store created successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc9));
				utilityFileWriteOP.writeToLog(tcid, "New Store created successfully", "PASS",ResultPath,xwpfRun);


				driver.findElement(By.xpath("//button[@type='button' and contains(text(),'✖')]")).click();

				res=true;

				break;
			}
			case "harvest":
				//**********************Harvest Customer

			{
				driver.findElement(By.xpath("//input[@id='storeNameId']")).click();

				String wholesale_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc3, driver);
				logger.log(LogStatus.PASS, "Store New entry page displayed");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc3));
				utilityFileWriteOP.writeToLog(tcid, "Store New entry page displayed", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//input[@id='storeNameId']")).sendKeys(storename);

				driver.findElement(By.xpath("//input[@id='storeID']")).sendKeys(storeid);

				driver.findElement(By.xpath("//input[@id='tradingNameId']")).sendKeys(tradingname);

				Select availability = new Select(driver.findElement(By.xpath("//select[@id='availability']")));
				availability.selectByVisibleText("ACTIVE");


				String wholesale_sc4=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc4, driver);
				logger.log(LogStatus.PASS, "All details filled up for Store details");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc4));
				utilityFileWriteOP.writeToLog(tcid, "All details filled up for Store details", "PASS",ResultPath,xwpfRun);


				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).click();


				Select allocation = new Select(driver.findElement(By.xpath("//select[@id='storeAllocationP']")));
				allocation.selectByVisibleText("P2333");
				Thread.sleep(2000);

				Select status = new Select(driver.findElement(By.xpath("//select[@id='statusId']")));
				status.selectByVisibleText("rollout");
				Thread.sleep(2000);
				String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
				logger.log(LogStatus.PASS, "All details filled up for main details");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc5));
				utilityFileWriteOP.writeToLog(tcid, "All details filled up for main details", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//a[contains(text(),'Address')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Address')]")).click();
				Thread.sleep(2000);

				driver.findElement(By.xpath("//input[@id='addressLine1']")).click();
				driver.findElement(By.xpath("//input[@id='addressLine1']")).sendKeys(AddressLine1);


				driver.findElement(By.xpath("//input[@id='postcodeId']")).click();
				driver.findElement(By.xpath("//input[@id='postcodeId']")).sendKeys("NN172AE");

				String wholesale_sc6=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc6, driver);
				logger.log(LogStatus.PASS, "Address details filled successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc6));
				utilityFileWriteOP.writeToLog(tcid, "Address details filled successfully", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).click();
				Thread.sleep(2000);
				Select depotType = new Select(driver.findElement(By.xpath("//select[@name='supplyDepotType']")));
				depotType.selectByVisibleText("Bury");
				Thread.sleep(2000);

				String wholesale_sc7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc7, driver);
				logger.log(LogStatus.PASS, "Supplying Depot details displayed successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc7));
				utilityFileWriteOP.writeToLog(tcid, "Supplying Depot details displayed successfully", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("//h4[@class='orders-heading' and contains(text(),'Current Opportunity')]")).click();

				driver.findElement(By.xpath("//input[@id='Histval_0']")).sendKeys("YYYYYYN");
				Thread.sleep(1000);

				driver.findElement(By.xpath("//input[@id='Histval_1']")).sendKeys("YYYYYYN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histval_2']")).sendKeys("YYYYYNN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histval_3']")).sendKeys("YYYYNYN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histcal_0']")).clear();
				driver.findElement(By.xpath("//input[@id='Histcal_0']")).sendKeys(effectiveDate);

				driver.findElement(By.xpath("//input[@id='Histcal_1']")).clear();
				driver.findElement(By.xpath("//input[@id='Histcal_1']")).sendKeys(effectiveDate);

				driver.findElement(By.xpath("//input[@id='Histcal_2']")).clear();
				driver.findElement(By.xpath("//input[@id='Histcal_2']")).sendKeys(effectiveDate);

				driver.findElement(By.xpath("//input[@id='Histcal_3']")).clear();
				driver.findElement(By.xpath("//input[@id='Histcal_3']")).sendKeys(effectiveDate);
				Thread.sleep(1000);

				String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
				logger.log(LogStatus.PASS, "Delivery days details filled up successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc8));
				utilityFileWriteOP.writeToLog(tcid, "Delivery days details filled up successfully", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).isDisplayed();
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).click();

				Thread.sleep(3000);
				driver.findElement(By.xpath("//div[contains(text(),'Store added')]")).isDisplayed();

				String wholesale_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc9, driver);
				logger.log(LogStatus.PASS, "New Store created successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc9));
				utilityFileWriteOP.writeToLog(tcid, "New Store created successfully", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//button[@type='button' and contains(text(),'✖')]")).click();

				res=true;

				break;
			}

			case "mccolls":
				//**********************McColls Customer

			{
				driver.findElement(By.xpath("//input[@id='storeNameId']")).click();

				String wholesale_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc3, driver);
				logger.log(LogStatus.PASS, "Store New entry page displayed");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc3));
				utilityFileWriteOP.writeToLog(tcid, "Store New entry page displayed", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//input[@id='storeNameId']")).sendKeys(storename);

				driver.findElement(By.xpath("//input[@id='storeID']")).sendKeys(storeid);

				driver.findElement(By.xpath("//input[@id='tradingNameId']")).sendKeys(tradingname);

				Select availability = new Select(driver.findElement(By.xpath("//select[@id='availability']")));
				availability.selectByVisibleText("ACTIVE");


				String wholesale_sc4=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc4, driver);
				logger.log(LogStatus.PASS, "All details filled up for Store details");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc4));
				utilityFileWriteOP.writeToLog(tcid, "All details filled up for Store details", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).click();


				Select allocation = new Select(driver.findElement(By.xpath("//select[@id='storeAllocationP']")));
				allocation.selectByVisibleText("P2333");
				Thread.sleep(2000);

				Select status = new Select(driver.findElement(By.xpath("//select[@id='statusId']")));
				status.selectByVisibleText("rollout");
				Thread.sleep(2000);
				String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
				logger.log(LogStatus.PASS, "All details filled up for main details");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc5));
				utilityFileWriteOP.writeToLog(tcid, "All details filled up for main details", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//a[contains(text(),'Address')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Address')]")).click();
				Thread.sleep(2000);

				driver.findElement(By.xpath("//input[@id='addressLine1']")).click();
				driver.findElement(By.xpath("//input[@id='addressLine1']")).sendKeys(AddressLine1);


				driver.findElement(By.xpath("//input[@id='postcodeId']")).click();
				driver.findElement(By.xpath("//input[@id='postcodeId']")).sendKeys("SS7 4EN");

				String wholesale_sc6=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc6, driver);
				logger.log(LogStatus.PASS, "Address details filled successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc6));
				utilityFileWriteOP.writeToLog(tcid, "Address details filled successfully", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).click();
				Thread.sleep(2000);
				Select depotType = new Select(driver.findElement(By.xpath("//select[@name='supplyDepotType']")));
				depotType.selectByVisibleText("Bury");
				Thread.sleep(2000);

				String wholesale_sc7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc7, driver);
				logger.log(LogStatus.PASS, "Supplying Depot details displayed successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc7));
				utilityFileWriteOP.writeToLog(tcid, "Supplying Depot details displayed successfully", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("//h4[@class='orders-heading' and contains(text(),'Current Opportunity')]")).click();

				driver.findElement(By.xpath("//input[@id='Histval_0']")).sendKeys("YYYYYYN");
				Thread.sleep(1000);

				driver.findElement(By.xpath("//input[@id='Histval_1']")).sendKeys("YYYYYYN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histval_2']")).sendKeys("YYYYYNN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histval_3']")).sendKeys("YYYYNYN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histval_4']")).sendKeys("YYYYNYN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histval_5']")).sendKeys("YYYYYYN");
				Thread.sleep(1000);

				driver.findElement(By.xpath("//input[@id='Histcal_0']")).clear();
				driver.findElement(By.xpath("//input[@id='Histcal_0']")).sendKeys(effectiveDate);

				driver.findElement(By.xpath("//input[@id='Histcal_1']")).clear();
				driver.findElement(By.xpath("//input[@id='Histcal_1']")).sendKeys(effectiveDate);

				driver.findElement(By.xpath("//input[@id='Histcal_2']")).clear();
				driver.findElement(By.xpath("//input[@id='Histcal_2']")).sendKeys(effectiveDate);

				driver.findElement(By.xpath("//input[@id='Histcal_3']")).clear();
				driver.findElement(By.xpath("//input[@id='Histcal_3']")).sendKeys(effectiveDate);

				driver.findElement(By.xpath("//input[@id='Histcal_4']")).clear();
				driver.findElement(By.xpath("//input[@id='Histcal_4']")).sendKeys(effectiveDate);

				driver.findElement(By.xpath("//input[@id='Histcal_5']")).clear();
				driver.findElement(By.xpath("//input[@id='Histcal_5']")).sendKeys(effectiveDate);
				Thread.sleep(1000);

				String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
				logger.log(LogStatus.PASS, "Delivery days details filled up successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc8));
				utilityFileWriteOP.writeToLog(tcid, "Delivery days details filled up successfully", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).isDisplayed();
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).click();

				Thread.sleep(3000);
				driver.findElement(By.xpath("//div[contains(text(),'Store added')]")).isDisplayed();

				String wholesale_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc9, driver);
				logger.log(LogStatus.PASS, "New Store created successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc9));
				utilityFileWriteOP.writeToLog(tcid, "New Store created successfully", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//button[@type='button' and contains(text(),'✖')]")).click();

				res=true;

				break;
			}

			case "mcdly":
				//**********************McDaily Customer

			{
				driver.findElement(By.xpath("//input[@id='storeNameId']")).click();

				String wholesale_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc3, driver);
				logger.log(LogStatus.PASS, "Store New entry page displayed");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc3));
				utilityFileWriteOP.writeToLog(tcid, "Store New entry page displayed", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//input[@id='storeNameId']")).sendKeys(storename);

				driver.findElement(By.xpath("//input[@id='storeID']")).sendKeys(storeid);

				driver.findElement(By.xpath("//input[@id='tradingNameId']")).sendKeys(tradingname);

				Select availability = new Select(driver.findElement(By.xpath("//select[@id='availability']")));
				availability.selectByVisibleText("ACTIVE");

				String wholesale_sc4=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc4, driver);
				logger.log(LogStatus.PASS, "All details filled up for Store details");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc4));
				utilityFileWriteOP.writeToLog(tcid, "All details filled up for Store details", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).click();


				Select allocation = new Select(driver.findElement(By.xpath("//select[@id='storeAllocationP']")));
				allocation.selectByVisibleText("P20000");
				Thread.sleep(2000);

				Select status = new Select(driver.findElement(By.xpath("//select[@id='statusId']")));
				status.selectByVisibleText("rollout");
				Thread.sleep(2000);
				String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
				logger.log(LogStatus.PASS, "All details filled up for main details");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc5));
				utilityFileWriteOP.writeToLog(tcid, "All details filled up for main details", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//a[contains(text(),'Address')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Address')]")).click();
				Thread.sleep(2000);

				driver.findElement(By.xpath("//input[@id='addressLine1']")).click();
				driver.findElement(By.xpath("//input[@id='addressLine1']")).sendKeys(AddressLine1);


				driver.findElement(By.xpath("//input[@id='postcodeId']")).click();
				driver.findElement(By.xpath("//input[@id='postcodeId']")).sendKeys("JE2 4TA");

				String wholesale_sc6=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc6, driver);
				logger.log(LogStatus.PASS, "Address details filled successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc6));
				utilityFileWriteOP.writeToLog(tcid, "Address details filled successfully", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).click();
				Thread.sleep(2000);
				Select depotType = new Select(driver.findElement(By.xpath("//select[@name='supplyDepotType']")));
				depotType.selectByVisibleText("Bury");
				Thread.sleep(2000);

				String wholesale_sc7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc7, driver);
				logger.log(LogStatus.PASS, "Supplying Depot details displayed successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc7));
				utilityFileWriteOP.writeToLog(tcid, "Supplying Depot details displayed successfully", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("//h4[@class='orders-heading' and contains(text(),'Current Opportunity')]")).click();

				driver.findElement(By.xpath("//input[@id='Histval_0']")).sendKeys("YYYYYYN");
				Thread.sleep(1000);

				driver.findElement(By.xpath("//input[@id='Histval_1']")).sendKeys("YYYYYYN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histval_2']")).sendKeys("YYYYYNN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histval_3']")).sendKeys("YYYYNYN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histval_4']")).sendKeys("YYYYNYN");
				Thread.sleep(1000);

				driver.findElement(By.xpath("//input[@id='Histcal_0']")).clear();
				driver.findElement(By.xpath("//input[@id='Histcal_0']")).sendKeys(effectiveDate);

				driver.findElement(By.xpath("//input[@id='Histcal_1']")).clear();
				driver.findElement(By.xpath("//input[@id='Histcal_1']")).sendKeys(effectiveDate);

				driver.findElement(By.xpath("//input[@id='Histcal_2']")).clear();
				driver.findElement(By.xpath("//input[@id='Histcal_2']")).sendKeys(effectiveDate);

				driver.findElement(By.xpath("//input[@id='Histcal_3']")).clear();
				driver.findElement(By.xpath("//input[@id='Histcal_3']")).sendKeys(effectiveDate);

				driver.findElement(By.xpath("//input[@id='Histcal_4']")).clear();
				driver.findElement(By.xpath("//input[@id='Histcal_4']")).sendKeys(effectiveDate);


				String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
				logger.log(LogStatus.PASS, "Delivery days details filled up successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc8));
				utilityFileWriteOP.writeToLog(tcid, "Delivery days details filled up successfully", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).isDisplayed();
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).click();

				Thread.sleep(3000);
				driver.findElement(By.xpath("//div[contains(text(),'Store added')]")).isDisplayed();

				String wholesale_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc9, driver);
				logger.log(LogStatus.PASS, "New Store created successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc9));
				utilityFileWriteOP.writeToLog(tcid, "New Store created successfully", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//button[@type='button' and contains(text(),'✖')]")).click();

				res=true;

				break;
			}

			case "mpk":
				//**********************MPK Customer

			{
				driver.findElement(By.xpath("//input[@id='storeNameId']")).click();

				String wholesale_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc3, driver);
				logger.log(LogStatus.PASS, "Store New entry page displayed");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc3));
				utilityFileWriteOP.writeToLog(tcid, "Store New entry page displayed", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//input[@id='storeNameId']")).sendKeys(storename);

				driver.findElement(By.xpath("//input[@id='storeID']")).sendKeys(storeid);

				driver.findElement(By.xpath("//input[@id='tradingNameId']")).sendKeys(tradingname);

				Select availability = new Select(driver.findElement(By.xpath("//select[@id='availability']")));
				availability.selectByVisibleText("ACTIVE");


				String wholesale_sc4=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc4, driver);
				logger.log(LogStatus.PASS, "All details filled up for Store details");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc4));
				utilityFileWriteOP.writeToLog(tcid, "All details filled up for Store details", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).click();


				Select allocation = new Select(driver.findElement(By.xpath("//select[@id='storeAllocationP']")));
				allocation.selectByVisibleText("P20000");
				Thread.sleep(2000);

				Select status = new Select(driver.findElement(By.xpath("//select[@id='statusId']")));
				status.selectByVisibleText("rollout");
				Thread.sleep(2000);
				String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
				logger.log(LogStatus.PASS, "All details filled up for main details");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc5));
				utilityFileWriteOP.writeToLog(tcid, "All details filled up for main details", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//a[contains(text(),'Address')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Address')]")).click();
				Thread.sleep(2000);

				driver.findElement(By.xpath("//input[@id='addressLine1']")).click();
				driver.findElement(By.xpath("//input[@id='addressLine1']")).sendKeys(AddressLine1);


				driver.findElement(By.xpath("//input[@id='postcodeId']")).click();
				driver.findElement(By.xpath("//input[@id='postcodeId']")).sendKeys("NG25 0BW");

				String wholesale_sc6=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc6, driver);
				logger.log(LogStatus.PASS, "Address details filled successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc6));
				utilityFileWriteOP.writeToLog(tcid, "Address details filled successfully", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).click();
				Thread.sleep(2000);
				Select depotType = new Select(driver.findElement(By.xpath("//select[@name='supplyDepotType']")));
				depotType.selectByVisibleText("Bury");
				Thread.sleep(2000);

				String wholesale_sc7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc7, driver);
				logger.log(LogStatus.PASS, "Supplying Depot details displayed successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc7));
				utilityFileWriteOP.writeToLog(tcid, "Supplying Depot details displayed successfully", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("//h4[@class='orders-heading' and contains(text(),'Current Opportunity')]")).click();

				driver.findElement(By.xpath("//input[@id='Histval_0']")).sendKeys("YYYYYYN");
				Thread.sleep(1000);

				driver.findElement(By.xpath("//input[@id='Histval_1']")).sendKeys("YYYYYYN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histval_2']")).sendKeys("YYYYYNN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histval_3']")).sendKeys("YYYYNYN");
				Thread.sleep(1000);

				driver.findElement(By.xpath("//input[@id='Histcal_0']")).clear();
				driver.findElement(By.xpath("//input[@id='Histcal_0']")).sendKeys(effectiveDate);

				driver.findElement(By.xpath("//input[@id='Histcal_1']")).clear();
				driver.findElement(By.xpath("//input[@id='Histcal_1']")).sendKeys(effectiveDate);

				driver.findElement(By.xpath("//input[@id='Histcal_2']")).clear();
				driver.findElement(By.xpath("//input[@id='Histcal_2']")).sendKeys(effectiveDate);

				driver.findElement(By.xpath("//input[@id='Histcal_3']")).clear();
				driver.findElement(By.xpath("//input[@id='Histcal_3']")).sendKeys(effectiveDate);
				Thread.sleep(1000);

				String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
				logger.log(LogStatus.PASS, "Delivery days details filled up successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc8));
				utilityFileWriteOP.writeToLog(tcid, "Delivery days details filled up successfully", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).isDisplayed();
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).click();

				Thread.sleep(3000);
				driver.findElement(By.xpath("//div[contains(text(),'Store added')]")).isDisplayed();

				String wholesale_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc9, driver);
				logger.log(LogStatus.PASS, "New Store created successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc9));
				utilityFileWriteOP.writeToLog(tcid, "New Store created successfully", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//button[@type='button' and contains(text(),'✖')]")).click();

				res=true;

				break;
			}

			case "rontec":
				//**********************Rontec Customer

			{
				driver.findElement(By.xpath("//input[@id='storeNameId']")).click();

				String wholesale_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc3, driver);
				logger.log(LogStatus.PASS, "Store New entry page displayed");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc3));
				utilityFileWriteOP.writeToLog(tcid, "Store New entry page displayed", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//input[@id='storeNameId']")).sendKeys(storename);

				driver.findElement(By.xpath("//input[@id='storeID']")).sendKeys(storeid);

				driver.findElement(By.xpath("//input[@id='tradingNameId']")).sendKeys(tradingname);

				Select availability = new Select(driver.findElement(By.xpath("//select[@id='availability']")));
				availability.selectByVisibleText("ACTIVE");


				String wholesale_sc4=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc4, driver);
				logger.log(LogStatus.PASS, "All details filled up for Store details");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc4));
				utilityFileWriteOP.writeToLog(tcid, "All details filled up for Store details", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).click();


				Select allocation = new Select(driver.findElement(By.xpath("//select[@id='storeAllocationP']")));
				allocation.selectByVisibleText("P20000");
				Thread.sleep(2000);

				Select status = new Select(driver.findElement(By.xpath("//select[@id='statusId']")));
				status.selectByVisibleText("rollout");
				Thread.sleep(2000);
				String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
				logger.log(LogStatus.PASS, "All details filled up for main details");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc5));
				utilityFileWriteOP.writeToLog(tcid, "All details filled up for main details", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//a[contains(text(),'Address')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Address')]")).click();
				Thread.sleep(2000);

				driver.findElement(By.xpath("//input[@id='addressLine1']")).click();
				driver.findElement(By.xpath("//input[@id='addressLine1']")).sendKeys(AddressLine1);

				driver.findElement(By.xpath("//input[@id='contactEmailId']")).click();
				driver.findElement(By.xpath("//input[@id='contactEmailId']")).sendKeys(EmailId);

				driver.findElement(By.xpath("//input[@id='postcodeId']")).click();
				driver.findElement(By.xpath("//input[@id='postcodeId']")).sendKeys("TTT TTT");

				String wholesale_sc6=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc6, driver);
				logger.log(LogStatus.PASS, "Address details filled successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc6));
				utilityFileWriteOP.writeToLog(tcid, "Address details filled successfully", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).click();
				Thread.sleep(2000);
				Select depotType = new Select(driver.findElement(By.xpath("//select[@name='supplyDepotType']")));
				depotType.selectByVisibleText("Allington");
				Thread.sleep(2000);

				String wholesale_sc7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc7, driver);
				logger.log(LogStatus.PASS, "Supplying Depot details displayed successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc7));
				utilityFileWriteOP.writeToLog(tcid, "Supplying Depot details displayed successfully", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("//h4[@class='orders-heading' and contains(text(),'Current Opportunity')]")).click();

				driver.findElement(By.xpath("//input[@id='Histval_0']")).sendKeys("YYYYYYN");
				Thread.sleep(1000);

				driver.findElement(By.xpath("//input[@id='Histval_1']")).sendKeys("YYYYYYN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histval_2']")).sendKeys("YYYYYNN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histval_3']")).sendKeys("YYYYNYN");
				Thread.sleep(1000);

				driver.findElement(By.xpath("//input[@id='Histcal_0']")).clear();
				driver.findElement(By.xpath("//input[@id='Histcal_0']")).sendKeys(effectiveDate);

				driver.findElement(By.xpath("//input[@id='Histcal_1']")).clear();
				driver.findElement(By.xpath("//input[@id='Histcal_1']")).sendKeys(effectiveDate);

				driver.findElement(By.xpath("//input[@id='Histcal_2']")).clear();
				driver.findElement(By.xpath("//input[@id='Histcal_2']")).sendKeys(effectiveDate);

				driver.findElement(By.xpath("//input[@id='Histcal_3']")).clear();
				driver.findElement(By.xpath("//input[@id='Histcal_3']")).sendKeys(effectiveDate);
				Thread.sleep(1000);

				String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
				logger.log(LogStatus.PASS, "Delivery days details filled up successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc8));
				utilityFileWriteOP.writeToLog(tcid, "Delivery days details filled up successfully", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).isDisplayed();
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).click();

				Thread.sleep(3000);
				driver.findElement(By.xpath("//div[contains(text(),'Store added')]")).isDisplayed();

				String wholesale_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc9, driver);
				logger.log(LogStatus.PASS, "New Store created successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc9));
				utilityFileWriteOP.writeToLog(tcid, "New Store created successfully", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//button[@type='button' and contains(text(),'✖')]")).click();

				res=true;

				break;
			}

			case "rontec-b":
				//**********************Rontec B Customer

			{
				driver.findElement(By.xpath("//input[@id='storeNameId']")).click();

				String wholesale_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc3, driver);
				logger.log(LogStatus.PASS, "Store New entry page displayed");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc3));
				utilityFileWriteOP.writeToLog(tcid, "Store New entry page displayed", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//input[@id='storeNameId']")).sendKeys(storename);

				driver.findElement(By.xpath("//input[@id='storeID']")).sendKeys(storeid);

				driver.findElement(By.xpath("//input[@id='tradingNameId']")).sendKeys(tradingname);

				Select availability = new Select(driver.findElement(By.xpath("//select[@id='availability']")));
				availability.selectByVisibleText("ACTIVE");

				String wholesale_sc4=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc4, driver);
				logger.log(LogStatus.PASS, "All details filled up for Store details");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc4));
				utilityFileWriteOP.writeToLog(tcid, "All details filled up for Store details", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).click();


				Select allocation = new Select(driver.findElement(By.xpath("//select[@id='storeAllocationP']")));
				allocation.selectByVisibleText("P30000");
				Thread.sleep(2000);

				Select status = new Select(driver.findElement(By.xpath("//select[@id='statusId']")));
				status.selectByVisibleText("rollout");
				Thread.sleep(2000);
				String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
				logger.log(LogStatus.PASS, "All details filled up for main details");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc5));
				utilityFileWriteOP.writeToLog(tcid, "All details filled up for main details", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//a[contains(text(),'Address')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Address')]")).click();
				Thread.sleep(2000);

				driver.findElement(By.xpath("//input[@id='addressLine1']")).click();
				driver.findElement(By.xpath("//input[@id='addressLine1']")).sendKeys(AddressLine1);


				driver.findElement(By.xpath("//input[@id='contactEmailId']")).click();
				driver.findElement(By.xpath("//input[@id='contactEmailId']")).sendKeys(EmailId);

				driver.findElement(By.xpath("//input[@id='postcodeId']")).click();
				driver.findElement(By.xpath("//input[@id='postcodeId']")).sendKeys("NN1 4GD");

				String wholesale_sc6=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc6, driver);
				logger.log(LogStatus.PASS, "Address details filled successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc6));
				utilityFileWriteOP.writeToLog(tcid, "Address details filled successfully", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).click();
				Thread.sleep(2000);
				Select depotType = new Select(driver.findElement(By.xpath("//select[@name='supplyDepotType']")));
				depotType.selectByVisibleText("Allington");
				Thread.sleep(2000);

				String wholesale_sc7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc7, driver);
				logger.log(LogStatus.PASS, "Supplying Depot details displayed successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc7));
				utilityFileWriteOP.writeToLog(tcid, "Supplying Depot details displayed successfully", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("//h4[@class='orders-heading' and contains(text(),'Current Opportunity')]")).click();

				driver.findElement(By.xpath("//input[@id='Histval_0']")).sendKeys("YYYYYYN");
				Thread.sleep(1000);

				driver.findElement(By.xpath("//input[@id='Histval_1']")).sendKeys("YYYYYYN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histval_2']")).sendKeys("YYYYYNN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histval_3']")).sendKeys("YYYYNYN");

				driver.findElement(By.xpath("//input[@id='Histcal_0']")).clear();
				driver.findElement(By.xpath("//input[@id='Histcal_0']")).sendKeys(effectiveDate);

				driver.findElement(By.xpath("//input[@id='Histcal_1']")).clear();
				driver.findElement(By.xpath("//input[@id='Histcal_1']")).sendKeys(effectiveDate);

				driver.findElement(By.xpath("//input[@id='Histcal_2']")).clear();
				driver.findElement(By.xpath("//input[@id='Histcal_2']")).sendKeys(effectiveDate);

				driver.findElement(By.xpath("//input[@id='Histcal_3']")).clear();
				driver.findElement(By.xpath("//input[@id='Histcal_3']")).sendKeys(effectiveDate);
				Thread.sleep(1000);


				String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
				logger.log(LogStatus.PASS, "Delivery days details filled up successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc8));
				utilityFileWriteOP.writeToLog(tcid, "Delivery days details filled up successfully", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).isDisplayed();
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).click();

				Thread.sleep(3000);
				driver.findElement(By.xpath("//div[contains(text(),'Store added')]")).isDisplayed();

				String wholesale_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc9, driver);
				logger.log(LogStatus.PASS, "New Store created successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc9));
				utilityFileWriteOP.writeToLog(tcid, "New Store created successfully", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//button[@type='button' and contains(text(),'✖')]")).click();

				res=true;

				break;
			}


			case "sandpiper":
				//**********************Sandpiper Customer

			{
				driver.findElement(By.xpath("//input[@id='storeNameId']")).click();

				String wholesale_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc3, driver);
				logger.log(LogStatus.PASS, "Store New entry page displayed");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc3));
				utilityFileWriteOP.writeToLog(tcid, "Store New entry page displayed", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//input[@id='storeNameId']")).sendKeys(storename);

				driver.findElement(By.xpath("//input[@id='storeID']")).sendKeys(storeid);

				driver.findElement(By.xpath("//input[@id='tradingNameId']")).sendKeys(tradingname);

				Select availability = new Select(driver.findElement(By.xpath("//select[@id='availability']")));
				availability.selectByVisibleText("ACTIVE");

				String wholesale_sc4=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc4, driver);
				logger.log(LogStatus.PASS, "All details filled up for Store details");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc4));
				utilityFileWriteOP.writeToLog(tcid, "All details filled up for Store details", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).click();

				Select allocation = new Select(driver.findElement(By.xpath("//select[@id='storeAllocationP']")));
				allocation.selectByVisibleText("P20000");
				Thread.sleep(2000);

				Select status = new Select(driver.findElement(By.xpath("//select[@id='statusId']")));
				status.selectByVisibleText("rollout");
				Thread.sleep(2000);
				String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
				logger.log(LogStatus.PASS, "All details filled up for main details");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc5));
				utilityFileWriteOP.writeToLog(tcid, "All details filled up for main details", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//a[contains(text(),'Address')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Address')]")).click();
				Thread.sleep(2000);

				driver.findElement(By.xpath("//input[@id='addressLine1']")).click();
				driver.findElement(By.xpath("//input[@id='addressLine1']")).sendKeys(AddressLine1);


				driver.findElement(By.xpath("//input[@id='postcodeId']")).click();
				driver.findElement(By.xpath("//input[@id='postcodeId']")).sendKeys("GY7 9RS");

				Select port = new Select(driver.findElement(By.xpath("//select[@id='storePort_id']")));
				port.selectByVisibleText("St Helier");

				String wholesale_sc6=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc6, driver);
				logger.log(LogStatus.PASS, "Address details filled successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc6));
				utilityFileWriteOP.writeToLog(tcid, "Address details filled successfully", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).click();
				Thread.sleep(2000);
				Select depotType = new Select(driver.findElement(By.xpath("//select[@name='supplyDepotType']")));
				depotType.selectByVisibleText("Bury");
				Thread.sleep(2000);

				String wholesale_sc7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc7, driver);
				logger.log(LogStatus.PASS, "Supplying Depot details displayed successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc7));
				utilityFileWriteOP.writeToLog(tcid, "Supplying Depot details displayed successfully", "PASS",ResultPath,xwpfRun);

				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("//h4[@class='orders-heading' and contains(text(),'Current Opportunity')]")).click();

				driver.findElement(By.xpath("//input[@id='Histval_0']")).sendKeys("YYYYYYN");
				Thread.sleep(1000);

				driver.findElement(By.xpath("//input[@id='Histval_1']")).sendKeys("YYYYYYN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histval_2']")).sendKeys("YYYYYNN");
				Thread.sleep(1000);
				driver.findElement(By.xpath("//input[@id='Histval_3']")).sendKeys("YYYYNYN");

				driver.findElement(By.xpath("//input[@id='Histcal_0']")).clear();
				driver.findElement(By.xpath("//input[@id='Histcal_0']")).sendKeys(effectiveDate);

				driver.findElement(By.xpath("//input[@id='Histcal_1']")).clear();
				driver.findElement(By.xpath("//input[@id='Histcal_1']")).sendKeys(effectiveDate);

				driver.findElement(By.xpath("//input[@id='Histcal_2']")).clear();
				driver.findElement(By.xpath("//input[@id='Histcal_2']")).sendKeys(effectiveDate);

				driver.findElement(By.xpath("//input[@id='Histcal_3']")).clear();
				driver.findElement(By.xpath("//input[@id='Histcal_3']")).sendKeys(effectiveDate);
				Thread.sleep(1000);


				String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
				logger.log(LogStatus.PASS, "Delivery days details filled up successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc8));
				utilityFileWriteOP.writeToLog(tcid, "Delivery days details filled up successfully", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).isDisplayed();
				driver.findElement(By.xpath("//button[@type='submit' and contains(text(),'Save')]")).click();

				Thread.sleep(3000);
				driver.findElement(By.xpath("//div[contains(text(),'Store added')]")).isDisplayed();

				String wholesale_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc9, driver);
				logger.log(LogStatus.PASS, "New Store created successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc9));
				utilityFileWriteOP.writeToLog(tcid, "New Store created successfully", "PASS",ResultPath,xwpfRun);
				driver.findElement(By.xpath("//button[@type='button' and contains(text(),'✖')]")).click();

				res=true;

				break;
			}
			default: System.out.println("unknown test");
			}


		}

		catch(Exception e){
			e.printStackTrace();
			String wholesale_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc9, driver);
			logger.log(LogStatus.FAIL, "Error occured while store creation");
			logger.log(LogStatus.FAIL, logger.addScreenCapture(wholesale_sc9));
			utilityFileWriteOP.writeToLog(tcid, "Error occured while store creation", "FAIL",ResultPath,xwpfRun);
			res=false;
		}finally{
			return res;
		}
	}

	

	/*
	 * Method Name: store_New_Entry_Madatory_Field_Error
	 * Functionality Description: Store new Entry page error validation for all mandatory fields
	 * Created on:08/01/2020
	 * Created By:Morrisons
	 */

	public static boolean store_New_Entry_Madatory_Field_Error(String customer,WebDriver driver, WebDriverWait wait, String tcid,String ResultPath,XWPFRun xwpfRun, ExtentTest logger){
		boolean res=false;
		try{

			ResultPath=System.getProperty("user.dir")+"/"+ResultPath;

			Thread.sleep(4000);

			driver.findElement(By.xpath("//span[@class='menu-product-name-inline dis-inlineblc padd-top-5 text-left']")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("//h5[contains(text(),'Store  New Entry')]")).isDisplayed();
			driver.findElement(By.xpath("//h5[contains(text(),'Store  New Entry')]")).click();
			Thread.sleep(5000);

			String currentWindow=driver.getWindowHandle();
			Set<String> windows=driver.getWindowHandles();

			for(String window:windows)
			{
				if(!window.equals(currentWindow))
				{
					driver.switchTo().window(window);
					//utilityFileWriteOP.writeToLog(tcid, "AWS Console Window", "Switched!!!",ResultPath, xwpfRun,"");
					break;
				}
			}

			if(customer.equalsIgnoreCase("amazon")){

				driver.findElement(By.xpath("//input[@id='storeNameId']")).click();

				driver.findElement(By.xpath("//input[@id='storeID']")).click();

				driver.findElement(By.xpath("//input[@id='tradingNameId']")).click();

				driver.findElement(By.xpath("//input[@id='merchantId']")).click();

				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).click();

				Thread.sleep(3000);
				String wholesale_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc9, driver);
				logger.log(LogStatus.PASS, "Mandatory fields error is displayed");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc9));
				utilityFileWriteOP.writeToLog(tcid, "Mandatory fields error is displayed", "PASS",ResultPath,xwpfRun);


				driver.findElement(By.xpath("//span[@id='errStoreName' and contains(text(),'Store Name is required')]")).isDisplayed();
				driver.findElement(By.xpath("//span[@id='errstoreID' and contains(text(),'Store ID is required')]")).isDisplayed();
				driver.findElement(By.xpath("//span[@id='errtradingName' and contains(text(),'Trading Name is required')]")).isDisplayed();
				driver.findElement(By.xpath("//span[@id='errormerchantId' and contains(text(),'Merchant ID is required')]")).isDisplayed();


				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).click();
				Thread.sleep(2000);

				driver.findElement(By.xpath("//input[@id='Histval_0']")).click();
				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).isDisplayed();
				driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).click();

				driver.findElement(By.xpath("//div[contains(@style,'red')]")).isDisplayed();

				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("window.scrollBy(0,300)");

				driver.findElement(By.xpath("//div[contains(@style,'red')]")).isDisplayed();
				String wholesale_sc10=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc10, driver);
				logger.log(LogStatus.PASS, "Mandatory fields error at OPS for delivery Days is displayed");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc10));
				utilityFileWriteOP.writeToLog(tcid, "Mandatory fields error at OPS for delivery Days is displayed", "PASS",ResultPath,xwpfRun);

				res=true;

			}
			else{

				driver.findElement(By.xpath("//input[@id='storeNameId']")).click();

				driver.findElement(By.xpath("//input[@id='storeID']")).click();

				driver.findElement(By.xpath("//input[@id='tradingNameId']")).click();

				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).click();

				Thread.sleep(3000);
				String wholesale_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc9, driver);
				logger.log(LogStatus.PASS, "Mandatory fields error is displayed");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc9));
				utilityFileWriteOP.writeToLog(tcid, "Mandatory fields error is displayed", "PASS",ResultPath,xwpfRun);


				driver.findElement(By.xpath("//span[@id='errStoreName' and contains(text(),'Store Name is required')]")).isDisplayed();
				driver.findElement(By.xpath("//span[@id='errstoreID' and contains(text(),'Store ID is required')]")).isDisplayed();
				driver.findElement(By.xpath("//span[@id='errtradingName' and contains(text(),'Trading Name is required')]")).isDisplayed();

				driver.findElement(By.xpath("//select[@id='storeAllocationP']")).click();
				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).click();

				driver.findElement(By.xpath("//select[@id='statusId']")).click();
				driver.findElement(By.xpath("//a[contains(text(),'Main Details')]")).click();

				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("window.scrollBy(0,200)");

				driver.findElement(By.xpath("(//div[contains(@style,'red')])[1]")).isDisplayed();
				driver.findElement(By.xpath("(//div[contains(@style,'red')])[2]")).isDisplayed();
				String wholesale_sc10=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc10, driver);
				logger.log(LogStatus.PASS, "Mandatory fields error messages are validated");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc10));
				utilityFileWriteOP.writeToLog(tcid, "Mandatory fields error messages are validated", "PASS",ResultPath,xwpfRun);


				res=true;

			}

		}	 	   
		catch(Exception e){
			e.printStackTrace();
			String wholesale_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc9, driver);
			logger.log(LogStatus.FAIL, "Error occured while validating mandatory fields error");
			logger.log(LogStatus.FAIL, logger.addScreenCapture(wholesale_sc9));
			utilityFileWriteOP.writeToLog(tcid, "Error occured while validating mandatory fields error", "FAIL",ResultPath,xwpfRun);
			res=false;
		}finally{
			return res;
		}
	}



	public static boolean Bulkupload_Store_AllCustomer(WebDriver driver, WebDriverWait wait, String tcid,String customer,String errorfilepath,String filepath,String ResultPath,XWPFRun xwpfRun, ExtentTest logger){
		boolean res=false;

		try{

			ResultPath=System.getProperty("user.dir")+"/"+ResultPath;

			driver.findElement(By.xpath("//img[@alt='"+customer+"']")).click();
			Thread.sleep(5000);

			logger.log(LogStatus.PASS, "Sales order page is displayed for :"+customer+"");
			String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
			utilityFileWriteOP.writeToLog(tcid, "Required customer is", "clicked!!!",ResultPath,xwpfRun);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc5));

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//img[@src='app/images/IconCatalogueManagement.svg']")));
			driver.findElement(By.xpath("//img[@src='app/images/IconCatalogueManagement.svg']")).click();

			logger.log(LogStatus.PASS, "Catalogue management icon is clicked in sales order page of customer :"+customer);
			String wholesale_sc6=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc6, driver);
			utilityFileWriteOP.writeToLog(tcid, "clicked on ", "Catalogue management icon!!!",ResultPath,xwpfRun);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc6));

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h5[contains(text(),'Store Bulk Upload')]")));
			driver.findElement(By.xpath("//h5[contains(text(),'Store Bulk Upload')]")).click();

			logger.log(LogStatus.PASS, "Store Bulk upload  is clicked for customer :"+customer);
			String wholesale_sc7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc7, driver);
			utilityFileWriteOP.writeToLog(tcid, "Store Bulk upload page is displayed !!", "PASS",ResultPath,xwpfRun);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc7));


			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h4[text()='Uploaded Store Files']")));


			if(driver.findElement(By.xpath("//button[contains(text(),'Upload New Store')]")).isDisplayed())
			{
				WebElement Cat=driver.findElement(By.xpath("//button[contains(text(),'Upload New Store')]"));
				Cat.click();

				Thread.sleep(2000);
				String wholesale_sc4=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc4, driver);
				logger.log(LogStatus.PASS, "Page to upload the file is displayed");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc4));
				utilityFileWriteOP.writeToLog(tcid, "Page to upload the file is displayed", "PASS",ResultPath,xwpfRun);

			}


			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='storeCatalogueUploadFrm']/ng2-file-input/div/div[2]/button")));
			System.out.println("Filepath :"+errorfilepath);

			WebElement upload=driver.findElement(By.xpath("//div[@class='ng2-file-input']/input[@type='file']"));
			upload.sendKeys(errorfilepath);
			Thread.sleep(2000);
			logger.log(LogStatus.PASS, "Error message is displayed if user trying to upload files other than .CSV");
			String wholesale_sc15=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc15, driver);
			utilityFileWriteOP.writeToLog(tcid, "Store file is not uploaded if file is not .CSV ", "PASS",ResultPath,xwpfRun);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc15));

			try{
				Thread.sleep(2000);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'Please select CSV File only')]")));
				if(driver.findElement(By.xpath("//span[contains(text(),'Please select CSV File only')]")).isDisplayed()){
					System.out.println("INSIDE IF");
					driver.findElement(By.xpath("//button[contains(text(),'Ok')]")).click();	
				}
				Thread.sleep(2000);  
			}catch(Exception e){}
			WebElement upload1=driver.findElement(By.xpath("//div[@class='ng2-file-input']/input[@type='file']"));

			upload1.sendKeys(filepath);
			logger.log(LogStatus.PASS, "Now Store file with .CSV format uploaded successfully for customer :"+customer);
			String wholesale_sc100=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc100, driver);
			utilityFileWriteOP.writeToLog(tcid, "Store file uploaded successfully!!!", "PASS",ResultPath,xwpfRun);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc100));

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@type='button' and text()='Submit']")));	             

			driver.findElement(By.xpath("//button[@type='button' and text()='Submit']")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("//*[@id='storeFileUploadBox']/div/div/div[2]/div/div[2]/button")).click();

			for(int j = 0; j < 50; j++)
			{
				try{
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("((//table[@class='table table-bordered table-striped margin-bottom-0']//tr)[2]//td)[5][contains(text(),'Upload Completed')]")));
					logger.log(LogStatus.PASS, " File upload completed sucessfully for "+customer);
					String wholesale_sc11=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
					TakeScreenShot.saveScreenShot(wholesale_sc11, driver);
					utilityFileWriteOP.writeToLog(tcid, "File upload completed sucessfully", "PASS",ResultPath,xwpfRun);
					logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc11));
					break;
				}catch(Exception e)
				{
					driver.navigate().refresh();
					//							driver.findElement(By.xpath("//button[@type='submit'][contains(text(),'VIEW')]")).click();

				}
			}
			String pattern = "ddMMyyyy";
			String dateInString =new SimpleDateFormat(pattern).format(new Date());
			String flnme=driver.findElement(By.xpath("//table[@class='table table-bordered table-striped margin-bottom-0']//tbody/tr/td[2]")).getText();
			System.out.println("File name in table viewed is"+flnme);
			if(driver.findElement(By.xpath("//table[@class='table table-bordered table-striped margin-bottom-0']//tbody/tr/td[2]")).getText().contains("-"+customer+"-"+dateInString))
			{
				System.out.println("Store name validated");
			}
			else
			{
				System.out.println("Store name not validated");
			}
			Thread.sleep(2000);
			driver.findElement(By.xpath("((//table[@class='table table-bordered table-striped margin-bottom-0']//tr)[2]//td)[4]")).click();


			logger.log(LogStatus.PASS, " Newly uploaded Store file contain name as : storecatalog-"+customer+"-ddmmyyyyyhhmmss.csv is downloaded successfully");
			String wholesale_sc11=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc11, driver);
			utilityFileWriteOP.writeToLog(tcid, "Newly uploaded Store file", "downloaded!!!",ResultPath,xwpfRun);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc11));

			res=true;



		}catch(Exception e){
			e.printStackTrace();
			res=false;

		}finally{
			return res;
		}
	}


	/*
	 * Method Name: BulkExport_Store_AllCustomer
	 * Functionality Description:New Store Bulk Export all customer
	 * Created on:08/01/2020
	 * Created By:Morrisons
	 */

	public static boolean BulkExport_Store_AllCustomer(WebDriver driver, WebDriverWait wait, String tcid,String customer,String exportDate,String ResultPath,XWPFRun xwpfRun, ExtentTest logger){
		boolean res=false;

		try{

			ResultPath=System.getProperty("user.dir")+"/"+ResultPath;

			driver.findElement(By.xpath("//img[@alt='"+customer+"']")).click();
			Thread.sleep(5000);

			logger.log(LogStatus.PASS, "Sales order page is displayed for :"+customer+"");
			String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
			utilityFileWriteOP.writeToLog(tcid, "Required customer is", "clicked!!!",ResultPath,xwpfRun);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc5));

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//img[@src='app/images/IconCatalogueManagement.svg']")));
			driver.findElement(By.xpath("//img[@src='app/images/IconCatalogueManagement.svg']")).click();

			logger.log(LogStatus.PASS, "Catalogue management icon is clicked in sales order page of customer :"+customer);
			String wholesale_sc6=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc6, driver);
			utilityFileWriteOP.writeToLog(tcid, "clicked on ", "Catalogue management icon!!!",ResultPath,xwpfRun);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc6));

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h5[contains(text(),'Store Bulk Export')]")));
			driver.findElement(By.xpath("//h5[contains(text(),'Store Bulk Export')]")).click();

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@id='orderdateinput']")));

			logger.log(LogStatus.PASS, "Store Bulk Export  is clicked for customer :"+customer);
			String wholesale_sc7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc7, driver);
			utilityFileWriteOP.writeToLog(tcid, "Store Bulk Export page is displayed !!", "PASS",ResultPath,xwpfRun);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc7));

			WholesaleStore_Edit_Functions.pickExportCalendarDate(driver,wait,exportDate);

			logger.log(LogStatus.PASS, "Export date selected from calendar successfully");
			String wholesale_sc15=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc15, driver);
			utilityFileWriteOP.writeToLog(tcid, "Export date selected from calendar successfully", "PASS",ResultPath,xwpfRun);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc15));


			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='Initiate New Store Export']")));

			driver.findElement(By.xpath("//button[text()='Initiate New Store Export']")).click();	

			Thread.sleep(2000);  

			driver.findElement(By.xpath("//span[contains(text(),'request for initiating Store Export')]")).isDisplayed();

			logger.log(LogStatus.PASS, "Now Store Bulk Export for customer initiated sucessfully"+customer);
			String wholesale_sc100=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc100, driver);
			utilityFileWriteOP.writeToLog(tcid, "Now Store Bulk Export for customer initiated sucessfully", "PASS",ResultPath,xwpfRun);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc100));
			Thread.sleep(2000);
			System.out.println("Export date is:"+exportDate);
			driver.findElement(By.xpath("//h5[contains(text(),'Store Process')]/button[@type='button' and contains(text(),'✖') and @id='close-popup']")).isDisplayed();

			driver.findElement(By.xpath("//h5[contains(text(),'Store Process')]/button[@type='button' and contains(text(),'✖') and @id='close-popup']")).click();


			for(int j = 0; j < 5; j++)
			{
				try{
					System.out.println("Value of I counter is:"+j);
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("((//table[contains(@class,'table-striped search-result-table text-nowrap')]//tr)[2]//td)[4][contains(text(),'"+exportDate+"')]")));
					break;
				}catch(Exception e)
				{
					System.out.println("Value of I counter in catch block is:"+j);
					driver.navigate().refresh();
					//							driver.findElement(By.xpath("//button[@type='submit'][contains(text(),'VIEW')]")).click();

				}
			}
			boolean compl=driver.findElement(By.xpath("((//table[contains(@class,'table-striped search-result-table text-nowrap')]//tr)[2]//td)[5][contains(text(),'Completed')]")).isDisplayed();
			boolean date=driver.findElement(By.xpath("((//table[contains(@class,'table-striped search-result-table text-nowrap')]//tr)[2]//td)[4][contains(text(),'"+exportDate+"')]")).isDisplayed();

			if(compl && date)
			{
				System.out.println("Store bulk Export validated");
				logger.log(LogStatus.PASS, "Store bulk Export done successfully");
				String wholesale_sc11=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc11, driver);
				utilityFileWriteOP.writeToLog(tcid, "Store bulk Export done successfully", "PASS",ResultPath,xwpfRun);
				logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc11));
				Thread.sleep(2000);
				driver.findElement(By.xpath("((//table[contains(@class,'table-bordered table-striped')]//tr)[2]//td)[6]")).isDisplayed();
				driver.findElement(By.xpath("((//table[contains(@class,'table-bordered table-striped')]//tr)[2]//td)[6]")).click();
				Thread.sleep(2000);

				logger.log(LogStatus.PASS, " Store file contain name as : -"+customer+"-stores-ddmmyyyyyhhmmss.csv is downloaded successfully");
				String wholesale_sc16=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc16, driver);
				utilityFileWriteOP.writeToLog(tcid, "Store file export", "downloaded!!!",ResultPath,xwpfRun);
				logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc16));

				res=true;
			}
			else
			{
				System.out.println("Store export not validated");
				logger.log(LogStatus.PASS, "Store bulk Export done failed");
				String wholesale_sc11=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc11, driver);
				utilityFileWriteOP.writeToLog(tcid, "Store bulk Export ", "FAIL",ResultPath,xwpfRun);
				logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc11));
				res=false;
			}
			Thread.sleep(2000);

		}catch(Exception e){
			e.printStackTrace();
			logger.log(LogStatus.PASS, "Store bulk Export done failed");
			String wholesale_sc11=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc11, driver);
			utilityFileWriteOP.writeToLog(tcid, "Store bulk Export ", "FAIL",ResultPath,xwpfRun);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc11));
			res=false;

		}finally{
			return res;
		}
	}

	/*
	 * Method Name:Wholesale Log out
	 * Functionality Description:Wholesale Log out
	 * Created on:08/01/2020
	 * Created By:Morrisons
	 */


	public static boolean AWW_Logout(WebDriver driver, WebDriverWait wait, String tcid, String poratlUserName,String ResultPath,XWPFRun xwpfrun, ExtentTest logger) 
	{

		boolean res =false;
		try
		{
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(text(),'"+poratlUserName+"')]")));
			Thread.sleep(3000);
			driver.findElement(By.xpath("//button[contains(text(),'"+poratlUserName+"')]")).click();


			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(text(),'Logout')]")));
			driver.findElement(By.xpath("//a[contains(text(),'Logout')]")).click();

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='username' and @placeholder='Email address']")));
			String wholesale_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";
			TakeScreenShot.saveScreenShot(wholesale_sc9, driver);


			if (driver.findElement(By.xpath("//input[@id='username' and @placeholder='Email address']")).isDisplayed())
			{
				res=true;
				logger.log(LogStatus.PASS, "User logged out successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc9));
				utilityFileWriteOP.writeToLog(tcid, "User logged OUT", "successfully!!!",ResultPath,xwpfrun);

			}else{
				logger.log(LogStatus.FAIL, "User not logged out successfully");
				logger.log(LogStatus.FAIL, logger.addScreenCapture(wholesale_sc9));
				utilityFileWriteOP.writeToLog(tcid, "User not logged out", "successfully!!!",ResultPath,xwpfrun);

			}
			Thread.sleep(2000);

			ArrayList<String> tabs_windows = new ArrayList<String> (driver.getWindowHandles());

			try{

				if(tabs_windows.size()>1){
					driver.switchTo().window(tabs_windows.get(0));
					//wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//i[@id='order_Qnt_Info']")));
					Thread.sleep(500);
					driver.close();
					driver.switchTo().window(tabs_windows.get(1));
				}


			}catch(Exception e){
				System.out.println("Number of tabs: "+tabs_windows.size());
			}

		}
		catch(Exception e)
		{
			res=false;
			return res;
		}
		finally
		{
			return res;
		}
	}


	/*
	 * Method Name: newStoreUpload_Day2validation
	 * Functionality Description:New Store Upload Day2 Validation of CSV with portal displayed values
	 * Created on:08/01/2020
	 * Created By:Morrisons
	 */

	public static boolean newStoreUpload_Day2validation(WebDriver driver, WebDriverWait wait, String tcid,String customer,String storeid,String category,String FinalFilePath,String ResultPath,XWPFRun xwpfRun, ExtentTest logger){
		boolean res=false;
		try{
			String[] StoreId = storeid.split(",");

			ResultPath=System.getProperty("user.dir")+"/"+ResultPath;

			driver.findElement(By.xpath("//img[@alt='"+customer+"']")).click();


			logger.log(LogStatus.PASS, "customerid is Clicked");
			String wholesale_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc3, driver);
			utilityFileWriteOP.writeToLog(tcid, "Required Customer", "Clicked!!!",ResultPath,xwpfRun);
			logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc3));

			Thread.sleep(2000);
			driver.findElement(By.xpath("//img[@src='app/images/IconCatalogueManagement.svg']")).click();
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='csr'][contains(text(),'Store Enquiry')]")));
			driver.findElement(By.xpath("//*[@id='csr'][contains(text(),'Store Enquiry')]")).click();
			driver.findElement(By.xpath("//h4[contains(text(),'Store Enquiry')]")).isDisplayed();

			driver.findElement(By.xpath("(//tr[@class='order-number-table cursor-pointer']/td[contains(text(),'"+storeid+"')])[2]")).isDisplayed();

			String wholesale_sc4=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc4, driver);
			utilityFileWriteOP.writeToLog(tcid, "Store number "+storeid+" is been displayed in page", "PASS",ResultPath,xwpfRun);
			logger.log(LogStatus.PASS, "Store number "+storeid+" is been displayed in page");
			logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc4));

			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollBy(0,300)");
			Thread.sleep(2000);

			driver.findElement(By.xpath("(//tr[@class='order-number-table cursor-pointer']/td[contains(text(),'"+storeid+"')])[2]")).click();


			//				ResultPath=System.getProperty("user.dir")+"/"+ResultPath;

			Thread.sleep(4000);

			String currentWindow=driver.getWindowHandle();
			Set<String> windows=driver.getWindowHandles();

			for(String window:windows)
			{
				if(!window.equals(currentWindow))
				{
					driver.switchTo().window(window);
					//utilityFileWriteOP.writeToLog(tcid, "AWS Console Window", "Switched!!!",ResultPath, xwpfRun,"");
					break;
				}
			}

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@class='edit-text' and contains(text(),'Edit')]")));
			System.out.println("Count of Store :"+StoreId.length);

			if(customer.equalsIgnoreCase("amazon")){

				//					ResultPath=System.getProperty("user.dir")+"/"+ResultPath;

				System.out.println("Resultpath in function call is"+ResultPath);

				for(String eachStoreID:StoreId) {

					Map<String, String> columnVals=Additional_Functions.ReadStoreDetails.readStoreDtl(System.getProperty("user.dir")+"/DownloadFiles/"+FinalFilePath,eachStoreID);

					String strid=driver.findElement(By.xpath("//div[@class='grey-background']/div/div/span[text()='Store ID']/..")).getText().trim();
					strid = strid.substring(strid.indexOf(":")+1).trim();
					System.out.println("Store id after trim:"+strid);
					String mapStoreid=columnVals.get("STOREID").toString().trim();
					if(strid.equals(mapStoreid)){
						System.out.println("PASS");
						System.out.println("Inside storeid if");
					}else{
						System.out.println("FAIL");
						System.out.println("Inside storeid else");
						res=false;
					}

					String strnm=driver.findElement(By.xpath("//div[@class='grey-background']/div/div/span[text()='Store Name']/..")).getText().trim();
					strnm = strnm.substring(strnm.indexOf(":")+1).trim();
					System.out.println("After trim:"+strnm);
					String mapStorename=columnVals.get("STORE-NAME").toString().trim();
					if(strnm.equals(mapStorename)){
						System.out.println(" STore name PASS");
						res=true;
					}else{
						System.out.println("FAIL");
						res=false;
					}

					String strpfx=driver.findElement(By.xpath("//div[@class='grey-background']/div/div/span[text()='Store ID Prefix']/..")).getText().trim();
					strpfx = strpfx.substring(strpfx.indexOf(":")+1).trim();
					System.out.println("After trim:"+strpfx);
					String mapStoreprfx=columnVals.get("STORE_ID_PREFIX").toString().trim();
					if(strpfx.equals(mapStoreprfx)){

						System.out.println(" Store ID Prefix PASS");
						String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
						TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
						utilityFileWriteOP.writeToLog(tcid, "Store number "+storeid+" with prefix validated successfully", "PASS",ResultPath,xwpfRun);
						logger.log(LogStatus.PASS, "Store number "+storeid+" with prefix validated successfully");
						logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc5));

						res=true;
					}else{
						System.out.println("FAIL");
						res=false;
					}


					String avl=driver.findElement(By.xpath("//div[@class='grey-background']/div/div/span[text()='Availability']/..")).getText().trim();
					avl = avl.substring(avl.indexOf(":")+1).trim();
					System.out.println("After trim:"+avl);
					String mapavl=columnVals.get("availability").toString().trim();
					if(avl.equals(mapavl)){
						System.out.println(" Availability PASS");
						res=true;
					}else{
						System.out.println("FAIL");
						res=false;
					}

					String trdnm=driver.findElement(By.xpath("//div[@class='grey-background']/div/div/span[text()='Trading Name']/..")).getText().trim();
					trdnm = trdnm.substring(trdnm.indexOf(":")+1).trim();
					System.out.println("After trim:"+trdnm);
					String maptrdname=columnVals.get("TRADING-NAME").toString().trim();
					if(trdnm.equals(maptrdname)){
						System.out.println(" Trading name PASS");
						res=true;
					}else{
						System.out.println("FAIL");
						res=false;
					}

					String desc=driver.findElement(By.xpath("//div[@class='grey-background']/div/div/span[text()='Description']/..")).getText().trim();
					desc = desc.substring(desc.indexOf(":")+1).trim();
					System.out.println("After trim:"+desc);
					String mapdesc=columnVals.get("description").toString().trim();
					if(desc.equals(mapdesc)){
						System.out.println("Description PASS");
						res=true;
					}else{
						System.out.println("FAIL");
						res=false;
					}

					driver.findElement(By.xpath("//div[@id='repricingContainer']//div[text()='Status']/../div[2]")).click();
					String stts=driver.findElement(By.xpath("//div[@id='repricingContainer']//div[text()='Status']/../div[2]")).getText().trim();

					System.out.println("After trim:"+stts);
					String mapstatus=columnVals.get("STATUS").toString().trim();
					if(stts.equals(mapstatus)){
						System.out.println("Status PASS");
						res=true;
					}else{
						System.out.println("FAIL");
						res=false;
					}

					String code=driver.findElement(By.xpath("//div[@id='repricingContainer']//div[text()='Region Code']/../div[2]")).getText().trim();
					driver.findElement(By.xpath("//div[@id='repricingContainer']//div[text()='Region Code']/../div[2]")).click();
					System.out.println("After trim:"+code);
					String mapcode=columnVals.get("REGION-CODE").toString().trim();
					if(code.equals(mapcode)){
						System.out.println("Region code PASS");
						String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
						TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
						utilityFileWriteOP.writeToLog(tcid, "Store main details validated successfully", "PASS",ResultPath,xwpfRun);
						logger.log(LogStatus.PASS, "Store main details validated successfully");
						logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc5));
						res=true;
					}else{
						System.out.println("FAIL");
						res=false;
					}




					driver.findElement(By.xpath("//a[contains(text(),'Address')]")).isDisplayed();
					driver.findElement(By.xpath("//a[contains(text(),'Address')]")).click();
					Thread.sleep(2000);

					driver.findElement(By.xpath("//div[@id='repricingContainer']//div[text()='Address Name']/../div[2]")).click();
					String addr=driver.findElement(By.xpath("//div[@id='repricingContainer']//div[text()='Address Name']/../div[2]")).getText().trim();

					System.out.println("After trim:"+addr);
					String mapaddr=columnVals.get("ADDRESS-NAME").toString().trim();
					if(addr.equals(mapaddr)){

						System.out.println(" Address PASS");
						String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
						TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
						utilityFileWriteOP.writeToLog(tcid, "Store address details validated successfully", "PASS",ResultPath,xwpfRun);
						logger.log(LogStatus.PASS, "Store address details validated successfully");
						logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc5));
						res=true;
					}else{
						System.out.println("FAIL");
						res=false;
					}



					driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).isDisplayed();
					driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).click();
					Thread.sleep(2000);

					driver.findElement(By.xpath("//div[@id='repricingContainer']//span[text()='Supplying Depot ']/..")).click();
					String supp=driver.findElement(By.xpath("//div[@id='repricingContainer']//span[text()='Supplying Depot ']/..")).getText().trim();
					supp = supp.substring(supp.indexOf(":")+1).trim();
					System.out.println("After trim:"+supp);
					String mapsupp=columnVals.get("SUPPLYING-DEPOT-NAME").toString().trim();
					if(supp.equals(mapsupp)){
						System.out.println(" Supplying Depot PASS");
						System.out.println(" Address PASS");

						String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
						TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
						utilityFileWriteOP.writeToLog(tcid, "Store supplying depot details validated successfully", "PASS",ResultPath,xwpfRun);
						logger.log(LogStatus.PASS, "Store supplying depot details validated successfully");
						logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc5));
						res=true;
					}else{
						System.out.println("FAIL");
						res=false;
					}


					driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).isDisplayed();
					driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).click();
					Thread.sleep(2000);


					String ops=driver.findElement(By.xpath("((//h4[text()='Current Opportunity']//parent::div//following-sibling::div//table)[1]//td[text()='ALL']//parent::tr//td)[2]")).getText().trim();
					driver.findElement(By.xpath("((//h4[text()='Current Opportunity']//parent::div//following-sibling::div//table)[1]//td[text()='ALL']//parent::tr//td)[2]")).click();
					System.out.println("After trim:"+ops);
					String mapops=columnVals.get("ORDER-OPS-ALL").toString().trim();
					if(ops.equals(mapops)){
						System.out.println("Deliverying Opportunities PASS");

						String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
						TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
						utilityFileWriteOP.writeToLog(tcid, "Store OPS details validated successfully", "PASS",ResultPath,xwpfRun);
						logger.log(LogStatus.PASS, "Store OPS details validated successfully");
						logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc5));
						res=true;

					}else{
						System.out.println("FAIL");
						res=false;
					}
				}

			}

			else if(customer.equalsIgnoreCase("mpk")){


				String path=System.getProperty("user.dir")+"/DownloadFiles/"+FinalFilePath;

				System.out.println("File path in function call is"+path);
				System.out.println("File path in "+FinalFilePath);


				for(String eachStoreID:StoreId) {


					Map<String, String> columnVals=Additional_Functions.ReadStoreDetails.readStoreDtl(System.getProperty("user.dir")+"/DownloadFiles/"+FinalFilePath,eachStoreID);

					String strid=driver.findElement(By.xpath("//div[@class='grey-background']/div/div/span[text()='Store ID']/..")).getText().trim();
					strid = strid.substring(strid.indexOf(":")+1).trim();
					System.out.println("Store id after trim:"+strid);
					String mapStoreid=columnVals.get("STOREID").toString().trim();
					if(strid.equals(mapStoreid)){
						System.out.println("PASS");
						res=true;
						System.out.println("Inside storeid if");
					}else{
						System.out.println("FAIL");
						System.out.println("Inside storeid else");
						res=false;
					}

					String strnm=driver.findElement(By.xpath("//div[@class='grey-background']/div/div/span[text()='Store Name']/..")).getText().trim();
					strnm = strnm.substring(strnm.indexOf(":")+1).trim();
					System.out.println("After trim:"+strnm);
					String mapStorename=columnVals.get("STORE-NAME").toString().trim();
					if(strnm.equals(mapStorename)){
						System.out.println(" STore name PASS");
						res=true;
					}else{
						System.out.println("FAIL");
						res=false;
					}

					String strpfx=driver.findElement(By.xpath("//div[@class='grey-background']/div/div/span[text()='Store ID Prefix']/..")).getText().trim();
					strpfx = strpfx.substring(strpfx.indexOf(":")+1).trim();
					System.out.println("After trim:"+strpfx);
					String mapStoreprfx=columnVals.get("STORE_ID_PREFIX").toString().trim();
					if(strpfx.equals(mapStoreprfx)){

						System.out.println(" Store ID Prefix PASS");
						String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
						TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
						utilityFileWriteOP.writeToLog(tcid, "Store number "+storeid+" with prefix validated successfully", "PASS",ResultPath,xwpfRun);
						logger.log(LogStatus.PASS, "Store number "+storeid+" with prefix validated successfully");
						logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc5));
						res=true;

					}else{
						System.out.println("FAIL");
						res=false;
					}


					String avl=driver.findElement(By.xpath("//div[@class='grey-background']/div/div/span[text()='Availability']/..")).getText().trim();
					avl = avl.substring(avl.indexOf(":")+1).trim();
					System.out.println("After trim:"+avl);
					String mapavl=columnVals.get("availability").toString().trim();
					if(avl.equals(mapavl)){
						System.out.println(" Availability PASS");
						res=true;
					}else{
						System.out.println("FAIL");
						res=false;
					}

					String trdnm=driver.findElement(By.xpath("//div[@class='grey-background']/div/div/span[text()='Trading Name']/..")).getText().trim();
					trdnm = trdnm.substring(trdnm.indexOf(":")+1).trim();
					System.out.println("After trim:"+trdnm);
					String maptrdname=columnVals.get("TRADING-NAME").toString().trim();
					if(trdnm.equals(maptrdname)){
						System.out.println(" Trading name PASS");
						res=true;
					}else{
						System.out.println("FAIL");
						res=false;
					}

					String desc=driver.findElement(By.xpath("//div[@class='grey-background']/div/div/span[text()='Description']/..")).getText().trim();
					desc = desc.substring(desc.indexOf(":")+1).trim();
					System.out.println("After trim:"+desc);
					String mapdesc=columnVals.get("description").toString().trim();
					if(desc.equals(mapdesc)){
						System.out.println("Description PASS");
						res=true;
					}else{
						System.out.println("FAIL");
						res=false;
					}

					driver.findElement(By.xpath("//div[@id='repricingContainer']//div[text()='Allocation Priority']/../div[2]")).click();
					String prior=driver.findElement(By.xpath("//div[@id='repricingContainer']//div[text()='Allocation Priority']/../div[2]")).getText().trim();

					System.out.println("After trim:"+prior);
					String mapprior=columnVals.get("ALLOCATION_PRIORITY").toString().trim();
					if(prior.equals(mapprior)){
						System.out.println("Allocation Priority PASS");
						res=true;
					}else{
						System.out.println("FAIL");
						res=false;
					}

					driver.findElement(By.xpath("//div[@id='repricingContainer']//div[text()='Status']/../div[2]")).click();
					String stts=driver.findElement(By.xpath("//div[@id='repricingContainer']//div[text()='Status']/../div[2]")).getText().trim();

					System.out.println("After trim:"+stts);
					String mapstatus=columnVals.get("STATUS").toString().trim();
					if(stts.equals(mapstatus)){
						System.out.println("Status PASS");
						String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
						TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
						utilityFileWriteOP.writeToLog(tcid, "Store main details validated successfully", "PASS",ResultPath,xwpfRun);
						logger.log(LogStatus.PASS, "Store main details validated successfully");
						logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc5));
						res=true;
					}else{
						System.out.println("FAIL");
						res=false;
					}

					driver.findElement(By.xpath("//a[contains(text(),'Address')]")).isDisplayed();
					driver.findElement(By.xpath("//a[contains(text(),'Address')]")).click();
					Thread.sleep(2000);

					driver.findElement(By.xpath("//div[@id='repricingContainer']//div[text()='Address Name']/../div[2]")).click();
					String addr=driver.findElement(By.xpath("//div[@id='repricingContainer']//div[text()='Address Name']/../div[2]")).getText().trim();

					System.out.println("After trim:"+addr);
					String mapaddr=columnVals.get("ADDRESS-POSTCODE").toString().trim();
					if(addr.equals(mapaddr)){

						System.out.println(" Address PASS");
						String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
						TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
						utilityFileWriteOP.writeToLog(tcid, "Store address details validated successfully", "PASS",ResultPath,xwpfRun);
						logger.log(LogStatus.PASS, "Store address details validated successfully");
						logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc5));
						res=true;
					}else{
						System.out.println("FAIL");
						res=false;
					}



					driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).isDisplayed();
					driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).click();
					Thread.sleep(2000);

					driver.findElement(By.xpath("//div[@id='repricingContainer']//span[text()='Supplying Depot ']/..")).click();
					String supp=driver.findElement(By.xpath("//div[@id='repricingContainer']//span[text()='Supplying Depot ']/..")).getText().trim();
					supp = supp.substring(supp.indexOf(":")+1).trim();
					System.out.println("After trim:"+supp);
					String mapsupp=columnVals.get("SUPPLYING-DEPOT-NAME").toString().trim();
					if(supp.equals(mapsupp)){
						System.out.println(" Supplying Depot PASS");
						System.out.println(" Address PASS");

						String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
						TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
						utilityFileWriteOP.writeToLog(tcid, "Store supplying depot details validated successfully", "PASS",ResultPath,xwpfRun);
						logger.log(LogStatus.PASS, "Store supplying depot details validated successfully");
						logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc5));
						res=true;
					}else{
						System.out.println("FAIL");
						res=false;
					}


					driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).isDisplayed();
					driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).click();
					Thread.sleep(2000);

					String arr[]=category.split(",");


					for(int i=0;i<arr.length;i++){

						String ops=driver.findElement(By.xpath("((//h4[text()='Current Opportunity']//parent::div//following-sibling::div//table)[1]//td[text()='"+arr[i]+"']//parent::tr//td)[2]")).getText().trim();
						driver.findElement(By.xpath("((//h4[text()='Current Opportunity']//parent::div//following-sibling::div//table)[1]//td[text()='"+arr[i]+"']//parent::tr//td)[2]")).click();
						System.out.println("After trim:"+ops);
						String mapops=columnVals.get("ORDER-OPS-"+arr[i]+"").toString().trim();
						if(ops.equals(mapops)){
							System.out.println("Deliverying Opportunities PASS" +arr[i]);

							String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
							TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
							utilityFileWriteOP.writeToLog(tcid, "Store OPS details validated successfully", "PASS",ResultPath,xwpfRun);
							logger.log(LogStatus.PASS, "Store OPS details validated successfully");
							logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc5));
							res=true;
						}
						else{
							System.out.println("FAIL");
							res=false;
						}}
				}

			}
			else {


				String path=System.getProperty("user.dir")+"/DownloadFiles/"+FinalFilePath;

				System.out.println("File path in function call is"+path);
				System.out.println("File path in "+FinalFilePath);


				for(String eachStoreID:StoreId) {


					Map<String, String> columnVals=Additional_Functions.ReadStoreDetails.readStoreDtl(System.getProperty("user.dir")+"/DownloadFiles/"+FinalFilePath,eachStoreID);

					String strid=driver.findElement(By.xpath("//div[@class='grey-background']/div/div/span[text()='Store ID']/..")).getText().trim();
					strid = strid.substring(strid.indexOf(":")+1).trim();
					System.out.println("Store id after trim:"+strid);
					String mapStoreid=columnVals.get("STOREID").toString().trim();
					if(strid.equals(mapStoreid)){
						System.out.println("PASS");
						res=true;
						System.out.println("Inside storeid if");
					}else{
						System.out.println("FAIL");
						System.out.println("Inside storeid else");
						res=false;
					}

					String strnm=driver.findElement(By.xpath("//div[@class='grey-background']/div/div/span[text()='Store Name']/..")).getText().trim();
					strnm = strnm.substring(strnm.indexOf(":")+1).trim();
					System.out.println("After trim:"+strnm);
					String mapStorename=columnVals.get("STORE-NAME").toString().trim();
					if(strnm.equals(mapStorename)){
						System.out.println(" STore name PASS");
						res=true;
					}else{
						System.out.println("FAIL");
						res=false;
					}

					String strpfx=driver.findElement(By.xpath("//div[@class='grey-background']/div/div/span[text()='Store ID Prefix']/..")).getText().trim();
					strpfx = strpfx.substring(strpfx.indexOf(":")+1).trim();
					System.out.println("After trim:"+strpfx);
					String mapStoreprfx=columnVals.get("STORE_ID_PREFIX").toString().trim();
					if(strpfx.equals(mapStoreprfx)){

						System.out.println(" Store ID Prefix PASS");
						String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
						TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
						utilityFileWriteOP.writeToLog(tcid, "Store number "+storeid+" with prefix validated successfully", "PASS",ResultPath,xwpfRun);
						logger.log(LogStatus.PASS, "Store number "+storeid+" with prefix validated successfully");
						logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc5));
						res=true;

					}else{
						System.out.println("FAIL");
						res=false;
					}


					String avl=driver.findElement(By.xpath("//div[@class='grey-background']/div/div/span[text()='Availability']/..")).getText().trim();
					avl = avl.substring(avl.indexOf(":")+1).trim();
					System.out.println("After trim:"+avl);
					String mapavl=columnVals.get("availability").toString().trim();
					if(avl.equals(mapavl)){
						System.out.println(" Availability PASS");
						res=true;
					}else{
						System.out.println("FAIL");
						res=false;
					}

					String trdnm=driver.findElement(By.xpath("//div[@class='grey-background']/div/div/span[text()='Trading Name']/..")).getText().trim();
					trdnm = trdnm.substring(trdnm.indexOf(":")+1).trim();
					System.out.println("After trim:"+trdnm);
					String maptrdname=columnVals.get("TRADING-NAME").toString().trim();
					if(trdnm.equals(maptrdname)){
						System.out.println(" Trading name PASS");
						res=true;
					}else{
						System.out.println("FAIL");
						res=false;
					}

					String desc=driver.findElement(By.xpath("//div[@class='grey-background']/div/div/span[text()='Description']/..")).getText().trim();
					desc = desc.substring(desc.indexOf(":")+1).trim();
					System.out.println("After trim:"+desc);
					String mapdesc=columnVals.get("description").toString().trim();
					if(desc.equals(mapdesc)){
						System.out.println("Description PASS");
						res=true;
					}else{
						System.out.println("FAIL");
						res=false;
					}

					driver.findElement(By.xpath("//div[@id='repricingContainer']//div[text()='Allocation Priority']/../div[2]")).click();
					String prior=driver.findElement(By.xpath("//div[@id='repricingContainer']//div[text()='Allocation Priority']/../div[2]")).getText().trim();

					System.out.println("After trim:"+prior);
					String mapprior=columnVals.get("ALLOCATION_PRIORITY").toString().trim();
					if(prior.equals(mapprior)){
						System.out.println("Allocation Priority PASS");
						res=true;
					}else{
						System.out.println("FAIL");
						res=false;
					}

					driver.findElement(By.xpath("//div[@id='repricingContainer']//div[text()='Status']/../div[2]")).click();
					String stts=driver.findElement(By.xpath("//div[@id='repricingContainer']//div[text()='Status']/../div[2]")).getText().trim();

					System.out.println("After trim:"+stts);
					String mapstatus=columnVals.get("STATUS").toString().trim();
					if(stts.equals(mapstatus)){
						System.out.println("Status PASS");
						String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
						TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
						utilityFileWriteOP.writeToLog(tcid, "Store main details validated successfully", "PASS",ResultPath,xwpfRun);
						logger.log(LogStatus.PASS, "Store main details validated successfully");
						logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc5));
						res=true;
					}else{
						System.out.println("FAIL");
						res=false;
					}

					driver.findElement(By.xpath("//a[contains(text(),'Address')]")).isDisplayed();
					driver.findElement(By.xpath("//a[contains(text(),'Address')]")).click();
					Thread.sleep(2000);

					driver.findElement(By.xpath("//div[@id='repricingContainer']//div[text()='Address Name']/../div[2]")).click();
					String addr=driver.findElement(By.xpath("//div[@id='repricingContainer']//div[text()='Address Name']/../div[2]")).getText().trim();

					System.out.println("After trim:"+addr);
					String mapaddr=columnVals.get("ADDRESS-NAME").toString().trim();
					if(addr.equals(mapaddr)){

						System.out.println(" Address PASS");
						String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
						TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
						utilityFileWriteOP.writeToLog(tcid, "Store address details validated successfully", "PASS",ResultPath,xwpfRun);
						logger.log(LogStatus.PASS, "Store address details validated successfully");
						logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc5));
						res=true;
					}else{
						System.out.println("FAIL");
						res=false;
					}



					driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).isDisplayed();
					driver.findElement(By.xpath("//a[contains(text(),'Supplying Depot')]")).click();
					Thread.sleep(2000);

					driver.findElement(By.xpath("//div[@id='repricingContainer']//span[text()='Supplying Depot ']/..")).click();
					String supp=driver.findElement(By.xpath("//div[@id='repricingContainer']//span[text()='Supplying Depot ']/..")).getText().trim();
					supp = supp.substring(supp.indexOf(":")+1).trim();
					System.out.println("After trim:"+supp);
					String mapsupp=columnVals.get("SUPPLYING-DEPOT-NAME").toString().trim();
					if(supp.equals(mapsupp)){
						System.out.println(" Supplying Depot PASS");
						System.out.println(" Address PASS");

						String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
						TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
						utilityFileWriteOP.writeToLog(tcid, "Store supplying depot details validated successfully", "PASS",ResultPath,xwpfRun);
						logger.log(LogStatus.PASS, "Store supplying depot details validated successfully");
						logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc5));
						res=true;
					}else{
						System.out.println("FAIL");
						res=false;
					}


					driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).isDisplayed();
					driver.findElement(By.xpath("//a[contains(text(),'Delivery Opportunities')]")).click();
					Thread.sleep(2000);

					String arr[]=category.split(",");


					for(int i=0;i<arr.length;i++){

						String ops=driver.findElement(By.xpath("((//h4[text()='Current Opportunity']//parent::div//following-sibling::div//table)[1]//td[text()='"+arr[i]+"']//parent::tr//td)[2]")).getText().trim();
						driver.findElement(By.xpath("((//h4[text()='Current Opportunity']//parent::div//following-sibling::div//table)[1]//td[text()='"+arr[i]+"']//parent::tr//td)[2]")).click();
						System.out.println("After trim:"+ops);
						String mapops=columnVals.get("ORDER-OPS-"+arr[i]+"").toString().trim();
						if(ops.equals(mapops)){
							System.out.println("Deliverying Opportunities PASS" +arr[i]);

							String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
							TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
							utilityFileWriteOP.writeToLog(tcid, "Store OPS details validated successfully", "PASS",ResultPath,xwpfRun);
							logger.log(LogStatus.PASS, "Store OPS details validated successfully");
							logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc5));
							res=true;
						}
						else{
							System.out.println("FAIL");
							res=false;
						}}
				}

			}

		}

		catch(Exception e){
			e.printStackTrace();
			String wholesale_sc16=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc16, driver);
			utilityFileWriteOP.writeToLog(tcid, "Error Store details validation", "PASS",ResultPath,xwpfRun);
			logger.log(LogStatus.FAIL, "Error Store details validation");
			logger.log(LogStatus.FAIL, logger.addScreenCapture(wholesale_sc16));

			res=false;
		}finally{
			return res;
		}

	}

	/*
	 * Method Name: compareExportCSVFile
	 * Functionality Description:Compare Exported Catalogue CSV file
	 * Created on:08/01/2020
	 * Created By:Morrisons
	 */

	public static boolean compareExportCSVFile(String storeid,String sourcefile,String targetFile,WebDriver driver, WebDriverWait wait, String ResultPath,XWPFRun xwpfRun, ExtentTest logger) throws Exception
	{
		boolean res=false;
		try{
			String[] StoreId = storeid.split(",");

			ResultPath=System.getProperty("user.dir")+"/"+ResultPath;


			for(String eachStoreID:StoreId) {

				Map<String, String> File1Vals=Additional_Functions.ReadStoreDetails.readStoreDtl(sourcefile,eachStoreID);

				Map<String, String> File2Vals=Additional_Functions.ReadStoreDetails.readStoreDtl(System.getProperty("user.dir")+"/ExportDownloadFiles/"+targetFile,eachStoreID);

				System.out.println("First file values" +File1Vals);
				System.out.println("Second file values" +File2Vals);
				String strid=File1Vals.get("STOREID").toString().trim();
				String avl=File1Vals.get("availability").toString().trim();
				String strnm=File1Vals.get("STORE-NAME").toString().trim();
				String status=File1Vals.get("STATUS").toString().trim();
				String add=File1Vals.get("TRADING-NAME").toString().trim();
				String supp=File1Vals.get("SUPPLYING-DEPOT-NAME").toString().trim();
				String cust=File1Vals.get("customer").toString().trim();
				String cat=File1Vals.get("catalogue").toString().trim();
				String stdpr=File1Vals.get("STORE_ID_PREFIX").toString().trim();
				String addnm=File1Vals.get("ADDRESS-NAME").toString().trim();
				String eff=File1Vals.get("EFFECTIVE_DATE").toString().trim();
				String desc=File1Vals.get("description").toString().trim();

				String avl1=File2Vals.get("availability").toString().trim();
				String strnm1=File2Vals.get("STORE-NAME").toString().trim();
				String status1=File2Vals.get("STATUS").toString().trim();
				String add1=File2Vals.get("TRADING-NAME").toString().trim();
				String supp1=File2Vals.get("SUPPLYING-DEPOT-NAME").toString().trim();
				String cust1=File2Vals.get("customer").toString().trim();
				String cat1=File2Vals.get("catalogue").toString().trim();
				String stdpr1=File2Vals.get("STORE_ID_PREFIX").toString().trim();
				String addnm1=File2Vals.get("ADDRESS-NAME").toString().trim();
				String eff1=File2Vals.get("EFFECTIVE_DATE").toString().trim();
				String desc1=File2Vals.get("description").toString().trim();

				String mapStoreid=File2Vals.get("STOREID").toString().trim();
				if(strid.equals(mapStoreid)
						&& avl.equalsIgnoreCase(avl1)
						&& strnm.equalsIgnoreCase(strnm1)
						&& status.equalsIgnoreCase(status1)
						&& add.equalsIgnoreCase(add1)
						&& supp.equalsIgnoreCase(supp1)
						&& cust.equalsIgnoreCase(cust1)
						&& cat.equalsIgnoreCase(cat1)
						&& stdpr.equalsIgnoreCase(stdpr1)
						&& addnm.equalsIgnoreCase(addnm1)
						&& eff.equalsIgnoreCase(eff1)
						&& desc.equalsIgnoreCase(desc1))
				{
					System.out.println("PASS");
					res=true;
					System.out.println("Inside storeid if");
					logger.log(LogStatus.PASS, "Both Uploaded and Exported Store file has been validated successfully");
				}else{
					System.out.println("FAIL");
					System.out.println("Inside storeid else");
					logger.log(LogStatus.FAIL, "Both Uploaded and Exported Store file validation failed");

					res=false;
				}


			}
		}

		catch(Exception e){
			e.printStackTrace();

		}

		finally{
			return res;
		}
	}

	/*
	 * Method Name: compareExportCSVFile
	 * Functionality Description:Compare Exported Catalogue CSV file
	 * Created on:08/01/2020
	 * Created By:Morrisons
	 */

	public static boolean compareExportCSVPromotionFile(String CSVcustomer,String itemId,String sourcefile,String targetFile,WebDriver driver, WebDriverWait wait, String ResultPath,XWPFRun xwpfRun, ExtentTest logger) throws Exception
	{
		boolean res=false;
		try{
			String[] ItemID = itemId.split(",");

			ResultPath=System.getProperty("user.dir")+"/"+ResultPath;
			
			System.out.println("Source file inside function call is: "+sourcefile);
			System.out.println("Target file is :"+System.getProperty("user.dir")+"/PromoCheckDownloadFiles/"+targetFile);

			for(String eachitemid:ItemID) {

				Map<String, String> File1Vals=Additional_Functions.ReadStoreDetails.readPromoItemDtl(sourcefile,eachitemid);

				Map<String, String> File2Vals=Additional_Functions.ReadStoreDetails.readOtherPromoItemDtl(System.getProperty("user.dir")+"/PromoCheckDownloadFiles/"+targetFile,eachitemid);

				System.out.println("First file values" +File1Vals);
				System.out.println("Second file values" +File2Vals);
				
				String pin=File1Vals.get("id:PIN").toString().trim();
				
				String england=File1Vals.get("prices:PWSP:GBP").toString().trim();
				String scotland=File1Vals.get("prices:PWSCP:GBP").toString().trim();
				
				String strtdate=File1Vals.get("START_DATE").toString().trim();
				String enddate=File1Vals.get("END_DATE").toString().trim();
				
				
				String mapPIN=File2Vals.get("Item ID").toString().trim();
				String customer1=File2Vals.get("Customer").toString().trim();
				String england1=File2Vals.get("Price(PWSP)").toString().trim();
				String scotland1=File2Vals.get("Price(PWSCP)").toString().trim();
				
				String strtdate1=File2Vals.get("Start Date").toString().trim();
				String enddate1=File2Vals.get("End Date").toString().trim();


				System.out.println("pin.equals(mapPIN"+pin.equals(mapPIN));
				System.out.println(CSVcustomer.contains(customer1));
				System.out.println(england1.contains(england));
				System.out.println(scotland1.contains(scotland));
				System.out.println(strtdate1.contains(strtdate));
				System.out.println(enddate1.contains(enddate));
				
				if(pin.equals(mapPIN)
						&& CSVcustomer.contains(customer1)
						&& england1.contains(england)
						&& scotland1.contains(scotland) ||scotland.contains(scotland1)
						&& strtdate1.contains(strtdate)
						&& enddate1.contains(enddate)
						)
				{
					System.out.println("PASS");
					res=true;
					System.out.println("Inside Promotion PIN  if");
					logger.log(LogStatus.PASS, "Both Uploaded and Exported Promotion file has been validated successfully");
				}else{
					System.out.println("FAIL");
					System.out.println("Inside Promotion PIN else");
					logger.log(LogStatus.FAIL, "Both Uploaded and Exported Promotion file validation failed");

					res=false;
				}


			}
		}

		catch(Exception e){
			e.printStackTrace();

		}

		finally{
			return res;
		}
	}

	/*
	 * Method Name: compareExportCSVFile
	 * Functionality Description:Compare Exported Catalogue CSV file
	 * Created on:08/01/2020
	 * Created By:Morrisons
	 */

	public static boolean compareExportCSVPromotionFile_Amazon(String CSVcustomer,String itemId,String sourcefile,String targetFile,WebDriver driver, WebDriverWait wait, String ResultPath,XWPFRun xwpfRun, ExtentTest logger) throws Exception
	{
		boolean res=false;
		try{
			String[] ItemID = itemId.split(",");

			ResultPath=System.getProperty("user.dir")+"/"+ResultPath;
			
			System.out.println("Source file inside function call is: "+sourcefile);
			System.out.println("Target file is :"+System.getProperty("user.dir")+"/PromoCheckDownloadFiles/"+targetFile);

			for(String eachitemid:ItemID) {

				Map<String,HashMap<String,String>> File1Vals=Additional_Functions.ReadStoreDetails.amazonReadPromoCSV(sourcefile,eachitemid);

				Map<String,HashMap<String,String>> File2Vals=Additional_Functions.ReadStoreDetails.amazonReadPromoReportCSV(System.getProperty("user.dir")+"/PromoCheckDownloadFiles/"+targetFile,eachitemid);

				System.out.println("First file values" +File1Vals);
				System.out.println("Second file values" +File2Vals);
				
				String pin=File1Vals.get(eachitemid).get("id:PIN").toString().trim();
				System.out.println("pin :"+pin);
				
				String england=File1Vals.get(eachitemid).get("prices:PWSPENG:GBP").toString().trim();
				System.out.println("england :"+england);
				String scotland=File1Vals.get(eachitemid).get("prices:PWSPSCO:GBP").toString().trim();
				System.out.println("scotland :"+scotland);
				String wales=File1Vals.get(eachitemid).get("prices:PWSPWAL:GBP").toString().trim();
				System.out.println("wales :"+wales);
				
				String strtdate=File1Vals.get(eachitemid).get("START_DATE").toString().trim();
				System.out.println("strtdate :"+strtdate);
				String enddate=File1Vals.get(eachitemid).get("END_DATE").toString().trim();
				System.out.println("enddate :"+enddate);
				
				

				String pin1=File1Vals.get(eachitemid).get("id:PIN").toString().trim();
				System.out.println("pin** :"+pin1);
				
				String england1=File1Vals.get(eachitemid).get("prices:PWSPENG:GBP").toString().trim();
				System.out.println("england** :"+england1);
				String scotland1=File1Vals.get(eachitemid).get("prices:PWSPSCO:GBP").toString().trim();
				System.out.println("scotland** :"+scotland1);
				String wales1=File1Vals.get(eachitemid).get("prices:PWSPWAL:GBP").toString().trim();
				System.out.println("wales **:"+wales1);
				
				String strtdate1=File1Vals.get(eachitemid).get("START_DATE").toString().trim();
				System.out.println("strtdate **:"+strtdate1);
				String enddate1=File1Vals.get(eachitemid).get("END_DATE").toString().trim();
				System.out.println("enddate** :"+enddate1);
				
				String customer1=File2Vals.get(eachitemid).get("customer").toString().trim();
				System.out.println("customer1 **:"+customer1);
				
				/*
				
				
				String mapPIN=File2Vals.get(eachitemid).get(" Item ID").toString().trim();
				System.out.println("mapPIN :"+mapPIN);
				
				String england1=File2Vals.get(eachitemid).get(" Price(PWSP)").toString().trim();
				System.out.println("england1 :"+england1);
				String scotland1=File2Vals.get(eachitemid).get(" Price(PWSCP)").toString().trim();
				System.out.println("scotland1 :"+scotland1);
				
				String strtdate1=File2Vals.get(eachitemid).get(" Start Date").toString().trim();
				System.out.println("strtdate1 :"+strtdate1);
				String enddate1=File2Vals.get(eachitemid).get(" End Date").toString().trim();
				System.out.println("enddate1 :"+enddate1);
*/

				
				if(pin.equals(pin1)
						&& CSVcustomer.contains(customer1)
						&& england1.contains(england)
						&& scotland1.contains(scotland)
						&& strtdate1.contains(strtdate)
						&& enddate1.contains(enddate)
						)
				{
					System.out.println("PASS");
					res=true;
					System.out.println("Inside Promotion PIN  if");
					logger.log(LogStatus.PASS, "Both Uploaded and Exported Promotion file has been validated successfully");
				}else{
					System.out.println("FAIL");
					System.out.println("Inside Promotion PIN else");
					logger.log(LogStatus.FAIL, "Both Uploaded and Exported Promotion file validation failed");

					res=false;
				}


			}
		}

		catch(Exception e){
			e.printStackTrace();

		}

		finally{
			return res;
		}
	}

	
	
	
	/*
	 * Method Name:Bulk upload catalogue
	 * Functionality Description:Bulk upload catalogue
	 * Created on:08/01/2020
	 * Created By:Morrisons
	 */

	public static boolean Bulkuploadcatalogue(WebDriver driver, WebDriverWait wait, String tcid,String customer,String channeltype,String errorfilepath,String filepath,String ResultPath,XWPFRun xwpfRun, ExtentTest logger){
		boolean res=false;

		try{

			ResultPath=System.getProperty("user.dir")+"/"+ResultPath;

			driver.findElement(By.xpath("//img[@alt='"+customer+"']")).click();
			Thread.sleep(5000);

			logger.log(LogStatus.PASS, "Sales order page is displayed for :"+customer+"");
			String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
			utilityFileWriteOP.writeToLog(tcid, "Required customer is", "clicked!!!",ResultPath,xwpfRun);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc5));

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//img[@src='app/images/IconCatalogueManagement.svg']")));
			driver.findElement(By.xpath("//img[@src='app/images/IconCatalogueManagement.svg']")).click();

			logger.log(LogStatus.PASS, "Catalogue management icon is clicked in sales order page of customer "+customer);
			String wholesale_sc6=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc6, driver);
			utilityFileWriteOP.writeToLog(tcid, "clicked on ", "Catalogue management icon!!!",ResultPath,xwpfRun);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc6));

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h5[contains(text(),'Catalogue Bulk Upload')]")));
			driver.findElement(By.xpath("//h5[contains(text(),'Catalogue Bulk Upload')]")).click();

			logger.log(LogStatus.PASS, "Catalogue Bulk upload  is clicked for customer :"+customer);
			String wholesale_sc7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc7, driver);
			utilityFileWriteOP.writeToLog(tcid, "Catalogue Bulk upload page is", "displayed!!!",ResultPath,xwpfRun);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc7));


			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@class='btn pull-right mrgn-r-2 search-button adv-search-btn form-control']")));

			WebElement selectElement = driver.findElement(By.xpath("//select[@name='catalogueType']"));
			Select select = new Select(selectElement);
			select.selectByValue(channeltype);
			Thread.sleep(2000);
			logger.log(LogStatus.PASS, "channelType is entered for customer :"+customer);
			String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
			utilityFileWriteOP.writeToLog(tcid, "channelType is", "entered!!!",ResultPath,xwpfRun);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc8));

			
			driver.findElement(By.xpath("//button[contains(text(),'VIEW')]")).click();
			Thread.sleep(2000);
			logger.log(LogStatus.PASS, "View button clicked ");
			String wholesale_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc9, driver);
			utilityFileWriteOP.writeToLog(tcid, "View button", "clicked!!!",ResultPath,xwpfRun);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc9));


			if(driver.findElement(By.xpath("//button[contains(text(),'Upload new catalogue file')]")).isDisplayed())
			{
				WebElement Catalogue=driver.findElement(By.xpath("//button[contains(text(),'Upload new catalogue file')]"));
				Catalogue.click();
			}

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='fullRangeUploadFrm']/ng2-file-input/div/div[2]/button")));
			System.out.println("Filepath :"+errorfilepath);

			WebElement upload=driver.findElement(By.xpath("//div[@class='ng2-file-input']/input[@type='file']"));
			upload.sendKeys(errorfilepath);

			logger.log(LogStatus.PASS, "Error message is displayed if user trying to uplaod files other than .CSV");
			String wholesale_sc15=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc15, driver);
			utilityFileWriteOP.writeToLog(tcid, "catalogue file uploadded", "successfully!!!",ResultPath,xwpfRun);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc15));

			try{

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//p[contains(text(),'Please select CSV File only')]")));
				if(driver.findElement(By.xpath("//p[contains(text(),'Please select CSV File only')]")).isDisplayed()){
					System.out.println("INSIDE IF");
					driver.findElement(By.xpath("//button[contains(text(),'Ok')]")).click();	
				}
				Thread.sleep(500);  
			}catch(Exception e)
			{
				e.printStackTrace();
				res=false;
			}
			WebElement upload1=driver.findElement(By.xpath("//div[@class='ng2-file-input']/input[@type='file']"));

			upload1.sendKeys(filepath);

			logger.log(LogStatus.PASS, "Now catalogue file with .CSV format uploaded successfully for customer :"+customer);
			String wholesale_sc100=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc100, driver);
			utilityFileWriteOP.writeToLog(tcid, "catalogue file uploadded", "successfully!!!",ResultPath,xwpfRun);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc100));

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@type='button' and text()='Submit']")));	             

			driver.findElement(By.xpath("//button[@type='button' and text()='Submit']")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("//*[@id='fullRangeUploadBox']/div/div/div[3]/div/button")).click();

			WebElement selectElement1 = driver.findElement(By.xpath("//select[@name='catalogueType']"));
			Select select1 = new Select(selectElement1);
			select1.selectByValue(channeltype);

			logger.log(LogStatus.PASS, "Channel type is entered");
			String wholesale_sc10=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc10, driver);
			utilityFileWriteOP.writeToLog(tcid, "Channel type is", "entered!!!",ResultPath,xwpfRun);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc10));

			//Thread.sleep(200);
			driver.findElement(By.xpath("//button[@type='submit'][contains(text(),'VIEW')]")).click();

			for(int j = 0; j < 20; j++)
			{
				try{
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("((//table[@id='tbl']//tr)[2]//td)[4][contains(text(),'Upload Completed')]")));
					break;
				}catch(Exception e)
				{
					driver.findElement(By.xpath("//button[@type='submit'][contains(text(),'VIEW')]")).click();

				}
			}
			String pattern = "ddMMyyyy";
			String dateInString =new SimpleDateFormat(pattern).format(new Date());
			if(driver.findElement(By.xpath("((//table[@id='tbl']//tr)[2]//td)[2]")).getText().contains("-"+customer+"-"+dateInString))
			{
				System.out.println("Catalogue name validated");
				res=true;
			}
			else
			{
				System.out.println("Catalogue name not validated");
				res=false;
			}
			Thread.sleep(2000);
			driver.findElement(By.xpath("((//table[@id='tbl']//tr)[2]//td)[3]")).click();


			logger.log(LogStatus.PASS, " Newly uploaded Catalogue file contain name as :"+customer+"-ddmmyyyyyhhmmss.csv downloaded successfully");
			String wholesale_sc11=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc11, driver);
			utilityFileWriteOP.writeToLog(tcid, "Newly uploaded Catalogue file", "downloaded!!!",ResultPath,xwpfRun);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc11));



		}catch(Exception e){
			e.printStackTrace();
			res=false;

		}finally{
			return res;
		}
	}


	/*
	 * Method Name: Bulkuploadcatalogue_CUST
	 * Functionality Description:Catalogue Bulk Upload for All customer
	 * Created on:08/01/2020
	 * Created By:Morrisons
	 */

	public static boolean Bulkuploadcatalogue_CUST(WebDriver driver, WebDriverWait wait, String tcid,String customer,String channeltype,String errorfilepath,String filepath,String ResultPath,XWPFRun xwpfRun, ExtentTest logger){
		boolean res=false;

		try{

			ResultPath=System.getProperty("user.dir")+"/"+ResultPath;

			driver.findElement(By.xpath("//img[@alt='"+customer+"']")).click();
			Thread.sleep(5000);


			logger.log(LogStatus.PASS, "Sales order page is displayed for :"+customer+"");
			String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
			utilityFileWriteOP.writeToLog(tcid, "Required customer is", "clicked!!!",ResultPath,xwpfRun);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc5));

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//img[@src='app/images/IconCatalogueManagement.svg']")));
			driver.findElement(By.xpath("//img[@src='app/images/IconCatalogueManagement.svg']")).click();

			logger.log(LogStatus.PASS, "Catalogue management icon is clicked in sales order page of customer "+customer);
			String wholesale_sc6=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc6, driver);
			utilityFileWriteOP.writeToLog(tcid, "clicked on ", "Catalogue management icon!!!",ResultPath,xwpfRun);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc6));

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h5[contains(text(),'Catalogue Bulk Upload')]")));
			driver.findElement(By.xpath("//h5[contains(text(),'Catalogue Bulk Upload')]")).click();

			logger.log(LogStatus.PASS, "Catalogue Bulk upload  is clicked for customer :"+customer);
			String wholesale_sc7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc7, driver);
			utilityFileWriteOP.writeToLog(tcid, "Catalogue Bulk upload page is", "displayed!!!",ResultPath,xwpfRun);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc7));




			if(driver.findElement(By.xpath("//button[contains(text(),'Upload new catalogue file')]")).isDisplayed())
			{
				WebElement Catalogue=driver.findElement(By.xpath("//button[contains(text(),'Upload new catalogue file')]"));
				Catalogue.click();
			}


			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='fullRangeUploadFrm']/ng2-file-input/div/div[2]/button")));
			System.out.println("ErrorFilepath :"+errorfilepath);

			WebElement upload=driver.findElement(By.xpath("//div[@class='ng2-file-input']/input[@type='file']"));
			upload.sendKeys(errorfilepath);

			logger.log(LogStatus.PASS, "Error message is displayed if user trying to uplaod files other than .CSV");
			String wholesale_sc15=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc15, driver);
			utilityFileWriteOP.writeToLog(tcid, "catalogue file uploadded", "successfully!!!",ResultPath,xwpfRun);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc15));

			try{
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//p[contains(text(),'Please select CSV File only')]")));
				if(driver.findElement(By.xpath("//p[contains(text(),'Please select CSV File only')]")).isDisplayed()){
					System.out.println("INSIDE IF");
					driver.findElement(By.xpath("//button[contains(text(),'Ok')]")).click();	
				}
				Thread.sleep(500);  
			}catch(Exception e){
				e.printStackTrace();
			}
			System.out.println("Filepath :"+filepath);
			WebElement upload1=driver.findElement(By.xpath("//div[@class='ng2-file-input']/input[@type='file']"));

			upload1.sendKeys(filepath);
			logger.log(LogStatus.PASS, "Now catalogue file with .CSV format uploaded successfully for customer :"+customer);
			String wholesale_sc100=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc100, driver);
			utilityFileWriteOP.writeToLog(tcid, "catalogue file uploadded", "successfully!!!",ResultPath,xwpfRun);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc100));

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@type='button' and text()='Submit']")));	             

			driver.findElement(By.xpath("//button[@type='button' and text()='Submit']")).click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@type='button' and text()='Ok']")));
			driver.findElement(By.xpath("//button[@type='button' and text()='Ok']")).click();



			for(int j = 0; j < 20; j++)
			{
				try{
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("((//table[@id='tbl']//tr)[2]//td)[4][contains(text(),'Upload Completed')]")));
					break;
				}catch(Exception e)
				{
					driver.navigate().refresh();

				}
			}
			String pattern = "ddMMyyyy";
			String dateInString =new SimpleDateFormat(pattern).format(new Date());
			if(driver.findElement(By.xpath("((//table[@id='tbl']//tr)[2]//td)[2]")).getText().contains("-"+customer+"-"+dateInString))
			{
				System.out.println("Catalogue name validated");
				res=true;
			}
			else
			{
				System.out.println("Catalogue name not validated");
				res=false;
			}
			Thread.sleep(2000);
			driver.findElement(By.xpath("((//table[@id='tbl']//tr)[2]//td)[3]")).click();


			logger.log(LogStatus.PASS, " Newly uploaded Catalogue file contain name as :"+customer+"-ddmmyyyyyhhmmss.csv downloaded successfully");
			String wholesale_sc11=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc11, driver);
			utilityFileWriteOP.writeToLog(tcid, "Newly uploaded Catalogue file", "downloaded!!!",ResultPath,xwpfRun);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc11));

		}catch(Exception e){
			e.printStackTrace();
			res=false;

		}finally{
			return res;
		}
	}

	/*
	 * Method Name:BulkuploadcatalogueHeaderIssue
	 * Functionality Description:Catalogue Bulk Upload No Header Issue
	 * Created on:08/01/2020
	 * Created By:Morrisons
	 */

	public static boolean BulkuploadcatalogueHeaderIssue(WebDriver driver, WebDriverWait wait, String tcid,String customer,String channeltype,String filepath,String ResultPath,XWPFRun xwpfRun, ExtentTest logger){
		boolean res=false;

		try{

			ResultPath=System.getProperty("user.dir")+"/"+ResultPath;

			driver.findElement(By.xpath("//img[@alt='"+customer+"']")).click();


			logger.log(LogStatus.PASS, "Required customer is entered");
			String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
			utilityFileWriteOP.writeToLog(tcid, "Required customer is", "entered!!!",ResultPath,xwpfRun);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc5));

			Thread.sleep(2000);
			driver.findElement(By.xpath("//img[@src='app/images/IconCatalogueManagement.svg']")).click();

			logger.log(LogStatus.PASS, "Catalogue management icon is clicked");
			String wholesale_sc6=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc6, driver);
			utilityFileWriteOP.writeToLog(tcid, "clicked on ", "Catalogue management icon!!!",ResultPath,xwpfRun);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc6));

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h5[contains(text(),'Catalogue Bulk Upload')]")));
			driver.findElement(By.xpath("//h5[contains(text(),'Catalogue Bulk Upload')]")).click();

			logger.log(LogStatus.PASS, "Catalogue Bulk upload  is clicked");
			String wholesale_sc7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc7, driver);
			utilityFileWriteOP.writeToLog(tcid, "Catalogue Bulk upload page is", "displayed!!!",ResultPath,xwpfRun);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc7));


			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@class='btn pull-right mrgn-r-2 search-button adv-search-btn form-control']")));

			WebElement selectElement = driver.findElement(By.xpath("//select[@name='catalogueType']"));
			Select select = new Select(selectElement);
			select.selectByValue(channeltype);

			logger.log(LogStatus.PASS, "channelType is entered");
			String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
			utilityFileWriteOP.writeToLog(tcid, "channelType is", "entered!!!",ResultPath,xwpfRun);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc8));

			//Thread.sleep(200);
			driver.findElement(By.xpath("//button[@class='btn pull-right mrgn-r-2 search-button adv-search-btn form-control']")).click();

			logger.log(LogStatus.PASS, "Submit button clicked ");
			String wholesale_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc9, driver);
			utilityFileWriteOP.writeToLog(tcid, "Invoice date is", "entered!!!",ResultPath,xwpfRun);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc9));

			Thread.sleep(2000);
			driver.findElement(By.xpath("//button[@class='btn pull-right mrgn-r-2 search-button adv-search-btn form-control']")).click();

			//now go to target page where we need to upload the csv file
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='Upload new catalogue file']")));
			driver.findElement(By.xpath("//button[text()='Upload new catalogue file']")).click();
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='fullRangeUploadFrm']/ng2-file-input/div/div[2]/button")));
			System.out.println("Filepath :"+filepath);

			WebElement upload=driver.findElement(By.xpath("//div[@class='ng2-file-input']/input[@type='file']"));

			upload.sendKeys(filepath);
			logger.log(LogStatus.PASS, "catalogue file uploaded");
			String wholesale_sc15=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc15, driver);
			utilityFileWriteOP.writeToLog(tcid, "catalogue file uploadded", "successfully!!!",ResultPath,xwpfRun);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc15));

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@type='button' and text()='Submit']")));	             

			driver.findElement(By.xpath("//button[@type='button' and text()='Submit']")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("//*[@id='fullRangeUploadBox']/div/div/div[3]/div/button")).click();

			WebElement selectElement1 = driver.findElement(By.xpath("//select[@name='catalogueType']"));
			Select select1 = new Select(selectElement1);
			select1.selectByValue(channeltype);

			logger.log(LogStatus.PASS, "Channel type is entered");
			String wholesale_sc10=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc10, driver);
			utilityFileWriteOP.writeToLog(tcid, "Channel type is", "entered!!!",ResultPath,xwpfRun);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc10));

			//Thread.sleep(200);
			driver.findElement(By.xpath("//button[@type='submit'][contains(text(),'VIEW')]")).click();

			for(int i = 0; i < 15; i++)
			{
				try{
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("((//table[@id='tbl']//tr)[2]//td)[4][contains(text(),'Validation Failed')]")));
					break;
				}catch(Exception e)
				{
					driver.findElement(By.xpath("//button[@type='submit'][contains(text(),'VIEW')]")).click();

				}
			}

			driver.findElement(By.xpath("((//table[@id='tbl']//tr)[2]//td)[5]")).click();


			logger.log(LogStatus.PASS, "Validation failed for newly uploaded file and Error file is downloaded");
			String wholesale_sc11=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc11, driver);
			utilityFileWriteOP.writeToLog(tcid, "Error file is", "downloaded!!!",ResultPath,xwpfRun);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc11));

			res=true;

		}



		catch(Exception e){
			e.printStackTrace();
			res=false;

		}finally{
			return res;
		}
	}

	/*
	 * Method Name:NASVALIDATION
	 * Functionality Description:Validate file generation at NAS path
	 * Created on:08/01/2020
	 * Created By:Morrisons
	 */

	public static boolean NASVALIDATION(WebDriver driver, WebDriverWait wait, String tcid,String filepath,String SFTPUserName,String SFTPHostName,int SFTPPort,String SFTPPassword,String NasPath,String filename,String ResultPath,XWPFRun xwpfRun, ExtentTest logger){
		boolean res=false;





		JSch jsch = new JSch();
		Session session = null;



		try {

			boolean isFilePresent = false;
			session = jsch.getSession(SFTPUserName, SFTPHostName, SFTPPort);
			session.setConfig("StrictHostKeyChecking", "no");
			session.setPassword(SFTPPassword);
			session.connect();

			Channel channel = session.openChannel("sftp");
			channel.connect();

			ChannelSftp sftpchannel = (ChannelSftp) channel;

			sftpchannel.cd(NasPath);

			System.out.println("The Current path is " + sftpchannel.pwd());

			Vector<ChannelSftp.LsEntry> list = sftpchannel.ls("*.csv");




			for(ChannelSftp.LsEntry entry: list) {
				String NasFiles = entry.getFilename();


				System.out.println(filename);

				if(NasFiles.contains(filename)){


					System.out.println("File :"+filename+" is present in NAS");

					res = true;
					break;
				}




			}

			sftpchannel.exit();

			session.disconnect();


		} catch (Exception e1) {
			// TODO: handle exception
			e1.printStackTrace();
			System.out.println(e1);
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during catalogue file search in nas ", "Due to :"+e1);
		}
		finally{
			return res;
		}
	}

	/*
	 * Method Name: navigatetocatalogue
	 * Functionality Description:Navigate through Catalogue 
	 * Created on:08/01/2020
	 * Created By:Morrisons
	 */
	public static boolean navigatetocatalogue(WebDriver driver, WebDriverWait wait, String tcid,String customer,String itemid,String cataloguetype, String ResultPath,XWPFRun xwpfRun, ExtentTest logger){
		boolean res=false;
		try{

			ResultPath=System.getProperty("user.dir")+"/"+ResultPath;

			driver.findElement(By.xpath("//img[@alt='"+customer+"']")).click();
			Thread.sleep(2000);
			logger.log(LogStatus.PASS, "Clicked on '"+customer+"' and home page for "+customer+" opened successfully");
			String wholesale_sc1=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc1, driver);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc1));
			utilityFileWriteOP.writeToLog(tcid, "Required Customer", "Clicked!!!",ResultPath,xwpfRun);
			
			driver.findElement(By.xpath("//img[@src='app/images/IconCatalogueManagement.svg']")).click();
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='csr']")));
			driver.findElement(By.xpath("//*[@id='csr']")).click();

			logger.log(LogStatus.PASS, "Clicked on Catalogue management and User  navigated to catalogue management page successfully for customer : "+customer);
			String wholesale_sc2=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc2, driver);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc2));
			utilityFileWriteOP.writeToLog(tcid, "Catalogue management", "Clicked!!!",ResultPath,xwpfRun);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='search-order-border']/form/div[1]/label")));

			WebElement selectElement = driver.findElement(By.xpath("//select[@name='catalogueType']"));
			Select select = new Select(selectElement);
			select.selectByValue(cataloguetype);

			logger.log(LogStatus.PASS, "Select "+cataloguetype+" in catalogue type select box for customer : '"+customer+"'");
			String wholesale_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc3, driver);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc3));
			utilityFileWriteOP.writeToLog(tcid, "Catalogue type ", "selected!!!",ResultPath,xwpfRun);

			Thread.sleep(2000);
			driver.findElement(By.xpath("//input[@id='catalogueCode']")).sendKeys(itemid);

			logger.log(LogStatus.PASS, "Provide item pin as :"+itemid+" in the product code field");
			String wholesale_sc4=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc4, driver);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc4));
			utilityFileWriteOP.writeToLog(tcid, "Item PIN", "Provided!!!",ResultPath,xwpfRun);

			Thread.sleep(2000);
			driver.findElement(By.xpath("//button[@class='btn pull-right mrgn-r-2 search-button adv-search-btn form-control']")).click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//td[contains(text(), '"+itemid.trim()+"')])[2]")));

			logger.log(LogStatus.PASS, "Search Results for Products fetched successfully for that searched item pin "+itemid+"for customer :"+customer);
			String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc5));
			utilityFileWriteOP.writeToLog(tcid, "Search Results for that item PIN", "displayed!!!",ResultPath,xwpfRun);

			driver.findElement(By.xpath("(//td[contains(text(), '"+itemid.trim()+"')])[2]")).click();
			Thread.sleep(2000);
			ArrayList<String> tabs_windows = new ArrayList<String> (driver.getWindowHandles());
			driver.switchTo().window(tabs_windows.get(1));
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h2[contains(text(),'PRODUCT DETAILS')]")));

			Actions action = new Actions(driver);
			WebElement tableElement1=driver.findElement(By.xpath("//h2[contains(text(),'Product Identifiers')]")); 
			action.moveToElement(tableElement1);
			action.perform();
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollBy(0,200)");
			Thread.sleep(500);

			logger.log(LogStatus.PASS, "Product Identifiers details for item pin : "+itemid+" for customer "+customer+" displayed successfully" );
			String wholesale_sc6=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc6, driver);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc6));
			utilityFileWriteOP.writeToLog(tcid, "Product Identifiers details", "displayed!!!",ResultPath,xwpfRun);

			WebElement tableElement2=driver.findElement(By.xpath("//h2[contains(text(),'Product Pricing')]")); 
			action.moveToElement(tableElement2);
			action.perform();
			js.executeScript("window.scrollBy(0,200)");

			logger.log(LogStatus.PASS, "Product Pricing details for item pin : "+itemid+" for customer "+customer+" displayed successfully");
			String wholesale_sc7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc7, driver);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc7));
			utilityFileWriteOP.writeToLog(tcid, "Product Pricing details", "displayed!!!",ResultPath,xwpfRun);

			WebElement tableElement3=driver.findElement(By.xpath("//h2[contains(text(),'Product Attributes')]"));
			action.moveToElement(tableElement3);
			action.perform();

			js.executeScript("window.scrollBy(0,1000)");

			String wholesale_sc8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc8, driver);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc8));
			utilityFileWriteOP.writeToLog(tcid, "Product Attributes details", "displayed!!!",ResultPath,xwpfRun);

			if(driver.findElement(By.xpath("//h2[contains(text(),'PRODUCT DETAILS')]")).isDisplayed()){
				res=true;
			}else
				res=false;

			driver.close();
			ArrayList<String> tabs_windows1 = new ArrayList<String> (driver.getWindowHandles());
			driver.switchTo().window(tabs_windows1.get(0));
		}
		catch(Exception e){
			e.printStackTrace();
			res=false;
		}finally{
			return res;
		}
	}
	
	
	/*
	 * Method Name: navigatetoCustomerType
	 * Functionality Description:Navigate through Catalogue for specific customer
	 * Created on:30/01/2020
	 * Created By:Morrisons
	 */
	public static boolean navigatetoCustomerType(WebDriver driver, WebDriverWait wait, String tcid,String customer, String ResultPath,XWPFRun xwpfRun, ExtentTest logger){
		boolean res=false;
		try{

			ResultPath=System.getProperty("user.dir")+"/"+ResultPath;

			driver.findElement(By.xpath("//img[@alt='"+customer+"']")).click();
			Thread.sleep(2000);
			logger.log(LogStatus.PASS, "Clicked on '"+customer+"' and home page for "+customer+" opened successfully");
			String wholesale_sc1=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc1, driver);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc1));
			utilityFileWriteOP.writeToLog(tcid, "Required Customer", "Clicked!!!",ResultPath,xwpfRun);
			
			Thread.sleep(2000);
			res=true;
		}
		catch(Exception e){
			e.printStackTrace();
			res=false;
		}finally{
			return res;
		}
	}
	
	
	
	
	
	/*
	 * Method Name: newcatalogueitemvalidationcust
	 * Functionality Description:Catlalogue Item validation with portal and successfully uploaded CSV file
	 * Created on:08/01/2020
	 * Created By:Morrisons
	 */
	public static boolean newcatalogueitemvalidationcust(WebDriver driver, WebDriverWait wait, String tcid,String filename,String customer,String itemid,String cataloguetype, String ResultPath,XWPFRun xwpfRun, ExtentTest logger){
		boolean res=true;
		try{
			String[] ItemId = itemid.split(",");

			ResultPath=System.getProperty("user.dir")+"/"+ResultPath;

			driver.findElement(By.xpath("//img[@alt='"+customer+"']")).click();


			utilityFileWriteOP.writeToLog(tcid, "Required Customer", "Clicked!!!",ResultPath,xwpfRun);
			logger.log(LogStatus.PASS, "customerid is Clicked");
			String wholesale_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc3, driver);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc3));

			Thread.sleep(2000);
			driver.findElement(By.xpath("//img[@src='app/images/IconCatalogueManagement.svg']")).click();
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='csr']")));
			driver.findElement(By.xpath("//*[@id='csr']")).click();

			//*[@id="catalogueType"]/option[2]

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='search-order-border']/form/div[1]/label")));

			WebElement selectElement = driver.findElement(By.xpath("//select[@name='catalogueType']"));
			Select select = new Select(selectElement);
			select.selectByValue(cataloguetype);

			utilityFileWriteOP.writeToLog(tcid, "Catalogur Type", "Entered!!!",ResultPath,xwpfRun);
			logger.log(LogStatus.PASS, "Catalogur Type is Entered");
			String wholesale_sc4=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc4, driver);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc4));

			Thread.sleep(2000);
			driver.findElement(By.xpath("//input[@id='catalogueCode']")).sendKeys(itemid);

			utilityFileWriteOP.writeToLog(tcid, "Required ProductID", "Entered!!!",ResultPath,xwpfRun);
			logger.log(LogStatus.PASS, "Required ProductID is Entered");
			String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc5));

			Thread.sleep(2000);
			driver.findElement(By.xpath("//button[@class='btn pull-right mrgn-r-2 search-button adv-search-btn form-control']")).click();

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='exportable']/table/tbody/tr[1]/td[1]")));
			driver.findElement(By.xpath("//*[@id='exportable']/table/tbody/tr[1]/td[1]")).click();
			Thread.sleep(2000);
			ArrayList<String> tabs_windows = new ArrayList<String> (driver.getWindowHandles());
			driver.switchTo().window(tabs_windows.get(1));
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h2[contains(text(),'PRODUCT DETAILS')]")));

			Actions action = new Actions(driver);
			WebElement tableElement1=driver.findElement(By.xpath("//h2[contains(text(),'Product Identifiers')]")); 
			action.moveToElement(tableElement1);
			action.perform();
			Thread.sleep(500);
			//***************************************************


			for(String eachItemid:ItemId) {

				System.out.println("Filename to be read is "+filename);
				System.out.println("The path to read file is :"+System.getProperty("user.dir")+"/DownloadedFiles/"+filename);
				Map<String, String> columnVals=Additional_Functions.ReadItemCatalogue.readCatalogue(System.getProperty("user.dir")+"/DownloadedFiles/"+filename,eachItemid);

				String mapPIN=columnVals.get("maps:PIN").toString().trim();
				if((mapPIN.length()>0)){
					String uiPIN=driver.findElement(By.xpath("(//div[@id='itemCodeMapping']//table//span[contains(text(),'PIN')]//ancestor::tr//td)[2]//span")).getText().trim();
					WebElement pin=driver.findElement(By.xpath("(//div[@id='itemCodeMapping']//table//span[contains(text(),'PIN')]//ancestor::tr//td)[2]//span"));
					JavascriptExecutor js = (JavascriptExecutor) driver;
					js.executeScript("arguments[0].scrollIntoView(true);", pin);
					if(uiPIN.equals(mapPIN)){
						System.out.println(" PIN PASS");
						res=res && true;
					}else{
						System.out.println("PIN FAIL");
						res=res && false;
					}
				}

				String mapLPC=columnVals.get("maps:LPC").toString().trim();
				if((mapLPC.length()>0)){
					String uiLPC=driver.findElement(By.xpath("(//div[@id='itemCodeMapping']//table//span[contains(text(),'LPC')]//ancestor::tr//td)[2]//span")).getText().trim();

					if(uiLPC.equals(mapLPC)){
						System.out.println("maps:LPC PASS");
						res=res && true;
					}else{
						System.out.println("maps:LPC FAIL");
						res=res && false;
					}
				}

				String mapEAN=columnVals.get("maps:EAN").toString().trim();
				if((mapEAN.length()>0)){
					String uiEAN=driver.findElement(By.xpath("(//div[@id='itemCodeMapping']//table//span[text()='EAN']//ancestor::tr//td)[2]//span")).getText().trim();
					WebElement EAN=driver.findElement(By.xpath("(//div[@id='itemCodeMapping']//table//span[text()='EAN']//ancestor::tr//td)[2]//span"));
					JavascriptExecutor js = (JavascriptExecutor) driver;
					js.executeScript("arguments[0].scrollIntoView(true);", EAN);
					if(uiEAN.contains(mapEAN)){
						System.out.println("maps:EAN PASS");
						utilityFileWriteOP.writeToLog(tcid, "Item details validation", "PASS",ResultPath,xwpfRun);
						logger.log(LogStatus.PASS, "Item details validation passed");
						String wholesale_sc15=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
						TakeScreenShot.saveScreenShot(wholesale_sc15, driver);
						logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc15));
						res=res && true;
					}else{
						System.out.println("maps:EAN FAIL");
						res=res && false;
					}
				}

				String mapCLIENTID=columnVals.get("maps:CLIENTID").toString().trim();
				if((mapCLIENTID.length()>0)){
					String uiCLIENTID=driver.findElement(By.xpath("(//div[@id='itemCodeMapping']//table//span[(text()='CLIENTID')]//ancestor::tr//td)[2]//span")).getText().trim();

					if(uiCLIENTID.contains(mapCLIENTID)){
						System.out.println("uiCLIENTID"+uiCLIENTID);
						System.out.println("mapCLIENTID"+mapCLIENTID);
						System.out.println("maps:CLIENTID PASS");
						res=res && true;
					}else{
						System.out.println("maps:CLIENTID FAIL");
						res=res && false;
					}
				}

				String mapWSCP=columnVals.get("prices:WSCP:GBP").toString().trim();
				if((mapWSCP.length()>0)){
					String uiWSCP=driver.findElement(By.xpath("(//div[@id='itemPriceInfo']//table//span[contains(text(),'WSCP')]//ancestor::tr//td)[2]//span")).getText().trim();

					if(uiWSCP.contains(mapWSCP) || mapWSCP.contains(uiWSCP)){
						System.out.println("prices:WSCP:GBP PASS");
						res=res && true;
					}else{
						System.out.println("prices:WSCP:GBP FAIL");
						res=res && false;
					}
				}

				String mapVAT=columnVals.get("prices:VAT:NONE").toString().trim();
				if((mapVAT.length()>0)){
					String uiVAT=driver.findElement(By.xpath("(//div[@id='itemPriceInfo']//table//span[contains(text(),'VAT')]//ancestor::tr//td)[2]//span")).getText().trim();

					if(uiVAT.contains(mapVAT)){
						System.out.println("uiVAT"+uiVAT);
						System.out.println("mapVAT"+mapVAT);
						System.out.println("prices:VAT:NONE PASS");
						res=res && true;
					}else{
						System.out.println("prices:VAT:NONE FAIL");
						res=res && false;
					}
				}

				String mapCS=columnVals.get("prices:CS:NONE").toString().trim();
				if((mapCS.length()>0)){
					String uiCS=driver.findElement(By.xpath("(//div[@id='itemPriceInfo']//table//span[contains(text(),'CS')]//ancestor::tr//td)[2]//span")).getText().trim();

					if(uiCS.contains(mapCS)){
						System.out.println("prices:CS:NONE PASS");
						res=res && true;
					}else{
						System.out.println("prices:CS:NONE FAIL");
						res=res && false;
					}
				}

				String mapWSP=columnVals.get("prices:WSP:GBP").toString().trim();
				if((mapWSP.length()>0)){
					String uiWSP=driver.findElement(By.xpath("((//div[@id='itemPriceInfo']//table//span[contains(text(),'WSP')]//ancestor::tr//td)[2]//span)[2]")).getText().trim();

					if(uiWSP.contains(mapWSP) || mapWSP.contains(uiWSP)){
						System.out.println("mapWSP"+mapWSP);

						System.out.println("uiWSP"+uiWSP);
						System.out.println("prices:WSP:GBP PASS");
						res=res && true;
					}else{
						System.out.println("mapWSP"+mapWSP);

						System.out.println("uiWSP"+uiWSP);
						System.out.println("prices:WSP:GBP FAIL");
						res=res && false;
					}
				}

				String mapRRP=columnVals.get("prices:RRP:GBP").toString().trim();
				if((mapRRP.length()>0)){
					String uiRRP=driver.findElement(By.xpath("((//div[@id='itemPriceInfo']//table//span[text()='RRP']//ancestor::tr//td)[2]//span)[2]")).getText().trim();

					if(uiRRP.contains(mapRRP) || mapRRP.contains(uiRRP)){
						System.out.println("mapRRP"+mapRRP);

						System.out.println("uiRRP"+uiRRP);
						System.out.println("prices:RRP:GBP PASS");
						res=res && true;
					}else{
						System.out.println("mapRRP"+mapRRP);

						System.out.println("uiRRP"+uiRRP);
						System.out.println("prices:RRP:GBP FAIL");
						res=res && false;
					}
				}

				String mapORDERCATEGORY=columnVals.get("lookups:ORDER-CATEGORY").toString().trim();
				if((mapORDERCATEGORY.length()>0)){
					String uiORDERCATEGORY=driver.findElement(By.xpath("(//div[@id='itemAttr']//table//span[contains(text(),'ORDER-CATEGORY')]//ancestor::tr//td)[2]//span")).getText().trim();
					WebElement ordCategory=driver.findElement(By.xpath("(//div[@id='itemAttr']//table//span[contains(text(),'ORDER-CATEGORY')]//ancestor::tr//td)[2]//span"));
					JavascriptExecutor js = (JavascriptExecutor) driver;
					js.executeScript("arguments[0].scrollIntoView(true);", ordCategory);

					if(uiORDERCATEGORY.equals(mapORDERCATEGORY)){
						System.out.println("lookups:ORDER PASS");
						res=res && true;
						utilityFileWriteOP.writeToLog(tcid, "Order category validation", "PASS",ResultPath,xwpfRun);
						logger.log(LogStatus.PASS, "Order category validation passed");
						String wholesale_sc15=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
						TakeScreenShot.saveScreenShot(wholesale_sc15, driver);
						logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc15));
					}else{
						System.out.println("lookups:ORDER FAIL");
						res=res && false;
					}
				}


				String mapSTATUS=columnVals.get("lookups:STATUS").toString().trim();
				if((mapSTATUS.length()>0)){
					String uiSTATUS=driver.findElement(By.xpath("(//div[@id='itemAttr']//table//span[contains(text(),'STATUS')]//ancestor::tr//td)[2]//span")).getText().trim();

					if(uiSTATUS.equals(mapSTATUS)){
						System.out.println("lookups:STATUS PASS");
						res=res && true;
					}else{
						System.out.println("lookups:STATUS FAIL");
						res=res && false;
					}
				}



				String mapMINSHELFLIFE=columnVals.get("lookups:MIN-SHELF-LIFE").toString().trim();
				if((mapMINSHELFLIFE.length()>0)){
					String uiMINSHELFLIFE=driver.findElement(By.xpath("(//div[@id='itemAttr']//table//span[contains(text(),'MIN-SHELF-LIFE')]//ancestor::tr//td)[2]//span")).getText().trim();

					if(uiMINSHELFLIFE.equals(mapMINSHELFLIFE)){
						System.out.println("lookups:MIN PASS");
						res=res && true;
					}else{
						System.out.println("lookups:MIN FAIL");
						res=res && false;
					}
				}

				String mapSINGLEPICK=columnVals.get("lookups:SINGLE-PICK").toString().trim();
				if((mapSINGLEPICK.length()>0)){
					String uiSINGLEPICK=driver.findElement(By.xpath("(//div[@id='itemAttr']//table//span[contains(text(),'SINGLE-PICK')]//ancestor::tr//td)[2]//span")).getText().trim();

					if(uiSINGLEPICK.equals(mapSINGLEPICK)){
						System.out.println("lookups:SINGLE PASS");
						res=res && true;
					}else{
						System.out.println("lookups:SINGLE FAIL");
						res=res && false;
					}
				}

				String mapSUBCATEGORY=columnVals.get("lookups:SUB-CATEGORY").toString().trim();
				if((mapSUBCATEGORY.length()>0)){
					String uiSUBCATEGORY=driver.findElement(By.xpath("(//div[@id='itemAttr']//table//span[contains(text(),'SUB-CATEGORY')]//ancestor::tr//td)[2]//span")).getText().trim();

					if(uiSUBCATEGORY.equals(mapSUBCATEGORY)){
						System.out.println("lookups:SUB PASS");
						res=res && true;
					}else{
						System.out.println("lookups:SUB FAIL");
						res=res && false;
					}
				}
			}

			System.out.println("After all if else result is:"+res);

			if(res){
				driver.close();
				ArrayList<String> tabs_windows1 = new ArrayList<String> (driver.getWindowHandles());
				driver.switchTo().window(tabs_windows1.get(0));
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//span[contains(text(),'Catalogue')])[4]")));

				driver.findElement(By.xpath("(//span[contains(text(),'Catalogue')])[4]")).click();
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h5[contains(text(),'Catalogue Bulk Export')]")));
				driver.findElement(By.xpath("//h5[contains(text(),'Catalogue Bulk Export')]")).click();
				WebElement selectElement1 = driver.findElement(By.xpath("//select[@name='catalogueType']"));
				Select select1 = new Select(selectElement1);
				select1.selectByValue(cataloguetype);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[contains(text(),'Initiate New Catalogue Export')]")));
				driver.findElement(By.xpath("//button[contains(text(),'Initiate New Catalogue Export')]")).click();
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@class='close-icon']")));
				driver.findElement(By.xpath("//button[@class='close-icon']")).click();

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[contains(text(),'VIEW')]")));
				driver.findElement(By.xpath("//button[contains(text(),'VIEW')]")).click();
				String ExportCatalogue=driver.findElement(By.xpath("((//table//tbody//tr)[1]//td)[2]")).getText();
				System.out.println("Filename is :"+ExportCatalogue);
				driver.findElement(By.xpath("((//table//tbody//tr)[1]//td)[3]")).click();
			}
			else{
				System.out.println("Item details not validated");
				utilityFileWriteOP.writeToLog(tcid, "Item details not validated ", "FAIL",ResultPath,xwpfRun);
				logger.log(LogStatus.FAIL, "Item details not validated");
				String wholesale_sc15=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc15, driver);
				logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc15));
			}

		}catch(Exception e){
			e.printStackTrace();
			res=false;
		}finally{
			return res;
		}
	}
	/*
	 * Method Name:newcatalogueitemvalidationamazon
	 * Functionality Description:New Catalogue Item Validation Amazon
	 * Created on:08/01/2020
	 * Created By:Morrisons
	 */

	public static boolean newcatalogueitemvalidationamazon(WebDriver driver, WebDriverWait wait, String tcid,String filename,String customer,String itemid,String cataloguetype, String ResultPath,XWPFRun xwpfRun, ExtentTest logger){
		boolean res=true;
		try{
			String[] ItemId = itemid.split(",");

			ResultPath=System.getProperty("user.dir")+"/"+ResultPath;

			driver.findElement(By.xpath("//img[@alt='"+customer+"']")).click();


			utilityFileWriteOP.writeToLog(tcid, "Required Customer", "Clicked!!!",ResultPath,xwpfRun);
			logger.log(LogStatus.PASS, "customerid is Clicked");
			String wholesale_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc3, driver);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc3));

			Thread.sleep(2000);
			driver.findElement(By.xpath("//img[@src='app/images/IconCatalogueManagement.svg']")).click();
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='csr']")));
			driver.findElement(By.xpath("//*[@id='csr']")).click();

			//*[@id="catalogueType"]/option[2]

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='search-order-border']/form/div[1]/label")));

			WebElement selectElement = driver.findElement(By.xpath("//select[@name='catalogueType']"));
			Select select = new Select(selectElement);
			select.selectByValue(cataloguetype);

			utilityFileWriteOP.writeToLog(tcid, "Catalogur Type", "Entered!!!",ResultPath,xwpfRun);
			logger.log(LogStatus.PASS, "Catalogur Type is Entered");
			String wholesale_sc4=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc4, driver);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc4));

			Thread.sleep(2000);
			driver.findElement(By.xpath("//input[@id='catalogueCode']")).sendKeys(itemid);

			utilityFileWriteOP.writeToLog(tcid, "Required ProductID", "Entered!!!",ResultPath,xwpfRun);
			logger.log(LogStatus.PASS, "Required ProductID is Entered");
			String wholesale_sc5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc5, driver);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc5));

			Thread.sleep(2000);
			driver.findElement(By.xpath("//button[@class='btn pull-right mrgn-r-2 search-button adv-search-btn form-control']")).click();

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='exportable']/table/tbody/tr[1]/td[1]")));
			driver.findElement(By.xpath("//*[@id='exportable']/table/tbody/tr[1]/td[1]")).click();
			Thread.sleep(2000);
			ArrayList<String> tabs_windows = new ArrayList<String> (driver.getWindowHandles());
			driver.switchTo().window(tabs_windows.get(1));
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h2[contains(text(),'PRODUCT DETAILS')]")));

			Actions action = new Actions(driver);
			WebElement tableElement1=driver.findElement(By.xpath("//h2[contains(text(),'Product Identifiers')]")); 
			action.moveToElement(tableElement1);
			action.perform();
			Thread.sleep(500);
			//***************************************************

			for(String eachItemid:ItemId) {

				Map<String, String> columnVals=Additional_Functions.ReadItemCatalogue.readCatalogue(System.getProperty("user.dir")+"/DownloadedFiles/"+filename,eachItemid);

				String mapPIN=columnVals.get("maps:PIN").toString().trim();
				if((mapPIN.length()>0)){
					String uiPIN=driver.findElement(By.xpath("(//div[@id='itemCodeMapping']//table//span[contains(text(),'PIN')]//ancestor::tr//td)[2]//span")).getText().trim();

					if(uiPIN.equals(mapPIN)){
						System.out.println(" PIN PASS");
						res=res && true;
					}else{
						System.out.println("PIN FAIL");
						res=res && false;
					}
				}

				String mapLPC=columnVals.get("maps:LPC").toString().trim();
				if((mapLPC.length()>0)){
					String uiLPC=driver.findElement(By.xpath("(//div[@id='itemCodeMapping']//table//span[contains(text(),'LPC')]//ancestor::tr//td)[2]//span")).getText().trim();

					if(uiLPC.equals(mapLPC)){
						System.out.println("maps:LPC PASS");
						res=res && true;
					}else{
						System.out.println("maps:LPC FAIL");
						res=res && false;
					}
				}

				String mapEAN=columnVals.get("maps:EAN").toString().trim();
				if((mapEAN.length()>0)){
					String uiEAN=driver.findElement(By.xpath("(//div[@id='itemCodeMapping']//table//span[contains(text(),'EAN')]//ancestor::tr//td)[2]//span")).getText().trim();

					if(uiEAN.equals(mapEAN)){
						System.out.println("maps:EAN PASS");
						utilityFileWriteOP.writeToLog(tcid, "Item details validation", "PASS",ResultPath,xwpfRun);
						logger.log(LogStatus.PASS, "Item details validation passed");
						String wholesale_sc15=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
						TakeScreenShot.saveScreenShot(wholesale_sc15, driver);
						logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc15));
						res=res && true;
					}else{
						System.out.println("maps:EAN FAIL");
						res=res && false;
					}
				}

				String mapCLIENTID=columnVals.get("maps:CLIENTID").toString().trim();
				if((mapCLIENTID.length()>0)){
					String uiCLIENTID=driver.findElement(By.xpath("(//div[@id='itemCodeMapping']//table//span[(text()='CLIENTID')]//ancestor::tr//td)[2]//span")).getText().trim();

					if(uiCLIENTID.contains(mapCLIENTID)){
						System.out.println("uiCLIENTID"+uiCLIENTID);
						System.out.println("mapCLIENTID"+mapCLIENTID);
						System.out.println("maps:CLIENTID PASS");
						res=res && true;
					}else{
						System.out.println("maps:CLIENTID FAIL");
						res=res && false;
					}
				}

				String mapWSCP=columnVals.get("prices:WSCP:GBP").toString().trim();
				if((mapWSCP.length()>0)){
					String uiWSCP=driver.findElement(By.xpath("(//div[@id='itemPriceInfo']//table//span[contains(text(),'WSCP')]//ancestor::tr//td)[2]//span")).getText().trim();

					if(uiWSCP.equals(mapWSCP)){
						System.out.println("prices:WSCP:GBP PASS");
						res=res && true;
					}else{
						System.out.println("prices:WSCP:GBP FAIL");
						res=res && false;
					}
				}

				String mapVAT=columnVals.get("prices:VAT:NONE").toString().trim();
				if((mapVAT.length()>0)){
					String uiVAT=driver.findElement(By.xpath("(//div[@id='itemPriceInfo']//table//span[contains(text(),'VAT')]//ancestor::tr//td)[2]//span")).getText().trim();

					if(uiVAT.contains(mapVAT)){
						System.out.println("uiVAT"+uiVAT);
						System.out.println("mapVAT"+mapVAT);
						System.out.println("prices:VAT:NONE PASS");
						res=res && true;
					}else{
						System.out.println("prices:VAT:NONE FAIL");
						res=res && false;
					}
				}

				String mapCS=columnVals.get("prices:CS:NONE").toString().trim();
				if((mapCS.length()>0)){
					String uiCS=driver.findElement(By.xpath("(//div[@id='itemPriceInfo']//table//span[contains(text(),'CS')]//ancestor::tr//td)[2]//span")).getText().trim();

					if(uiCS.equals(mapCS)){
						System.out.println("prices:CS:NONE PASS");
						res=res && true;
					}else{
						System.out.println("prices:CS:NONE FAIL");
						res=res && false;
					}
				}

				String mapWSP=columnVals.get("prices:WSP:GBP").toString().trim();
				if((mapWSP.length()>0)){
					String uiWSP=driver.findElement(By.xpath("((//div[@id='itemPriceInfo']//table//span[contains(text(),'WSP')]//ancestor::tr//td)[2]//span)[2]")).getText().trim();

					if(uiWSP.contains(mapWSP)){
						System.out.println("mapWSP"+mapWSP);
						System.out.println("uiWSP"+uiWSP);
						System.out.println("prices:WSP:GBP PASS");
						res=res && true;
					}else{
						System.out.println("prices:WSP:GBP FAIL");
						res=res && false;
					}
				}

				String mapRRP=columnVals.get("prices:RRP:GBP").toString().trim();
				if((mapRRP.length()>0)){
					String uiRRP=driver.findElement(By.xpath("((//div[@id='itemPriceInfo']//table//span[contains(text(),'RRP')]//ancestor::tr//td)[2]//span)[2]")).getText().trim();

					if(uiRRP.contains(mapRRP)){
						System.out.println("mapRRP"+mapRRP);
						System.out.println("uiRRP"+uiRRP);
						System.out.println("prices:RRP:GBP PASS");
						res=res && true;
					}else{
						System.out.println("prices:RRP:GBP FAIL");
						res=res && false;
					}
				}

				String mapORDERCATEGORY=columnVals.get("lookups:ORDER-CATEGORY").toString().trim();
				if((mapORDERCATEGORY.length()>0)){
					String uiORDERCATEGORY=driver.findElement(By.xpath("(//div[@id='itemAttr']//table//span[contains(text(),'ORDER-CATEGORY')]//ancestor::tr//td)[2]//span")).getText().trim();

					if(uiORDERCATEGORY.equals(mapORDERCATEGORY)){
						System.out.println("lookups:ORDER PASS");
						res=res && true;
						utilityFileWriteOP.writeToLog(tcid, "Order category validation", "PASS",ResultPath,xwpfRun);
						logger.log(LogStatus.PASS, "Order category validation passed");
						String wholesale_sc15=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
						TakeScreenShot.saveScreenShot(wholesale_sc15, driver);
						logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc15));
					}else{
						System.out.println("lookups:ORDER FAIL");
						res=res && false;
					}
				}


				String mapSTATUS=columnVals.get("lookups:STATUS").toString().trim();
				if((mapSTATUS.length()>0)){
					String uiSTATUS=driver.findElement(By.xpath("(//div[@id='itemAttr']//table//span[contains(text(),'STATUS')]//ancestor::tr//td)[2]//span")).getText().trim();

					if(uiSTATUS.equals(mapSTATUS)){
						System.out.println("lookups:STATUS PASS");
						res=res && true;
					}else{
						System.out.println("lookups:STATUS FAIL");
						res=res && false;
					}
				}



				String mapMINSHELFLIFE=columnVals.get("lookups:MIN-SHELF-LIFE").toString().trim();
				if((mapMINSHELFLIFE.length()>0)){
					String uiMINSHELFLIFE=driver.findElement(By.xpath("(//div[@id='itemAttr']//table//span[contains(text(),'MIN-SHELF-LIFE')]//ancestor::tr//td)[2]//span")).getText().trim();

					if(uiMINSHELFLIFE.equals(mapMINSHELFLIFE)){
						System.out.println("lookups:MIN PASS");
						res=res && true;
					}else{
						System.out.println("lookups:MIN FAIL");
						res=res && false;
					}
				}

				String mapSINGLEPICK=columnVals.get("lookups:SINGLE-PICK").toString().trim();
				if((mapSINGLEPICK.length()>0)){
					String uiSINGLEPICK=driver.findElement(By.xpath("(//div[@id='itemAttr']//table//span[contains(text(),'SINGLE-PICK')]//ancestor::tr//td)[2]//span")).getText().trim();

					if(uiSINGLEPICK.equals(mapSINGLEPICK)){
						System.out.println("lookups:SINGLE PASS");
						res=res && true;
					}else{
						System.out.println("lookups:SINGLE FAIL");
						res=res && false;
					}
				}

				String mapSUBCATEGORY=columnVals.get("lookups:SUB-CATEGORY").toString().trim();
				if((mapSUBCATEGORY.length()>0)){
					String uiSUBCATEGORY=driver.findElement(By.xpath("(//div[@id='itemAttr']//table//span[contains(text(),'SUB-CATEGORY')]//ancestor::tr//td)[2]//span")).getText().trim();

					if(uiSUBCATEGORY.equals(mapSUBCATEGORY)){
						System.out.println("lookups:SUB PASS");
						res=res && true;
					}else{
						System.out.println("lookups:SUB FAIL");
						res=res && false;
					}
				}
			}

			System.out.println("After all if else result is:"+res);

			if(res){
				driver.close();
				ArrayList<String> tabs_windows1 = new ArrayList<String> (driver.getWindowHandles());
				driver.switchTo().window(tabs_windows1.get(0));
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//span[contains(text(),'Catalogue')])[4]")));

				driver.findElement(By.xpath("(//span[contains(text(),'Catalogue')])[4]")).click();
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h5[contains(text(),'Catalogue Bulk Export')]")));
				driver.findElement(By.xpath("//h5[contains(text(),'Catalogue Bulk Export')]")).click();
				WebElement selectElement1 = driver.findElement(By.xpath("//select[@name='catalogueType']"));
				Select select1 = new Select(selectElement1);
				select1.selectByValue(cataloguetype);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[contains(text(),'Initiate New Catalogue Export')]")));
				driver.findElement(By.xpath("//button[contains(text(),'Initiate New Catalogue Export')]")).click();
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@class='close-icon']")));
				driver.findElement(By.xpath("//button[@class='close-icon']")).click();

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[contains(text(),'VIEW')]")));
				driver.findElement(By.xpath("//button[contains(text(),'VIEW')]")).click();
				String ExportCatalogue=driver.findElement(By.xpath("((//table//tbody//tr)[1]//td)[2]")).getText();
				System.out.println("Filename is :"+ExportCatalogue);
				driver.findElement(By.xpath("((//table//tbody//tr)[1]//td)[3]")).click();
			}
			else{
				System.out.println("Item details not validated");
				utilityFileWriteOP.writeToLog(tcid, "Item details not validated ", "FAIL",ResultPath,xwpfRun);
				logger.log(LogStatus.FAIL, "Item details not validated");
				String wholesale_sc15=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(wholesale_sc15, driver);
				logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc15));
			}

		}catch(Exception e){
			e.printStackTrace();
			res=false;
		}finally{
			return res;
		}
	}


	/*
	 * Method Name: compareCatalogueExportCSVFile
	 * Functionality Description:Comapre between two catalogue uploaded and exported CSV files
	 * Created on:08/01/2020
	 * Created By:Morrisons
	 */
	public static boolean compareCatalogueExportCSVFile(String customer,String itemid,String sourcefile,String targetFile,WebDriver driver, WebDriverWait wait, String ResultPath,XWPFRun xwpfRun, ExtentTest logger) throws Exception
	{
		boolean res=false;
		try{
			String[] AllItemId = itemid.split(",");

			ResultPath=System.getProperty("user.dir")+"/"+ResultPath;

			switch(customer){

			case "mccolls":
			{

				for(String eachitemID:AllItemId) {

					Map<String, String> File1Vals=Additional_Functions.ReadItemCatalogue.readCatalogue(sourcefile,eachitemID);

					Map<String, String> File2Vals=Additional_Functions.ReadItemCatalogue.readCatalogue(System.getProperty("user.dir")+"/ExportDownloadFiles/"+targetFile,eachitemID);

					System.out.println("First file values" +File1Vals);
					System.out.println("Second file values" +File2Vals);


					String catalogue=File1Vals.get("catalogue").toString().trim();
					String availability=File1Vals.get("availability").toString().trim();
					String workflow=File1Vals.get("workflow").toString().trim();
					String description=File1Vals.get("description").toString().trim();
					String MIN=File1Vals.get("maps:MIN").toString().trim();
					String PIN=File1Vals.get("maps:PIN").toString().trim();
					String LPC=File1Vals.get("maps:LPC").toString().trim();
					String CLIENTID=File1Vals.get("maps:CLIENTID").toString().trim();
					String EAN=File1Vals.get("maps:EAN").toString().trim();
					String EAN2=File1Vals.get("maps:EAN2").toString().trim();
					String LeadSKU=File1Vals.get("lookups:LeadSKU").toString().trim();
					String WSCP=File1Vals.get("prices:WSCP:GBP").toString().trim();
					String VAT=File1Vals.get("prices:VAT:NONE").toString().trim();
					String CS=File1Vals.get("prices:CS:NONE").toString().trim();
					String WSP=File1Vals.get("prices:WSP:GBP").toString().trim();
					String RRP=File1Vals.get("prices:RRP:GBP").toString().trim();
					String STATUS=File1Vals.get("lookups:STATUS").toString().trim();
					String ORDERCATEGORY=File1Vals.get("lookups:ORDER-CATEGORY").toString().trim();
					String MINSHIPLIFE=File1Vals.get("lookups:MIN-SHIP-LIFE").toString().trim();
					String MINSHELFLIFE=File1Vals.get("lookups:MIN-SHELF-LIFE").toString().trim();
					String SINGLEPICK=File1Vals.get("lookups:SINGLE-PICK").toString().trim();
					String StockedinCPC=File1Vals.get("lookups:StockedinCPC").toString().trim();
					String MANUFACTURER=File1Vals.get("lookups:MANUFACTURER").toString().trim();
					String DEPARTMENT=File1Vals.get("lookups:DEPARTMENT").toString().trim();
					String SUBCATEGORY=File1Vals.get("lookups:SUB-CATEGORY").toString().trim();
					String DIMENSIONSLENGTH=File1Vals.get("lookups:DIMENSIONS-LENGTH").toString().trim();
					String DIMENSIONSHEIGHT=File1Vals.get("lookups:DIMENSIONS-HEIGHT").toString().trim();
					String DIMENSIONSWIDTH=File1Vals.get("lookups:DIMENSIONS-WIDTH").toString().trim();
					String DIMENSIONSWEIGHT=File1Vals.get("lookups:DIMENSIONS-WEIGHT").toString().trim();
					String PriceMarkGBP=File1Vals.get("prices:PriceMark:GBP").toString().trim();
					String PINCATCHWEIGHTIND=File1Vals.get("lookups:PIN-CATCH-WEIGHT-IND").toString().trim();
					String CATCHWEIGHTTYPE=File1Vals.get("lookups:CATCH-WEIGHT-TYPE").toString().trim();
					String MINSELLINGUOM=File1Vals.get("lookups:MIN-SELLING-UOM").toString().trim();
					String COST_PRICEGBP=File1Vals.get("prices:COST_PRICE:GBP").toString().trim();



					String catalogue1=File1Vals.get("catalogue").toString().trim();
					String availability1=File1Vals.get("availability").toString().trim();
					String workflow1=File1Vals.get("workflow").toString().trim();
					String description1=File1Vals.get("description").toString().trim();
					String MIN1=File1Vals.get("maps:MIN").toString().trim();
					String PIN1=File1Vals.get("maps:PIN").toString().trim();
					String LPC1=File1Vals.get("maps:LPC").toString().trim();
					String CLIENTID1=File1Vals.get("maps:CLIENTID").toString().trim();
					String EAN1=File1Vals.get("maps:EAN").toString().trim();
					String EAN21=File1Vals.get("maps:EAN2").toString().trim();
					String LeadSKU1=File1Vals.get("lookups:LeadSKU").toString().trim();
					String WSCP1=File1Vals.get("prices:WSCP:GBP").toString().trim();
					String VAT1=File1Vals.get("prices:VAT:NONE").toString().trim();
					String CS1=File1Vals.get("prices:CS:NONE").toString().trim();
					String WSP1=File1Vals.get("prices:WSP:GBP").toString().trim();
					String RRP1=File1Vals.get("prices:RRP:GBP").toString().trim();
					String STATUS1=File1Vals.get("lookups:STATUS").toString().trim();
					String ORDERCATEGORY1=File1Vals.get("lookups:ORDER-CATEGORY").toString().trim();
					String MINSHIPLIFE1=File1Vals.get("lookups:MIN-SHIP-LIFE").toString().trim();
					String MINSHELFLIFE1=File1Vals.get("lookups:MIN-SHELF-LIFE").toString().trim();
					String SINGLEPICK1=File1Vals.get("lookups:SINGLE-PICK").toString().trim();
					String StockedinCPC1=File1Vals.get("lookups:StockedinCPC").toString().trim();
					String MANUFACTURER1=File1Vals.get("lookups:MANUFACTURER").toString().trim();
					String DEPARTMENT1=File1Vals.get("lookups:DEPARTMENT").toString().trim();
					String SUBCATEGORY1=File1Vals.get("lookups:SUB-CATEGORY").toString().trim();
					String DIMENSIONSLENGTH1=File1Vals.get("lookups:DIMENSIONS-LENGTH").toString().trim();
					String DIMENSIONSHEIGHT1=File1Vals.get("lookups:DIMENSIONS-HEIGHT").toString().trim();
					String DIMENSIONSWIDTH1=File1Vals.get("lookups:DIMENSIONS-WIDTH").toString().trim();
					String DIMENSIONSWEIGHT1=File1Vals.get("lookups:DIMENSIONS-WEIGHT").toString().trim();
					String PriceMarkGBP1=File1Vals.get("prices:PriceMark:GBP").toString().trim();
					String PINCATCHWEIGHTIND1=File1Vals.get("lookups:PIN-CATCH-WEIGHT-IND").toString().trim();
					String CATCHWEIGHTTYPE1=File1Vals.get("lookups:CATCH-WEIGHT-TYPE").toString().trim();
					String MINSELLINGUOM1=File1Vals.get("lookups:MIN-SELLING-UOM").toString().trim();
					String COST_PRICEGBP1=File1Vals.get("prices:COST_PRICE:GBP").toString().trim();


					if(MIN.equals(MIN1)
							&& catalogue.equalsIgnoreCase(catalogue1)
							&& availability.equalsIgnoreCase(availability1)
							&& workflow.equalsIgnoreCase(workflow1)
							&& description.equalsIgnoreCase(description1)
							&& MIN.equalsIgnoreCase(MIN1)
							&& PIN.equalsIgnoreCase(PIN1)
							&& LPC.equalsIgnoreCase(LPC1)
							&& CLIENTID.equalsIgnoreCase(CLIENTID1)
							&& EAN.equalsIgnoreCase(EAN1)
							&& EAN2.equalsIgnoreCase(EAN21)
							&& LeadSKU.equalsIgnoreCase(LeadSKU1)
							&& WSCP.equalsIgnoreCase(WSCP1)
							&& VAT.equalsIgnoreCase(VAT1)
							&& CS.equalsIgnoreCase(CS1)
							&& WSP.equalsIgnoreCase(WSP1)
							&& RRP.equalsIgnoreCase(RRP1)
							&& STATUS.equalsIgnoreCase(STATUS1)
							&& ORDERCATEGORY.equalsIgnoreCase(ORDERCATEGORY1)
							&& MINSHIPLIFE.equalsIgnoreCase(MINSHIPLIFE1)
							&& MINSHELFLIFE.equalsIgnoreCase(MINSHELFLIFE1)
							&& SINGLEPICK.equalsIgnoreCase(SINGLEPICK1)
							&& StockedinCPC.equalsIgnoreCase(StockedinCPC1)
							&& MANUFACTURER.equalsIgnoreCase(MANUFACTURER1)
							&& DEPARTMENT.equalsIgnoreCase(DEPARTMENT1)
							&& SUBCATEGORY.equalsIgnoreCase(SUBCATEGORY1)
							&& DIMENSIONSLENGTH.equalsIgnoreCase(DIMENSIONSLENGTH1)
							&& DIMENSIONSHEIGHT.equalsIgnoreCase(DIMENSIONSHEIGHT1)
							&& DIMENSIONSWIDTH.equalsIgnoreCase(DIMENSIONSWIDTH1)
							&& DIMENSIONSWEIGHT.equalsIgnoreCase(DIMENSIONSWEIGHT1)
							&& PriceMarkGBP.equalsIgnoreCase(PriceMarkGBP1)
							&& PINCATCHWEIGHTIND.equalsIgnoreCase(PINCATCHWEIGHTIND1)
							&& CATCHWEIGHTTYPE.equalsIgnoreCase(CATCHWEIGHTTYPE1)
							&& MINSELLINGUOM.equalsIgnoreCase(MINSELLINGUOM1)
							&& COST_PRICEGBP.equalsIgnoreCase(COST_PRICEGBP1))





					{
						System.out.println("PASS");
						res=true;
						System.out.println("Inside storeid if");
						logger.log(LogStatus.PASS, "Both Uploaded and Exported catalougue file has been validated successfully");
						res=true;

					}else{
						System.out.println("FAIL");
						System.out.println("Inside storeid else");
						logger.log(LogStatus.FAIL, "Both Uploaded and Exported catalougue file validation failed");

						res=false;
					}

				}
			}
			break;


			}
		}

		catch(Exception e){
			e.printStackTrace();

		}

		finally{
			return res;
		}
	}

	/*
	 * Method Name: newcatalogueitemvalidationcust
	 * Functionality Description:Catlalogue Item validation with portal and successfully uploaded CSV file
	 * Created on:08/01/2020
	 * Created By:Morrisons
	 */
	public static boolean catalogueExportOtherCustomer(WebDriver driver, WebDriverWait wait, String tcid,String customer,String catalogueType, String ResultPath,XWPFRun xwpfRun, ExtentTest logger){
		boolean res=true;
		try{
			

			ResultPath=System.getProperty("user.dir")+"/"+ResultPath;

			driver.findElement(By.xpath("//img[@alt='"+customer+"']")).click();


			utilityFileWriteOP.writeToLog(tcid, "Required Customer", "Clicked!!!",ResultPath,xwpfRun);
			logger.log(LogStatus.PASS, "customerid is Clicked");
			String wholesale_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc3, driver);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc3));

			Thread.sleep(2000);
			driver.findElement(By.xpath("//img[@src='app/images/IconCatalogueManagement.svg']")).click();
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h5[contains(text(),'Catalogue Bulk Export')]")));
			driver.findElement(By.xpath("//h5[contains(text(),'Catalogue Bulk Export')]")).click();

			//*[@id="catalogueType"]/option[2]

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='search-order-border']/form/div[1]/label")));
		
			WebElement selectElement = driver.findElement(By.xpath("//select[@name='catalogueType']"));
			Select select = new Select(selectElement);
//			select.selectByIndex(1);
			select.selectByVisibleText(catalogueType);

			utilityFileWriteOP.writeToLog(tcid, "Catalogue Type", "Entered!!!",ResultPath,xwpfRun);
			logger.log(LogStatus.PASS, "Catalogue Type is Entered");
			String wholesale_sc4=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_sc4, driver);
			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc4));

			Thread.sleep(2000);
		
			
			driver.findElement(By.xpath("//button[contains(text(),'Initiate New Catalogue Export')]")).click();
			utilityFileWriteOP.writeToLog(tcid, "Click Initiate New catalogue Export button", "Clicked Initiate New catalogue Export button",ResultPath,xwpfRun);
			String wholesale_1=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(wholesale_1, driver);
			logger.log(LogStatus.PASS,"Clicked Initiate New catalogue Export button"+ logger.addScreenCapture(wholesale_1));
			
			if(driver.findElement(By.xpath("//*[contains(text(),'Catalogue Process')]")).isDisplayed())
			{
				
				String exportfilename=driver.findElement(By.xpath("(//div[@class='new-batch-num-popup'])[3]//span")).getText();
				String[] exportfile=exportfilename.split("-");
  				
  				System.out.println("First 1"+exportfile[0]);
  				System.out.println("First 2"+exportfile[1]);
  				System.out.println("First 3"+exportfile[2]);
  				
  				utilityFileWriteOP.writeToLog(tcid, "Catalogue file export", "Catalogue File exported is : "+exportfile[2],ResultPath,xwpfRun);
  				String wholesale_2=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
  				TakeScreenShot.saveScreenShot(wholesale_2, driver);
  				logger.log(LogStatus.INFO,"Catalogue file export", "Catalogue File exported is : "+exportfile[2]+ logger.addScreenCapture(wholesale_2));
  				
  				driver.findElement(By.xpath("(//button[@id='close-popup'])[2]")).click();
  				utilityFileWriteOP.writeToLog(tcid, "Click (X) button", "Close (X) button is clicked",ResultPath,xwpfRun);
  				String wholesale_3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
  				TakeScreenShot.saveScreenShot(wholesale_3, driver);
  				logger.log(LogStatus.PASS,"Click (X) button", "Close (X) button is clicked"+ logger.addScreenCapture(wholesale_3));
  				
  				Thread.sleep(4000);
  				driver.findElement(By.xpath("//button[contains(text(),'VIEW')]")).click();
  				utilityFileWriteOP.writeToLog(tcid, "Click VIEW button", "Close VIEW button is clicked",ResultPath,xwpfRun);
  				String wholesale_4=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
  				TakeScreenShot.saveScreenShot(wholesale_4, driver);
  				logger.log(LogStatus.PASS,"Click VIEW button", "Close VIEW button is clicked"+ logger.addScreenCapture(wholesale_4));
  				
  				for(int i=1;i<10;i++){
		        	
		        	try{
		        		
		        		driver.navigate().refresh();
		        		WebElement selectElement1 = driver.findElement(By.xpath("//select[@name='catalogueType']"));
		    			Select select1 = new Select(selectElement1);

		    			select1.selectByVisibleText(catalogueType);
		        		driver.findElement(By.xpath("//button[contains(text(),'VIEW')]")).click();
		        		
		        	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@id='exportable']//tr["+i+"]//td)[2]"))).getText().contains(exportfile[2]);
		        	
		        	  String prm=driver.findElement(By.xpath("(//div[@id='exportable']//tr["+i+"]//td)[2]")).getText();
		        	
		        	  System.out.println("Counter is:"+i);
		        	  System.out.println("String get is :"+prm);
		        	  System.out.println("String to be find is "+exportfile[2]);
		        
		        	  if(prm.contains(exportfile[2])){
		        		  System.out.println("in If condition");
		        	
		        		  driver.findElement(By.xpath("(//div[@id='exportable']//tr//td[3])[1]")).click();
							Thread.sleep(2000);
							logger.log(LogStatus.PASS, "Catalogue export file downloaded successfully");
		      				String wholesale_sc19=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		      				TakeScreenShot.saveScreenShot(wholesale_sc19, driver);
		      				utilityFileWriteOP.writeToLog(tcid, "Catalogue export file downloaded successfully", "PASS",ResultPath,xwpfRun);
		      				logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc19));
		      				res=true;
		      				break;
		        	}}
		        	
		        	 catch(Exception e){
		        		  System.out.println("Expected file is not displayed or present in iteration "+i);
		        		  driver.navigate().refresh();

		      			WebElement selectCatalogue = driver.findElement(By.xpath("//select[@name='catalogueType']"));
		      			Select select1 = new Select(selectCatalogue);
		      			select1.selectByIndex(1);
		    		        Thread.sleep(2000);
		      				driver.findElement(By.xpath("//button[contains(text(),'VIEW')]")).click();
		    		        Thread.sleep(2000);
		        	  }
		        }
			}

		}catch(Exception e){
			e.printStackTrace();
			res=false;
		}finally{
			return res;
		}
	}
	
	
	
	


}