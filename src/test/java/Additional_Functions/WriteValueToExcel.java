package Additional_Functions;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import Utilities.excelCellValueWrite;

import com.opencsv.CSVIterator;
import com.opencsv.CSVParser;
import com.opencsv.CSVReader;
import com.relevantcodes.extentreports.ExtentTest;

public class WriteValueToExcel {
	
public static boolean writeOrderIDKeyword(String Keyword,String TestKeyword_1,String orderID,String TestDataPath, String SheetName,Map<String, Integer> map) throws Exception{
	
	boolean res=false;
	
	try{
		int rows=0;
		Workbook wrk1 = Workbook.getWorkbook(new File(TestDataPath));
		Sheet sheet1 = wrk1.getSheet(SheetName);
		rows = sheet1.getRows();
		Cell[] FirstRow = sheet1.getRow(0);
		int r1 = 0;
	

		int occurances = 0;
		
		for(int i=0; i < FirstRow.length; i++)
		{
			map.put(FirstRow[i].getContents().trim(), i);
		}

		for(r1=1; r1<rows; r1++) {
		Keyword = sheet1.getCell(map.get("Keyword"), r1).getContents().trim();



		if(occurances>0){
		   	break;
		}

		if(Keyword.equalsIgnoreCase(TestKeyword_1)) {

		occurances=occurances+1;
		Keyword = sheet1.getCell(map.get("Keyword"), r1).getContents().trim();
		break;

		}
		}
		
		System.out.println("Value of R2:"+r1);
	
	
	excelCellValueWrite.writeValueToCell(orderID, r1, map.get("orderid"), TestDataPath, SheetName);
	res=true;		
		
	}
	catch(Exception e){
		e.printStackTrace();
		res=false;
		
	}
	finally{
		return res;
	}
}
	
	




	public static String lastStoreFile(String directorypath,String customer) {
		File dir = new File(directorypath);
		File[] files = dir.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.contains(customer);
				
			}
		});
		long lastMod = Long.MIN_VALUE;
		String choice = null;
		for (File file : files) {
			if (file.lastModified() > lastMod) {
				choice = file.getName();
				lastMod = file.lastModified();
			}
		}
		return choice;
	}
	
	
	
}
