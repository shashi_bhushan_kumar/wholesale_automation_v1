package Additional_Functions;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import Utilities.excelCellValueWrite;

import com.opencsv.CSVIterator;
import com.opencsv.CSVParser;
import com.opencsv.CSVReader;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class compareCSVFunction {

	/*
	 * Method Name: compareCatalogueExportCSVFile
	 * Functionality Description:Compare Exported Catalogue CSV file
	 * Created on:08/01/2020
	 * Created By:Morrisons
	 */

	public static boolean compareCatalogueExportCSVFile(String itemid,String sourcefile,String targetFile,WebDriver driver, WebDriverWait wait, String ResultPath,XWPFRun xwpfRun, ExtentTest logger) throws Exception
	{
		boolean res=false;
		try{
			
			System.out.println("sourcefile:"+sourcefile);
			System.out.println("itemid:"+itemid);
			String[] ItemId = itemid.split(",");

			ResultPath=System.getProperty("user.dir")+"/"+ResultPath;


			for(String eachItemid:ItemId) {
				
				Map<String, String> File1Vals=Additional_Functions.ReadItemCatalogue.readExportCatalogue(sourcefile,eachItemid);

				Map<String, String> File2Vals=Additional_Functions.ReadItemCatalogue.readExportCatalogue(System.getProperty("user.dir")+"/ExportDownloadFiles/"+targetFile,eachItemid);


				System.out.println("First file values" +File1Vals);
				System.out.println("Second file values" +File2Vals);
				
				
				String PIN=File1Vals.get("maps:PIN").toString().trim();
				String LPC=File1Vals.get("maps:LPC").toString().trim();
				String MIN=File1Vals.get("maps:MIN").toString().trim();
				String EAN=File1Vals.get("maps:EAN").toString().trim();
				String clientID=File1Vals.get("maps:CLIENTID").toString().trim();
				String WSCP=File1Vals.get("prices:WSCP:GBP").toString().trim();
				String VAT=File1Vals.get("prices:VAT:NONE").toString().trim();
				String CS=File1Vals.get("prices:CS:NONE").toString().trim();
				String WSP=File1Vals.get("prices:WSP:GBP").toString().trim();
				String RRP=File1Vals.get("prices:RRP:GBP").toString().trim();
				String orderCatg=File1Vals.get("lookups:ORDER-CATEGORY").toString().trim();
				String status=File1Vals.get("lookups:STATUS").toString().trim();
				String MINslife=File1Vals.get("lookups:MIN-SHELF-LIFE").toString().trim();
				String singlePick=File1Vals.get("lookups:SINGLE-PICK").toString().trim();
				String subCategory=File1Vals.get("lookups:SUB-CATEGORY").toString().trim();
				

				String PIN1=File2Vals.get("maps:PIN").toString().trim();
				String LPC1=File2Vals.get("maps:LPC").toString().trim();
				String MIN1=File2Vals.get("maps:MIN").toString().trim();
				String EAN1=File2Vals.get("maps:EAN").toString().trim();
				String clientID1=File2Vals.get("maps:CLIENTID").toString().trim();
				String WSCP1=File2Vals.get("prices:WSCP:GBP").toString().trim();
				String VAT1=File2Vals.get("prices:VAT:NONE").toString().trim();
				String CS1=File2Vals.get("prices:CS:NONE").toString().trim();
				String WSP1=File2Vals.get("prices:WSP:GBP").toString().trim();
				String RRP1=File2Vals.get("prices:RRP:GBP").toString().trim();
				String orderCatg1=File2Vals.get("lookups:ORDER-CATEGORY").toString().trim();
				String status1=File2Vals.get("lookups:STATUS").toString().trim();
				String MINslife1=File2Vals.get("lookups:MIN-SHELF-LIFE").toString().trim();
				String singlePick1=File2Vals.get("lookups:SINGLE-PICK").toString().trim();
				String subCategory1=File2Vals.get("lookups:SUB-CATEGORY").toString().trim();

				
				if(PIN.equals(PIN1)
						&& LPC.equalsIgnoreCase(LPC1)
						&& MIN.equalsIgnoreCase(MIN1)
						&& status.equalsIgnoreCase(status1)
						&& EAN.equalsIgnoreCase(EAN1)
						&& clientID.equalsIgnoreCase(clientID1)
						&& WSCP.equalsIgnoreCase(WSCP1)
						&& VAT.equalsIgnoreCase(VAT1)
						&& CS.equalsIgnoreCase(CS1)
						&& RRP.equalsIgnoreCase(RRP1)
						&& orderCatg.equalsIgnoreCase(orderCatg1)
						&& status.equalsIgnoreCase(status1)
						&& MINslife.equalsIgnoreCase(MINslife1)
						&& singlePick.equalsIgnoreCase(singlePick1)
						&& subCategory.equalsIgnoreCase(subCategory1))
				{
					System.out.println("PASS");
					res=true;
					System.out.println("Inside storeid if");
					logger.log(LogStatus.PASS, "Both Uploaded and Exported Store file has been validated successfully");
				}else{
					System.out.println("FAIL");
					System.out.println("Inside storeid else");
					logger.log(LogStatus.FAIL, "Both Uploaded and Exported Store file validation failed");

					res=false;
				}


			}
		}

		catch(Exception e){
			e.printStackTrace();

		}

		finally{
			return res;
		}
	}

	
	
	
	
}
