package OrderServices_Functions;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.poi.xwpf.usermodel.XWPFRun;

import com.relevantcodes.extentreports.ExtentTest;

import Utilities.FolderZipper;
import Utilities.utilityFileWriteOP;

public class Consolidation {

	
	public static HashMap<String, String> LoadItemsNAS(String  fin,HashMap<String, String> hm) {
		
		// this function will return hashmap of itemno as key and consolidated qty
		
		BufferedReader br =null;
		
		try{
		
		File file = new File(fin);
		
		
		FileInputStream fis = new FileInputStream(fin);
	 
		
		
		//Construct BufferedReader from InputStreamReader
		br = new BufferedReader(new InputStreamReader(fis));
	 
		String line = null;
		
		
		
	/*	
		int Qty=Integer.parseInt(Exp_quantityOrderedJson);

		System.out.println(" Current item  qty is "+Qty);


		int CaseSize=Integer.parseInt(Exp_itemCaseSize);

		System.out.println("Current item  case size is "+CaseSize);



		int Qt= ((Qty +CaseSize - 1) / CaseSize);

		
		*/
		
		
		
		
		
while ((line = br.readLine()) != null) {
			
			
	System.out.println(line);
			
    String []Consolidation=	line.split(",");
			
			

			
	int  oldQty=0;
	
	String CaseSize=Consolidation[3];
	
	String Type=Consolidation[2];
			
	if(hm.get(Consolidation[0])!=null){
			
			
	oldQty=Integer.parseInt(hm.get(Consolidation[0]));// retrieve old value
			
			
	    }
	
	
	int newQty=Integer.parseInt(Consolidation[1]);
	

	 
	int FinalQty=oldQty+newQty;
	
/*	
	if(Consolidation[2].contentEquals("EA")){
		
		
	  String CaseSize=Consolidation[3];	
		
		
	  int Qty=FinalQty;
	  
	  int CaseSizeItem=Integer.parseInt(CaseSize);
	  
	  
	  int Qt= ((Qty +CaseSizeItem - 1) / CaseSizeItem);
		
	  FinalQty=Qt;
	  
		
	}
	
	
	*/
	
	
	String Final_qty=String.valueOf(FinalQty);
	
	//String testStr=Final_qty+","+CaseSize+","+Type;
	
	
	hm.put(Consolidation[0], Final_qty);
		

	  }  
		
		
	}
		
	catch(Exception e){    
			
			
		System.out.println(e);
			
			
	}
		
		finally{
		
			
			try {
				br.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return hm;	
			
			
		}
	
	}
	

	
	
public static String  getCaseSize_And_ItemType(String  fin,String Item) {
		
		BufferedReader br =null;
		
		String Qty_Casse=null;
		
		try{
		
		File file = new File(fin);
		
		
		FileInputStream fis = new FileInputStream(fin);
	 
		
		
		//Construct BufferedReader from InputStreamReader
		br = new BufferedReader(new InputStreamReader(fis));
	 
		String line = null;
		

		
		
		
		while ((line = br.readLine()) != null) {
			
			
	System.out.println(line);
			
    String []Consolidation=	line.split(",");
    
    String itemNo=Consolidation[0];
    
    
    String CaseSize=null;
    
    String QtyType=null;
    
    
    
  if(Item.contentEquals(itemNo)){
			
			
	  CaseSize=Consolidation[2];
    
	  QtyType=Consolidation[3];
	  
	  
	  Qty_Casse=CaseSize+"#"+QtyType;
	  
	  
      break;
	  
	  
    }
    
    
    
    
    
    
    
    
	
		
	}  }
		
	catch(Exception e){    
			
			
		System.out.println(e);
			
			
	}
		
		finally{
			
			try {
				br.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		return Qty_Casse;	
			
			
		}
	

		}

	
	
	
	
	
public static void main(String[] args) throws IOException {
		
	
	  HashMap<String, String> hm = null;

					
		hm = new HashMap<String, String>();
		
		/*hm.put("Exp_customerId", Exp_customerId);
		hm.put("Exp_OrderId", Exp_OrderId);
		hm.put("Exp_MessageType", Exp_MessageType);
		hm.put("Exp_ShipToLocationId", Exp_ShipToLocationId);
		hm.put("Exp_ShipToDeliverAt", Exp_ShipToDeliverAt);
		hm.put("Exp_OrdRefCode", Exp_OrdRefCode);
		hm.put("Exp_quantityOrdered", itemListQty[k]);
		hm.put("Exp_itemId", itemList[k]);*/
		
	Consolidation.LoadItemsNAS("TempResultFile/ConsolidationLog.csv",hm);
		
		
     System.out.println(hm);

	}



public static HashMap<String, String> LoadTempItemsNAS(String  fin,HashMap<String, String> hm) {
	
	// this function will return hashmap of itemno as key and consolidated qty
	
	BufferedReader br =null;
	
	try{
	
	File file = new File(fin);
	
	
	FileInputStream fis = new FileInputStream(fin);
 
	
	
	//Construct BufferedReader from InputStreamReader
	br = new BufferedReader(new InputStreamReader(fis));
 
	String line = null;
	
	
	
/*	
	int Qty=Integer.parseInt(Exp_quantityOrderedJson);

	System.out.println(" Current item  qty is "+Qty);


	int CaseSize=Integer.parseInt(Exp_itemCaseSize);

	System.out.println("Current item  case size is "+CaseSize);



	int Qt= ((Qty +CaseSize - 1) / CaseSize);

	
	*/
	
	
	
	
	
while ((line = br.readLine()) != null) {
		
		
System.out.println(line);
		
String []Consolidation=	line.split(",");
		
		

		
int  oldQty=0;

String CaseSize=Consolidation[3];

String Type=Consolidation[2];
		
if(hm.get(Consolidation[0])!=null){
		
		
oldQty=Integer.parseInt(hm.get(Consolidation[0]));// retrieve old value
		
		
    }


int newQty=Integer.parseInt(Consolidation[1]);


 
int FinalQty=oldQty+newQty;

/*	
if(Consolidation[2].contentEquals("EA")){
	
	
  String CaseSize=Consolidation[3];	
	
	
  int Qty=FinalQty;
  
  int CaseSizeItem=Integer.parseInt(CaseSize);
  
  
  int Qt= ((Qty +CaseSizeItem - 1) / CaseSizeItem);
	
  FinalQty=Qt;
  
	
}


*/


String Final_qty=String.valueOf(FinalQty);

//String testStr=Final_qty+","+CaseSize+","+Type;


hm.put(Consolidation[0], Final_qty);
	

  }  
	
	
}
	
catch(Exception e){    
		
		
	System.out.println(e);
		
		
}
	
	finally{
	
		
		try {
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	return hm;	
		
		
	}

}



public static ArrayList<HashMap<String, String>> S3Content_ConsolidationCSV(String filename,String ShiptoDeliverDt,String LocalPath,
		
			String OrderRefCode,String shipToLocationId, List<String> l,String tcid,String ItemTypeList,String TotalItemList,String Resultpath ,XWPFRun xwpfrun, ExtentTest logger
             
		)     throws IOException {

	int Exp_itemCount=l.size();
	
	
	String DeliveryDt=ShiptoDeliverDt.replaceAll("-","");
	
	
	String [] ItemsAll_Type=ItemTypeList.split(",");
	
	String [] ItemsAll=TotalItemList.split(",");

	String[] ItemsListValid = new String[50];
	
	
	for(int k=0;k<ItemsAll_Type.length;k++){
	
	
	if(ItemsAll_Type[k].contentEquals("P")){
		
		ItemsListValid[k]="P";
			
	       }
	}
	
	
	int NoOfRecordsInNAS=0;
	
	int NoOfValidItems=ItemsAll_Type.length;

	
//String matchpattern="mccolls-"+OrderRefCode+"-"+shipToLocationId+"-"+"20190825"+"-"+"*"+".csv";
	
	String matchpattern="mccolls-"+OrderRefCode+"-"+shipToLocationId+"-"+DeliveryDt+"-"+"*"+".csv";	
	
	System.out.println("Ship to location ID is "+shipToLocationId);

	System.out.println("Match Pattern :"+matchpattern);
	
	BufferedReader br = null;

	ArrayList<HashMap<String, String>> ListofMaps = new ArrayList<HashMap<String,String>>();
	HashMap<String, String> hm = null;
		
	ArrayList<String> HeaderList = null;

		
		try {
			
			//Read from local

			String CurrentLine = null;
			int linecount = 0;
			String[] strValuearr = null;

	HeaderList = new ArrayList<String>();
	br = new BufferedReader(new FileReader(LocalPath + filename));
	
	linecount=0;
			
	while((CurrentLine = br.readLine()) != null) {

		
			if(linecount==0){
				
	
		     }

			
if(linecount > 0) {
	
	
	
	File theDir = new File("Results/MatchedConsolidatedFiles");

	// if the directory does not exist, create it
if (!theDir.exists()) {
	    System.out.println("creating directory: " + theDir.getName());
	    
	    
	    boolean result = false;

	    try{
	        theDir.mkdir();
	        result = true;
	    } 
	    catch(SecurityException se){
	        //handle it
	    }        
	    if(result) {    
	       
	    System.out.println("MatchedConsolidatedFiles  DIR created");  
	   
	    }
	}	
	
	
					
	
//sftpchannel.get(filename, "Results/MatchedConsolidatedFiles/"+filename);

FolderZipper.copyfile(LocalPath + filename, "Results/MatchedConsolidatedFiles/"+filename);


hm = new HashMap<String, String>();

				strValuearr= CurrentLine.split(",");
		/*
				hm.put("customerId", strValuearr[0]);
				hm.put("orderId", strValuearr[1]);
				hm.put("itemId", strValuearr[21]);
		*/		
				
		    	hm.put("customerId", strValuearr[0]);

		    	hm.put("orderId", strValuearr[1]);

		    	hm.put("orderReferenceCode", strValuearr[4]);

		    	hm.put("messageId", strValuearr[8]);

		    	hm.put("shipToLocationId", strValuearr[11]);

		    	hm.put("itemId", strValuearr[21]);

		    	hm.put("quantityOrdered", strValuearr[27]);
		    	
		    	hm.put("quantityType", strValuearr[26]);
		    	
		    	hm.put("itemDescription", strValuearr[25]);
		    	
		    	
		    	hm.put("itemAlternateId", strValuearr[24]);
		    	
		    	hm.put("shipToDeliverAt", strValuearr[19]);
		    	
		    	hm.put("createdAt", strValuearr[28]);
		    	
		    	
		    	
		    	ListofMaps.add(hm);
	
	            
	            NoOfRecordsInNAS=NoOfRecordsInNAS+1;
	            
	

	  }

	  
linecount++;
		
	
}
	
	
	if((linecount-1)>Exp_itemCount){
	
	
		  utilityFileWriteOP.writeToLog(tcid, "Consolidated file in S3 contains more Item then expected ","");
		
	
	}


	br.close();		
	
	
		


	} 
		
		
catch (Exception e) {
			// TODO: handle exception
	e.printStackTrace();
	System.out.println(e);
	utilityFileWriteOP.writeToLog(tcid, "Error occurred during Temporary Order ID Extraction ", "Due to :"+e);

}

finally{
			
//br.close();		


return ListofMaps;
	
     }
		

	}








}
