package OrderServices_Functions
;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Vector;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;




import Utilities.*;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import jxl.Sheet;
import jxl.Workbook;

public class Consolidation_OrderServices {
	
public static boolean OrderIdGenerationI5aConsolidation(String TestDataPath, String SheetName, String tcid, String Resultpath) throws IOException {
		
		boolean res = false;

		
		int r = 0;
		
		
		
	    try {
	    	
	    	int rows = 0;
			
	    	Workbook wrk1 = Workbook.getWorkbook(new File(TestDataPath));
	    	
			Sheet sheet1 = wrk1.getSheet(SheetName); 
	         
			rows = sheet1.getRows();
			
			//long currentDateSec = (System.currentTimeMillis() / 1000);
			
			
			for(r=1; r<rows; r++) {
	
				
				long currentDateSec = (System.currentTimeMillis() / 1000);
				 excelCellValueWrite.writeValueToCell(String.valueOf(currentDateSec), r, 0, TestDataPath, SheetName);

				 Thread.sleep(1200);
				
			}
			
			wrk1.close();
			
			res = true;
	        	  
	       
		} catch (Exception e) {
			// TODO: handle exception
			
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during Order ID generation ", "Due to :"+e,Resultpath);
	        
			res = false;
			return res;
		}
	    
	    finally{
	        
	    	
	        
	}
	    
	    return res;
	    
		
	}



public static String GetCallItemDetailsFromWebPortal(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String ItemId, String itemBaseType, String quantityType, String itemLineId, String quantityOrdered, String tcid, String Resultpath) throws IOException {
	
	String res = null;
	
	int MaxwaitingTime = 60000;
	
	int regularWaitingTime = 10000;
	
	HashMap<String, String> mapofMaps = new HashMap<String, String>();
	HashMap<String, String> mapofPrices = new HashMap<String, String>();

	String priceOrderedCurrency = null;
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        //https://sit-api.morrisons.com/wholesaleorder/v1/customers/mccolls/catalogues/main/items/101743031?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36&filtertype=maptype&filtervalue=PIN
        
        HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/mccolls/catalogues/main/items/" + ItemId + "?" + ApiKey+"&filtertype=maptype&filtervalue=PIN");
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Item ID is " + ItemId);
        utilityFileWriteOP.writeToLog(tcid, "Searching the attributes for ", "Item ID "+ItemId,Resultpath);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",Resultpath);
        
     /*   for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
        	
        	Thread.sleep(regularWaitingTime);*/
        
        response = httpclient.execute(target, httpget);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),Resultpath);
        
        String jsonresponse = EntityUtils.toString(response.getEntity());
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,Resultpath);
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString(),Resultpath);
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        System.out.println(jarray.size());
        for(int i=0; i<jarray.size(); i++) {
        	
        	jsonobject = jarray.get(i).getAsJsonObject();
        	
        	String description = jsonobject.get("description").toString().replaceAll("^\"|\"$", "");
        	
        	//String mapsJsonBody = jsonobject.get("maps").toString().replaceAll("^\"|\"$", "");
	        JsonArray jarrayMaps = jsonobject.getAsJsonArray("maps");
	        
	        for(int mapsindex=0; mapsindex<jarrayMaps.size(); mapsindex++) {
	        	
	        	JsonObject jsonobjectMapsBody = jarrayMaps.get(mapsindex).getAsJsonObject();
	        	
	        	String mapJsonKey = jsonobjectMapsBody.get("type").toString().replaceAll("^\"|\"$", "");
	        	
		        JsonArray jarrayMapsBody = jsonobjectMapsBody.getAsJsonArray("values");
		        
		        JsonObject jsonobjectMapsBodyValue = jarrayMapsBody.get(0).getAsJsonObject();
		        
		        String mapJsonvalue = jsonobjectMapsBodyValue.get("value").toString().replaceAll("^\"|\"$", "");
		        
		        mapofMaps.put(mapJsonKey, mapJsonvalue);

	        	
	        }
	        
		    JsonArray jarrayPrices = jsonobject.getAsJsonArray("prices");

	        
	        for(int pricesindex=0; pricesindex<jarrayPrices.size(); pricesindex++) {
	        	
	        	JsonObject jsonobjectPricesBody = jarrayPrices.get(pricesindex).getAsJsonObject();
	        	
	        	String pricesJsonKey = jsonobjectPricesBody.get("type").toString().replaceAll("^\"|\"$", "");
	        	
		        JsonArray jarrayPricesBody = jsonobjectPricesBody.getAsJsonArray("values");
		        
		        JsonObject jsonobjectPricesBodyValue = jarrayPricesBody.get(0).getAsJsonObject();
		        
		        String pricesJsonvalue = jsonobjectPricesBodyValue.get("value").toString().replaceAll("^\"|\"$", "");
		        
		        mapofPrices.put(pricesJsonKey, pricesJsonvalue);
		        
		        if(pricesJsonKey.equals("WSP")) {
		        	
			        priceOrderedCurrency = jsonobjectPricesBodyValue.get("currency").toString().replaceAll("^\"|\"$", "");
			        mapofPrices.put("priceOrderedCurrency", priceOrderedCurrency);

		        	
		        }

	        	
	        }
	        
	       	//fetching  priceOrderedCurrency
	        
	        String PriceOrderedAmount = null;
	        
	        switch (quantityType) {
			case "CS":
				PriceOrderedAmount = mapofPrices.get("WSCP");
				break;
				
			case "EA":
				PriceOrderedAmount = mapofPrices.get("WSP");
				break;

			default:
				break;
			}
	        
	        
		    
		    System.out.println(mapofMaps.size());
		    System.out.println(mapofPrices.size());
		    
		    String itemDetailsbody = "{\"quantityType\": \"" + quantityType + "\",\"itemId\": \"" + ItemId +"\",\"itemAlternateId\": {\"clientId\": \"" + mapofMaps.get("CLIENTID") +"\",\"barcodeEan\": \"" + mapofMaps.get("EAN") +"\",\"skuPin\": \"" + mapofMaps.get("PIN") +"\"},\"priceOrderedAmount\": " + PriceOrderedAmount + ",\"priceOrderedCurrency\": \"" + priceOrderedCurrency + "\",\"itemBaseType\": \"" + itemBaseType + "\",\"itemLineId\": \"" + itemLineId + "\",\"quantityOrdered\": " + quantityOrdered + ",\"priceOrderedTaxRate\": " + mapofPrices.get("VAT") + ",\"itemDescription\": \"" + description + "\",\"itemCaseSize\": " + mapofPrices.get("CS").substring(0, mapofPrices.get("CS").indexOf(".")) + "}";
		    	    
		    System.out.println(itemDetailsbody);	    
		    	     
		    res = itemDetailsbody;
			
			
        	
        }
        	  
       
        
       // }
    	
	} catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e,Resultpath);
        
		res = null;
		return res;
	}
    
    finally{
        
        response.close();
        
}
    
    return res;
    
	
}



public static boolean PostCallOrderCreation_Negative_SC(String DriverExcelPath, String ItemDetailsSheetName, String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String OrderCount,String MessageType, String ShipToLocationId, String ShipToDeliverAt, String AllOrderAttributes, String AllMessageAttributes, String AllShipToAddressAttributes, String AllBillToAddressAttributes, String AllBillToTaxAddressAttributes, String AllItemAttributes, int r, String TemporaryConsolidationFilePath,String Keyword,String tFileName,String tcid,String ResultPath,String ItemType,XWPFRun xwpfRun) throws IOException {
	
	boolean res = false;
	
	int rr=1;
	
	Workbook wrk1 =null;

	CloseableHttpResponse response=null;
    
	
	try {
	

	wrk1 = Workbook.getWorkbook(new File(DriverExcelPath));
		
		Sheet sheet1 = wrk1.getSheet(Keyword);
		
		Sheet env=wrk1.getSheet("Env");

		int rows = sheet1.getRows();

		//int r = 0;

	//String OrderCount="1:1";
	
	//String ItemID[] = AllItemIDs.split(",");
	//String QuantityOrdered[] = AllQuantitiesOrdered.split(",");
	String OrderAttributes[] = AllOrderAttributes.split(",");
	String MessageAttributes[] = AllMessageAttributes.split(",");
	String ShipToAddressAttributes[] = AllShipToAddressAttributes.split(",");
	String BillToAddressAttributes[] = AllBillToAddressAttributes.split(",");
	String BillToTaxAddressAttributes[] = AllBillToTaxAddressAttributes.split(",");
	//String ItemAttributes[] = AllItemAttributes.split(",");
	
	String ItemDetails = "";
	//String Keyword = null;
	String Items = null;
	
	String ItemTypes[] = ItemType.split(",");
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

   
    int occurances = 0;
    

		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpPost httppost = new HttpPost(UrlTail + "?" + ApiKey);
        
        httppost.setConfig(config);
        
        httppost.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Executing request " + httppost.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httppost.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfRun,"");
        
        /*for(int i = 1; i <= ItemID.length; i++) {
        	
        	ItemDetails = ItemDetails + "{\"itemId\": \"" + ItemID[i-1] + "\",\"itemLineId\": " + i + ",\"itemBaseType\": \"" + ItemAttributes[0] + "\",\"itemAlternateId\": {\"skuMin\": \"" + ItemAttributes[1] + "\",\"skuPin\": \"" + ItemAttributes[2] + "\",\"skuLegacy\": \"" + ItemAttributes[3] + "\",\"barcodeAsin\": \"" + ItemAttributes[4] + "\",\"barcodeEan\": \"" + ItemAttributes[5] + "\"},\"itemDescription\": \"" + ItemAttributes[6] + "\",\"quantityType\": \"" + ItemAttributes[7] + "\",\"quantityOrdered\": " + QuantityOrdered[i-1] + ",\"priceOrderedCurrency\": \"" + ItemAttributes[8] + "\",\"priceOrderedAmount\": " + ItemAttributes[9] + ",\"priceOrderedTaxRate\": " + ItemAttributes[10] + "}";
        	
        	if(i!=ItemID.length)
        		ItemDetails = ItemDetails + ",";
        }*/
        
       
		//int cols = sheet1.getColumns();

        
        System.out.println("Value of r "+r);
			
        Items = sheet1.getCell(26, r).getContents().trim();
			
			System.out.println("Number of ItemIDs " +Items);
			
			
			String[] AllItemsInItemDetails = Items.split(",");
			
			//System.out.println("Number of ItemIDs " + ItemID.length);
		
			
System.out.println("Items with details " + AllItemsInItemDetails.length);

	
for(int c=32; c< 32 + AllItemsInItemDetails.length; c++) {
				
		String ItemDetailsJson = sheet1.getCell(c, r).getContents().trim();
		
		
		
		//String NAS_ItemDetailsJson=ItemDetailsList.get(p);

		JsonElement Exp_NASjsonelement = new JsonParser().parse(ItemDetailsJson);


		JsonObject Exp_NASjsonobject = Exp_NASjsonelement.getAsJsonObject();

		String Exp_itemCaseSize= Exp_NASjsonobject.get("itemCaseSize").toString().replaceAll("^\"|\"$", "");
		
		String Exp_itemId=Exp_NASjsonobject.get("itemId").toString().replaceAll("^\"|\"$", "");
		
		//String Exp_quantityType=NASjsonobject.get("quantityType").toString().replaceAll("^\"|\"$", "");
		
				
		String Exp_quantityOrderedJson=Exp_NASjsonobject.get("quantityOrdered").toString().replaceAll("^\"|\"$", "");
		
		
		String Exp_quantityType = Exp_NASjsonobject.get("quantityType").toString().replaceAll("^\"|\"$", "");

		
		
	/*	
		
		if(Exp_quantityType.equalsIgnoreCase("EA")){
			 
		int Qty=Integer.parseInt(Exp_quantityOrderedJson);

		System.out.println(" Current item  qty is "+Qty);


		int CaseSize=Integer.parseInt(Exp_itemCaseSize);

		System.out.println("Current item  case size is "+CaseSize);



		int Qt= ((Qty +CaseSize - 1) / CaseSize);



		Exp_quantityOrderedJson=String.valueOf(Qt);

			 
		}
	*/	
		
		System.out.println("Final Qty is  is "+Exp_quantityOrderedJson);
		
		System.out.println("Final Item  is  is "+Exp_itemId);
		
		//Date date = new Date();
        
	//	System.out.println(new Timestamp(date.getTime()));

	
System.out.println(tFileName);

String ItemST=ItemTypes[c-32];

if(ItemST.contentEquals("P")){
		
 CreateTempLog.writeToLog(Exp_itemId, Exp_quantityOrderedJson,Exp_quantityType,Exp_itemCaseSize, tFileName);
 
 excelCellValueWrite.writeValueToCell(tFileName, 0, 0, DriverExcelPath, "Env");
 
  }

 

		
// CreateTempLog.writeToLog(Exp_itemId, Exp_quantityOrderedJson, "TempResultFile/ConsolidationLog.csv");		

				
ItemDetails = ItemDetails + ItemDetailsJson;
	        	
	if(c!=32+AllItemsInItemDetails.length -1)
	        		
	        
	   ItemDetails = ItemDetails + ",";
			
			
       }
			

        //String json = "{ \"orders\": {\"orderId\": \"387429-02\", \"orderBuyerPartyId\": \"5013546056078\", \"orderSellerPartyId\": \"5013546229809\", \"orderReferenceCode\": \"Local-Order-3607-20-20170314\",\"messageId\": \"FS-20170811-133400\", \"messageSenderPartyId\": \"5013546056078\", \"messageRecipientPartyId\": \"5013546229809\", \"messageType\": \"MOR\", \"messageCreatedAt\": \"2017-08-11T13:34:00Z\",\"shipToPartyId\": \"MOR-HEEL\", \"shipToLocationId\": \"542\", \"shipToAddressName\": \"Palmer Harvey\", \"shipToAddressLine1\": \"Maxted Road\", \"shipToAddressLine2\": \"--\", \"shipToAddressCity\": \"Hemel Hempstead\", \"shipToAddressState\": \"Herts\", \"shipToAddressPostCode\": \"HP2 7DX\", \"shipToAddressCountryCode\": \"UK\",\"shipToDeliverAt\": \"2017-03-15\", \"shipToDeliverLatestAt\": \"2017-03-15\",\"billToPartyId\": \"5013546056078\", \"billToAddressName\": \"PALMER AND HARVEY MCLANE\", \"billToAddressLine1\": \"P&H HOUSE\", \"billToAddressLine2\": \"DAVIGDOR ROAD\", \"billToAddressCity\": \"HOVE\", \"billToAddressState\": \"EAST SUSSEX\", \"billToAddressPostCode\": \"BN3 1RE\", \"billToAddressCountryCode\": \"UK\",\"billToTaxId\": \"GB232169089\", \"billToTaxAddressName\": \"PALMER AND HARVEY MCLANE\", \"billToTaxAddressLine1\": \"P&H HOUSE\", \"billToTaxAddressLine2\": \"DAVIGDOR ROAD\", \"billToTaxAddressCity\": \"HOVE\", \"billToTaxAddressState\": \"EAST SUSSEX\", \"billToTaxAddressPostCode\": \"BN3 1RE\", \"billToTaxAddressCountryCode\": \"UK\"}, \"items\": [{\"itemId\": \"104466391\", \"itemLineId\": 1, \"itemBaseType\": \"skuMin\", \"itemDescription\": \"M Pork Sausage Rolls 6s\",\"itemAlternateId\": { \"skuMin\": \"104466391\",\"skuLegacy\": \"0617707\", \"phId\": \"53238\" },\"quantityType\": \"CS\", \"quantityOrdered\": 1,\"priceOrderedCurrency\": \"GBP\", \"priceOrderedAmount\": 1.75, \"priceOrderedTaxRate\": 0},{\"itemId\": \"105462210\", \"itemLineId\": 2, \"itemBaseType\": \"skuMin\", \"itemDescription\": \"M Banana Flav Milk 1L\",\"itemAlternateId\": { \"skuMin\": \"105462210\",\"skuLegacy\": \"0403674\", \"phId\": \"53241\" },\"quantityType\": \"CS\", \"quantityOrdered\": 1,\"priceOrderedCurrency\": \"GBP\", \"priceOrderedAmount\": 1.29, \"priceOrderedTaxRate\": 0}] }";
       
        //{\"itemId\": \"" + ItemID[0] + "\",\"itemLineId\": 1,\"itemBaseType\": \"skuMin\",\"itemAlternateId\": {\"skuMin\": \"M104593859\",\"skuPin\": \"P104593867\",\"skuLegacy\": \"L0472851\",\"barcodeAsin\": \"ASIN29384845\",\"barcodeEan\": \"EAN3928445\"},\"itemDescription\": \"Heinz Baked Beans\",\"quantityType\": \"unit\",\"quantityOrdered\": " + QuantityOrdered[0] + ",\"priceOrderedCurrency\": \"GBP\",\"priceOrderedAmount\": 1.00,\"priceOrderedTaxRate\": 20.00},{\"itemId\": \"" + ItemID[1] + "\",\"itemLineId\": 2,\"itemBaseType\": \"skuMin\",\"itemAlternateId\": {\"skuMin\": \"M104593859\",\"skuPin\": \"P103201067\",\"skuLegacy\": \"L0419068\",\"barcodeAsin\": \"ASIN29383445\",\"barcodeEan\": \"EAN39234545\"},\"itemDescription\": \"Walkers Crips\",\"quantityType\": \"unit\",\"quantityOrdered\": " + QuantityOrdered[1] + ",\"priceOrderedCurrency\": \"GBP\",\"priceOrderedAmount\": 2.00,\"priceOrderedTaxRate\": 5.00}
        
        
        
        //String json = "{  \"orders\":{\"orderId\": \"" + OrderID + "\",\"orderBuyerPartyId\": \"" + OrderAttributes[0] + "\",\"orderSellerPartyId\": \"" + OrderAttributes[1] + "\",\"orderReferenceCode\": \"" + OrderAttributes[2] + "\",\"messageId\": \"" + MessageAttributes[0] + "\",\"messageSenderPartyId\": \"" + MessageAttributes[1] + "\",\"messageRecipientPartyId\": \"" + MessageAttributes[2] + "\",\"messageType\": \"" + MessageType + "\",\"messageCreatedAt\": \"" + MessageAttributes[3] + "\",\"shipToPartyId\": \"" + ShipToAddressAttributes[0] + "\",\"shipToLocationId\": \"" + ShipToLocationId + "\",\"shipToAddressName\": \"" + ShipToAddressAttributes[1] + "\",\"shipToAddressLine1\": \"" + ShipToAddressAttributes[2] + "\",\"shipToAddressLine2\": \"" + ShipToAddressAttributes[3] + "\",\"shipToAddressCity\": \"" + ShipToAddressAttributes[4] + "\",\"shipToAddressState\": \"" + ShipToAddressAttributes[5] + "\",\"shipToAddressPostCode\": \"" + ShipToAddressAttributes[6] + "\",\"shipToAddressCountryCode\": \"" + ShipToAddressAttributes[7] + "\",\"shipToDeliverAt\": \"" + ShipToDeliverAt + "\",\"shipToDeliverLatestAt\": \"" + ShipToAddressAttributes[8] + "\",\"shipFromLocationId\": \"" + ShipToAddressAttributes[9] + "\",\"shipFromAddressName\": \"" + ShipToAddressAttributes[10] + "\",\"billToPartyId\": \"" + BillToAddressAttributes[0] + "\",\"billToAddressName\": \"" + BillToAddressAttributes[1] + "\",\"billToAddressLine1\": \"" + BillToAddressAttributes[2] + "\",\"billToAddressLine2\": \"" + BillToAddressAttributes[3] + "\",\"billToAddressCity\": \"" + BillToAddressAttributes[4] + "\",\"billToAddressState\": \"" + BillToAddressAttributes[5] + "\",\"billToAddressPostCode\": \"" + BillToAddressAttributes[6] + "\",\"billToAddressCountryCode\": \"" + BillToAddressAttributes[7] + "\",\"billToTaxId\": \"" + BillToTaxAddressAttributes[0] + "\",\"billToTaxAddressName\": \"" + BillToTaxAddressAttributes[1] + "\",\"billToTaxAddressLine1\": \"" + BillToTaxAddressAttributes[2] + "\",\"billToTaxAddressLine2\": \"" + BillToTaxAddressAttributes[3] + "\",\"billToTaxAddressCity\": \"" + BillToTaxAddressAttributes[4] + "\",\"billToTaxAddressState\": \"" + BillToTaxAddressAttributes[5] + "\",\"billToTaxAddressPostCode\": \"" + BillToTaxAddressAttributes[6] + "\",\"billToTaxAddressCountryCode\": \"" + BillToTaxAddressAttributes[7] + "\"},\"items\": [" + ItemDetails + "]}";
        String json = "{  \"orders\":{\"orderId\": \"" + OrderID + "\",\"orderCount\": \"" + OrderCount + "\",\"shipToLocationId\": \"" + ShipToLocationId + "\",\"shipToPartyId\": \"" + ShipToAddressAttributes[0] + "\",\"orderSellerPartyId\": \"" + OrderAttributes[1] + "\",\"billToAddressName\": \"" + BillToAddressAttributes[1] + "\",\"shipToAddressLine1\": \"" + ShipToAddressAttributes[2] + "\",\"shipToAddressCity\": \"" + ShipToAddressAttributes[4] + "\",\"shipToAddressLine2\": \"" + ShipToAddressAttributes[3] + "\",\"billToTaxAddressCity\": \"" + BillToTaxAddressAttributes[4] + "\",\"orderReferenceCode\": \"" + OrderAttributes[2] + "\",\"messageType\": \"" + MessageType + "\",\"billToPartyId\": \"" + BillToAddressAttributes[0] + "\",\"shipToAddressPostCode\": \"" + ShipToAddressAttributes[6] + "\",\"billToAddressLine1\": \"" + BillToTaxAddressAttributes[2] + "\",\"billToTaxAddressPostCode\": \"" + BillToTaxAddressAttributes[6] + "\",\"billToAddressLine2\": \"" + BillToAddressAttributes[3] + "\",\"billToAddressPostCode\": \"" + BillToAddressAttributes[6] + "\",\"messageSenderPartyId\": \"" + MessageAttributes[1] + "\",\"shipToAddressState\": \"" + ShipToAddressAttributes[5] + "\",\"shipToDeliverAt\": \"" + ShipToDeliverAt + "\",\"shipToAddressName\": \"" + ShipToAddressAttributes[1] + "\",\"messageCreatedAt\": \"" + MessageAttributes[3] + "\",\"billToAddressState\": \"" + BillToAddressAttributes[5] + "\",\"billToTaxId\": \"" + BillToTaxAddressAttributes[0] + "\",\"messageId\": \"" + MessageAttributes[0] + "\",\"billToAddressCountryCode\": \"" + BillToAddressAttributes[7] + "\",\"billToTaxAddressState\": \"" + BillToTaxAddressAttributes[5] + "\",\"billToTaxAddressCountryCode\": \"" + BillToTaxAddressAttributes[7] + "\",\"messageRecipientPartyId\": \"" + MessageAttributes[2] + "\",\"shipToAddressCountryCode\": \"" + ShipToAddressAttributes[7] + "\",\"orderBuyerPartyId\": \"" + OrderAttributes[0] + "\",\"billToTaxAddressLine1\": \"" + BillToTaxAddressAttributes[2] + "\",\"shipToDeliverLatestAt\": \"" + ShipToAddressAttributes[8] + "\",\"billToTaxAddressLine2\": \"" + BillToTaxAddressAttributes[3] + "\",\"billToAddressCity\": \"" + BillToAddressAttributes[4] + "\",\"billToTaxAddressName\": \"" + BillToTaxAddressAttributes[1] + "\"},\"items\": [" + ItemDetails + "]}";

        
        //String json = "{  \"orders\":{\"orderId\": \"" + OrderID + "\",\"shipToLocationId\": \"" + ShipToLocationId + "\",\"shipToPartyId\": \"" + ShipToAddressAttributes[0] + "\",\"orderSellerPartyId\": \"" + OrderAttributes[1] + "\",\"billToAddressName\": \"" + BillToAddressAttributes[1] + "\",\"shipToAddressLine1\": \"" + ShipToAddressAttributes[2] + "\",\"shipToAddressCity\": \"" + ShipToAddressAttributes[4] + "\",\"shipToAddressLine2\": \"" + ShipToAddressAttributes[3] + "\",\"billToTaxAddressCity\": \"" + BillToTaxAddressAttributes[4] + "\",\"orderReferenceCode\": \"" + OrderAttributes[2] + "\",\"messageType\": \"" + MessageType + "\",\"billToPartyId\": \"" + BillToAddressAttributes[0] + "\",\"shipToAddressPostCode\": \"" + ShipToAddressAttributes[6] + "\",\"billToAddressLine1\": \"" + BillToTaxAddressAttributes[2] + "\",\"billToTaxAddressPostCode\": \"" + BillToTaxAddressAttributes[6] + "\",\"billToAddressLine2\": \"" + BillToAddressAttributes[3] + "\",\"billToAddressPostCode\": \"" + BillToAddressAttributes[6] + "\",\"messageSenderPartyId\": \"" + MessageAttributes[1] + "\",\"shipToAddressState\": \"" + ShipToAddressAttributes[5] + "\",\"shipToDeliverAt\": \"" + ShipToDeliverAt + "\",\"shipToAddressName\": \"" + ShipToAddressAttributes[1] + "\",\"messageCreatedAt\": \"" + MessageAttributes[3] + "\",\"billToAddressState\": \"" + BillToAddressAttributes[5] + "\",\"billToTaxId\": \"" + BillToTaxAddressAttributes[0] + "\",\"messageId\": \"" + MessageAttributes[0] + "\",\"billToAddressCountryCode\": \"" + BillToAddressAttributes[7] + "\",\"billToTaxAddressState\": \"" + BillToTaxAddressAttributes[5] + "\",\"billToTaxAddressCountryCode\": \"" + BillToTaxAddressAttributes[7] + "\",\"messageRecipientPartyId\": \"" + MessageAttributes[2] + "\",\"shipToAddressCountryCode\": \"" + ShipToAddressAttributes[7] + "\",\"orderBuyerPartyId\": \"" + OrderAttributes[0] + "\",\"billToTaxAddressLine1\": \"" + BillToTaxAddressAttributes[2] + "\",\"shipToDeliverLatestAt\": \"" + ShipToAddressAttributes[8] + "\",\"billToTaxAddressLine2\": \"" + BillToTaxAddressAttributes[3] + "\",\"billToAddressCity\": \"" + BillToAddressAttributes[4] + "\",\"billToTaxAddressName\": \"" + BillToTaxAddressAttributes[1] + "\",\"billToTaxAddressPostCode\": \"" + BillToTaxAddressAttributes[6] + "\",\"billToTaxAddressCountryCode\": \"" + BillToTaxAddressAttributes[7] + "\"},\"items\": [" + ItemDetails + "]}";

        
        // httppost.setEntity(new StringEntity(json, "UTF-8"));
        
        System.out.println(json);
        
        httppost.setEntity(new StringEntity(json));
		
       // httppost.setHeader("apikey", "l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httppost.setHeader("Accept", "application/json");
		httppost.setHeader("Content-type", "application/json");
		
		utilityFileWriteOP.writeToLog(tcid, "Sending the JSON ", "Body :- "+json);
		
		
		
		 System.out.println("JSON:::: "+json);
		
		response = httpclient.execute(target, httppost);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code is ", "Displayed as: "+response.getStatusLine().getStatusCode(),ResultPath,xwpfRun,"");

        System.out.println(EntityUtils.toString(response.getEntity()));
        utilityFileWriteOP.writeToLog(tcid, "Response Body is ", "Body :- "+EntityUtils.toString(response.getEntity()),ResultPath,xwpfRun,"");
    
        //response.toString();
		if(response.getStatusLine().getStatusCode() == 201)
			res = true;
		else
			
		    res=false;

    }
		catch (Exception e) {
		// TODO: handle exception
		System.out.println(e);
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during POST call ", "Due to :"+e,ResultPath,xwpfRun,"");
        
		res = false;
		
		
	}
    
    finally{
        
        response.close();
        
        wrk1.close();
        
        return res;
        
}
    
   
	
	
 }


public static boolean GetOrderStatusValidation(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String tcid,String ResultPath,XWPFRun xwpfRun) throws IOException {
	
	boolean res = false;
	
	
	String [] shipStatusArrey=statusCurrentValue.split(",");
	
	statusCurrentValue=shipStatusArrey[0];

	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "?" + ApiKey);
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        
        
        for (int j=0;j<4;j++){
        
        
       Thread.sleep(2000);
        	
        	
        System.out.println("Order ID is " + OrderID);
        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfRun,"");

        response = httpclient.execute(target, httpget);

        
        
      //  System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        
        
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath,xwpfRun,"");
        
        String jsonresponse = EntityUtils.toString(response.getEntity());
        
        
        
        
      //  System.out.println(jsonresponse);
        
        
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse);
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        JsonArray jarray = jsonobject.getAsJsonArray("orders");
        
        JsonObject jsonobject1=jarray.get(0).getAsJsonObject();
       
        
     //   JsonArray jarray = jsonobject.getAsJsonArray("items");
      //  System.out.println(jarray.size());
     //   for(int i=0; i<jarray.size(); i++) {
        
      //  System.out.println(jsonobject.toString());
        
        
        //utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
        
       // JsonArray jarray = jsonobject.getAsJsonArray("orders");
        
        
    //  System.out.println(jarray);
       
    
	        
        	
        //String statusValidationResult = jsonobject.get("statusValidation").toString();
	        
	        String statusCurrent = jsonobject1.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
	        
	        //System.out.println("StatusValidation Result is " + statusValidationResult);

	        
	    	System.out.println("statusCurrent is " +statusCurrent+"###"+statusCurrentValue);
	        
	        if(statusCurrent.equals(statusCurrentValue)) {
	        	res = true;
				utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent, "Success",ResultPath,xwpfRun,"");
				//System.out.println("statusCurrent is " +statusCurrent+"###"+statusCurrentValue);
				
			     break;
			
	        }
	        
	        else{
	        	res = false;
				utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent,"FAIL",ResultPath,xwpfRun,"");
				System.out.println("statusCurrent is " +statusCurrent);
				
				continue;
	        	
	        }
        	
       
        }
        
      
    	
	}  //end of try
    
    
    
    catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e,ResultPath,xwpfRun,"");
        
		res = false;
		
		
		//return res;
	}
    
    finally{
        
     response.close();
     
     return res;
        
}
    
    
    
	
}


public static boolean GetCallItemStatusValidationWithInvalidItems(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID,String statusCurrentValue, String ItemStatus, String tcid,String ResultPath,XWPFRun xwpfRun) throws IOException {
	
	boolean res = true;
	
	int p=0;
	
	
	 HashMap<String, String> hm = null;
	 HashMap<String, String> hm1 = null;
	 
	 HashMap<String, String> ValidStatusCnt = null;
	 HashMap<String, String> InvalidValidStatusCnt = null;
	 
	 
	 HashMap<String, String> ValidStatusCntST_StringList = null;
	 HashMap<String, String> InvalidValidStatusCnt_StringList = null;
	 
	 
	 
	 
	 
		
     hm = new HashMap<String, String>();
     
     hm1 = new HashMap<String, String>();
     
     ValidStatusCnt = new HashMap<String, String>();
     
     InvalidValidStatusCnt=new HashMap<String, String>();
     
     
	 ValidStatusCntST_StringList = new HashMap<String, String>();
	InvalidValidStatusCnt_StringList = new HashMap<String, String>();
     
     
     
     
     
	
	int validExpectedCount=0;
	int invalidExpectedCount=0;
	
	String[] ItemsStatus = new String[50];
	
	String[] ItemsStatusInput=ItemStatus.split(",");
	
	for(int u=0;u<ItemsStatusInput.length;u++){
	
	
	  if(ItemsStatusInput[u].contains("P")){
	
		
		  validExpectedCount=validExpectedCount+1;
		  
		  
	  }
	  
	  
	  else  {
		  
		  
		  invalidExpectedCount=invalidExpectedCount+1;
		  
		  
		  
	  }
		  
		  
		
	}
		
		
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Order ID is " + OrderID);
        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfRun,"");
        
       // for(int j =1; j<= 10; j ++) {
        	
        	
        Thread.sleep(10000);
        
        response = httpclient.execute(target, httpget);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
      
        
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath,xwpfRun,"");
        
        String jsonresponse = EntityUtils.toString(response.getEntity());
        
        
        System.out.println(jsonresponse);
        
        
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath,xwpfRun,"");
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
      
        
        //System.out.println(jsonobject.toString());
       
        
        
     //   utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
        
       
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        
        int stCnt=jarray.size();
        
        
     //   System.out.println("Json Array Size  for get call is "+stCnt);
        
        
        int mtchCnt=0;
        
      
        System.out.println(jarray.size());
       
        String statusCurrent =null;
        String CurrentItemID=null;
        
        
        for(int i=0; i<jarray.size(); i++) {
        	
        	jsonobject = jarray.get(i).getAsJsonObject();
	        String statusValidationResult = jsonobject.get("statusValidation").toString();
	        
	        statusCurrent = jsonobject.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
	        CurrentItemID = jsonobject.get("itemId").toString().replaceAll("^\"|\"$", "");

	        System.out.println("StatusValidation Result is " + statusValidationResult);

	        
	        utilityFileWriteOP.writeToLog(tcid, "StatusValidation Result for Item  ::  "+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", "")+" IS ..", statusValidationResult,ResultPath,xwpfRun,"");
	        
	        utilityFileWriteOP.writeToLog(tcid, "StatusValidation Result for Item  ::  "+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", "")+" IS ..", statusValidationResult,ResultPath);
	        
	        int countOne = statusValidationResult.split("1").length - 1;
	        
	if(countOne == 9) {
	        	res = true;
	        	//System.out.println("Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));

	        	//utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is successful ", "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath,xwpfRun,"");
				
	        	ValidStatusCntST_StringList.put(String.valueOf(i),statusValidationResult);
	        	
	        	//System.out.println("Count of 1 is 9");
				
				
	  hm1.put(String.valueOf(i), "For Item ID -"+ CurrentItemID+"  Status Validation Result  is "  +statusValidationResult);
				
	 // utilityFileWriteOP.writeToLog(tcid, "For Item ID -"+ CurrentItemID+"  Status Validation Result  is "  +statusValidationResult, "",ResultPath);		
				
	       
	
	}
	       
	        
	else {
	        	
	        	//res = false;
		
		InvalidValidStatusCnt_StringList.put(String.valueOf(i),statusValidationResult);
				
		        hm1.put(String.valueOf(i), "For Item ID -"+ CurrentItemID+"  Status Validation Result  is "  +statusValidationResult);
	        	//utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion Failed ", "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath,xwpfRun,"");

		      //  utilityFileWriteOP.writeToLog(tcid, "For Item ID -"+ CurrentItemID+"  Status Validation Result   "  +statusValidationResult, "",ResultPath);		
				
		        
		        
	        	//break;
	        }
	        
	        
	        
	        if(statusCurrent.equals(statusCurrentValue)) {
	        	
	        	// ItemsStatus[p]=statusCurrent;
	        	 
	        	 ValidStatusCnt.put(String.valueOf(i), statusCurrent);
	        	 
	        	 
	        	 hm.put(String.valueOf(i), "For Item ID -"+ CurrentItemID+"  Status is "  +statusCurrent);
	        	
	        	 p=p+1;
	        	
	        	// utilityFileWriteOP.writeToLog(tcid, "For Item ID -"+ CurrentItemID+"  Status Validation Result   "  +statusCurrent, "",ResultPath);		
					
	        	 
	        	 
	        	 
	        	 
	        	
	        	//res = true;
				utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent+" for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),"",ResultPath,xwpfRun,"");
				
				 hm.put(String.valueOf(i), "For Item ID -"+ CurrentItemID+"  Status is "  +statusCurrent);
				
				System.out.println("statusCurrent is " +statusCurrent+"for "+CurrentItemID);
				
				
		      //	 utilityFileWriteOP.writeToLog(tcid, "For Item ID -"+ CurrentItemID+"  Status Validation Result   "  +statusCurrent, "",ResultPath);	
				
				//mtchCnt=mtchCnt+1;
				
				
			//break;
	        }
	        
	        else{
	        	
	        	
	    //ItemsStatus[p]=statusCurrent;
	    
	    InvalidValidStatusCnt.put(String.valueOf(i), statusCurrent);
	        	
	        	
      utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent+" for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),"",ResultPath,xwpfRun,"");
				
	   System.out.println("statusCurrent is " +statusCurrent+"for "+CurrentItemID);
	   
	   hm.put(String.valueOf(i), "For Item ID -"+ CurrentItemID+"  Status is "  +statusCurrent);
	   
	 //  utilityFileWriteOP.writeToLog(tcid, "For Item ID -"+ CurrentItemID+"  Status Validation Result   "  +statusCurrent, "",ResultPath);		
		
	        	
	
				
	//System.out.println("statusCurrent is " +statusCurrent);
	        	
	        }
        	
      }
        
        
  //  utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent+" for Item ID:"+CurrentItemID,"",ResultPath,xwpfRun,"");
		    

    
    
    
  for(int cnt=0;cnt<hm.size();cnt++){
    
    String status=hm.get(String.valueOf(cnt));
    	
    //utilityFileWriteOP.writeToLog(tcid, status,"",ResultPath,xwpfRun,"");

    }
    
  for(int cnt1=0;cnt1<hm1.size();cnt1++){
	    
	    String status=hm.get(String.valueOf(cnt1));
	    	
	   // utilityFileWriteOP.writeToLog(tcid, status,"",ResultPath,xwpfRun,"");

	 } 
  

  
  if(validExpectedCount==ValidStatusCnt.size()){

	// utilityFileWriteOP.writeToLog(tcid, "Item Status validation is success for all valid items " ,"",ResultPath,xwpfRun,"");

	 res=res&&true;
	 
	//  utilityFileWriteOP.writeToLog(tcid, "Item Status validation is success for all valid items ", "",ResultPath);		
		

   } 
  
  
  else{
  
	  
	 // utilityFileWriteOP.writeToLog(tcid, "Item Status validation is not success for all valid items " ,"",ResultPath,xwpfRun,"");
	  res=res&&false;
	  
	 // utilityFileWriteOP.writeToLog(tcid, "Item Status validation is not success for all valid items ", "",ResultPath);		
  
  
  
  }
  
  
  
  if(invalidExpectedCount==InvalidValidStatusCnt.size()){

		// utilityFileWriteOP.writeToLog(tcid, "Item Status validation is success for all invalid items " ,"",ResultPath,xwpfRun,"");

		 res=res&&true;
		 
		 
		// utilityFileWriteOP.writeToLog(tcid, "Item Status validation is success for all invalid items ", "",ResultPath);		
			
		 
  
  } 
	  
	  
else{
	  
		  
     //  utilityFileWriteOP.writeToLog(tcid, "Item Status validation is not success for all invalid items " ,"",ResultPath,xwpfRun,"");

       res=res&&false;
       
     //  utilityFileWriteOP.writeToLog(tcid, "Item Status validation is not success for all invalid items ", "",ResultPath);		
		
       
       

	  }

  
/*
  ValidStatusCntST_StringList = new HashMap<String, String>();
  InvalidValidStatusCnt_StringList = new HashMap<String, String>();
  
  
  
  if(validExpectedCount==ValidStatusCntST_StringList.size()){

		 utilityFileWriteOP.writeToLog(tcid, "Item Status validation is success for all valid items " ,"",ResultPath,xwpfRun,"");

		 res=res&&true;
		 
		 
		 utilityFileWriteOP.writeToLog(tcid, "Item Status String  validation is success for all valid items ", "",ResultPath);		
			
		 
	   } 
	  
	  
	  else{
	  
		  
		  utilityFileWriteOP.writeToLog(tcid, "Item Status validation is not success for all valid items " ,"",ResultPath,xwpfRun,"");
		  res=res&&false;
		  
		  
		  utilityFileWriteOP.writeToLog(tcid, "Item Status String  validation not  success for all valid items ", "",ResultPath);		
			
	  
	  }
  
  
  if(invalidExpectedCount==InvalidValidStatusCnt_StringList.size()){

		 utilityFileWriteOP.writeToLog(tcid, "Item Status  validation is success for all invalid items " ,"",ResultPath,xwpfRun,"");

		 res=res&&true;
		 
		 utilityFileWriteOP.writeToLog(tcid, "Item Status String  validation is success for all invalid items ", "",ResultPath);		
			
		 
		 

} 
	  
	  
else{
	  
		  
    utilityFileWriteOP.writeToLog(tcid, "Item Status validation is not success for all invalid items " ,"",ResultPath,xwpfRun,"");

    utilityFileWriteOP.writeToLog(tcid, "Item Status String  validation not success for all invalid items ", "",ResultPath);		
	
    
    
    res=res&&false;

	  }
  
  */
  

        
 //  }
    	
} 
    
    
catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		
		
	//	utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+ItemsStatus.toString(),"",ResultPath,xwpfRun,"");
		
		
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e,ResultPath,xwpfRun,"");
        
	//	res = false;
		
		
	}
    
    finally{
        
        response.close();
        
        return res;
        
   }

}


	
	

}
