package OrderServices_Functions
;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Vector;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;




import Utilities.*;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import jxl.Sheet;
import jxl.Workbook;

public class McColls_OrderServices {
	

public static boolean OrderIdGenerationI19a(String TestDataPath, String SheetName,String TestKeyword,String tcid, String Resultpath) throws IOException {
		
		boolean res = false;

		int r = 0;
		
	    try {
	    	
	    	int rows = 0;
			
	    	Workbook wrk1 = Workbook.getWorkbook(new File(TestDataPath));
	    	
			Sheet sheet1 = wrk1.getSheet(SheetName); 
	         
			rows = sheet1.getRows();
			
			//long currentDateSec = (System.currentTimeMillis() / 1000);
			
			for(r=1; r<rows; r++) {
				String Keyword = sheet1.getCell(2, r).getContents().trim();
				if(Keyword.equalsIgnoreCase(TestKeyword)){
				long currentDateSec = (System.currentTimeMillis() / 1000);
				excelCellValueWrite.writeValueToCell(String.valueOf(currentDateSec), r, 16, TestDataPath, SheetName);
				System.out.println("Order Id generated: "+String.valueOf(currentDateSec));
				Thread.sleep(1200);
										
				
				}
			}
			
			
		
			
			wrk1.close();
			res = true;
			
	        	  
	       
		} catch (Exception e) {
			// TODO: handle exception
			
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during Order ID generation ", "Due to :"+e,Resultpath);
			
	        
			res = false;
			return res;
		}
	    
	    finally{
	        
	    	
	        
	}
	    
	    return res;
	    
		
	}



public static String OrderIdGeneration(String TestDataPath, String SheetName,String TestKeyword,String tcid, String Resultpath) throws IOException {
		
	String orderIdgen=null;
		int r = 0;
		
	    try {
	    	
	    	int rows = 0;
			
	    	Workbook wrk1 = Workbook.getWorkbook(new File(TestDataPath));
	    	
			Sheet sheet1 = wrk1.getSheet(SheetName); 
	         
			rows = sheet1.getRows();
			
			//long currentDateSec = (System.currentTimeMillis() / 1000);
			
			for(r=1; r<rows; r++) {
				String Keyword = sheet1.getCell(2, r).getContents().trim();
				if(Keyword.equalsIgnoreCase(TestKeyword)){
				long currentDateSec = (System.currentTimeMillis() / 1000);
				excelCellValueWrite.writeValueToCell(String.valueOf(currentDateSec), r, 16, TestDataPath, SheetName);
				System.out.println("Order Id generated: "+String.valueOf(currentDateSec));
				Thread.sleep(1200);
				orderIdgen=String.valueOf(currentDateSec);										
				
				}
			}
			
			
		
			
			wrk1.close();
			return orderIdgen;
			
	        	  
	       
		} catch (Exception e) {
			// TODO: handle exception
			
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during Order ID generation ", "Due to :"+e,Resultpath);
			
	        
			
			return orderIdgen;
		}
	    
	    finally{
	        
	    	   return orderIdgen;	
	        
	}
	    
	 
	    
		
	}


public static String GetCallquantityTypeFromWebPortal(String CatalogueURL,String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String ItemId, String tcid, String Resultpath,XWPFRun xwpfRun, ExtentTest logger) throws IOException {
	
	String res = null;
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        //https://sit-api.morrisons.com/wholesaleorder/v1/customers/mccolls/catalogues/main/items/101743031?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36&filtertype=maptype&filtervalue=PIN
       
        HttpGet httpget = new HttpGet(CatalogueURL+ ItemId + "?" + ApiKey+"&filtertype=maptype&filtervalue=PIN");
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Item ID is " + ItemId);
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);

        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",Resultpath,xwpfRun, "");
        response = httpclient.execute(target, httpget);
        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        logger.log(LogStatus.INFO, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
       
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),Resultpath,xwpfRun, "");
        logger.log(LogStatus.INFO, "Response code for Item search in Catalogue displayed is: "+response.getStatusLine().getStatusCode() +" for item ID : "+ItemId);
        String jsonresponse = EntityUtils.toString(response.getEntity());
        System.out.println(jsonresponse);
        
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,Resultpath,xwpfRun, "");
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString(),Resultpath,xwpfRun, "");
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        System.out.println(jarray.size());
        for(int i=0; i<jarray.size(); i++) {
        	jsonobject = jarray.get(i).getAsJsonObject();
        	JsonArray jarraylookups = jsonobject.getAsJsonArray("lookups");
		    HashMap<String, Long> mapofquantityType = new HashMap<String, Long>();
		    ArrayList<String> listoflookupkeys = new ArrayList<String>();
		    ArrayList<Long> listoflookupstartvalues = new ArrayList<Long>();
		    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
		    for(int lookupsindex=0; lookupsindex<jarraylookups.size(); lookupsindex++) {
		    	JsonObject jsonobjectlookupsBody = jarraylookups.get(lookupsindex).getAsJsonObject();
	        	String lookupsJsonKey = jsonobjectlookupsBody.get("type").toString().replaceAll("^\"|\"$", "");
	        	JsonArray jarraylookupsBody = jsonobjectlookupsBody.getAsJsonArray("values");
		        if(lookupsJsonKey.equals("SINGLE-PICK")) {
		        	for(int lookupsBodyindex=0; lookupsBodyindex<jarraylookupsBody.size(); lookupsBodyindex++) {
		        		JsonObject jsonobjectlookupsBodyValue = jarraylookupsBody.get(lookupsBodyindex).getAsJsonObject();
		        		String lookupsJsonvalue = jsonobjectlookupsBodyValue.get("value").toString().replaceAll("^\"|\"$", "");
				        String lookupsJsonstartdate = jsonobjectlookupsBodyValue.get("start").toString().replaceAll("^\"|\"$", "");
				        System.out.println(lookupsJsonvalue + " " + lookupsJsonstartdate);
				        Date date = formatter.parse(lookupsJsonstartdate.replaceAll("Z$", "+0000"));
				    	long startdatemilli = date.getTime();
				        mapofquantityType.put(lookupsJsonvalue, startdatemilli);
		        	}
	        	}
	    	}
		    for(String str: mapofquantityType.keySet()) {
		    	listoflookupkeys.add(str);
		    	listoflookupstartvalues.add(mapofquantityType.get(str));
		    }

		    long latestdatemilli = 0; 
		    for(int startdateindex=0; startdateindex<listoflookupstartvalues.size(); startdateindex++) {
		    	if(listoflookupstartvalues.get(startdateindex) > latestdatemilli) {
		    		latestdatemilli = listoflookupstartvalues.get(startdateindex);
		    	}
		    }
		    String finalSinglePickkey = null;
		    String quantityTypeFromWebPortal = null;
		    for(int mapofquantityTypeindex=0; mapofquantityTypeindex<mapofquantityType.size(); mapofquantityTypeindex++) {
		    	if( (mapofquantityType.get(listoflookupkeys.get(mapofquantityTypeindex))) == latestdatemilli) {
		    		finalSinglePickkey = listoflookupkeys.get(mapofquantityTypeindex);
		    	}
		    }
		    
		    System.out.println("latest date in milli "+latestdatemilli);
		    if(finalSinglePickkey.equals("Y")) {
		    	quantityTypeFromWebPortal = "EA";
		    }
		    
		    if(finalSinglePickkey.equals("N")) {
		    	quantityTypeFromWebPortal = "CS";
		    }
		    res = quantityTypeFromWebPortal;
		}
        	  
       
	} catch (Exception e) {
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e,Resultpath,xwpfRun, "");
		logger.log(LogStatus.FAIL, "Error occurred during GET call due to: "+e);
        res = null;
		return res;
	}
    
    finally{
        
        response.close();
        
}
    
    return res;
    
	
}

/*public static boolean GetCallItemStatusValidation(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String tcid,String ResultPath,XWPFRun xwpfrun, ExtentTest logger) throws IOException {
	
	boolean res = false;
	
	int MaxwaitingTime = 60000;
	
	int regularwaitTime = 10000;
	String expectedorderReferenceCode = System.getProperty("orderReferenceCode");
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Order ID is " + OrderID);
        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");
        logger.log(LogStatus.INFO, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
       
        
        
        for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
        	
        	
        	Thread.sleep(regularwaitTime);
        
        response = httpclient.execute(target, httpget);
        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");
        logger.log(LogStatus.INFO, "Response code displayed is: " + response.getStatusLine().getStatusCode());
        String jsonresponse = EntityUtils.toString(response.getEntity());
        
        
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath,xwpfrun,"");
        	
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        
        int stCnt=jarray.size();
        
        int mtchCnt=0;
        
        System.out.println(jarray.size());
        for(int i=0; i<jarray.size(); i++) {
        	
        	jsonobject = jarray.get(i).getAsJsonObject();
	        String statusValidationResult = jsonobject.get("statusValidation").toString();
	        
	        String statusCurrent = jsonobject.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
	    	String orderReferenceCode = jsonobject.get("orderReferenceCode").toString().replaceAll("^\"|\"$", "");

	        System.out.println("StatusValidation Result is " + statusValidationResult);
	        
	        utilityFileWriteOP.writeToLog(tcid, "StatusValidation Result is " + statusValidationResult, "",ResultPath,xwpfrun,"");
	        logger.log(LogStatus.INFO, "StatusValidation Result is : " + statusValidationResult);
	        int countOne = statusValidationResult.split("1").length - 1;
	        
	        if(countOne == 10) {
	        	res = true;
	        	System.out.println("Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				utilityFileWriteOP.writeToLog(tcid, "StatusValidation is successful ", "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				logger.log(LogStatus.PASS, "StatusValidation is successful");
				System.out.println("Count of 1 is 10");
	        }
	        else {
	        	res = false;
				utilityFileWriteOP.writeToLog(tcid, "StatusValidation Failed ", "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
	        	logger.log(LogStatus.FAIL, "StatusValidation Failed");
	        	//break;
	        }
	        
	        if(statusCurrent.equals(statusCurrentValue)) {
	        	res = true;
				utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent, "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath,xwpfrun,"");
				System.out.println("statusCurrent is " +statusCurrent);
				logger.log(LogStatus.PASS, "StatusCurrent is "+statusCurrent+". Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				mtchCnt=mtchCnt+1;
				
				
			//break;
	        }
	        
	        else{
	        	res = false;
				utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent, "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath,xwpfrun,"");
				logger.log(LogStatus.FAIL, "StatusCurrent is "+statusCurrent+". Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				System.out.println("statusCurrent is " +statusCurrent);
	        	
	        }
	        
	        if(orderReferenceCode.equals(expectedorderReferenceCode)) {
	        	res = true;
				utilityFileWriteOP.writeToLog(tcid, "orderReferenceCode is "+orderReferenceCode, "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath,xwpfrun,"");
				System.out.println("orderReferenceCode is " +orderReferenceCode);
				logger.log(LogStatus.PASS, "orderReferenceCode is "+orderReferenceCode+". Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				//mtchCnt=mtchCnt+1;
				
				
			//break;
	        }
	        
	        else{
	        	res = false;
				utilityFileWriteOP.writeToLog(tcid, "orderReferenceCode is "+orderReferenceCode, "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath,xwpfrun,"");
				logger.log(LogStatus.FAIL, "orderReferenceCode is "+orderReferenceCode+". Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				System.out.println("orderReferenceCode is " +orderReferenceCode);
	        	
	        }
        	
        }
        	  
       
        if(mtchCnt==stCnt) {
        	break;
        
        }
        
      }
    	
} 
    
    
catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
        
		res = false;
		
		
	}
    
    finally{
        
        response.close();
        
        return res;
        
}
    
  
    
	
}
	*/
public static String GetCallItemDetailsFromWebPortal(String CatalogueURL,String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String ItemId, String itemBaseType, String quantityType, String itemLineId, String quantityOrdered, String tcid, String Resultpath, XWPFRun xwpfRun,ExtentTest logger) throws IOException {
	
	String res = null;
	
	int MaxwaitingTime = 60000;
	
	int regularWaitingTime = 10000;
	
	HashMap<String, String> mapofMaps = new HashMap<String, String>();
	HashMap<String, String> mapofPrices = new HashMap<String, String>();

	String priceOrderedCurrency = null;
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        //https://sit-api.morrisons.com/wholesaleorder/v1/customers/mccolls/catalogues/main/items/101743031?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36&filtertype=maptype&filtervalue=PIN
        
        HttpGet httpget = new HttpGet(CatalogueURL + ItemId + "?" + ApiKey+"&filtertype=maptype&filtervalue=PIN");
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Item ID is " + ItemId);
        utilityFileWriteOP.writeToLog(tcid, "Searching the attributes for ", "Item ID "+ItemId,Resultpath);
        
//        logger.log(LogStatus.INFO, "Searching the attributes for Item ID: "+ItemId);
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",Resultpath);
        
     /*   for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
        	
        	Thread.sleep(regularWaitingTime);*/
        logger.log(LogStatus.INFO, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
       
        response = httpclient.execute(target, httpget);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),Resultpath);
        
        logger.log(LogStatus.INFO, "Response code for item attributes is displayed as: "+response.getStatusLine().getStatusCode() +"for item id:"+ItemId);
        String jsonresponse = EntityUtils.toString(response.getEntity());
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,Resultpath);
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString(),Resultpath);
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        System.out.println(jarray.size());
        for(int i=0; i<jarray.size(); i++) {
        	
        	jsonobject = jarray.get(i).getAsJsonObject();
        	
        	String description = jsonobject.get("description").toString().replaceAll("^\"|\"$", "");
        	
        	//String mapsJsonBody = jsonobject.get("maps").toString().replaceAll("^\"|\"$", "");
	        JsonArray jarrayMaps = jsonobject.getAsJsonArray("maps");
	        
	        for(int mapsindex=0; mapsindex<jarrayMaps.size(); mapsindex++) {
	        	
	        	JsonObject jsonobjectMapsBody = jarrayMaps.get(mapsindex).getAsJsonObject();
	        	
	        	String mapJsonKey = jsonobjectMapsBody.get("type").toString().replaceAll("^\"|\"$", "");
	        	
		        JsonArray jarrayMapsBody = jsonobjectMapsBody.getAsJsonArray("values");
		        
		        JsonObject jsonobjectMapsBodyValue = jarrayMapsBody.get(0).getAsJsonObject();
		        
		        String mapJsonvalue = jsonobjectMapsBodyValue.get("value").toString().replaceAll("^\"|\"$", "");
		        
		        mapofMaps.put(mapJsonKey, mapJsonvalue);

	        	
	        }
	        
		    JsonArray jarrayPrices = jsonobject.getAsJsonArray("prices");

	        
	        for(int pricesindex=0; pricesindex<jarrayPrices.size(); pricesindex++) {
	        	
	        	JsonObject jsonobjectPricesBody = jarrayPrices.get(pricesindex).getAsJsonObject();
	        	
	        	String pricesJsonKey = jsonobjectPricesBody.get("type").toString().replaceAll("^\"|\"$", "");
	        	
		        JsonArray jarrayPricesBody = jsonobjectPricesBody.getAsJsonArray("values");
		        
		        JsonObject jsonobjectPricesBodyValue = jarrayPricesBody.get(0).getAsJsonObject();
		        
		        String pricesJsonvalue = jsonobjectPricesBodyValue.get("value").toString().replaceAll("^\"|\"$", "");
		        
		        mapofPrices.put(pricesJsonKey, pricesJsonvalue);
		        
		        if(pricesJsonKey.equals("WSP")) {
		        	
			        priceOrderedCurrency = jsonobjectPricesBodyValue.get("currency").toString().replaceAll("^\"|\"$", "");
			        mapofPrices.put("priceOrderedCurrency", priceOrderedCurrency);

		        	
		        }

	        	
	        }
	        
	       	System.out.println(mapofMaps.size());
		    System.out.println(mapofPrices.size());
		    
		    String itemDetailsbody = "{\"quantityType\": \"" + quantityType + "\",\"itemId\": \"" + ItemId +"\",\"itemAlternateId\": {\"clientId\": \"" + mapofMaps.get("CLIENTID") +"\",\"barcodeEan\": \"" + mapofMaps.get("EAN") +"\",\"skuPin\": \"" + mapofMaps.get("PIN") +"\"},\"priceOrderedAmount\": " + mapofPrices.get("WSP") + ",\"priceOrderedCurrency\": \"" + priceOrderedCurrency + "\",\"itemBaseType\": \"" + itemBaseType + "\",\"itemLineId\": \"" + itemLineId + "\",\"quantityOrdered\": " + quantityOrdered + ",\"priceOrderedTaxRate\": " + mapofPrices.get("VAT") + ",\"itemDescription\": \"" + description + "\",\"itemCaseSize\": " + mapofPrices.get("CS").substring(0, mapofPrices.get("CS").indexOf(".")) + "}";
		    	    
		    System.out.println(itemDetailsbody);	    
		    	     
		    res = itemDetailsbody;
			
			
        	
        }
        	  
       
        
       // }
    	
	} catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e,Resultpath);
        
		res = null;
		return res;
	}
    
    finally{
        
        response.close();
        
}
    
    return res;
    
	
}



public static String GetCallVatAmountFromWebPortal(String CatalogueURL,String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String ItemId, String tcid, String Resultpath) throws IOException {
	
	String res = null;
	
	String EBSVatDetails = null;
	
	String pricesJsonvalue = null;
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        //https://sit-api.morrisons.com/wholesaleorder/v1/customers/mccolls/catalogues/main/items/101743031?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36&filtertype=maptype&filtervalue=PIN
        
        HttpGet httpget = new HttpGet(CatalogueURL+ ItemId + "?" + ApiKey+"&filtertype=maptype&filtervalue=PIN");
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Item ID is " + ItemId);
        utilityFileWriteOP.writeToLog(tcid, "Searching the attributes for ", "ItemId "+ItemId,Resultpath);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",Resultpath);
        
        response = httpclient.execute(target, httpget);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),Resultpath);
        
        String jsonresponse = EntityUtils.toString(response.getEntity());
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,Resultpath);
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString(),Resultpath);
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        System.out.println(jarray.size());
        for(int i=0; i<jarray.size(); i++) {
        	
        	jsonobject = jarray.get(i).getAsJsonObject();
	        
        	JsonArray jarrayprices = jsonobject.getAsJsonArray("prices");
		    
		    for(int pricesindex=0; pricesindex<jarrayprices.size(); pricesindex++) {
		    	
		    	JsonObject jsonobjectpricesBody = jarrayprices.get(pricesindex).getAsJsonObject();
	        	
	        	String pricesJsonKey = jsonobjectpricesBody.get("type").toString().replaceAll("^\"|\"$", "");
	        	
	        	JsonArray jarraypricesBody = jsonobjectpricesBody.getAsJsonArray("values");
		        
        		
	        		if(pricesJsonKey.equals("VAT")) {
	        			
	        			
	        			for(int pricesBodyindex=0; pricesBodyindex<jarraypricesBody.size(); pricesBodyindex++) {
			        		
					        JsonObject jsonobjectpricesBodyValue = jarraypricesBody.get(pricesBodyindex).getAsJsonObject();

					        pricesJsonvalue = jsonobjectpricesBodyValue.get("value").toString().replaceAll("^\"|\"$", "");
					     
			        	}	        		
	        	}	        	
		    }
		   
		    Double vatRatevalueDouble = Double.parseDouble(pricesJsonvalue);
		    
		    
		    int vatRateInt = vatRatevalueDouble.intValue();
		    
		    if( (vatRateInt <= 5) && (vatRateInt > 0) ) {

		    	EBSVatDetails = "LOWER";
		    }
		    else

		    	if(vatRateInt >= 20) {

		    		EBSVatDetails = "STANDARD 20";
		    	}
		    	else
		    		if(vatRateInt == 0) {
		    			EBSVatDetails = "ZERO";
		    		}
		    		else
		    		{
		    			EBSVatDetails = "MIXED";
		    		}
		    
		    
		   	     
		    res = EBSVatDetails;
			
			
        	
        }
        	  
       
	} catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e,Resultpath);
        
		res = null;
		return res;
	}
    
    finally{
        
        response.close();
        
}
    
    return res;
    
	
}
/***********************************************************************************************************************
 * Method name : PostCallOrderCreationGeneralSeperateItemDetails
 * Description : to create item details json and validate the post call response
*************************************************************************************************************** */

public static boolean PostCallOrderCreationGeneralSeperateItemDetails(String DriverExcelPath, String ItemDetailsSheetName, String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String MessageType, String ShipToLocationId, String OrderCount,String ShipToDeliverAt,String OrderReferenceCode, String AllOrderAttributes, String AllMessageAttributes, String AllShipToAddressAttributes, String AllBillToAddressAttributes, String AllBillToTaxAddressAttributes, String AllItemAttributes, String TestKeyword, String tcid,String ResultPath,XWPFRun xwfprun,ExtentTest logger) throws IOException {
	
	boolean res = false;
	
	//String ItemID[] = AllItemIDs.split(",");
	//String QuantityOrdered[] = AllQuantitiesOrdered.split(",");
	String OrderAttributes[] = AllOrderAttributes.split(",");
	String MessageAttributes[] = AllMessageAttributes.split(",");
	String ShipToAddressAttributes[] = AllShipToAddressAttributes.split(",");
	String BillToAddressAttributes[] = AllBillToAddressAttributes.split(",");
	String BillToTaxAddressAttributes[] = AllBillToTaxAddressAttributes.split(",");
	//String ItemAttributes[] = AllItemAttributes.split(",");
	
	String ItemDetails = "";
	String Keyword = null;
	String Items = null;
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    int r = 0;
    int occurances = 0;
    
    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpPost httppost = new HttpPost(UrlTail + "?" + ApiKey);
        
        httppost.setConfig(config);
        
        httppost.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Executing request " + httppost.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httppost.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwfprun,"");
        logger.log (LogStatus.INFO, "Executing request " + httppost.getRequestLine() + " to " + target + " via " + proxy);
        Workbook wrk1 = Workbook.getWorkbook(new File(DriverExcelPath));
		Sheet sheet1 = wrk1.getSheet(ItemDetailsSheetName);
		int rows = sheet1.getRows();
		for(r=1; r<rows; r++) {
			Keyword = sheet1.getCell(0, r).getContents().trim();
			
			//if(occurances>0){
		          
		   //if(!(TestKeyword.contentEquals(Keyword))){

			    //	break;
			    	
		    	//}

			// }
			
			if(Keyword.equalsIgnoreCase(TestKeyword)) {
				  
				 occurances=occurances+1;
			
			Items = sheet1.getCell(1, r).getContents().trim();
			
			String[] AllItemsInItemDetails = Items.split(",");
			
			//System.out.println("Number of ItemIDs " + ItemID.length);
			System.out.println("Items with details " + AllItemsInItemDetails.length);
			
			for(int c=2; c< 2 + AllItemsInItemDetails.length; c++) {
				
				String ItemDetailsJson = sheet1.getCell(c, r).getContents().trim();
				
				ItemDetails = ItemDetails + ItemDetailsJson;
	        	
	        	if(c!=AllItemsInItemDetails.length + 1)
	        		ItemDetails = ItemDetails + ",";
			}
			}
			
		}
        
        //String json = "{ \"orders\": {\"orderId\": \"387429-02\", \"orderBuyerPartyId\": \"5013546056078\", \"orderSellerPartyId\": \"5013546229809\", \"orderReferenceCode\": \"Local-Order-3607-20-20170314\",\"messageId\": \"FS-20170811-133400\", \"messageSenderPartyId\": \"5013546056078\", \"messageRecipientPartyId\": \"5013546229809\", \"messageType\": \"MOR\", \"messageCreatedAt\": \"2017-08-11T13:34:00Z\",\"shipToPartyId\": \"MOR-HEEL\", \"shipToLocationId\": \"542\", \"shipToAddressName\": \"Palmer Harvey\", \"shipToAddressLine1\": \"Maxted Road\", \"shipToAddressLine2\": \"--\", \"shipToAddressCity\": \"Hemel Hempstead\", \"shipToAddressState\": \"Herts\", \"shipToAddressPostCode\": \"HP2 7DX\", \"shipToAddressCountryCode\": \"UK\",\"shipToDeliverAt\": \"2017-03-15\", \"shipToDeliverLatestAt\": \"2017-03-15\",\"billToPartyId\": \"5013546056078\", \"billToAddressName\": \"PALMER AND HARVEY MCLANE\", \"billToAddressLine1\": \"P&H HOUSE\", \"billToAddressLine2\": \"DAVIGDOR ROAD\", \"billToAddressCity\": \"HOVE\", \"billToAddressState\": \"EAST SUSSEX\", \"billToAddressPostCode\": \"BN3 1RE\", \"billToAddressCountryCode\": \"UK\",\"billToTaxId\": \"GB232169089\", \"billToTaxAddressName\": \"PALMER AND HARVEY MCLANE\", \"billToTaxAddressLine1\": \"P&H HOUSE\", \"billToTaxAddressLine2\": \"DAVIGDOR ROAD\", \"billToTaxAddressCity\": \"HOVE\", \"billToTaxAddressState\": \"EAST SUSSEX\", \"billToTaxAddressPostCode\": \"BN3 1RE\", \"billToTaxAddressCountryCode\": \"UK\"}, \"items\": [{\"itemId\": \"104466391\", \"itemLineId\": 1, \"itemBaseType\": \"skuMin\", \"itemDescription\": \"M Pork Sausage Rolls 6s\",\"itemAlternateId\": { \"skuMin\": \"104466391\",\"skuLegacy\": \"0617707\", \"phId\": \"53238\" },\"quantityType\": \"CS\", \"quantityOrdered\": 1,\"priceOrderedCurrency\": \"GBP\", \"priceOrderedAmount\": 1.75, \"priceOrderedTaxRate\": 0},{\"itemId\": \"105462210\", \"itemLineId\": 2, \"itemBaseType\": \"skuMin\", \"itemDescription\": \"M Banana Flav Milk 1L\",\"itemAlternateId\": { \"skuMin\": \"105462210\",\"skuLegacy\": \"0403674\", \"phId\": \"53241\" },\"quantityType\": \"CS\", \"quantityOrdered\": 1,\"priceOrderedCurrency\": \"GBP\", \"priceOrderedAmount\": 1.29, \"priceOrderedTaxRate\": 0}] }";
       
        //{\"itemId\": \"" + ItemID[0] + "\",\"itemLineId\": 1,\"itemBaseType\": \"skuMin\",\"itemAlternateId\": {\"skuMin\": \"M104593859\",\"skuPin\": \"P104593867\",\"skuLegacy\": \"L0472851\",\"barcodeAsin\": \"ASIN29384845\",\"barcodeEan\": \"EAN3928445\"},\"itemDescription\": \"Heinz Baked Beans\",\"quantityType\": \"unit\",\"quantityOrdered\": " + QuantityOrdered[0] + ",\"priceOrderedCurrency\": \"GBP\",\"priceOrderedAmount\": 1.00,\"priceOrderedTaxRate\": 20.00},{\"itemId\": \"" + ItemID[1] + "\",\"itemLineId\": 2,\"itemBaseType\": \"skuMin\",\"itemAlternateId\": {\"skuMin\": \"M104593859\",\"skuPin\": \"P103201067\",\"skuLegacy\": \"L0419068\",\"barcodeAsin\": \"ASIN29383445\",\"barcodeEan\": \"EAN39234545\"},\"itemDescription\": \"Walkers Crips\",\"quantityType\": \"unit\",\"quantityOrdered\": " + QuantityOrdered[1] + ",\"priceOrderedCurrency\": \"GBP\",\"priceOrderedAmount\": 2.00,\"priceOrderedTaxRate\": 5.00}
        
        
       // *******JSON updated S*******************
        //String json = "{  \"orders\":{\"orderId\": \"" + OrderID + "\",\"orderBuyerPartyId\": \"" + OrderAttributes[0] + "\",\"orderSellerPartyId\": \"" + OrderAttributes[1] + "\",\"orderReferenceCode\": \"" + OrderAttributes[2] + "\",\"messageId\": \"" + MessageAttributes[0] + "\",\"messageSenderPartyId\": \"" + MessageAttributes[1] + "\",\"messageRecipientPartyId\": \"" + MessageAttributes[2] + "\",\"messageType\": \"" + MessageType + "\",\"messageCreatedAt\": \"" + MessageAttributes[3] + "\",\"shipToPartyId\": \"" + ShipToAddressAttributes[0] + "\",\"shipToLocationId\": \"" + ShipToLocationId + "\",\"shipToAddressName\": \"" + ShipToAddressAttributes[1] + "\",\"shipToAddressLine1\": \"" + ShipToAddressAttributes[2] + "\",\"shipToAddressLine2\": \"" + ShipToAddressAttributes[3] + "\",\"shipToAddressCity\": \"" + ShipToAddressAttributes[4] + "\",\"shipToAddressState\": \"" + ShipToAddressAttributes[5] + "\",\"shipToAddressPostCode\": \"" + ShipToAddressAttributes[6] + "\",\"shipToAddressCountryCode\": \"" + ShipToAddressAttributes[7] + "\",\"shipToDeliverAt\": \"" + ShipToDeliverAt + "\",\"shipToDeliverLatestAt\": \"" + ShipToAddressAttributes[8] + "\",\"shipFromLocationId\": \"" + ShipToAddressAttributes[9] + "\",\"shipFromAddressName\": \"" + ShipToAddressAttributes[10] + "\",\"billToPartyId\": \"" + BillToAddressAttributes[0] + "\",\"billToAddressName\": \"" + BillToAddressAttributes[1] + "\",\"billToAddressLine1\": \"" + BillToAddressAttributes[2] + "\",\"billToAddressLine2\": \"" + BillToAddressAttributes[3] + "\",\"billToAddressCity\": \"" + BillToAddressAttributes[4] + "\",\"billToAddressState\": \"" + BillToAddressAttributes[5] + "\",\"billToAddressPostCode\": \"" + BillToAddressAttributes[6] + "\",\"billToAddressCountryCode\": \"" + BillToAddressAttributes[7] + "\",\"billToTaxId\": \"" + BillToTaxAddressAttributes[0] + "\",\"billToTaxAddressName\": \"" + BillToTaxAddressAttributes[1] + "\",\"billToTaxAddressLine1\": \"" + BillToTaxAddressAttributes[2] + "\",\"billToTaxAddressLine2\": \"" + BillToTaxAddressAttributes[3] + "\",\"billToTaxAddressCity\": \"" + BillToTaxAddressAttributes[4] + "\",\"billToTaxAddressState\": \"" + BillToTaxAddressAttributes[5] + "\",\"billToTaxAddressPostCode\": \"" + BillToTaxAddressAttributes[6] + "\",\"billToTaxAddressCountryCode\": \"" + BillToTaxAddressAttributes[7] + "\"},\"items\": [" + ItemDetails + "]}";
        String json = "{\"orders\":{\"orderId\": \"" + OrderID + "\",\"shipToLocationId\": \"" + ShipToLocationId + "\",\"orderCount\": \"" +OrderCount + "\",\"shipToPartyId\": \"" + ShipToAddressAttributes[0] + "\",\"orderSellerPartyId\": \"" + OrderAttributes[1] + "\",\"billToAddressName\": \"" + BillToAddressAttributes[1] + "\",\"shipToAddressLine1\": \"" + ShipToAddressAttributes[2] + "\",\"shipToAddressCity\": \"" + ShipToAddressAttributes[4] + "\",\"shipToAddressLine2\": \"" + ShipToAddressAttributes[3] + "\",\"billToTaxAddressCity\": \"" + BillToTaxAddressAttributes[4] + "\",\"orderReferenceCode\": \"" + OrderReferenceCode + "\",\"messageType\": \"" + MessageType + "\",\"billToPartyId\": \"" + BillToAddressAttributes[0] + "\",\"shipToAddressPostCode\": \"" + ShipToAddressAttributes[6] + "\",\"billToAddressLine1\": \"" + BillToTaxAddressAttributes[2] + "\",\"billToTaxAddressPostCode\": \"" + BillToTaxAddressAttributes[6] + "\",\"billToAddressLine2\": \"" + BillToAddressAttributes[3] + "\",\"billToAddressPostCode\": \"" + BillToAddressAttributes[6] + "\",\"messageSenderPartyId\": \"" + MessageAttributes[1] + "\",\"shipToAddressState\": \"" + ShipToAddressAttributes[5] + "\",\"shipToDeliverAt\": \"" + ShipToDeliverAt + "\",\"shipToAddressName\": \"" + ShipToAddressAttributes[1] + "\",\"messageCreatedAt\": \"" + MessageAttributes[3] + "\",\"billToAddressState\": \"" + BillToAddressAttributes[5] + "\",\"billToTaxId\": \"" + BillToTaxAddressAttributes[0] + "\",\"messageId\": \"" + MessageAttributes[0] + "\",\"billToAddressCountryCode\": \"" + BillToAddressAttributes[7] + "\",\"billToTaxAddressState\": \"" + BillToTaxAddressAttributes[5] + "\",\"billToTaxAddressCountryCode\": \"" + BillToTaxAddressAttributes[7] + "\",\"messageRecipientPartyId\": \"" + MessageAttributes[2] + "\",\"shipToAddressCountryCode\": \"" + ShipToAddressAttributes[7] + "\",\"orderBuyerPartyId\": \"" + OrderAttributes[0] + "\",\"billToTaxAddressLine1\": \"" + BillToTaxAddressAttributes[2] + "\",\"shipToDeliverLatestAt\": \"" + ShipToAddressAttributes[8] + "\",\"billToTaxAddressLine2\": \"" + BillToTaxAddressAttributes[3] + "\",\"billToAddressCity\": \"" + BillToAddressAttributes[4] + "\",\"billToTaxAddressName\": \"" + BillToTaxAddressAttributes[1] + "\"},\"items\": [" + ItemDetails + "]}";
        //String json = "{  \"orders\":{\"orderId\": \"" + OrderID + "\",\"shipToLocationId\": \"" + ShipToLocationId + "\",\"shipToPartyId\": \"" + ShipToAddressAttributes[0] + "\",\"orderSellerPartyId\": \"" + OrderAttributes[1] + "\",\"billToAddressName\": \"" + BillToAddressAttributes[1] + "\",\"shipToAddressLine1\": \"" + ShipToAddressAttributes[2] + "\",\"shipToAddressCity\": \"" + ShipToAddressAttributes[4] + "\",\"shipToAddressLine2\": \"" + ShipToAddressAttributes[3] + "\",\"billToTaxAddressCity\": \"" + BillToTaxAddressAttributes[4] + "\",\"orderReferenceCode\": \"" + OrderAttributes[2] + "\",\"messageType\": \"" + MessageType + "\",\"billToPartyId\": \"" + BillToAddressAttributes[0] + "\",\"shipToAddressPostCode\": \"" + ShipToAddressAttributes[6] + "\",\"billToAddressLine1\": \"" + BillToTaxAddressAttributes[2] + "\",\"billToTaxAddressPostCode\": \"" + BillToTaxAddressAttributes[6] + "\",\"billToAddressLine2\": \"" + BillToAddressAttributes[3] + "\",\"billToAddressPostCode\": \"" + BillToAddressAttributes[6] + "\",\"messageSenderPartyId\": \"" + MessageAttributes[1] + "\",\"shipToAddressState\": \"" + ShipToAddressAttributes[5] + "\",\"shipToDeliverAt\": \"" + ShipToDeliverAt + "\",\"shipToAddressName\": \"" + ShipToAddressAttributes[1] + "\",\"messageCreatedAt\": \"" + MessageAttributes[3] + "\",\"billToAddressState\": \"" + BillToAddressAttributes[5] + "\",\"billToTaxId\": \"" + BillToTaxAddressAttributes[0] + "\",\"messageId\": \"" + MessageAttributes[0] + "\",\"billToAddressCountryCode\": \"" + BillToAddressAttributes[7] + "\",\"billToTaxAddressState\": \"" + BillToTaxAddressAttributes[5] + "\",\"billToTaxAddressCountryCode\": \"" + BillToTaxAddressAttributes[7] + "\",\"messageRecipientPartyId\": \"" + MessageAttributes[2] + "\",\"shipToAddressCountryCode\": \"" + ShipToAddressAttributes[7] + "\",\"orderBuyerPartyId\": \"" + OrderAttributes[0] + "\",\"billToTaxAddressLine1\": \"" + BillToTaxAddressAttributes[2] + "\",\"shipToDeliverLatestAt\": \"" + ShipToAddressAttributes[8] + "\",\"billToTaxAddressLine2\": \"" + BillToTaxAddressAttributes[3] + "\",\"billToAddressCity\": \"" + BillToAddressAttributes[4] + "\",\"billToTaxAddressName\": \"" + BillToTaxAddressAttributes[1] + "\",\"billToTaxAddressPostCode\": \"" + BillToTaxAddressAttributes[6] + "\",\"billToTaxAddressCountryCode\": \"" + BillToTaxAddressAttributes[7] + "\"},\"items\": [" + ItemDetails + "]}";

        
        // httppost.setEntity(new StringEntity(json, "UTF-8"));
        
        System.out.println(json);
        
        httppost.setEntity(new StringEntity(json));
		
       // httppost.setHeader("apikey", "l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httppost.setHeader("Accept", "application/json");
		httppost.setHeader("Content-type", "application/json");
		
		utilityFileWriteOP.writeToLog(tcid, "Sending the JSON ", "Body :- "+json,ResultPath,xwfprun,"");
		response = httpclient.execute(target, httppost);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code is ", "Displayed as: "+response.getStatusLine().getStatusCode(),ResultPath,xwfprun,"");
        System.out.println(EntityUtils.toString(response.getEntity()));
        utilityFileWriteOP.writeToLog(tcid, "Response Body is ", "Body :- "+EntityUtils.toString(response.getEntity()),ResultPath,xwfprun,"");
        
        //response.toString();
		if(response.getStatusLine().getStatusCode() == 201){
			logger.log(LogStatus.PASS, "Response code for Order is : "+response.getStatusLine().getStatusCode());
			res = true;
		}else{
			logger.log(LogStatus.FAIL, "Response code for Order is : "+response.getStatusLine().getStatusCode());
			res=false;
		}
        
	} catch (Exception e) {
		// TODO: handle exception
		System.out.println(e);
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during POST call ", "Due to :"+e,ResultPath);
        
		res = false;
		return res;
		
	}
    
    finally{
        
        response.close();
        
}
    
    return res;
	
	
}

/***********************************************************************************************************************
 * Method name : GetOrderStatusValidation
 * Description : validate the get call response and check the order status
*************************************************************************************************************** */

public static boolean GetOrderStatusValidation(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String tcid,String ResultPath, XWPFRun xwpfrun,ExtentTest logger) throws IOException {
	
	boolean res = false;
	
	
	String [] shipStatusArrey=statusCurrentValue.split(",");
	
	statusCurrentValue=shipStatusArrey[0];
	String expectedorderReferenceCode = System.getProperty("orderReferenceCode");
	System.out.println(expectedorderReferenceCode);
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "?" + ApiKey);
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        
        
        for (int j=0;j<1;j++){
        
        
       Thread.sleep(5000);
        	
        	
        System.out.println("Order ID is " + OrderID);
        
        
        utilityFileWriteOP.writeToLog(tcid, "Status for ", "Order ID "+OrderID,ResultPath);
        logger.log(LogStatus.INFO, "Order ID: "+OrderID);
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");
        logger.log(LogStatus.INFO, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        response = httpclient.execute(target, httpget);

        
        
        //System.out.println("Response code is "+response.toString());
        
        
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");
        logger.log(LogStatus.INFO, "Response code for the Order ID "+OrderID+" displayed as: "+response.getStatusLine().getStatusCode());
        String jsonresponse = EntityUtils.toString(response.getEntity());
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath,xwpfrun,"");
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        JsonArray jarray = jsonobject.getAsJsonArray("orders");
    	JsonObject jsonobject1=jarray.get(0).getAsJsonObject();
    	String statusCurrent = jsonobject1.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
    	String orderReferenceCode = jsonobject1.get("orderReferenceCode").toString().replaceAll("^\"|\"$", "");
    	System.out.println(orderReferenceCode+" "+expectedorderReferenceCode);
    	if(statusCurrent.equals(statusCurrentValue) && orderReferenceCode.equals(expectedorderReferenceCode)) {
	        	res = true;
				utilityFileWriteOP.writeToLog(tcid, "Orders StatusCurrent is "+statusCurrent+" and orderReferenceCode is "+orderReferenceCode, "Success",ResultPath,xwpfrun,"");
				logger.log(LogStatus.PASS, "Current Order Status is : "+statusCurrent+" and orderReferenceCode is "+orderReferenceCode);
				
				System.out.println("Current Order Status is : " +statusCurrent+" and orderReferenceCode is "+orderReferenceCode);
				
			     break;
			
	        }
	        
	        else{
	        	res = false;
				utilityFileWriteOP.writeToLog(tcid, "Orders StatusCurrent is "+statusCurrent+" Orders orderReferenceCode is "+orderReferenceCode,"FAIL",ResultPath,xwpfrun,"");
				logger.log(LogStatus.FAIL, "Orders StatusCurrent is : "+statusCurrent+" Orders orderReferenceCode is "+orderReferenceCode);
				System.out.println("statusCurrent is " +statusCurrent+" Orders orderReferenceCode is "+orderReferenceCode);
				
				continue;
	        	
	        }
        	
       
        }
        
      
    	
	}  //end of try
    
    
    
    catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e,ResultPath);
        
		res = false;
		
		
		//return res;
	}
    
    finally{
        
     response.close();
     
     return res;
        
}
	
}

/***********************************************************************************************************************
 * Method name : ShipCallOrderShipment_New
 * Description : 
*************************************************************************************************************** */

public static boolean ShipCallOrderShipment_New(String ShipmentURL,String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String MessageType, String ShipToLocationId, String ShipToDeliverAt, String AllItemIDs, String AllQuantitiesOrdered, String tcid,String ResultPath,XWPFRun xwfprun,ExtentTest logger) throws IOException {
	
	boolean res = false;
	
	String ItemID[] = AllItemIDs.split(",");
	String QuantityOrdered[] = AllQuantitiesOrdered.split(",");
	
	String ItemDetails = "";
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpPost httppostship = new HttpPost(ShipmentURL+ OrderID + "?" + ApiKey);
        httppostship.setConfig(config);
        httppostship.addHeader(AuthorizationKey, AuthorizationValue);
        System.out.println("Item length is "+ItemID.length);
        //*****To be updated******
        System.out.println("Executing request " + httppostship.getRequestLine() + " to " + target + " via " + proxy);
        logger.log(LogStatus.INFO, "Executing request " + httppostship.getRequestLine() + " to " + target + " via " + proxy);
        for(int i = 1; i<= ItemID.length; i++) {
        	
        	ItemDetails = ItemDetails + "{\"itemId\": \"" + ItemID[i-1] + "\",\"itemLineId\":\""+i+"\",\"itemBaseType\":\"skuPIN\",\"quantityShipped\": " + QuantityOrdered[i-1] + ",\"quantityType\":\"CS\",\"containerId\": \"39906351\",\"despatchAt\": \"2019-02-22T10:51:00Z\",\"quantityOperator\": \"add\"}";

        	if(i!=ItemID.length)
        		ItemDetails = ItemDetails + ",";
        }
        
//        String json = "{\"id\": \""+OrderID+"\",\"params\": {\"label\": {\"print\": true,\"copies\": 1,\"device\": \"prnt-138493\"}},\"items\": [" + ItemDetails + "]}";
      
        String json = "{\"id\": \""+OrderID+"\",\"params\": {\"source\": \"dhl\"},\"items\": [" + ItemDetails + "]}";
        System.out.println(json);		  
        httppostship.setEntity(new StringEntity(json));
        
        httppostship.setHeader("Accept", "application/json");
		httppostship.setHeader("Content-type", "application/json");
		
		utilityFileWriteOP.writeToLog(tcid, "Sending the JSON ", "Body :- "+json,ResultPath,xwfprun,"");

		response = httpclient.execute(target, httppostship);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code is ", "Displayed as: "+response.getStatusLine().getStatusCode(),ResultPath,xwfprun,"");
        String responsebody = EntityUtils.toString(response.getEntity());
        System.out.println(responsebody);
        
        utilityFileWriteOP.writeToLog(tcid, "Response Body is ", "Body :- "+responsebody,ResultPath,xwfprun,"");
        
        if(response.getStatusLine().getStatusCode() == 202){
			res = true;
        	logger.log(LogStatus.PASS, "Expected Response Code is 202. Actual Response Code is: "+response.getStatusLine().getStatusCode());
        }else{
			res=false;
			logger.log(LogStatus.FAIL, "Expected Response Code is 202. Actual Response Code is: "+response.getStatusLine().getStatusCode());
        }
	} catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
		System.out.println(e);
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during POST call ", "Due to :"+e,ResultPath);
		logger.log(LogStatus.PASS, "Error occurred during POST call: "+e);
		res = false;
		return res;
	}
    finally{
    	response.close();
    }
    
    return res;
}

/***********************************************************************************************************************
 * Method name : ShipCallOrderShipment_NewClosure
 * Description : 
*************************************************************************************************************** */
public static boolean ShipCallOrderShipment_NewClosure(String ShipmentURL,String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String MessageType, String ShipToLocationId, String ShipToDeliverAt, String AllItemIDs, String AllQuantitiesOrdered, String tcid,String ResultPath,XWPFRun xwfprun,ExtentTest logger) throws IOException {
	
	boolean res = false;
	
	String ItemID[] = AllItemIDs.split(",");
	String QuantityOrdered[] = AllQuantitiesOrdered.split(",");
	
	String ItemDetails = "";
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        
        HttpPost httppostship = new HttpPost(ShipmentURL + OrderID + "?" + ApiKey);
        
        httppostship.setConfig(config);
        
        httppostship.addHeader(AuthorizationKey, AuthorizationValue);
        
        
        System.out.println("Item length is "+ItemID.length);
        
        
        //*****To be updated******
        System.out.println("Executing request " + httppostship.getRequestLine() + " to " + target + " via " + proxy);
        logger.log(LogStatus.INFO, "Executing request " + httppostship.getRequestLine() + " to " + target + " via " + proxy);
        for(int i = 1; i<= ItemID.length; i++) {
        	
        	        	
        	ItemDetails = ItemDetails + "{\"itemId\": \"COMPLETE\",\"itemLineId\":\"999\",\"itemBaseType\":\"skuPIN\",\"quantityShipped\": \"999\",\"quantityType\":\"CS\",\"containerId\": \"CONTAINER\",\"quantityOperator\": \"add\"}";

        	if(i!=ItemID.length)
        		ItemDetails = ItemDetails + ",";
        }
        
//        String json = "{\"id\": \""+OrderID+"\",\"params\": {\"label\": {\"print\": true,\"copies\": 1,\"device\": \"prnt-138493\"}},\"items\": [" + ItemDetails + "]}";
      
        String OrderID1 =OrderID+1;
        
        System.out.println("Updated order id while closure"+OrderID1);
        String json = "{\"id\": \""+OrderID1+"\",\"params\": {\"source\": \"dhl\"},\"items\": [" + ItemDetails + "]}";
        System.out.println(json);		  
        httppostship.setEntity(new StringEntity(json));
        httppostship.setHeader("Accept", "application/json");
		httppostship.setHeader("Content-type", "application/json");
		
		utilityFileWriteOP.writeToLog(tcid, "Sending the JSON ", "Body :- "+json,ResultPath,xwfprun,"");
		response = httpclient.execute(target, httppostship);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code is ", "Displayed as: "+response.getStatusLine().getStatusCode(),ResultPath,xwfprun,"");
        String responsebody = EntityUtils.toString(response.getEntity());
        System.out.println(responsebody);
        
        utilityFileWriteOP.writeToLog(tcid, "Response Body is ", "Body :- "+responsebody,ResultPath,xwfprun,"");
       //logger.log(LogStatus.INFO, "Item details present in the JSON file - "+responsebody);
        
        if(response.getStatusLine().getStatusCode() == 202){
        	logger.log(LogStatus.PASS, "Expected Response Code is 202. Actual Response Code is: "+response.getStatusLine().getStatusCode());
        	res = true;
        }else{
        	logger.log(LogStatus.FAIL, "Expected Response Code is 202. Actual Response Code is: "+response.getStatusLine().getStatusCode());
			res=false;
        }
	} catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
		System.out.println(e);
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during POST call ", "Due to :"+e,ResultPath);
        
		res = false;
		return res;
	}
    finally{
    	response.close();
    }
    
    return res;
}



public static boolean getEDNOrderStatus(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String tcid,String ResultPath, XWPFRun xwpfrun,ExtentTest logger) throws IOException {
	
	boolean res = false;
	
	
	String [] shipStatusArrey=statusCurrentValue.split(",");
	
	statusCurrentValue=shipStatusArrey[0];
	String expectedorderReferenceCode = System.getProperty("orderReferenceCode");
	System.out.println(expectedorderReferenceCode);
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpGet httpget = new HttpGet(UrlTail + OrderID + "?" + ApiKey);
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        
 
        for (int j=0;j<1;j++){
        
        
       Thread.sleep(5000);
        	
        	
        System.out.println("Order ID is " + OrderID);
        
       utilityFileWriteOP.writeToLog(tcid, "Status for ", "Order ID "+OrderID,ResultPath);
        logger.log(LogStatus.INFO, "Order ID: "+OrderID);
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");
        logger.log(LogStatus.INFO, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        response = httpclient.execute(target, httpget);

        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");
        logger.log(LogStatus.INFO, "EDN Response code displayed as: "+response.getStatusLine().getStatusCode());
        String jsonresponse = EntityUtils.toString(response.getEntity());
        System.out.println(jsonresponse);
        
        
        if(statusCurrentValue.equalsIgnoreCase("Confirmed")&&response.getStatusLine().getStatusCode()==404)
        {
        	res = true;
			utilityFileWriteOP.writeToLog(tcid, "Status of the order is Confirmed . EDN Response code validated successfully", "Success",ResultPath,xwpfrun,"");
			logger.log(LogStatus.PASS, "Status of the order is Confirmed. EDN Response code validated successfully and response code is "+response.getStatusLine().getStatusCode());
			break;
        }
        
        else if(statusCurrentValue.equalsIgnoreCase("Shipped")&&response.getStatusLine().getStatusCode()==200)
        {
        	res = true;
			utilityFileWriteOP.writeToLog(tcid, "Status of the order is Shipped .EDN Response code validated successfully", "Success",ResultPath,xwpfrun,"");
			logger.log(LogStatus.PASS, "Status of the order is Shipped .EDN Response code validated successfully and response code is "+response.getStatusLine().getStatusCode());
			break;
        }
        else
        {
        	res=false;
        	utilityFileWriteOP.writeToLog(tcid, "EDN Response code not validated", "FAIL",ResultPath,xwpfrun,"");
			logger.log(LogStatus.FAIL, "EDN Response code not validated successfully and response code is "+response.getStatusLine().getStatusCode());
			
			
        }
        
        
        
        
        
        /*utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath,xwpfrun,"");
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        JsonArray jarray = jsonobject.getAsJsonArray("orders");
    	JsonObject jsonobject1=jarray.get(0).getAsJsonObject();
    	String statusCurrent = jsonobject1.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
    	String orderReferenceCode = jsonobject1.get("orderReferenceCode").toString().replaceAll("^\"|\"$", "");
    	System.out.println(orderReferenceCode+" "+expectedorderReferenceCode);
    	if(statusCurrent.equals(statusCurrentValue) && orderReferenceCode.equals(expectedorderReferenceCode)) {
	        	res = true;
				utilityFileWriteOP.writeToLog(tcid, "Orders StatusCurrent is "+statusCurrent+" Orders orderReferenceCode is "+orderReferenceCode, "Success",ResultPath,xwpfrun,"");
				logger.log(LogStatus.PASS, "Orders StatusCurrent is : "+statusCurrent+" Orders orderReferenceCode is "+orderReferenceCode);
				
				System.out.println("Orders statusCurrent is " +statusCurrent+" Orders orderReferenceCode is "+orderReferenceCode);
				
			     break;
			
	        }
	        
	        else{
	        	res = false;
				utilityFileWriteOP.writeToLog(tcid, "Orders StatusCurrent is "+statusCurrent+" Orders orderReferenceCode is "+orderReferenceCode,"FAIL",ResultPath,xwpfrun,"");
				logger.log(LogStatus.FAIL, "Orders StatusCurrent is : "+statusCurrent+" Orders orderReferenceCode is "+orderReferenceCode);
				System.out.println("statusCurrent is " +statusCurrent+" Orders orderReferenceCode is "+orderReferenceCode);
				
				continue;
	        	
	        }*/
        	
       
        }
        
      
    	
	}  //end of try
    
    
    
    catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e,ResultPath);
        
		res = false;
		
		
		//return res;
	}
    
    finally{
        
     response.close();
     
     return res;
        
}
	
}

public static String GetCallItemDetailsStatusValidation(String CatalogueURL,String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String ItemId, String itemBaseType, String quantityType, String itemLineId, String quantityOrdered, String tcid, String Resultpath, ExtentTest logger) throws IOException {
	
	String res = null;
	
	int MaxwaitingTime = 60000;
	
	int regularWaitingTime = 10000;
	
	HashMap<String, String> mapofMaps = new HashMap<String, String>();
	HashMap<String, String> mapofPrices = new HashMap<String, String>();

	String priceOrderedCurrency = null;
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        //https://sit-api.morrisons.com/wholesaleorder/v1/customers/mccolls/catalogues/main/items/101743031?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36&filtertype=maptype&filtervalue=PIN
        
        HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/mccolls/catalogues/main/items/" + ItemId + "?" + ApiKey+"&filtertype=maptype&filtervalue=PIN");
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Item ID is " + ItemId);
        utilityFileWriteOP.writeToLog(tcid, "Searching the attributes for ", "Item ID "+ItemId,Resultpath);
        
        logger.log(LogStatus.INFO, "Searching the attributes for Item ID: "+ItemId);
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",Resultpath);
        
     /*   for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
        	
        	Thread.sleep(regularWaitingTime);*/
        
        response = httpclient.execute(target, httpget);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),Resultpath);
        logger.log(LogStatus.INFO, "Response code is displayed as: "+response.getStatusLine().getStatusCode());
        String jsonresponse = EntityUtils.toString(response.getEntity());
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,Resultpath);
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString(),Resultpath);
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        System.out.println(jarray.size());
        for(int i=0; i<jarray.size(); i++) {
        	
        	jsonobject = jarray.get(i).getAsJsonObject();
        	
        	String description = jsonobject.get("description").toString().replaceAll("^\"|\"$", "");
        	
        	//String mapsJsonBody = jsonobject.get("maps").toString().replaceAll("^\"|\"$", "");
	        JsonArray jarrayMaps = jsonobject.getAsJsonArray("maps");
	        
	        for(int mapsindex=0; mapsindex<jarrayMaps.size(); mapsindex++) {
	        	
	        	JsonObject jsonobjectMapsBody = jarrayMaps.get(mapsindex).getAsJsonObject();
	        	
	        	String mapJsonKey = jsonobjectMapsBody.get("type").toString().replaceAll("^\"|\"$", "");
	        	
		        JsonArray jarrayMapsBody = jsonobjectMapsBody.getAsJsonArray("values");
		        
		        JsonObject jsonobjectMapsBodyValue = jarrayMapsBody.get(0).getAsJsonObject();
		        
		        String mapJsonvalue = jsonobjectMapsBodyValue.get("value").toString().replaceAll("^\"|\"$", "");
		        
		        mapofMaps.put(mapJsonKey, mapJsonvalue);

	        	
	        }
	        
		    JsonArray jarrayPrices = jsonobject.getAsJsonArray("prices");

	        
	        for(int pricesindex=0; pricesindex<jarrayPrices.size(); pricesindex++) {
	        	
	        	JsonObject jsonobjectPricesBody = jarrayPrices.get(pricesindex).getAsJsonObject();
	        	
	        	String pricesJsonKey = jsonobjectPricesBody.get("type").toString().replaceAll("^\"|\"$", "");
	        	
		        JsonArray jarrayPricesBody = jsonobjectPricesBody.getAsJsonArray("values");
		        
		        JsonObject jsonobjectPricesBodyValue = jarrayPricesBody.get(0).getAsJsonObject();
		        
		        String pricesJsonvalue = jsonobjectPricesBodyValue.get("value").toString().replaceAll("^\"|\"$", "");
		        
		        mapofPrices.put(pricesJsonKey, pricesJsonvalue);
		        
		        if(pricesJsonKey.equals("WSP")) {
		        	
			        priceOrderedCurrency = jsonobjectPricesBodyValue.get("currency").toString().replaceAll("^\"|\"$", "");
			        mapofPrices.put("priceOrderedCurrency", priceOrderedCurrency);

		        	
		        }

	        	
	        }
	        
	       	System.out.println(mapofMaps.size());
		    System.out.println(mapofPrices.size());
		    
		    String itemDetailsbody = "{\"quantityType\": \"" + quantityType + "\",\"itemId\": \"" + ItemId +"\",\"itemAlternateId\": {\"clientId\": \"" + mapofMaps.get("CLIENTID") +"\",\"barcodeEan\": \"" + mapofMaps.get("EAN") +"\",\"skuPin\": \"" + mapofMaps.get("PIN") +"\"},\"priceOrderedAmount\": " + mapofPrices.get("WSP") + ",\"priceOrderedCurrency\": \"" + priceOrderedCurrency + "\",\"itemBaseType\": \"" + itemBaseType + "\",\"itemLineId\": \"" + itemLineId + "\",\"quantityOrdered\": " + quantityOrdered + ",\"priceOrderedTaxRate\": " + mapofPrices.get("VAT") + ",\"itemDescription\": \"" + description + "\",\"itemCaseSize\": " + mapofPrices.get("CS").substring(0, mapofPrices.get("CS").indexOf(".")) + "}";
		    	    
		    System.out.println(itemDetailsbody);	    
		    	     
		    res = itemDetailsbody;
			
			
        	
        }
        	  
       
        
       // }
    	
	} catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e,Resultpath);
        
		res = null;
		return res;
	}
    
    finally{
        
        response.close();
        
}
    
    return res;
    
	
}


/*public static boolean GetCallItemStatusValidation(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String tcid,String ResultPath,XWPFRun xwpfrun, ExtentTest logger) throws IOException {
	
	boolean res = false;
	
	int MaxwaitingTime = 60000;
	
	int regularwaitTime = 10000;
	String expectedorderReferenceCode = System.getProperty("orderReferenceCode");
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Order ID is " + OrderID);
        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");
        logger.log(LogStatus.INFO, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
       
        
        
        for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
        	
        	
        	Thread.sleep(regularwaitTime);
        
        response = httpclient.execute(target, httpget);
        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");
        logger.log(LogStatus.INFO, "Response code displayed is: " + response.getStatusLine().getStatusCode());
        String jsonresponse = EntityUtils.toString(response.getEntity());
        
        
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath,xwpfrun,"");
        	
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        
        int stCnt=jarray.size();
        
        int mtchCnt=0;
        
        System.out.println(jarray.size());
        for(int i=0; i<jarray.size(); i++) {
        	
        	jsonobject = jarray.get(i).getAsJsonObject();
	        String statusValidationResult = jsonobject.get("statusValidation").toString();
	        
	        String statusCurrent = jsonobject.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
	    	String orderReferenceCode = jsonobject.get("orderReferenceCode").toString().replaceAll("^\"|\"$", "");
	    	String priceConfirmedTaxRate = jsonobject.get("priceConfirmedTaxRate").toString().replaceAll("^\"|\"$", "");

	        System.out.println("StatusValidation Result is " + statusValidationResult);
	        
	        utilityFileWriteOP.writeToLog(tcid, "StatusValidation Result is " + statusValidationResult, "",ResultPath,xwpfrun,"");
	        logger.log(LogStatus.INFO, "StatusValidation Result is : " + statusValidationResult);
	        int countOne = statusValidationResult.split("1").length - 1;
	        
	        if(countOne == 10) {
	        	res = true;
	        	System.out.println("Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				utilityFileWriteOP.writeToLog(tcid, "StatusValidation is successful ", "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				logger.log(LogStatus.PASS, "StatusValidation is successful");
				System.out.println("Count of 1 is 10");
	        }
	        else {
	        	res = false;
				utilityFileWriteOP.writeToLog(tcid, "StatusValidation Failed ", "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
	        	logger.log(LogStatus.FAIL, "StatusValidation Failed");
	        	//break;
	        }
	        
	        if(statusCurrent.equals(statusCurrentValue)) {
	        	res = true;
				utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent, "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath,xwpfrun,"");
				System.out.println("statusCurrent is " +statusCurrent);
				logger.log(LogStatus.PASS, "StatusCurrent is "+statusCurrent+". Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				mtchCnt=mtchCnt+1;
				
				
			//break;
	        }
	        
	        else{
	        	res = false;
				utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent, "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath,xwpfrun,"");
				logger.log(LogStatus.FAIL, "StatusCurrent is "+statusCurrent+". Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				System.out.println("statusCurrent is " +statusCurrent);
	        	
	        }
	        
	        if(orderReferenceCode.equals(expectedorderReferenceCode)) {
	        	res = true;
				utilityFileWriteOP.writeToLog(tcid, "orderReferenceCode is "+orderReferenceCode, "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath,xwpfrun,"");
				System.out.println("orderReferenceCode is " +orderReferenceCode);
				logger.log(LogStatus.PASS, "orderReferenceCode is "+orderReferenceCode+". Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				//mtchCnt=mtchCnt+1;
				
				
			//break;
	        }
	        
	        else{
	        	res = false;
				utilityFileWriteOP.writeToLog(tcid, "orderReferenceCode is "+orderReferenceCode, "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath,xwpfrun,"");
				logger.log(LogStatus.FAIL, "orderReferenceCode is "+orderReferenceCode+". Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				System.out.println("orderReferenceCode is " +orderReferenceCode);
	        	
	        }
	        
	        String ExpectedVatrate = GetCallRawVatAmountFromWebPortal(ProxyHostName, ProxyPort, SYSUserName, SYSPassWord, TargetHostName, TargetPort, TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue, jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""), tcid, ResultPath, xwpfrun);
	        if(priceConfirmedTaxRate.equals(ExpectedVatrate)) {
	        	res = true;
				utilityFileWriteOP.writeToLog(tcid, "priceConfirmedTaxRate is "+priceConfirmedTaxRate, "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath,xwpfrun,"");
				System.out.println("priceConfirmedTaxRate is " +priceConfirmedTaxRate);
				logger.log(LogStatus.PASS, "priceConfirmedTaxRate is "+priceConfirmedTaxRate+". Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				//mtchCnt=mtchCnt+1;
				
				
			//break;
	        }
	        
	        else{
	        	res = false;
				utilityFileWriteOP.writeToLog(tcid, "priceConfirmedTaxRate is "+priceConfirmedTaxRate, "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath,xwpfrun,"");
				logger.log(LogStatus.FAIL, "priceConfirmedTaxRate is "+priceConfirmedTaxRate+". Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				System.out.println("priceConfirmedTaxRate is " +priceConfirmedTaxRate);
	        	
	        }
	        
	        
	        
        }
        	  
       
        if(mtchCnt==stCnt) {
        	break;
        
        }
        
      }
    	
} 
    
    
catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
        
		res = false;
		
		
	}
    
    finally{
        
        response.close();
        
        return res;
        
}
    
  
    
	
}*/
	




public static String GetCallRawVatAmountFromWebPortal(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String ItemId, String tcid, String Resultpath, XWPFRun xwpfrun) throws IOException {
	
	String res = null;
	
	//String EBSVatDetails = null;
	
	String pricesJsonvalue = null;
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        //https://sit-api.morrisons.com/wholesaleorder/v1/customers/mccolls/catalogues/main/items/101743031?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36&filtertype=maptype&filtervalue=PIN
        
        HttpGet httpget = new HttpGet(UrlTail + ItemId + "?" + ApiKey+"&filtertype=maptype&filtervalue=PIN");
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Item ID is " + ItemId);
       // utilityFileWriteOP.writeToLog(tcid, "Searching the attributes for ", "ItemId "+ItemId,Resultpath);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        //utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",Resultpath);
        
        response = httpclient.execute(target, httpget);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
     //   utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),Resultpath);
        
        String jsonresponse = EntityUtils.toString(response.getEntity());
        System.out.println(jsonresponse);
      //  utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,Resultpath);
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
       // utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString(),Resultpath);
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        System.out.println(jarray.size());
        for(int i=0; i<jarray.size(); i++) {
        	
        	jsonobject = jarray.get(i).getAsJsonObject();
	        
        	JsonArray jarrayprices = jsonobject.getAsJsonArray("prices");
		    
		    for(int pricesindex=0; pricesindex<jarrayprices.size(); pricesindex++) {
		    	
		    	JsonObject jsonobjectpricesBody = jarrayprices.get(pricesindex).getAsJsonObject();
	        	
	        	String pricesJsonKey = jsonobjectpricesBody.get("type").toString().replaceAll("^\"|\"$", "");
	        	
	        	JsonArray jarraypricesBody = jsonobjectpricesBody.getAsJsonArray("values");
		        
        		
	        		if(pricesJsonKey.equals("VAT")) {
	        			
	        			
	        			for(int pricesBodyindex=0; pricesBodyindex<jarraypricesBody.size(); pricesBodyindex++) {
			        		
					        JsonObject jsonobjectpricesBodyValue = jarraypricesBody.get(pricesBodyindex).getAsJsonObject();

					        pricesJsonvalue = jsonobjectpricesBodyValue.get("value").toString().replaceAll("^\"|\"$", "");
					     
			        	}	        		
	        	}	        	
		    }
		   
		    Double vatRatevalueDouble = Double.parseDouble(pricesJsonvalue);
		    
		    
		    int vatRateInt = vatRatevalueDouble.intValue();
		    
		    /*if( (vatRateInt <= 5) && (vatRateInt > 0) ) {

		    	EBSVatDetails = "LOWER";
		    }
		    else

		    	if(vatRateInt >= 20) {

		    		EBSVatDetails = "STANDARD 20";
		    	}
		    	else
		    		if(vatRateInt == 0) {
		    			EBSVatDetails = "ZERO";
		    		}
		    		else
		    		{
		    			EBSVatDetails = "MIXED";
		    		}
		    */
		    
		   	     
		    res = String.valueOf(vatRateInt);
			
			
        	
        }
        	  
       
	} catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		//utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e,Resultpath);
        
		res = null;
		return res;
	}
    
    finally{
        
        response.close();
        
}
    
    return res;
    
	
}



public static boolean GetCallItemStatusValidation(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String CatalogueURL,String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String tcid,String ResultPath,XWPFRun xwpfrun, ExtentTest logger) throws IOException {
	
	boolean res = false;
	
	int MaxwaitingTime = 10000;
	
	int regularwaitTime = 10000;
	String expectedorderReferenceCode = System.getProperty("orderReferenceCode");
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Order ID is " + OrderID);
        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID,ResultPath);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");
        logger.log(LogStatus.INFO, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
       
        
        
        
//        for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
        for(int td = 10000; td<= MaxwaitingTime; td = td+ 10000) {
        	
        	
        	Thread.sleep(regularwaitTime);
        
        response = httpclient.execute(target, httpget);
        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");
        logger.log(LogStatus.INFO, "Response code for item details validation displayed is: " + response.getStatusLine().getStatusCode());
        String jsonresponse = EntityUtils.toString(response.getEntity());
        
        
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath,xwpfrun,"");
        	
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString(),ResultPath);
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        
        int stCnt=jarray.size();
        
        int mtchCnt=0;
        
        System.out.println(jarray.size());
        for(int i=0; i<jarray.size(); i++) {
        	
        	jsonobject = jarray.get(i).getAsJsonObject();
	        String statusValidationResult = jsonobject.get("statusValidation").toString();
	        
	        String statusCurrent = jsonobject.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
	    	String orderReferenceCode = jsonobject.get("orderReferenceCode").toString().replaceAll("^\"|\"$", "");
	    	String priceConfirmedTaxRate = jsonobject.get("priceConfirmedTaxRate").toString().replaceAll("^\"|\"$", "");
	    	String quantityType = jsonobject.get("quantityType").toString().replaceAll("^\"|\"$", "");
	    	String priceConfirmedAmount = jsonobject.get("priceConfirmedAmount").toString().replaceAll("^\"|\"$", "");
	    	String priceConfirmedEachesAmount = jsonobject.get("priceConfirmedEachesAmount").toString().replaceAll("^\"|\"$", "");

	        System.out.println("StatusValidation Result is " + statusValidationResult);
	        
	        utilityFileWriteOP.writeToLog(tcid, "StatusValidation String is " + statusValidationResult, "",ResultPath,xwpfrun,"");
	        logger.log(LogStatus.INFO, "Status Validation String is : " + statusValidationResult);
	        int countOne = statusValidationResult.split("1").length - 1;
	        
	        if(countOne == 10) {
	        	res = true;
	        	System.out.println("Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				utilityFileWriteOP.writeToLog(tcid, "Status Validation string is displayed as expected. ", "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath);
				logger.log(LogStatus.PASS, "Status Validation string is displayed as expected");
				System.out.println("Count of 1 is 10");
	        }
	        else {
	        	res = false;
				utilityFileWriteOP.writeToLog(tcid, "Status Validation Failed ", "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath);
	        	logger.log(LogStatus.FAIL, "Status Validation Failed");
	        	//break;
	        }
	        
	        if(statusCurrent.equals(statusCurrentValue)) {
	        	res = true;
				utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent, "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath,xwpfrun,"");
				System.out.println("statusCurrent is " +statusCurrent);
				logger.log(LogStatus.PASS, "Current status is "+statusCurrent+". Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				mtchCnt=mtchCnt+1;
				
				
			//break;
	        }
	        
	        else{
	        	res = false;
				utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent, "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath,xwpfrun,"");
				logger.log(LogStatus.FAIL, "StatusCurrent is "+statusCurrent+". Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				System.out.println("statusCurrent is " +statusCurrent);
	        	
	        }
	        
	        if(orderReferenceCode.equals(expectedorderReferenceCode)) {
	        	res = true;
				utilityFileWriteOP.writeToLog(tcid, "Order Reference Code is "+orderReferenceCode, "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath,xwpfrun,"");
				System.out.println("orderReferenceCode is " +orderReferenceCode);
				logger.log(LogStatus.PASS, "order Reference Code is "+orderReferenceCode+". Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				//mtchCnt=mtchCnt+1;
				
				
			//break;
	        }
	        
	        else{
	        	res = false;
				utilityFileWriteOP.writeToLog(tcid, "orderReferenceCode is "+orderReferenceCode, "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath,xwpfrun,"");
				logger.log(LogStatus.FAIL, "orderReferenceCode is "+orderReferenceCode+". Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				System.out.println("orderReferenceCode is " +orderReferenceCode);
	        	
	        }
	        
	        if(CatalogueURL.contains("sandpiper"))
	        {
	       // String ExpectedVatrate = GetCallRawVatAmountFromWebPortal(ProxyHostName, ProxyPort, SYSUserName, SYSPassWord, TargetHostName, TargetPort, TargetHeader, CatalogueURL, ApiKey, AuthorizationKey, AuthorizationValue, jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""), tcid, ResultPath, xwpfrun);
	        priceConfirmedTaxRate.equals("0.00");
	        	res = true;
				utilityFileWriteOP.writeToLog(tcid, "Price Confirmed Tax Rate is "+priceConfirmedTaxRate, "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath,xwpfrun,"");
				System.out.println("priceConfirmedTaxRate is " +priceConfirmedTaxRate);
				logger.log(LogStatus.PASS, "Price Confirmed Tax Rate is "+priceConfirmedTaxRate+". Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				//mtchCnt=mtchCnt+1;
		
	        }
	        else
	        {
	        	String ExpectedVatrate = GetCallRawVatAmountFromWebPortal(ProxyHostName, ProxyPort, SYSUserName, SYSPassWord, TargetHostName, TargetPort, TargetHeader, CatalogueURL, ApiKey, AuthorizationKey, AuthorizationValue, jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""), tcid, ResultPath, xwpfrun);
		        if(priceConfirmedTaxRate.equals(ExpectedVatrate)) {
		        	res = true;
					utilityFileWriteOP.writeToLog(tcid, "priceConfirmedTaxRate is "+priceConfirmedTaxRate, "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath,xwpfrun,"");
					System.out.println("priceConfirmedTaxRate is " +priceConfirmedTaxRate);
					logger.log(LogStatus.PASS, "priceConfirmedTaxRate is "+priceConfirmedTaxRate+". Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
					//mtchCnt=mtchCnt+1;
					
					
				//break;
		        }
		        
		        else{
		        	res = false;
					utilityFileWriteOP.writeToLog(tcid, "priceConfirmedTaxRate is "+priceConfirmedTaxRate, "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath,xwpfrun,"");
					logger.log(LogStatus.FAIL, "priceConfirmedTaxRate is "+priceConfirmedTaxRate+". Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
					System.out.println("priceConfirmedTaxRate is " +priceConfirmedTaxRate);
		        	
		        }
	        }
	        
	        if(quantityType.equals("CS")) {
	        	
	        	String WSCPRate=GetCallWSCPorWSPFromWebPortal(ProxyHostName, ProxyPort, SYSUserName, SYSPassWord, TargetHostName, TargetPort, TargetHeader, CatalogueURL, ApiKey, AuthorizationKey, AuthorizationValue, jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""), quantityType,tcid, ResultPath, xwpfrun, logger);
	        	
	        	if(priceConfirmedAmount.equals(WSCPRate)) {
	        		res = true;
					utilityFileWriteOP.writeToLog(tcid, "Price Confirmed Amount is "+priceConfirmedAmount+", WSCP Rate is"+WSCPRate, "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath,xwpfrun,"");
					System.out.println("priceConfirmedAmount is " +priceConfirmedAmount+", WSCP Rate is"+WSCPRate);
					logger.log(LogStatus.PASS, "Price Confirmed Amount is "+priceConfirmedAmount+",WSCP Rate is"+WSCPRate+". Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
					//mtchCnt=mtchCnt+1;
					
					
				//break;
	        	}     
	        	else{
	        		res = false;
					utilityFileWriteOP.writeToLog(tcid, "priceConfirmedAmount is "+priceConfirmedAmount+", WSCP Rate is"+WSCPRate, "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath,xwpfrun,"");
					System.out.println("priceConfirmedAmount is " +priceConfirmedAmount+", WSCP Rate is"+WSCPRate);
					logger.log(LogStatus.PASS, "priceConfirmedAmount is "+priceConfirmedAmount+",WSCP Rate is"+WSCPRate+". Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
					
		        }
	        }
	        if(quantityType.equals("EA")) {
	        	String WSPRate=GetCallWSCPorWSPFromWebPortal(ProxyHostName, ProxyPort, SYSUserName, SYSPassWord, TargetHostName, TargetPort, TargetHeader, CatalogueURL, ApiKey, AuthorizationKey, AuthorizationValue, jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""), quantityType,tcid, ResultPath, xwpfrun, logger);
	        	
	        	if(priceConfirmedEachesAmount.equals(WSPRate)) {
	        		res = true;
					utilityFileWriteOP.writeToLog(tcid, "WSP Rate is"+WSPRate+", priceConfirmedEachesAmount is"+priceConfirmedEachesAmount, "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath,xwpfrun,"");
					System.out.println("WSP Rate is"+WSPRate+", priceConfirmedEachesAmount is"+priceConfirmedEachesAmount);
					logger.log(LogStatus.PASS, "WSP Rate is"+WSPRate+", priceConfirmedEachesAmount is"+priceConfirmedEachesAmount+". Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
					//mtchCnt=mtchCnt+1;
					
					
				//break;
	        	}
	        	else{
	        		res = false;
					utilityFileWriteOP.writeToLog(tcid, "priceConfirmedAmount is "+priceConfirmedAmount+", priceConfirmedEachesAmount is"+priceConfirmedEachesAmount, "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath,xwpfrun,"");
					System.out.println("priceConfirmedAmount is " +priceConfirmedAmount+", priceConfirmedEachesAmount is"+priceConfirmedEachesAmount);
					logger.log(LogStatus.PASS, "priceConfirmedAmount is "+priceConfirmedAmount+", priceConfirmedEachesAmount is"+priceConfirmedEachesAmount+". Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
					
		        }
	        }
	        
	        
	        
	        
	        
        }
        	  
       
        if(mtchCnt==stCnt) {
        	break;
        
        }
        
      }
    	
} 
    
    
catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e,ResultPath);
        
		res = false;
		
		
	}
    
    finally{
        
        response.close();
        
        return res;
        
}
    
  
    
	
}

public static boolean GetCallItemLevelRejectionStatusValidation(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String tcid,String ResultPath,XWPFRun xwpfrun, ExtentTest logger) throws IOException, InterruptedException {
	
	boolean res = false;
	Thread.sleep(6000);
	int MaxwaitingTime = 10000;
	
	int regularwaitTime = 10000;
	String expectedorderReferenceCode = System.getProperty("orderReferenceCode");
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Order ID is " + OrderID);
        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID,ResultPath);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");
        logger.log(LogStatus.INFO, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
       
        
        
        for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
        	
        	
        	Thread.sleep(regularwaitTime);
        
        response = httpclient.execute(target, httpget);
        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");
        logger.log(LogStatus.INFO, "Response code displayed is: " + response.getStatusLine().getStatusCode());
        String jsonresponse = EntityUtils.toString(response.getEntity());
        
        
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath,xwpfrun,"");
        	
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
        
        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString(),ResultPath);
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        
        int stCnt=jarray.size();
        
        int mtchCnt=0;
        
        System.out.println(jarray.size());
        for(int i=0; i<jarray.size(); i++) {
        	
        	jsonobject = jarray.get(i).getAsJsonObject();
	        String statusValidationResult = jsonobject.get("statusValidation").toString();
	        
	        String statusCurrent = jsonobject.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
	    	String orderReferenceCode = jsonobject.get("orderReferenceCode").toString().replaceAll("^\"|\"$", "");
	    	String priceConfirmedTaxRate = jsonobject.get("priceConfirmedTaxRate").toString().replaceAll("^\"|\"$", "");
	    	String quantityType = jsonobject.get("quantityType").toString().replaceAll("^\"|\"$", "");
	    	String priceConfirmedAmount = jsonobject.get("priceConfirmedAmount").toString().replaceAll("^\"|\"$", "");
	    	String priceConfirmedEachesAmount = jsonobject.get("priceConfirmedEachesAmount").toString().replaceAll("^\"|\"$", "");

	        System.out.println("StatusValidation Result is " + statusValidationResult);
	        
	        utilityFileWriteOP.writeToLog(tcid, "StatusValidation Result is " + statusValidationResult, "",ResultPath,xwpfrun,"");
	        logger.log(LogStatus.INFO, "StatusValidation Result is : " + statusValidationResult);
	       
	        if(statusValidationResult.contains("active:0")||
	        		statusValidationResult.contains("orderable:0")||
	        		statusValidationResult.contains("category:0")||
	        		statusValidationResult.contains("case:0")) {
	        	res = true;
	        	System.out.println("Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				utilityFileWriteOP.writeToLog(tcid, "Validate Item level rejection for Shipped order", "Item level rejection validate successfully.Status validation string is : "+statusValidationResult,ResultPath);
				logger.log(LogStatus.PASS, "Item level rejection validate successfully.Status validation string is : "+statusValidationResult);
				
	        }
	        else {
	        	res = false;
				utilityFileWriteOP.writeToLog(tcid, "StatusValidation Failed ", "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath);
	        	logger.log(LogStatus.FAIL, "StatusValidation Failed");
	        	//break;
	        }
	        
	        if(statusCurrent.equals(statusCurrentValue)) {
	        	res = true;
				utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent, "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath,xwpfrun,"");
				System.out.println("statusCurrent is " +statusCurrent);
				logger.log(LogStatus.PASS, "StatusCurrent is "+statusCurrent+". Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				mtchCnt=mtchCnt+1;
				
				
			//break;
	        }
	        
	        else{
	        	res = false;
				utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent, "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath,xwpfrun,"");
				logger.log(LogStatus.FAIL, "StatusCurrent is "+statusCurrent+". Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				System.out.println("statusCurrent is " +statusCurrent);
	        	
	        }
	        
	       
	        
        }
        	  
       
        if(mtchCnt==stCnt) {
        	break;
        
        }
        
      }
    	
} 
    
    
catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e,ResultPath);
        
		res = false;
		
		
	}
    
    finally{
        
        response.close();
        
        return res;
        
}
    
  
    
	
}

public static boolean GetCallHeaderLevelRejectionStatusValidation(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String tcid,String ResultPath,XWPFRun xwpfrun, ExtentTest logger) throws IOException, InterruptedException {
	
	boolean res = false;
	Thread.sleep(6000);
	int MaxwaitingTime = 10000;
	
	int regularwaitTime = 10000;
	String expectedorderReferenceCode = System.getProperty("orderReferenceCode");
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Order ID is " + OrderID);
        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID,ResultPath);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");
        logger.log(LogStatus.INFO, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
       
        
        
        for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
        	
        	
        	Thread.sleep(regularwaitTime);
        
        response = httpclient.execute(target, httpget);
        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");
        logger.log(LogStatus.INFO, "Response code displayed is: " + response.getStatusLine().getStatusCode());
        String jsonresponse = EntityUtils.toString(response.getEntity());
        
        
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath,xwpfrun,"");
        	
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
        
        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString(),ResultPath);
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        
        int stCnt=jarray.size();
        
        int mtchCnt=0;
        
        System.out.println(jarray.size());
        for(int i=0; i<jarray.size(); i++) {
        	
        	jsonobject = jarray.get(i).getAsJsonObject();
	        String statusValidationResult = jsonobject.get("statusValidation").toString();
	        
	        String statusCurrent = jsonobject.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
	    	String orderReferenceCode = jsonobject.get("orderReferenceCode").toString().replaceAll("^\"|\"$", "");
	    	String priceConfirmedTaxRate = jsonobject.get("priceConfirmedTaxRate").toString().replaceAll("^\"|\"$", "");
	    	String quantityType = jsonobject.get("quantityType").toString().replaceAll("^\"|\"$", "");
	    	String priceConfirmedAmount = jsonobject.get("priceConfirmedAmount").toString().replaceAll("^\"|\"$", "");
	    	String priceConfirmedEachesAmount = jsonobject.get("priceConfirmedEachesAmount").toString().replaceAll("^\"|\"$", "");

	        System.out.println("StatusValidation Result is " + statusValidationResult);
	        
	        utilityFileWriteOP.writeToLog(tcid, "StatusValidation Result is " + statusValidationResult, "",ResultPath,xwpfrun,"");
	        logger.log(LogStatus.INFO, "StatusValidation Result is : " + statusValidationResult);
	       
	        if(statusValidationResult.contains("location:0")||
	        	statusValidationResult.contains("ops:0")) {
	        	res = true;
	        	System.out.println("Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				utilityFileWriteOP.writeToLog(tcid, "Validate Header level rejection for Shipped order", "Header level rejection validated successfully.Status validation string is : "+statusValidationResult,ResultPath);
				logger.log(LogStatus.PASS, "Header level rejection validated successfully.Status validation string is : "+statusValidationResult);
				
	        }
	        else {
	        	res = false;
				utilityFileWriteOP.writeToLog(tcid, "StatusValidation Failed ", "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath);
	        	logger.log(LogStatus.FAIL, "StatusValidation Failed");
	        	//break;
	        }
	        
	        if(statusCurrent.equals(statusCurrentValue)) {
	        	res = true;
				utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent, "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath,xwpfrun,"");
				System.out.println("statusCurrent is " +statusCurrent);
				logger.log(LogStatus.PASS, "StatusCurrent is "+statusCurrent+". Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				mtchCnt=mtchCnt+1;
				
				
			//break;
	        }
	        
	        else{
	        	res = false;
				utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent, "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath,xwpfrun,"");
				logger.log(LogStatus.FAIL, "StatusCurrent is "+statusCurrent+". Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				System.out.println("statusCurrent is " +statusCurrent);
	        	
	        }
	        
	       
	        
        }
        	  
       
        if(mtchCnt==stCnt) {
        	break;
        
        }
        
      }
    	
} 
    
    
catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e,ResultPath);
        
		res = false;
		
		
	}
    
    finally{
        
        response.close();
        
        return res;
        
}
    
  
    
	
}

public static boolean GetCallDuplicateItemStatusValidation(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String tcid,String ResultPath,XWPFRun xwpfrun, ExtentTest logger) throws IOException, InterruptedException {
	
	boolean res = false;
	Thread.sleep(6000);
	int MaxwaitingTime = 10000;
	
	int regularwaitTime = 10000;
	String expectedorderReferenceCode = System.getProperty("orderReferenceCode");
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Order ID is " + OrderID);
        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID,ResultPath);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");
        logger.log(LogStatus.INFO, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
       
        
        
        for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
        	
        	
        	Thread.sleep(regularwaitTime);
        
        response = httpclient.execute(target, httpget);
        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");
        logger.log(LogStatus.INFO, "Response code displayed is: " + response.getStatusLine().getStatusCode());
        String jsonresponse = EntityUtils.toString(response.getEntity());
        
        
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath,xwpfrun,"");
        	
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
        
        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString(),ResultPath);
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        
        int stCnt=jarray.size();
        
        int mtchCnt=0;
        
        System.out.println(jarray.size());
        for(int i=0; i<jarray.size(); i++) {
        	
        	jsonobject = jarray.get(i).getAsJsonObject();
	        String statusValidationResult = jsonobject.get("statusValidation").toString();
	        
	        String statusCurrent = jsonobject.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
	   
	        System.out.println("Status Validation String is " + statusValidationResult);
	        
	        utilityFileWriteOP.writeToLog(tcid, "Status Validation String is " + statusValidationResult, "",ResultPath,xwpfrun,"");
	        logger.log(LogStatus.INFO, "Status Validation String is : " + statusValidationResult);
	       
	        if(statusValidationResult.contains("duplicateItem:0")&&
	        		(jsonobject.get("itemId").toString().replaceAll("^\"|\"$", "").endsWith("D1")||
	        				jsonobject.get("itemId").toString().replaceAll("^\"|\"$", "").endsWith("D2")) ) {
	        	res = true;
	        	System.out.println("Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				utilityFileWriteOP.writeToLog(tcid, "Validate Duplicate items", "Conditions for duplicate item validated successfully.Status validation string is : "+statusValidationResult,ResultPath);
				logger.log(LogStatus.PASS, "Conditions for duplicate item validated successfully.Status validation string is : "+statusValidationResult);
				
	        }
	        else {
	        	res = false;
				utilityFileWriteOP.writeToLog(tcid, "Status Validation Failed ", "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath);
	        	logger.log(LogStatus.FAIL, "StatusValidation Failed");
	        	//break;
	        }
	        
	        if(statusCurrent.equals(statusCurrentValue)) {
	        	res = true;
				utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent, "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath,xwpfrun,"");
				System.out.println("statusCurrent is " +statusCurrent);
				logger.log(LogStatus.PASS, "StatusCurrent is "+statusCurrent+". Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				mtchCnt=mtchCnt+1;
				
				
			//break;
	        }
	        
	        else{
	        	res = false;
				utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent, "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath,xwpfrun,"");
				logger.log(LogStatus.FAIL, "StatusCurrent is "+statusCurrent+". Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				System.out.println("statusCurrent is " +statusCurrent);
	        	
	        }
	        
	       
	        
        }
        	  
       
        if(mtchCnt==stCnt) {
        	break;
        
        }
        
      }
    	
} 
    
    
catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e,ResultPath);
        
		res = false;
		
		
	}
    
    finally{
        
        response.close();
        
        return res;
        
}
    
  
    
	
}

public static String GetCallWSCPorWSPFromWebPortal(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String ItemID, String quantityType, String tcid, String Resultpath, XWPFRun xwpfRun, ExtentTest logger) throws IOException {
	
	String res = null;
	
	String pricesJsonvalue = null;
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        //https://sit-api.morrisons.com/wholesaleorder/v1/customers/mccolls/catalogues/main/items/101743031?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36&filtertype=maptype&filtervalue=PIN
        
        ///wholesaleorder/v1/customers/sandpiper/catalogues/main/items/
        HttpGet httpget = new HttpGet(UrlTail + ItemID + "?" + ApiKey+"&filtertype=maptype&filtervalue=PIN");
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("ItemID ID is " + ItemID);
        //utilityFileWriteOP.writeToLog(tcid, "Searching the attributes for ", "ItemID "+ItemID,Resultpath);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
       // utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",Resultpath);
        
     /*   for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
        	
        	Thread.sleep(regularWaitingTime);*/
        
        response = httpclient.execute(target, httpget);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
       // utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),Resultpath);
        
        String jsonresponse = EntityUtils.toString(response.getEntity());
        System.out.println(jsonresponse);
       // utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,Resultpath);
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
       // utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString(),Resultpath);
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        System.out.println(jarray.size());
        for(int i=0; i<jarray.size(); i++) {
        	
        	jsonobject = jarray.get(i).getAsJsonObject();
	
	        
		    JsonArray jarrayprices = jsonobject.getAsJsonArray("prices");
		    
		    
		    for(int pricesindex=0; pricesindex<jarrayprices.size(); pricesindex++) {
		    	
		    	JsonObject jsonobjectpricesBody = jarrayprices.get(pricesindex).getAsJsonObject();
	        	
	        	String pricesJsonKey = jsonobjectpricesBody.get("type").toString().replaceAll("^\"|\"$", "");
	        	
	        	JsonArray jarraypricesBody = jsonobjectpricesBody.getAsJsonArray("values");
		        
	        	if(quantityType.equals("CS")) {
	        		
	        		if(pricesJsonKey.equals("WSCP")) {
	        			
	        			
	        			for(int pricesBodyindex=0; pricesBodyindex<jarraypricesBody.size(); pricesBodyindex++) {
			        		
					        JsonObject jsonobjectpricesBodyValue = jarraypricesBody.get(pricesBodyindex).getAsJsonObject();
					       
					        pricesJsonvalue = jsonobjectpricesBodyValue.get("value").getAsBigDecimal().stripTrailingZeros().toString().replaceAll("^\"|\"$", "");
					        
			        	}
	        			
	        			
	        		}
	        		
	        	}
	        	
	        	if(quantityType.equals("EA")) {
	        		
	        		if(pricesJsonKey.equals("WSP")) {
	        			
	        			
	        			for(int pricesBodyindex=0; pricesBodyindex<jarraypricesBody.size(); pricesBodyindex++) {
			        		
					        JsonObject jsonobjectpricesBodyValue = jarraypricesBody.get(pricesBodyindex).getAsJsonObject();

					        pricesJsonvalue = jsonobjectpricesBodyValue.get("value").getAsBigDecimal().stripTrailingZeros().toString().replaceAll("^\"|\"$", "");
					     
			        	}
	        			
	        			
	        		}
	        		
	        	}
	        	
	        	/*if(pricesJsonKey.equals("ORDER-OPS")) {
	        		
	        		
	        		for(int pricesBodyindex=0; pricesBodyindex<jarraypricesBody.size(); pricesBodyindex++) {
		        		
				        JsonObject jsonobjectpricesBodyValue = jarraypricesBody.get(pricesBodyindex).getAsJsonObject();

				        pricesJsonvalue = jsonobjectpricesBodyValue.get("value").toString().replaceAll("^\"|\"$", "");
				     
		        	}
	        	
 	        		
	        	}*/
	    	
		    }
	   
		   	     
		    res = pricesJsonvalue;
			
			
        	
        }
        	  
       
	} catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		//utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e,Resultpath);
        
		res = null;
		return res;
	}
    
    finally{
        
        response.close();
        
}
    
    return res;
    
	
}


public static boolean PostCallCleanupJob_run(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String EndParameters, String AuthorizationKey, String AuthorizationValue,String tcid,String ResultPath, XWPFRun xwpfRun, ExtentTest logger) throws IOException {
	
	boolean res = false;
	


	CloseableHttpResponse response=null;
    
	
	try {
	


    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

   

    

		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpPost httppost = new HttpPost(UrlTail + "?" + ApiKey + EndParameters);
        
        httppost.setConfig(config);
        
        httppost.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Executing request " + httppost.getRequestLine() + " to " + target + " via " + proxy);
       // utilityFileWriteOP.writeToLog(tcid, "Executing request " + httppost.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath, xwpfRun, "");
        


     
        //\"orderCount\": \"" + OrderCount + "\",
        
        //String json = "{  \"orders\":{\"orderId\": \"" + OrderID + "\",\"orderBuyerPartyId\": \"" + OrderAttributes[0] + "\",\"orderSellerPartyId\": \"" + OrderAttributes[1] + "\",\"orderReferenceCode\": \"" + OrderAttributes[2] + "\",\"messageId\": \"" + MessageAttributes[0] + "\",\"messageSenderPartyId\": \"" + MessageAttributes[1] + "\",\"messageRecipientPartyId\": \"" + MessageAttributes[2] + "\",\"messageType\": \"" + MessageType + "\",\"messageCreatedAt\": \"" + MessageAttributes[3] + "\",\"shipToPartyId\": \"" + ShipToAddressAttributes[0] + "\",\"shipToLocationId\": \"" + ShipToLocationId + "\",\"shipToAddressName\": \"" + ShipToAddressAttributes[1] + "\",\"shipToAddressLine1\": \"" + ShipToAddressAttributes[2] + "\",\"shipToAddressLine2\": \"" + ShipToAddressAttributes[3] + "\",\"shipToAddressCity\": \"" + ShipToAddressAttributes[4] + "\",\"shipToAddressState\": \"" + ShipToAddressAttributes[5] + "\",\"shipToAddressPostCode\": \"" + ShipToAddressAttributes[6] + "\",\"shipToAddressCountryCode\": \"" + ShipToAddressAttributes[7] + "\",\"shipToDeliverAt\": \"" + ShipToDeliverAt + "\",\"shipToDeliverLatestAt\": \"" + ShipToAddressAttributes[8] + "\",\"shipFromLocationId\": \"" + ShipToAddressAttributes[9] + "\",\"shipFromAddressName\": \"" + ShipToAddressAttributes[10] + "\",\"billToPartyId\": \"" + BillToAddressAttributes[0] + "\",\"billToAddressName\": \"" + BillToAddressAttributes[1] + "\",\"billToAddressLine1\": \"" + BillToAddressAttributes[2] + "\",\"billToAddressLine2\": \"" + BillToAddressAttributes[3] + "\",\"billToAddressCity\": \"" + BillToAddressAttributes[4] + "\",\"billToAddressState\": \"" + BillToAddressAttributes[5] + "\",\"billToAddressPostCode\": \"" + BillToAddressAttributes[6] + "\",\"billToAddressCountryCode\": \"" + BillToAddressAttributes[7] + "\",\"billToTaxId\": \"" + BillToTaxAddressAttributes[0] + "\",\"billToTaxAddressName\": \"" + BillToTaxAddressAttributes[1] + "\",\"billToTaxAddressLine1\": \"" + BillToTaxAddressAttributes[2] + "\",\"billToTaxAddressLine2\": \"" + BillToTaxAddressAttributes[3] + "\",\"billToTaxAddressCity\": \"" + BillToTaxAddressAttributes[4] + "\",\"billToTaxAddressState\": \"" + BillToTaxAddressAttributes[5] + "\",\"billToTaxAddressPostCode\": \"" + BillToTaxAddressAttributes[6] + "\",\"billToTaxAddressCountryCode\": \"" + BillToTaxAddressAttributes[7] + "\"},\"items\": [" + ItemDetails + "]}";
        String json = "{}"; 

        
        //String json = "{  \"orders\":{\"orderId\": \"" + OrderID + "\",\"shipToLocationId\": \"" + ShipToLocationId + "\",\"shipToPartyId\": \"" + ShipToAddressAttributes[0] + "\",\"orderSellerPartyId\": \"" + OrderAttributes[1] + "\",\"billToAddressName\": \"" + BillToAddressAttributes[1] + "\",\"shipToAddressLine1\": \"" + ShipToAddressAttributes[2] + "\",\"shipToAddressCity\": \"" + ShipToAddressAttributes[4] + "\",\"shipToAddressLine2\": \"" + ShipToAddressAttributes[3] + "\",\"billToTaxAddressCity\": \"" + BillToTaxAddressAttributes[4] + "\",\"orderReferenceCode\": \"" + OrderAttributes[2] + "\",\"messageType\": \"" + MessageType + "\",\"billToPartyId\": \"" + BillToAddressAttributes[0] + "\",\"shipToAddressPostCode\": \"" + ShipToAddressAttributes[6] + "\",\"billToAddressLine1\": \"" + BillToTaxAddressAttributes[2] + "\",\"billToTaxAddressPostCode\": \"" + BillToTaxAddressAttributes[6] + "\",\"billToAddressLine2\": \"" + BillToAddressAttributes[3] + "\",\"billToAddressPostCode\": \"" + BillToAddressAttributes[6] + "\",\"messageSenderPartyId\": \"" + MessageAttributes[1] + "\",\"shipToAddressState\": \"" + ShipToAddressAttributes[5] + "\",\"shipToDeliverAt\": \"" + ShipToDeliverAt + "\",\"shipToAddressName\": \"" + ShipToAddressAttributes[1] + "\",\"messageCreatedAt\": \"" + MessageAttributes[3] + "\",\"billToAddressState\": \"" + BillToAddressAttributes[5] + "\",\"billToTaxId\": \"" + BillToTaxAddressAttributes[0] + "\",\"messageId\": \"" + MessageAttributes[0] + "\",\"billToAddressCountryCode\": \"" + BillToAddressAttributes[7] + "\",\"billToTaxAddressState\": \"" + BillToTaxAddressAttributes[5] + "\",\"billToTaxAddressCountryCode\": \"" + BillToTaxAddressAttributes[7] + "\",\"messageRecipientPartyId\": \"" + MessageAttributes[2] + "\",\"shipToAddressCountryCode\": \"" + ShipToAddressAttributes[7] + "\",\"orderBuyerPartyId\": \"" + OrderAttributes[0] + "\",\"billToTaxAddressLine1\": \"" + BillToTaxAddressAttributes[2] + "\",\"shipToDeliverLatestAt\": \"" + ShipToAddressAttributes[8] + "\",\"billToTaxAddressLine2\": \"" + BillToTaxAddressAttributes[3] + "\",\"billToAddressCity\": \"" + BillToAddressAttributes[4] + "\",\"billToTaxAddressName\": \"" + BillToTaxAddressAttributes[1] + "\",\"billToTaxAddressPostCode\": \"" + BillToTaxAddressAttributes[6] + "\",\"billToTaxAddressCountryCode\": \"" + BillToTaxAddressAttributes[7] + "\"},\"items\": [" + ItemDetails + "]}";

        
        // httppost.setEntity(new StringEntity(json, "UTF-8"));
        
        System.out.println(json);
        
        httppost.setEntity(new StringEntity(json));
		
       // httppost.setHeader("apikey", "l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httppost.setHeader("Accept", "application/json");
		httppost.setHeader("Content-type", "application/json");
		
		//utilityFileWriteOP.writeToLog(tcid, "Sending the JSON ", "Body :- "+json);
		response = httpclient.execute(target, httppost);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        //utilityFileWriteOP.writeToLog(tcid, "Response code is ", "Displayed as: "+response.getStatusLine().getStatusCode(),ResultPath);

        //System.out.println(EntityUtils.toString(response.getEntity()));
        //utilityFileWriteOP.writeToLog(tcid, "Response Body is ", "Body :- "+EntityUtils.toString(response.getEntity()),ResultPath);
    
        int ResponseCode = response.getStatusLine().getStatusCode();
        
        //response.toString();
		if( (ResponseCode == 201) || (ResponseCode == 204))
			res = true;
		else
			
			
			res=false;

    }
		catch (Exception e) {
		// TODO: handle exception
			e.printStackTrace();
		System.out.println(e);
		//utilityFileWriteOP.writeToLog(tcid, "Error occurred during POST call ", "Due to :"+e,ResultPath,xwpfRun, "");
        
		res = false;
		
		
	}
    
    finally{
        
        response.close();
        

        
        return res;
        
}
	
}	




public static boolean launchBrowserForAWS(WebDriver driver, WebDriverWait wait, String tcid, String DriverName, String DriverPath ,String DriverType,String BrowserPath, String ECSURL,String AWSUser,String AWSPass, String searchkey, String taskname,String[] taskid,String Resultpath,XWPFRun xwpfrun,ExtentTest logger)
{
	boolean res=false;
	try
	{
		
		System.setProperty(DriverName,DriverPath);
		System.out.println("Driver Type:"+DriverType);
		System.out.println("BrowserPath:"+BrowserPath);


		
		if((DriverType.equalsIgnoreCase("ChromeDriver"))&&(BrowserPath.equalsIgnoreCase("Default"))){
			HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
			chromePrefs.put("profile.default_content_settings.popups", 0);
			chromePrefs.put("download.prompt_for_download", "false");
			chromePrefs.put("download.", "false");
			chromePrefs.put("download.default_directory", System.getProperty("user.dir"));
			ChromeOptions options = new ChromeOptions();
		//	options.setBinary(ChromeBrowserExePath);

			options.setExperimentalOption("prefs", chromePrefs);
			options.setExperimentalOption("useAutomationExtension", false);
			DesiredCapabilities cap = DesiredCapabilities.chrome();
			cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			cap.setCapability(ChromeOptions.CAPABILITY, options);
			driver = new ChromeDriver(cap);
			wait=new WebDriverWait(driver, 60);			
			driver.manage().deleteAllCookies();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			
			wait = new WebDriverWait(driver, 30);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);	
		
			
			driver.manage().deleteAllCookies();
			driver.get(ECSURL);
			driver.manage().window().maximize();
			driver.findElement(By.xpath("//input[@id='username']")).sendKeys(AWSUser);
			String awsDynamoDB_sc=Resultpath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc, driver);
			utilityFileWriteOP.writeToLog(tcid,"Username entered for AWS : "+AWSUser,Resultpath);
			logger.log(LogStatus.PASS, "AWS Login page", "Username entered for AWS : "+AWSUser+logger.addScreenCapture(awsDynamoDB_sc));
		
			
			driver.findElement(By.xpath("//input[@id='password']")).sendKeys(AWSPass);
			String awsDynamoDB_sc1=Resultpath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc1, driver);
			utilityFileWriteOP.writeToLog(tcid,"Username entered for AWS : "+AWSPass,Resultpath);
			logger.log(LogStatus.PASS, "AWS Login page", "Username entered for AWS : "+AWSPass+logger.addScreenCapture(awsDynamoDB_sc));
			
			
			String awsDynamoDB_s=Resultpath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_s, driver);
			driver.findElement(By.xpath("//a[@id='signin_button']")).click();
			utilityFileWriteOP.writeToLog(tcid,"Clicked on Sign In button",Resultpath);
			logger.log(LogStatus.PASS, "AWS Login page", "Clicked on Sign In button"+logger.addScreenCapture(awsDynamoDB_s));
			
	
			
			if(driver.findElement(By.xpath("//span[text()='AWS Management Console']")).isDisplayed())
			{
				String awsDynamoDB_sc2=Resultpath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(awsDynamoDB_sc2, driver);
				utilityFileWriteOP.writeToLog(tcid,"Clicked on Sign In button",Resultpath);
				logger.log(LogStatus.PASS, "AWS Login page", "Navigation to AWS Management Console page is successful"+logger.addScreenCapture(awsDynamoDB_sc2));
				res=true;
				
			}
			else
			{
				String awsDynamoDB_sc2=Resultpath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(awsDynamoDB_sc2, driver);
				utilityFileWriteOP.writeToLog(tcid,"Clicked on Sign In button",Resultpath);
				logger.log(LogStatus.FAIL, "AWS Login page", "Navigation to AWS Management Console page is not successful"+logger.addScreenCapture(awsDynamoDB_sc2));
				res=false;
			}
			

				String[] tasknames= taskname.split(",");
				if(driver.findElement(By.xpath("//input[@id='search-box-input' and @class='awsui-input awsui-input-type-search']")).isDisplayed())
				{
					
					driver.findElement(By.xpath("//input[@id='search-box-input' and @class='awsui-input awsui-input-type-search']")).sendKeys(searchkey);
					String AWSServicePage_WP1=Resultpath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";
					TakeScreenShot.saveScreenShot(AWSServicePage_WP1, driver);
					utilityFileWriteOP.writeToLog(tcid,searchkey+" Keyword entered in Seach textbox field",Resultpath);
					logger.log(LogStatus.PASS, "Enter ECS Keyword", searchkey+" Keyword entered in Seach textbox field"+logger.addScreenCapture(AWSServicePage_WP1));
					
					driver.findElement(By.xpath("//input[@id='search-box-input' and @class='awsui-input awsui-input-type-search']")).sendKeys(Keys.RETURN);
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(text(),'Task Definitions')]")));
					
					
					//loop for the number of jobs to run
					
					for(int i=0;i<tasknames.length;i++){
					
					//click on task definitions				
					driver.findElement(By.xpath("//a[contains(text(),'Task Definitions')]")).click();
					String AWSServicePage_WP=Resultpath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";
					TakeScreenShot.saveScreenShot(AWSServicePage_WP, driver);
					utilityFileWriteOP.writeToLog(tcid,"Clicked on Task Definitions",Resultpath);
					logger.log(LogStatus.PASS, "Click Task Definitions", "Clicked on Task Definitions"+logger.addScreenCapture(AWSServicePage_WP));
					
					//driver.findElement(By.xpath("//a[@id='side-nav-item-taskDefinitions']")).click();
					Thread.sleep(2000);
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[@class='ecs-header-text ng-binding' and contains(text(),'Task Definitions')]")));
					driver.findElement(By.xpath("//span[@class='awsui-select-value' and contains(text(),'50')]")).click();
					WebElement element2 = driver.findElement(By.xpath("//li[@title='100' and contains(text(),'100')]"));
					JavascriptExecutor executor2 = (JavascriptExecutor)driver;
					executor2.executeScript("arguments[0].click();", element2);		
										
					//loop
					
					boolean taskvisible =false;
					driver.findElement(By.xpath("//input[@placeholder ='Filter in this page' and @type='text']")).sendKeys(tasknames[i]);
					int count=0;
					driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
					while(taskvisible!=true){
						try{
						taskvisible= driver.findElement(By.xpath("//a[contains(text(),'"+tasknames[i]+"')]")).isDisplayed();
						}
						catch(Exception e){
						System.out.println("Visibilty of task"+taskvisible);
						driver.findElement(By.xpath("//i[@class='awsui-icon angle-right']")).click();
						count++;
						if(count>15)
							break;
						}
					}
					
					System.out.println("task found");
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
					driver.findElement(By.xpath("(//div[contains(@class,'ui-grid-selection-row-header-buttons ui-grid-icon-ok ng-scope')])[1]")).click();
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='btn dropdown-label ng-binding' and contains(text(),'Actions')]")));
					driver.findElement(By.xpath("//div[@class='btn dropdown-label ng-binding' and contains(text(),'Actions')]")).click();
					driver.findElement(By.xpath("//a[contains(text(),'Run Task')]")).click();
					
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[@class='ecs-header-text ng-binding' and contains(text(),'Run Task')]")));
					// Take Screenshot
					String RunTask_TC002_REG_WP2=Resultpath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";
					TakeScreenShot.saveScreenShot(RunTask_TC002_REG_WP2, driver);
					//utilityFileWriteOP.writeToLog(TC_no, "Run Task Screen", "Opens!!!",Resultpath);
					Thread.sleep(2000);
					driver.findElement(By.xpath("//div[@class='ecs-table-row-content ng-isolate-scope' and @items='clustersDropDown']")).click();
					//utilityFileWriteOP.writeToLog(TC_no,"Click on Cluster Drop Down","List Expanded",Resultpath);
					Thread.sleep(2000);
					// Take Screenshot
					String ClusterDropDown_TC002_REG_WP2=Resultpath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";
					TakeScreenShot.saveScreenShot(ClusterDropDown_TC002_REG_WP2, driver);
					
					WebElement element = driver.findElement(By.xpath("//a[contains(text(),'nonprod-ECS-ECSCluster-W68V9ZHCS399')]"));
					JavascriptExecutor executor = (JavascriptExecutor)driver;
					executor.executeScript("arguments[0].click();", element);
					System.out.println("Selected cluster");
				//	utilityFileWriteOP.writeToLog(TC_no,"nonprod-ECS-ECSCluster-W68V9ZHCS399","option selected",Resultpath,xwpfrun);
					Thread.sleep(2000);
					// Take Screenshot
					String ClusterSelection_TC002_REG_WP2=Resultpath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";
					TakeScreenShot.saveScreenShot(ClusterSelection_TC002_REG_WP2, driver);
					
					WebElement elementswitch = driver.findElement(By.xpath("//span[contains(text(),'Switch to launch type')]"));
					JavascriptExecutor executorswitch = (JavascriptExecutor)driver;
					executor.executeScript("arguments[0].click();", elementswitch);
					
					
					
					//driver.findElement(By.xpath("//span[contains(text(),'Run Task')]")).click();
					WebElement element1 = driver.findElement(By.xpath("//span[contains(text(),'Run Task')]"));
					//WebElement element1 = driver.findElement(By.xpath("//span[contains(text(),'Cancel')]"));
					JavascriptExecutor executor1 = (JavascriptExecutor)driver;
					executor.executeScript("arguments[0].click();", element1);
					//driver.findElement(By.xpath("//span[contains(text(),'Cancel')]")).click();
					//System.out.println("Clicked Cancel");
					System.out.println("Clicked Run Task");
					String AWSServicePage_WP3=Resultpath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";
					TakeScreenShot.saveScreenShot(AWSServicePage_WP3, driver);
					utilityFileWriteOP.writeToLog(tcid,"Clicked on Run Task button",Resultpath);
					logger.log(LogStatus.PASS, "Click Run Task", "Clicked on Run Task button"+logger.addScreenCapture(AWSServicePage_WP3));
					
					//	driver.findElement(By.xpath("//button[@class='awsui-button awsui-button-size-normal awsui-button-variant-link awsui-hover-child-icons']")).click();
					//  Take Screenshot
					String TaskTriggered=Resultpath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";
					TakeScreenShot.saveScreenShot(TaskTriggered, driver);
					//utilityFileWriteOP.writeToLog(TC_no, "RunTask Button clicked", "Passed!!!",Resultpath,xwpfrun);
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[contains(text(),'Created tasks successfully')]")));
					//  Take Screenshot
					String TaskSuccess=Resultpath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";
					TakeScreenShot.saveScreenShot(TaskSuccess, driver);
					
					taskid[0]=driver.findElement(By.xpath("//div/span/div[contains(text(),'Task Ids')]")).getText().replace("Task Ids : [", "").replace("\"", "").replace("]", "");
					System.out.println("Task ID: "+taskid[0]);
					System.out.println("Transfer Request task Triggered Successfully");
					Thread.sleep(1000);
					}
					driver.close();
					Thread.sleep(2000);
					
					//utilityFileWriteOP.writeToLog(TC_no, "Transfer Request task triggered succesfully, moving to previous tab", "Passed!!!",Resultpath,xwpfrun);   
					res=true; 		
					return res;
					
				}
				
				else
				{
					res =false;
					return res;
				}
			
		}

		
		
	}
	catch(Exception e)
	{
		e.printStackTrace();
		String awsDynamoDB_sc=Resultpath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(awsDynamoDB_sc, driver);
		res=false;
		//Utilities_i5A_All.utilityFileWriteOP.writeToLog(tcid, "A problem occurred while logging in AWS Rackspace", "Due to "+e,Resultpath);
	}
	finally
	{
		return res;
	}
	
}




public static boolean aws_login(WebDriver driver, WebDriverWait wait, String tcid, String un, String pw ,String Resultpath,XWPFRun xwpfrun,ExtentTest logger)
{
	boolean res=false;
	
	try
	{
		
		Thread.sleep(10000);
		
		//wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='username']")));
	//	Utilities_i5A_All.utilityFileWriteOP.writeToLog(tcid, "Login Page of AWS Rackspace", "Displayed!!!",Resultpath);
		
		String awsDynamoDB_sc=Resultpath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(awsDynamoDB_sc, driver);
		
		driver.findElement(By.xpath("//input[@id='username']")).sendKeys(un);
		//Utilities_i5A_All.utilityFileWriteOP.writeToLog(tcid, "Username", "Entered!!!",Resultpath);
		
		driver.findElement(By.xpath("//input[@id='password']")).sendKeys(pw);
		//Utilities_i5A_All.utilityFileWriteOP.writeToLog(tcid, "Password", "Entered!!!",Resultpath);
		
		String awsDynamoDB_sc1=Resultpath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(awsDynamoDB_sc1, driver);
		
		
		driver.findElement(By.xpath("//a[@id='signin_button']")).click();
		
		
		//Utilities_i5A_All.utilityFileWriteOP.writeToLog(tcid, "Signin  Button", "Clicked!!!",Resultpath);
		
		
		if(driver.findElement(By.xpath("//span[text()='AWS Management Console']")).isDisplayed())
		{
			String awsDynamoDB_sc2=Resultpath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc2, driver);
			
			res=true;
			//Utilities_i5A_All.utilityFileWriteOP.writeToLog(tcid, "AWS Management Console Page", "Displayed!!!",Resultpath);
		}
		
		else
		{
			
		}
		
		
		
		
		
		
		
		
	}
	catch(Exception e)
	{
		e.printStackTrace();
		String awsDynamoDB_sc=Resultpath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(awsDynamoDB_sc, driver);
		res=false;
		//Utilities_i5A_All.utilityFileWriteOP.writeToLog(tcid, "A problem occurred while logging in AWS Rackspace", "Due to "+e,Resultpath);
	}
	finally
	{
		return res;
	}
	
}


public static boolean Ecs_task_runner_job_trigger_task_ID_Retrieve(WebDriver driver,WebDriverWait wait, String TC_no, String searchkey, String taskname , String Resultpath ,XWPFRun xwpfrun,ExtentTest logger,String[] taskid)
{
	{
		String[] tasknames= taskname.split(",");
		boolean res=false;
		try{

			// Take Screenshot

			String AfterAWSLogin_WP1=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
				TakeScreenShot.saveScreenShot(AfterAWSLogin_WP1, driver);
			
		

			boolean imgres1=driver.findElement(By.xpath("//input[@id='search-box-input' and @class='awsui-input awsui-input-type-search']")).isDisplayed();

			if(imgres1)
			{
				//utilityFileWriteOP.writeToLog(TC_no, "AWS Services Page", "Found!!!",Resultpath,xwpfrun);
				// Take Screenshot
				
				String AWSServicePage_WP1=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
				TakeScreenShot.saveScreenShot(AWSServicePage_WP1, driver);
				
				//Enter ecs keyword in the search window
				driver.findElement(By.xpath("//input[@id='search-box-input' and @class='awsui-input awsui-input-type-search']")).sendKeys(searchkey);
				//utilityFileWriteOP.writeToLog(TC_no, "ECS Keyword", "Entered!!!",Resultpath);
				driver.findElement(By.xpath("//input[@id='search-box-input' and @class='awsui-input awsui-input-type-search']")).sendKeys(Keys.RETURN);
						
				//Waiting for ECS Functions Search Page
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(text(),'Task Definitions')]")));
				
				//utilityFileWriteOP.writeToLog(TC_no, "ECS Search Page", "Found!!!",Resultpath);
				// Take Screenshot
				
				String LambdaFnSearch_WP1=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
				TakeScreenShot.saveScreenShot(LambdaFnSearch_WP1, driver);
				
				
				
				//loop for the number of jobs to run
				
				for(int i=0;i<tasknames.length;i++){
				
				//click on task definitions				
				driver.findElement(By.xpath("//a[contains(text(),'Task Definitions')]")).click();
				
				//driver.findElement(By.xpath("//a[@id='side-nav-item-taskDefinitions']")).click();
				Thread.sleep(2000);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[@class='ecs-header-text ng-binding' and contains(text(),'Task Definitions')]")));
				driver.findElement(By.xpath("//span[@class='awsui-select-value' and contains(text(),'50')]")).click();
				WebElement element2 = driver.findElement(By.xpath("//li[@title='100' and contains(text(),'100')]"));
				JavascriptExecutor executor2 = (JavascriptExecutor)driver;
				executor2.executeScript("arguments[0].click();", element2);		
									
				//loop
				
				boolean taskvisible =false;
				
				
				driver.findElement(By.xpath("//input[@placeholder ='Filter in this page' and @type='text']")).sendKeys(tasknames[i]);
				//utilityFileWriteOP.writeToLog(TC_no, "Task name: "+tasknames[i], "Entered!!!",Resultpath);
				int count=0;
				driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
				while(taskvisible!=true){
					try{
					taskvisible= driver.findElement(By.xpath("//a[contains(text(),'"+tasknames[i]+"')]")).isDisplayed();
					}
					catch(Exception e){
					System.out.println("Visibilty of task"+taskvisible);
					driver.findElement(By.xpath("//i[@class='awsui-icon angle-right']")).click();
					count++;
					if(count>15)
						break;
					}
				}
				
				
				System.out.println("task found");
				//<i class="awsui-icon angle-right" ng-click="pagination.pageForward()" ng-class="{'awsui-icon-disabled': pagination.pageForwardDisabled()}"></i>
				
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				
				
				driver.findElement(By.xpath("(//div[contains(@class,'ui-grid-selection-row-header-buttons ui-grid-icon-ok ng-scope')])[1]")).click();
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='btn dropdown-label ng-binding' and contains(text(),'Actions')]")));
				driver.findElement(By.xpath("//div[@class='btn dropdown-label ng-binding' and contains(text(),'Actions')]")).click();
				driver.findElement(By.xpath("//a[contains(text(),'Run Task')]")).click();
				
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[@class='ecs-header-text ng-binding' and contains(text(),'Run Task')]")));
				// Take Screenshot
				String RunTask_TC002_REG_WP2=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
				TakeScreenShot.saveScreenShot(RunTask_TC002_REG_WP2, driver);
				//utilityFileWriteOP.writeToLog(TC_no, "Run Task Screen", "Opens!!!",Resultpath);
				Thread.sleep(2000);
				driver.findElement(By.xpath("//div[@class='ecs-table-row-content ng-isolate-scope' and @items='clustersDropDown']")).click();
				//utilityFileWriteOP.writeToLog(TC_no,"Click on Cluster Drop Down","List Expanded",Resultpath);
				Thread.sleep(2000);
				// Take Screenshot
				String ClusterDropDown_TC002_REG_WP2=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
				TakeScreenShot.saveScreenShot(ClusterDropDown_TC002_REG_WP2, driver);
				
				WebElement element = driver.findElement(By.xpath("//a[contains(text(),'nonprod-ECS-ECSCluster-W68V9ZHCS399')]"));
				JavascriptExecutor executor = (JavascriptExecutor)driver;
				executor.executeScript("arguments[0].click();", element);
				System.out.println("Selected cluster");
			//	utilityFileWriteOP.writeToLog(TC_no,"nonprod-ECS-ECSCluster-W68V9ZHCS399","option selected",Resultpath,xwpfrun);
				Thread.sleep(2000);
				// Take Screenshot
				String ClusterSelection_TC002_REG_WP2=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
				TakeScreenShot.saveScreenShot(ClusterSelection_TC002_REG_WP2, driver);
				
				WebElement elementswitch = driver.findElement(By.xpath("//span[contains(text(),'Switch to launch type')]"));
				JavascriptExecutor executorswitch = (JavascriptExecutor)driver;
				executor.executeScript("arguments[0].click();", elementswitch);
				
				
				
				//driver.findElement(By.xpath("//span[contains(text(),'Run Task')]")).click();
				WebElement element1 = driver.findElement(By.xpath("//span[contains(text(),'Run Task')]"));
				//WebElement element1 = driver.findElement(By.xpath("//span[contains(text(),'Cancel')]"));
				JavascriptExecutor executor1 = (JavascriptExecutor)driver;
				executor.executeScript("arguments[0].click();", element1);
				//driver.findElement(By.xpath("//span[contains(text(),'Cancel')]")).click();
				//System.out.println("Clicked Cancel");
				System.out.println("Clicked Run Task");
				
				//	driver.findElement(By.xpath("//button[@class='awsui-button awsui-button-size-normal awsui-button-variant-link awsui-hover-child-icons']")).click();
				//  Take Screenshot
				String TaskTriggered=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
				TakeScreenShot.saveScreenShot(TaskTriggered, driver);
				//utilityFileWriteOP.writeToLog(TC_no, "RunTask Button clicked", "Passed!!!",Resultpath,xwpfrun);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[contains(text(),'Created tasks successfully')]")));
				//  Take Screenshot
				String TaskSuccess=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";
				TakeScreenShot.saveScreenShot(TaskSuccess, driver);
				
				taskid[0]=driver.findElement(By.xpath("//div/span/div[contains(text(),'Task Ids')]")).getText().replace("Task Ids : [", "").replace("\"", "").replace("]", "");
				System.out.println("Task ID: "+taskid[0]);
				System.out.println("Transfer Request task Triggered Successfully");
				Thread.sleep(1000);
				}
				driver.close();
				Thread.sleep(2000);
				
				//utilityFileWriteOP.writeToLog(TC_no, "Transfer Request task triggered succesfully, moving to previous tab", "Passed!!!",Resultpath,xwpfrun);   
				res=true; 		
				return res;
				
			}
			
			else
			{
				res =false;
				return res;
			}
			
		}
		catch (Exception e) 
		{	
			e.printStackTrace();
			res=false;
			System.out.println("The error is "+e);
			//utilityFileWriteOP.writeToLog(TC_no, "Problem ocuurred for "+e, "Triggering Daily Load",Resultpath);
			return res;
		}

		finally 
		{
			return res;
		}


	}
	
}	

public static boolean AWSLogOut (String UserName,WebDriver driver, WebDriverWait wait, String TC_no , String Resultpath ,  String Screenshotpath,XWPFRun xwpfrun) 
{
	boolean res = false;
	try
	{
		String MainPg_title = "Fanatical Support for AWS";
		String currentWindow = driver.getWindowHandle();  //will keep current window to switch back
		System.out.println("Current Title: "+driver.getTitle());
		for(String winHandle : driver.getWindowHandles())
		{
			if (driver.switchTo().window(winHandle).getTitle().equals(MainPg_title))  //Search for the desired Window
			{

				
				System.out.println("Entered to this block working correctly");
				break;

			} 
			else 
			{
				
				driver.switchTo().window(currentWindow);
			} 
		}


		
		//wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@class='rs-dropdown-title' and contains(text(),' "+ UserName + "' )]")));
		driver.findElement(By.xpath("//div[@class='phx-dropdown-title' and contains(text(),'"+UserName.toUpperCase()+"')]")).click();

		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@href='/aws/logout' and @data-pilot-tracking-id='logout']")));
		driver.findElement(By.xpath("//a[@href='/aws/logout' and @data-pilot-tracking-id='logout']")).click();
		Thread.sleep(3000);
		//Take Screenshot
		String ClickonLogout=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";   
		TakeScreenShot.saveScreenShot(ClickonLogout, driver);
		utilityFileWriteOP.writeToLog(TC_no, "Logout Button Clicked", "Done!!!",Resultpath);
		Thread.sleep(5000);

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='username']")));

		String AfterLogout_WP1=Resultpath+"Screenshot/"+TC_no+"/"+TC_no+getCurrentDate.getTimeStamp()+".png";

		TakeScreenShot.saveScreenShot(AfterLogout_WP1, driver);

		if(driver.findElement(By.xpath("//input[@id='username']")).isDisplayed())
		{
			res=true;
		}


		if(res)
		{
			//utilityFileWriteOP.writeToLog(TC_no, "Logout", "Successfull!!!",Resultpath,xwpfrun);
			System.out.println("Logout-Pass");
		}
		if(!res)
		{
			//utilityFileWriteOP.writeToLog(TC_no, "Cannot Logout", "Failed to Logout!!!",Resultpath,xwpfrun);
			System.out.println("Logout-Fail");
		}


		return res;


	}

	catch (Exception e)
	{
		res=false;
		System.out.println("The error is "+e);
		utilityFileWriteOP.writeToLog(TC_no, "Problem ocuurred for"+e, "while logging out from AWS",Resultpath);
		return res;
	}

	finally
	{
		return res;
	}
}


public static HashMap<String, String> LoadItemsNAS(String  fin,HashMap<String, String> hm) {
	
	// this function will return hashmap of itemno as key and consolidated qty
	
	BufferedReader br =null;
	
	try{
	
	File file = new File(fin);
	
	
	FileInputStream fis = new FileInputStream(fin);
 
	
	
	//Construct BufferedReader from InputStreamReader
	br = new BufferedReader(new InputStreamReader(fis));
 
	String line = null;
	
	
	
/*	
	int Qty=Integer.parseInt(Exp_quantityOrderedJson);

	System.out.println(" Current item  qty is "+Qty);


	int CaseSize=Integer.parseInt(Exp_itemCaseSize);

	System.out.println("Current item  case size is "+CaseSize);



	int Qt= ((Qty +CaseSize - 1) / CaseSize);

	
	*/
	
	
	
	
	
while ((line = br.readLine()) != null) {
		
		
System.out.println(line);
		
String []Consolidation=	line.split(",");
		
		

		
int  oldQty=0;

String CaseSize=Consolidation[3];

String Type=Consolidation[2];
		
if(hm.get(Consolidation[0])!=null){
		
		
oldQty=Integer.parseInt(hm.get(Consolidation[0]));// retrieve old value
		
		
    }


int newQty=Integer.parseInt(Consolidation[1]);


 
int FinalQty=oldQty+newQty;

/*	
if(Consolidation[2].contentEquals("EA")){
	
	
  String CaseSize=Consolidation[3];	
	
	
  int Qty=FinalQty;
  
  int CaseSizeItem=Integer.parseInt(CaseSize);
  
  
  int Qt= ((Qty +CaseSizeItem - 1) / CaseSizeItem);
	
  FinalQty=Qt;
  
	
}


*/


String Final_qty=String.valueOf(FinalQty);

//String testStr=Final_qty+","+CaseSize+","+Type;


hm.put(Consolidation[0], Final_qty);
	

  }  
	
	
}
	
catch(Exception e){    
		
		
	System.out.println(e);
		
		
}
	
	finally{
	
		
		try {
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	return hm;	
		
		
	}

}



























//******To get the latest file extract generated after batch within 20 mins(latest file )


		
		
		
		//*********To Fetch the Invoice ID from the Shipment file



}
