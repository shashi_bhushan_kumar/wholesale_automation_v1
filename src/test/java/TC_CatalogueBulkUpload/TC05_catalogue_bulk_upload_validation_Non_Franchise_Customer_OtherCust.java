/*
 * Name of the Test Case : TC_01_Web_Portal_Catalogue_Enquiry_Validation
 * Test Case Description : 1.Validate the product should be fetched correctly after clicking on search button.
                           2.Validate that by clicking on the product all details should be displayed correctly.
 * Number of Manual Steps :7
 * Created on : 15/12/2019
 * Created By : Biswarup Sen
 **/
package TC_CatalogueBulkUpload;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.testng.Assert;
import org.testng.annotations.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

import Additional_Functions.McColls_Amazon_RackSpace;
import Additional_Functions.Wholesalewebportalfunctions_Functions;
import Utilities.*;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;



public class TC05_catalogue_bulk_upload_validation_Non_Franchise_Customer_OtherCust {

	String URL;
	//String DynamoURL;
	String DriverPath;
	String DriverName;
	String DriverType;
	String BrowserPath;
	String ServerName;
	String SheetName;
	String Final_Result="";
	String Downloadfilepath="";
	String TestDataPath;
	String location="";
	ExtentReports extent;
	ExtentTest logger;
	Integer stepnum=0;

	String ResultPath=null;
	String downloadFilepath="";
	public WebDriver driver = null;

	public WebDriverWait wait = null;
	//String downloadFilepath = "S:/Mordering 3B/Automation/Files";
	//********************Declaring Environment variable org.apache.commons.logging.Log**********************
	static {
		System.setProperty("org.apache.commons.logging.Log",
				"org.apache.commons.logging.impl.NoOpLog");
	}
	//*******************************************************************************************************
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@BeforeTest
	public void setUp() throws Exception {

		URL=ProjectConfigurations.LoadProperties("Portal_ProjectURL");
		//DynamoURL=ProjectConfigurations.LoadProperties("Relex_DynamoURL");
		DriverPath=ProjectConfigurations.LoadProperties("Portal_DriverPath");
		DriverName=ProjectConfigurations.LoadProperties("Portal_DriverName");
		DriverType=ProjectConfigurations.LoadProperties("Portal_DriverType");
		BrowserPath=ProjectConfigurations.LoadProperties("Portal_BrowserPath");
		ServerName=ProjectConfigurations.LoadProperties("AutomationServerName");
		Downloadfilepath=ProjectConfigurations.LoadProperties("downloadpath");
		//////////////////Setting up user Brwoser////////////////////////////////////////
		System.setProperty(DriverName,DriverPath);
		HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
		chromePrefs.put("profile.default_content_settings.popups", 0);
		chromePrefs.put("download.prompt_for_download", "false");
		chromePrefs.put("download.", "false");
		chromePrefs.put("download.default_directory", System.getProperty("user.dir")+Downloadfilepath);
		System.out.println("Downloadpath"+System.getProperty("user.dir")+Downloadfilepath);
		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("prefs", chromePrefs);
		options.setExperimentalOption("useAutomationExtension", false);
		DesiredCapabilities cap = DesiredCapabilities.chrome();
		cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		cap.setCapability(ChromeOptions.CAPABILITY, options);
		driver = new ChromeDriver(cap);
		wait=new WebDriverWait(driver, 20);	
		System.out.println("URL: "+URL);
		driver.get(URL);


		driver.manage().window().maximize();

		// to be removed..................	

		TestDataPath=ProjectConfigurations.LoadProperties("Portal_TestDataPath");
		SheetName=ProjectConfigurations.LoadProperties("Portal_SheetName");
		downloadFilepath=ProjectConfigurations.LoadProperties("Portal_DownloadPath");
		//				ResultPath="Results/";
		//*********************Setting up Result Path*****************************
			
		if(ResultPath==null){
			ResultPath=Reporting_Utilities.createResultPath("WholesaleAutomation"+getCurrentDate.getISTDateddMM()+"/CatalogueBulk_Cust");
		}
		System.out.println("Result path is "+ResultPath);
		//************************************************************************

		//*********************************Initialising Extent Report Variables**********************************
		extent = new ExtentReports (System.getProperty("user.dir") +ResultPath+"WebServiceSeleniumReport.html", false);
		extent.loadConfig(new File(System.getProperty("user.dir")+"\\extent-config.xml"));
		//*******************************************************************************************************
	}

	@AfterTest
	public void tearDown() throws Exception {
		extent.endTest(logger);
		extent.flush();
		extent.close();	
		driver.quit();

	}

	@Test
	public void test() throws InterruptedException,IOException {
		String TestKeyword = "CatalogueBulk_Cust_NonFranch";
		String TestCaseNo = null;
		String TestCaseName = null;
		String Keyword = null;
		String DriverSheetPath = TestDataPath;
		String driversheetname=SheetName;		
		String rwsusername= null;
		String rwspassword = null;

		String customer=null;

		String filepath=null;
		String errorfilepath=null;
		String channeltype=null;
		String s3username=null;
		String s3password=null;
		String SFTPUserName=null;
		String SFTPHostName=null;
		String SFTPPort=null;
		String SFTPPassword=null;
		String NasPath=null;
		String filename=null;

		String consolidatedScreenshotpath="";
		//-------------------------------------------------

		int r = 0;
		try {


			//-----------------------------------------------
			int rows = 0;
			int occurances = 0;
			Workbook wrk1 = Workbook.getWorkbook(new File(TestDataPath));
			Sheet sheet1 = wrk1.getSheet(SheetName);
			rows = sheet1.getRows();
			Cell[] FirstRow = sheet1.getRow(0);
			Map<String, Integer> map = new HashMap<String, Integer>();
			for(int i=0; i < FirstRow.length; i++)
			{
				map.put(FirstRow[i].getContents().trim(), i);
			}


			for(r=1; r<rows; r++) {
				Keyword = sheet1.getCell(map.get("Keyword"), r).getContents().trim();

				if(occurances>6){
					break;
				}

				if(Keyword.equalsIgnoreCase(TestKeyword)) {
					System.out.println("Keyword: "+Keyword);
					System.out.println("TestKeyword: "+TestKeyword);
					occurances=occurances+1;
					Keyword = sheet1.getCell(map.get("Keyword"), r).getContents().trim();
					TestCaseNo = sheet1.getCell(map.get("TestCaseNo"), r).getContents().trim();
					TestCaseName = sheet1.getCell(map.get("TestCaseName"), r).getContents().trim();

					rwsusername=sheet1.getCell(map.get("poratlUserName"), r).getContents().trim();
					rwspassword=sheet1.getCell(map.get("portalPassword"), r).getContents().trim();
					customer=sheet1.getCell(map.get("customer"),r).getContents().trim();

					filepath=System.getProperty("user.dir")+"/Upload/"+sheet1.getCell(map.get("filename"),r).getContents().trim();
					channeltype=sheet1.getCell(map.get("channeltype"), r).getContents().trim();
					
					SFTPUserName = sheet1.getCell(map.get("SFTPUserName"), r).getContents().trim();
					SFTPHostName = sheet1.getCell(map.get("SFTPHostName"), r).getContents().trim();
					SFTPPort = sheet1.getCell(map.get("SFTPPort"), r).getContents().trim();

					SFTPPassword=sheet1.getCell(map.get("SFTPPassword"), r).getContents().trim();
					NasPath=sheet1.getCell(map.get("NasPath"), r).getContents().trim();
					filename=sheet1.getCell(map.get("filename"), r).getContents().trim();
					filepath=System.getProperty("user.dir")+"/Upload/"+sheet1.getCell(map.get("filename"),r).getContents().trim();

					errorfilepath=System.getProperty("user.dir")+"/Upload/"+sheet1.getCell(map.get("errorfilename"),r).getContents().trim();
					s3username=sheet1.getCell(map.get("s3username"),r).getContents().trim();
					s3password=sheet1.getCell(map.get("s3password"),r).getContents().trim();
					//-----------------------Extent Report-----------
					logger = extent.startTest(TestCaseName);

					//--------------------Doc File---------------------
					XWPFDocument doc = new XWPFDocument();
					XWPFParagraph p = doc.createParagraph();
					XWPFRun xwpfRun = p.createRun();

					//****************************Create Path for Word Document and Screenshots****************************************
					File screenshotpath = new File(System.getProperty("user.dir")+ResultPath+"Screenshot/"+TestCaseNo+"/");
					screenshotpath.mkdirs();
					consolidatedScreenshotpath=System.getProperty("user.dir")+"/"+ResultPath+"Screenshot/"+TestCaseNo+"/"+TestCaseNo+"_";
					//****************************************************************************************************************** 


					//-------------------HTML Header--------------------

					Reporting_Utilities.writeHeaderToHTMLLog(TestCaseNo, TestCaseName, ResultPath);
					utilityFileWriteOP.writeToLog("*********************************START**********************************",ResultPath);	 			 
					utilityFileWriteOP.writeToLog(getCurrentDate.getISTTime(),ResultPath);
					//********************Declaring environment variables for stepnumber*******************

					try{
						stepnum=Integer.parseInt(System.getProperty("stepnumber"));	
					}catch(Exception e){
						System.setProperty("stepnumber", "1");
						stepnum=Integer.parseInt(System.getProperty("stepnumber"));	
					}



					//*****************************************Login to Webportal**********************************
					boolean WebportalLogin1 = Wholesalewebportalfunctions_Functions.wholesale_login(driver, wait, TestCaseNo, rwsusername, rwspassword, ResultPath,xwpfRun, logger);
					if(WebportalLogin1==true) {
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Login and Navigate to Respective Customer", "Navigated to Respective Customer succesfully", "PASS", ResultPath);
						logger.log(LogStatus.PASS, "WholeSale Login successful");

					}else{

						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Navigate to Respective Customer", "Error occured Navigating", "FAIL", ResultPath);
						logger.log(LogStatus.FAIL, "Error occured during loging to Reflexis Workforce Scheduler");
						throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}
					stepnum++;

					//*****************************************Function for Catalogue Bulk Upload **********************************

					boolean WebportalCatelogueBulkupload = Wholesalewebportalfunctions_Functions.Bulkuploadcatalogue_CUST(driver, wait, TestCaseNo, customer,channeltype,errorfilepath,filepath, ResultPath, xwpfRun, logger);
					if(WebportalCatelogueBulkupload==true) {
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Item search for specific customer in catalogue", "item id searched for Respective Customer succesfully", "PASS", ResultPath);
						logger.log(LogStatus.PASS, "Item id and details searched for respective customer");
					}else{

						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Item search for specific customer in catalogue", "Error occured during searching item", "FAIL", ResultPath);
						logger.log(LogStatus.FAIL, "Error occured during searching Item id and details  for respective customer");
						throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}
					stepnum++;
					
					//*****************************************Function for WebPortal Logout **********************************
					
					boolean WebportalLogout= Wholesalewebportalfunctions_Functions.AWW_Logout(driver, wait, TestCaseNo,rwsusername,ResultPath,xwpfRun, logger);
					if(WebportalLogout==true) {
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Logout for user", "user logged out succesfully", "PASS", ResultPath);
						logger.log(LogStatus.PASS, "User logged out successfully");
						driver.quit();
					}else{

						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Logout for user", "Error occured during logout", "FAIL", ResultPath);
						logger.log(LogStatus.FAIL, "Error occured during logout");
						throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}
					stepnum++;

					//*****************************************Function for Nas Validation **********************************
					boolean NASVALIDATION= Wholesalewebportalfunctions_Functions.NASVALIDATION(driver, wait, TestCaseNo,filepath,SFTPUserName,SFTPHostName,Integer.parseInt(SFTPPort),SFTPPassword,NasPath,filename,ResultPath,xwpfRun, logger);
					if(NASVALIDATION==true) {
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Logout for user", "user logged out succesfully", "PASS", ResultPath);
						logger.log(LogStatus.PASS, "User logged out");
					}else{

						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Logout for user", "Error occured during logout", "FAIL", ResultPath);
						logger.log(LogStatus.FAIL, "Error occured during logout");
						throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}
					stepnum++;				

					//*****************************************Function for ASW Login **********************************
					boolean loginAWS = McColls_Amazon_RackSpace.aws_login(driver, wait, TestCaseNo, s3username, s3password,ResultPath,xwpfRun, logger);
					if(loginAWS==true) {
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Login to Amazon Rackspace", "Logged in to Amazon rackspace succesfully", "PASS", ResultPath);
						logger.log(LogStatus.PASS, "AWS login is successful");
					}else{
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Login to Amazon Rackspace", "Error occured in login to Amazon Rackspace", "FAIL", ResultPath);
						logger.log(LogStatus.FAIL, "AWS login is unsuccessful");
						throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}
					stepnum++;	
					//*****************************************Function for ECS TASK Runner **********************************
					boolean runECSTask=McColls_Amazon_RackSpace.Ecs_task_runner_job_trigger_new(driver,wait,TestCaseNo,ResultPath,ResultPath+"Screenshot/"+TestCaseNo+"/", xwpfRun, logger);
					if(runECSTask==true) {
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Run ECS Task", "ECS task  triggered successfully", "PASS", ResultPath);
						logger.log(LogStatus.PASS, "Triggering of ECS task  is successful");
					}else{
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Run ECS Task", "Error occured during running ECS task  Amazon Rackspace", "FAIL", ResultPath);
						logger.log(LogStatus.FAIL, "Error occured during running ECS task  in Amazon Rackspace");
					}	
						stepnum++;

					 
						if (WebportalLogin1 && WebportalCatelogueBulkupload && WebportalLogout && NASVALIDATION && loginAWS && runECSTask){
						Final_Result="PASS";	 	
						logger.log(LogStatus.PASS, "Catalogue bulk upload validation for customer "+customer+" is successful");
						utilityFileWriteOP.writeToLog(TestCaseNo, "Catalogue bulk upload validation for customer "+customer+" is successful", "PASS",ResultPath);
						excelCellValueWrite.writeValueToCell(Final_Result, r, 3, DriverSheetPath, driversheetname);							  
						Assert.assertTrue(true);
						FileOutputStream out1 = new FileOutputStream(consolidatedScreenshotpath+Final_Result+".docx");
						doc.write(out1);
						out1.close();
						doc.close();
					}else {
						Final_Result="FAIL"; 
						logger.log(LogStatus.FAIL, "Catalogue bulk upload validation for customer "+customer+" is not successful");
						excelCellValueWrite.writeValueToCell(Final_Result, r, 3, DriverSheetPath, driversheetname);
						utilityFileWriteOP.writeToLog(TestCaseNo, "Catalogue bulk upload validation for customer "+customer+" is not successful", "FAIL",ResultPath);
						Assert.assertTrue(false);
					}
					extent.endTest(logger);
					extent.flush();
					extent.close();	
				}

			}


		} 

		catch (Exception e) {

			e.printStackTrace();

			Final_Result="FAIL"; 

			excelCellValueWrite.writeValueToCell(Final_Result, r, 3, DriverSheetPath, driversheetname);
			Assert.assertTrue(false);
		}
		finally
		{  

			RowGenerator.collateHTMLLog(TestCaseNo, ResultPath);

			logger.log(LogStatus.INFO, "Evidance link for - "+TestCaseName+" <a href='file:///"+consolidatedScreenshotpath+Final_Result+".docx"+"'>EvidenceLink</a>");
			//Reporting_Utilities.writeCloseRowToHTMLLog(TestCaseNo, ResultPath);

		}}}





