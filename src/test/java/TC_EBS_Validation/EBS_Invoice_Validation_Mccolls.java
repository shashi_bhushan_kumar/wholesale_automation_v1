package TC_EBS_Validation;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.testng.Assert;
import org.testng.annotations.*;

import Utilities.*;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class EBS_Invoice_Validation_Mccolls {

	String SheetName;
	String Final_Result="";
	String TestDataPath;
	ExtentReports extent;
	ExtentTest logger;
	Integer stepnum=0;
	String ResultPath=null;
	public String EBSUFTResultPath="";
	//********************Declaring Environment variable org.apache.commons.logging.Log**********************
	static {
		System.setProperty("org.apache.commons.logging.Log",
		                        "org.apache.commons.logging.impl.NoOpLog");
		}
	//*******************************************************************************************************
	
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}
	
	@BeforeTest
	public void setUp() throws Exception {
		TestDataPath=ProjectConfigurations.LoadProperties("EBS_Testdatapath");
		SheetName=ProjectConfigurations.LoadProperties("Ebs_Sheetname");
				
		//*********************Setting up Result Path*****************************
		if(ResultPath==null){
			ResultPath=Reporting_Utilities.createResultPath("ReflixworkforceSchedular"+getCurrentDate.getISTDateddMM()+"/EBS_Invoice_Validation");
		}
		//************************************************************************
		
		 //*********************************Initialising Extent Report Variables**********************************
		 extent = new ExtentReports (System.getProperty("user.dir") +ResultPath+"WebServiceSeleniumReport.html", false);
		 extent.loadConfig(new File(System.getProperty("user.dir")+"\\extent-config.xml"));
		 //*******************************************************************************************************
	}
	
	@AfterTest
	public void tearDown() throws Exception {
		extent.endTest(logger);
		extent.flush();
		extent.close();		
	}
	
	@Test
	public void test() throws InterruptedException,IOException {
		
		String TestKeyword = "Distribution_tab_Mccolls";
		String TestCaseNo = null;
		String TestCaseName = null;
		String Keyword = null;
		String DriverSheetPath = TestDataPath;
		String driversheetname=SheetName;
		String consolidatedScreenshotpath="";
		String consolidatedParameter= null;	
		String modifiedTestDataPathforUFt= null;
		boolean res = false;
		
		int r = 0;
		try {
			
			int rows = 0;
			int occurances = 0;
			Workbook wrk1 = Workbook.getWorkbook(new File(TestDataPath));
			Sheet sheet1 = wrk1.getSheet(SheetName);
			rows = sheet1.getRows();
			Cell[] FirstRow = sheet1.getRow(0);
			Map<String, Integer> map = new HashMap<String, Integer>();
			for(int i=0; i < FirstRow.length; i++)
			{
				map.put(FirstRow[i].getContents().trim(), i);
			}
			
			for(r=1; r<rows; r++) {
				Keyword = sheet1.getCell(map.get("Keyword"), r).getContents().trim();
				if(occurances>0){
			    	break;
				 }
				if(Keyword.equalsIgnoreCase(TestKeyword)) {
					occurances=occurances+1;
					Keyword = sheet1.getCell(map.get("Keyword"), r).getContents().trim();
					TestCaseNo = sheet1.getCell(map.get("TestCaseNo"), r).getContents().trim();
					TestCaseName = sheet1.getCell(map.get("TestCaseName"), r).getContents().trim();
					logger = extent.startTest(TestCaseName);
					
					try{
						stepnum=Integer.parseInt(System.getProperty("stepnumber"));	
					}catch(Exception e){
						System.setProperty("stepnumber", "1");
						stepnum=Integer.parseInt(System.getProperty("stepnumber"));	
					}
					modifiedTestDataPathforUFt= DriverSheetPath.replace("/", "\\");
					consolidatedParameter= TestKeyword+","+modifiedTestDataPathforUFt+","+driversheetname;
					System.out.println("parameter name "+consolidatedParameter);
				    Process p=  Runtime.getRuntime().exec( "cscript S:/MTEBS/Automation/Selenium_Project_McColls/New_Wholesale_EBS_Project/EBS_InvoiceValidation_UFT_run.vbs"+" "+consolidatedParameter);
				    try {				    	 
						p.waitFor();							
						res=true;														
				    } catch (InterruptedException e) {
						e.printStackTrace();
					  }
				    if (res=true){
				    	logger.log(LogStatus.PASS, "UFT script for EBS validation completed successfully");
				    	String EBSValidationResult= utilityFileWriteOP.ReadUFTResult();
				    	if (EBSValidationResult.contentEquals("PASS")){
				    		logger.log(LogStatus.PASS, "All EBS validation steps completed successfully. See UFT report for detailed result.");
				    	}
				    	else{
				    		logger.log(LogStatus.FAIL, "All EBS validation steps not completed successfully. See UFT report for detailed result");
				    	}
				    	EBSUFTResultPath= utilityFileWriteOP.ReadUFTResultPath();
				    	System.out.println("UFT Result path: "+EBSUFTResultPath);
				    }
				    else{
				    	logger.log(LogStatus.FAIL, "UFT script for EBS validation Failed");
				    }
				    	  
				}
			}
			
		}catch (Exception e) {
			e.printStackTrace();
			Final_Result="FAIL"; 
			excelCellValueWrite.writeValueToCell(Final_Result, r, 3, DriverSheetPath, driversheetname);
			Assert.assertTrue(false);
			}
		
			finally
			{  
			RowGenerator.collateHTMLLog(TestCaseNo, ResultPath);
			logger.log(LogStatus.INFO, "Evidance link for - "+TestCaseName+" <a href='file:///"+EBSUFTResultPath+"'>EvidenceLink</a>");
			}
	}
	
	
}
