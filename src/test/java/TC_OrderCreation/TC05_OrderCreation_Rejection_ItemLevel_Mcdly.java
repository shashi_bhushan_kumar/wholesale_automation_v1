package TC_OrderCreation;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.testng.Assert;
import org.testng.annotations.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import OrderServices_Functions.McColls_OrderServices;
import Utilities.*;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TC05_OrderCreation_Rejection_ItemLevel_Mcdly {

	String SheetName;
	String TestDataPath;
	ExtentReports extent;
	ExtentTest logger;
	Integer stepnum=0;

	String ResultPath="";
	String downloadFilepath="";

	String Final_Result="";

	

	public WebDriver driver = null;

	public WebDriverWait wait = null;


	static {
		System.setProperty("org.apache.commons.logging.Log",
				"org.apache.commons.logging.impl.NoOpLog");
	}
	//*******************************************************************************************************
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@BeforeTest
	public void setUp() throws Exception {

		TestDataPath=ProjectConfigurations.LoadProperties("OrderService_TestDataPath");
		SheetName=ProjectConfigurations.LoadProperties("OrderService_SheetName");
		ResultPath=System.getProperty("resultpath");
		if(ResultPath==null){
			ResultPath=Reporting_Utilities.createResultPath("API_RestCallAutomation"+getCurrentDate.getISTDateddMM()+"/API_WebServiceAutomation");
		}
		System.out.println("Result path is "+ResultPath);

		extent = new ExtentReports (System.getProperty("user.dir") +ResultPath+"WebServiceSeleniumReport.html", false);

		extent.loadConfig(new File(System.getProperty("user.dir")+"\\extent-config.xml"));
		//*******************************************************************************************************
		
		
		
		
		
	}

	@AfterTest
	public void tearDown() throws Exception {
		extent.endTest(logger);
		extent.flush();
		extent.close();	


	}

	@Test
	public void test() throws InterruptedException,IOException {
		String TestKeyword = "OrderCreation_Item_Level_Rejection_Mcdly";
		String TestCaseNo = null;
		String TestCaseName = null;
		String Keyword = null;
		String DriverSheetPath = TestDataPath;
		String driversheetname=SheetName;		

		//--------------------------------------------
		String ProxyHostName = null;
		String ProxyPort = null;
		String SYSUserName = null;
		String SYSPassWord = null;
		String TargetHostName = null;
		String TargetPort = null;
		String TargetHeader = null;
		String UrlTail = null;
		String ApiKey = null;
		String AuthorizationKey = null;
		String AuthorizationValue = null;
		String AllOrderIDs = null;
		String AllMessageTypes = null;
		String AllShipToLocationIds = null;
		String ShipToDeliverAt = null;
		String AllOrderAttributes = null;
		String AllMessageAttributes = null;
		String AllShipToAddressAttributes = null;
		String AllBillToAddressAttributes = null;
		String AllBillToTaxAddressAttributes = null;
		String ColumnNames = null;
		String QuantitytoShip = null;
		String OrderCount = null;
		String ItemIDs = null;
		String EDNUrltail=null;
		
		
		String OrderID=null;

		//--------------------Doc File---------------------
		XWPFDocument doc = new XWPFDocument();
		XWPFParagraph p = doc.createParagraph();
		XWPFRun xwpfRun = p.createRun();

		String consolidatedScreenshotpath="";
		//-------------------------------------------------

		int r = 0;
		try {


			int rows = 0;
			int occurances = 0;
			int ItemDetailsoccurances = 0;
			Workbook wrk1 = Workbook.getWorkbook(new File(TestDataPath));
			Sheet sheet1 = wrk1.getSheet(SheetName);
			rows = sheet1.getRows();
			Cell[] FirstRow = sheet1.getRow(0);
			Map<String, Integer> map = new HashMap<String, Integer>();
			for(int i=0; i < FirstRow.length; i++)
			{
				map.put(FirstRow[i].getContents().trim(), i);
			}

			Sheet sheet2 = wrk1.getSheet("ItemDetails");
			rows = sheet1.getRows();
			int itemdetailsrows = sheet2.getRows();
			
			for(int i=1; i<itemdetailsrows; i++) {
				Keyword = sheet2.getCell(0, i).getContents().trim();
				if(ItemDetailsoccurances>0){
					if(!(TestKeyword.contentEquals(Keyword))){
						break;
					}
				}
			
				if(Keyword.equalsIgnoreCase(TestKeyword)) {

					ItemDetailsoccurances=ItemDetailsoccurances+1;
					ItemIDs = sheet2.getCell(1, i).getContents().trim();
					System.out.println("ItemIDs"+ItemIDs);
				}
			}
			

	  for(r=1; r<rows; r++) {
				Keyword = sheet1.getCell(map.get("Keyword"), r).getContents().trim();
				if(occurances>0){
					break;
				}
				if(Keyword.equalsIgnoreCase(TestKeyword)) {
					
					 boolean OrderIDGenerationValidation=true;
					 Boolean OrderIDGeneration = McColls_OrderServices.OrderIdGenerationI19a(TestDataPath, SheetName, TestKeyword,TestCaseNo, ResultPath);
					 if( (OrderIDGeneration)) {
						 OrderIDGenerationValidation = OrderIDGenerationValidation && true;
						 utilityFileWriteOP.writeToLog("Order ID Generation Script", "Successfully Created Order IDs for "+SheetName,ResultPath);
						 
						 
					 }
					 else {
						OrderIDGenerationValidation = OrderIDGenerationValidation && false;
						utilityFileWriteOP.writeToLog("Order ID Generation Script", "Failed to create Order IDs for "+SheetName,ResultPath);
						
						
						 
					 }		
					
					
					System.out.println("Keyword: "+Keyword);
					System.out.println("TestKeyword: "+TestKeyword);
					occurances=occurances+1;
					Keyword = sheet1.getCell(map.get("Keyword"), r).getContents().trim();
					TestCaseNo = sheet1.getCell(map.get("TestCaseNo"), r).getContents().trim();
					TestCaseName = sheet1.getCell(map.get("TestCaseName"), r).getContents().trim();
					Keyword = sheet1.getCell(map.get("Keyword"), r).getContents().trim();			
					
					//-----------------------Extent Report-----------
					logger = extent.startTest(TestCaseName);
				
					//---------------------------------------------
					

					//OrderID=sheet1.getCell(map.get("OrderID"), r).getContents().trim(); 				
					ProxyHostName = sheet1.getCell(map.get("ProxyHostName"), r).getContents().trim();
					String CatalogueURL= sheet1.getCell(map.get("CatalogueURL"), r).getContents().trim();
					String ShipmentURL= sheet1.getCell(map.get("ShipmentURL"), r).getContents().trim();
					String OrderReferenceCode=sheet1.getCell(map.get("orderReferenceCode"), r).getContents().trim();
					ProxyPort = sheet1.getCell(map.get("ProxyPort"), r).getContents().trim();
					SYSUserName = sheet1.getCell(map.get("SYSUserName"), r).getContents().trim();
					SYSPassWord = sheet1.getCell(map.get("SYSPassWord"), r).getContents().trim();
					TargetHostName = sheet1.getCell(map.get("TargetHostName"), r).getContents().trim();
					TargetPort = sheet1.getCell(map.get("TargetPort"), r).getContents().trim();
					TargetHeader = sheet1.getCell(map.get("TargetHeader"), r).getContents().trim();
					UrlTail = sheet1.getCell(map.get("UrlTail"), r).getContents().trim();
					EDNUrltail=sheet1.getCell(map.get("EDNUrlTail"), r).getContents().trim();
					ApiKey = sheet1.getCell(map.get("ApiKey"), r).getContents().trim();
					AuthorizationKey = sheet1.getCell(map.get("AuthorizationKey"), r).getContents().trim();
					AuthorizationValue = sheet1.getCell(map.get("AuthorizationValue"), r).getContents().trim();
					
					AllOrderIDs = sheet1.getCell(map.get("OrderID"), r).getContents().trim();
					AllMessageTypes = sheet1.getCell(map.get("MessageType"), r).getContents().trim();
					AllShipToLocationIds = sheet1.getCell(map.get("ShipToLocationId"), r).getContents().trim();
					ShipToDeliverAt = sheet1.getCell(map.get("ShipToDeliverAt"), r).getContents().trim();
					AllOrderAttributes = sheet1.getCell(map.get("orderBuyerPartyId,orderSellerPartyId"), r).getContents().trim();
					AllMessageAttributes = sheet1.getCell(map.get("messageId,messageSenderPartyId,messageRecipientPartyId,messageCreatedAt"), r).getContents().trim();
					AllShipToAddressAttributes = sheet1.getCell(map.get("shipToPartyId,shipToAddressName,shipToAddressLine1,shipToAddressLine2,shipToAddressCity,shipToAddressState,shipToAddressPostCode,shipToAddressCountryCode,shipToDeliverLatestAt,shipFromLocationId,shipFromAddressName"), r).getContents().trim();
					AllBillToAddressAttributes = sheet1.getCell(map.get("billToPartyId,billToAddressName,billToAddressLine1,billToAddressLine2,billToAddressCity,billToAddressState,billToAddressPostCode,billToAddressCountryCode"), r).getContents().trim();
					AllBillToTaxAddressAttributes = sheet1.getCell(map.get("billToTaxId,billToTaxAddressName,billToTaxAddressLine1,billToTaxAddressLine2,billToTaxAddressCity,billToTaxAddressState,billToTaxAddressPostCode,billToTaxAddressCountryCode"), r).getContents().trim();
					ColumnNames = sheet1.getCell(map.get("ColumnName"), r).getContents().trim();
					QuantitytoShip = sheet1.getCell(map.get("QuantitytoShip"), r).getContents().trim();
					String quantityOrdered = sheet1.getCell(map.get("QuantityOrdered"), r).getContents().trim();
					OrderCount = sheet1.getCell(map.get("OrderCount"), r).getContents().trim();
					
					//String OrderID[] = AllOrderIDs.split(",");
					String ShipToLocationId[] = AllShipToLocationIds.split(",");
					String MessageType[] = AllMessageTypes.split(",");
					ColumnNames.split(",");
					String AllItemIDs[] = ItemIDs.split(",");
					//String orderReferenceCode = AllOrderAttributes.split(",")[];
					
					
					System.setProperty("orderReferenceCode", OrderReferenceCode);  // no need to keep in system variable
					
					
					
					boolean post = true;
					boolean get = true;
					boolean ship = true;
					boolean edncode = true;
					boolean aftership = true;
					Boolean RestShip=false;
					boolean RestShipppedClosure= false;
					Boolean RestGetAfterShip=false;
					Boolean RestGetItem=true;
					Boolean RestGet=true;
					int order = 0;
					String[] AllItemIds = ItemIDs.split(",");
					String[] AllQuantitiesOrdered = quantityOrdered.split(",");
					System.out.println("Ship to location Id length is "+ShipToLocationId.length);

						
					
					for(int k=0; k<AllItemIds.length; k++) {

						String quantityType = McColls_OrderServices.GetCallquantityTypeFromWebPortal(CatalogueURL,ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue, AllItemIDs[k], TestCaseNo, ResultPath, xwpfRun,logger);
						String RestGetItemDetails = McColls_OrderServices.GetCallItemDetailsFromWebPortal(CatalogueURL,ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue, AllItemIds[k], "skuPin", quantityType, String.valueOf(k+1), AllQuantitiesOrdered[k], TestCaseNo, ResultPath,xwpfRun, logger);
						int itemDetailsPrintOccurence = 0;

						for(int i=1; i<itemdetailsrows; i++) {

							Keyword = sheet2.getCell(0, i).getContents().trim();

							System.out.println(sheet2.getCell(0, i).getContents().trim());
							if(itemDetailsPrintOccurence>0){

								if(!(TestKeyword.contentEquals(Keyword))){
									break;

								}

							}
							if(Keyword.equalsIgnoreCase(TestKeyword)) {

								itemDetailsPrintOccurence=itemDetailsPrintOccurence+1;
								excelCellValueWrite.writeValueToCell(RestGetItemDetails, i, 2+k, TestDataPath, "ItemDetails");


							}
						}


					}
					
					
					
					
					
		
					File screenshotpath = new File(System.getProperty("user.dir")+ResultPath+"Screenshot/"+TestCaseNo+"/");
					screenshotpath.mkdirs();
					consolidatedScreenshotpath=System.getProperty("user.dir")+"/"+ResultPath+"Screenshot/"+TestCaseNo+"/"+TestCaseNo+"_";
					//****************************************************************************************************************** 

					utilityFileWriteOP.writeToLog("*********************************START**********************************",ResultPath);	 			 
					utilityFileWriteOP.writeToLog(getCurrentDate.getISTTime(),ResultPath);

					//********************Declaring environment variables for stepnumber*******************

					try{
						stepnum=Integer.parseInt(System.getProperty("stepnumber"));	
					}catch(Exception e){
						System.setProperty("stepnumber", "1");
						stepnum=Integer.parseInt(System.getProperty("stepnumber"));	
					}

					
					Boolean RestPost = McColls_OrderServices.PostCallOrderCreationGeneralSeperateItemDetails(TestDataPath, "ItemDetails", ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue, AllOrderIDs, MessageType[0], ShipToLocationId[0],OrderCount, ShipToDeliverAt, OrderReferenceCode,AllOrderAttributes, AllMessageAttributes, AllShipToAddressAttributes, AllBillToAddressAttributes, AllBillToTaxAddressAttributes, "", TestKeyword, TestCaseNo,ResultPath,xwpfRun,logger);

					if(RestPost) {
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "1", "Post Order Service Call", "Order Created Successfully.", "PASS", ResultPath);
						logger.log(LogStatus.PASS, "Post Order Service Call:  Order Created Successfully.");
					}

					else{

						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "1", "Post Order Service Call", "Error Occured during Order Creation.", "FAIL", ResultPath);
						logger.log(LogStatus.FAIL, "Post Order Service Call:  Order Creation Failed.");
						throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}

					post = post && RestPost;
				
					//-----------------------Making GET call to validate order item details-----------
					boolean RestGetItemDetails = McColls_OrderServices.GetCallItemLevelRejectionStatusValidation(ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue, AllOrderIDs, "shipped", TestCaseNo,ResultPath,xwpfRun, logger);

					if(RestGetItemDetails==true) {

						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "3", "Get Item Status Validation", "Item Status Validated Successfully.", "PASS", ResultPath);
						logger.log(LogStatus.PASS, "Get Item Status Validation : Item Status Validated Successfully.");
					}

					else{

						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "3", "Get Item Status Validation", "Error Occured during Item Status Validation.", "FAIL", ResultPath);
						logger.log(LogStatus.FAIL, "Get Item Status Validation : Item Status Validation Failed.");
						throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}

					get = get && RestGetItemDetails && RestGet;
					
					
					
			if (RestGet){

				logger.log(LogStatus.PASS, "Successful EDN Shipment validation");
						Final_Result="PASS";	 						 
						utilityFileWriteOP.writeToLog(TestCaseNo, "Get Order events for Order ID "+OrderID, "PASS",ResultPath);
						excelCellValueWrite.writeValueToCell(Final_Result, r, 3, DriverSheetPath, driversheetname);							  

						Assert.assertTrue(true);
					}


					else {
						
						logger.log(LogStatus.FAIL, "Error during EDN Shipment validation");
						Final_Result="FAIL"; 
						excelCellValueWrite.writeValueToCell(Final_Result, r, 3, DriverSheetPath, driversheetname);

						utilityFileWriteOP.writeToLog(TestCaseNo, "Get Order events for Order ID "+OrderID, "FAIL",ResultPath);

						Assert.assertTrue(false);
					}
				}
			}

		} 

		catch (Exception e) {

			e.printStackTrace();

			Final_Result="FAIL"; 
			utilityFileWriteOP.writeToLog(TestCaseNo, "Get Order events for Order ID "+OrderID, "FAIL"+e,ResultPath);
			logger.log(LogStatus.FAIL, "PO Services- GetOrderEvents API validation");

			excelCellValueWrite.writeValueToCell(Final_Result, r, 3, DriverSheetPath, driversheetname);
			Assert.assertTrue(false);
		}
		finally
		{  

			RowGenerator.collateHTMLLog(TestCaseNo, ResultPath);
			FileOutputStream out1 = new FileOutputStream(consolidatedScreenshotpath+Final_Result+".docx");
			doc.write(out1);
			out1.close();
			doc.close();
			logger.log(LogStatus.INFO, "Evidance link for - "+TestCaseName+" <a href='file:///"+consolidatedScreenshotpath+Final_Result+".docx"+"'>EvidenceLink</a>");


		}
	}

}

