/*
 * Name of the Test Case : TC_02_Web_Portal_Order_Enquiry_Advanced_Search_Validation
 * Test Case Description : 1.Validate after clicking on Advanced Search,  Advance Search Criteria for Sales Orders should be displayed.
                           2.Validate user should be able to provide minimum one or maximum two criteria.
                           3. Validate that after clicking on submit button , Search Results for Sales Orders should be fetched.
                           4.Validate that after clicking on sales order number , all details should be fetched correctly on a new tab.
 * Number of Manual Steps :10
 * Created on : 13/12/2019
 * Created By : Biswarup Sen
 **/


package TC_OrderEnquiry;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.testng.Assert;
import org.testng.annotations.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

import Additional_Functions.McColls_Amazon_RackSpace;


import Additional_Functions.Wholesalewebportalfunctions_Functions;
import Utilities.*;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;



public class TC02_Web_Portal_Order_Enquiry_Advanced_Search_Validation {

	String URL;
	//String DynamoURL;
	String DriverPath;
	String DriverName;
	String DriverType;
	String BrowserPath;
	String ServerName;
	String SheetName;
	String Final_Result="";
	String Downloadfilepath="";
	String TestDataPath;
	String location="";
	ExtentReports extent;
	ExtentTest logger;
	Integer stepnum=0;

	String ResultPath=null;
	String downloadFilepath="";
	public WebDriver driver = null;

	public WebDriverWait wait = null;
	//String downloadFilepath = "S:/Mordering 3B/Automation/Files";
	//********************Declaring Environment variable org.apache.commons.logging.Log**********************
	static {
		System.setProperty("org.apache.commons.logging.Log",
				"org.apache.commons.logging.impl.NoOpLog");
	}
	//*******************************************************************************************************
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}



	@BeforeTest
	public void setUp() throws Exception {

		URL=ProjectConfigurations.LoadProperties("Portal_ProjectURL");
		//DynamoURL=ProjectConfigurations.LoadProperties("Relex_DynamoURL");
		DriverPath=ProjectConfigurations.LoadProperties("Portal_DriverPath");
		DriverName=ProjectConfigurations.LoadProperties("Portal_DriverName");
		DriverType=ProjectConfigurations.LoadProperties("Portal_DriverType");
		BrowserPath=ProjectConfigurations.LoadProperties("Portal_BrowserPath");
		ServerName=ProjectConfigurations.LoadProperties("AutomationServerName");
		Downloadfilepath=ProjectConfigurations.LoadProperties("Portal_DownloadPath");
		//////////////////Setting up user Brwoser////////////////////////////////////////
		System.setProperty(DriverName,DriverPath);
		HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
		chromePrefs.put("profile.default_content_settings.popups", 0);
		chromePrefs.put("download.prompt_for_download", "false");
		chromePrefs.put("download.", "false");
		chromePrefs.put("download.default_directory", System.getProperty("user.dir")+Downloadfilepath);
		System.out.println("Downloadpath"+System.getProperty("user.dir")+Downloadfilepath);
		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("prefs", chromePrefs);
		options.setExperimentalOption("useAutomationExtension", false);
		DesiredCapabilities cap = DesiredCapabilities.chrome();
		cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		cap.setCapability(ChromeOptions.CAPABILITY, options);
		driver = new ChromeDriver(cap);
		wait=new WebDriverWait(driver, 20);			
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		System.out.println("URL: "+URL);
		driver.get(URL);


		driver.manage().window().maximize();

		// to be removed..................	

		TestDataPath=ProjectConfigurations.LoadProperties("Portal_TestDataPath");
		SheetName=ProjectConfigurations.LoadProperties("Portal_SheetName");
		downloadFilepath=ProjectConfigurations.LoadProperties("Portal_DownloadPath");
		//				ResultPath="Results/";
		//*********************Setting up Result Path*****************************
		//		ResultPath=System.getProperty("resultpath");
		if(ResultPath==null){
			ResultPath=Reporting_Utilities.createResultPath("WholesaleAutomation"+getCurrentDate.getISTDateddMM()+"/Advance_Order_Search_Cust");
		}
		System.out.println("Result path is "+ResultPath);
		//************************************************************************

		//*********************************Initialising Extent Report Variables**********************************
		extent = new ExtentReports (System.getProperty("user.dir") +ResultPath+"WebServiceSeleniumReport.html", false);
		extent.loadConfig(new File(System.getProperty("user.dir")+"\\extent-config.xml"));
		//*******************************************************************************************************
	}

	@AfterClass
	public void tearDown() throws Exception {
		extent.endTest(logger);
		extent.flush();
		extent.close();	
		//driver.quit();

	}

	@Test
	public void test() throws InterruptedException,IOException {
		String TestKeyword = "Advance_search";
		String TestCaseNo = null;
		String TestCaseName = null;
		String Keyword = null;
		String DriverSheetPath = TestDataPath;
		String driversheetname=SheetName;		
		String rwsusername= null;
		String rwspassword = null;

		String customer=null;
		String orderid=null;
		String orderDate=null;
		String invoicedate=null;


		String consolidatedScreenshotpath="";
		//-------------------------------------------------

		int r = 0;
		try {
			int rows = 0;
			int occurances = 0;
			Workbook wrk1 = Workbook.getWorkbook(new File(TestDataPath));
			Sheet sheet1 = wrk1.getSheet(SheetName);
			rows = sheet1.getRows();
			Cell[] FirstRow = sheet1.getRow(0);
			Map<String, Integer> map = new HashMap<String, Integer>();
			for(int i=0; i < FirstRow.length; i++)
			{
				map.put(FirstRow[i].getContents().trim(), i);
			}


			for(r=1; r<rows; r++) {
				Keyword = sheet1.getCell(map.get("Keyword"), r).getContents().trim();

				if(occurances>7){
					break;
				}

				if(Keyword.equalsIgnoreCase(TestKeyword)) {
					System.out.println("Keyword: "+Keyword);
					System.out.println("TestKeyword: "+TestKeyword);
					occurances=occurances+1;
					Keyword = sheet1.getCell(map.get("Keyword"), r).getContents().trim();
					TestCaseNo = sheet1.getCell(map.get("TestCaseNo"), r).getContents().trim();
					TestCaseName = sheet1.getCell(map.get("TestCaseName"), r).getContents().trim();

					rwsusername=sheet1.getCell(map.get("poratlUserName"), r).getContents().trim();
					rwspassword=sheet1.getCell(map.get("portalPassword"), r).getContents().trim();
					customer=sheet1.getCell(map.get("customer"),r).getContents().trim();
					orderid=sheet1.getCell(map.get("orderid"),r).getContents().trim();
					orderDate =sheet1.getCell(map.get("orderDate"), r).getContents().trim();
					invoicedate =sheet1.getCell(map.get("invoicedate"), r).getContents().trim();
					//-----------------------Extent Report-----------
					logger = extent.startTest(TestCaseName);
					//-----------------------------------------------
					//--------------------Doc File---------------------
					XWPFDocument doc = new XWPFDocument();
					XWPFParagraph p = doc.createParagraph();
					XWPFRun xwpfRun = p.createRun();

					//****************************Create Path for Word Document and Screenshots****************************************
					File screenshotpath = new File(System.getProperty("user.dir")+ResultPath+"Screenshot/"+TestCaseNo+"/");
					screenshotpath.mkdirs();
					consolidatedScreenshotpath=System.getProperty("user.dir")+"/"+ResultPath+"Screenshot/"+TestCaseNo+"/"+TestCaseNo+"_";
					//****************************************************************************************************************** 


					//-------------------HTML Header--------------------

					Reporting_Utilities.writeHeaderToHTMLLog(TestCaseNo, TestCaseName, ResultPath);
					utilityFileWriteOP.writeToLog("*********************************START**********************************",ResultPath);	 			 
					utilityFileWriteOP.writeToLog(getCurrentDate.getISTTime(),ResultPath);
					//********************Declaring environment variables for stepnumber*******************

					try{
						stepnum=Integer.parseInt(System.getProperty("stepnumber"));	
					}catch(Exception e){
						System.setProperty("stepnumber", "1");
						stepnum=Integer.parseInt(System.getProperty("stepnumber"));	
					}

					//*****************************************Login to Webportal**********************************
					boolean WebportalLogin1 = Wholesalewebportalfunctions_Functions.wholesale_login(driver, wait, TestCaseNo, rwsusername, rwspassword, ResultPath,xwpfRun, logger);
					if(WebportalLogin1==true) {
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Navigate to Respective Customer", "Navigated to Respective Customer succesfully", "PASS", ResultPath);
						logger.log(LogStatus.PASS, "WholeSale Login successful and navigated to respective customer");
					}else{

						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Navigate to Respective Customer", "Error occured Navigating", "FAIL", ResultPath);
						logger.log(LogStatus.FAIL, "Error occured during loging to Reflexis Workforce Scheduler");
						throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}
					stepnum++;

					//*****************************************Navigate to Order Enquiry for Advance search**********************************	

					boolean WebportalOrderEN = Wholesalewebportalfunctions_Functions.advancesearchorder(orderDate,driver, wait, TestCaseNo, customer,orderid, ResultPath,xwpfRun, logger);
					if(WebportalOrderEN==true) {
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Navigate to Respective Customer", "Navigated to Respective Customer succesfully", "PASS", ResultPath);

					}else{
						Assert.assertTrue(false);
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Navigate to Respective Customer", "Error occured Navigating", "FAIL", ResultPath);
						logger.log(LogStatus.FAIL, "Error occured");
						throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}
					stepnum++;

					//*****************************************Navigate to Advance search for Invoice **********************************	

					boolean WebportalAdvanceEN = Wholesalewebportalfunctions_Functions.advancesearchinvoice(invoicedate,driver, wait, TestCaseNo,customer, ResultPath,xwpfRun, logger);
					if(WebportalAdvanceEN==true) {
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Order advance search for custome", "Order advance search for customer is successful ", "PASS", ResultPath);
						logger.log(LogStatus.PASS, "Invioce Enquiry is successful");
					}else{
						Assert.assertTrue(false);
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Order advance search for customer", "Order advance search for customer is not successful", "FAIL", ResultPath);
						logger.log(LogStatus.FAIL, "Invioce Enquiry is unsuccessful");
						throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}
					stepnum++;

					//*****************************************Logout from Webportal**********************************

					boolean WebportalLogout= Wholesalewebportalfunctions_Functions.AWW_Logout(driver, wait, TestCaseNo,rwsusername,ResultPath,xwpfRun, logger);
					if(WebportalLogout==true) {
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Logout for user", "user logged out succesfully", "PASS", ResultPath);
						logger.log(LogStatus.PASS, "User logged out successfully");
					}else{

						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Logout for user", "Error occured during logout", "FAIL", ResultPath);
						logger.log(LogStatus.FAIL, "Error occured during logout");
						throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}
					stepnum++;

					if (WebportalLogin1 && WebportalOrderEN && WebportalLogout){
						Final_Result="PASS";
						logger.log(LogStatus.PASS, "Order enquiry for customer done successfully");
						utilityFileWriteOP.writeToLog(TestCaseNo, "Order enquiry for customer done", "PASS",ResultPath);
						excelCellValueWrite.writeValueToCell(Final_Result, r, 3, DriverSheetPath, driversheetname);							  
						Assert.assertTrue(true);
						FileOutputStream out1 = new FileOutputStream(consolidatedScreenshotpath+Final_Result+".docx");
						doc.write(out1);
						out1.close();
						doc.close();
					}else {
						Final_Result="FAIL"; 
						logger.log(LogStatus.FAIL, "Order enquiry for customer  not done in wholesaleportal");
						excelCellValueWrite.writeValueToCell(Final_Result, r, 3, DriverSheetPath, driversheetname);
						utilityFileWriteOP.writeToLog(TestCaseNo, "Order enquiry for customer  not done in wholesaleportal", "FAIL",ResultPath);
						Assert.assertTrue(false);
					}

				}

			}


		} 

		catch (Exception e) {

			e.printStackTrace();

			Final_Result="FAIL"; 

			excelCellValueWrite.writeValueToCell(Final_Result, r, 3, DriverSheetPath, driversheetname);
			Assert.assertTrue(false);
		}
		finally
		{  

			RowGenerator.collateHTMLLog(TestCaseNo, ResultPath);

			logger.log(LogStatus.INFO, "Evidance link for - "+TestCaseName+" <a href='file:///"+consolidatedScreenshotpath+Final_Result+".docx"+"'>EvidenceLink</a>");
			//Reporting_Utilities.writeCloseRowToHTMLLog(TestCaseNo, ResultPath);

		}}}

