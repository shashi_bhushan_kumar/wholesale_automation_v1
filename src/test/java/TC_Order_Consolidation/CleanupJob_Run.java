package TC_Order_Consolidation;




import java.io.FileOutputStream;
import java.io.IOException;



import java.util.concurrent.TimeUnit;




import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;



import OrderServices_Functions.*;
import Utilities.*;



public class CleanupJob_Run {
	
	
	public static ExtentReports extent=null;
	public static ExtentTest logger=null;
	public static String  ResultPath=null;
	public static String  TCFolder=null;
	public static XWPFRun xwpfRun=null;

			String DriverPath;
			String DriverName;
			String DriverType;
			String BrowserPath;
			String ServerName;

			

			
			public static void Prepare(ExtentReports extent1,ExtentTest logger1,String ResultPath1,String TCFolder1,XWPFRun xwpf1){

				extent=extent1;
				logger=logger1;
				ResultPath=ResultPath1;
				TCFolder=TCFolder1;
				xwpfRun=xwpf1;
			}

			
			public static String getResultPath()
			{
		       return ResultPath;

			}


			public static String getTCFolderPath()
			{
		        return TCFolder;

			}

			//********************Declaring Environment variable org.apache.commons.logging.Log**********************
			static {
				System.setProperty("org.apache.commons.logging.Log",
						"org.apache.commons.logging.impl.NoOpLog");
			}
			


	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@BeforeTest
	public void setUp() throws Exception {
		
		//utilityFileWriteOP.writeToLog("*********************************START**********************************");	
		//utilityFileWriteOP.writeToLog(getCurrentDate.getISTTime());
		

		DriverPath=ProjectConfigurations.LoadProperties("McColls_I5a_DriverPath");
		DriverName=ProjectConfigurations.LoadProperties("McColls_I5a_DriverName");
		DriverType=ProjectConfigurations.LoadProperties("McColls_I5a_DriverType");
		BrowserPath=ProjectConfigurations.LoadProperties("McColls_I5a_BrowserPath");
		ServerName=ProjectConfigurations.LoadProperties("AutomationServerName");


		
		System.out.println(BrowserPath);
		
		if(ServerName.equalsIgnoreCase("Server1")){
			
			ResultPath=utilityFileWriteOP.ReadResultPathServer1();
		}
		
		
		if(ServerName.equalsIgnoreCase("Server2")){
			
			ResultPath=utilityFileWriteOP.ReadResultPathServer2();
			
		}

	}

	@AfterTest
	public void tearDown() throws Exception {
		
		//utilityFileWriteOP.writeToLog(getCurrentDate.getISTTime());

		//utilityFileWriteOP.writeToLog("*********************************End**********************************");
		
	}

	@Test
	public void test() throws IOException {

		String TestCaseNo = null;
		String TestCaseName = "Cleanup_Job_Run";

		String ProxyHostName = null;
		String ProxyPort = null;
		String SYSUserName = null;
		String SYSPassWord = null;
		
		String TargetHostName = null;
		String TargetPort = null;
		String TargetHeader = null;
		String UrlTail = null;
		String ApiKey = null;
		String EndParameters = null;
		String AuthorizationKey = null;
		String AuthorizationValue = null;


		
		try{
		
			TestCaseNo = "Cleanup_Job_Run";



	ProxyHostName = ProjectConfigurations.LoadProperties("McColls_I5a_ProxyHostName");
	ProxyPort = ProjectConfigurations.LoadProperties("McColls_I5a_ProxyPort");
	SYSUserName = ProjectConfigurations.LoadProperties("McColls_I5a_SYSUserName");
	SYSPassWord = ProjectConfigurations.LoadProperties("McColls_I5a_SYSPassWord");
	
	TargetHostName = ProjectConfigurations.LoadProperties("McColls_I5a_TargetHostName");
	
	TargetPort = ProjectConfigurations.LoadProperties("McColls_I5a_TargetPort");
	
	TargetHeader = ProjectConfigurations.LoadProperties("McColls_I5a_TargetHeader");
	
	UrlTail = ProjectConfigurations.LoadProperties("McColls_I5a_UrlTail");

	ApiKey = ProjectConfigurations.LoadProperties("McColls_I5a_ApiKey");
	
	EndParameters = ProjectConfigurations.LoadProperties("McColls_I5a_EndParameters");
	
	AuthorizationKey = ProjectConfigurations.LoadProperties("McColls_I5a_AuthorizationKey");
	AuthorizationValue = ProjectConfigurations.LoadProperties("McColls_I5a_AuthorizationValue");

	 Boolean CleanupJobPost = McColls_I5a_PostCall.PostCallCleanupJob_run(ProxyHostName,  Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, EndParameters, AuthorizationKey, AuthorizationValue, TestCaseNo,ResultPath,xwpfRun, logger);
			 
   // PostCallCleanupJob_run(ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, EndParameters, AuthorizationKey, AuthorizationValue, TestCaseNo, ResultPath); 
 
	 System.out.println(CleanupJobPost);
	 
	 Thread.sleep(2000);
	 
	 if(CleanupJobPost==true) {
		 
		 System.out.println("Clean Up Job ran Successfully");
		 Assert.assertTrue(true);
		}
				
	 else{
		 
		 System.out.println("Clean Up Job Failed");
		 throw new MyException("Test Stopped Because of Failure. Please check Execution log");
	
	 }

	 
	

	
	}
		
		
  catch(Exception e) {
	  
			e.printStackTrace();

			// Assert.assertTrue(true);
	   }

		

	}

}
