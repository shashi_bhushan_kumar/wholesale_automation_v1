package TC_Order_Consolidation;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.testng.Assert;
import org.testng.annotations.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

import Additional_Functions.*;
import OrderServices_Functions.Consolidation;
import OrderServices_Functions.McColls_OrderServices;
import Utilities.*;
import Utilities.*;
import Utilities.McColls_Amazon_RackSpace;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TC01_NewOrderCreation_Consolidation {

	String SheetName;
	String TestDataPath;
	ExtentReports extent;
	ExtentTest logger;
	Integer stepnum=0;
	String ResultPath="";
	String downloadFilepath="";

	String Final_Result="";
	public WebDriver driver = null;
	public WebDriverWait wait = null;

	String TCName=null;
	String AppworxURL=null;
	String AppworxUserName=null;
	String AppworxPassword=null;
	

	static {
		System.setProperty("org.apache.commons.logging.Log",
				"org.apache.commons.logging.impl.NoOpLog");
	}
	//*******************************************************************************************************
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@BeforeTest
	public void setUp() throws Exception {

		TestDataPath=ProjectConfigurations.LoadProperties("OrderService_TestDataPath");
		SheetName=ProjectConfigurations.LoadProperties("Consolidation_Sheet");
		ResultPath=System.getProperty("resultpath");
		
		System.out.println("Result path before is "+ResultPath);
		if(ResultPath==null){
//			ResultPath=Reporting_Utilities.createResultPath("WholesaleAutomation"+getCurrentDate.getISTDateddMM()+"/Consolidation");
			ResultPath=Reporting_Utilities.createResultPath("WholesaleAutomation"+getCurrentDate.getISTDateddMM()+"/Consolidation");

		}
		System.out.println("Result path after is "+ResultPath);

		extent = new ExtentReports (System.getProperty("user.dir") +ResultPath+"WebServiceSeleniumReport.html", false);

		extent.loadConfig(new File(System.getProperty("user.dir")+"\\extent-config.xml"));
		//*******************************************************************************************************
		
		
		
		
		
	}

	@AfterTest
	public void tearDown() throws Exception {
		extent.endTest(logger);
		extent.flush();
		//extent.close();	


	}
	/*
	 * 
	 */
	@Test
	public void test() throws InterruptedException,IOException {
		String TestKeyword = "Order_Consol_AM";
		String TestCaseNo = null;
		String TestCaseName = null;
		String Keyword = null;
		String DriverSheetPath = TestDataPath;
		String driversheetname=SheetName;		

		//--------------------------------------------
		String ProxyHostName = null;
		String ProxyPort = null;
		String SYSUserName = null;
		String SYSPassWord = null;
		String TargetHostName = null;
		String TargetPort = null;
		String TargetHeader = null;
		String UrlTail = null;
		String ApiKey = null;
		String AuthorizationKey = null;
		String AuthorizationValue = null;
		String AllOrderIDs = null;
		String AllMessageTypes = null;
		String AllShipToLocationIds = null;
		String ShipToDeliverAt = null;
		String AllOrderAttributes = null;
		String AllMessageAttributes = null;
		String AllShipToAddressAttributes = null;
		String AllBillToAddressAttributes = null;
		String AllBillToTaxAddressAttributes = null;
		String ColumnNames = null;
		String QuantitytoShip = null;
		String OrderCount = null;
		String ItemIDs = null;
		String EDNUrltail=null;
		String OrderID=null;
		String DriverName=null;
		String DriverPath=null;
		String DriverType=null;
		String BrowserPath=null;
		String ECSURL=null;
		String AWSUsername=null;
		String AWSPassword=null;
		
		String s3username=null;
		String s3password=null;
		
		String S3URL="https://rax-598f5024ec80460190f52fc91b01825a.signin.aws.amazon.com/console";
		String searchkey_Consolidation=null;
		String Jobname_Consolidation=null;
		

	
		
		String AllItemIDs=null;
		String Quantities="0";

		String ShipToLocationIds=null;

		String CustomerID="mccolls";
		String ItemTypeList="";

		String TempOrderID_NAS=null;
		
		
		
		
		String ConsolidationDownloadfilepath="";
		

		//--------------------Doc File---------------------
		XWPFDocument doc = new XWPFDocument();
		XWPFParagraph p = doc.createParagraph();
		XWPFRun xwpfRun = p.createRun();

		String consolidatedScreenshotpath="";
		//-------------------------------------------------

		int r = 0;
		try {


			int rows = 0;
			int occurances = 0;
			int ItemDetailsoccurances = 0;
			Workbook wrk1 = Workbook.getWorkbook(new File(TestDataPath));
			Sheet sheet1 = wrk1.getSheet(SheetName);
			rows = sheet1.getRows();
			Cell[] FirstRow = sheet1.getRow(0);
			Map<String, Integer> map = new HashMap<String, Integer>();
			for(int i=0; i < FirstRow.length; i++)
			{
				map.put(FirstRow[i].getContents().trim(), i);
			}

			Sheet sheet2 = wrk1.getSheet("ItemDetails");
			Sheet sheet3=null;
			rows = sheet1.getRows();
			int itemdetailsrows = sheet2.getRows();
			
			for(int i=1; i<itemdetailsrows; i++) {
				Keyword = sheet2.getCell(0, i).getContents().trim();
				if(ItemDetailsoccurances>0){
					if(!(TestKeyword.contentEquals(Keyword))){
						break;
					}
				}
			
				if(Keyword.equalsIgnoreCase(TestKeyword)) {

					ItemDetailsoccurances=ItemDetailsoccurances+1;
					ItemIDs = sheet2.getCell(1, i).getContents().trim();
					System.out.println("ItemIDs"+ItemIDs);
				}
			}
			
			int ecscnt=0;
			for(r=1; r<rows; r++) {
				Keyword = sheet1.getCell(map.get("Keyword"), r).getContents().trim();
			if(occurances>0){
				break;
			}
				if(Keyword.equalsIgnoreCase(TestKeyword)) {
					
					occurances++;
					
					

					//sheet2 = wrk1.getSheet(TestKeyword);

					sheet3 = wrk1.getSheet("Env");	 

					int NoOfRows= sheet2.getRows();
					int NoOfColumns= sheet2.getColumns();

					System.out.println("Total row no in ItemDetails sheet is "+NoOfRows);

					System.out.println("Total column no in ItemDetails sheet is "+NoOfColumns);



					//  NoOfRows=2;


//					String  TempFilePath= sheet3.getCell(0, 0).getContents().trim();

					String  TempFilePath="ConsolidationFileDownload/mccolls-FR-376-20200214-02121059.csv";
					
					
					
					//*************Appworx Batch
					TCName=ProjectConfigurations.LoadProperties("AppworxTC");
					AppworxURL=ProjectConfigurations.LoadProperties("AppworxURLSIT");
					AppworxUserName=ProjectConfigurations.LoadProperties("AppworxUserName");
					AppworxPassword=ProjectConfigurations.LoadProperties("AppworxPassword");
					String firstJobName="WHOLESALE_FILE_CONSOLIDATION";
					
					ConsolidationDownloadfilepath=ProjectConfigurations.LoadProperties("ConsolidationDownloadfilepath");
				/*	
					
					Boolean firstRes=Batch_JobRun.RunJob(AppworxURL,AppworxUserName,AppworxPassword,TCName,ResultPath,firstJobName);

					if(firstRes){
								System.out.println(firstJobName+" job executed successfully ");				
								utilityFileWriteOP.writeToLog("Batch Job Execution", firstJobName, "PASS");
						}

					else{
								System.out.println("Execution failed  for the batch job   "+firstJobName);
								utilityFileWriteOP.writeToLog("Batch Job Execution", firstJobName, "FAIL");
						  }	
					
					//********Order Id Generation
					
					

					 boolean OrderIDGenerationValidation=true;
					 Boolean OrderIDGeneration = McColls_OrderServices.OrderIdGenerationI19a(TestDataPath, SheetName,TestKeyword,TestCaseNo, ResultPath);
					 if( (OrderIDGeneration)) {
						 OrderIDGenerationValidation = OrderIDGenerationValidation && true;
						 utilityFileWriteOP.writeToLog("Order ID Generation Script", "Successfully Created Order IDs for "+SheetName,ResultPath);
						 
					 }
					 else {
						OrderIDGenerationValidation = OrderIDGenerationValidation && false;
						utilityFileWriteOP.writeToLog("Order ID Generation Script", "Failed to create Order IDs for "+SheetName,ResultPath);
						 
					 }
					*/
					System.out.println("occurances: "+occurances);
					System.out.println("Keyword: "+Keyword);
					System.out.println("TestKeyword: "+TestKeyword);
					occurances=occurances+1;
					Keyword = sheet1.getCell(map.get("Keyword"), r).getContents().trim();
					TestCaseNo = sheet1.getCell(map.get("TestCaseNo"), r).getContents().trim();
					TestCaseName = sheet1.getCell(map.get("TestCaseName"), r).getContents().trim();
					Keyword = sheet1.getCell(map.get("Keyword"), r).getContents().trim();	
					String EndParameters=ProjectConfigurations.LoadProperties("McColls_I5a_EndParameters");
					DriverName=ProjectConfigurations.LoadProperties("OrderService_DriverName");
					DriverPath=ProjectConfigurations.LoadProperties("OrderService_DriverPath");
					DriverType=ProjectConfigurations.LoadProperties("OrderService_DriverType");
					BrowserPath=ProjectConfigurations.LoadProperties("OrderService_BrowserPath");
					ECSURL=ProjectConfigurations.LoadProperties("ECSURL");
					AWSUsername=ProjectConfigurations.LoadProperties("AwsUsername");
					System.out.println(AWSUsername);
					AWSPassword=ProjectConfigurations.LoadProperties("Awspassword");
					System.out.println(AWSPassword);
					String SearchKey=ProjectConfigurations.LoadProperties("AWS_SearchKey");
					String ECSTaskName=ProjectConfigurations.LoadProperties("AWS_Jobname");
							
					
					//-----------------------Extent Report-----------
					logger = extent.startTest(TestCaseName);
				
					//---------------------------------------------
					

					//OrderID=sheet1.getCell(map.get("OrderID"), r).getContents().trim(); 				
					ProxyHostName = sheet1.getCell(map.get("ProxyHostName"), r).getContents().trim();
					String CatalogueURL= sheet1.getCell(map.get("CatalogueURL"), r).getContents().trim();
					String ShipmentURL= sheet1.getCell(map.get("ShipmentURL"), r).getContents().trim();
					String OrderReferenceCode=sheet1.getCell(map.get("orderReferenceCode"), r).getContents().trim();
					ProxyPort = sheet1.getCell(map.get("ProxyPort"), r).getContents().trim();
					SYSUserName = sheet1.getCell(map.get("SYSUserName"), r).getContents().trim();
					SYSPassWord = sheet1.getCell(map.get("SYSPassWord"), r).getContents().trim();
					TargetHostName = sheet1.getCell(map.get("TargetHostName"), r).getContents().trim();
					TargetPort = sheet1.getCell(map.get("TargetPort"), r).getContents().trim();
					TargetHeader = sheet1.getCell(map.get("TargetHeader"), r).getContents().trim();
					UrlTail = sheet1.getCell(map.get("UrlTail"), r).getContents().trim();
					EDNUrltail=sheet1.getCell(map.get("EDNUrlTail"), r).getContents().trim();
					ApiKey = sheet1.getCell(map.get("ApiKey"), r).getContents().trim();
					AuthorizationKey = sheet1.getCell(map.get("AuthorizationKey"), r).getContents().trim();
					AuthorizationValue = sheet1.getCell(map.get("AuthorizationValue"), r).getContents().trim();
					
					String CleanupURL=sheet1.getCell(map.get("CleanUpUrlTail"), r).getContents().trim();
					AllOrderIDs = sheet1.getCell(map.get("OrderID"), r).getContents().trim();
					AllMessageTypes = sheet1.getCell(map.get("MessageType"), r).getContents().trim();
					AllShipToLocationIds = sheet1.getCell(map.get("ShipToLocationId"), r).getContents().trim();
					String shipfromlocation = sheet1.getCell(map.get("shipToLocationId"), r).getContents().trim();
					System.out.println(CatalogueURL);
					System.out.println(AllShipToLocationIds);
					System.out.println(shipfromlocation);
					ShipToDeliverAt = sheet1.getCell(map.get("ShipToDeliverAt"), r).getContents().trim();
//					AllOrderAttributes = sheet1.getCell(map.get("orderBuyerPartyId,orderSellerPartyId"), r).getContents().trim();
					AllOrderAttributes = sheet1.getCell(map.get("orderBuyerPartyId,orderSellerPartyId,orderReferenceCode"), r).getContents().trim();
					AllMessageAttributes = sheet1.getCell(map.get("messageId,messageSenderPartyId,messageRecipientPartyId,messageCreatedAt"), r).getContents().trim();
					AllShipToAddressAttributes = sheet1.getCell(map.get("shipToPartyId,shipToAddressName,shipToAddressLine1,shipToAddressLine2,shipToAddressCity,shipToAddressState,shipToAddressPostCode,shipToAddressCountryCode,shipToDeliverLatestAt,shipFromLocationId,shipFromAddressName"), r).getContents().trim();
					AllBillToAddressAttributes = sheet1.getCell(map.get("billToPartyId,billToAddressName,billToAddressLine1,billToAddressLine2,billToAddressCity,billToAddressState,billToAddressPostCode,billToAddressCountryCode"), r).getContents().trim();
					AllBillToTaxAddressAttributes = sheet1.getCell(map.get("billToTaxId,billToTaxAddressName,billToTaxAddressLine1,billToTaxAddressLine2,billToTaxAddressCity,billToTaxAddressState,billToTaxAddressPostCode,billToTaxAddressCountryCode"), r).getContents().trim();
					ColumnNames = sheet1.getCell(map.get("ColumnName"), r).getContents().trim();
					QuantitytoShip = sheet1.getCell(map.get("QuantitytoShip"), r).getContents().trim();
					String quantityOrdered = sheet1.getCell(map.get("QuantityOrdered"), r).getContents().trim();
					OrderCount = sheet1.getCell(map.get("OrderCount"), r).getContents().trim();
					
					
					

					ItemTypeList= sheet1.getCell(map.get("Item Type"), r).getContents().trim();


					AllItemIDs = sheet1.getCell(map.get("ItemID"), r).getContents().trim();
					

					ShipToLocationIds=sheet1.getCell(map.get("shipToLocationId"), r).getContents().trim();


					String [] OrderAttributes=AllOrderAttributes.split(",");

					String OrdRefCode=OrderAttributes[2];

					
					
					searchkey_Consolidation= sheet1.getCell(map.get("Bucket_name"), r).getContents().trim();
					Jobname_Consolidation= sheet1.getCell(map.get("Jobname"), r).getContents().trim();
					
					//String OrderID[] = AllOrderIDs.split(",");
					String ShipToLocationId[] = AllShipToLocationIds.split(",");
					String MessageType[] = AllMessageTypes.split(",");
					ColumnNames.split(",");
//					 AllItemIDs = ItemIDs.split(",");
					//String orderReferenceCode = AllOrderAttributes.split(",")[];

					System.setProperty("orderReferenceCode", OrderReferenceCode);  // no need to keep in system variable
					
					
					
					boolean post = true;
					boolean get = true;
					boolean ship = true;
					boolean edncode = true;
					boolean aftership = true;
					Boolean RestShip=false;
					boolean RestShipppedClosure= false;
					Boolean RestGetAfterShip=false;
					Boolean RestGetItem=true;
					Boolean RestGet=true;
					int order = 0;
					
					Boolean resConsolidation_NegSC=false;
				/*	String[] AllItemIds = ItemIDs.split(",");
					String[] AllQuantitiesOrdered = quantityOrdered.split(",");
					System.out.println("Ship to location Id length is "+ShipToLocationId.length);
					
					
					
						
					for(int k=0; k<AllItemIds.length; k++) {

						String quantityType = McColls_OrderServices.GetCallquantityTypeFromWebPortal(CatalogueURL,ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue, AllItemIDs[k], TestCaseNo, ResultPath, xwpfRun,logger);
						String RestGetItemDetails = McColls_OrderServices.GetCallItemDetailsFromWebPortal(CatalogueURL,ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue, AllItemIds[k], "skuPin", quantityType, String.valueOf(k+1), AllQuantitiesOrdered[k], TestCaseNo, ResultPath, xwpfRun,logger);
						int itemDetailsPrintOccurence = 0;

						for(int i=1; i<itemdetailsrows; i++) {

							Keyword = sheet2.getCell(0, i).getContents().trim();

							System.out.println(sheet2.getCell(0, i).getContents().trim());
							if(itemDetailsPrintOccurence>0){

								if(!(TestKeyword.contentEquals(Keyword))){
									break;

								}

							}
							if(Keyword.equalsIgnoreCase(TestKeyword)) {

								itemDetailsPrintOccurence=itemDetailsPrintOccurence+1;
								excelCellValueWrite.writeValueToCell(RestGetItemDetails, i, 2+k, TestDataPath, "ItemDetails");


							}
						}


					}
					
					
		
					File screenshotpath = new File(System.getProperty("user.dir")+ResultPath+"Screenshot/"+TestCaseNo+"/");
					screenshotpath.mkdirs();
					consolidatedScreenshotpath=System.getProperty("user.dir")+"/"+ResultPath+"Screenshot/"+TestCaseNo+"/"+TestCaseNo+"_";
					
					String TC_ScFolder=ResultPath+TestCaseNo;
					System.out.println("TC_Sc Folder path - "+TC_ScFolder);
					//****************************************************************************************************************** 

					utilityFileWriteOP.writeToLog("*********************************START**********************************",ResultPath);	 			 
					utilityFileWriteOP.writeToLog(getCurrentDate.getISTTime(),ResultPath);

					//********************Declaring environment variables for stepnumber*******************

					try{
						stepnum=Integer.parseInt(System.getProperty("stepnumber"));	
					}catch(Exception e){
						System.setProperty("stepnumber", "1");
						stepnum=Integer.parseInt(System.getProperty("stepnumber"));	
					}
					
					
					Boolean RestPost = McColls_OrderServices.PostCallOrderCreationGeneralSeperateItemDetails(TestDataPath, "ItemDetails", ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue, AllOrderIDs, MessageType[0], ShipToLocationId[0],OrderCount, ShipToDeliverAt, OrderReferenceCode,AllOrderAttributes, AllMessageAttributes, AllShipToAddressAttributes, AllBillToAddressAttributes, AllBillToTaxAddressAttributes, "", TestKeyword, TestCaseNo,ResultPath,xwpfRun,logger);

					if(RestPost) {
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "1", "Post Order Service Call", "Order Created Successfully.", "PASS", ResultPath);
						logger.log(LogStatus.PASS, "Post Order Service Call:  Order Created Successfully.");
					}

					else{

						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "1", "Post Order Service Call", "Error Occured during Order Creation.", "FAIL", ResultPath);
						logger.log(LogStatus.FAIL, "Post Order Service Call:  Order Creation Failed.");
						throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}

					post = post && RestPost;
					
					 
					RestGet = McColls_OrderServices.GetOrderStatusValidation(ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue,AllOrderIDs, "confirmed", TestCaseNo,ResultPath,xwpfRun,logger);


					if(RestGet) {
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "2", "Get Order Service Call", "Order Validated Successfully.", "PASS", ResultPath);
						logger.log(LogStatus.PASS, "Get Order Service Call : Order Validated Successfully.");
					}

					else{
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "2", "Get Order Service Call", "Error Occured during Order Validation.", "FAIL", ResultPath);
						logger.log(LogStatus.FAIL, "Get Order Service Call : Order Validation Failed.");
						throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}

					RestGet=get && RestGet;
					
					//-----------------------Making GET call to validate order item details-----------
					RestGetItem = McColls_OrderServices.GetCallItemStatusValidation(ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail,CatalogueURL, ApiKey, AuthorizationKey, AuthorizationValue, AllOrderIDs, "confirmed", TestCaseNo,ResultPath,xwpfRun, logger);

					if(RestGetItem==true) {

						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "3", "Get Item Status Validation", "Item Status Validated Successfully.", "PASS", ResultPath);
						logger.log(LogStatus.PASS, "Get Item Status Validation : Item Status Validated Successfully.");
					}

					else{

						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "3", "Get Item Status Validation", "Error Occured during Item Status Validation.", "FAIL", ResultPath);
						logger.log(LogStatus.FAIL, "Get Item Status Validation : Item Status Validation Failed.");
						throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}

					get = get && RestGetItem && RestGet;
					
					//S3 login
				
					*/
					System.setProperty(DriverName,DriverPath);
					
				/*	if((DriverType.equalsIgnoreCase("ChromeDriver"))&&(BrowserPath.equalsIgnoreCase("Default"))){
						HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
						chromePrefs.put("profile.default_content_settings.popups", 0);
						chromePrefs.put("download.prompt_for_download", "false");
						chromePrefs.put("download.", "false");
						
						chromePrefs.put("download.default_directory", System.getProperty("user.dir")+ConsolidationDownloadfilepath);
						System.out.println("Downloadpath for Consolidation file from S3 is:"+System.getProperty("user.dir")+ConsolidationDownloadfilepath);
						
						ChromeOptions options = new ChromeOptions();
					//	options.setBinary(ChromeBrowserExePath);

						options.setExperimentalOption("prefs", chromePrefs);
						options.setExperimentalOption("useAutomationExtension", false);
						DesiredCapabilities cap = DesiredCapabilities.chrome();
						cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
						cap.setCapability(ChromeOptions.CAPABILITY, options);
						driver = new ChromeDriver(cap);
						wait=new WebDriverWait(driver, 60);			
						driver.manage().deleteAllCookies();
						driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
						
						wait = new WebDriverWait(driver, 30);
						driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);	
					
						
						driver.manage().deleteAllCookies();
						driver.get(S3URL);
						driver.manage().window().maximize();
					}
					
					boolean logins3 = McColls_Amazon_RackSpace.aws_login(driver, wait, TestCaseNo, AWSUsername, AWSPassword,ResultPath,xwpfRun, logger);
					if(logins3==true) {
						
						logger.log(LogStatus.PASS, "Logged in to Amazon Rackspace");
					}else{

						
						logger.log(LogStatus.FAIL, "Error occured in login to Amazon Rackspace");
					throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}

				boolean runECSTask=McColls_Amazon_RackSpace.Ecs_task_runner_job_trigger_Consolidation(driver,wait,TestCaseNo,ResultPath, xwpfRun, logger, Jobname_Consolidation);
					if(runECSTask==true) {
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Run ECS Task", "ECS task  triggered successfully", "PASS", ResultPath);
						logger.log(LogStatus.PASS, "Triggering of ECS task  is successful");
					}else{
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Run ECS Task", "Error occured during running ECS task  Amazon Rackspace", "FAIL", ResultPath);
						logger.log(LogStatus.FAIL, "Error occured during running ECS task  in Amazon Rackspace");
						throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}	
						stepnum++;
					
					boolean navigateToS3= McColls_Amazon_RackSpace.navigateToS3_Consolidation(driver, wait, TestCaseNo, ResultPath, xwpfRun, logger);
					if(navigateToS3==true) {
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Navigate to S3", "Navigation to S3 is successfull", "PASS", ResultPath);
						logger.log(LogStatus.PASS, "Navigate to S3 console is successful");
					}else{
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Navigate to S3", "Navigation to S3 is not successfull", "FAIL", ResultPath);
						logger.log(LogStatus.FAIL, "Error occured during navigating to S3 console");
						throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}	
						stepnum++;
						
					String DownloadS3file = McColls_Amazon_RackSpace.validateFileExistenceInPending_Consolidation( driver, wait, searchkey_Consolidation, TestCaseNo, ResultPath, xwpfRun, logger);
					if(DownloadS3file!=null) {
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Download S3 file", "Download S3 file is successfull", "PASS", ResultPath);
						logger.log(LogStatus.PASS, "Download S3 file is successful");
						
						excelCellValueWrite.writeValueToCell(DownloadS3file, r, map.get("DownloadS3file"), TestDataPath, SheetName);

					}else{
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Download S3 file", "Download S3 file is not successfull", "FAIL", ResultPath);
						logger.log(LogStatus.FAIL, "Error occured during Download S3 file");
						throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}	
						stepnum++;
*/
						String S3ConsolidationFile=excelCellValueWrite.readValueFromCell(TestKeyword, "DownloadS3file", TestDataPath, SheetName);

						//**********************************
						//Cpnsolidation file validation
						
						//**********
						
						System.out.println("S3ConsolidationFile :"+S3ConsolidationFile);

						HashMap<String, String> hm = null;


						hm = new HashMap<String, String>();

						hm=Consolidation.LoadItemsNAS(TempFilePath,hm);


						System.out.println("Hash Map is "+hm);

						int ItemCount= hm.size();


						System.out.println(ItemCount);

						System.out.println(hm);

//						String LocalPath="TempDataFiles/";
						String LocalPath="ConsolidationFileDownload/";


						List<String> l = new ArrayList<String>(hm.keySet());

						String[] itemsList = new String[l.size()];


						for(int p1=0;p1<l.size();p1++){


							itemsList[p1]=l.get(p1);

						}

						//-------------------HTML Header--------------------

						//----------------------------------------------------

//						ArrayList<HashMap<String, String>> TemporaryOrderIDExtract = Consolidation.S3Content_ConsolidationCSV(S3ConsolidationFile,ShipToDeliverAt,LocalPath,OrdRefCode, ShipToLocationIds, l, ItemTypeList,AllItemIDs,TestCaseNo);
						ArrayList<HashMap<String, String>> TemporaryOrderIDExtract=null;
						System.out.println( TemporaryOrderIDExtract.size());

						String [] ItemsAll_Type=ItemTypeList.split(",");

						String [] ItemsAll=AllItemIDs.split(",");

						String[] ItemsListValid = new String[50];


						for(int k=0;k<ItemsAll_Type.length;k++){


							if(ItemsAll_Type[k].contentEquals("P")){


								ItemsListValid[k]="P";


							}

						}


						//int NoOfRecordsInNAS=0;

						int NoOfValidItems=ItemsAll_Type.length;


						if(NoOfValidItems==itemsList.length){


							// utilityFileWriteOP.writeToLog(TestCaseNo, "Invalid Items not Present in the Consolidated File as expected ","PASS");

							resConsolidation_NegSC=true;

						}

						else {

							//utilityFileWriteOP.writeToLog(TestCaseNo, "Invalid Items are  Present in the Consolidated File  ","FAIL");

							resConsolidation_NegSC=false;


						}


						int cnt=0;

						int PassCount=0;

						for(int p1=0;p1<itemsList.length;p1++){



							for(int u=0;u<TemporaryOrderIDExtract.size();u++){



								String its=TemporaryOrderIDExtract.get(u).get("itemId").toString();



								if(itemsList[p1].contentEquals(its)){

									// shipToLocationId ,CNV ,shipToDeliverAt,itemId,itemAlternateId,itemDescription,quantityType,quantityOrdered

									String qty=TemporaryOrderIDExtract.get(u).get("quantityOrdered").toString();


									String shipToLocationId_NAS=TemporaryOrderIDExtract.get(u).get("shipToLocationId").toString();

									String messageId_NAS=TemporaryOrderIDExtract.get(u).get("messageId").toString();

									//String itemDescription_NAS=TemporaryOrderIDExtract.get(u).get("itemDescription").toString();

									String orderReferenceCode_NAS=TemporaryOrderIDExtract.get(u).get("orderReferenceCode").toString();

									String quantities_NAS=TemporaryOrderIDExtract.get(u).get("quantityOrdered").toString();

									TempOrderID_NAS=TemporaryOrderIDExtract.get(u).get("orderId").toString();

									String customerId_NAS=TemporaryOrderIDExtract.get(u).get("customerId").toString();

									String quantityType_NAS=TemporaryOrderIDExtract.get(u).get("quantityType").toString();

									String ShipToDeliverAt_NAS=TemporaryOrderIDExtract.get(u).get("shipToDeliverAt").toString();

									String createdAt_NAS=TemporaryOrderIDExtract.get(u).get("createdAt").toString();


									String itemDescription_NAS=TemporaryOrderIDExtract.get(u).get("itemDescription").toString();


									String itemAlternateId_NAS=TemporaryOrderIDExtract.get(u).get("itemAlternateId").toString();

									String [] BarcodeEAN=	itemAlternateId_NAS.split("\\|"); 


									if(u==0){



										 

									} 


									//2018-02-26


									String DATE_FORMAT = "yyyy-dd-mm";
									SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
									//  System.out.println("Formated Date " + sdf.format(date));

									System.out.println(ShipToDeliverAt_NAS);



									if(BarcodeEAN[1].contentEquals("clientId:wholesale")){

										PassCount=PassCount+1;

										System.out.println("clientId in NAS  "+ BarcodeEAN[1] +"matches with "+"clientId:wholesale");
										
										logger.log(LogStatus.PASS, "clientId in NAS   "+ BarcodeEAN[1] +"matches with clientId:wholesale"+": Success");


//										utilityFileWriteOP.writeToLog(TestCaseNo, "clientId in NAS   "+ BarcodeEAN[1] +"matches with clientId:wholesale", "Success");

//										utilityFileWriteOP.writeToLog(TestCaseNo, "clientId in NAS   "+ BarcodeEAN[1] +"matches with clientId:wholesale","",ResultPath,xwpfRun,"");



									}


									else {

										System.out.println("clientId in NAS  "+ BarcodeEAN[1] +"not matches with "+"clientId:wholesale");
										
										logger.log(LogStatus.FAIL, "clientId in NAS   "+ BarcodeEAN[1] +"matches with clientId:wholesale"+": Fail");
										/*utilityFileWriteOP.writeToLog(TestCaseNo, "clientId in NAS   "+ BarcodeEAN[1] +"Not matches with clientId:wholesale", "Fail");

										utilityFileWriteOP.writeToLog(TestCaseNo, "clientId in NAS   "+ BarcodeEAN[1] +"Not matches with clientId:wholesale","",ResultPath,xwpfRun,"");
*/

									} 




									if(BarcodeEAN[0].contentEquals("barcodeEan:wholesale")){

										PassCount=PassCount+1;

										System.out.println("barcodeEan in NAS  "+ BarcodeEAN[0] +"matches with "+"barcodeEan:wholesale");
										
										logger.log(LogStatus.PASS,  "barcodeEan in NAS   "+ BarcodeEAN[0] +"matches with barcodeEan:wholesale"+": Success");
/*
										utilityFileWriteOP.writeToLog(TestCaseNo, "barcodeEan in NAS   "+ BarcodeEAN[0] +"matches with barcodeEan:wholesale", "Fail");
										utilityFileWriteOP.writeToLog(TestCaseNo, "barcodeEan in NAS   "+ BarcodeEAN[0] +"matches with barcodeEan:wholesale","",ResultPath,xwpfRun,"");
*/
									}

									else {

										System.out.println("barcodeEan in NAS  "+ BarcodeEAN[0] +"not matches with "+"barcodeEan:wholesale");
										logger.log(LogStatus.FAIL,  "barcodeEan in NAS   "+ BarcodeEAN[0] +" not matches with barcodeEan:wholesale"+": Fail");
										
//										utilityFileWriteOP.writeToLog(TestCaseNo, "barcodeEan in NAS   "+ BarcodeEAN[0] +"not matches with barcodeEan:wholesale", "Success");
//										utilityFileWriteOP.writeToLog(TestCaseNo, "barcodeEan in NAS   "+ BarcodeEAN[0] +"not matches with barcodeEan:wholesale","",ResultPath,xwpfRun,"");
									}

									if(itemDescription_NAS.contentEquals("Optional")){

										PassCount=PassCount+1;

										System.out.println("itemDescription in NAS  "+ itemDescription_NAS +"matches with "+"Optional");
										logger.log(LogStatus.PASS,"itemDescription in NAS  "+ itemDescription_NAS +"matches with Optional"+": Success");
										/*utilityFileWriteOP.writeToLog(TestCaseNo, "itemDescription in NAS  "+ itemDescription_NAS +"matches with Optional", "Success");
										utilityFileWriteOP.writeToLog(TestCaseNo, "itemDescription in NAS  "+ itemDescription_NAS +"matches with Optional","",ResultPath,xwpfRun,"");
*/
									}


									else {

										System.out.println("itemDescription in NAS  "+ itemDescription_NAS +" not matches with "+"Optional");
										logger.log(LogStatus.FAIL,  "barcodeEan in NAS   "+ BarcodeEAN[0] +"Not matches with barcodeEan:wholesale"+": FAIL");
//										utilityFileWriteOP.writeToLog(TestCaseNo, "itemDescription in NAS  "+ itemDescription_NAS +"Not matches with Optional", "FAIL");
//										utilityFileWriteOP.writeToLog(TestCaseNo, "itemDescription in NAS  "+ itemDescription_NAS +"Not matches with Optional","",ResultPath,xwpfRun,"");
									}  	   

									if(ShipToDeliverAt.contentEquals(ShipToDeliverAt_NAS)){

										PassCount=PassCount+1;

										System.out.println("ShipToDeliverAt in NAS  "+ ShipToDeliverAt_NAS +"matches with "+ShipToDeliverAt);
										logger.log(LogStatus.PASS,"ShipToDeliverAt in NAS  "+ ShipToDeliverAt_NAS +"matches with "+ShipToDeliverAt+": Success");
										/*
										utilityFileWriteOP.writeToLog(TestCaseNo, "ShipToDeliverAt in NAS  "+ ShipToDeliverAt_NAS +"matches with "+ShipToDeliverAt, "Success");
										utilityFileWriteOP.writeToLog(TestCaseNo, "ShipToDeliverAt in NAS  "+ ShipToDeliverAt_NAS +"matches with "+ShipToDeliverAt,"",ResultPath,xwpfRun,"");
*/
									}
									else {

										System.out.println("ShipToDeliverAt in NAS  "+ ShipToDeliverAt_NAS +"not matches with "+ShipToDeliverAt);
										logger.log(LogStatus.FAIL, "ShipToDeliverAt in NAS  "+ ShipToDeliverAt_NAS +"not matches with "+ShipToDeliverAt+": FAIL");

										/*utilityFileWriteOP.writeToLog(TestCaseNo, "ShipToDeliverAt in NAS  "+ ShipToDeliverAt_NAS +"not matches with "+ShipToDeliverAt, "FAIL");
										utilityFileWriteOP.writeToLog(TestCaseNo, "ShipToDeliverAt in NAS  "+ ShipToDeliverAt_NAS +"not matches with "+ShipToDeliverAt,"",ResultPath,xwpfRun,"");*/
									} 	

									if(OrdRefCode.contentEquals(orderReferenceCode_NAS)){

										PassCount=PassCount+1;

										System.out.println("orderReferenceCode in NAS  "+ orderReferenceCode_NAS +"matches with "+OrdRefCode);
										logger.log(LogStatus.PASS,"orderReferenceCode in NAS  "+ orderReferenceCode_NAS +"matches with "+OrdRefCode+": Success");

										/*utilityFileWriteOP.writeToLog(TestCaseNo, "orderReferenceCode in NAS  "+ orderReferenceCode_NAS +"matches with "+OrdRefCode, "Success");
										utilityFileWriteOP.writeToLog(TestCaseNo, "orderReferenceCode in NAS  "+ orderReferenceCode_NAS +"matches with "+OrdRefCode,"",ResultPath,xwpfRun,"");*/
									}
									else {

//										utilityFileWriteOP.writeToLog(TestCaseNo, "orderReferenceCode in NAS  "+ orderReferenceCode_NAS +"not matches with "+OrdRefCode, "Fail");
										System.out.println("orderReferenceCode in NAS  "+ orderReferenceCode_NAS +"not matches with "+OrdRefCode);
										logger.log(LogStatus.FAIL, "orderReferenceCode in NAS  "+ orderReferenceCode_NAS +"not matches with "+OrdRefCode+": FAIL");

//										utilityFileWriteOP.writeToLog(TestCaseNo, "orderReferenceCode in NAS  "+ orderReferenceCode_NAS +"not matches with "+OrdRefCode,"",ResultPath,xwpfRun,"");
									}

									if(CustomerID.contentEquals(customerId_NAS)){

										PassCount=PassCount+1;

										System.out.println("CustomerID in NAS  "+ customerId_NAS +"matches with "+CustomerID);
										logger.log(LogStatus.PASS,"CustomerID in NAS  "+ customerId_NAS +"matches with "+CustomerID+": Success");
										
										/*utilityFileWriteOP.writeToLog(TestCaseNo, "CustomerID in NAS  "+ customerId_NAS +"matches with "+CustomerID , "Success");
										utilityFileWriteOP.writeToLog(TestCaseNo, "CustomerID in NAS "+ customerId_NAS +"matches with "+CustomerID,"",ResultPath,xwpfRun,"");*/
									}

									else {

										System.out.println("CustomerID in NAS  "+ CustomerID +"not matches with "+CustomerID);
										logger.log(LogStatus.FAIL, "CustomerID in NAS  "+ customerId_NAS +"not matching  with "+CustomerID+": FAIL");

										/*utilityFileWriteOP.writeToLog(TestCaseNo, "CustomerID in NAS  "+ customerId_NAS +"not matching  with "+CustomerID , "Fail");
										utilityFileWriteOP.writeToLog(TestCaseNo, "CustomerID in NAS "+ customerId_NAS +"not matches with "+CustomerID,"",ResultPath,xwpfRun,"");*/
									}   	

									if(ShipToLocationIds.contentEquals(shipToLocationId_NAS)){

										PassCount=PassCount+1;
										System.out.println("shipToLocationId in NAS  "+ shipToLocationId_NAS +"  matches with "+ShipToLocationIds);
										logger.log(LogStatus.PASS,"shipToLocationId in NAS  "+ shipToLocationId_NAS +"  matches with "+ShipToLocationIds+": Success");

										/*utilityFileWriteOP.writeToLog(TestCaseNo, "shipToLocationId in NAS  "+ shipToLocationId_NAS +"  matches with "+ShipToLocationIds , "Success");
										utilityFileWriteOP.writeToLog(TestCaseNo, "shipToLocationId in NAS  "+ shipToLocationId_NAS +"matches with "+ShipToLocationIds,"",ResultPath,xwpfRun,"");*/
									}

									else {
										System.out.println("shipToLocationId in NAS  "+ shipToLocationId_NAS +"  not matches with "+ShipToLocationIds);
										logger.log(LogStatus.FAIL, "shipToLocationId in NAS  "+ shipToLocationId_NAS +"  not matching  with "+ShipToLocationIds+": FAIL");
										/*
										utilityFileWriteOP.writeToLog(TestCaseNo, "shipToLocationId in NAS  "+ shipToLocationId_NAS +"  not matching  with "+ShipToLocationIds , "Fail");
										utilityFileWriteOP.writeToLog(TestCaseNo, "shipToLocationId in NAS  "+ shipToLocationId_NAS +"not matches with "+ShipToLocationIds,"",ResultPath,xwpfRun,"");*/
									}

									if(quantityType_NAS.contentEquals("CS")){

										PassCount=PassCount+1;
										System.out.println("quantityType in NAS  "+ quantityType_NAS +"  matches with "+"CS");

										logger.log(LogStatus.PASS, "quantityType in NAS  "+ quantityType_NAS +"  matches with "+"CS"+": Success");

										/*utilityFileWriteOP.writeToLog(TestCaseNo, "quantityType in NAS  "+ quantityType_NAS +"  matches with "+"CS" , "Success");
										utilityFileWriteOP.writeToLog(TestCaseNo, "quantityType in NAS  "+ quantityType_NAS +"matches with "+"CS","",ResultPath,xwpfRun,"");*/

									}
									else {
										System.out.println("quantityType in NAS  "+ quantityType_NAS +"  not matches with "+"CS");
										logger.log(LogStatus.FAIL, "quantityType in NAS  "+ quantityType_NAS +"  not matching  with "+"CS"+": FAIL");

										/*utilityFileWriteOP.writeToLog(TestCaseNo, "quantityType in NAS  "+ quantityType_NAS +"  not matching  with "+"CS" , "Fail");
										utilityFileWriteOP.writeToLog(TestCaseNo, "quantityType in NAS  "+ quantityType_NAS +"not matches with "+"CS","",ResultPath,xwpfRun,"");*/
									}

									String  exp_qty=hm.get(its).toString();

									String CS_QtyType= Consolidation.getCaseSize_And_ItemType(TempFilePath, its);

									String [] CS_QtyTypeArray=CS_QtyType.split("#");

									if((CS_QtyTypeArray[0]).contains("EA")){

										int Qt=0; 

										int Qty=Integer.parseInt(exp_qty);

										int CaseSize=Integer.parseInt(CS_QtyTypeArray[1]);

										Qt= ((Qty +CaseSize - 1) / CaseSize);

										exp_qty=String.valueOf(Qt);

									}

									if(exp_qty.contentEquals(qty)){

										PassCount=PassCount+1;

										System.out.println("Consolidated qty  for Item "+its+"is   "   +qty+" and it matches with expected quantity "+exp_qty);
										logger.log(LogStatus.PASS,"Consolidated qty  for Item "+its+"is"   +qty+" and it matches with expected quantity "+exp_qty+": Success");

										/*utilityFileWriteOP.writeToLog(TestCaseNo, "Consolidated qty  for Item "+its+"is"   +qty+" and it matches with expected quantity "+exp_qty , "Success");
										utilityFileWriteOP.writeToLog(TestCaseNo, "Consolidated qty  for Item "+its+"is"   +qty+" and it matches with expected quantity "+exp_qty,"",ResultPath,xwpfRun,"");*/
									}

									else {

										System.out.println("Consolidated qty  for Item "+its+"is"   +qty+" and it is not matching  with expected quantity "+exp_qty);
										logger.log(LogStatus.FAIL, "Consolidated qty  for Item "+its+"is"   +qty+" and it is not matching with expected quantity "+exp_qty+": FAIL");

										/*utilityFileWriteOP.writeToLog(TestCaseNo, "Consolidated qty  for Item "+its+"is"   +qty+" and it is not matching with expected quantity "+exp_qty , "Fail");
										utilityFileWriteOP.writeToLog(TestCaseNo, "Consolidated qty  for Item "+its+"is"   +qty+" and is not matching  with expected quantity "+exp_qty,"",ResultPath,xwpfRun,"");*/
									}

									break;
								}
							}
						}
						System.out.println("pass count"+PassCount);
						System.out.println("items list"+itemsList.length);
						if((PassCount==(9*itemsList.length))){


							//   if((PassCount==(6*itemsList.length))&&(resConsolidation_NegSC)){   
							Final_Result = "PASS";

//							Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "1", "NAS File Validation", "NAS File Validated Successfully.", "PASS", ResultPath);
							excelCellValueWrite.writeValueToCell(Final_Result, r, 3, TestDataPath, SheetName);

							 Assert.assertTrue(true);

								logger.log(LogStatus.PASS,"Consolidated file validation "+": Success");

//							utilityFileWriteOP.writeToLog(TestCaseNo, "Consolidated file validation ", "Success");

							System.out.println("Consolidated file validation PASS");
							
							

//							utilityFileWriteOP.writeToLog(TestCaseNo, "Consolidated file validation PASS","",ResultPath,xwpfRun,"");

						}


						else{

						Final_Result = "FAIL";

							Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "1", "NAS File Validation", "Error Occured during NAS File Validation.", "FAIL", ResultPath);

							excelCellValueWrite.writeValueToCell(Final_Result, r, 3, TestDataPath, SheetName);

							 Assert.assertTrue(false);
								logger.log(LogStatus.FAIL,"Consolidated file validation "+": FAIL");
//							utilityFileWriteOP.writeToLog(TestCaseNo, "Consolidated file validation ", "FAIL");

							System.out.println("Consolidated file validation FAIL");
							
							
//							utilityFileWriteOP.writeToLog(TestCaseNo, "Consolidated file validation FAIL","",ResultPath,xwpfRun,"");
						} 
						
						
						
					
					
					
					
			if (get){

				logger.log(LogStatus.PASS, "Successful EDN Shipment validation");
						Final_Result="PASS";	 						 
						utilityFileWriteOP.writeToLog(TestCaseNo, "Get Order events for Order ID "+OrderID, "PASS",ResultPath);
						excelCellValueWrite.writeValueToCell(Final_Result, r, 3, DriverSheetPath, driversheetname);							  

						Assert.assertTrue(true);
					}


					else {
						
						logger.log(LogStatus.FAIL, "Error during EDN Shipment validation");
						Final_Result="FAIL"; 
						excelCellValueWrite.writeValueToCell(Final_Result, r, 3, DriverSheetPath, driversheetname);

						utilityFileWriteOP.writeToLog(TestCaseNo, "Get Order events for Order ID "+OrderID, "FAIL",ResultPath);

						Assert.assertTrue(false);
					}
				}
			}
		
		} 

		catch (Exception e) {

			e.printStackTrace();

			Final_Result="FAIL"; 
			utilityFileWriteOP.writeToLog(TestCaseNo, "Get Order events for Order ID "+OrderID, "FAIL"+e,ResultPath);
			logger.log(LogStatus.FAIL, "PO Services- GetOrderEvents API validation");

			excelCellValueWrite.writeValueToCell(Final_Result, r, 3, DriverSheetPath, driversheetname);
			Assert.assertTrue(false);
		}
		finally
		{  

			RowGenerator.collateHTMLLog(TestCaseNo, ResultPath);
			FileOutputStream out1 = new FileOutputStream(consolidatedScreenshotpath+Final_Result+".docx");
			doc.write(out1);
			out1.close();
			doc.close();
			logger.log(LogStatus.INFO, "Evidance link for - "+TestCaseName+" <a href='file:///"+consolidatedScreenshotpath+Final_Result+".docx"+"'>EvidenceLink</a>");


		}
	}

}

