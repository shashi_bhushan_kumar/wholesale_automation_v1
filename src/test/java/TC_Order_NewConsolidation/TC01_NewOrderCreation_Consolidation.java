package TC_Order_NewConsolidation;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;



import Additional_Functions.Batch_JobRun;
import OrderServices_Functions.Consolidation;
import OrderServices_Functions.Consolidation_OrderServices;
import Utilities.*;

public class TC01_NewOrderCreation_Consolidation {


	String DriverPath;
	String DriverName;
	String DriverType;
	String BrowserPath;
	String ServerName;
	String SheetName;
	String ItemDetailsSheetName;

	String TestDataPath;
	String TemporaryFilePath;
	String ResultPath="";
	String TCName=null;
	String AppworxURL=null;
	String AppworxUserName=null;
	String AppworxPassword=null;

	String TemporaryConsolidationFilePath="";
	String firstJobName=null;
	String secondJobName=null;

	String Final_Result="";
	public WebDriver driver = null;
	public WebDriverWait wait = null;

	ExtentReports extent;
	ExtentTest logger;

	Integer stepnum=0;
	String AWSUsername=null;
	String AWSPassword=null;

	boolean resConsolidation_NegSC=false;
	String CustomerID="mccolls";

	String TempOrderID_NAS=null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@BeforeTest
	public void setUp() throws Exception {


		utilityFileWriteOP.writeToLog("*********************************START**********************************");	
		utilityFileWriteOP.writeToLog(getCurrentDate.getISTTime());

		//URL=ProjectConfigurations.LoadProperties("SalesforceAutomation_ProjectURL");
		DriverPath=ProjectConfigurations.LoadProperties("OrderService_DriverPath");
		DriverName=ProjectConfigurations.LoadProperties("OrderService_DriverName");
		DriverType=ProjectConfigurations.LoadProperties("OrderService_DriverType");
		BrowserPath=ProjectConfigurations.LoadProperties("OrderService_BrowserPath");
		ServerName=ProjectConfigurations.LoadProperties("AutomationServerName");
		TestDataPath=ProjectConfigurations.LoadProperties("OrderConsolidation_TestDataPath");
		SheetName=ProjectConfigurations.LoadProperties("OrderConsolidation_SheetName");

		TemporaryFilePath=ProjectConfigurations.LoadProperties("OrderService_TemporaryFilePath");
		TCName=ProjectConfigurations.LoadProperties("AppworxTC");
		AppworxURL=ProjectConfigurations.LoadProperties("AppworxURLSIT");
		AppworxUserName=ProjectConfigurations.LoadProperties("AppworxUserName");
		AppworxPassword=ProjectConfigurations.LoadProperties("AppworxPassword");
		firstJobName="WHOLESALE_FILE_CONSOLIDATION";
		secondJobName="FILE_CONSOLIDATION_COPY_EVENT";
		AWSUsername=ProjectConfigurations.LoadProperties("AwsUsername");
		System.out.println(AWSUsername);
		AWSPassword=ProjectConfigurations.LoadProperties("Awspassword");
		System.out.println(AWSPassword);

		System.out.println(BrowserPath);

		ResultPath=System.getProperty("resultpath");

		System.out.println("Result path before is "+ResultPath);
		if(ResultPath==null){
			//			ResultPath=Reporting_Utilities.createResultPath("WholesaleAutomation"+getCurrentDate.getISTDateddMM()+"/Consolidation");
			ResultPath=Reporting_Utilities.createResultPath("WholesaleAutomation"+getCurrentDate.getISTDateddMM()+"/Consolidation");

		}
		System.out.println("Result path after is "+ResultPath);

		extent = new ExtentReports (System.getProperty("user.dir") +ResultPath+"WebServiceSeleniumReport.html", false);

		extent.loadConfig(new File(System.getProperty("user.dir")+"\\extent-config.xml"));
		//*******************************************************************************************************


	}

	@AfterTest
	public void tearDown() throws Exception {
		extent.endTest(logger);
		extent.flush();

		utilityFileWriteOP.writeToLog(getCurrentDate.getISTTime());

		utilityFileWriteOP.writeToLog("*********************************End**********************************");

	}

	@Test
	public void test() throws InterruptedException,IOException {
		String TestCaseNo = null;
		String TestCaseName = null;
		String Keyword = null;
		String Final_Result = "FAIL";
		String TestKeyword = "N_OrderConsolidation";

		String DriverSheetPath = TestDataPath;

		String driversheetname=SheetName;


		String S3URL="https://rax-598f5024ec80460190f52fc91b01825a.signin.aws.amazon.com/console";
		String searchkey_Consolidation=null;
		String Jobname_Consolidation=null;


		String ConsolidationDownloadfilepath="";


		//--------------------Doc File---------------------
		XWPFDocument doc = new XWPFDocument();
		XWPFParagraph p = doc.createParagraph();
		XWPFRun xwpfRun = p.createRun();

		String consolidatedScreenshotpath="";
		//-------------------------------------------------




		//****************************************************
		//Appworx Batch for consolidation file before order id generation


		Boolean firstRes=Batch_JobRun.RunJob(AppworxURL,AppworxUserName,AppworxPassword,TCName,ResultPath,firstJobName);

		if(firstRes){
			System.out.println(firstJobName+" job executed successfully ");				
			utilityFileWriteOP.writeToLog("Batch Job Execution", firstJobName, "PASS");
		}

		else{
			System.out.println("Execution failed  for the batch job   "+firstJobName);
			utilityFileWriteOP.writeToLog("Batch Job Execution", firstJobName, "FAIL");
		}	

		//********Order ID generation

		Consolidation_OrderServices.OrderIdGenerationI5aConsolidation(TestDataPath, TestKeyword, "Test Data: OrderID Creation", ResultPath)	;


		//*************



		ItemDetailsSheetName=TestKeyword;
		String ProxyHostName = null;
		String ProxyPort = null;
		String SYSUserName = null;
		String SYSPassWord = null;

		String TargetHostName = null;
		String TargetPort = null;
		String TargetHeader = null;
		String UrlTail = null;
		String ApiKey = null;
		String AuthorizationKey = null;
		String AuthorizationValue = null;

		String AllOrderIDs = null;
		String AllMessageTypes = null;
		String AllShipToLocationIds = null;
		String ShipToDeliverAt = null;

		String AllOrderAttributes = null;
		String AllMessageAttributes = null;
		String AllShipToAddressAttributes = null;
		String AllBillToAddressAttributes = null;
		String AllBillToTaxAddressAttributes = null;
		String AllItemAttributes = null;

		String SFTPHostName = null;
		String SFTPPort = null;
		String SFTPUserName = null;
		String SFTPPassword = null;
		String NasPath = null;

		String ColumnNames = null;
		String ItemIDs = null;

		String Quantities="0";

		String ItemTypeList=null;
		String ShipToLocationIds=null;
		
		String OrdRefCode=null;
		String AllItemIDs=null;


		//-------------------------------------------------
		String OrderCount="";   // Default value

		int r = 0;
		try {



			int rows = 0;
			int occurances = 0;
			Workbook wrk1 = Workbook.getWorkbook(new File(TestDataPath));
			Sheet sheet1 = wrk1.getSheet(SheetName);
			System.out.println("Sheet 1 name:"+sheet1);
			rows = sheet1.getRows();
			Cell[] FirstRow = sheet1.getRow(0);
			Map<String, Integer> map = new HashMap<String, Integer>();
			for(int i=0; i < FirstRow.length; i++)
			{
				map.put(FirstRow[i].getContents().trim(), i);
			}


			for(r=1; r<rows; r++) {
				System.out.println("First Outer loop counter    :"+r);
				
				Keyword = sheet1.getCell(map.get("TestKeyword"), r).getContents().trim();
				System.out.println("Keyword: "+Keyword);
				System.out.println("TestKeyword: "+TestKeyword);
				TestCaseNo = sheet1.getCell(map.get("TestCaseNo"), r).getContents().trim();
				TestCaseName = sheet1.getCell(map.get("TestCaseName"), r).getContents().trim();


				//-----------------------Extent Report-----------
				logger = extent.startTest(TestCaseName);

				//---------------------------------------------
				
				if(occurances>0){
					break;
				}

				if(Keyword.equalsIgnoreCase(TestKeyword)) {



					Sheet sheet2 = wrk1.getSheet(TestKeyword);	

					Sheet sheet3 = wrk1.getSheet("Env");

					int NoOfRows= sheet2.getRows();

					//	 System.out.println("Hi");

					int NoOfColumns= sheet2.getColumns();


					System.out.println("Total row no in ItemDetails sheet is "+NoOfRows);

					System.out.println("Total column no in ItemDetails sheet is "+NoOfColumns);

					String OrderID=null;

					int Dt  = (int) new Date().getTime();

					String intDt=String.valueOf(Dt);

					String tFileName="TempResultFile/"+intDt+".csv";
					//				    
					//				    Sheet sheet2 = wrk1.getSheet(SheetName);

					System.out.println("The sheet 2 name retrieved is :"+sheet2);

					occurances=occurances+1;



					Cell[] FirstRow1 = sheet2.getRow(0);
					Map<String, Integer> map2 = new HashMap<String, Integer>();
					for(int i=0; i < FirstRow1.length; i++)
					{
						map2.put(FirstRow1[i].getContents().trim(), i);
					}

					for(r=1; r<NoOfRows; r++) {
						//					Keyword = sheet1.getCell(map.get("TestKeyword"), r).getContents().trim();

						System.out.println("Insider loop counter    :"+r);

						OrderID=sheet2.getCell(map2.get("OrderID"), r).getContents().trim();
						ProxyHostName=sheet2.getCell(map2.get("ProxyHostName"), r).getContents().trim();
						ProxyPort = sheet2.getCell(map2.get("ProxyPort"), r).getContents().trim();
						SYSUserName = sheet2.getCell(map2.get("SYSUserName"), r).getContents().trim();
						SYSPassWord = sheet2.getCell(map2.get("SYSPassWord"), r).getContents().trim();

						TargetHostName = sheet2.getCell(map2.get("TargetHostName"), r).getContents().trim();

						TargetPort = sheet2.getCell(map2.get("TargetPort"), r).getContents().trim();

						TargetHeader = sheet2.getCell(map2.get("TargetHeader"), r).getContents().trim();

						UrlTail = sheet2.getCell(map2.get("UrlTail"), r).getContents().trim();

						ApiKey = sheet2.getCell(map2.get("ApiKey"), r).getContents().trim();

						AuthorizationKey = sheet2.getCell(map2.get("AuthorizationKey"), r).getContents().trim();
						AuthorizationValue =sheet2.getCell(map2.get("AuthorizationValue"), r).getContents().trim();




						AllMessageTypes = sheet2.getCell(map2.get("MessageType"), r).getContents().trim();

						AllShipToLocationIds = sheet2.getCell(map2.get("ShipToLocationId"), r).getContents().trim();

						ShipToDeliverAt = sheet2.getCell(map2.get("ShipToDeliverAt"), r).getContents().trim();

						AllOrderAttributes = sheet2.getCell(map2.get("orderBuyerPartyId,orderSellerPartyId,orderReferenceCode"), r).getContents().trim();

						AllMessageAttributes = sheet2.getCell(map2.get("messageId,messageSenderPartyId,messageRecipientPartyId,messageCreatedAt"), r).getContents().trim();

						AllShipToAddressAttributes = sheet2.getCell(map2.get("shipToPartyId,shipToAddressName,shipToAddressLine1,shipToAddressLine2,shipToAddressCity,shipToAddressState,shipToAddressPostCode,shipToAddressCountryCode,shipToDeliverLatestAt,shipFromLocationId,shipFromAddressName"), r).getContents().trim();


						AllBillToAddressAttributes = sheet2.getCell(map2.get("billToPartyId,billToAddressName,billToAddressLine1,billToAddressLine2,billToAddressCity,billToAddressState,billToAddressPostCode,billToAddressCountryCode"), r).getContents().trim();
						AllBillToTaxAddressAttributes = sheet2.getCell(map2.get("billToTaxId,billToTaxAddressName,billToTaxAddressLine1,billToTaxAddressLine2,billToTaxAddressCity,billToTaxAddressState,billToTaxAddressPostCode,billToTaxAddressCountryCode"), r).getContents().trim();

						SFTPHostName = sheet2.getCell(map2.get("SFTPHostName"), r).getContents().trim();
						SFTPPort = sheet2.getCell(map2.get("SFTPPort"), r).getContents().trim();
						SFTPUserName = sheet2.getCell(map2.get("SFTPUserName"), r).getContents().trim();
						SFTPPassword = sheet2.getCell(map2.get("SFTPPassword"), r).getContents().trim();
						NasPath = sheet2.getCell(map2.get("NasPath"), r).getContents().trim();

						ColumnNames = sheet2.getCell(map2.get("ColumnName"), r).getContents().trim();

						AllItemIDs = sheet2.getCell(map2.get("ItemID"), r).getContents().trim();
						String QuantityType=sheet2.getCell(map2.get("quantityType"), r).getContents().trim();
						String AllShipQty = sheet2.getCell(map2.get("QuantityOrdered"), r).getContents().trim();

						ItemTypeList= sheet2.getCell(map2.get("Item Type"), r).getContents().trim();


						searchkey_Consolidation= sheet2.getCell(map2.get("Bucket_name"), r).getContents().trim();
						Jobname_Consolidation= sheet2.getCell(map2.get("Jobname"), r).getContents().trim();


						ShipToLocationIds=sheet2.getCell(map2.get("shipToLocationId"), r).getContents().trim();

						String  ExpOrderStatus=null;


						OrderCount=sheet2.getCell(map2.get("OrderCount"), r).getContents().trim();

						String ItemValidationStatus = sheet2.getCell(map2.get("Item Type"), r).getContents().trim();

						System.out.println("Order "+AllOrderAttributes);
						String [] OrderAttributes=AllOrderAttributes.split(",");
						System.out.println("Order Attributes"+OrderAttributes);
						OrdRefCode=OrderAttributes[2];


						String [] Itemss=AllItemIDs.split(",");


						String qtys[]=QuantityType.split(",");


						String []AllShipQtys=AllShipQty.split(",");


//						System.setProperty(DriverName,DriverPath);

						int itemDetailsPrintOccurence = 0;


						//-------------------HTML Header--------------------

						System.out.println("TestCaseNo :"+TestCaseNo);
						System.out.println("TestCaseName :"+TestCaseName);
						System.out.println("ResultPath :"+ResultPath);
						Reporting_Utilities.writeHeaderToHTMLLog(TestCaseNo, TestCaseName, ResultPath);

						//----------------------------------------------------



						String []ItemValidationStatusList=ItemValidationStatus.split(",");


						int trackingNegative=0;
						int trackingPositive=0;


						for(int t=0;t<ItemValidationStatusList.length;t++){


							if(ItemValidationStatusList[t].contains("N")){

								trackingNegative=trackingNegative+1;

							}

							if(ItemValidationStatusList[t].contains("P")){

								trackingPositive=trackingPositive+1;

							}
						}

						if(trackingPositive==ItemValidationStatusList.length){




							ExpOrderStatus="confirmed";

						}


						else{

							ExpOrderStatus="shipped-partial";


						}


						for(int i=0; i<Itemss.length; i++) {



							String RestGetItemDetails = Consolidation_OrderServices.GetCallItemDetailsFromWebPortal(ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue, Itemss[i], "skuPin",qtys[i], String.valueOf(i+1), AllShipQtys[i], TestCaseNo, ResultPath);


							excelCellValueWrite.writeValueToCell(RestGetItemDetails, r, 32+i, TestDataPath, ItemDetailsSheetName);


						}



						Boolean RestPost = Consolidation_OrderServices.PostCallOrderCreation_Negative_SC(TestDataPath, ItemDetailsSheetName, ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue,OrderID, OrderCount,AllMessageTypes, AllShipToLocationIds, ShipToDeliverAt, AllOrderAttributes, AllMessageAttributes, AllShipToAddressAttributes, AllBillToAddressAttributes, AllBillToTaxAddressAttributes,AllItemIDs, r,TemporaryConsolidationFilePath,Keyword, 

								tFileName,TestCaseNo,ResultPath,ItemValidationStatus,xwpfRun);
						
						
						Boolean OrderStatus= Consolidation_OrderServices.GetOrderStatusValidation(ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue, OrderID,ExpOrderStatus, TestCaseNo,ResultPath,xwpfRun);

						
						Boolean RestGet = Consolidation_OrderServices.GetCallItemStatusValidationWithInvalidItems(ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue,OrderID, "confirmed",ItemValidationStatus, TestCaseNo,ResultPath,xwpfRun);


						if(RestPost && OrderStatus && RestGet) {

							logger.log(LogStatus.PASS, "Order Created Successfully ");
							logger.log(LogStatus.PASS, "Order Status Validated Successfully ");
							logger.log(LogStatus.PASS, "Item Status Validated Successfully ");
						}

						else{

							logger.log(LogStatus.FAIL, "Error Occured during Order Creation");
							throw new MyException("Test Stopped Because of Failure. Please check Execution log");


						}

						System.out.println("Order ID : "+OrderID+"Created ");

					}



						//S3 login

					TestDataPath=ProjectConfigurations.LoadProperties("OrderConsolidation_TestDataPath");
					SheetName=ProjectConfigurations.LoadProperties("OrderConsolidation_SheetName");
					
					
					System.out.println("TestDataPath at start :"+TestDataPath);
					
					System.out.println("SheetName at start :"+SheetName);
						System.setProperty(DriverName,DriverPath);

						if((DriverType.equalsIgnoreCase("ChromeDriver"))&&(BrowserPath.equalsIgnoreCase("Default"))){
							HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
							chromePrefs.put("profile.default_content_settings.popups", 0);
							chromePrefs.put("download.prompt_for_download", "false");
							chromePrefs.put("download.", "false");

							chromePrefs.put("download.default_directory", System.getProperty("user.dir")+"\\"+"TempDataFiles\\");
							System.out.println("Downloadpath for Consolidation file from S3 is:"+System.getProperty("user.dir")+"\\"+"TempDataFiles\\");


							ChromeOptions options = new ChromeOptions();
							//	options.setBinary(ChromeBrowserExePath);

							options.setExperimentalOption("prefs", chromePrefs);
							options.setExperimentalOption("useAutomationExtension", false);
							DesiredCapabilities cap = DesiredCapabilities.chrome();
							cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
							cap.setCapability(ChromeOptions.CAPABILITY, options);
							driver = new ChromeDriver(cap);
							wait=new WebDriverWait(driver, 60);			
							driver.manage().deleteAllCookies();
							driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

							wait = new WebDriverWait(driver, 30);
							driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);	


							driver.manage().deleteAllCookies();
							driver.get(S3URL);
							driver.manage().window().maximize();
						}

						boolean logins3 = McColls_Amazon_RackSpace.aws_login(driver, wait, TestCaseNo, AWSUsername, AWSPassword,ResultPath,xwpfRun, logger);
						if(logins3==true) {

							logger.log(LogStatus.PASS, "Logged in to Amazon Rackspace");
						}else{


							logger.log(LogStatus.FAIL, "Error occured in login to Amazon Rackspace");
							throw new MyException("Test Stopped Because of Failure. Please check Execution log");
						}

						boolean runECSTask=McColls_Amazon_RackSpace.Ecs_task_runner_job_trigger_Consolidation(driver,wait,TestCaseNo,ResultPath, xwpfRun, logger, Jobname_Consolidation);
						if(runECSTask==true) {
							Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Run ECS Task", "ECS task  triggered successfully", "PASS", ResultPath);
							logger.log(LogStatus.PASS, "Triggering of ECS task  is successful");
						}else{
							Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Run ECS Task", "Error occured during running ECS task  Amazon Rackspace", "FAIL", ResultPath);
							logger.log(LogStatus.FAIL, "Error occured during running ECS task  in Amazon Rackspace");
							throw new MyException("Test Stopped Because of Failure. Please check Execution log");
						}	
						stepnum++;

						boolean navigateToS3= McColls_Amazon_RackSpace.navigateToS3_Consolidation(driver, wait, TestCaseNo, ResultPath, xwpfRun, logger);
						if(navigateToS3==true) {
							Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Navigate to S3", "Navigation to S3 is successfull", "PASS", ResultPath);
							logger.log(LogStatus.PASS, "Navigate to S3 console is successful");
						}else{
							Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Navigate to S3", "Navigation to S3 is not successfull", "FAIL", ResultPath);
							logger.log(LogStatus.FAIL, "Error occured during navigating to S3 console");
							throw new MyException("Test Stopped Because of Failure. Please check Execution log");
						}	
						stepnum++;

						String DownloadS3file = McColls_Amazon_RackSpace.validateFileExistenceInPending_Consolidation( driver, wait, searchkey_Consolidation, TestCaseNo, ResultPath, xwpfRun, logger);
						if(DownloadS3file!=null) {
							Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Download S3 file", "Download S3 file is successfull", "PASS", ResultPath);
							logger.log(LogStatus.PASS, "Download S3 file is successful");

							System.out.println("TestDataPath at start :"+TestDataPath);
							
							System.out.println("SheetName at start :"+SheetName);
//							driver.quit();
						

						}else{
							Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Download S3 file", "Download S3 file is not successfull", "FAIL", ResultPath);
							logger.log(LogStatus.FAIL, "Error occured during Download S3 file");
							throw new MyException("Test Stopped Because of Failure. Please check Execution log");
						}	
						stepnum++;


						String S3ConsolidationFile=DownloadS3file;

						System.out.println("S3ConsolidationFile :"+S3ConsolidationFile);
						Thread.sleep(12000);

						//////*******************************
						//Consolidation File validation

						String  TempFilePath= sheet3.getCell(0, 0).getContents().trim();

						System.out.println("TempFilePath   :"+TempFilePath);

						HashMap<String, String> hm = null;


						hm = new HashMap<String, String>();

						hm=Consolidation.LoadTempItemsNAS(TempFilePath,hm);


						System.out.println("Hash Map is "+hm);

						int ItemCount= hm.size();


						System.out.println(ItemCount);

						System.out.println(hm);

						String LocalPath=(System.getProperty("user.dir")+"/"+"TempDataFiles/");

						System.out.println("LocalPath  : "+LocalPath);
						List<String> l = new ArrayList<String>(hm.keySet());

						String[] itemsList = new String[l.size()];


						for(int p1=0;p1<l.size();p1++){


							itemsList[p1]=l.get(p1);

						}
						
						ResultPath=System.getProperty("user.dir")+"/"+ResultPath;

						//						ArrayList<HashMap<String, String>> TemporaryOrderIDExtract = McColls_I5a_Utilities_NAS.NASContent_RetriveNegativeSc(SFTPHostName, Integer.parseInt(SFTPPort), SFTPUserName, SFTPPassword, NasPath,ShipToDeliverAt,LocalPath,OrdRefCode, ShipToLocationIds, l, ItemTypeList,AllItemIDs,TestCaseNo);
						ArrayList<HashMap<String, String>> TemporaryOrderIDExtract = Consolidation.S3Content_ConsolidationCSV(S3ConsolidationFile,ShipToDeliverAt,LocalPath,OrdRefCode, ShipToLocationIds, l, ItemTypeList,AllItemIDs,TestCaseNo,ResultPath, xwpfRun, logger);

						System.out.println( TemporaryOrderIDExtract.size());

						String [] ItemsAll_Type=ItemTypeList.split(",");

						String [] ItemsAll=AllItemIDs.split(",");

						String[] ItemsListValid = new String[50];


						for(int k=0;k<ItemsAll_Type.length;k++){


							if(ItemsAll_Type[k].contentEquals("P")){


								ItemsListValid[k]="P";


							}

						}


						//int NoOfRecordsInNAS=0;

						int NoOfValidItems=ItemsAll_Type.length;


						if(NoOfValidItems==itemsList.length){


							// utilityFileWriteOP.writeToLog(TestCaseNo, "Invalid Items not Present in the Consolidated File as expected ","PASS");

							resConsolidation_NegSC=true;

						}

						else {

							//utilityFileWriteOP.writeToLog(TestCaseNo, "Invalid Items are  Present in the Consolidated File  ","FAIL");

							resConsolidation_NegSC=false;


						}


						int cnt=0;

						int PassCount=0;

						for(int p1=0;p1<itemsList.length;p1++){



							for(int u=0;u<TemporaryOrderIDExtract.size();u++){



								String its=TemporaryOrderIDExtract.get(u).get("itemId").toString();



								if(itemsList[p1].contentEquals(its)){

									// shipToLocationId ,CNV ,shipToDeliverAt,itemId,itemAlternateId,itemDescription,quantityType,quantityOrdered

									String qty=TemporaryOrderIDExtract.get(u).get("quantityOrdered").toString();


									String shipToLocationId_NAS=TemporaryOrderIDExtract.get(u).get("shipToLocationId").toString();

									String messageId_NAS=TemporaryOrderIDExtract.get(u).get("messageId").toString();

									//String itemDescription_NAS=TemporaryOrderIDExtract.get(u).get("itemDescription").toString();

									String orderReferenceCode_NAS=TemporaryOrderIDExtract.get(u).get("orderReferenceCode").toString();

									String quantities_NAS=TemporaryOrderIDExtract.get(u).get("quantityOrdered").toString();

									TempOrderID_NAS=TemporaryOrderIDExtract.get(u).get("orderId").toString();

									String customerId_NAS=TemporaryOrderIDExtract.get(u).get("customerId").toString();

									String quantityType_NAS=TemporaryOrderIDExtract.get(u).get("quantityType").toString();

									String ShipToDeliverAt_NAS=TemporaryOrderIDExtract.get(u).get("shipToDeliverAt").toString();

									String createdAt_NAS=TemporaryOrderIDExtract.get(u).get("createdAt").toString();


									String itemDescription_NAS=TemporaryOrderIDExtract.get(u).get("itemDescription").toString();


									String itemAlternateId_NAS=TemporaryOrderIDExtract.get(u).get("itemAlternateId").toString();

									String [] BarcodeEAN=	itemAlternateId_NAS.split("\\|"); 



									String DATE_FORMAT = "yyyy-dd-mm";
									SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
									//  System.out.println("Formated Date " + sdf.format(date));

									System.out.println(ShipToDeliverAt_NAS);



									if(BarcodeEAN[1].contentEquals("clientId:wholesale")){

										PassCount=PassCount+1;

										System.out.println("clientId in S3  "+ BarcodeEAN[1] +"matches with "+"clientId:wholesale");

										logger.log(LogStatus.PASS, "clientId in S3   "+ BarcodeEAN[1] +"matches with clientId:wholesale"+": Success");


									}


									else {

										System.out.println("clientId in S3  "+ BarcodeEAN[1] +"not matches with "+"clientId:wholesale");

										logger.log(LogStatus.FAIL, "clientId in S3   "+ BarcodeEAN[1] +"matches with clientId:wholesale"+": Fail");

									} 




									if(BarcodeEAN[0].contentEquals("barcodeEan:wholesale")){

										PassCount=PassCount+1;

										System.out.println("barcodeEan in S3  "+ BarcodeEAN[0] +"matches with "+"barcodeEan:wholesale");

										logger.log(LogStatus.PASS,  "barcodeEan in S3   "+ BarcodeEAN[0] +"matches with barcodeEan:wholesale"+": Success");

									}

									else {

										System.out.println("barcodeEan in S3  "+ BarcodeEAN[0] +"not matches with "+"barcodeEan:wholesale");
										logger.log(LogStatus.FAIL,  "barcodeEan in S3   "+ BarcodeEAN[0] +" not matches with barcodeEan:wholesale"+": Fail");

									}

									if(itemDescription_NAS.contentEquals("Optional")){

										PassCount=PassCount+1;

										System.out.println("itemDescription in S3  "+ itemDescription_NAS +"matches with "+"Optional");
										logger.log(LogStatus.PASS,"itemDescription in S3  "+ itemDescription_NAS +"matches with Optional"+": Success");

									}


									else {

										System.out.println("itemDescription in S3  "+ itemDescription_NAS +" not matches with "+"Optional");
										logger.log(LogStatus.FAIL,  "barcodeEan in S3   "+ BarcodeEAN[0] +"Not matches with barcodeEan:wholesale"+": FAIL");
									}  	   

									if(ShipToDeliverAt.contentEquals(ShipToDeliverAt_NAS)){

										PassCount=PassCount+1;

										System.out.println("ShipToDeliverAt in S3  "+ ShipToDeliverAt_NAS +"matches with "+ShipToDeliverAt);
										logger.log(LogStatus.PASS,"ShipToDeliverAt in S3  "+ ShipToDeliverAt_NAS +"matches with "+ShipToDeliverAt+": Success");

									}
									else {

										System.out.println("ShipToDeliverAt in S3  "+ ShipToDeliverAt_NAS +"not matches with "+ShipToDeliverAt);
										logger.log(LogStatus.FAIL, "ShipToDeliverAt in S3  "+ ShipToDeliverAt_NAS +"not matches with "+ShipToDeliverAt+": FAIL");
									} 	

									if(OrdRefCode.contentEquals(orderReferenceCode_NAS)){

										PassCount=PassCount+1;

										System.out.println("orderReferenceCode in S3  "+ orderReferenceCode_NAS +"matches with "+OrdRefCode);
										logger.log(LogStatus.PASS,"orderReferenceCode in S3  "+ orderReferenceCode_NAS +"matches with "+OrdRefCode+": Success");
									}
									else {

										System.out.println("orderReferenceCode in S3  "+ orderReferenceCode_NAS +"not matches with "+OrdRefCode);
										logger.log(LogStatus.FAIL, "orderReferenceCode in S3  "+ orderReferenceCode_NAS +"not matches with "+OrdRefCode+": FAIL");

									}

									if(CustomerID.contentEquals(customerId_NAS)){

										PassCount=PassCount+1;

										System.out.println("CustomerID in S3  "+ customerId_NAS +"matches with "+CustomerID);
										logger.log(LogStatus.PASS,"CustomerID in S3  "+ customerId_NAS +"matches with "+CustomerID+": Success");

									}

									else {

										System.out.println("CustomerID in S3  "+ CustomerID +"not matches with "+CustomerID);
										logger.log(LogStatus.FAIL, "CustomerID in S3  "+ customerId_NAS +"not matching  with "+CustomerID+": FAIL");


									}   	

									if(ShipToLocationIds.contentEquals(shipToLocationId_NAS)){

										PassCount=PassCount+1;
										System.out.println("shipToLocationId in S3  "+ shipToLocationId_NAS +"  matches with "+ShipToLocationIds);
										logger.log(LogStatus.PASS,"shipToLocationId in S3  "+ shipToLocationId_NAS +"  matches with "+ShipToLocationIds+": Success");


									}

									else {
										System.out.println("shipToLocationId in S3  "+ shipToLocationId_NAS +"  not matches with "+ShipToLocationIds);
										logger.log(LogStatus.FAIL, "shipToLocationId in S3  "+ shipToLocationId_NAS +"  not matching  with "+ShipToLocationIds+": FAIL");

									}

									if(quantityType_NAS.contentEquals("CS")){

										PassCount=PassCount+1;
										System.out.println("quantityType in S3  "+ quantityType_NAS +"  matches with "+"CS");

										logger.log(LogStatus.PASS, "quantityType in S3  "+ quantityType_NAS +"  matches with "+"CS"+": Success");

										

									}
									else {
										System.out.println("quantityType in S3  "+ quantityType_NAS +"  not matches with "+"CS");
										logger.log(LogStatus.FAIL, "quantityType in S3  "+ quantityType_NAS +"  not matching  with "+"CS"+": FAIL");

									}

									String  exp_qty=hm.get(its).toString();

									String CS_QtyType= Consolidation.getCaseSize_And_ItemType(TempFilePath, its);

									String [] CS_QtyTypeArray=CS_QtyType.split("#");

									if((CS_QtyTypeArray[0]).contains("EA")){

										int Qt=0; 

										int Qty=Integer.parseInt(exp_qty);

										int CaseSize=Integer.parseInt(CS_QtyTypeArray[1]);

										Qt= ((Qty +CaseSize - 1) / CaseSize);

										exp_qty=String.valueOf(Qt);

									}

									if(exp_qty.contentEquals(qty)){

										PassCount=PassCount+1;

										System.out.println("Consolidated qty  for Item "+its+"is   "   +qty+" and it matches with expected quantity "+exp_qty);
										logger.log(LogStatus.PASS,"Consolidated qty  for Item "+its+"is"   +qty+" and it matches with expected quantity "+exp_qty+": Success");


									}

									else {

										System.out.println("Consolidated qty  for Item "+its+"is"   +qty+" and it is not matching  with expected quantity "+exp_qty);
										logger.log(LogStatus.FAIL, "Consolidated qty  for Item "+its+"is"   +qty+" and it is not matching with expected quantity "+exp_qty+": FAIL");


									}

									break;
								}
							}
						}
						System.out.println("pass count"+PassCount);
						System.out.println("items list"+itemsList.length);
						

						//****************************************************
						//Appworx Batch for consolidation file movement from S3 to NAS

						
						
						System.out.println("Result path at last appworx batch is:"+ResultPath);

						Boolean secondRes=Batch_JobRun.RunJob(AppworxURL,AppworxUserName,AppworxPassword,TCName,ResultPath,secondJobName);

						if(secondRes){
							System.out.println(secondJobName+" job executed successfully ");				
							utilityFileWriteOP.writeToLog("Batch Job Execution", secondJobName, "PASS");
						}

						else{
							System.out.println("Execution failed  for the batch job   "+secondJobName);
							utilityFileWriteOP.writeToLog("Batch Job Execution", secondJobName, "FAIL");
						}
						
					
						Boolean NasFileValidate = WinSCPCall.ValidateConsolidationFile_At_Nas(SFTPUserName, SFTPPassword, DownloadS3file, NasPath, SFTPHostName, SFTPPort, TestCaseNo,ResultPath, xwpfRun, logger);
						if(NasFileValidate) {
							System.out.println("Result path at last NAS validation is:"+ResultPath);
							Reporting_Utilities.writeFinalStepToHTMLLog(TestCaseNo, stepnum.toString(), "Validate Consolidation file at NAS", "Validation of Consolidation file at NAS is successfull", "PASS", ResultPath);
							logger.log(LogStatus.PASS, "Validation of Consolidation file at NAS is successful");

							System.out.println("TestDataPath at start :"+TestDataPath);
							
							System.out.println("SheetName at start :"+SheetName);
							driver.quit();
						

						}else{
							System.out.println("Result path at last NAS validation is:"+ResultPath);
							Reporting_Utilities.writeFinalStepToHTMLLog(TestCaseNo, stepnum.toString(), "Validate Consolidation file at NAS", "Validation of Consolidation file at NAS is not successfull", "FAIL", ResultPath);
							logger.log(LogStatus.FAIL, "Error occured during validation of Consolidation file at NAS ");
							throw new MyException("Test Stopped Because of Failure. Please check Execution log");
						}	
						
						
						
						if((PassCount==(9*itemsList.length)) && NasFileValidate){


							//							   if((PassCount==(6*itemsList.length))&&(resConsolidation_NegSC)){   
							Final_Result = "PASS";
							
							System.out.println("TestDataPath at start :"+TestDataPath);
							
							System.out.println("SheetName at start :"+SheetName);
							
							System.out.println("Final_Result at end :"+Final_Result);
							
							System.out.println("Value of r:"+r);
							
							

//							excelCellValueWrite.writeValueToCell(Final_Result, r, 3, TestDataPath, SheetName);

							Assert.assertTrue(true);

							logger.log(LogStatus.PASS,"Consolidated file validation "+": Success");


							System.out.println("Consolidated file validation PASS");

						}
						

						else{

							Final_Result = "FAIL";
							
							System.out.println("TestDataPath at start :"+TestDataPath);
							
							System.out.println("SheetName at start :"+SheetName);

//							excelCellValueWrite.writeValueToCell(Final_Result, r, 3, TestDataPath, SheetName);

							Assert.assertTrue(false);
							logger.log(LogStatus.FAIL,"Consolidated file validation "+": FAIL");
							//							utilityFileWriteOP.writeToLog(TestCaseNo, "Consolidated file validation ", "FAIL");

							System.out.println("Consolidated file validation FAIL");


							//							utilityFileWriteOP.writeToLog(TestCaseNo, "Consolidated file validation FAIL","",ResultPath,xwpfRun,"");
						}



						Final_Result = "PASS";
						logger.log(LogStatus.PASS,"Final result is PASS");
						Assert.assertTrue(true);

					}


				}

			

		}


		catch(Exception e) {
			e.printStackTrace();



			Final_Result = "FAIL";
			logger.log(LogStatus.FAIL,"Final result is FAIL");
//			excelCellValueWrite.writeValueToCell(Final_Result, r, 3, DriverSheetPath, driversheetname);

			Assert.assertTrue(false);
		}


	}

}