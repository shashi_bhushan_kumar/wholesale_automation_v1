package TC_PushToDHL;
//52,53,54
/*
 * 	Status in torderheader:Raised in case of PO creation
	Items inserted in torderitem from ApprovedPORequests.json file
	"Event created in teventlog: Raised in case of PO creation

 */
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import Utilities.*;


public class TC02_PUSH2DHL_OrderAutoshipped {
	//************************************Declaring Variables********************************
	String SheetName;
	String Final_Result="";
	String TestDataPath;
	ExtentReports extent;
	ExtentTest logger;
	Integer stepnum=0;

	String ResultPath="";
	String downloadFilepath="";
	public WebDriver driver = null;

	public WebDriverWait wait = null;

	//********************Declaring Environment variable org.apache.commons.logging.Log**********************
	static {
		System.setProperty("org.apache.commons.logging.Log",
				"org.apache.commons.logging.impl.NoOpLog");
	}
	//*******************************************************************************************************



	@BeforeTest
	public void setUp() throws Exception {


		// to be removed..................	

		TestDataPath=ProjectConfigurations.LoadProperties("Portal_TestDataPath");
		SheetName=ProjectConfigurations.LoadProperties("Portal_SheetName");

		//				ResultPath="Results/";
		//*********************Setting up Result Path*****************************
		ResultPath=System.getProperty("resultpath");
		if(ResultPath==null){
			ResultPath=Reporting_Utilities.createResultPath("PO_Response_RMS_STG_"+getCurrentDate.getISTDateddMM()+"/trmsrequestadapterRDS");
		}
		System.out.println("Result path is "+ResultPath);
		//************************************************************************

		//*********************************Initialising Extent Report Variables**********************************
		extent = new ExtentReports (System.getProperty("user.dir") +ResultPath+"CreatePOSeleniumReport.html", false);
		extent.loadConfig(new File(System.getProperty("user.dir")+"\\extent-config.xml"));
		//*******************************************************************************************************

	}

	@AfterTest
	public void tearDown() throws Exception {

		extent.endTest(logger);
		extent.flush();
		extent.close();
	}

	@Test
	public void test() throws InterruptedException,IOException {
		String TestKeyword = "PUSH_DHL_in_RDS_Autoshipped";
		String TestCaseNo = null;
		String TestCaseName = null;
		String Keyword = null;
		String DriverSheetPath = TestDataPath;
		String driversheetname=SheetName;		

		String RDSusername=null, RDSpassword=null, RDShostName=null, RDSport=null;
		String orderid=null;
		String itemid=null;
		//String itemid=null;
		//String jobid=null;
		//--------------------Doc File---------------------
		XWPFDocument doc = new XWPFDocument();
		XWPFParagraph p = doc.createParagraph();
		XWPFRun xwpfRun = p.createRun();
		String consolidatedScreenshotpath="";
		//-------------------------------------------------


		//****************************Instatntiate Extent Reporting************************
		logger=extent.startTest("Validate if created confirmed Order is autoshipped");
		//*********************************************************************************
		int r = 0;
		boolean rdsvalidation = true;
		try {

			int rows = 0;
			int occurances = 0;
			Workbook wrk1 = Workbook.getWorkbook(new File(TestDataPath));
			Sheet sheet1 = wrk1.getSheet(SheetName);
			rows = sheet1.getRows();

			Cell[] FirstRow = sheet1.getRow(0);
			Map<String, Integer> map = new HashMap<String, Integer>();
			for(int i=0; i < FirstRow.length; i++)
			{
				map.put(FirstRow[i].getContents().trim(), i);
			}

			for(r=1; r<rows; r++) {

				Keyword = sheet1.getCell(map.get("Keyword"), r).getContents().trim();


				if(occurances>0){
					if(!(TestKeyword.contentEquals(Keyword))){
						break;
					}
				}

				if(Keyword.equalsIgnoreCase(TestKeyword)) {
					System.out.println("Keyword: "+Keyword);
					System.out.println("TestKeyword: "+TestKeyword);
					occurances=occurances+1;
					Keyword = sheet1.getCell(map.get("Keyword"), r).getContents().trim();
					TestCaseNo = sheet1.getCell(map.get("TestCaseNo"), r).getContents().trim();
					TestCaseName = sheet1.getCell(map.get("TestCaseName"), r).getContents().trim();	
					RDSusername= sheet1.getCell(map.get("RDSusername"), r).getContents().trim(); 
					RDSpassword= sheet1.getCell(map.get("RDSpassword"), r).getContents().trim(); 
					RDShostName= sheet1.getCell(map.get("RDShostName"), r).getContents().trim();
					RDSport= sheet1.getCell(map.get("RDSport"), r).getContents().trim();
					orderid=sheet1.getCell(map.get("orderid"), r).getContents().trim();
					//itemid=sheet1.getCell(map.get("itemid"), r).getContents().trim();
					//itemid=sheet1.getCell(map.get("itemId"), r).getContents().trim();
					//jobid = sheet1.getCell(map.get("Jobid"), r).getContents().trim();

					//****************************Create Path for Word Document and Screenshots****************************************
					File screenshotpath = new File(System.getProperty("user.dir")+ResultPath+"Screenshot/"+TestCaseNo+"/");
					screenshotpath.mkdirs();
					consolidatedScreenshotpath=System.getProperty("user.dir")+"/"+ResultPath+"Screenshot/"+TestCaseNo+"/"+TestCaseNo+"_";
					//****************************************************************************************************************** //-------------------HTML Header--------------------

					//-------------------HTML Header--------------------
					Reporting_Utilities.writeHeaderToHTMLLog(TestCaseNo, TestCaseName, ResultPath);
					utilityFileWriteOP.writeToLog("*********************************START**********************************",ResultPath);	 			 
					utilityFileWriteOP.writeToLog(getCurrentDate.getISTTime(),ResultPath);
					//********************Declaring environment variables for stepnumber*******************

					try{
						stepnum=Integer.parseInt(System.getProperty("stepnumber"));	
					}catch(Exception e){
						System.setProperty("stepnumber", "1");
						stepnum=Integer.parseInt(System.getProperty("stepnumber"));	
					}
					//************************************************************************************
					System.out.println("Waiting for couple of minutes for the data to reflect in RDS");
					Thread.sleep(60000);
					//************************************Validate Status from torderheader***********************************************

					String[] ordernoarr=orderid.split(",");
					for(String orderno:ordernoarr){
						String SQL="select http_response_code,total_items_quantities,order_request_payload from wholesale_shipments.wholesale_outbound_shipments_audit   where order_id = '"+orderid+"'";
						System.out.println("SQL Query: "+SQL);
						
						//*************To Retrieve the source path from RDS
						
						boolean createcsvfromtorderheader=RDS_DB.GetSourcepathFromRDS(RDSusername, RDSpassword, RDShostName, Integer.parseInt(RDSport), SQL, "wholesaleDHLautoshipped.csv", ResultPath, TestCaseNo, xwpfRun);
						
						
						//*************To move CSV file from PuttyUNIX to local
						
						boolean movetocsvfromputtyunixtorderheader=RDS_DB.MoveCSVFileFromPuttyUNIXtoLocal("S:\\Putty", System.getProperty("user.dir")+"/csv/", "wholesaleDHLautoshipped.csv", ResultPath, TestCaseNo, xwpfRun);
						
						if(createcsvfromtorderheader && movetocsvfromputtyunixtorderheader){
							HashMap<String, String> expectedvalues= new HashMap<String, String>();
							expectedvalues.put("response_error_cause", "");
							boolean validatetrmsrequestadapter=RDS_DB.ValidateRDSAutoshipped(System.getProperty("user.dir")+"/csv/", "wholesaleDHLautoshipped.csv", expectedvalues, "torderheader",itemid, logger);
							
							if(validatetrmsrequestadapter){
								logger.log(LogStatus.PASS, "\"orderstatuscurrent\" field validation in table torderheader is successful for orderno: "+orderno);
								Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Validation of \"orderstatuscurrent\" in torderheader", "\"orderstatuscurrent\" field validation in table torderheader is successful", "PASS", ResultPath);
								rdsvalidation=true;
							}else{
								logger.log(LogStatus.FAIL, "\"orderstatuscurrent\" field validation in table torderheader is not successful for orderno: "+orderno);
								Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Validation of \"orderstatuscurrent\" in torderheader", "\"orderstatuscurrent\" field validation in table torderheader is NOT successful", "FAIL", ResultPath);
								rdsvalidation=false;
							}
						}else
							logger.log(LogStatus.FAIL, "Data extraction from torderheader table is not successful");
						
					}

				
					String status=null;

				}			
			}	
			if(rdsvalidation==false){
				logger.log(LogStatus.FAIL, "\"orderstatuscurrent\" field validation in table torderheader is not successful for order");
				throw new MyException("Test Stopped Because of Failure. Please check Execution log");
			}
		}catch (Exception e) {
			e.printStackTrace();
			Final_Result="FAIL"; 
			excelCellValueWrite.writeValueToCell(Final_Result, r, 3, DriverSheetPath, driversheetname);
			Assert.assertTrue(false);
		}
		finally
		{  
			RowGenerator.collateHTMLLog(TestCaseNo, ResultPath);
			FileOutputStream out1 = new FileOutputStream(consolidatedScreenshotpath+Final_Result+".docx");
			doc.write(out1);
			out1.close();
			doc.close();
			//Reporting_Utilities.writeCloseRowToHTMLLog(TestCaseNo, ResultPath);

		}
	}

}