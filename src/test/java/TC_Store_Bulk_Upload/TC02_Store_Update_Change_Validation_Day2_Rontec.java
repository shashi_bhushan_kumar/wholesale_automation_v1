package TC_Store_Bulk_Upload;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.testng.Assert;
import org.testng.annotations.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

import Additional_Functions.McColls_Amazon_RackSpace;

import Additional_Functions.Wholesalewebportalfunctions_Functions;
import Utilities.*;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;



public class TC02_Store_Update_Change_Validation_Day2_Rontec {
	
	String URL;
	//String DynamoURL;
	String DriverPath;
	String DriverName;
	String DriverType;
	String BrowserPath;
	String ServerName;
	String SheetName;
	String Final_Result="";
	String Downloadfilepath="";
	String TestDataPath;
	String location="";
	ExtentReports extent;
	ExtentTest logger;
	Integer stepnum=0;
	
	String ResultPath="";
	String downloadFilepath="";
	public WebDriver driver = null;

	public WebDriverWait wait = null;
	
	//String downloadFilepath = "S:/Mordering 3B/Automation/Files";
	//********************Declaring Environment variable org.apache.commons.logging.Log**********************
	static {
		System.setProperty("org.apache.commons.logging.Log",
		                        "org.apache.commons.logging.impl.NoOpLog");
		}
	//*******************************************************************************************************
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@BeforeTest
	public void setUp() throws Exception {
		
		URL=ProjectConfigurations.LoadProperties("Portal_ProjectURL");
		//DynamoURL=ProjectConfigurations.LoadProperties("Relex_DynamoURL");
		DriverPath=ProjectConfigurations.LoadProperties("Portal_DriverPath");
		DriverName=ProjectConfigurations.LoadProperties("Portal_DriverName");
		DriverType=ProjectConfigurations.LoadProperties("Portal_DriverType");
		BrowserPath=ProjectConfigurations.LoadProperties("Portal_BrowserPath");
		ServerName=ProjectConfigurations.LoadProperties("AutomationServerName");
		Downloadfilepath=ProjectConfigurations.LoadProperties("downloadpath");
		System.out.println("Download path for CSV files"+Downloadfilepath);
		//////////////////Setting up user Brwoser////////////////////////////////////////
		System.setProperty(DriverName,DriverPath);
		HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
		chromePrefs.put("profile.default_content_settings.popups", 0);
		chromePrefs.put("download.prompt_for_download", "false");
		chromePrefs.put("download.", "false");
		chromePrefs.put("download.default_directory", System.getProperty("user.dir")+Downloadfilepath);
		System.out.println("Downloadpath"+System.getProperty("user.dir")+Downloadfilepath);
		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("prefs", chromePrefs);
		options.setExperimentalOption("useAutomationExtension", false);
		DesiredCapabilities cap = DesiredCapabilities.chrome();
		cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		cap.setCapability(ChromeOptions.CAPABILITY, options);
		driver = new ChromeDriver(cap);
		wait=new WebDriverWait(driver, 150);			
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		 driver.manage().window().maximize();
		
		System.out.println("URL: "+URL);
		driver.get(URL);
		
		
		 driver.manage().window().maximize();
		
		// to be removed..................	
		
		TestDataPath=ProjectConfigurations.LoadProperties("Portal_TestDataPath");
		SheetName=ProjectConfigurations.LoadProperties("Portal_SheetName");
		downloadFilepath=ProjectConfigurations.LoadProperties("Portal_DownloadPath");
//				ResultPath="Results/";
		//*********************Setting up Result Path*****************************
		ResultPath=System.getProperty("resultpath");
		if(ResultPath==null){
			ResultPath=Reporting_Utilities.createResultPath("Store_Upload_Day2_Rontec_"+getCurrentDate.getISTDateddMM()+"");
		}
		System.out.println("Result path is "+ResultPath);
		//************************************************************************
		
		 //*********************************Initialising Extent Report Variables**********************************
		 extent = new ExtentReports (System.getProperty("user.dir") +ResultPath+"Store_UploadSeleniumReport.html", false);
		 extent.loadConfig(new File(System.getProperty("user.dir")+"\\extent-config.xml"));
		 //*******************************************************************************************************
	}

	@AfterTest
	public void tearDown() throws Exception {
		extent.endTest(logger);
		extent.flush();
		extent.close();	
		driver.quit();
		
		
	}

	@Test
	public void test() throws InterruptedException,IOException {
		String TestKeyword = "Store_Upload_Day2_Validation_Rontec";
		String TestCaseNo = null;
		String TestCaseName = null;
		String Keyword = null;
		String DriverSheetPath = TestDataPath;
		String driversheetname=SheetName;		
		String rwsusername= null;
		String rwspassword = null;
		
		String customer=null;
		String storeid=null;
		String category=null; 
		String FinalFilePath=null;

		
		//--------------------Doc File---------------------
		XWPFDocument doc = new XWPFDocument();
        XWPFParagraph p = doc.createParagraph();
        XWPFRun xwpfRun = p.createRun();
        
        String consolidatedScreenshotpath="";
        //-------------------------------------------------

		
		int r = 0;
		try {
			
			
			//-----------------------Extent Report-----------
			logger = extent.startTest("WholeSale Store Bulk Upload Day2 Valdiation Rontec");
			//-----------------------------------------------
			
			
			int rows = 0;
			int occurances = 0;
			Workbook wrk1 = Workbook.getWorkbook(new File(TestDataPath));
			Sheet sheet1 = wrk1.getSheet(SheetName);
			rows = sheet1.getRows();
			Cell[] FirstRow = sheet1.getRow(0);
			Map<String, Integer> map = new HashMap<String, Integer>();
			for(int i=0; i < FirstRow.length; i++)
			{
				map.put(FirstRow[i].getContents().trim(), i);
			}
			
			
			
			
			
			for(r=1; r<rows; r++) {
				Keyword = sheet1.getCell(map.get("Keyword"), r).getContents().trim();
				
									

				if(occurances>0){
			    	break;
				 }
				
				if(Keyword.equalsIgnoreCase(TestKeyword)) {
					System.out.println("Keyword: "+Keyword);
					System.out.println("TestKeyword: "+TestKeyword);
					occurances=occurances+1;
					Keyword = sheet1.getCell(map.get("Keyword"), r).getContents().trim();
					TestCaseNo = sheet1.getCell(map.get("TestCaseNo"), r).getContents().trim();
					TestCaseName = sheet1.getCell(map.get("TestCaseName"), r).getContents().trim();
							
					rwsusername=sheet1.getCell(map.get("poratlUserName"), r).getContents().trim();
					rwspassword=sheet1.getCell(map.get("portalPassword"), r).getContents().trim();
					customer=sheet1.getCell(map.get("customer"),r).getContents().trim();
					storeid=sheet1.getCell(map.get("storeid"),r).getContents().trim();
					
					System.out.println("Store id from data sheet"+storeid);
					category=sheet1.getCell(map.get("category"),r).getContents().trim();
					FinalFilePath=sheet1.getCell(map.get("FinalFilePath"),r).getContents().trim();
					//****************************Create Path for Word Document and Screenshots****************************************
					File screenshotpath = new File(System.getProperty("user.dir")+ResultPath+"Screenshot/"+TestCaseNo+"/");
					screenshotpath.mkdirs();
					consolidatedScreenshotpath=System.getProperty("user.dir")+"/"+ResultPath+"Screenshot/"+TestCaseNo+"/"+TestCaseNo+"_";
					//****************************************************************************************************************** 
					
		  
					//-------------------HTML Header--------------------
		 
					Reporting_Utilities.writeHeaderToHTMLLog(TestCaseNo, TestCaseName, ResultPath);
					utilityFileWriteOP.writeToLog("*********************************START**********************************",ResultPath);	 			 
					utilityFileWriteOP.writeToLog(getCurrentDate.getISTTime(),ResultPath);
					//********************Declaring environment variables for stepnumber*******************
					
					try{
						stepnum=Integer.parseInt(System.getProperty("stepnumber"));	
					}catch(Exception e){
						System.setProperty("stepnumber", "1");
						stepnum=Integer.parseInt(System.getProperty("stepnumber"));	
					}
					
					
					//*****************************************Login to Webportal**********************************
boolean WebportalLogin = Wholesalewebportalfunctions_Functions.wholesale_login(driver, wait, TestCaseNo, rwsusername, rwspassword, ResultPath,xwpfRun, logger);
					if(WebportalLogin==true) {
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Login and Navigate to Respective Customer", "Navigated to Respective Customer succesfully", "PASS", ResultPath);
						logger.log(LogStatus.PASS, "Logged in to Wholesale webportal and navigated to respective customer");
						
					}else{
						 
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Login and Navigate to Respective Customer", "Error occured Navigating", "FAIL", ResultPath);
						logger.log(LogStatus.FAIL, "Error occured during Logging in to Wholesale webportal and navigating to respective customer");
						throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}
					stepnum++;
					
boolean WebportalCatelogueVAL = Wholesalewebportalfunctions_Functions.newStoreUpload_Day2validation(driver, wait, TestCaseNo, customer,storeid,category,FinalFilePath, ResultPath,xwpfRun, logger);
					if(WebportalCatelogueVAL==true) {
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Validate Store update details for specific customer", "Store update details for specific customer validation", "PASS", ResultPath);
						logger.log(LogStatus.PASS, "Store update details for specific customer validation passed ");
					}else{
						 
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Validate Store update details for specific customer", "Error occured during searching item", "FAIL", ResultPath);
						logger.log(LogStatus.FAIL, "Error occured during searching store and details  for respective customer");
						throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}
					stepnum++;
					
	
boolean WebportalLogout= Wholesalewebportalfunctions_Functions.AWW_Logout(driver, wait, TestCaseNo,rwsusername,ResultPath,xwpfRun, logger);
				if(WebportalLogout==true) {
					Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Logout for user", "user logged out succesfully", "PASS", ResultPath);
					logger.log(LogStatus.PASS, "User logged out");
				}else{
					 
					Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Logout for user", "Error occured during logout", "FAIL", ResultPath);
					logger.log(LogStatus.FAIL, "Error occured during logout");
					throw new MyException("Test Stopped Because of Failure. Please check Execution log");
				}
				stepnum++;
				
			if (WebportalLogin && WebportalCatelogueVAL && WebportalLogout){
					Final_Result="PASS";	 						 
					utilityFileWriteOP.writeToLog(TestCaseNo, "Store Update validation Day2 for customer:"+customer+" is ", "PASS",ResultPath);
					excelCellValueWrite.writeValueToCell(Final_Result, r, 3, DriverSheetPath, driversheetname);							  
					Assert.assertTrue(true);
				}else {
					Final_Result="FAIL"; 
					excelCellValueWrite.writeValueToCell(Final_Result, r, 3, DriverSheetPath, driversheetname);
					utilityFileWriteOP.writeToLog(TestCaseNo, "Store Update validation Day2 for customer:"+customer+" is ", "FAIL",ResultPath);
					Assert.assertTrue(false);
				}
						
				}
			}
		
			} 
	
		catch (Exception e) {
			
		e.printStackTrace();
		
		 Final_Result="FAIL"; 
		  
	     excelCellValueWrite.writeValueToCell(Final_Result, r, 3, DriverSheetPath, driversheetname);
	     Assert.assertTrue(false);
	}
	finally
	{  
		
		RowGenerator.collateHTMLLog(TestCaseNo, ResultPath);
		FileOutputStream out1 = new FileOutputStream(consolidatedScreenshotpath+Final_Result+".docx");
		doc.write(out1);
		out1.close();
		doc.close();
		logger.log(LogStatus.INFO, "Evidance link for - "+TestCaseName+" <a href='file:///"+consolidatedScreenshotpath+Final_Result+".docx"+"'>EvidenceLink</a>");
		//Reporting_Utilities.writeCloseRowToHTMLLog(TestCaseNo, ResultPath);

	}}}
	
