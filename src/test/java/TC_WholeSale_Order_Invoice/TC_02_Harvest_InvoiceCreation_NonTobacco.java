package TC_WholeSale_Order_Invoice;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import Additional_Functions.Batch_JobRun;
import Additional_Functions.Invoice_Miscellaneous_Functions;
import OrderServices_Functions.McColls_OrderServices;
import OrderServices_Functions.Order_Consolidation_Utilities;

import Utilities.*;

public class TC_02_Harvest_InvoiceCreation_NonTobacco {

	//String URL;
	public static ExtentReports extent=null;
	public static ExtentTest logger=null;
	String DriverPath;
	String DriverName;
	String DriverType;
	String BrowserPath;
	String ServerName;
	String SheetName;
	String ItemDetailsSheetName;

	String TestDataPath;
	String TemporaryFilePath;

	String ResultPath=null;
	public String EBSUFTResultPath="";
	public WebDriver driver = null;
	public WebDriverWait wait = null;
	String ChromeBrowserExePath=null;
	
	String TCName=null;
	String AppworxURL=null;
	String AppworxUserName=null;
	String AppworxPassword=null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@BeforeTest
	public void setUp() throws Exception {



		//URL=ProjectConfigurations.LoadProperties("SalesforceAutomation_ProjectURL");
		DriverPath=ProjectConfigurations.LoadProperties("Portal_DriverPath");
		DriverName=ProjectConfigurations.LoadProperties("Portal_DriverName");
		DriverType=ProjectConfigurations.LoadProperties("Portal_DriverType");
		BrowserPath=ProjectConfigurations.LoadProperties("Portal_BrowserPath");
		ServerName=ProjectConfigurations.LoadProperties("AutomationServerName");
		TestDataPath=ProjectConfigurations.LoadProperties("OrderService_TestDataPath");
		SheetName=ProjectConfigurations.LoadProperties("OrderService_SheetName");
		ItemDetailsSheetName=ProjectConfigurations.LoadProperties("ItemDetailsSheetName");
		TemporaryFilePath=ProjectConfigurations.LoadProperties("TemporaryFilePath");

		System.out.println(BrowserPath);
		
		TCName=ProjectConfigurations.LoadProperties("AppworxTC");
		AppworxURL=ProjectConfigurations.LoadProperties("AppworxURLSIT");
		AppworxUserName=ProjectConfigurations.LoadProperties("AppworxUserName");
		AppworxPassword=ProjectConfigurations.LoadProperties("AppworxPassword");
		/*
		System.out.println("UserName"+AppworxUserName);
		
		System.out.println("Password"+AppworxPassword);
		*/
		

		//*********************Setting up Result Path*****************************
//		ResultPath=System.getProperty("resultpath");
		if(ResultPath==null){
			System.out.println("Result path before is "+ResultPath);
			ResultPath=Reporting_Utilities.createResultPath("WholesaleAutomation"+getCurrentDate.getISTDateddMM()+"/Invoice_Creation");
		}
		System.out.println("Result path at create path is "+ResultPath);
		//************************************************************************

		//*********************************Initialising Extent Report Variables**********************************
		extent = new ExtentReports (System.getProperty("user.dir") +ResultPath+"InvoiceCreation_SeleniumReport.html", false);
		extent.loadConfig(new File(System.getProperty("user.dir")+"\\extent-config.xml"));
		//*******************************************************************************************************

		
	}

	@AfterTest
	public void tearDown() throws Exception {

		extent.endTest(logger);
		extent.flush();
		extent.close();	

	}

	@Test
	public void test() throws IOException {

		String TestCaseNo = null;
		String TestCaseName = null;
		String Keyword = null;
		String Final_Result = null;
		String TestKeyword = "Harvest_Verify_Invoice_Status";
		
		String DriverSheetPath = TestDataPath;
		
	
		String consolidatedParameter= null;	
		String modifiedTestDataPathforUFt= null;

		String ProxyHostName = null;
		String ProxyPort = null;
		String SYSUserName = null;
		String SYSPassWord = null;
		String CatalogueURL=null;
		String s3username=null;
		String s3password=null;
		String S3BucketPath=null;
		String S3BucketPath2=null;
		String S3BucketPath3=null;
		String S3URL="https://rax-598f5024ec80460190f52fc91b01825a.signin.aws.amazon.com/console";
		String TargetHostName = null;
		String TargetPort = null;
		String TargetHeader = null;
		String UrlTail = null;
		String ApiKey = null;
		String AuthorizationKey = null;
		String AuthorizationValue = null;

		String AllOrderIDs = null;
		String AllMessageTypes = null;
		String AllShipToLocationIds = null;
		String ShipToDeliverAt = null;
		String OrderCount=null;

		String AllOrderAttributes = null;
		String AllMessageAttributes = null;
		String AllShipToAddressAttributes = null;
		String AllBillToAddressAttributes = null;
		String AllBillToTaxAddressAttributes = null;
		String AllItemAttributes = null;

		String SFTPHostName = null;
		String SFTPPort = null;
		String SFTPUserName = null;
		String SFTPPassword = null;
		String NasPath = null;

		String ColumnNames = null;
		String ItemIDs = null;
		String QuantitytoShip = null;
		
		String firstJobName="WHOLESALE_INVOICE_FILE_MOVE";
		String secondJobName="WMM_WHOLESALE_IMPORT_SCHEDULER";
		String thirdJobName="WHOLESALE_ACCOUNT_EVENT_UPLOAD";
		String environment=null;
		String OrderIDGeneration = null;

		//--------------------Doc File---------------------
		XWPFDocument doc = new XWPFDocument();
		XWPFParagraph p = doc.createParagraph();
		XWPFRun xwpfRun = p.createRun();

		String consolidatedScreenshotpath="";
		//-------------------------------------------------

		int r = 0;
		try{

			int rows = 0;
			//int cols = 0;
			int occurances = 0;
			int ItemDetailsoccurances = 0;

			Workbook wrk1 = Workbook.getWorkbook(new File(TestDataPath));

			Sheet sheet1 = wrk1.getSheet(SheetName);
			Sheet sheet2 = wrk1.getSheet(ItemDetailsSheetName);

			rows = sheet1.getRows();
			Cell[] FirstRow = sheet1.getRow(0);
			Map<String, Integer> map = new HashMap<String, Integer>();
			for(int i=0; i < FirstRow.length; i++)
			{
				map.put(FirstRow[i].getContents().trim(), i);
			}
			//cols = sheet1.getColumns();
			int itemdetailsrows = sheet2.getRows();
//			System.out.println(itemdetailsrows);

			for(int i=1; i<itemdetailsrows; i++) {

				Keyword = sheet2.getCell(0, i).getContents().trim();

//				System.out.println(sheet2.getCell(0, i).getContents().trim());
				if(ItemDetailsoccurances>0){

					if(!(TestKeyword.contentEquals(Keyword))){

						break;

					}

				}
				if(Keyword.equalsIgnoreCase(TestKeyword)) {

					ItemDetailsoccurances=ItemDetailsoccurances+1;
					ItemIDs = sheet2.getCell(1, i).getContents().trim();

					System.out.println("Matched "+Keyword + sheet2.getCell(0, i).getContents().trim());
					
					boolean  OrderIDGenerationValidation=true;
					
					 OrderIDGeneration = McColls_OrderServices.OrderIdGeneration(TestDataPath, SheetName,TestKeyword,TestCaseNo, ResultPath);
					 if( (!OrderIDGeneration.isEmpty())) {
						 OrderIDGenerationValidation = OrderIDGenerationValidation && true;
						 utilityFileWriteOP.writeToLog("Order ID Generation Script", "Successfully Created Order IDs for "+SheetName,ResultPath);
//						 Additional_Functions.WriteValueToExcel.writeOrderIDKeyword(Keyword, "PUSH_DHL_in_RDS", OrderIDGeneration, TestDataPath, SheetName, map);
						 System.out.println("Order id written to excel is "+OrderIDGeneration);
//							logger.log(LogStatus.PASS, "Order ID Generated is:"+OrderIDGeneration);
						 
					 }
					 else {
						OrderIDGenerationValidation = OrderIDGenerationValidation && false;
						utilityFileWriteOP.writeToLog("Order ID Generation Script", "Failed to create Order IDs for "+SheetName,ResultPath);
						 
					 }

				}
			}
//			System.out.println("Number of Rows: "+rows);
			for(r=1; r<rows; r++) {

				TestCaseNo = sheet1.getCell(0, r).getContents().trim();
				TestCaseName = sheet1.getCell(1, r).getContents().trim();
				Keyword = sheet1.getCell(2, r).getContents().trim();

				
				ProxyHostName = sheet1.getCell(map.get("ProxyHostName"), r).getContents().trim();
				
				ProxyPort = sheet1.getCell(map.get("ProxyPort"), r).getContents().trim();
				SYSUserName = sheet1.getCell(map.get("SYSUserName"), r).getContents().trim();
				SYSPassWord = sheet1.getCell(map.get("SYSPassWord"), r).getContents().trim();
				TargetHostName = sheet1.getCell(map.get("TargetHostName"), r).getContents().trim();
				TargetPort = sheet1.getCell(map.get("TargetPort"), r).getContents().trim();
				TargetHeader = sheet1.getCell(map.get("TargetHeader"), r).getContents().trim();
				UrlTail = sheet1.getCell(map.get("UrlTail"), r).getContents().trim();
				CatalogueURL=sheet1.getCell(map.get("CatalogueURL"), r).getContents().trim();
				ApiKey = sheet1.getCell(map.get("ApiKey"), r).getContents().trim();
				AuthorizationKey = sheet1.getCell(map.get("AuthorizationKey"), r).getContents().trim();
				AuthorizationValue = sheet1.getCell(map.get("AuthorizationValue"), r).getContents().trim();
				String OrderReferenceCode=sheet1.getCell(map.get("orderReferenceCode"), r).getContents().trim();
				AllOrderIDs = sheet1.getCell(map.get("OrderID"), r).getContents().trim();
				AllMessageTypes = sheet1.getCell(map.get("MessageType"), r).getContents().trim();
				AllShipToLocationIds = sheet1.getCell(map.get("ShipToLocationId"), r).getContents().trim();
				ShipToDeliverAt = sheet1.getCell(map.get("ShipToDeliverAt"), r).getContents().trim();
				AllOrderAttributes = sheet1.getCell(map.get("orderBuyerPartyId,orderSellerPartyId"), r).getContents().trim();
				AllMessageAttributes = sheet1.getCell(map.get("messageId,messageSenderPartyId,messageRecipientPartyId,messageCreatedAt"), r).getContents().trim();
				AllShipToAddressAttributes = sheet1.getCell(map.get("shipToPartyId,shipToAddressName,shipToAddressLine1,shipToAddressLine2,shipToAddressCity,shipToAddressState,shipToAddressPostCode,shipToAddressCountryCode,shipToDeliverLatestAt,shipFromLocationId,shipFromAddressName"), r).getContents().trim();
				AllBillToAddressAttributes = sheet1.getCell(map.get("billToPartyId,billToAddressName,billToAddressLine1,billToAddressLine2,billToAddressCity,billToAddressState,billToAddressPostCode,billToAddressCountryCode"), r).getContents().trim();
				AllBillToTaxAddressAttributes = sheet1.getCell(map.get("billToTaxId,billToTaxAddressName,billToTaxAddressLine1,billToTaxAddressLine2,billToTaxAddressCity,billToTaxAddressState,billToTaxAddressPostCode,billToTaxAddressCountryCode"), r).getContents().trim();
				ColumnNames = sheet1.getCell(map.get("ColumnName"), r).getContents().trim();
				QuantitytoShip = sheet1.getCell(map.get("QuantitytoShip"), r).getContents().trim();
				String quantityOrdered = sheet1.getCell(map.get("QuantityOrdered"), r).getContents().trim();
				OrderCount = sheet1.getCell(map.get("OrderCount"), r).getContents().trim();
				
				SFTPHostName = sheet1.getCell(map.get("SFTPHostName"), r).getContents().trim();
				SFTPPort = sheet1.getCell(map.get("SFTPPort"), r).getContents().trim();
				SFTPUserName = sheet1.getCell(map.get("SFTPUserName"), r).getContents().trim();
				SFTPPassword = sheet1.getCell(map.get("SFTPPassword"), r).getContents().trim();
				NasPath = sheet1.getCell(map.get("NasPath"), r).getContents().trim();
				

				s3username=sheet1.getCell(map.get("s3username"), r).getContents().trim();
				s3password=sheet1.getCell(map.get("s3password"), r).getContents().trim();
				S3BucketPath=sheet1.getCell(map.get("S3BucketPath"), r).getContents().trim();
				S3BucketPath2=sheet1.getCell(map.get("S3BucketPath2"), r).getContents().trim();
				S3BucketPath3=sheet1.getCell(map.get("S3BucketPath3"), r).getContents().trim();
				environment=sheet1.getCell(map.get("environment"), r).getContents().trim();
				ColumnNames = sheet1.getCell(map.get("ColumnName"), r).getContents().trim();
				QuantitytoShip = sheet1.getCell(map.get("QuantitytoShip"), r).getContents().trim();
				
				String quantityType = sheet1.getCell(map.get("QuantityType"), r).getContents().trim();
			


				if(occurances>0){

					if(!(TestKeyword.contentEquals(Keyword))){

						break;

					}

				}

				if(Keyword.equalsIgnoreCase(TestKeyword)) {

					occurances=occurances+1;
					s3username=sheet1.getCell(map.get("s3username"), r).getContents().trim();
					s3password=sheet1.getCell(map.get("s3password"), r).getContents().trim();
					S3BucketPath=sheet1.getCell(map.get("S3BucketPath"), r).getContents().trim();
					
					logger = extent.startTest("Validate Invoice Generation"); 
					
					ColumnNames.split(",");
			
					String OrderID[] = AllOrderIDs.split(",");
					String ShipToLocationId[] = AllShipToLocationIds.split(",");
					String MessageType[] = AllMessageTypes.split(",");
					String AllColumnNames[] = ColumnNames.split(",");
					//System.out.println(ItemIDs);
					String AllItemIDs[] = ItemIDs.split(",");
//					String ItemID[] = AllItemIDs.split(",");
					String AllQuantityShipped[] = QuantitytoShip.split(",");

					boolean post = true;

					boolean get = true;

					boolean ship = true;

					boolean aftership = true;

					Boolean RestShip=false;

					boolean RestShipppedClosure= false;

					Boolean RestGetAfterShip=false;

					Boolean RestGetItem=true;

					Boolean RestGet=true;

					int order = 0;

					String[] AllItemIds = ItemIDs.split(",");
					String[] AllQuantitiesOrdered = quantityOrdered.split(",");
								
					String[] AllQuanityTypes = quantityType.split(",");
					

					System.out.println("Ship to location Id length is "+ShipToLocationId.length);
					

					for(int k=0; k<AllItemIds.length; k++) {


						String qtyType = Invoice_Miscellaneous_Functions.GetCallquantityTypeFromWebPortal(CatalogueURL,ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue, AllItemIDs[k], TestCaseNo, ResultPath, xwpfRun,logger);

					String RestGetItemDetails = Invoice_Miscellaneous_Functions.GetCallItemDetailsFromWebPortal(CatalogueURL,ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue, AllItemIds[k], "skuPin", quantityType, String.valueOf(k+1), AllQuantitiesOrdered[k], TestCaseNo, ResultPath, xwpfRun,logger);


						int itemDetailsPrintOccurence = 0;

						for(int i=1; i<itemdetailsrows; i++) {

							Keyword = sheet2.getCell(0, i).getContents().trim();

							System.out.println(sheet2.getCell(0, i).getContents().trim());
							if(itemDetailsPrintOccurence>0){

								if(!(TestKeyword.contentEquals(Keyword))){
									System.out.println("breaking with keyword "+ Keyword);
									break;

								}

							}
							if(Keyword.equalsIgnoreCase(TestKeyword)) {

								itemDetailsPrintOccurence=itemDetailsPrintOccurence+1;
								excelCellValueWrite.writeValueToCell(RestGetItemDetails, i, 2+k, TestDataPath, ItemDetailsSheetName);

							}
						}


					}

					File screenshotpath = new File(System.getProperty("user.dir")+ResultPath+"Screenshot/"+TestCaseNo+"/");
					screenshotpath.mkdirs();
					consolidatedScreenshotpath=System.getProperty("user.dir")+"/"+ResultPath+"Screenshot/"+TestCaseNo+"/"+TestCaseNo+"_";
					
					//-------------------HTML Header--------------------

					Reporting_Utilities.writeHeaderToHTMLLog(TestCaseNo, TestCaseName, ResultPath);

			
					//----------------------------------------------------
					
					

// VATAMOUNT    TAX_CODE
					for(int vatAmountindex=0; vatAmountindex<AllItemIds.length; vatAmountindex++) {

						String VatAmount = Invoice_Miscellaneous_Functions.GetCallVatAmountFromWebPortal(CatalogueURL,ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue, AllItemIds[vatAmountindex], TestCaseNo, ResultPath,xwpfRun,logger);
						excelCellValueWrite.writeValueToCell(OrderIDGeneration, r, map.get("Order_Id"), DriverSheetPath, SheetName);
						Utilities.utilityFileWriteOP.appendValueToCell(VatAmount, r,map.get("TAX_CODE"), DriverSheetPath, SheetName);

						if(vatAmountindex != (AllItemIds.length - 1 )) {

							Utilities.utilityFileWriteOP.appendValueToCell(",", r,map.get("TAX_CODE"), DriverSheetPath, SheetName);
//							Utilities.utilityFileWriteOP.appendValueToCell(",", r, 46, TestDataPath, SheetName);

						}
						System.out.println("VatAmount for loop :"+VatAmount);
					}
					
					

					boolean RestPost = McColls_OrderServices.PostCallOrderCreationGeneralSeperateItemDetails(TestDataPath, "ItemDetails", ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue, AllOrderIDs, MessageType[0], ShipToLocationId[0],OrderCount, ShipToDeliverAt, OrderReferenceCode,AllOrderAttributes, AllMessageAttributes, AllShipToAddressAttributes, AllBillToAddressAttributes, AllBillToTaxAddressAttributes, "", TestKeyword, TestCaseNo,ResultPath,xwpfRun,logger);

					if(RestPost==true) {
						logger.log(LogStatus.PASS, "Order Created Successfully");
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "1", "Post Order Service Call", "Order Created Successfully.", "PASS", ResultPath);
					}

					else{
						logger.log(LogStatus.FAIL, "Error Occured during Order Creation");
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "1", "Post Order Service Call", "Error Occured during Order Creation.", "FAIL", ResultPath);
						throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}

					post = post && RestPost;


					RestGet = Invoice_Miscellaneous_Functions.GetOrderStatusValidation(ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue, OrderID[order], "confirmed", TestCaseNo,ResultPath,xwpfRun,logger);


					if(RestGet==true) {
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "2", "Get Order Service Call", "Order Validated Successfully.", "PASS", ResultPath);
						logger.log(LogStatus.PASS, "Order Validated Successfully");		

					}

					else{
						logger.log(LogStatus.FAIL, "Error Occured during Order validation");
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "2", "Get Order Service Call", "Error Occured during Order Validation.", "FAIL", ResultPath);

						throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}

					RestGet=get && RestGet;			 


					RestGetItem = Invoice_Miscellaneous_Functions.GetCallItemStatusValidation(ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue, OrderID[order], "confirmed", TestCaseNo,ResultPath,xwpfRun,logger);

					if(RestGetItem==true) {

						logger.log(LogStatus.PASS, "Item Status Validated Successfully");
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "3", "Get Item Status Validation", "Item Status Validated Successfully.", "PASS", ResultPath);
					}

					else{
						logger.log(LogStatus.FAIL, "Error Occured during Item status validation");
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "3", "Get Item Status Validation", "Error Occured during Item Status Validation.", "FAIL", ResultPath);

						throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}

					get = get && RestGetItem && RestGet;


					Thread.sleep(5000);

					RestShip = Invoice_Miscellaneous_Functions.ShipCallOrderShipment_New("harvest",ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue, OrderID[order], MessageType[0], ShipToLocationId[0], ShipToDeliverAt, ItemIDs, QuantitytoShip, TestCaseNo,ResultPath,xwpfRun,logger);
					if (RestShip==true) {
						logger.log(LogStatus.PASS, "Order Shipped Successfully.");
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "4", "Ship Order Service Call", "Order Shipped Successfully.", "PASS", ResultPath);
					}

					else{
						logger.log(LogStatus.FAIL, "Error Occured during Order shipped");
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "4", "Ship Order Service Call", "Error Occured during Order Shipped.", "FAIL", ResultPath);

						throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}							 


					ship = ship && RestShip;	


					RestShipppedClosure = Invoice_Miscellaneous_Functions.ShipCallOrderShipment_NewClosure("harvest",ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue, OrderID[order], MessageType[0], ShipToLocationId[0], ShipToDeliverAt, ItemIDs, QuantitytoShip, TestCaseNo,ResultPath,xwpfRun,logger);

					if(RestShipppedClosure==true) {
						logger.log(LogStatus.PASS, "Order closure Shipped Successfully");
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "4", "Shipment Closure Order Service Call", "Order closure Shipped Successfully.", "PASS", ResultPath);
					}

					else{
						logger.log(LogStatus.FAIL, "Error Occured during Order Shipped");
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "4", "Shipment Closure Order Service Call", "Error Occured during Order Shipped.", "FAIL", ResultPath);

						throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}							 


					ship = ship && RestShip && RestShipppedClosure;	


					RestGetAfterShip = Invoice_Miscellaneous_Functions.GetCallItemStatusValidation(ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue, OrderID[order], "shipped", TestCaseNo,ResultPath,xwpfRun,logger);

					if(RestGetAfterShip==true) {
						logger.log(LogStatus.PASS, "Shipped Order Validated Successfully");
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "5", "Get Order Service Call After Order Shipped", "Shipped Order Validated Successfully.", "PASS", ResultPath);
					}

					else{
						logger.log(LogStatus.FAIL, "Error Occured during shipped Order validation");
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "5", "Get Order Service Call After Order Shipped", "Error Occured during Shipped Order Validation.", "FAIL", ResultPath);

						throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}

					aftership = aftership && RestGetAfterShip;	


					if( (post) && (get)&& (ship) && (aftership)){

//						Final_Result = "PASS";
//						excelCellValueWrite.writeValueToCell(Final_Result, r, 3, TestDataPath, SheetName);
						excelCellValueWrite.writeValueToCell(OrderIDGeneration, r, map.get("Order_Id"), DriverSheetPath, SheetName);
						excelCellValueWrite.writeValueToCell(OrderIDGeneration, r, map.get("OrderID"), DriverSheetPath, SheetName);
						 System.out.println("Order id write to excel complete");

						Assert.assertTrue(true);
					}
					else {
//						Final_Result = "FAIL";
//						excelCellValueWrite.writeValueToCell(Final_Result, r, 3, TestDataPath, SheetName);

						Assert.assertTrue(false);

					}

					System.setProperty(DriverName,DriverPath);
					
					if((DriverType.equalsIgnoreCase("ChromeDriver"))&&(BrowserPath.equalsIgnoreCase("Default"))){
						HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
						chromePrefs.put("profile.default_content_settings.popups", 0);
						chromePrefs.put("download.prompt_for_download", "false");
						chromePrefs.put("download.", "false");
						chromePrefs.put("download.default_directory", System.getProperty("user.dir"));
						ChromeOptions options = new ChromeOptions();
					//	options.setBinary(ChromeBrowserExePath);

						options.setExperimentalOption("prefs", chromePrefs);
						options.setExperimentalOption("useAutomationExtension", false);
						DesiredCapabilities cap = DesiredCapabilities.chrome();
						cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
						cap.setCapability(ChromeOptions.CAPABILITY, options);
						driver = new ChromeDriver(cap);
						wait=new WebDriverWait(driver, 60);			
						driver.manage().deleteAllCookies();
						driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
						
						wait = new WebDriverWait(driver, 30);
						driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);	
					
						
						driver.manage().deleteAllCookies();
						driver.get(S3URL);
						driver.manage().window().maximize();
					}
					
					boolean logins3 = McColls_Amazon_RackSpace.aws_login(driver, wait, TestCaseNo, s3username, s3password,ResultPath,xwpfRun, logger);
					if(logins3==true) {
						
						logger.log(LogStatus.PASS, "Logged in to Amazon Rackspace");
					}else{

						
						logger.log(LogStatus.FAIL, "Error occured in login to Amazon Rackspace");
					throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}


					//*****************************************Navigate to S3**********************************
					boolean navigatetos3 = McColls_Amazon_RackSpace.navigateToS3(driver, wait, TestCaseNo,ResultPath,xwpfRun, logger);
					if(navigatetos3==true) {
						
						logger.log(LogStatus.PASS, "Navigated to S3 in Amazon Rackspace");
					}
					else{
						
						logger.log(LogStatus.FAIL, "Error occured in navigating through Amazon Rackspace");
						throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}


					//***************************************Navigation to specific folder at S3**********************

					boolean validatefileAtS3 = McColls_Amazon_RackSpace.searchFileinBucketPath(S3BucketPath,"w-har-"+AllOrderIDs+".csv",driver, wait,TestCaseNo,ResultPath,xwpfRun, logger);
					if(validatefileAtS3==true) {
						
						logger.log(LogStatus.PASS, "File validated at S3 successfully");
//						driver.quit();
						
					}
					else{
						
						logger.log(LogStatus.FAIL, "Error occured in navigating through Amazon Rackspace");
						throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}
	
						
					Boolean firstRes=Batch_JobRun.RunJob(AppworxURL,AppworxUserName,AppworxPassword,TCName,ResultPath,firstJobName);

					if(firstRes){
								System.out.println(firstJobName+" job executed successfully ");				
								utilityFileWriteOP.writeToLog("Batch Job Execution", firstJobName, "PASS");
						}

					else{
								System.out.println("Execution failed  for the batch job   "+firstJobName);
								utilityFileWriteOP.writeToLog("Batch Job Execution", firstJobName, "FAIL");
						  }	
					
				
					String latFilename =WinSCPCall.latestS3FileExtract_SFTP_harvest(AllOrderIDs,SFTPUserName, SFTPPassword,NasPath, SFTPHostName,SFTPPort,TestCaseNo, ResultPath, xwpfRun,logger);

					if(latFilename !=null) {
						
						logger.log(LogStatus.PASS, "Shipment File at Nas Path validated Successfully");
						 Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "1", "Validate Shipment File at Nas Path", "Shipment File at Nas Path validated Successfully.", "PASS", ResultPath);
						}
							
					else{
						logger.log(LogStatus.FAIL, "Error Occured during Validation of Shipment File at Nas Path");
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "1", "Validate Shipment File at Nas Path", "Error Occured during Validation of Shipment File at Nas Path.", "FAIL", ResultPath);
						
						throw new MyException("Test Stopped Because of Failure. Please check Execution log");
				}
				
				//	--will do maunally
			
					
					Boolean secondRes=Batch_JobRun.RunJob(AppworxURL,AppworxUserName,AppworxPassword,TCName,ResultPath,secondJobName);

					if(secondRes){
								System.out.println(secondJobName+" job executed successfully ");				
								utilityFileWriteOP.writeToLog("Batch Job Execution", secondJobName, "PASS");
						}

					else{
								System.out.println("Execution failed  for the batch job   "+secondJobName);
								utilityFileWriteOP.writeToLog("Batch Job Execution", secondJobName, "FAIL");
						  }	
					
					
					
					
					//Uncomment her
					String latFilename2 =WinSCPCall.latestS3FileExtract_SFTP_harvest(AllOrderIDs,SFTPUserName, SFTPPassword,"/data/nas/sit/wholesale_amazon/wholesale_ebs/out/pending", SFTPHostName,SFTPPort,TestCaseNo, ResultPath, xwpfRun,logger);

					if(latFilename2 !=null) {
						
						logger.log(LogStatus.PASS, "Shipment File at Nas Path validated Successfully");
						
						 Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "1", "Validate Shipment File at Nas Path", "Shipment File at Nas Path validated Successfully.", "PASS", ResultPath);
						}
							
					else{
						logger.log(LogStatus.FAIL, "Error Occured during Validation of Shipment File at Nas Path");
						
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "1", "Validate Shipment File at Nas Path", "Error Occured during Validation of Shipment File at Nas Path.", "FAIL", ResultPath);
						
						throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}

					
					//**********To fetch the Invoice Id from the file

					String InvoiceId =WinSCPCall.fetchInvoiceId_harvest(AllOrderIDs,SFTPUserName, SFTPPassword,SFTPHostName,SFTPPort,TestCaseNo,"TempDataFiles", ResultPath, xwpfRun,logger);

//					String VATamt =WinSCPCall.fetchVATamt(AllOrderIDs,SFTPUserName, SFTPPassword,SFTPHostName,SFTPPort,TestCaseNo,"TempDataFiles", ResultPath, xwpfRun,logger);

					//Invoice_Id
//					excelCellValueWrite.writeValueToCell(InvoiceId, r, 20, DriverSheetPath, SheetName);
					System.out.println("Invoice_Id"+InvoiceId);
					
					excelCellValueWrite.writeValueToCell(InvoiceId, r, map.get("Invoice_Id"), DriverSheetPath, SheetName);


					if(InvoiceId !=null) {
						
						logger.log(LogStatus.PASS, "Invoice Id is copied successfully.");
						
						 Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "2", "Fetch the Invoice ID from local temp file", "Invoice Id is copied successfully.", "PASS", ResultPath);
						 utilityFileWriteOP.writeToLog(TestCaseNo, "Extracted Invoice ID is "+InvoiceId+"  in the path //data//nas//sit//wholesale_amazon//wholesale_ebs//out//pending", "Done");
						 System.out.println("Extracted invoice id  is:"+InvoiceId);
						 
						}
							
					else{
						logger.log(LogStatus.FAIL, "Error Occured while fetching Invoice Id");
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "2", "Fetch the Invoice ID from Local temp file", "Error Occured while fetching Invoice Id", "FAIL", ResultPath);
						
						throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}
					
					
					
							String TotalAmount = Invoice_Miscellaneous_Functions.checkTotalAmountInPending(SFTPHostName, Integer.parseInt(SFTPPort), SFTPUserName, SFTPPassword, "/data/nas/sit/wholesale_amazon/wholesale_ebs/out/pending", "TempDataFiles", OrderID[order], TestCaseNo, ResultPath);		 
							String TotalVATAmount = Invoice_Miscellaneous_Functions.checkTotalVATAmountInPending(SFTPHostName, Integer.parseInt(SFTPPort), SFTPUserName, SFTPPassword, "/data/nas/sit/wholesale_amazon/wholesale_ebs/out/pending", "TempDataFiles", OrderID[order], TestCaseNo, ResultPath);		 

							System.out.println("The total amount is "+TotalAmount);	
							
							double TotalAmountinDouble = Double.parseDouble(TotalAmount);

							double TotalVATAmtDouble = Double.parseDouble(TotalVATAmount);
							
							DecimalFormat df = new DecimalFormat("0.00");
							
								 
							 excelCellValueWrite.writeValueToCell(df.format(TotalAmountinDouble), r, map.get("Total_Amount"), DriverSheetPath, SheetName);
							 excelCellValueWrite.writeValueToCell(df.format(TotalVATAmtDouble), r, map.get("TotalVAT_Amount"), DriverSheetPath, SheetName);

		 
							 System.out.println("VATamt get is:"+TotalVATAmtDouble);
								System.out.println("Total Amount get is:"+TotalAmountinDouble);
								
								
				
			
					Boolean thirdRes=Batch_JobRun.RunJob_SITEnv(AppworxURL,AppworxUserName,AppworxPassword,TCName,ResultPath,thirdJobName);

					if(thirdRes){
								System.out.println(thirdJobName+" job executed successfully ");				
								utilityFileWriteOP.writeToLog("Batch Job Execution", thirdJobName, "PASS");
						}

					else{
								System.out.println("Execution failed  for the batch job   "+thirdJobName);
								utilityFileWriteOP.writeToLog("Batch Job Execution", thirdJobName, "FAIL");
						  }	
					
					System.setProperty(DriverName,DriverPath);
					
					if((DriverType.equalsIgnoreCase("ChromeDriver"))&&(BrowserPath.equalsIgnoreCase("Default"))){
						HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
						chromePrefs.put("profile.default_content_settings.popups", 0);
						chromePrefs.put("download.prompt_for_download", "false");
						chromePrefs.put("download.", "false");
						chromePrefs.put("download.default_directory", System.getProperty("user.dir"));
						ChromeOptions options = new ChromeOptions();
					//	options.setBinary(ChromeBrowserExePath);

						options.setExperimentalOption("prefs", chromePrefs);
						options.setExperimentalOption("useAutomationExtension", false);
						DesiredCapabilities cap = DesiredCapabilities.chrome();
						cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
						cap.setCapability(ChromeOptions.CAPABILITY, options);
						driver = new ChromeDriver(cap);
						wait=new WebDriverWait(driver, 60);			
						driver.manage().deleteAllCookies();
						driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
						
						wait = new WebDriverWait(driver, 30);
						driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);	
					
						
						driver.manage().deleteAllCookies();
						driver.get(S3URL);
						driver.manage().window().maximize();
					}
					
					
				
					boolean logins3_1 = McColls_Amazon_RackSpace.aws_login(driver, wait, TestCaseNo, s3username, s3password,ResultPath,xwpfRun, logger);
					if(logins3_1==true) {
						
						logger.log(LogStatus.PASS, "Logged in to Amazon Rackspace");
					}else{

						
						logger.log(LogStatus.FAIL, "Error occured in login to Amazon Rackspace");
					throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}


					//*****************************************Navigate to S3**********************************
					boolean navigatetos3_1 = McColls_Amazon_RackSpace.navigateToS3(driver, wait, TestCaseNo,ResultPath,xwpfRun, logger);
					if(navigatetos3_1==true) {
						
						logger.log(LogStatus.PASS, "Navigated to S3 in Amazon Rackspace");
					}
					else{
						
						logger.log(LogStatus.FAIL, "Error occured in navigating through Amazon Rackspace");
						throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}


					//***************************************Navigation to specific folder at S3**********************

					boolean validatefileAtS3_1 = McColls_Amazon_RackSpace.searchFileinBucketPath(S3BucketPath2,"w-har-"+AllOrderIDs+".csv",driver, wait,TestCaseNo,ResultPath,xwpfRun, logger);
					if(validatefileAtS3_1==true) {
						
						logger.log(LogStatus.PASS, "File validated at S3 successfully");
//						driver.quit();
						
					}
					else{
						
						logger.log(LogStatus.FAIL, "Error occured in navigating through Amazon Rackspace");
						throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}
					
					driver.get(S3URL);
					
					boolean logins3_2 = McColls_Amazon_RackSpace.aws_login(driver, wait, TestCaseNo, s3username, s3password,ResultPath,xwpfRun, logger);
					if(logins3_2==true) {
						
						logger.log(LogStatus.PASS, "Logged in to Amazon Rackspace");
					}else{

						
						logger.log(LogStatus.FAIL, "Error occured in login to Amazon Rackspace");
					throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}

					
					String task="wholesale-event-processor-task-"+environment;
							System.out.println("Task name"+task);
					
						String[] taskid=new String[1];
						String taskID="";
						boolean LambdaJobRun = Order_Consolidation_Utilities.Ecs_task_runner_job_trigger_task_ID_Retrieve(driver, wait, TestCaseNo, "ECS", task, ResultPath,xwpfRun,taskid);
						
						 if(LambdaJobRun==true) {
							 logger.log(LogStatus.PASS, "ECS Task Runner JOB triggered Successfully");
//							 Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "2", "ECS Task Runner JOB", "ECS Task Runner JOB triggered Successfully.", "PASS", ResultPath);
							 System.out.println("Outsidecaller script taskid "+taskid[0]);
//							 taskID=taskid[0];
						 }
									
						else{
							logger.log(LogStatus.FAIL, "Error Occured during ECS Task Runner JOB");
//							Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "2", "ECS Task Runner JOB", "Error Occured during ECS Task Runner JOB.", "FAIL", ResultPath);
						throw new MyException("Test Stopped Because of Failure. Please check Execution log");
						}
						 
				

							//*****************************************Navigate to S3**********************************
							boolean navigatetos3_3 = McColls_Amazon_RackSpace.navigateToS3(driver, wait, TestCaseNo,ResultPath,xwpfRun, logger);
							if(navigatetos3_3==true) {
								
								logger.log(LogStatus.PASS, "Navigated to S3 in Amazon Rackspace");
							}
							else{
								
								logger.log(LogStatus.FAIL, "Error occured in navigating through Amazon Rackspace");
								throw new MyException("Test Stopped Because of Failure. Please check Execution log");
							}


							//***************************************Navigation to specific folder at S3**********************

							boolean validatefileAtS3_3 = McColls_Amazon_RackSpace.searchFileinBucketPath(S3BucketPath3,"w-har-"+AllOrderIDs+".csv",driver, wait,TestCaseNo,ResultPath,xwpfRun, logger);
							if(validatefileAtS3_3==true) {
								
								logger.log(LogStatus.PASS, "File validated at S3 successfully");
								driver.quit();
								System.out.println("Driver is closed");
							}
							else{
								
								logger.log(LogStatus.FAIL, "Error occured in navigating through Amazon Rackspace");
								throw new MyException("Test Stopped Because of Failure. Please check Execution log");
							}
							
							
							boolean invoicedRestGet = Invoice_Miscellaneous_Functions.GetOrderStatusValidation(ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue, OrderID[order], "invoiced", TestCaseNo,ResultPath,xwpfRun,logger);


							if(invoicedRestGet==true) {
								Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "2", "Get Order Service Call", "Order as Invoiced Validated Successfully.", "PASS", ResultPath);
								logger.log(LogStatus.PASS, "Order as Invoiced validated Successfully");		

							}

							else{
								logger.log(LogStatus.FAIL, "Error Occured during Order validation");
								Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "2", "Get Order Service Call", "Error Occured during Order Validation.", "FAIL", ResultPath);

								throw new MyException("Test Stopped Because of Failure. Please check Execution log");
							}

							System.out.println("VATamt get is:"+TotalVATAmtDouble);
							String VATAmt=String.valueOf(TotalVATAmtDouble);
							System.out.println("Total Amount get is:"+TotalAmountinDouble);
							String TotAmt=String.valueOf(TotalAmountinDouble);
					
					boolean otherValuesRestGet = Invoice_Miscellaneous_Functions.GetOtherValuesValidation(ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue, OrderID[order], "invoiced",InvoiceId,TotAmt,VATAmt, TestCaseNo,ResultPath,xwpfRun,logger);


					if(otherValuesRestGet==true) {
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "2", "Get Order Service Call", "Order as Invoiced Validated Successfully.", "PASS", ResultPath);
						logger.log(LogStatus.PASS, "Order as Invoiced validated Successfully");		
						Final_Result = "PASS";
					}

					else{
						logger.log(LogStatus.FAIL, "Error Occured during Order validation");
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "2", "Get Order Service Call", "Error Occured during Order Validation.", "FAIL", ResultPath);
						Final_Result = "FAIL";
						throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}
							
					
							
				}
				
			}
			
		

		}
		catch(Exception e) {
			e.printStackTrace();

			Final_Result = "FAIL";
			excelCellValueWrite.writeValueToCell(Final_Result, r, 3, TestDataPath, SheetName);

			Assert.assertTrue(false);
		}
		finally
		
		{    
			
			System.out.println("Consolidated screen shot "+(consolidatedScreenshotpath+Final_Result+"1.docx"));
			FileOutputStream out1 = new FileOutputStream(consolidatedScreenshotpath+Final_Result+"1.docx");
		doc.write(out1);
		out1.close();


		//Reporting_Utilities.writeCloseRowToHTMLLog(TestCaseNo, ResultPath);
		RowGenerator.collateHTMLLog(TestCaseNo, ResultPath);
		}

	}
}

