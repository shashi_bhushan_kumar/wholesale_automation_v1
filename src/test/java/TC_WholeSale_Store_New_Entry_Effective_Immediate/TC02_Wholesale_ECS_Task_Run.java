package TC_WholeSale_Store_New_Entry_Effective_Immediate;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.testng.Assert;
import org.testng.annotations.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

import Additional_Functions.McColls_Amazon_RackSpace;
import Additional_Functions.Wholesalewebportalfunctions_Functions;
import Utilities.*;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;



public class TC02_Wholesale_ECS_Task_Run {
	
	String URL;
	//String DynamoURL;
	String DriverPath;
	String DriverName;
	String DriverType;
	String BrowserPath;
	String ServerName;
	String SheetName;
	String Final_Result="";
	String Downloadfilepath="";
	String TestDataPath;
	String location="";
	ExtentReports extent;
	ExtentTest logger;
	Integer stepnum=0;
	
	String ResultPath=null;
	String downloadFilepath="";
	
	String ChromeBrowserExePath=null;

	
	String AwsUsername = null;
	String Awspassword = null;
	String Searchkey = null;
	String jobname = null;
	public WebDriver driver = null;

	public WebDriverWait wait = null;
	//String downloadFilepath = "S:/Mordering 3B/Automation/Files";
	//********************Declaring Environment variable org.apache.commons.logging.Log**********************
	static {
		System.setProperty("org.apache.commons.logging.Log",
		                        "org.apache.commons.logging.impl.NoOpLog");
		}
	//*******************************************************************************************************
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@BeforeTest
	public void setUp() throws Exception {
		
		URL=ProjectConfigurations.LoadProperties("Portal_ProjectURL");
		//DynamoURL=ProjectConfigurations.LoadProperties("Relex_DynamoURL");
		DriverPath=ProjectConfigurations.LoadProperties("Portal_DriverPath");
		DriverName=ProjectConfigurations.LoadProperties("Portal_DriverName");
		DriverType=ProjectConfigurations.LoadProperties("Portal_DriverType");
		BrowserPath=ProjectConfigurations.LoadProperties("Portal_BrowserPath");
		ServerName=ProjectConfigurations.LoadProperties("AutomationServerName");
		Downloadfilepath=ProjectConfigurations.LoadProperties("downloadpath");
		
		AwsUsername = ProjectConfigurations.LoadProperties("AWS_Username");
		Awspassword= ProjectConfigurations.LoadProperties("AWS_Password");
		Searchkey = ProjectConfigurations.LoadProperties("AWS_SearchKey");
		jobname = ProjectConfigurations.LoadProperties("AWS_Jobname");
		//////////////////Setting up user Brwoser////////////////////////////////////////
		System.setProperty(DriverName,DriverPath);
		HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
		chromePrefs.put("profile.default_content_settings.popups", 0);
		chromePrefs.put("download.prompt_for_download", "false");
		chromePrefs.put("download.", "false");
		chromePrefs.put("download.default_directory", System.getProperty("user.dir")+Downloadfilepath);
		System.out.println("Downloadpath"+System.getProperty("user.dir")+Downloadfilepath);
		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("prefs", chromePrefs);
		options.setExperimentalOption("useAutomationExtension", false);
		DesiredCapabilities cap = DesiredCapabilities.chrome();
		cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		cap.setCapability(ChromeOptions.CAPABILITY, options);
		driver = new ChromeDriver(cap);
		wait=new WebDriverWait(driver, 20);			
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		driver.get("https://rax-598f5024ec80460190f52fc91b01825a.signin.aws.amazon.com/console");
	
		driver.manage().window().maximize();

		AwsUsername="usr-glob-n-mo-testautomation-user-002";
				
		Awspassword="]S1oeO-|Xju7";
		
		
		 driver.manage().window().maximize();
		
		// to be removed..................	
		
		TestDataPath=ProjectConfigurations.LoadProperties("Portal_TestDataPath");
		SheetName=ProjectConfigurations.LoadProperties("Portal_SheetName");
		downloadFilepath=ProjectConfigurations.LoadProperties("Portal_DownloadPath");
//				ResultPath="Results/";
		//*********************Setting up Result Path*****************************
//		ResultPath=System.getProperty("resultpath");
		if(ResultPath==null){
			ResultPath=Reporting_Utilities.createResultPath("ECS_Task_Run_"+getCurrentDate.getISTDateddMM()+"");
		}
		System.out.println("Result path is "+ResultPath);
		//************************************************************************
		
		 //*********************************Initialising Extent Report Variables**********************************
		 extent = new ExtentReports (System.getProperty("user.dir") +ResultPath+"Task_Run_SeleniumReport.html", false);
		 extent.loadConfig(new File(System.getProperty("user.dir")+"\\extent-config.xml"));
		 //*******************************************************************************************************
	}

	@AfterTest
	public void tearDown() throws Exception {
		extent.endTest(logger);
		extent.flush();
		extent.close();	
		driver.quit();
		Thread.sleep(5000);
		
	}

	@Test
	public void test() throws InterruptedException,IOException {
		String TestKeyword = "ECS_Job_Run";
		String TestCaseNo = null;
		String TestCaseName = null;
		String Keyword = null;
		String DriverSheetPath = TestDataPath;
		System.out.println("Test data path:"+TestDataPath);
		String driversheetname=SheetName;
		System.out.println("SheetName:"+SheetName);
		
		
		
		//--------------------------------------------
		
		//--------------------Doc File---------------------
		XWPFDocument doc = new XWPFDocument();
        XWPFParagraph p = doc.createParagraph();
        XWPFRun xwpfRun = p.createRun();
        
        String consolidatedScreenshotpath="";
        //-------------------------------------------------

		int r = 0;
		try {
			
			
			//-----------------------Extent Report-----------
			logger = extent.startTest("WholeSale_ECS_Task_Run");
			//-----------------------------------------------
			
			
			int rows = 0;
			int occurances = 0;
			Workbook wrk1 = Workbook.getWorkbook(new File(TestDataPath));
			Sheet sheet1 = wrk1.getSheet(SheetName);
			System.out.println();
			rows = sheet1.getRows();
			Cell[] FirstRow = sheet1.getRow(0);
			Map<String, Integer> map = new HashMap<String, Integer>();
			for(int i=0; i < FirstRow.length; i++)
			{
				map.put(FirstRow[i].getContents().trim(), i);
			}
			
			
			for(r=1; r<rows; r++) {
				Keyword = sheet1.getCell(map.get("Keyword"), r).getContents().trim();
				
									

				if(occurances>0){
			    	break;
				 }
				
				if(Keyword.equalsIgnoreCase(TestKeyword)) {
					System.out.println("Keyword: "+Keyword);
					System.out.println("TestKeyword: "+TestKeyword);
					occurances=occurances+1;
					Keyword = sheet1.getCell(map.get("Keyword"), r).getContents().trim();
					TestCaseNo = sheet1.getCell(map.get("TestCaseNo"), r).getContents().trim();
					TestCaseName = sheet1.getCell(map.get("TestCaseName"), r).getContents().trim();
					
					
					//****************************Create Path for Word Document and Screenshots****************************************
					File screenshotpath = new File(System.getProperty("user.dir")+ResultPath+"Screenshot/"+TestCaseNo+"/");
					screenshotpath.mkdirs();
					consolidatedScreenshotpath=System.getProperty("user.dir")+"/"+ResultPath+"Screenshot/"+TestCaseNo+"/"+TestCaseNo+"_";
					//****************************************************************************************************************** 
					
		  
					//-------------------HTML Header--------------------
		 
					Reporting_Utilities.writeHeaderToHTMLLog(TestCaseNo, TestCaseName, ResultPath);
					utilityFileWriteOP.writeToLog("*********************************START**********************************",ResultPath);	 			 
					utilityFileWriteOP.writeToLog(getCurrentDate.getISTTime(),ResultPath);
					//********************Declaring environment variables for stepnumber*******************
					
					try{
						stepnum=Integer.parseInt(System.getProperty("stepnumber"));	
					}catch(Exception e){
						System.setProperty("stepnumber", "1");
						stepnum=Integer.parseInt(System.getProperty("stepnumber"));	
					}
					
					
					
					//*****************************************Login to AWS**********************************
					
					boolean AwsLogin = McColls_Amazon_RackSpace.aws_login(driver, wait, TestCaseNo, AwsUsername, Awspassword, ResultPath,xwpfRun,logger);

					if(AwsLogin==true) {
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "AWS LOGIN", "AWS Logged In Successfully.", "PASS", ResultPath);
						logger.log(LogStatus.PASS, "Logged in to AWS successfully");
					}else{
						 
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "AWS LOGIN", "Error Occured during AWS Login.", "FAIL", ResultPath);
						logger.log(LogStatus.FAIL, "Error occured during loging to WebPortal");
						throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}
					stepnum++;
		
	
	boolean jobRun = McColls_Amazon_RackSpace.Store_Entry_Ecs_task_job_trigger(driver, wait, TestCaseNo, Searchkey,jobname, ResultPath,xwpfRun, logger);
	if(jobRun==true) {
		Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "ECS Task Runner JOB", "ECS Task Runner JOB triggered Successfully.", "PASS", ResultPath);
		logger.log(LogStatus.PASS, "ECS Task Runner JOB triggered Successfully.");
	}else{
		 
		Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "ECS Task Runner JOB", "Error occured while ECS job run", "FAIL", ResultPath);
		logger.log(LogStatus.FAIL, "Error occured while ECS job run");
		throw new MyException("Test Stopped Because of Failure. Please check Execution log");
	}
	stepnum++;
	
	
if (AwsLogin && jobRun ){
		Final_Result="PASS";	 						 
		utilityFileWriteOP.writeToLog(TestCaseNo, "ECS Task run successfully completed ", "PASS",ResultPath);
		excelCellValueWrite.writeValueToCell(Final_Result, r, 3, DriverSheetPath, driversheetname);							  
		Assert.assertTrue(true);
	}else {
		Final_Result="FAIL"; 
		excelCellValueWrite.writeValueToCell(Final_Result, r, 3, DriverSheetPath, driversheetname);
		utilityFileWriteOP.writeToLog(TestCaseNo, "ECS Job run failed", "FAIL",ResultPath);
		Assert.assertTrue(false);
	}
	
			}
		
		
	} 
	
	
		}
	
	
	
		catch (Exception e) {
			
		e.printStackTrace();
		
		 Final_Result="FAIL"; 
		  
	     excelCellValueWrite.writeValueToCell(Final_Result, r, 3, DriverSheetPath, driversheetname);
	     Assert.assertTrue(false);
	}
	finally
	{  
		
		RowGenerator.collateHTMLLog(TestCaseNo, ResultPath);
		FileOutputStream out1 = new FileOutputStream(consolidatedScreenshotpath+Final_Result+".docx");
		doc.write(out1);
		out1.close();
		doc.close();
		logger.log(LogStatus.INFO, "Evidance link for - "+TestCaseName+" <a href='file:///"+consolidatedScreenshotpath+Final_Result+".docx"+"'>EvidenceLink</a>");
		//Reporting_Utilities.writeCloseRowToHTMLLog(TestCaseNo, ResultPath);

	}}}
	
