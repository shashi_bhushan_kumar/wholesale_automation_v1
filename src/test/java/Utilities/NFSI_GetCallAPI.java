package Utilities;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.poi.xwpf.usermodel.XWPFRun;

import Utilities.utilityFileWriteOP;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class NFSI_GetCallAPI {
	
	
	
	@SuppressWarnings("finally")
	public static boolean GetOrderStatusValidation(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String tcid,String ResultPath, XWPFRun xwpfrun,ExtentTest logger) throws IOException {
		
		boolean res = false;

	
		CloseableHttpResponse response=null;
	    
	    CredentialsProvider credsProvider = new BasicCredentialsProvider();
		
	    credsProvider.setCredentials(
	    		new AuthScope(ProxyHostName, ProxyPort),
	    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

	    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

	    try {
			
	    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
	        
	    	HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
	        
	        RequestConfig config = RequestConfig.custom()
	                .setProxy(proxy)
	                .build();
	        
	        
	        
	      //  System.out.println("get call url is "+UrlTail +"/"+ OrderID + "?" + ApiKey);
	        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "?" + ApiKey);
	     
	        httpget.setConfig(config);
	        
	        httpget.addHeader(AuthorizationKey, AuthorizationValue);
	        	        
	        response = httpclient.execute(target, httpget);
	        
	        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
	        logger.log(LogStatus.INFO, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
	                
	       // System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);

	        String jsonresponse = EntityUtils.toString(response.getEntity());
	        
	        
	        System.out.println("Response is "+jsonresponse);
	        
	        utilityFileWriteOP.writeToLog(tcid, "Response  is "+jsonresponse,"",ResultPath,xwpfrun,"");
	        
	
	        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
	        JsonObject jsonobject = jsonelement.getAsJsonObject();
	        
	        
	        jsonobject =jsonobject.getAsJsonObject("orderInfo");
	        
	        JsonElement orderIDJson =jsonobject.get("orderId");
	    	
	       // JsonObject jsonobject1=jarray.get(0).getAsJsonObject();	    	
	    	
	        if(orderIDJson.getAsString().contentEquals(OrderID)){
	        	
	        	
	        	
	       utilityFileWriteOP.writeToLog(tcid, "Order details validated successfully in API response ","",ResultPath,xwpfrun,"");
	       
	       logger.log(LogStatus.PASS, "Order details validated successfully in API response ");
	       
	       res=true;
	       
	       
	        	
	        	
	        }
		        	
	        
	        
	        else {
	        	
	        	
	        	logger.log(LogStatus.FAIL, "Order details validation failed in API response data");
	        	  utilityFileWriteOP.writeToLog(tcid, "Order details validation failed in API response data","",ResultPath,xwpfrun,"");
	        	
	        }
		       
	    	
	    	/*
		        
		        JsonArray jarray = jsonobject.getAsJsonArray("orders");
		    	
		        
		        
		        JsonObject jsonobject1=jarray.get(0).getAsJsonObject();
		    	String statusCurrent = jsonobject1.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
		    	*/

	        
	        
	     /*
	         
	   for (int j=0;j<120;j++){
	        
	        
	       Thread.sleep(5000);
	        	
	        	
	        System.out.println("Order ID is " + OrderID);
	        
	        
	        utilityFileWriteOP.writeToLog(tcid, "Status for ", "Order ID "+OrderID);
	        logger.log(LogStatus.INFO, "Status for Order ID: "+OrderID);
	        
	        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
	        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");
	        logger.log(LogStatus.INFO, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
	        response = httpclient.execute(target, httpget);

	        
	        
	      //  System.out.println("Response code is "+response.getStatusLine().getStatusCode());
	        
	        
	        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");
	        logger.log(LogStatus.INFO, "Response code displayed as: "+response.getStatusLine().getStatusCode());
	        String jsonresponse = EntityUtils.toString(response.getEntity());
	        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath,xwpfrun,"");
	        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
	        JsonObject jsonobject = jsonelement.getAsJsonObject();
	        JsonArray jarray = jsonobject.getAsJsonArray("orders");
	    	JsonObject jsonobject1=jarray.get(0).getAsJsonObject();
	    	String statusCurrent = jsonobject1.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
	    	
	    	
	    	
	    	if(statusCurrent.equals(statusCurrentValue)) {
		        	res = true;
					utilityFileWriteOP.writeToLog(tcid, "Orders StatusCurrent is "+statusCurrent, "Success",ResultPath,xwpfrun,"");
					logger.log(LogStatus.PASS, "Orders StatusCurrent is : "+statusCurrent);
					
					System.out.println("Orders statusCurrent is " +statusCurrent);
					
				     break;
				
		        }
		        
		        else{
		        	res = false;
					utilityFileWriteOP.writeToLog(tcid, "Orders StatusCurrent is "+statusCurrent,"FAIL",ResultPath,xwpfrun,"");
					logger.log(LogStatus.FAIL, "Orders StatusCurrent is : "+statusCurrent);
					System.out.println("statusCurrent is " +statusCurrent);
					
					continue;
		        	
		        }
	        	
	       
	        }
	        
	     */ 
	    	
		}  //end of try
	    
	    
	    
	    catch (Exception e) {
			// TODO: handle exception
			
			e.printStackTrace();
			
			
			logger.log(LogStatus.FAIL, "Error occurred during GET call");
      	  utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call","",ResultPath,xwpfrun,"");
			
			
			
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
	        
			res = false;
			
			
			//return res;
		}
	    
	    finally{
	        
	     response.close();
	     
	     return res;
	        
	}
	
	}
	
	
@SuppressWarnings("finally")
public static boolean GetOrderEvents(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String tcid,String ResultPath, XWPFRun xwpfrun,ExtentTest logger) throws IOException {
		
		boolean res = false;
		CloseableHttpResponse response=null;
	    CredentialsProvider credsProvider = new BasicCredentialsProvider();
		credsProvider.setCredentials(
	    		new AuthScope(ProxyHostName, ProxyPort),
	    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials
		CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();
		try {
			HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
	        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
	        RequestConfig config = RequestConfig.custom()
	                .setProxy(proxy)
	                .build();
	      //  System.out.println("get call url is "+UrlTail +"/"+ OrderID + "?" + ApiKey);
	        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/events?" + ApiKey);
	        httpget.setConfig(config);
	        httpget.addHeader(AuthorizationKey, AuthorizationValue);
	        response = httpclient.execute(target, httpget);
	        logger.log(LogStatus.INFO, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
	        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
	        String jsonresponse = EntityUtils.toString(response.getEntity());
	        System.out.println("Response is "+jsonresponse);
	        utilityFileWriteOP.writeToLog(tcid, "GetOrderEvents API response is "+jsonresponse, "",ResultPath,xwpfrun,"");
	        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
	        JsonObject jsonobject = jsonelement.getAsJsonObject();
	        JsonArray jarray = jsonobject.getAsJsonArray("events");
	        ArrayList <String> EventList=new ArrayList<String>();
	        for(int i=0;i<jarray.size();i++){
	        	JsonObject jsonobject1=jarray.get(i).getAsJsonObject();	        	
	        	String EventId = jsonobject1.get("eventId").toString().replaceAll("^\"|\"$", "");	    	
	        	EventList.add(EventId);
	        }
	        
	        
	        if(!(EventList.isEmpty())){
	            res = true;
				utilityFileWriteOP.writeToLog(tcid, "Events retrieve for the order  "+OrderID+" is "+EventList, "Success",ResultPath,xwpfrun,"");
				logger.log(LogStatus.PASS, "Events retrieve for the order  "+OrderID+" is "+EventList);
				System.out.println("Event list is "+EventList);
	        }else {
	        	utilityFileWriteOP.writeToLog(tcid, "No Events retrieved for the order  "+OrderID, "No events",ResultPath,xwpfrun,"");
	 			logger.log(LogStatus.FAIL, "No Events retrieved for the order  "+OrderID);
	 			System.out.println("Event list is "+EventList);
	 			res=false;
	        }


	    	
		}  //end of try
	    
	    
	    
	    catch (Exception e) {
			// TODO: handle exception
	    	utilityFileWriteOP.writeToLog(tcid, "Failed to retrieve events for Order "+OrderID, "FAIL"+e,ResultPath,xwpfrun,"");
 			logger.log(LogStatus.FAIL, "Failed to retrieve events for Order "+OrderID);
 			e.printStackTrace();
 			res = false;
			
		}
	    
	    finally{
	        
	    response.close();
	     
	     return res;
	        
	}
	
	}
	
	
	
	
	
	
	
	
	
	
	
}
	
	
	
	


