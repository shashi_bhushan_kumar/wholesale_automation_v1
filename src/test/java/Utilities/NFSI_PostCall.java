package Utilities;

import java.io.IOException;



import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.http.HttpHost;
import org.apache.http.NameValuePair;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.slf4j.Logger;



import Utilities.*;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;


public class NFSI_PostCall {
	
	public static ArrayList<HashMap<String, String>> FetchItemDetailsbyOrderID(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue,String OrderID,String tcid,String ResultPath,XWPFRun xwpfrun) throws IOException {

		//String res = null;
		
		ArrayList<NameValuePair> postParameters;
		ArrayList<HashMap<String, String>> listofOrderandItems = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> AllOrderDetails = null;
		HashMap<String, String> AllItemDetails = null;
		
		CloseableHttpResponse response=null;
	    
	    CredentialsProvider credsProvider = new BasicCredentialsProvider();
		
	    credsProvider.setCredentials(
	    		new AuthScope(ProxyHostName, ProxyPort),
	    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

	    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

	  
	    
	    try {
			
	    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
	        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
	        
	        RequestConfig config = RequestConfig.custom()
	                .setProxy(proxy)
	                .build();
	        
	        HttpPost httppost = new HttpPost("/oauth/token");
	        
	        httppost.setConfig(config);        
	       // httppost.addHeader(AuthorizationKey, AuthorizationValue);
	        
	        System.out.println("Executing request " + httppost.getRequestLine() + " to " + target + " via " + proxy);
	        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httppost.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");
	       
	        //String json = "{}";       
	      //  System.out.println(json);
	        
	        postParameters = new ArrayList<NameValuePair>();
	        postParameters.add(new BasicNameValuePair("grant_type", "client_credentials"));
	        postParameters.add(new BasicNameValuePair("client_id", "morrisonsfw"));
	        postParameters.add(new BasicNameValuePair("client_secret", "7sUufcSgJ894KytQ"));
	        
	        httppost.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));
	       // httppost.setEntity(new StringEntity(json));
			
	      
	        httppost.setHeader("Accept", "application/json");
			httppost.setHeader("Content-type", "application/x-www-form-urlencoded");
			//httppost.setHeader("client_id", "morrisonsfw");
			//httppost.setHeader("client_secret", "7sUufcSgJ894KytQ");
	
			response = httpclient.execute(target, httppost);
	        System.out.println(response);
	        
	        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
	        utilityFileWriteOP.writeToLog(tcid, "Response code is ", "Displayed as: "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");

	        String responseBody = EntityUtils.toString(response.getEntity());
	        
	        System.out.println(responseBody);
	        utilityFileWriteOP.writeToLog(tcid, "Response Body is ", " : "+responseBody,ResultPath,xwpfrun,"");
	        
	        //response.toString();
			if(response.getStatusLine().getStatusCode() != 200)
				throw new MyException("Test Stopped Because of Failure. Please check Execution log");
			
			
			JsonElement jsonelement = new JsonParser().parse(responseBody);
	        JsonObject jsonobject = jsonelement.getAsJsonObject();
			
	        String access_token = jsonobject.get("access_token").toString().replaceAll("^\"|\"$", "");
	        
	        System.out.println("Bearer "+access_token);
	        HttpGet httpget = new HttpGet(UrlTail +"/orders/"+ OrderID);
	        httpget.setConfig(config);
	        
	        httpget.addHeader(AuthorizationKey, "Bearer "+access_token);
	        
	        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
	        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");

	        response = httpclient.execute(target, httpget);
	        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
	        
	        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is ",ResultPath,xwpfrun,""+response.getStatusLine().getStatusCode());
	        
	        String jsonresponse = EntityUtils.toString(response.getEntity());
	        utilityFileWriteOP.writeToLog(tcid, "Response Body is ", " : "+jsonresponse,ResultPath,xwpfrun,"");

	        System.out.println(jsonresponse);
	        
	        jsonelement = new JsonParser().parse(jsonresponse);
	        jsonobject = jsonelement.getAsJsonObject();
	        
	        
	        // Add Order Details
	        String OrderIDfromGet = jsonobject.get("orderId").toString();
	        
	        String discountCodefromGet = jsonobject.get("discountCode").toString().replaceAll("^\"|\"$", "");
	        String totalDiscountfromGet = jsonobject.get("totalDiscount").toString().replaceAll("^\"|\"$", "");
	        String totalOrderValuefromGet = jsonobject.get("totalOrderValue").toString().replaceAll("^\"|\"$", "");
	        String customerfromGet = jsonobject.get("customer").toString().replaceAll("^\"|\"$", "");
	      
	        JsonElement jsonelementcustomer = new JsonParser().parse(customerfromGet);
		    JsonObject  jsonobjectcustomer = jsonelementcustomer.getAsJsonObject();
		    
		    String emailAddressfromGet = jsonobjectcustomer.get("emailAddress").toString().replaceAll("^\"|\"$", "");
		    String fullNamefromGet = jsonobjectcustomer.get("fullName").toString().replaceAll("^\"|\"$", "");
		    String marketingOptInfromGet = jsonobjectcustomer.get("marketingOptIn").toString().replaceAll("^\"|\"$", "");

	        
	        AllOrderDetails = new HashMap<String, String>();
	        
	        
	        AllOrderDetails.put("orderId", OrderIDfromGet);
	        AllOrderDetails.put("orderId", OrderIDfromGet);
	        AllOrderDetails.put("discountCode", discountCodefromGet);
	        AllOrderDetails.put("totalDiscount", totalDiscountfromGet);
	        AllOrderDetails.put("totalOrderValue", totalOrderValuefromGet);
	        AllOrderDetails.put("emailAddress", emailAddressfromGet);
	        AllOrderDetails.put("fullName", fullNamefromGet);
	        AllOrderDetails.put("marketingOptIn", marketingOptInfromGet);
	        
	        listofOrderandItems.add(AllOrderDetails);
	        
	        //Get Item Details
	        JsonArray jarray = jsonobject.getAsJsonArray("orderItems");
	        System.out.println(jarray.size());
	        for(int i=0; i<jarray.size(); i++) {
	        	
	        	jsonobject = jarray.get(i).getAsJsonObject();
		        String ItemID = jsonobject.get("itemId").toString();
		        String status = jsonobject.get("status").toString().replaceAll("^\"|\"$", "");
		        String notes = jsonobject.get("notes").toString().replaceAll("^\"|\"$", "");
		        String Itemdescription = jsonobject.get("description").toString().replaceAll("^\"|\"$", "");

		        
		        String messageOnCard = jsonobject.get("messageOnCard").toString().replaceAll("^\"|\"$", "");
		        String deliveryContactNumber = jsonobject.get("deliveryContactNumber").toString().replaceAll("^\"|\"$", "");
		        String deliveryInstructions = jsonobject.get("deliveryInstructions").toString().replaceAll("^\"|\"$", "");

		        String deliverydate = jsonobject.get("deliveryDate").toString().replaceAll("^\"|\"$", "");
		        deliverydate = deliverydate.substring(0, 10);
		        
		        String recipient = jsonobject.get("recipient").toString().replaceAll("^\"|\"$", "");
		        JsonElement jsonelementrecipient = new JsonParser().parse(recipient);
		        JsonObject jsonobjectrecipient = jsonelementrecipient.getAsJsonObject();
		        
		        String recipientFullname = jsonobjectrecipient.get("fullName").toString().replaceAll("^\"|\"$", "");

		        
		        String deliveryAddress = jsonobjectrecipient.get("deliveryAddress").toString().replaceAll("^\"|\"$", "");
		        JsonElement jsonelementdeliveryAddress = new JsonParser().parse(deliveryAddress);
		        JsonObject jsonobjectdeliveryAddress = jsonelementdeliveryAddress.getAsJsonObject();
		        
		        String addressline1 = jsonobjectdeliveryAddress.get("addressLine1").toString().replaceAll("^\"|\"$", "");
		        String addressline2 = jsonobjectdeliveryAddress.get("addressLine2").toString().replaceAll("^\"|\"$", "");
		        String town = jsonobjectdeliveryAddress.get("town").toString().replaceAll("^\"|\"$", "");
		        String county = jsonobjectdeliveryAddress.get("county").toString().replaceAll("^\"|\"$", "");
		      
		        String postcode = jsonobjectdeliveryAddress.get("postcode").toString().replaceAll("^\"|\"$", "");
		        
		        AllItemDetails = new HashMap<String, String>();
		        
		        AllItemDetails.put("itemId", ItemID);
		        AllItemDetails.put("status", status);
		        AllItemDetails.put("deliverydate", deliverydate);
		        AllItemDetails.put("postcode", postcode);
		        AllItemDetails.put("notes", notes);
		        AllItemDetails.put("Itemdescription", Itemdescription);
		        AllItemDetails.put("messageOnCard", messageOnCard);
		        AllItemDetails.put("deliveryContactNumber", deliveryContactNumber);
		        AllItemDetails.put("deliveryInstructions", deliveryInstructions);
		        AllItemDetails.put("recipientFullname", recipientFullname);
		        AllItemDetails.put("addressline1", addressline1);
		        AllItemDetails.put("addressline2", addressline2);
		        AllItemDetails.put("town", town);
		        AllItemDetails.put("county", county);
		        listofOrderandItems.add(AllItemDetails);
	        }
	        
	        
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during Fetching Order and Item Details ", "Due to :"+e, ResultPath,xwpfrun,"");
	        
			listofOrderandItems = null;
			return listofOrderandItems;
			
		}
	    
	    finally{
	        
	        response.close();
	        
	}
	    
	    return listofOrderandItems;
		
		
	}
	
	
	
	public static ArrayList<HashMap<String, String>> FetchItemDetailsbyOrderIDwithAddonItem(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue,String OrderID,String tcid,String ResultPath,XWPFRun xwpfrun) throws IOException {

		//String res = null;
		
		ArrayList<NameValuePair> postParameters;
		ArrayList<HashMap<String, String>> listofOrderandItems = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> AllOrderDetails = null;
		HashMap<String, String> AllItemDetails = null;
		
		CloseableHttpResponse response=null;
	    
	    CredentialsProvider credsProvider = new BasicCredentialsProvider();
		
	    credsProvider.setCredentials(
	    		new AuthScope(ProxyHostName, ProxyPort),
	    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

	    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

	  
	    
	    try {
			
	    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
	        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
	        
	        RequestConfig config = RequestConfig.custom()
	                .setProxy(proxy)
	                .build();
	        
	        HttpPost httppost = new HttpPost("/oauth/token");
	        
	        httppost.setConfig(config);        
	       // httppost.addHeader(AuthorizationKey, AuthorizationValue);
	        
	        System.out.println("Executing request " + httppost.getRequestLine() + " to " + target + " via " + proxy);
	        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httppost.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");
	       
	        //String json = "{}";       
	      //  System.out.println(json);
	        
	        postParameters = new ArrayList<NameValuePair>();
	        postParameters.add(new BasicNameValuePair("grant_type", "client_credentials"));
	        postParameters.add(new BasicNameValuePair("client_id", "morrisonsfw"));
	        postParameters.add(new BasicNameValuePair("client_secret", "7sUufcSgJ894KytQ"));
	        
	        httppost.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));
	       // httppost.setEntity(new StringEntity(json));
			
	      
	        httppost.setHeader("Accept", "application/json");
			httppost.setHeader("Content-type", "application/x-www-form-urlencoded");
			//httppost.setHeader("client_id", "morrisonsfw");
			//httppost.setHeader("client_secret", "7sUufcSgJ894KytQ");
	
			response = httpclient.execute(target, httppost);
	        System.out.println(response);
	        
	        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
	        utilityFileWriteOP.writeToLog(tcid, "Response code is ", "Displayed as: "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");

	        String responseBody = EntityUtils.toString(response.getEntity());
	        
	        System.out.println(responseBody);
	        utilityFileWriteOP.writeToLog(tcid, "Response Body is ", "Body :- "+responseBody,ResultPath,xwpfrun,"");
	        
	        //response.toString();
			if(response.getStatusLine().getStatusCode() != 200)
				throw new MyException("Test Stopped Because of Failure. Please check Execution log");
			
			
			JsonElement jsonelement = new JsonParser().parse(responseBody);
	        JsonObject jsonobject = jsonelement.getAsJsonObject();
			
	        String access_token = jsonobject.get("access_token").toString().replaceAll("^\"|\"$", "");
	        
	        System.out.println("Bearer "+access_token);
	        HttpGet httpget = new HttpGet(UrlTail +"/orders/"+ OrderID);
	        httpget.setConfig(config);
	        
	        httpget.addHeader(AuthorizationKey, "Bearer "+access_token);
	        
	        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
	        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");

	        response = httpclient.execute(target, httpget);
	        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
	        
	        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is ",ResultPath,xwpfrun,""+response.getStatusLine().getStatusCode());
	        
	        String jsonresponse = EntityUtils.toString(response.getEntity());
	        utilityFileWriteOP.writeToLog(tcid, "Response Body is ", "Body :- "+jsonresponse,ResultPath,xwpfrun,"");

	        System.out.println(jsonresponse);
	        
	        jsonelement = new JsonParser().parse(jsonresponse);
	        jsonobject = jsonelement.getAsJsonObject();
	        
	        
	        // Add Order Details
	        String OrderIDfromGet = jsonobject.get("orderId").toString();
	        
	        String discountCodefromGet = jsonobject.get("discountCode").toString().replaceAll("^\"|\"$", "");
	        String totalDiscountfromGet = jsonobject.get("totalDiscount").toString().replaceAll("^\"|\"$", "");
	        String totalOrderValuefromGet = jsonobject.get("totalOrderValue").toString().replaceAll("^\"|\"$", "");
	        String customerfromGet = jsonobject.get("customer").toString().replaceAll("^\"|\"$", "");
	      
	        JsonElement jsonelementcustomer = new JsonParser().parse(customerfromGet);
		    JsonObject  jsonobjectcustomer = jsonelementcustomer.getAsJsonObject();
		    
		    String emailAddressfromGet = jsonobjectcustomer.get("emailAddress").toString().replaceAll("^\"|\"$", "");
		    String fullNamefromGet = jsonobjectcustomer.get("fullName").toString().replaceAll("^\"|\"$", "");
		    String marketingOptInfromGet = jsonobjectcustomer.get("marketingOptIn").toString().replaceAll("^\"|\"$", "");

	        
	        AllOrderDetails = new HashMap<String, String>();
	        
	        
	        AllOrderDetails.put("orderId", OrderIDfromGet);
	        AllOrderDetails.put("discountCode", discountCodefromGet);
	        AllOrderDetails.put("totalDiscount", totalDiscountfromGet);
	        AllOrderDetails.put("totalOrderValue", totalOrderValuefromGet);
	        AllOrderDetails.put("emailAddress", emailAddressfromGet);
	        AllOrderDetails.put("fullName", fullNamefromGet);
	        AllOrderDetails.put("marketingOptIn", marketingOptInfromGet);
	        
	        listofOrderandItems.add(AllOrderDetails);
	        
	        //Get Item Details
	        JsonArray jarray = jsonobject.getAsJsonArray("orderItems");
	        for(int i=0; i<jarray.size(); i++) {
	        	jsonobject = jarray.get(i).getAsJsonObject();
		        String ItemID = jsonobject.get("itemId").toString();
		        String status = jsonobject.get("status").toString().replaceAll("^\"|\"$", "");
		        String notes = jsonobject.get("notes").toString().replaceAll("^\"|\"$", "");
		        String Itemdescription = jsonobject.get("description").toString().replaceAll("^\"|\"$", "");

		        
		        String messageOnCard = jsonobject.get("messageOnCard").toString().replaceAll("^\"|\"$", "");
		        String deliveryContactNumber = jsonobject.get("deliveryContactNumber").toString().replaceAll("^\"|\"$", "");
		        String deliveryInstructions = jsonobject.get("deliveryInstructions").toString().replaceAll("^\"|\"$", "");

		        
		        String deliverydate = jsonobject.get("deliveryDate").toString().replaceAll("^\"|\"$", "");
		        deliverydate = deliverydate.substring(0, 10);
		        
		        String recipient = jsonobject.get("recipient").toString().replaceAll("^\"|\"$", "");
		        JsonElement jsonelementrecipient = new JsonParser().parse(recipient);
		        JsonObject jsonobjectrecipient = jsonelementrecipient.getAsJsonObject();
		        
		        String recipientFullname = jsonobjectrecipient.get("fullName").toString().replaceAll("^\"|\"$", "");

		        
		        String deliveryAddress = jsonobjectrecipient.get("deliveryAddress").toString().replaceAll("^\"|\"$", "");
		        JsonElement jsonelementdeliveryAddress = new JsonParser().parse(deliveryAddress);
		        JsonObject jsonobjectdeliveryAddress = jsonelementdeliveryAddress.getAsJsonObject();
		        
		        String addressline1 = jsonobjectdeliveryAddress.get("addressLine1").toString().replaceAll("^\"|\"$", "");
		        String addressline2 = jsonobjectdeliveryAddress.get("addressLine2").toString().replaceAll("^\"|\"$", "");
		        String town = jsonobjectdeliveryAddress.get("town").toString().replaceAll("^\"|\"$", "");
		        String county = jsonobjectdeliveryAddress.get("county").toString().replaceAll("^\"|\"$", "");
		      
		        String postcode = jsonobjectdeliveryAddress.get("postcode").toString().replaceAll("^\"|\"$", "");
		        
		        AllItemDetails = new HashMap<String, String>();
		        
		        AllItemDetails.put("itemId", ItemID);
		        AllItemDetails.put("status", status);
		        AllItemDetails.put("deliverydate", deliverydate);
		        AllItemDetails.put("postcode", postcode);
		        AllItemDetails.put("notes", notes);
		        AllItemDetails.put("Itemdescription", Itemdescription);
		        AllItemDetails.put("messageOnCard", messageOnCard);
		        AllItemDetails.put("deliveryContactNumber", deliveryContactNumber);
		        AllItemDetails.put("deliveryInstructions", deliveryInstructions);
		        AllItemDetails.put("recipientFullname", recipientFullname);
		        AllItemDetails.put("addressline1", addressline1);
		        AllItemDetails.put("addressline2", addressline2);
		        AllItemDetails.put("town", town);
		        AllItemDetails.put("county", county);

		        String addonItems = jsonobject.get("addonItems").toString().replaceAll("^\"|\"$", "");
		        JsonElement jsonelementaddonItems = new JsonParser().parse(addonItems);
		        JsonArray jsonArrayaddonItems = jsonelementaddonItems.getAsJsonArray();
		        for(int m=0; m<jsonArrayaddonItems.size(); m++) {
		        	JsonObject jsonobjectaddonItems = jsonArrayaddonItems.get(m).getAsJsonObject();
		        	
		        	String description = jsonobjectaddonItems.get("description").toString().replaceAll("^\"|\"$", "");
		        	
		        	AllItemDetails.put("addonItem"+(m+1), description);
		        }
		        
		        
		        
		        listofOrderandItems.add(AllItemDetails);
		        
		        
		        
		       
		        
	        }
	        
	        
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during Fetching Order and Item Details ", "Due to :"+e, ResultPath,xwpfrun,"");
	        
			listofOrderandItems = null;
			return listofOrderandItems;
			
		}
	    
	    finally{
	        
	        response.close();
	        
	}
	    
	    return listofOrderandItems;
		
		
	}
	
	
	
	
	
	public static ArrayList<HashMap<String, String>> FetchItemDetailsbyItemIDwithAddonItem(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue,String OrderID,String ItemId,String tcid,String ResultPath,XWPFRun xwpfrun) throws IOException {

		//String res = null;
		
		ArrayList<NameValuePair> postParameters;
		ArrayList<HashMap<String, String>> listofOrderandItems = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> AllOrderDetails = null;
		HashMap<String, String> AllItemDetails = null;
		
		CloseableHttpResponse response=null;
	    
	    CredentialsProvider credsProvider = new BasicCredentialsProvider();
		
	    credsProvider.setCredentials(
	    		new AuthScope(ProxyHostName, ProxyPort),
	    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

	    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

	  
	    
	    try {
			
	    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
	        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
	        
	        RequestConfig config = RequestConfig.custom()
	                .setProxy(proxy)
	                .build();
	        
	        HttpPost httppost = new HttpPost("/oauth/token");
	        
	        httppost.setConfig(config);        
	       // httppost.addHeader(AuthorizationKey, AuthorizationValue);
	        
	        System.out.println("Executing request " + httppost.getRequestLine() + " to " + target + " via " + proxy);
	        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httppost.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");
	       
	        //String json = "{}";       
	      //  System.out.println(json);
	        
	        postParameters = new ArrayList<NameValuePair>();
	        postParameters.add(new BasicNameValuePair("grant_type", "client_credentials"));
	        postParameters.add(new BasicNameValuePair("client_id", "morrisonsfw"));
	        postParameters.add(new BasicNameValuePair("client_secret", "7sUufcSgJ894KytQ"));
	        
	        httppost.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));
	       // httppost.setEntity(new StringEntity(json));
			
	      
	        httppost.setHeader("Accept", "application/json");
			httppost.setHeader("Content-type", "application/x-www-form-urlencoded");
			//httppost.setHeader("client_id", "morrisonsfw");
			//httppost.setHeader("client_secret", "7sUufcSgJ894KytQ");
	
			response = httpclient.execute(target, httppost);
	        System.out.println(response);
	        
	        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
	        utilityFileWriteOP.writeToLog(tcid, "Response code is ", "Displayed as: "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");

	        String responseBody = EntityUtils.toString(response.getEntity());
	        
	        System.out.println(responseBody);
	        utilityFileWriteOP.writeToLog(tcid, "Response Body is ", "Body :- "+responseBody,ResultPath,xwpfrun,"");
	        
	        //response.toString();
			if(response.getStatusLine().getStatusCode() != 200)
				throw new MyException("Test Stopped Because of Failure. Please check Execution log");
			
			
			JsonElement jsonelement = new JsonParser().parse(responseBody);
	        JsonObject jsonobject = jsonelement.getAsJsonObject();
			
	        String access_token = jsonobject.get("access_token").toString().replaceAll("^\"|\"$", "");
	        
	        System.out.println("Bearer "+access_token);
	        HttpGet httpget = new HttpGet(UrlTail +"/orders/"+ OrderID+"/items/"+ItemId);
	        httpget.setConfig(config);
	        
	        httpget.addHeader(AuthorizationKey, "Bearer "+access_token);
	        
	        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
	        response = httpclient.execute(target, httpget);
	        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
	        
	        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is ",ResultPath,xwpfrun,""+response.getStatusLine().getStatusCode());
	        
	        String jsonresponse = EntityUtils.toString(response.getEntity());
	        System.out.println(jsonresponse);
	        
	        jsonelement = new JsonParser().parse(jsonresponse);
	        jsonobject = jsonelement.getAsJsonObject();
	        
	        
	        // Add Order Details
	        //String OrderIDfromGet = jsonobject.get("orderId").toString();
	        
	        String discountCodefromGet = jsonobject.get("discountCode").toString().replaceAll("^\"|\"$", "");
	        String totalDiscountfromGet = jsonobject.get("totalDiscount").toString().replaceAll("^\"|\"$", "");
	        String totalOrderValuefromGet = jsonobject.get("totalOrderValue").toString().replaceAll("^\"|\"$", "");
	        String customerfromGet = jsonobject.get("customer").toString().replaceAll("^\"|\"$", "");
	      
	        JsonElement jsonelementcustomer = new JsonParser().parse(customerfromGet);
		    JsonObject  jsonobjectcustomer = jsonelementcustomer.getAsJsonObject();
		    
		    String emailAddressfromGet = jsonobjectcustomer.get("emailAddress").toString().replaceAll("^\"|\"$", "");
		    String fullNamefromGet = jsonobjectcustomer.get("fullName").toString().replaceAll("^\"|\"$", "");
		    String marketingOptInfromGet = jsonobjectcustomer.get("marketingOptIn").toString().replaceAll("^\"|\"$", "");

	        
	        
	        AllOrderDetails = new HashMap<String, String>();
	        
	        
	        AllOrderDetails.put("orderId", OrderID);
	        AllOrderDetails.put("discountCode", discountCodefromGet);
	        AllOrderDetails.put("totalDiscount", totalDiscountfromGet);
	        AllOrderDetails.put("totalOrderValue", totalOrderValuefromGet);
	        AllOrderDetails.put("emailAddress", emailAddressfromGet);
	        AllOrderDetails.put("fullName", fullNamefromGet);
	        AllOrderDetails.put("marketingOptIn", marketingOptInfromGet);
	        
	        listofOrderandItems.add(AllOrderDetails);
	        
	        //Get Item Details
	        
	        String ItemID = jsonobject.get("itemId").toString(); 
	        String status = jsonobject.get("status").toString().replaceAll("^\"|\"$", "");
	        String notes = jsonobject.get("notes").toString().replaceAll("^\"|\"$", "");
	        String Itemdescription = jsonobject.get("description").toString().replaceAll("^\"|\"$", "");

	        
	        String messageOnCard = jsonobject.get("messageOnCard").toString().replaceAll("^\"|\"$", "");
	        String deliveryContactNumber = jsonobject.get("deliveryContactNumber").toString().replaceAll("^\"|\"$", "");
	        String deliveryInstructions = jsonobject.get("deliveryInstructions").toString().replaceAll("^\"|\"$", "");

	        
	        String recipient = jsonobject.get("recipient").toString().replaceAll("^\"|\"$", "");
	        JsonElement jsonelementrecipient = new JsonParser().parse(recipient);
	        JsonObject jsonobjectrecipient = jsonelementrecipient.getAsJsonObject();
	        
	        String recipientFullname = jsonobjectrecipient.get("fullName").toString().replaceAll("^\"|\"$", "");

	        
	        String deliveryAddress = jsonobjectrecipient.get("deliveryAddress").toString().replaceAll("^\"|\"$", "");
	        JsonElement jsonelementdeliveryAddress = new JsonParser().parse(deliveryAddress);
	        JsonObject jsonobjectdeliveryAddress = jsonelementdeliveryAddress.getAsJsonObject();
	        
	        String addressline1 = jsonobjectdeliveryAddress.get("addressLine1").toString().replaceAll("^\"|\"$", "");
	        String addressline2 = jsonobjectdeliveryAddress.get("addressLine2").toString().replaceAll("^\"|\"$", "");
	        String town = jsonobjectdeliveryAddress.get("town").toString().replaceAll("^\"|\"$", "");
	        String county = jsonobjectdeliveryAddress.get("county").toString().replaceAll("^\"|\"$", "");
	      
	        String postcode = jsonobjectdeliveryAddress.get("postcode").toString().replaceAll("^\"|\"$", "");
	     
	        String deliverydate = jsonobject.get("deliveryDate").toString().replaceAll("^\"|\"$", "");
	        deliverydate = deliverydate.substring(0, 10);
	        
	        AllItemDetails = new HashMap<String, String>();
	        
	        AllItemDetails.put("itemId", ItemID);
	        AllItemDetails.put("status", status);
	        AllItemDetails.put("deliverydate", deliverydate);
	        AllItemDetails.put("postcode", postcode);
	        AllItemDetails.put("notes", notes);
	        AllItemDetails.put("Itemdescription", Itemdescription);
	        AllItemDetails.put("messageOnCard", messageOnCard);
	        AllItemDetails.put("deliveryContactNumber", deliveryContactNumber);
	        AllItemDetails.put("deliveryInstructions", deliveryInstructions);
	        AllItemDetails.put("recipientFullname", recipientFullname);
	        AllItemDetails.put("addressline1", addressline1);
	        AllItemDetails.put("addressline2", addressline2);
	        AllItemDetails.put("town", town);
	        AllItemDetails.put("county", county);
	        
	        String addonItems = jsonobject.get("addonItems").toString().replaceAll("^\"|\"$", "");
	        JsonElement jsonelementaddonItems = new JsonParser().parse(addonItems);
	        JsonArray jsonArrayaddonItems = jsonelementaddonItems.getAsJsonArray();
	        for(int m=0; m<jsonArrayaddonItems.size(); m++) {
	        	JsonObject jsonobjectaddonItems = jsonArrayaddonItems.get(m).getAsJsonObject();
	        	
	        	String description = jsonobjectaddonItems.get("description").toString().replaceAll("^\"|\"$", "");
	        	
	        	AllItemDetails.put("addonItem"+(m+1), description);
	        }
	        
	        
	        
	        listofOrderandItems.add(AllItemDetails);

	       
	        
	        
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during Fetching Order and Item Details ", "Due to :"+e, ResultPath,xwpfrun,"");
	        
			listofOrderandItems = null;
			return listofOrderandItems;
			
		}
	    
	    finally{
	        
	        response.close();
	        
	}
	    
	    return listofOrderandItems;
		
		
	}

	
	public static ArrayList<HashMap<String, String>> FetchItemDetailsbyDeliveryDate(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue,String OrderID,String DeliveryDate,String tcid,String ResultPath,XWPFRun xwpfrun) throws IOException {

		//String res = null;
		
		ArrayList<NameValuePair> postParameters;
		ArrayList<HashMap<String, String>> listofOrderandItems = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> AllOrderDetails = null;
		HashMap<String, String> AllItemDetails = null;
		
		CloseableHttpResponse response=null;
	    
	    CredentialsProvider credsProvider = new BasicCredentialsProvider();
		
	    credsProvider.setCredentials(
	    		new AuthScope(ProxyHostName, ProxyPort),
	    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

	    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

	  
	    
	    try {
			
	    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
	        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
	        
	        RequestConfig config = RequestConfig.custom()
	                .setProxy(proxy)
	                .build();
	        
	        HttpPost httppost = new HttpPost("/oauth/token");
	        
	        httppost.setConfig(config);        
	       // httppost.addHeader(AuthorizationKey, AuthorizationValue);
	        
	        System.out.println("Executing request " + httppost.getRequestLine() + " to " + target + " via " + proxy);
	        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httppost.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");
	       
	        //String json = "{}";       
	      //  System.out.println(json);
	        
	        postParameters = new ArrayList<NameValuePair>();
	        postParameters.add(new BasicNameValuePair("grant_type", "client_credentials"));
	        postParameters.add(new BasicNameValuePair("client_id", "morrisonsfw"));
	        postParameters.add(new BasicNameValuePair("client_secret", "7sUufcSgJ894KytQ"));
	        
	        httppost.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));
	       // httppost.setEntity(new StringEntity(json));
			
	      
	        httppost.setHeader("Accept", "application/json");
			httppost.setHeader("Content-type", "application/x-www-form-urlencoded");
			//httppost.setHeader("client_id", "morrisonsfw");
			//httppost.setHeader("client_secret", "7sUufcSgJ894KytQ");
	
			response = httpclient.execute(target, httppost);
	        System.out.println(response);
	        
	        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
	        utilityFileWriteOP.writeToLog(tcid, "Response code is ", "Displayed as: "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");

	        String responseBody = EntityUtils.toString(response.getEntity());
	        
	        System.out.println(responseBody);
	        utilityFileWriteOP.writeToLog(tcid, "Response Body is ", "Body :- "+responseBody,ResultPath,xwpfrun,"");
	        
	        //response.toString();
			if(response.getStatusLine().getStatusCode() != 200)
				throw new MyException("Test Stopped Because of Failure. Please check Execution log");
			
			
			JsonElement jsonelement = new JsonParser().parse(responseBody);
	        JsonObject jsonobject = jsonelement.getAsJsonObject();
			
	        String access_token = jsonobject.get("access_token").toString().replaceAll("^\"|\"$", "");
	        
	        System.out.println("Bearer "+access_token);
	        
	        
	        String deliverymonth = DeliveryDate.split("/")[1];
	        String deliverymonthnum = null;
	        
	        switch (deliverymonth) {
			case "January":
				deliverymonthnum = "01";
				break;
				
			case "February":
				deliverymonthnum = "02";
				break;
				
			case "March":
				deliverymonthnum = "03";
				break;
				
			case "April":
				deliverymonthnum = "04";
				break;
				
			case "May":
				deliverymonthnum = "05";
				break;
				
			case "June":
				deliverymonthnum = "06";
				break;
				
			case "July":
				deliverymonthnum = "07";
				break;
				
			case "August":
				deliverymonthnum = "08";
				break;
				
			case "September":
				deliverymonthnum = "09";
				break;
				
			case "October":
				deliverymonthnum = "10";
				break;
				
			case "November":
				deliverymonthnum = "11";
				break;
				
			case "December":
				deliverymonthnum = "12";
				break;

			default:
				break;
			}
	        
	        String deliverydateforPostman = DeliveryDate.split("/")[0] +"/"+deliverymonthnum +"/"+ DeliveryDate.split("/")[2];
	        
	        HttpGet httpget = new HttpGet(UrlTail +"/orders/?delivery-date="+ deliverydateforPostman);
	        httpget.setConfig(config);
	        
	        httpget.addHeader(AuthorizationKey, "Bearer "+access_token);
	        
	        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
	        response = httpclient.execute(target, httpget);
	        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
	        
	        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is ",ResultPath,xwpfrun,""+response.getStatusLine().getStatusCode());
	        
	        String jsonresponse = EntityUtils.toString(response.getEntity());
	        System.out.println(jsonresponse);
	        
	        jsonelement = new JsonParser().parse(jsonresponse);
	        //jsonobject = jsonelement.getAsJsonObject();
	        JsonArray jarraybody = jsonelement.getAsJsonArray();
	        
	        for(int j=0; j<jarraybody.size(); j++) {
	        	
	        	
	        	jsonobject = jarraybody.get(j).getAsJsonObject();
	        	
	        	 // Add Order Details
		        String OrderIDfromGet = jsonobject.get("orderId").toString();
		        
		        if( !(OrderIDfromGet.equals(OrderID)) ) {
		        	continue;
		        }
		        
		        String discountCodefromGet = jsonobject.get("discountCode").toString().replaceAll("^\"|\"$", "");
		        String totalDiscountfromGet = jsonobject.get("totalDiscount").toString().replaceAll("^\"|\"$", "");
		        String totalOrderValuefromGet = jsonobject.get("totalOrderValue").toString().replaceAll("^\"|\"$", "");
		        String customerfromGet = jsonobject.get("customer").toString().replaceAll("^\"|\"$", "");
		      
		        JsonElement jsonelementcustomer = new JsonParser().parse(customerfromGet);
			    JsonObject  jsonobjectcustomer = jsonelementcustomer.getAsJsonObject();
			    
			    String emailAddressfromGet = jsonobjectcustomer.get("emailAddress").toString().replaceAll("^\"|\"$", "");
			    String fullNamefromGet = jsonobjectcustomer.get("fullName").toString().replaceAll("^\"|\"$", "");
			    String marketingOptInfromGet = jsonobjectcustomer.get("marketingOptIn").toString().replaceAll("^\"|\"$", "");

		        
		        
		        AllOrderDetails = new HashMap<String, String>();
		        
		        AllOrderDetails.put("orderId", OrderIDfromGet);
		        AllOrderDetails.put("discountCode", discountCodefromGet);
		        AllOrderDetails.put("totalDiscount", totalDiscountfromGet);
		        AllOrderDetails.put("totalOrderValue", totalOrderValuefromGet);
		        AllOrderDetails.put("emailAddress", emailAddressfromGet);
		        AllOrderDetails.put("fullName", fullNamefromGet);
		        AllOrderDetails.put("marketingOptIn", marketingOptInfromGet);
		        
		        listofOrderandItems.add(AllOrderDetails);
		        
		        
		        //Get Item Details
		        JsonArray jarray = jsonobject.getAsJsonArray("orderItems");
		        System.out.println(jarray.size());
		        for(int i=0; i<jarray.size(); i++) {
		        	
		        	jsonobject = jarray.get(i).getAsJsonObject();
			        String ItemID = jsonobject.get("itemId").toString();
			        String status = jsonobject.get("status").toString().replaceAll("^\"|\"$", "");
			        String notes = jsonobject.get("notes").toString().replaceAll("^\"|\"$", "");
			        String Itemdescription = jsonobject.get("description").toString().replaceAll("^\"|\"$", "");

			        
			        String deliverydate = jsonobject.get("deliveryDate").toString().replaceAll("^\"|\"$", "");
			        deliverydate = deliverydate.substring(0, 10);
			        
			        String messageOnCard = jsonobject.get("messageOnCard").toString().replaceAll("^\"|\"$", "");
			        String deliveryContactNumber = jsonobject.get("deliveryContactNumber").toString().replaceAll("^\"|\"$", "");
			        String deliveryInstructions = jsonobject.get("deliveryInstructions").toString().replaceAll("^\"|\"$", "");

			        
			        String recipient = jsonobject.get("recipient").toString().replaceAll("^\"|\"$", "");
			        JsonElement jsonelementrecipient = new JsonParser().parse(recipient);
			        JsonObject jsonobjectrecipient = jsonelementrecipient.getAsJsonObject();
			        
			        String recipientFullname = jsonobjectrecipient.get("fullName").toString().replaceAll("^\"|\"$", "");

			        
			        String deliveryAddress = jsonobjectrecipient.get("deliveryAddress").toString().replaceAll("^\"|\"$", "");
			        JsonElement jsonelementdeliveryAddress = new JsonParser().parse(deliveryAddress);
			        JsonObject jsonobjectdeliveryAddress = jsonelementdeliveryAddress.getAsJsonObject();
			        
			        String addressline1 = jsonobjectdeliveryAddress.get("addressLine1").toString().replaceAll("^\"|\"$", "");
			        String addressline2 = jsonobjectdeliveryAddress.get("addressLine2").toString().replaceAll("^\"|\"$", "");
			        String town = jsonobjectdeliveryAddress.get("town").toString().replaceAll("^\"|\"$", "");
			        String county = jsonobjectdeliveryAddress.get("county").toString().replaceAll("^\"|\"$", "");
			        
			        String postcode = jsonobjectdeliveryAddress.get("postcode").toString().replaceAll("^\"|\"$", "");
			        
			        AllItemDetails = new HashMap<String, String>();
			        
			        AllItemDetails.put("itemId", ItemID);
			        AllItemDetails.put("status", status);
			        AllItemDetails.put("deliverydate", deliverydate);
			        AllItemDetails.put("postcode", postcode);
			        AllItemDetails.put("notes", notes);
			        AllItemDetails.put("Itemdescription", Itemdescription);
			        AllItemDetails.put("messageOnCard", messageOnCard);
			        AllItemDetails.put("deliveryContactNumber", deliveryContactNumber);
			        AllItemDetails.put("deliveryInstructions", deliveryInstructions);
			        AllItemDetails.put("recipientFullname", recipientFullname);
			        AllItemDetails.put("addressline1", addressline1);
			        AllItemDetails.put("addressline2", addressline2);
			        AllItemDetails.put("town", town);
			        AllItemDetails.put("county", county);
			        
			        
			        listofOrderandItems.add(AllItemDetails);
		        }
		        
		        break;
		        
	        }
	        
	        
	       /*
	        //Get Item Details
	        JsonArray jarray = jsonobject.getAsJsonArray("orderItems");
	        System.out.println(jarray.size());
	        for(int i=0; i<jarray.size(); i++) {
	        	
	        	jsonobject = jarray.get(i).getAsJsonObject();
		        String ItemID = jsonobject.get("itemId").toString();
		        String status = jsonobject.get("status").toString().replaceAll("^\"|\"$", "");
		        
		        String deliverydate = jsonobject.get("deliveryDate").toString().replaceAll("^\"|\"$", "");
		        deliverydate = deliverydate.substring(0, 10);
		        
		        String recipient = jsonobject.get("recipient").toString().replaceAll("^\"|\"$", "");
		        JsonElement jsonelementrecipient = new JsonParser().parse(recipient);
		        JsonObject jsonobjectrecipient = jsonelementrecipient.getAsJsonObject();
		        
		        String deliveryAddress = jsonobjectrecipient.get("deliveryAddress").toString().replaceAll("^\"|\"$", "");
		        JsonElement jsonelementdeliveryAddress = new JsonParser().parse(deliveryAddress);
		        JsonObject jsonobjectdeliveryAddress = jsonelementdeliveryAddress.getAsJsonObject();
		        
		        String postcode = jsonobjectdeliveryAddress.get("postcode").toString().replaceAll("^\"|\"$", "");
		        
		        AllItemDetails = new HashMap<String, String>();
		        
		        AllItemDetails.put("itemId", ItemID);
		        AllItemDetails.put("status", status);
		        AllItemDetails.put("deliverydate", deliverydate);
		        AllItemDetails.put("postcode", postcode);

		        
		        
		        listofOrderandItems.add(AllItemDetails);
	        }*/
	        
	        
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during Fetching Order and Item Details ", "Due to :"+e, ResultPath,xwpfrun,"");
	        
			listofOrderandItems = null;
			return listofOrderandItems;
			
		}
	    
	    finally{
	        
	        response.close();
	        
	}
	    
	    return listofOrderandItems;
		
		
	}
	
	public static ArrayList<HashMap<String, String>> FetchItemDetailsbyDeliveryDatewithAddonItem(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue,String OrderID,String DeliveryDate,String tcid,String ResultPath,XWPFRun xwpfrun) throws IOException {

		//String res = null;
		
		ArrayList<NameValuePair> postParameters;
		ArrayList<HashMap<String, String>> listofOrderandItems = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> AllOrderDetails = null;
		HashMap<String, String> AllItemDetails = null;
		
		CloseableHttpResponse response=null;
	    
	    CredentialsProvider credsProvider = new BasicCredentialsProvider();
		
	    credsProvider.setCredentials(
	    		new AuthScope(ProxyHostName, ProxyPort),
	    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

	    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

	  
	    
	    try {
			
	    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
	        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
	        
	        RequestConfig config = RequestConfig.custom()
	                .setProxy(proxy)
	                .build();
	        
	        HttpPost httppost = new HttpPost("/oauth/token");
	        
	        httppost.setConfig(config);        
	       // httppost.addHeader(AuthorizationKey, AuthorizationValue);
	        
	        System.out.println("Executing request " + httppost.getRequestLine() + " to " + target + " via " + proxy);
	        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httppost.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");
	       
	        //String json = "{}";       
	      //  System.out.println(json);
	        
	        postParameters = new ArrayList<NameValuePair>();
	        postParameters.add(new BasicNameValuePair("grant_type", "client_credentials"));
	        postParameters.add(new BasicNameValuePair("client_id", "morrisonsfw"));
	        postParameters.add(new BasicNameValuePair("client_secret", "7sUufcSgJ894KytQ"));
	        
	        httppost.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));
	       // httppost.setEntity(new StringEntity(json));
			
	      
	        httppost.setHeader("Accept", "application/json");
			httppost.setHeader("Content-type", "application/x-www-form-urlencoded");
			//httppost.setHeader("client_id", "morrisonsfw");
			//httppost.setHeader("client_secret", "7sUufcSgJ894KytQ");
	
			response = httpclient.execute(target, httppost);
	        System.out.println(response);
	        
	        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
	        utilityFileWriteOP.writeToLog(tcid, "Response code is ", "Displayed as: "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");

	        String responseBody = EntityUtils.toString(response.getEntity());
	        
	        System.out.println(responseBody);
	        utilityFileWriteOP.writeToLog(tcid, "Response Body is ", "Body :- "+responseBody,ResultPath,xwpfrun,"");
	        
	        //response.toString();
			if(response.getStatusLine().getStatusCode() != 200)
				throw new MyException("Test Stopped Because of Failure. Please check Execution log");
			
			
			JsonElement jsonelement = new JsonParser().parse(responseBody);
	        JsonObject jsonobject = jsonelement.getAsJsonObject();
			
	        String access_token = jsonobject.get("access_token").toString().replaceAll("^\"|\"$", "");
	        
	        System.out.println("Bearer "+access_token);
	        
	        
	        String deliverymonth = DeliveryDate.split("/")[1];
	        String deliverymonthnum = null;
	        
	        switch (deliverymonth) {
			case "January":
				deliverymonthnum = "01";
				break;
				
			case "February":
				deliverymonthnum = "02";
				break;
				
			case "March":
				deliverymonthnum = "03";
				break;
				
			case "April":
				deliverymonthnum = "04";
				break;
				
			case "May":
				deliverymonthnum = "05";
				break;
				
			case "June":
				deliverymonthnum = "06";
				break;
				
			case "July":
				deliverymonthnum = "07";
				break;
				
			case "August":
				deliverymonthnum = "08";
				break;
				
			case "September":
				deliverymonthnum = "09";
				break;
				
			case "October":
				deliverymonthnum = "10";
				break;
				
			case "November":
				deliverymonthnum = "11";
				break;
				
			case "December":
				deliverymonthnum = "12";
				break;

			default:
				break;
			}
	        
	        String deliverydateforPostman = DeliveryDate.split("/")[0] +"/"+deliverymonthnum +"/"+ DeliveryDate.split("/")[2];
	        
	        HttpGet httpget = new HttpGet(UrlTail +"/orders/?delivery-date="+ deliverydateforPostman);
	        httpget.setConfig(config);
	        
	        httpget.addHeader(AuthorizationKey, "Bearer "+access_token);
	        
	        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
	        response = httpclient.execute(target, httpget);
	        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
	        
	        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is ",ResultPath,xwpfrun,""+response.getStatusLine().getStatusCode());
	        
	        String jsonresponse = EntityUtils.toString(response.getEntity());
	        System.out.println(jsonresponse);
	        
	        jsonelement = new JsonParser().parse(jsonresponse);
	        //jsonobject = jsonelement.getAsJsonObject();
	        JsonArray jarraybody = jsonelement.getAsJsonArray();
	        
	        for(int j=0; j<jarraybody.size(); j++) {
	        	
	        	
	        	jsonobject = jarraybody.get(j).getAsJsonObject();
	        	
	        	 // Add Order Details
		        String OrderIDfromGet = jsonobject.get("orderId").toString();
		        
		        if( !(OrderIDfromGet.equals(OrderID)) ) {
		        	continue;
		        }
		        
		        String discountCodefromGet = jsonobject.get("discountCode").toString().replaceAll("^\"|\"$", "");
		        String totalDiscountfromGet = jsonobject.get("totalDiscount").toString().replaceAll("^\"|\"$", "");
		        String totalOrderValuefromGet = jsonobject.get("totalOrderValue").toString().replaceAll("^\"|\"$", "");
		        String customerfromGet = jsonobject.get("customer").toString().replaceAll("^\"|\"$", "");
		      
		        JsonElement jsonelementcustomer = new JsonParser().parse(customerfromGet);
			    JsonObject  jsonobjectcustomer = jsonelementcustomer.getAsJsonObject();
			    
			    String emailAddressfromGet = jsonobjectcustomer.get("emailAddress").toString().replaceAll("^\"|\"$", "");
			    String fullNamefromGet = jsonobjectcustomer.get("fullName").toString().replaceAll("^\"|\"$", "");
			    String marketingOptInfromGet = jsonobjectcustomer.get("marketingOptIn").toString().replaceAll("^\"|\"$", "");

		        
		        
		        AllOrderDetails = new HashMap<String, String>();
		        
		        AllOrderDetails.put("orderId", OrderIDfromGet);
		        AllOrderDetails.put("discountCode", discountCodefromGet);
		        AllOrderDetails.put("totalDiscount", totalDiscountfromGet);
		        AllOrderDetails.put("totalOrderValue", totalOrderValuefromGet);
		        AllOrderDetails.put("emailAddress", emailAddressfromGet);
		        AllOrderDetails.put("fullName", fullNamefromGet);
		        AllOrderDetails.put("marketingOptIn", marketingOptInfromGet);
		        
		        listofOrderandItems.add(AllOrderDetails);
		        
		        
		        //Get Item Details
		        JsonArray jarray = jsonobject.getAsJsonArray("orderItems");
		        System.out.println(jarray.size());
		        for(int i=0; i<jarray.size(); i++) {
		        	
		        	jsonobject = jarray.get(i).getAsJsonObject();
			        String ItemID = jsonobject.get("itemId").toString();
			        String status = jsonobject.get("status").toString().replaceAll("^\"|\"$", "");
			        String notes = jsonobject.get("notes").toString().replaceAll("^\"|\"$", "");
			        String Itemdescription = jsonobject.get("description").toString().replaceAll("^\"|\"$", "");

			        
			        String deliverydate = jsonobject.get("deliveryDate").toString().replaceAll("^\"|\"$", "");
			        deliverydate = deliverydate.substring(0, 10);
			        
			        String messageOnCard = jsonobject.get("messageOnCard").toString().replaceAll("^\"|\"$", "");
			        String deliveryContactNumber = jsonobject.get("deliveryContactNumber").toString().replaceAll("^\"|\"$", "");
			        String deliveryInstructions = jsonobject.get("deliveryInstructions").toString().replaceAll("^\"|\"$", "");

			        
			        String recipient = jsonobject.get("recipient").toString().replaceAll("^\"|\"$", "");
			        JsonElement jsonelementrecipient = new JsonParser().parse(recipient);
			        JsonObject jsonobjectrecipient = jsonelementrecipient.getAsJsonObject();
			        
			        String recipientFullname = jsonobjectrecipient.get("fullName").toString().replaceAll("^\"|\"$", "");

			        
			        String deliveryAddress = jsonobjectrecipient.get("deliveryAddress").toString().replaceAll("^\"|\"$", "");
			        JsonElement jsonelementdeliveryAddress = new JsonParser().parse(deliveryAddress);
			        JsonObject jsonobjectdeliveryAddress = jsonelementdeliveryAddress.getAsJsonObject();
			        
			        String addressline1 = jsonobjectdeliveryAddress.get("addressLine1").toString().replaceAll("^\"|\"$", "");
			        String addressline2 = jsonobjectdeliveryAddress.get("addressLine2").toString().replaceAll("^\"|\"$", "");
			        String town = jsonobjectdeliveryAddress.get("town").toString().replaceAll("^\"|\"$", "");
			        String county = jsonobjectdeliveryAddress.get("county").toString().replaceAll("^\"|\"$", "");
			        
			        String postcode = jsonobjectdeliveryAddress.get("postcode").toString().replaceAll("^\"|\"$", "");
			        
			        AllItemDetails = new HashMap<String, String>();
			        
			        AllItemDetails.put("itemId", ItemID);
			        AllItemDetails.put("status", status);
			        AllItemDetails.put("deliverydate", deliverydate);
			        AllItemDetails.put("postcode", postcode);
			        AllItemDetails.put("notes", notes);
			        AllItemDetails.put("Itemdescription", Itemdescription);
			        AllItemDetails.put("messageOnCard", messageOnCard);
			        AllItemDetails.put("deliveryContactNumber", deliveryContactNumber);
			        AllItemDetails.put("deliveryInstructions", deliveryInstructions);
			        AllItemDetails.put("recipientFullname", recipientFullname);
			        AllItemDetails.put("addressline1", addressline1);
			        AllItemDetails.put("addressline2", addressline2);
			        AllItemDetails.put("town", town);
			        AllItemDetails.put("county", county);
			        
			        
			        listofOrderandItems.add(AllItemDetails);
			        
			        String addonItems = jsonobject.get("addonItems").toString().replaceAll("^\"|\"$", "");
			        JsonElement jsonelementaddonItems = new JsonParser().parse(addonItems);
			        JsonArray jsonArrayaddonItems = jsonelementaddonItems.getAsJsonArray();
			        for(int m=0; m<jsonArrayaddonItems.size(); m++) {
			        	JsonObject jsonobjectaddonItems = jsonArrayaddonItems.get(m).getAsJsonObject();
			        	
			        	String description = jsonobjectaddonItems.get("description").toString().replaceAll("^\"|\"$", "");
			        	
			        	AllItemDetails.put("addonItem"+(m+1), description);
			        }
			        
			        
			        
			        listofOrderandItems.add(AllItemDetails);
			        
		        }
		        
		        break;
		        
	        }
	        
	        
	       /*
	        //Get Item Details
	        JsonArray jarray = jsonobject.getAsJsonArray("orderItems");
	        System.out.println(jarray.size());
	        for(int i=0; i<jarray.size(); i++) {
	        	
	        	jsonobject = jarray.get(i).getAsJsonObject();
		        String ItemID = jsonobject.get("itemId").toString();
		        String status = jsonobject.get("status").toString().replaceAll("^\"|\"$", "");
		        
		        String deliverydate = jsonobject.get("deliveryDate").toString().replaceAll("^\"|\"$", "");
		        deliverydate = deliverydate.substring(0, 10);
		        
		        String recipient = jsonobject.get("recipient").toString().replaceAll("^\"|\"$", "");
		        JsonElement jsonelementrecipient = new JsonParser().parse(recipient);
		        JsonObject jsonobjectrecipient = jsonelementrecipient.getAsJsonObject();
		        
		        String deliveryAddress = jsonobjectrecipient.get("deliveryAddress").toString().replaceAll("^\"|\"$", "");
		        JsonElement jsonelementdeliveryAddress = new JsonParser().parse(deliveryAddress);
		        JsonObject jsonobjectdeliveryAddress = jsonelementdeliveryAddress.getAsJsonObject();
		        
		        String postcode = jsonobjectdeliveryAddress.get("postcode").toString().replaceAll("^\"|\"$", "");
		        
		        AllItemDetails = new HashMap<String, String>();
		        
		        AllItemDetails.put("itemId", ItemID);
		        AllItemDetails.put("status", status);
		        AllItemDetails.put("deliverydate", deliverydate);
		        AllItemDetails.put("postcode", postcode);

		        
		        
		        listofOrderandItems.add(AllItemDetails);
	        }*/
	        
	        
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during Fetching Order and Item Details ", "Due to :"+e, ResultPath,xwpfrun,"");
	        
			listofOrderandItems = null;
			return listofOrderandItems;
			
		}
	    
	    finally{
	        
	        response.close();
	        
	}
	    
	    return listofOrderandItems;
		
		
	}

	
	public static ArrayList<HashMap<String, String>> FetchItemDetailsbyEmailIDwithAddonItem(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue,String OrderID,String EmailID,String tcid,String ResultPath,XWPFRun xwpfrun) throws IOException {

		//String res = null;
		
		ArrayList<NameValuePair> postParameters;
		ArrayList<HashMap<String, String>> listofOrderandItems = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> AllOrderDetails = null;
		HashMap<String, String> AllItemDetails = null;
		
		CloseableHttpResponse response=null;
	    
	    CredentialsProvider credsProvider = new BasicCredentialsProvider();
		
	    credsProvider.setCredentials(
	    		new AuthScope(ProxyHostName, ProxyPort),
	    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

	    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

	  
	    
	    try {
			
	    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
	        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
	        
	        RequestConfig config = RequestConfig.custom()
	                .setProxy(proxy)
	                .build();
	        
	        HttpPost httppost = new HttpPost("/oauth/token");
	        
	        httppost.setConfig(config);        
	       // httppost.addHeader(AuthorizationKey, AuthorizationValue);
	        
	        System.out.println("Executing request " + httppost.getRequestLine() + " to " + target + " via " + proxy);
	        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httppost.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");
	       
	        //String json = "{}";       
	      //  System.out.println(json);
	        
	        postParameters = new ArrayList<NameValuePair>();
	        postParameters.add(new BasicNameValuePair("grant_type", "client_credentials"));
	        postParameters.add(new BasicNameValuePair("client_id", "morrisonsfw"));
	        postParameters.add(new BasicNameValuePair("client_secret", "7sUufcSgJ894KytQ"));
	        
	        httppost.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));
	       // httppost.setEntity(new StringEntity(json));
			
	      
	        httppost.setHeader("Accept", "application/json");
			httppost.setHeader("Content-type", "application/x-www-form-urlencoded");
			//httppost.setHeader("client_id", "morrisonsfw");
			//httppost.setHeader("client_secret", "7sUufcSgJ894KytQ");
	
			response = httpclient.execute(target, httppost);
	        System.out.println(response);
	        
	        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
	        utilityFileWriteOP.writeToLog(tcid, "Response code is ", "Displayed as: "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");

	        String responseBody = EntityUtils.toString(response.getEntity());
	        
	        System.out.println(responseBody);
	        utilityFileWriteOP.writeToLog(tcid, "Response Body is ", ": "+responseBody,ResultPath,xwpfrun,"");
	        
	        //response.toString();
			if(response.getStatusLine().getStatusCode() != 200)
				throw new MyException("Test Stopped Because of Failure. Please check Execution log");
			
			
			JsonElement jsonelement = new JsonParser().parse(responseBody);
	        JsonObject jsonobject = jsonelement.getAsJsonObject();
			
	        String access_token = jsonobject.get("access_token").toString().replaceAll("^\"|\"$", "");
	        
	        System.out.println("Bearer "+access_token);
	        HttpGet httpget = new HttpGet(UrlTail +"/orders/?email="+EmailID);
	       //{{url}}/orders/?email={{email}}
	        httpget.setConfig(config);
	        
	        httpget.addHeader(AuthorizationKey, "Bearer "+access_token);
	        
	        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
	        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");

	        response = httpclient.execute(target, httpget);
	        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
	        
	        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is ",ResultPath,xwpfrun,""+response.getStatusLine().getStatusCode());
	        
	        String jsonresponse = EntityUtils.toString(response.getEntity());
	        System.out.println(jsonresponse);
	        utilityFileWriteOP.writeToLog(tcid, "Response Body is ", " : "+jsonresponse,ResultPath,xwpfrun,"");

	        
	        jsonelement = new JsonParser().parse(jsonresponse);
	       // jsonobject = jsonelement.getAsJsonObject();
	        JsonArray jarraybody = jsonelement.getAsJsonArray();
	        
	        // Add Order Details
 for(int j=0; j<jarraybody.size(); j++) {
	        	
	        	
	        	jsonobject = jarraybody.get(j).getAsJsonObject();
	        	
	        	 // Add Order Details
		        String OrderIDfromGet = jsonobject.get("orderId").toString();
		        
		        if( !(OrderIDfromGet.equals(OrderID)) ) {
		        	continue;
		        }
		        
		        String discountCodefromGet = jsonobject.get("discountCode").toString().replaceAll("^\"|\"$", "");
		        String totalDiscountfromGet = jsonobject.get("totalDiscount").toString().replaceAll("^\"|\"$", "");
		        String totalOrderValuefromGet = jsonobject.get("totalOrderValue").toString().replaceAll("^\"|\"$", "");
		        String customerfromGet = jsonobject.get("customer").toString().replaceAll("^\"|\"$", "");
		        
		        JsonElement jsonelementcustomer = new JsonParser().parse(customerfromGet);
			    JsonObject  jsonobjectcustomer = jsonelementcustomer.getAsJsonObject();
			    
			    String emailAddressfromGet = jsonobjectcustomer.get("emailAddress").toString().replaceAll("^\"|\"$", "");
			    String fullNamefromGet = jsonobjectcustomer.get("fullName").toString().replaceAll("^\"|\"$", "");
			    String marketingOptInfromGet = jsonobjectcustomer.get("marketingOptIn").toString().replaceAll("^\"|\"$", "");

		        
		        AllOrderDetails = new HashMap<String, String>();
		        
		        
		        AllOrderDetails.put("orderId", OrderIDfromGet);
		        AllOrderDetails.put("discountCode", discountCodefromGet);
		        AllOrderDetails.put("totalDiscount", totalDiscountfromGet);
		        AllOrderDetails.put("totalOrderValue", totalOrderValuefromGet);
		        AllOrderDetails.put("emailAddress", emailAddressfromGet);
		        AllOrderDetails.put("fullName", fullNamefromGet);
		        AllOrderDetails.put("marketingOptIn", marketingOptInfromGet);
		        
		        listofOrderandItems.add(AllOrderDetails);
		        
		        
		        //Get Item Details
		        JsonArray jarray = jsonobject.getAsJsonArray("orderItems");
		        System.out.println(jarray.size());
		        for(int i=0; i<jarray.size(); i++) {
		        	
		        	jsonobject = jarray.get(i).getAsJsonObject();
			        String ItemID = jsonobject.get("itemId").toString();
			        String status = jsonobject.get("status").toString().replaceAll("^\"|\"$", "");
			        String notes = jsonobject.get("notes").toString().replaceAll("^\"|\"$", "");
			        String Itemdescription = jsonobject.get("description").toString().replaceAll("^\"|\"$", "");
			        
			        
			        String deliverydate = jsonobject.get("deliveryDate").toString().replaceAll("^\"|\"$", "");
			        deliverydate = deliverydate.substring(0, 10);
			        
			        String messageOnCard = jsonobject.get("messageOnCard").toString().replaceAll("^\"|\"$", "");
			        String deliveryContactNumber = jsonobject.get("deliveryContactNumber").toString().replaceAll("^\"|\"$", "");
			        String deliveryInstructions = jsonobject.get("deliveryInstructions").toString().replaceAll("^\"|\"$", "");

			        
			        String recipient = jsonobject.get("recipient").toString().replaceAll("^\"|\"$", "");
			        JsonElement jsonelementrecipient = new JsonParser().parse(recipient);
			        JsonObject jsonobjectrecipient = jsonelementrecipient.getAsJsonObject();
			        
			        String recipientFullname = jsonobjectrecipient.get("fullName").toString().replaceAll("^\"|\"$", "");
			        
			        String deliveryAddress = jsonobjectrecipient.get("deliveryAddress").toString().replaceAll("^\"|\"$", "");
			        JsonElement jsonelementdeliveryAddress = new JsonParser().parse(deliveryAddress);
			        JsonObject jsonobjectdeliveryAddress = jsonelementdeliveryAddress.getAsJsonObject();
			        
			        String addressline1 = jsonobjectdeliveryAddress.get("addressLine1").toString().replaceAll("^\"|\"$", "");
			        String addressline2 = jsonobjectdeliveryAddress.get("addressLine2").toString().replaceAll("^\"|\"$", "");
			        String town = jsonobjectdeliveryAddress.get("town").toString().replaceAll("^\"|\"$", "");
			        String county = jsonobjectdeliveryAddress.get("county").toString().replaceAll("^\"|\"$", "");

			        String postcode = jsonobjectdeliveryAddress.get("postcode").toString().replaceAll("^\"|\"$", "");
			        
			        AllItemDetails = new HashMap<String, String>();
			        
			        AllItemDetails.put("itemId", ItemID);
			        AllItemDetails.put("status", status);
			        AllItemDetails.put("deliverydate", deliverydate);
			        AllItemDetails.put("postcode", postcode);
			        AllItemDetails.put("notes", notes);
			        AllItemDetails.put("Itemdescription", Itemdescription);
			        AllItemDetails.put("messageOnCard", messageOnCard);
			        AllItemDetails.put("deliveryContactNumber", deliveryContactNumber);
			        AllItemDetails.put("deliveryInstructions", deliveryInstructions);
			        AllItemDetails.put("recipientFullname", recipientFullname);
			        AllItemDetails.put("addressline1", addressline1);
			        AllItemDetails.put("addressline2", addressline2);
			        AllItemDetails.put("town", town);
			        AllItemDetails.put("county", county);
			        
			        listofOrderandItems.add(AllItemDetails);
			        
			        String addonItems = jsonobject.get("addonItems").toString().replaceAll("^\"|\"$", "");
			        JsonElement jsonelementaddonItems = new JsonParser().parse(addonItems);
			        JsonArray jsonArrayaddonItems = jsonelementaddonItems.getAsJsonArray();
			        for(int m=0; m<jsonArrayaddonItems.size(); m++) {
			        	JsonObject jsonobjectaddonItems = jsonArrayaddonItems.get(m).getAsJsonObject();
			        	
			        	String description = jsonobjectaddonItems.get("description").toString().replaceAll("^\"|\"$", "");
			        	
			        	AllItemDetails.put("addonItem"+(m+1), description);
			        }
			        
			        
			        
			        listofOrderandItems.add(AllItemDetails);

		        }
		        
		        break;
		        
	        }
	        
	        
	       /*
	        //Get Item Details
	        JsonArray jarray = jsonobject.getAsJsonArray("orderItems");
	        System.out.println(jarray.size());
	        for(int i=0; i<jarray.size(); i++) {
	        	
	        	jsonobject = jarray.get(i).getAsJsonObject();
		        String ItemID = jsonobject.get("itemId").toString();
		        String status = jsonobject.get("status").toString().replaceAll("^\"|\"$", "");
		        
		        String deliverydate = jsonobject.get("deliveryDate").toString().replaceAll("^\"|\"$", "");
		        deliverydate = deliverydate.substring(0, 10);
		        
		        String recipient = jsonobject.get("recipient").toString().replaceAll("^\"|\"$", "");
		        JsonElement jsonelementrecipient = new JsonParser().parse(recipient);
		        JsonObject jsonobjectrecipient = jsonelementrecipient.getAsJsonObject();
		        
		        String deliveryAddress = jsonobjectrecipient.get("deliveryAddress").toString().replaceAll("^\"|\"$", "");
		        JsonElement jsonelementdeliveryAddress = new JsonParser().parse(deliveryAddress);
		        JsonObject jsonobjectdeliveryAddress = jsonelementdeliveryAddress.getAsJsonObject();
		        
		        String postcode = jsonobjectdeliveryAddress.get("postcode").toString().replaceAll("^\"|\"$", "");
		        
		        AllItemDetails = new HashMap<String, String>();
		        
		        AllItemDetails.put("itemId", ItemID);
		        AllItemDetails.put("status", status);
		        AllItemDetails.put("deliverydate", deliverydate);
		        AllItemDetails.put("postcode", postcode);

		        
		        
		        listofOrderandItems.add(AllItemDetails);
	        }*/

	        
	        
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during Fetching Order and Item Details ", "", ResultPath,xwpfrun,"");
	        
			listofOrderandItems = null;
			return listofOrderandItems;
			
		}
	    
	    finally{
	        
	        response.close();
	        
	}
	    
	    return listofOrderandItems;
		
		
	}
	
	public static ArrayList<HashMap<String, String>> FetchItemDetailsbyEmailID(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue,String OrderID,String EmailID,String tcid,String ResultPath,XWPFRun xwpfrun) throws IOException {

		//String res = null;
		
		ArrayList<NameValuePair> postParameters;
		ArrayList<HashMap<String, String>> listofOrderandItems = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> AllOrderDetails = null;
		HashMap<String, String> AllItemDetails = null;
		
		CloseableHttpResponse response=null;
	    
	    CredentialsProvider credsProvider = new BasicCredentialsProvider();
		
	    credsProvider.setCredentials(
	    		new AuthScope(ProxyHostName, ProxyPort),
	    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

	    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

	  
	    
	    try {
			
	    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
	        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
	        
	        RequestConfig config = RequestConfig.custom()
	                .setProxy(proxy)
	                .build();
	        
	        HttpPost httppost = new HttpPost("/oauth/token");
	        
	        httppost.setConfig(config);        
	       // httppost.addHeader(AuthorizationKey, AuthorizationValue);
	        
	        System.out.println("Executing request " + httppost.getRequestLine() + " to " + target + " via " + proxy);
	        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httppost.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");
	       
	        //String json = "{}";       
	      //  System.out.println(json);
	        
	        postParameters = new ArrayList<NameValuePair>();
	        postParameters.add(new BasicNameValuePair("grant_type", "client_credentials"));
	        postParameters.add(new BasicNameValuePair("client_id", "morrisonsfw"));
	        postParameters.add(new BasicNameValuePair("client_secret", "7sUufcSgJ894KytQ"));
	        
	        httppost.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));
	       // httppost.setEntity(new StringEntity(json));
			
	      
	        httppost.setHeader("Accept", "application/json");
			httppost.setHeader("Content-type", "application/x-www-form-urlencoded");
			//httppost.setHeader("client_id", "morrisonsfw");
			//httppost.setHeader("client_secret", "7sUufcSgJ894KytQ");
	
			response = httpclient.execute(target, httppost);
	        System.out.println(response);
	        
	        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
	        utilityFileWriteOP.writeToLog(tcid, "Response code is ", "Displayed as: "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");

	        String responseBody = EntityUtils.toString(response.getEntity());
	        
	        System.out.println(responseBody);
	        utilityFileWriteOP.writeToLog(tcid, "Response Body is ", ":  "+responseBody,ResultPath,xwpfrun,"");
	        
	        //response.toString();
			if(response.getStatusLine().getStatusCode() != 200)
				throw new MyException("Test Stopped Because of Failure. Please check Execution log");
			
			
			JsonElement jsonelement = new JsonParser().parse(responseBody);
	        JsonObject jsonobject = jsonelement.getAsJsonObject();
			
	        String access_token = jsonobject.get("access_token").toString().replaceAll("^\"|\"$", "");
	        
	        System.out.println("Bearer "+access_token);
	        HttpGet httpget = new HttpGet(UrlTail +"/orders/?email="+EmailID);
	       //{{url}}/orders/?email={{email}}
	        httpget.setConfig(config);
	        
	        httpget.addHeader(AuthorizationKey, "Bearer "+access_token);
	        
	        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
	        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");

	        response = httpclient.execute(target, httpget);
	        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
	        
	        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is ",ResultPath,xwpfrun,""+response.getStatusLine().getStatusCode());
	        
	        String jsonresponse = EntityUtils.toString(response.getEntity());
	        System.out.println(jsonresponse);
	        utilityFileWriteOP.writeToLog(tcid, "Response Body is ", ": "+jsonresponse,ResultPath,xwpfrun,"");

	        
	        jsonelement = new JsonParser().parse(jsonresponse);
	       // jsonobject = jsonelement.getAsJsonObject();
	        JsonArray jarraybody = jsonelement.getAsJsonArray();
	        
	        // Add Order Details
 for(int j=0; j<jarraybody.size(); j++) {
	        	
	        	
	        	jsonobject = jarraybody.get(j).getAsJsonObject();
	        	
	        	 // Add Order Details
		        String OrderIDfromGet = jsonobject.get("orderId").toString();
		        
		        if( !(OrderIDfromGet.equals(OrderID)) ) {
		        	continue;
		        }
		        
		        String discountCodefromGet = jsonobject.get("discountCode").toString().replaceAll("^\"|\"$", "");
		        String totalDiscountfromGet = jsonobject.get("totalDiscount").toString().replaceAll("^\"|\"$", "");
		        String totalOrderValuefromGet = jsonobject.get("totalOrderValue").toString().replaceAll("^\"|\"$", "");
		        String customerfromGet = jsonobject.get("customer").toString().replaceAll("^\"|\"$", "");
		        
		        JsonElement jsonelementcustomer = new JsonParser().parse(customerfromGet);
			    JsonObject  jsonobjectcustomer = jsonelementcustomer.getAsJsonObject();
			    
			    String emailAddressfromGet = jsonobjectcustomer.get("emailAddress").toString().replaceAll("^\"|\"$", "");
			    String fullNamefromGet = jsonobjectcustomer.get("fullName").toString().replaceAll("^\"|\"$", "");
			    String marketingOptInfromGet = jsonobjectcustomer.get("marketingOptIn").toString().replaceAll("^\"|\"$", "");

		        
		        AllOrderDetails = new HashMap<String, String>();
		        
		        
		        AllOrderDetails.put("orderId", OrderIDfromGet);
		        AllOrderDetails.put("discountCode", discountCodefromGet);
		        AllOrderDetails.put("totalDiscount", totalDiscountfromGet);
		        AllOrderDetails.put("totalOrderValue", totalOrderValuefromGet);
		        AllOrderDetails.put("emailAddress", emailAddressfromGet);
		        AllOrderDetails.put("fullName", fullNamefromGet);
		        AllOrderDetails.put("marketingOptIn", marketingOptInfromGet);
		        
		        listofOrderandItems.add(AllOrderDetails);
		        
		        
		        //Get Item Details
		        JsonArray jarray = jsonobject.getAsJsonArray("orderItems");
		        System.out.println(jarray.size());
		        for(int i=0; i<jarray.size(); i++) {
		        	
		        	jsonobject = jarray.get(i).getAsJsonObject();
			        String ItemID = jsonobject.get("itemId").toString();
			        String status = jsonobject.get("status").toString().replaceAll("^\"|\"$", "");
			        String notes = jsonobject.get("notes").toString().replaceAll("^\"|\"$", "");
			        String Itemdescription = jsonobject.get("description").toString().replaceAll("^\"|\"$", "");
			        
			        
			        String deliverydate = jsonobject.get("deliveryDate").toString().replaceAll("^\"|\"$", "");
			        deliverydate = deliverydate.substring(0, 10);
			        
			        String messageOnCard = jsonobject.get("messageOnCard").toString().replaceAll("^\"|\"$", "");
			        String deliveryContactNumber = jsonobject.get("deliveryContactNumber").toString().replaceAll("^\"|\"$", "");
			        String deliveryInstructions = jsonobject.get("deliveryInstructions").toString().replaceAll("^\"|\"$", "");

			        
			        String recipient = jsonobject.get("recipient").toString().replaceAll("^\"|\"$", "");
			        JsonElement jsonelementrecipient = new JsonParser().parse(recipient);
			        JsonObject jsonobjectrecipient = jsonelementrecipient.getAsJsonObject();
			        
			        String recipientFullname = jsonobjectrecipient.get("fullName").toString().replaceAll("^\"|\"$", "");
			        
			        String deliveryAddress = jsonobjectrecipient.get("deliveryAddress").toString().replaceAll("^\"|\"$", "");
			        JsonElement jsonelementdeliveryAddress = new JsonParser().parse(deliveryAddress);
			        JsonObject jsonobjectdeliveryAddress = jsonelementdeliveryAddress.getAsJsonObject();
			        
			        String addressline1 = jsonobjectdeliveryAddress.get("addressLine1").toString().replaceAll("^\"|\"$", "");
			        String addressline2 = jsonobjectdeliveryAddress.get("addressLine2").toString().replaceAll("^\"|\"$", "");
			        String town = jsonobjectdeliveryAddress.get("town").toString().replaceAll("^\"|\"$", "");
			        String county = jsonobjectdeliveryAddress.get("county").toString().replaceAll("^\"|\"$", "");

			        String postcode = jsonobjectdeliveryAddress.get("postcode").toString().replaceAll("^\"|\"$", "");
			        
			        AllItemDetails = new HashMap<String, String>();
			        
			        AllItemDetails.put("itemId", ItemID);
			        AllItemDetails.put("status", status);
			        AllItemDetails.put("deliverydate", deliverydate);
			        AllItemDetails.put("postcode", postcode);
			        AllItemDetails.put("notes", notes);
			        AllItemDetails.put("Itemdescription", Itemdescription);
			        AllItemDetails.put("messageOnCard", messageOnCard);
			        AllItemDetails.put("deliveryContactNumber", deliveryContactNumber);
			        AllItemDetails.put("deliveryInstructions", deliveryInstructions);
			        AllItemDetails.put("recipientFullname", recipientFullname);
			        AllItemDetails.put("addressline1", addressline1);
			        AllItemDetails.put("addressline2", addressline2);
			        AllItemDetails.put("town", town);
			        AllItemDetails.put("county", county);
			        
			        listofOrderandItems.add(AllItemDetails);
			        
			       /* String addonItems = jsonobject.get("addonItems").toString().replaceAll("^\"|\"$", "");
			        JsonElement jsonelementaddonItems = new JsonParser().parse(addonItems);
			        JsonArray jsonArrayaddonItems = jsonelementaddonItems.getAsJsonArray();
			        for(int m=0; m<jsonArrayaddonItems.size(); m++) {
			        	JsonObject jsonobjectaddonItems = jsonArrayaddonItems.get(m).getAsJsonObject();
			        	
			        	String description = jsonobjectaddonItems.get("description").toString().replaceAll("^\"|\"$", "");
			        	
			        	AllItemDetails.put("addonItem"+(m+1), description);
			        }
			        
			        
			        
			        listofOrderandItems.add(AllItemDetails);*/

		        }
		        
		        break;
		        
	        }
	        
	        
	       /*
	        //Get Item Details
	        JsonArray jarray = jsonobject.getAsJsonArray("orderItems");
	        System.out.println(jarray.size());
	        for(int i=0; i<jarray.size(); i++) {
	        	
	        	jsonobject = jarray.get(i).getAsJsonObject();
		        String ItemID = jsonobject.get("itemId").toString();
		        String status = jsonobject.get("status").toString().replaceAll("^\"|\"$", "");
		        
		        String deliverydate = jsonobject.get("deliveryDate").toString().replaceAll("^\"|\"$", "");
		        deliverydate = deliverydate.substring(0, 10);
		        
		        String recipient = jsonobject.get("recipient").toString().replaceAll("^\"|\"$", "");
		        JsonElement jsonelementrecipient = new JsonParser().parse(recipient);
		        JsonObject jsonobjectrecipient = jsonelementrecipient.getAsJsonObject();
		        
		        String deliveryAddress = jsonobjectrecipient.get("deliveryAddress").toString().replaceAll("^\"|\"$", "");
		        JsonElement jsonelementdeliveryAddress = new JsonParser().parse(deliveryAddress);
		        JsonObject jsonobjectdeliveryAddress = jsonelementdeliveryAddress.getAsJsonObject();
		        
		        String postcode = jsonobjectdeliveryAddress.get("postcode").toString().replaceAll("^\"|\"$", "");
		        
		        AllItemDetails = new HashMap<String, String>();
		        
		        AllItemDetails.put("itemId", ItemID);
		        AllItemDetails.put("status", status);
		        AllItemDetails.put("deliverydate", deliverydate);
		        AllItemDetails.put("postcode", postcode);

		        
		        
		        listofOrderandItems.add(AllItemDetails);
	        }*/

	        
	        
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during Fetching Order and Item Details ", "", ResultPath,xwpfrun,"");
	        
			listofOrderandItems = null;
			return listofOrderandItems;
			
		}
	    
	    finally{
	        
	        response.close();
	        
	}
	    
	    return listofOrderandItems;
		
		
	}
	
	
	public static ArrayList<HashMap<String, String>> FetchItemDetailsbyPostCode(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue,String OrderID,String PostCode,String tcid,String ResultPath,XWPFRun xwpfrun) throws IOException {
		
		//String res = null;
		
		ArrayList<NameValuePair> postParameters;
		ArrayList<HashMap<String, String>> listofOrderandItems = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> AllOrderDetails = null;
		HashMap<String, String> AllItemDetails = null;
		
		CloseableHttpResponse response=null;
	    
	    CredentialsProvider credsProvider = new BasicCredentialsProvider();
		
	    credsProvider.setCredentials(
	    		new AuthScope(ProxyHostName, ProxyPort),
	    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

	    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

	  
	    
	    try {
			
	    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
	        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
	        
	        RequestConfig config = RequestConfig.custom()
	                .setProxy(proxy)
	                .build();
	        
	        HttpPost httppost = new HttpPost("/oauth/token");
	        
	        httppost.setConfig(config);        
	       // httppost.addHeader(AuthorizationKey, AuthorizationValue);
	        
	        System.out.println("Executing request " + httppost.getRequestLine() + " to " + target + " via " + proxy);
	        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httppost.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");
	       
	        //String json = "{}";       
	      //  System.out.println(json);
	        
	        postParameters = new ArrayList<NameValuePair>();
	        postParameters.add(new BasicNameValuePair("grant_type", "client_credentials"));
	        postParameters.add(new BasicNameValuePair("client_id", "morrisonsfw"));
	        postParameters.add(new BasicNameValuePair("client_secret", "7sUufcSgJ894KytQ"));
	        
	        httppost.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));
	       // httppost.setEntity(new StringEntity(json));
			
	      
	        httppost.setHeader("Accept", "application/json");
			httppost.setHeader("Content-type", "application/x-www-form-urlencoded");
			//httppost.setHeader("client_id", "morrisonsfw");
			//httppost.setHeader("client_secret", "7sUufcSgJ894KytQ");
	
			response = httpclient.execute(target, httppost);
	        System.out.println(response);
	        
	        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
	        utilityFileWriteOP.writeToLog(tcid, "Response code is ", "Displayed as: "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");

	        String responseBody = EntityUtils.toString(response.getEntity());
	        
	        System.out.println(responseBody);
	        utilityFileWriteOP.writeToLog(tcid, "Response Body is ", "Body :- "+responseBody,ResultPath,xwpfrun,"");
	        
	        //response.toString();
			if(response.getStatusLine().getStatusCode() != 200)
				throw new MyException("Test Stopped Because of Failure. Please check Execution log");
			
			
			JsonElement jsonelement = new JsonParser().parse(responseBody);
	        JsonObject jsonobject = jsonelement.getAsJsonObject();
			
	        String access_token = jsonobject.get("access_token").toString().replaceAll("^\"|\"$", "");
	        
	        System.out.println("Bearer "+access_token);
	      int size=PostCode.length();
	      System.out.println(size);
	      String restUrl = URLEncoder.encode(PostCode, "UTF-8");
	        HttpGet httpget = new HttpGet(UrlTail +"/orders?postcode="+restUrl);
	        
			     //   HttpGet httpget = new HttpGet(UrlTail +"/orders?postcode=RG30 2EZ");

	        //HttpGet httpget = new HttpGet(UrlTail +"/orders/?email="+EmailID);
	        //{{url}}/orders?postcode={{postcode}}
	       //{{url}}/orders/?email={{email}}
	        httpget.setConfig(config);
	        
	        httpget.addHeader(AuthorizationKey, "Bearer "+access_token);
	        
	        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
	        response = httpclient.execute(target, httpget);
	        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
	        
	        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is ",ResultPath,xwpfrun,""+response.getStatusLine().getStatusCode());
	        
	        String jsonresponse = EntityUtils.toString(response.getEntity());
	        utilityFileWriteOP.writeToLog(tcid, "Response Body is ", "Body :- "+jsonresponse,ResultPath,xwpfrun,"");

	        System.out.println(jsonresponse);
	        
	        jsonelement = new JsonParser().parse(jsonresponse);
	       // jsonobject = jsonelement.getAsJsonObject();
	        JsonArray jarraybody = jsonelement.getAsJsonArray();
	        
	        // Add Order Details
 for(int j=0; j<jarraybody.size(); j++) {
	        	
	        	
	        	jsonobject = jarraybody.get(j).getAsJsonObject();
	        	
	        	 // Add Order Details
		        String OrderIDfromGet = jsonobject.get("orderId").toString();
		        
		        if( !(OrderIDfromGet.equals(OrderID)) ) {
		        	continue;
		        }
		        
		        String discountCodefromGet = jsonobject.get("discountCode").toString().replaceAll("^\"|\"$", "");
		        String totalDiscountfromGet = jsonobject.get("totalDiscount").toString().replaceAll("^\"|\"$", "");
		        String totalOrderValuefromGet = jsonobject.get("totalOrderValue").toString().replaceAll("^\"|\"$", "");
		        String customerfromGet = jsonobject.get("customer").toString().replaceAll("^\"|\"$", "");
		        
		        JsonElement jsonelementcustomer = new JsonParser().parse(customerfromGet);
			    JsonObject  jsonobjectcustomer = jsonelementcustomer.getAsJsonObject();
			    
			    String emailAddressfromGet = jsonobjectcustomer.get("emailAddress").toString().replaceAll("^\"|\"$", "");
			    String fullNamefromGet = jsonobjectcustomer.get("fullName").toString().replaceAll("^\"|\"$", "");
			    String marketingOptInfromGet = jsonobjectcustomer.get("marketingOptIn").toString().replaceAll("^\"|\"$", "");

		        
		        AllOrderDetails = new HashMap<String, String>();
		        
		        
		        AllOrderDetails.put("orderId", OrderIDfromGet);
		        AllOrderDetails.put("discountCode", discountCodefromGet);
		        AllOrderDetails.put("totalDiscount", totalDiscountfromGet);
		        AllOrderDetails.put("totalOrderValue", totalOrderValuefromGet);
		        AllOrderDetails.put("emailAddress", emailAddressfromGet);
		        AllOrderDetails.put("fullName", fullNamefromGet);
		        AllOrderDetails.put("marketingOptIn", marketingOptInfromGet);
		        
		        listofOrderandItems.add(AllOrderDetails);
		        
		        
		        //Get Item Details
		        JsonArray jarray = jsonobject.getAsJsonArray("orderItems");
		        System.out.println(jarray.size());
		        for(int i=0; i<jarray.size(); i++) {
		        	
		        	jsonobject = jarray.get(i).getAsJsonObject();
			        String ItemID = jsonobject.get("itemId").toString();
			        String status = jsonobject.get("status").toString().replaceAll("^\"|\"$", "");
			        String notes = jsonobject.get("notes").toString().replaceAll("^\"|\"$", "");
			        String Itemdescription = jsonobject.get("description").toString().replaceAll("^\"|\"$", "");

			        
			        String deliverydate = jsonobject.get("deliveryDate").toString().replaceAll("^\"|\"$", "");
			        deliverydate = deliverydate.substring(0, 10);
			        
			        String messageOnCard = jsonobject.get("messageOnCard").toString().replaceAll("^\"|\"$", "");
			        String deliveryContactNumber = jsonobject.get("deliveryContactNumber").toString().replaceAll("^\"|\"$", "");
			        String deliveryInstructions = jsonobject.get("deliveryInstructions").toString().replaceAll("^\"|\"$", "");

			        
			        String recipient = jsonobject.get("recipient").toString().replaceAll("^\"|\"$", "");
			        JsonElement jsonelementrecipient = new JsonParser().parse(recipient);
			        JsonObject jsonobjectrecipient = jsonelementrecipient.getAsJsonObject();
			        
			        String recipientFullname = jsonobjectrecipient.get("fullName").toString().replaceAll("^\"|\"$", "");

			        
			        String deliveryAddress = jsonobjectrecipient.get("deliveryAddress").toString().replaceAll("^\"|\"$", "");
			        JsonElement jsonelementdeliveryAddress = new JsonParser().parse(deliveryAddress);
			        JsonObject jsonobjectdeliveryAddress = jsonelementdeliveryAddress.getAsJsonObject();
			        
			        String addressline1 = jsonobjectdeliveryAddress.get("addressLine1").toString().replaceAll("^\"|\"$", "");
			        String addressline2 = jsonobjectdeliveryAddress.get("addressLine2").toString().replaceAll("^\"|\"$", "");
			        String town = jsonobjectdeliveryAddress.get("town").toString().replaceAll("^\"|\"$", "");
			        String county = jsonobjectdeliveryAddress.get("county").toString().replaceAll("^\"|\"$", "");

			        
			        String postcode = jsonobjectdeliveryAddress.get("postcode").toString().replaceAll("^\"|\"$", "");
			        
			        AllItemDetails = new HashMap<String, String>();
			        
			        AllItemDetails.put("itemId", ItemID);
			        AllItemDetails.put("status", status);
			        AllItemDetails.put("deliverydate", deliverydate);
			        AllItemDetails.put("postcode", postcode);
			        AllItemDetails.put("notes", notes);
			        AllItemDetails.put("Itemdescription", Itemdescription);
			        AllItemDetails.put("messageOnCard", messageOnCard);
			        AllItemDetails.put("deliveryContactNumber", deliveryContactNumber);
			        AllItemDetails.put("deliveryInstructions", deliveryInstructions);
			        AllItemDetails.put("recipientFullname", recipientFullname);
			        AllItemDetails.put("addressline1", addressline1);
			        AllItemDetails.put("addressline2", addressline2);
			        AllItemDetails.put("town", town);
			        AllItemDetails.put("county", county);
			        
			        
			        listofOrderandItems.add(AllItemDetails);
			        
			      /*  String addonItems = jsonobject.get("addonItems").toString().replaceAll("^\"|\"$", "");
			        JsonElement jsonelementaddonItems = new JsonParser().parse(addonItems);
			        JsonArray jsonArrayaddonItems = jsonelementaddonItems.getAsJsonArray();
			        for(int m=0; m<jsonArrayaddonItems.size(); m++) {
			        	JsonObject jsonobjectaddonItems = jsonArrayaddonItems.get(m).getAsJsonObject();
			        	
			        	String description = jsonobjectaddonItems.get("description").toString().replaceAll("^\"|\"$", "");
			        	
			        	AllItemDetails.put("addonItem"+(m+1), description);
			        }
			        
			        
			        
			        listofOrderandItems.add(AllItemDetails);*/

		        }
		        
		        break;
		        
	        }
	        
	        
	       /*
	        //Get Item Details
	        JsonArray jarray = jsonobject.getAsJsonArray("orderItems");
	        System.out.println(jarray.size());
	        for(int i=0; i<jarray.size(); i++) {
	        	
	        	jsonobject = jarray.get(i).getAsJsonObject();
		        String ItemID = jsonobject.get("itemId").toString();
		        String status = jsonobject.get("status").toString().replaceAll("^\"|\"$", "");
		        
		        String deliverydate = jsonobject.get("deliveryDate").toString().replaceAll("^\"|\"$", "");
		        deliverydate = deliverydate.substring(0, 10);
		        
		        String recipient = jsonobject.get("recipient").toString().replaceAll("^\"|\"$", "");
		        JsonElement jsonelementrecipient = new JsonParser().parse(recipient);
		        JsonObject jsonobjectrecipient = jsonelementrecipient.getAsJsonObject();
		        
		        String deliveryAddress = jsonobjectrecipient.get("deliveryAddress").toString().replaceAll("^\"|\"$", "");
		        JsonElement jsonelementdeliveryAddress = new JsonParser().parse(deliveryAddress);
		        JsonObject jsonobjectdeliveryAddress = jsonelementdeliveryAddress.getAsJsonObject();
		        
		        String postcode = jsonobjectdeliveryAddress.get("postcode").toString().replaceAll("^\"|\"$", "");
		        
		        AllItemDetails = new HashMap<String, String>();
		        
		        AllItemDetails.put("itemId", ItemID);
		        AllItemDetails.put("status", status);
		        AllItemDetails.put("deliverydate", deliverydate);
		        AllItemDetails.put("postcode", postcode);

		        
		        
		        listofOrderandItems.add(AllItemDetails);
	        }*/

	        
	        
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during Fetching Order and Item Details ", "Due to :"+e, ResultPath,xwpfrun,"");
	        
			listofOrderandItems = null;
			return listofOrderandItems;
			
		}
	    
	    finally{
	        
	        response.close();
	        
	}
	    
	    return listofOrderandItems;
		
		
	}
	
public static ArrayList<HashMap<String, String>> FetchItemDetailsbyPostCodewithAddonItem(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue,String OrderID,String PostCode,String tcid,String ResultPath,XWPFRun xwpfrun) throws IOException {
		
		//String res = null;
		
		ArrayList<NameValuePair> postParameters;
		ArrayList<HashMap<String, String>> listofOrderandItems = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> AllOrderDetails = null;
		HashMap<String, String> AllItemDetails = null;
		
		CloseableHttpResponse response=null;
	    
	    CredentialsProvider credsProvider = new BasicCredentialsProvider();
		
	    credsProvider.setCredentials(
	    		new AuthScope(ProxyHostName, ProxyPort),
	    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

	    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

	  
	    
	    try {
			
	    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
	        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
	        
	        RequestConfig config = RequestConfig.custom()
	                .setProxy(proxy)
	                .build();
	        
	        HttpPost httppost = new HttpPost("/oauth/token");
	        
	        httppost.setConfig(config);        
	       // httppost.addHeader(AuthorizationKey, AuthorizationValue);
	        
	        System.out.println("Executing request " + httppost.getRequestLine() + " to " + target + " via " + proxy);
	        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httppost.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");
	       
	        //String json = "{}";       
	      //  System.out.println(json);
	        
	        postParameters = new ArrayList<NameValuePair>();
	        postParameters.add(new BasicNameValuePair("grant_type", "client_credentials"));
	        postParameters.add(new BasicNameValuePair("client_id", "morrisonsfw"));
	        postParameters.add(new BasicNameValuePair("client_secret", "7sUufcSgJ894KytQ"));
	        
	        httppost.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));
	       // httppost.setEntity(new StringEntity(json));
			
	      
	        httppost.setHeader("Accept", "application/json");
			httppost.setHeader("Content-type", "application/x-www-form-urlencoded");
			//httppost.setHeader("client_id", "morrisonsfw");
			//httppost.setHeader("client_secret", "7sUufcSgJ894KytQ");
	
			response = httpclient.execute(target, httppost);
	        System.out.println(response);
	        
	        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
	        utilityFileWriteOP.writeToLog(tcid, "Response code is ", "Displayed as: "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");

	        String responseBody = EntityUtils.toString(response.getEntity());
	        
	        System.out.println(responseBody);
	        utilityFileWriteOP.writeToLog(tcid, "Response Body is ", "Body :- "+responseBody,ResultPath,xwpfrun,"");
	        
	        //response.toString();
			if(response.getStatusLine().getStatusCode() != 200)
				throw new MyException("Test Stopped Because of Failure. Please check Execution log");
			
			
			JsonElement jsonelement = new JsonParser().parse(responseBody);
	        JsonObject jsonobject = jsonelement.getAsJsonObject();
			
	        String access_token = jsonobject.get("access_token").toString().replaceAll("^\"|\"$", "");
	        
	        System.out.println("Bearer "+access_token);
	      int size=PostCode.length();
	      System.out.println(size);
	      String restUrl = URLEncoder.encode(PostCode, "UTF-8");
	        HttpGet httpget = new HttpGet(UrlTail +"/orders?postcode="+restUrl);
	        
			     //   HttpGet httpget = new HttpGet(UrlTail +"/orders?postcode=RG30 2EZ");

	        //HttpGet httpget = new HttpGet(UrlTail +"/orders/?email="+EmailID);
	        //{{url}}/orders?postcode={{postcode}}
	       //{{url}}/orders/?email={{email}}
	        httpget.setConfig(config);
	        
	        httpget.addHeader(AuthorizationKey, "Bearer "+access_token);
	        
	        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
	        response = httpclient.execute(target, httpget);
	        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
	        
	        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is ",ResultPath,xwpfrun,""+response.getStatusLine().getStatusCode());
	        
	        String jsonresponse = EntityUtils.toString(response.getEntity());
	        utilityFileWriteOP.writeToLog(tcid, "Response Body is ", "Body :- "+jsonresponse,ResultPath,xwpfrun,"");

	        System.out.println(jsonresponse);
	        
	        jsonelement = new JsonParser().parse(jsonresponse);
	       // jsonobject = jsonelement.getAsJsonObject();
	        JsonArray jarraybody = jsonelement.getAsJsonArray();
	        
	        // Add Order Details
 for(int j=0; j<jarraybody.size(); j++) {
	        	
	        	
	        	jsonobject = jarraybody.get(j).getAsJsonObject();
	        	
	        	 // Add Order Details
		        String OrderIDfromGet = jsonobject.get("orderId").toString();
		        
		        if( !(OrderIDfromGet.equals(OrderID)) ) {
		        	continue;
		        }
		        
		        String discountCodefromGet = jsonobject.get("discountCode").toString().replaceAll("^\"|\"$", "");
		        String totalDiscountfromGet = jsonobject.get("totalDiscount").toString().replaceAll("^\"|\"$", "");
		        String totalOrderValuefromGet = jsonobject.get("totalOrderValue").toString().replaceAll("^\"|\"$", "");
		        String customerfromGet = jsonobject.get("customer").toString().replaceAll("^\"|\"$", "");
		        
		        JsonElement jsonelementcustomer = new JsonParser().parse(customerfromGet);
			    JsonObject  jsonobjectcustomer = jsonelementcustomer.getAsJsonObject();
			    
			    String emailAddressfromGet = jsonobjectcustomer.get("emailAddress").toString().replaceAll("^\"|\"$", "");
			    String fullNamefromGet = jsonobjectcustomer.get("fullName").toString().replaceAll("^\"|\"$", "");
			    String marketingOptInfromGet = jsonobjectcustomer.get("marketingOptIn").toString().replaceAll("^\"|\"$", "");

		        
		        AllOrderDetails = new HashMap<String, String>();
		        
		        
		        AllOrderDetails.put("orderId", OrderIDfromGet);
		        AllOrderDetails.put("discountCode", discountCodefromGet);
		        AllOrderDetails.put("totalDiscount", totalDiscountfromGet);
		        AllOrderDetails.put("totalOrderValue", totalOrderValuefromGet);
		        AllOrderDetails.put("emailAddress", emailAddressfromGet);
		        AllOrderDetails.put("fullName", fullNamefromGet);
		        AllOrderDetails.put("marketingOptIn", marketingOptInfromGet);
		        
		        listofOrderandItems.add(AllOrderDetails);
		        
		        
		        //Get Item Details
		        JsonArray jarray = jsonobject.getAsJsonArray("orderItems");
		        System.out.println(jarray.size());
		        for(int i=0; i<jarray.size(); i++) {
		        	
		        	jsonobject = jarray.get(i).getAsJsonObject();
			        String ItemID = jsonobject.get("itemId").toString();
			        String status = jsonobject.get("status").toString().replaceAll("^\"|\"$", "");
			        String notes = jsonobject.get("notes").toString().replaceAll("^\"|\"$", "");
			        String Itemdescription = jsonobject.get("description").toString().replaceAll("^\"|\"$", "");

			        
			        String deliverydate = jsonobject.get("deliveryDate").toString().replaceAll("^\"|\"$", "");
			        deliverydate = deliverydate.substring(0, 10);
			        
			        String messageOnCard = jsonobject.get("messageOnCard").toString().replaceAll("^\"|\"$", "");
			        String deliveryContactNumber = jsonobject.get("deliveryContactNumber").toString().replaceAll("^\"|\"$", "");
			        String deliveryInstructions = jsonobject.get("deliveryInstructions").toString().replaceAll("^\"|\"$", "");

			        
			        String recipient = jsonobject.get("recipient").toString().replaceAll("^\"|\"$", "");
			        JsonElement jsonelementrecipient = new JsonParser().parse(recipient);
			        JsonObject jsonobjectrecipient = jsonelementrecipient.getAsJsonObject();
			        
			        String recipientFullname = jsonobjectrecipient.get("fullName").toString().replaceAll("^\"|\"$", "");

			        
			        String deliveryAddress = jsonobjectrecipient.get("deliveryAddress").toString().replaceAll("^\"|\"$", "");
			        JsonElement jsonelementdeliveryAddress = new JsonParser().parse(deliveryAddress);
			        JsonObject jsonobjectdeliveryAddress = jsonelementdeliveryAddress.getAsJsonObject();
			        
			        String addressline1 = jsonobjectdeliveryAddress.get("addressLine1").toString().replaceAll("^\"|\"$", "");
			        String addressline2 = jsonobjectdeliveryAddress.get("addressLine2").toString().replaceAll("^\"|\"$", "");
			        String town = jsonobjectdeliveryAddress.get("town").toString().replaceAll("^\"|\"$", "");
			        String county = jsonobjectdeliveryAddress.get("county").toString().replaceAll("^\"|\"$", "");

			        
			        String postcode = jsonobjectdeliveryAddress.get("postcode").toString().replaceAll("^\"|\"$", "");
			        
			        AllItemDetails = new HashMap<String, String>();
			        
			        AllItemDetails.put("itemId", ItemID);
			        AllItemDetails.put("status", status);
			        AllItemDetails.put("deliverydate", deliverydate);
			        AllItemDetails.put("postcode", postcode);
			        AllItemDetails.put("notes", notes);
			        AllItemDetails.put("Itemdescription", Itemdescription);
			        AllItemDetails.put("messageOnCard", messageOnCard);
			        AllItemDetails.put("deliveryContactNumber", deliveryContactNumber);
			        AllItemDetails.put("deliveryInstructions", deliveryInstructions);
			        AllItemDetails.put("recipientFullname", recipientFullname);
			        AllItemDetails.put("addressline1", addressline1);
			        AllItemDetails.put("addressline2", addressline2);
			        AllItemDetails.put("town", town);
			        AllItemDetails.put("county", county);
			        
			        
			        listofOrderandItems.add(AllItemDetails);
			        
			        String addonItems = jsonobject.get("addonItems").toString().replaceAll("^\"|\"$", "");
			        JsonElement jsonelementaddonItems = new JsonParser().parse(addonItems);
			        JsonArray jsonArrayaddonItems = jsonelementaddonItems.getAsJsonArray();
			        for(int m=0; m<jsonArrayaddonItems.size(); m++) {
			        	JsonObject jsonobjectaddonItems = jsonArrayaddonItems.get(m).getAsJsonObject();
			        	
			        	String description = jsonobjectaddonItems.get("description").toString().replaceAll("^\"|\"$", "");
			        	
			        	AllItemDetails.put("addonItem"+(m+1), description);
			        }
			        
			        
			        
			        listofOrderandItems.add(AllItemDetails);

		        }
		        
		        break;
		        
	        }
	        
	        
	       /*
	        //Get Item Details
	        JsonArray jarray = jsonobject.getAsJsonArray("orderItems");
	        System.out.println(jarray.size());
	        for(int i=0; i<jarray.size(); i++) {
	        	
	        	jsonobject = jarray.get(i).getAsJsonObject();
		        String ItemID = jsonobject.get("itemId").toString();
		        String status = jsonobject.get("status").toString().replaceAll("^\"|\"$", "");
		        
		        String deliverydate = jsonobject.get("deliveryDate").toString().replaceAll("^\"|\"$", "");
		        deliverydate = deliverydate.substring(0, 10);
		        
		        String recipient = jsonobject.get("recipient").toString().replaceAll("^\"|\"$", "");
		        JsonElement jsonelementrecipient = new JsonParser().parse(recipient);
		        JsonObject jsonobjectrecipient = jsonelementrecipient.getAsJsonObject();
		        
		        String deliveryAddress = jsonobjectrecipient.get("deliveryAddress").toString().replaceAll("^\"|\"$", "");
		        JsonElement jsonelementdeliveryAddress = new JsonParser().parse(deliveryAddress);
		        JsonObject jsonobjectdeliveryAddress = jsonelementdeliveryAddress.getAsJsonObject();
		        
		        String postcode = jsonobjectdeliveryAddress.get("postcode").toString().replaceAll("^\"|\"$", "");
		        
		        AllItemDetails = new HashMap<String, String>();
		        
		        AllItemDetails.put("itemId", ItemID);
		        AllItemDetails.put("status", status);
		        AllItemDetails.put("deliverydate", deliverydate);
		        AllItemDetails.put("postcode", postcode);

		        
		        
		        listofOrderandItems.add(AllItemDetails);
	        }*/

	        
	        
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during Fetching Order and Item Details ", "Due to :"+e, ResultPath,xwpfrun,"");
	        
			listofOrderandItems = null;
			return listofOrderandItems;
			
		}
	    
	    finally{
	        
	        response.close();
	        
	}
	    
	    return listofOrderandItems;
		
		
	}
	
	
	public static String PostPreAdviceDetails(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue,String ContentKey, String ContentValue, HashMap<String, String> CreateOrderDetails,String retrievedField, String tcid,String ResultPath,XWPFRun xwpfrun, ExtentTest logger) throws IOException {

		String res = null;
		String ItemDetailsString = "";
		CloseableHttpResponse response=null;
	    CredentialsProvider credsProvider = new BasicCredentialsProvider();
		credsProvider.setCredentials(
	    		new AuthScope(ProxyHostName, ProxyPort),
	    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials
		CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();
		try {
			HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
	        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
	        RequestConfig config = RequestConfig.custom()
	                .setProxy(proxy)
	                .build();
	        HttpPost httppost = new HttpPost(UrlTail + "?" + ApiKey);
	        httppost.setConfig(config);        
	        httppost.addHeader(AuthorizationKey, AuthorizationValue);
	        httppost.addHeader(ContentKey, ContentValue);
	        System.out.println("Executing request " + httppost.getRequestLine() + " to " + target + " via " + proxy);
	        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httppost.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");
	        logger.log(LogStatus.PASS, "Executing request " + httppost.getRequestLine() + " to " + target + " via " + proxy);
	        String json = "{\"filename\": \""+CreateOrderDetails.get("filename")+"\",\"resource\": \""+CreateOrderDetails.get("resource")+"\",\"action\": \""+CreateOrderDetails.get("action")+"\"}";
        	System.out.println(json);
        	utilityFileWriteOP.writeToLog(tcid, "Sending json body ", " "+json,ResultPath,xwpfrun,"");
        	logger.log(LogStatus.PASS, "Sending json body ", " "+json);
        	httppost.setEntity(new StringEntity(json));
			httppost.setHeader("Content-type", "application/json");
			response = httpclient.execute(target, httppost);
	        System.out.println(response);
	        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
	        logger.log(LogStatus.PASS, "Response code is "+response.getStatusLine().getStatusCode());
	        utilityFileWriteOP.writeToLog(tcid, "Response code is ", "Displayed as: "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");
	        String responseBody = EntityUtils.toString(response.getEntity());
	        System.out.println(responseBody);
	        utilityFileWriteOP.writeToLog(tcid, "Response Body is ", " "+responseBody,ResultPath,xwpfrun,"");
	        
	        //response.toString();
			if(response.getStatusLine().getStatusCode() != 202) {
				//Reporting_Utilities.writeStepToHTMLLog(tcid,String.valueOf(++StepCount), "New Customer Registration using Identity API call", "FAIL", "FAIL", ResultPath);
		        utilityFileWriteOP.writeToLog(tcid, "Create Order Details Sending Failed", ""+responseBody,ResultPath,xwpfrun,"");
		        logger.log(LogStatus.FAIL, "Create Order Details Sending Failed "+responseBody);
		        res = null;
				throw new MyException("Test Stopped Because of Failure. Please check Execution log");
		    }else{
				
		        utilityFileWriteOP.writeToLog(tcid, "Response Body is ", " : "+responseBody,ResultPath,xwpfrun,"");
//		        logger.log(LogStatus.PASS, "Response Body is : "+responseBody);
		        JsonElement jsonelement = new JsonParser().parse(responseBody);
		        JsonObject jsonobject = jsonelement.getAsJsonObject();
		        res = jsonobject.get(retrievedField).toString().replace("\"", "");;
			}
			
	
	        
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during Stock Movement API Call ", "", ResultPath,xwpfrun,"");
			
	
			
			res = null;
			return res;
			
		}
	    
	    finally{
	        
	        response.close();
	        
	}
	    
	    return res;
		
		
	}
	
	
	public static String PostOrderRequest(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue,String ContentKey, String ContentValue, HashMap<String, String> CreateOrderDetails,String retrievedField, String tcid,String ResultPath,XWPFRun xwpfrun, ExtentTest logger) throws IOException {

		String res = null;
		String ItemDetailsString = "";
		CloseableHttpResponse response=null;
	    CredentialsProvider credsProvider = new BasicCredentialsProvider();
		credsProvider.setCredentials(
	    		new AuthScope(ProxyHostName, ProxyPort),
	    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials
		CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();
		try {
			HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
	        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
	        RequestConfig config = RequestConfig.custom()
	                .setProxy(proxy)
	                .build();
	        HttpPost httppost = new HttpPost(UrlTail + "?" + ApiKey);
	        httppost.setConfig(config);        
	        httppost.addHeader(AuthorizationKey, AuthorizationValue);
	        httppost.addHeader(ContentKey, ContentValue);
	        System.out.println("Executing request " + httppost.getRequestLine() + " to " + target + " via " + proxy);
	        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httppost.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");
	        logger.log(LogStatus.PASS, "Executing request " + httppost.getRequestLine() + " to " + target + " via " + proxy);
	        String json = "{\"customerId\":\""+CreateOrderDetails.get("customerId")+"\",\"orderInfo\":{\"orderAlternateId\":\""+CreateOrderDetails.get("orderAlternateId")+"\",\"orderId\":\""+CreateOrderDetails.get("orderId")+"\"},\"supplierInfo\":{\"supplierId\":\""+CreateOrderDetails.get("supplierId")+"\"},\"shippingInfo\":{\"shipTo\":{\"locationId\":\""+CreateOrderDetails.get("locationId")+"\",\"deliveryDate\":\""+CreateOrderDetails.get("deliveryDate")+"\"}},\"orderLines\":[{\"itemId\":\""+CreateOrderDetails.get("itemId")+"\",\"packId\":\""+CreateOrderDetails.get("packId")+"\",\"packSize\":"+CreateOrderDetails.get("packSize")+",\"orderedQuantity\":"+CreateOrderDetails.get("orderedQuantity")+",\"quantityType\":\""+CreateOrderDetails.get("quantityType")+"\",\"lineId\":0}]}";
        	System.out.println(json);
        	utilityFileWriteOP.writeToLog(tcid, "Sending json body ", " "+json,ResultPath,xwpfrun,"");
        	logger.log(LogStatus.PASS, "Sending json body ", " "+json);
        	httppost.setEntity(new StringEntity(json));
			httppost.setHeader("Content-type", "application/json");
			response = httpclient.execute(target, httppost);
	        System.out.println(response);
	        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
	        logger.log(LogStatus.PASS, "Response code is "+response.getStatusLine().getStatusCode());
	        utilityFileWriteOP.writeToLog(tcid, "Response code is ", "Displayed as: "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");
	        String responseBody = EntityUtils.toString(response.getEntity());
	        System.out.println(responseBody);
	        utilityFileWriteOP.writeToLog(tcid, "Response Body is ", " "+responseBody,ResultPath,xwpfrun,"");
	        
	        //response.toString();
			if(response.getStatusLine().getStatusCode() != 201) {
				//Reporting_Utilities.writeStepToHTMLLog(tcid,String.valueOf(++StepCount), "New Customer Registration using Identity API call", "FAIL", "FAIL", ResultPath);
		        utilityFileWriteOP.writeToLog(tcid, "Create Order Details Sending Failed", ""+responseBody,ResultPath,xwpfrun,"");
		        logger.log(LogStatus.FAIL, "Create Order Details Sending Failed "+responseBody);
		        res = null;
				throw new MyException("Test Stopped Because of Failure. Please check Execution log");
		    }else{
				
		        utilityFileWriteOP.writeToLog(tcid, "Response Body is ", " : "+responseBody,ResultPath,xwpfrun,"");
//		        logger.log(LogStatus.PASS, "Response Body is : "+responseBody);
		        JsonElement jsonelement = new JsonParser().parse(responseBody);
		        JsonObject jsonobject = jsonelement.getAsJsonObject();
		        res = jsonobject.get(retrievedField).toString().replace("\"", "");;
			}
			
	
	        
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during Stock Movement API Call ", "", ResultPath,xwpfrun,"");
			
	
			
			res = null;
			return res;
			
		}
	    
	    finally{
	        
	        response.close();
	        
	}
	    
	    return res;
		
		
	}
	
	public static String PostStatusDetails(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue,String ContentKey, String ContentValue, HashMap<String, String> CreateOrderDetails,String retrievedField, String tcid,String ResultPath,XWPFRun xwpfrun, ExtentTest logger) throws IOException, InterruptedException {
		
		String res = null;
		CloseableHttpResponse response=null;
	    CredentialsProvider credsProvider = new BasicCredentialsProvider();
		credsProvider.setCredentials(
	    		new AuthScope(ProxyHostName, ProxyPort),
	    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials
		CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();
		try {
			HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
	        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
	        RequestConfig config = RequestConfig.custom()
	                .setProxy(proxy)
	                .build();
	        HttpGet httpget = new HttpGet(UrlTail + "?" + ApiKey);
	        httpget.setConfig(config);        
	        httpget.addHeader(AuthorizationKey, AuthorizationValue);
	        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
	        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");
//	        logger.log(LogStatus.INFO, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
			response = httpclient.execute(target, httpget);
	        System.out.println(response);
	        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
//	        logger.log(LogStatus.INFO, "Response code is "+response.getStatusLine().getStatusCode());
	        utilityFileWriteOP.writeToLog(tcid, "Response code is ", "Displayed as: "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");
	        String responseBody = EntityUtils.toString(response.getEntity());
	        System.out.println(responseBody);
	        utilityFileWriteOP.writeToLog(tcid, "Response Body is ", " "+responseBody,ResultPath,xwpfrun,"");
	       ;
	        //response.toString();
			if(response.getStatusLine().getStatusCode() != 202) {
				
		        JsonElement jsonelement = new JsonParser().parse(responseBody);
		        JsonObject jsonobject = jsonelement.getAsJsonObject();
		        res = jsonobject.get(retrievedField).toString().replace("\"", "");
		    }else{
				JsonElement jsonelement = new JsonParser().parse(responseBody);
		        JsonObject jsonobject = jsonelement.getAsJsonObject();
		        res = jsonobject.get(retrievedField).toString().replace("\"", "");;
		        if(res.contains("orderfilereceived")){
		        	utilityFileWriteOP.writeToLog(tcid, "Response Body is ", " : "+responseBody,ResultPath,xwpfrun,"");
		        	logger.log(LogStatus.INFO, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
		        	logger.log(LogStatus.INFO, "Response code is "+response.getStatusLine().getStatusCode());
			        logger.log(LogStatus.PASS, "Response Body is : "+responseBody);
		        }
		        
			}
			
	
	        
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during Stock Movement API Call ", "", ResultPath,xwpfrun,"");
			
	
			
			res = null;
			return res;
			
		}
	    
	    finally{
	        
	        response.close();
	        
	}
	    
	    return res;
		
		
	}
public static String getDetails(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, 
		int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue,
		String ContentKey, String ContentValue,String retrievedField, HashMap<String, String> expectedRes,String tcid,String ResultPath,XWPFRun xwpfrun, ExtentTest logger) throws IOException, InterruptedException {
		
		String res = null;
		CloseableHttpResponse response=null;
	    CredentialsProvider credsProvider = new BasicCredentialsProvider();
		credsProvider.setCredentials(
	    		new AuthScope(ProxyHostName, ProxyPort),
	    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials
		CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();
		try {
			HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
	        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
	        RequestConfig config = RequestConfig.custom()
	                .setProxy(proxy)
	                .build();
	        HttpGet httpget = new HttpGet(UrlTail + "?" + ApiKey);
	        httpget.setConfig(config);        
	        httpget.addHeader(AuthorizationKey, AuthorizationValue);
	        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
	        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");
//	        logger.log(LogStatus.INFO, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
			response = httpclient.execute(target, httpget);
	        System.out.println(response);
	        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
//	        logger.log(LogStatus.INFO, "Response code is "+response.getStatusLine().getStatusCode());
	        utilityFileWriteOP.writeToLog(tcid, "Response code is ", "Displayed as: "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");
	        String responseBody = EntityUtils.toString(response.getEntity());
	        System.out.println(responseBody);
	        utilityFileWriteOP.writeToLog(tcid, "Response Body is ", " "+responseBody,ResultPath,xwpfrun,"");
	       ;
	        //response.toString();
			if(response.getStatusLine().getStatusCode() != 200) {
				
		        JsonElement jsonelement = new JsonParser().parse(responseBody);
		        JsonObject jsonobject = jsonelement.getAsJsonObject();
		        res = jsonobject.get(retrievedField).toString().replace("\"", "");
		    }else{
				JsonElement jsonelement = new JsonParser().parse(responseBody);
		        JsonObject jsonobjectMain = jsonelement.getAsJsonObject();
		        jsonelement = jsonobjectMain.get("orderRequestInfo");
		        JsonObject jsonobject = jsonelement.getAsJsonObject();
		        res = jsonobject.get(retrievedField).toString().replace("\"", "");
		        jsonelement = jsonobjectMain.get("orderInfo");
		        jsonobject = jsonelement.getAsJsonObject();
		        String orderAlternateId=jsonobject.get("orderAlternateId").toString().replace("\"", "");
		        String orderId=jsonobject.get("orderId").toString().replace("\"", "");
		        jsonelement = jsonobjectMain.get("supplierInfo");
		        jsonobject = jsonelement.getAsJsonObject();
		        String supplierId=jsonobject.get("supplierId").toString().replace("\"", "");
		        logger.log(LogStatus.INFO, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
	        	logger.log(LogStatus.INFO, "Response code is "+response.getStatusLine().getStatusCode());
		        if(supplierId.trim().contains(expectedRes.get("supplierId").trim())==true){
		        	logger.log(LogStatus.PASS, "supplierId validation is successful. Value displayed is : "+supplierId);
		        }
		        if(orderAlternateId.trim().contains(expectedRes.get("orderAlternateId").trim())==true){
		        	logger.log(LogStatus.PASS, "orderAlternateId validation is successful. Value displayed is : "+orderAlternateId);
		        }
		        if(orderId.trim().contains(expectedRes.get("orderId").trim())==true){
		        	logger.log(LogStatus.PASS, "orderId validation is successful. Value displayed is : "+orderId);
		        }
		        if(res!=null){
		        	utilityFileWriteOP.writeToLog(tcid, "Response Body is ", " : "+responseBody,ResultPath,xwpfrun,"");
		        	
			        logger.log(LogStatus.PASS, "Response Body is : "+responseBody);
		        }
		        
			}
			
	
	        
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during Stock Movement API Call ", "", ResultPath,xwpfrun,"");
			
	
			
			res = null;
			return res;
			
		}
	    
	    finally{
	        
	        response.close();
	        
	}
	    
	    return res;
		
		
	}
public static String getOneIdfromList(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue,String ContentKey, String ContentValue,String ArrVal, String retrievedField, String tcid,String ResultPath,XWPFRun xwpfrun, Logger logger) throws IOException, InterruptedException {
		
		String res = null;
		CloseableHttpResponse response=null;
	    CredentialsProvider credsProvider = new BasicCredentialsProvider();
		credsProvider.setCredentials(
	    		new AuthScope(ProxyHostName, ProxyPort),
	    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials
		CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();
		try {
			HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
	        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
	        RequestConfig config = RequestConfig.custom()
	                .setProxy(proxy)
	                .build();
	        HttpGet httpget = new HttpGet(UrlTail + "?" + ApiKey);
	        httpget.setConfig(config);        
	        httpget.addHeader(AuthorizationKey, AuthorizationValue);
	        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
//	        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");
//	        logger.log(LogStatus.INFO, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
			response = httpclient.execute(target, httpget);
	        System.out.println(response);
	        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
//	        logger.log(LogStatus.INFO, "Response code is "+response.getStatusLine().getStatusCode());
//	        utilityFileWriteOP.writeToLog(tcid, "Response code is ", "Displayed as: "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");
	        String responseBody = EntityUtils.toString(response.getEntity());
	        System.out.println(responseBody);
//	        utilityFileWriteOP.writeToLog(tcid, "Response Body is ", " "+responseBody,ResultPath,xwpfrun,"");
	       ;
	        //response.toString();
			if(response.getStatusLine().getStatusCode() >= 200 && response.getStatusLine().getStatusCode() <= 200) {
				
		        JsonElement jsonelement = new JsonParser().parse(responseBody);
		        JsonObject jsonobject = jsonelement.getAsJsonObject();
		        JsonArray jarray = jsonobject.getAsJsonArray(ArrVal);
		        if(jarray.size()<1){
		        	logger.info("Test Stopped because no data is retrieved");
		        	throw new MyException("Test Stopped because no data is retrieved");
		        }
		        jsonelement= jarray.get(0).getAsJsonObject();
	        	jsonobject = jsonelement.getAsJsonObject();
		        Map<String, Object> jsontoMap=jsonutilities.jsonToMap(jsonobject);
		        res = jsontoMap.get(retrievedField).toString().replace("\"", "");
		        if(res.length()>0){
//		        	utilityFileWriteOP.writeToLog(tcid, "Response Body is ", " : "+responseBody,ResultPath,xwpfrun,"");
		        	logger.info("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
		        	logger.info("Response code is "+response.getStatusLine().getStatusCode());
			        logger.info("Response Body is : "+responseBody);
		        }
		    }else{
		    	logger.info("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
	        	logger.info("Response code is "+response.getStatusLine().getStatusCode());
				JsonElement jsonelement = new JsonParser().parse(responseBody);
		        JsonObject jsonobject = jsonelement.getAsJsonObject();
		        res = jsonobject.get(retrievedField).toString().replace("\"", "");;
		        
		        
			}
			
	
	        
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
//			utilityFileWriteOP.writeToLog(tcid, "Error occurred during Stock Movement API Call ", "", ResultPath,xwpfrun,"");
			
	
			
			res = null;
			return res;
			
		}
	    
	    finally{
	        
	        response.close();
	        
	}
	    
	    return res;
		
		
	}
}
