package Utilities;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class NotePadUtilities {

	public static String readNotePad(String AbsoluteFileName) throws FileNotFoundException{
		File file=new File(AbsoluteFileName);
		Scanner sc = new Scanner(file);
		if(sc.hasNext())
			return sc.nextLine();
		else
			return null;
	}
	
	public static void WriteInNotepad(String TestResult) {
        try {
        	FileWriter fwOb = new FileWriter(System.getProperty("user.dir")+"/Result.txt", false); 
            PrintWriter pwOb = new PrintWriter(fwOb, false);
            pwOb.flush();
            fwOb.write(TestResult + "\n");
        	pwOb.close();
            fwOb.close();
        } catch (IOException e) {
        	e.printStackTrace();
        }
    }
	public static void AppendInNotepad(String TestResult) {
        try {
        	FileWriter fw = new FileWriter(System.getProperty("user.dir")+"/Result.txt", true); 
        	BufferedWriter bw = new BufferedWriter(fw); 
        	PrintWriter pw = new PrintWriter(bw); 
        	pw.println(TestResult+"\n");
        	
        	pw.flush();

        	
        } catch (IOException e) {
        	e.printStackTrace();
        }
    }
}
