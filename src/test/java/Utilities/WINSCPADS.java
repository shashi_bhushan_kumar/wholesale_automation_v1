package Utilities;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Utilities.TakeScreenShot;
import Utilities.excelCellValueWrite;
import Utilities.getCurrentDate;
import Utilities.utilityFileWriteOP;

public class WINSCPADS {
	
	
	
public static boolean LoginRelexADS (WebDriverWait wait, WebDriver driver, String username, String Pwd , String tcid, int r, String DriverSheetPath,String SheetName,String ResultPath){
		
		boolean res=false;
		try{
			String AfterLogin=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(AfterLogin, driver);
			
		   	
			driver.findElement(By.xpath("//*[@id='ember441']")).sendKeys(username);
			driver.findElement(By.xpath("//*[@id='ember445']")).sendKeys(Pwd);

			Thread.sleep(2000);
		
			driver.findElement(By.xpath("//*[@id='ember458']/label")).click();
			
			System.out.println("1");
			wait.until(ExpectedConditions.invisibilityOfElementLocated((By.xpath("//div[@class='ember-view _1lN6b flame-view _297BG']"))));
			utilityFileWriteOP.writeToLog(tcid, "Login page displayed", "PASS",ResultPath);
			String AfterLogin1=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(AfterLogin1, driver);
			utilityFileWriteOP.writeToLog(tcid, "Login Successful", "PASS",ResultPath);
			
			System.out.println("2");
			List<WebElement> popuplist = driver.findElements(By.xpath("//div[@class = '_2DqB0 KSjA4 YEzHN']"));
			int popupcount = popuplist.size();
			
			
			for(int c = 0 ; c<popupcount ; c++)
			{
				
				WebElement element=driver.findElement(By.xpath("//div[@class = '_2DqB0 KSjA4 YEzHN']"));
				
				JavascriptExecutor executor = (JavascriptExecutor)driver;
				executor.executeScript("arguments[0].click();", element);
				System.out.println("hey");
			
			}
			utilityFileWriteOP.writeToLog(tcid, "Popups closed", "PASS",ResultPath);
			Thread.sleep(5000);
			res=true;
		}
		catch (Exception e) {
			
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Problem occured in Login", "Fail",ResultPath);
			String AfterLogin=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(AfterLogin, driver);
			res = false;
			return res;
		}
		return res;
	}
	public static boolean ReadDatainADS (WebDriverWait wait, WebDriver driver, String username, String Pwd , String tcid, int r, String DriverSheetPath,String SheetName,String ResultPath){
		boolean res=false;
				try{
					String AfterLogin=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
					TakeScreenShot.saveScreenShot(AfterLogin, driver);
					
				   	
					driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
					
					WebElement op=driver.findElement(By.xpath("//label[@class='_3tCQF' and text()='Optimizer']"));
					Actions actions = new Actions(driver);
					actions.moveToElement(op).click().build().perform();
					System.out.println("3");
					
					utilityFileWriteOP.writeToLog(tcid, "Optimizer button Clicked", "PASS",ResultPath);
					String AfterLogin2=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
					TakeScreenShot.saveScreenShot(AfterLogin2, driver);
					driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//span[@class='folder-name' and text()='Testing'])[2]")));
					driver.findElement(By.xpath("(//span[@class='folder-name' and text()='Testing'])[2]")).click();
					utilityFileWriteOP.writeToLog(tcid, "Testing", "Clicked",ResultPath);
					System.out.println("4");
					driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);	
					driver.findElement(By.xpath("//div[contains (text(),'Read in ADS data')]")).click();

					utilityFileWriteOP.writeToLog(tcid, "Read in ADS data", "Clicked",ResultPath);
					String AfterLogin3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
					TakeScreenShot.saveScreenShot(AfterLogin3, driver);
					
					driver.findElement(By.xpath("//label[@class='_3tCQF' and text()='Start']")).click();
					utilityFileWriteOP.writeToLog(tcid, "Start button Clicked", "PASS",ResultPath);

					String AfterLogin5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
					TakeScreenShot.saveScreenShot(AfterLogin5, driver);
					
					
					int i=0;
					Thread.sleep(1000);
					driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);	
					while(i==0)
					{
						Thread.sleep(3000);
						System.out.println("Waiting for job to complete");
						i=driver.findElements(By.xpath("//div[@class = '_2DqB0 KSjA4 YEzHN']")).size();
					}
					driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);	
					driver.findElement(By.xpath("//div[@class = '_2DqB0 KSjA4 YEzHN']")).click();
					System.out.println("ADS data pulled in Relex");
					Thread.sleep(5000);
					res=true;
				}
				catch (Exception e) {
					
					e.printStackTrace();
					utilityFileWriteOP.writeToLog(tcid, "Problem occured in Login", "Fail",ResultPath);
					String AfterLogin=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
					TakeScreenShot.saveScreenShot(AfterLogin, driver);
					res = false;
					return res;
				}
				
				return res;
	}
	public static boolean CreateSmoothedOrders (WebDriverWait wait, WebDriver driver, String username, String Pwd , String tcid, int NoOfOrderToBeCreated ,int r, String DriverSheetPath,String SheetName,String ResultPath) {
		
		
	boolean res= false;	
	
	try{
	
		String AfterLogin=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin, driver);
		
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
				
		WebElement worksp=driver.findElement(By.xpath("//label[@class='_3tCQF' and text()='Workspace']"));
		Actions action = new Actions(driver);
		action.moveToElement(worksp).click().build().perform();
		utilityFileWriteOP.writeToLog(tcid, "Workspace button Clicked", "PASS",ResultPath);
		
		String AfterLogin7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin7, driver);
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='folder-name' and text()='Testing']")));
		driver.findElement(By.xpath("//span[@class='folder-name' and text()='WM Validation']")).click();
		utilityFileWriteOP.writeToLog(tcid, "Testing", "WM Validation",ResultPath);
		
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//div[contains(text(),'Smoothing Range')]")).click();
		utilityFileWriteOP.writeToLog(tcid, "Smoothing Range", "Clicked",ResultPath);
		
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);

		wait.until(ExpectedConditions.visibilityOfAllElements(driver.findElements(By.xpath("//table[@class='flame-table']/tbody/tr"))));
		int rowCount=driver.findElements(By.xpath("//table[@class='flame-table']/tbody/tr")).size();
		System.out.println(rowCount);
		ArrayList<String> Oid = new ArrayList<String>();
		Thread.sleep(7000);
		
	
		int count=0;
		driver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);
		
		for(int m=2; m<rowCount;m++)
		{
			
			String status=driver.findElement(By.xpath("//table[@class='flame-table']/tbody/tr["+m+"]/td[3]")).getText();
			//System.out.println(status);
			if(status.contains("No"))
			{
				
				driver.findElement(By.xpath("//table[@class='flame-table']/tbody/tr["+m+"]/td[2]")).click();
				
				driver.findElement(By.xpath("//table[@class='flame-table']/tbody/tr["+m+"]/td[3]")).click();
	
				
				Robot rb=new Robot();
				rb.keyPress(KeyEvent.VK_ENTER);
				rb.keyRelease(KeyEvent.VK_ENTER);
				
				rb.keyPress(KeyEvent.VK_DOWN);
				//rb.keyRelease(KeyEvent.VK_DOWN);
				
				rb.keyPress(KeyEvent.VK_DOWN);
				//rb.keyRelease(KeyEvent.VK_DOWN);
				
				rb.keyPress(KeyEvent.VK_ENTER);
				rb.keyRelease(KeyEvent.VK_ENTER);
				
			
				Thread.sleep(5000);
				String orderNo=driver.findElement(By.xpath("//table[@class='flame-table']/tbody/tr["+m+"]/td[2]")).getText();
				Oid.add(orderNo);	
				System.out.println(driver.findElement(By.xpath("//table[@class='flame-table']/tbody/tr["+m+"]/td[3]")).getText());
				System.out.println(orderNo);
				count++;
				
				utilityFileWriteOP.writeToLog(tcid, "Order number accepted", orderNo, ResultPath);				
				String AfterLogin10=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(AfterLogin10, driver);
				
			
				if(count==r)
				{
					break;
				}
			}
		}
		
		
		
		WebElement accept=driver.findElement(By.xpath("//span[@class='working-copy']"));
		Actions action2 = new Actions(driver);
		action2.moveToElement(accept).click().build().perform();
		
		String AfterLogin9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin9, driver);
	
		driver.findElement(By.xpath("//label[contains(text(), 'Accept All')]")).click();
		
		String AfterLogin11=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin11, driver);
		
		Thread.sleep(5000);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		
		
		WebElement op1=driver.findElement(By.xpath("//label[@class='_3tCQF' and text()='Optimizer']"));
		Actions actions1 = new Actions(driver);
		actions1.moveToElement(op1).click().build().perform();
		System.out.println("3");
		
		utilityFileWriteOP.writeToLog(tcid, "Optimizer button Clicked", "PASS",ResultPath);
		String AfterLogin20=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin20, driver);
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//span[@class='folder-name' and text()='Testing'])[2]")));
		driver.findElement(By.xpath("(//span[@class='folder-name' and text()='Testing'])[2]")).click();
		utilityFileWriteOP.writeToLog(tcid, "Testing", "Clicked",ResultPath);
		System.out.println("4");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		Robot rb3 = new Robot();			
		rb3.keyPress(KeyEvent.VK_PAGE_DOWN);
		//rb3.keyRelease(KeyEvent.VK_PAGE_DOWN);
		
		
		Thread.sleep(2000);
		
		
		
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//div[contains (text(),'Send smoothed orders')]")).click();
		System.out.println("5");
		utilityFileWriteOP.writeToLog(tcid, "Send smoothed orders", "Clicked",ResultPath);
		String AfterLogin30=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin30, driver);
		
		driver.findElement(By.xpath("//label[@class='_3tCQF' and text()='Start']")).click();
		utilityFileWriteOP.writeToLog(tcid, "Start button Clicked", "PASS",ResultPath);

		String AfterLogin50=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin50, driver);
		String jobid=driver.findElement(By.xpath(".//div[@class = 'bubble-id']")).getText();
		
		System.out.println("job id is: "+ jobid);
		
		excelCellValueWrite.writeValueToCell(jobid, r, 21, DriverSheetPath, SheetName);
		

		
		utilityFileWriteOP.writeToLog(tcid, "Job finished", "PASS",ResultPath);
		utilityFileWriteOP.writeToLog(tcid, "jobid", jobid,ResultPath);
		
		String AfterLogin10=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin10, driver);
		
		String finaljobid= jobid.substring(1);
		int j=0;
		Thread.sleep(1000);
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);	
		while(j==0)
		{
			Thread.sleep(3000);
			System.out.println("Waiting for job to complete");
			j=driver.findElements(By.xpath("//div[@class = '_2DqB0 KSjA4 YEzHN']")).size();
		}
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);	
		driver.findElement(By.xpath("//div[@class = '_2DqB0 KSjA4 YEzHN']")).click();
		
		
		
		
		
		
		
		
		driver.findElement(By.xpath("//label[@class='_3tCQF' and text() = 'Monitoring']")).click();				
		Thread.sleep(3000);	
		
		driver.findElement(By.xpath("//a[contains(text(),'job.log."+finaljobid+"')]")).click();	
		
		ArrayList<String> alltabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(alltabs.get(1));	
		
		String BodyText = driver.findElement(By.xpath("//pre[contains(text(), 'with job ID')]")).getText();
		
		String JsonJobID = BodyText.substring(BodyText.indexOf("with job ID") + 12, BodyText.indexOf("with job ID") + 48);
		System.out.println(JsonJobID);
		String BodyText2 =driver.findElement(By.xpath("//pre[contains(text(), 'with job ID')]")).getText();
		String Filename =JsonJobID + "." + BodyText2.substring(BodyText.indexOf("Sent file RLXORD") + 10,BodyText2.indexOf("Sent file RLXORD") + 48);
		System.out.println(Filename);
		excelCellValueWrite.writeValueToCell(JsonJobID, r, 27, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(Filename, r, 28, DriverSheetPath, SheetName);

		driver.switchTo().window(alltabs.get(0));
		
		
		
		
		
		
		
		String final_order = "";
		
		for (String Order:Oid)
		{
			
				String orderid = Order.replaceAll(",", "");
			
			
				final_order =final_order + orderid + ",";
			
											
		}
		
		excelCellValueWrite.writeValueToCell(final_order, r, 22, DriverSheetPath, SheetName);
		
		
		
		res=true;
		
		
		
	} catch (Exception e) {
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Problem occured in Login", "Fail",ResultPath);
		String AfterLogin=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin, driver);
		res = false;
		return res;
	}
	
	return res;
	}
	
	
	public static boolean validateFileExistenceInPending(WebDriver driver, WebDriverWait wait, String searchKey, String FileName, String tcid,String ResultPath,XWPFRun xwpfRun)
	{
		boolean res=false;
		try
		{

			driver.findElement(By.xpath("//input[@placeholder='Search for buckets' and @type='text']")).sendKeys(searchKey);
			utilityFileWriteOP.writeToLog(tcid, "Search field is populated with ", "Keyword "+searchKey,ResultPath,xwpfRun,"");
			
			
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(@class,'list-view-item-name') and text()='"+searchKey+"']")));
			String awsDynamoDB_sc1=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc1, driver);
			utilityFileWriteOP.writeToLog(tcid, "Link related to keyword "+searchKey, "Appeared!!!",ResultPath,xwpfRun);
			
			String awsDynamoDB_sc2=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc2, driver);
			
			driver.findElement(By.xpath("//a[contains(@class,'list-view-item-name') and text()='"+searchKey+"']")).click();
			utilityFileWriteOP.writeToLog(tcid, "Link related to keyword "+searchKey, "Clicked!!!",ResultPath,xwpfRun);
			
			
			driver.findElement(By.xpath("//a[contains(text(), 'inbound')]")).click();
			 
			 driver.findElement(By.xpath("//a[contains(text(), 'transactions')]")).click();
			 
			 WebElement element =  driver.findElement(By.xpath("//a[contains(text(), 'whstock')]"));
			 ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
			
			 element.click();

			 driver.findElement(By.xpath("//a[contains(text(), 'pending')]")).click();
			
			
			
		
			if(driver.findElement(By.xpath("//a[contains(@class,'list-view-item-name object-name ng-binding') and contains(text(),'"+FileName+"')]")).isDisplayed())
				{
					String filename=driver.findElement(By.xpath("//a[contains(@class,'list-view-item-name object-name ng-binding') and contains(text(),'whstock_')]")).getText();
					res=true;
					String awsDynamoDB_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
					TakeScreenShot.saveScreenShot(awsDynamoDB_sc3, driver);
					utilityFileWriteOP.writeToLog(tcid, "The file with name "+filename+"Exists in pending folder!!!","PASS",ResultPath,xwpfRun);
				}
			driver.findElement(By.xpath("//*[@id='nav-usernameMenu']/div[2]")).click();
			driver.findElement(By.xpath("//*[@id='aws-console-logout']")).click();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			String awsDynamoDB_sc=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc, driver);
			res=false;
			utilityFileWriteOP.writeToLog(tcid, "A problem occurred while navigating through AWS S3 bucket", "Due to "+e,ResultPath,xwpfRun);
		}
		
		
		return res;
		
	}
	
	
	
	
	
	
	public static boolean WHonOrderFilevalidationInS3(WebDriver driver, WebDriverWait wait, String searchKey, String FileName, String tcid,String ResultPath,XWPFRun xwpfRun)
	{
		boolean res=false;
		try
		{

			driver.findElement(By.xpath("//input[@placeholder='Search for buckets' and @type='text']")).sendKeys(searchKey);
			String awsDynamoDB_sc1=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc1, driver);
			utilityFileWriteOP.writeToLog(tcid, "Search field is populated with ", "Keyword "+searchKey,ResultPath,xwpfRun);
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(@class,'list-view-item-name') and text()='"+searchKey+"']")));
			String awsDynamoDB_sc2=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc2, driver);
			utilityFileWriteOP.writeToLog(tcid, "Link related to keyword "+searchKey, "Appeared!!!",ResultPath,xwpfRun);
			driver.findElement(By.xpath("//a[contains(@class,'list-view-item-name') and text()='"+searchKey+"']")).click();
			utilityFileWriteOP.writeToLog(tcid, "Link related to keyword "+searchKey, "Clicked!!!",ResultPath,xwpfRun,"");
			
			
			driver.findElement(By.xpath("//a[contains(text(), 'inbound')]")).click();
			 
			 driver.findElement(By.xpath("//a[contains(text(), 'transactions')]")).click();
			 WebElement element =  driver.findElement(By.xpath("//a[contains(text(), 'whonorders')]"));
			 ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
			 element.click();
			 driver.findElement(By.xpath("//a[contains(text(), 'pending')]")).click();
			if(driver.findElement(By.xpath("//a[contains(text(),'"+FileName+"')]")).isDisplayed())
				{
					String filename=driver.findElement(By.xpath("//a[contains(text(),'"+FileName+"')]")).getText();
					res=true;
					String awsDynamoDB_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
					TakeScreenShot.saveScreenShot(awsDynamoDB_sc3, driver);
					utilityFileWriteOP.writeToLog(tcid, "The file with name "+filename+"Exists in pending folder!!!","PASS",ResultPath,xwpfRun);
				}
			driver.findElement(By.xpath("//*[@id='nav-usernameMenu']/div[2]")).click();
			driver.findElement(By.xpath("//*[@id='aws-console-logout']")).click();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			String awsDynamoDB_sc=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc, driver);
			res=false;
			utilityFileWriteOP.writeToLog(tcid, "A problem occurred while navigating through AWS S3 bucket", "Due to "+e,ResultPath,xwpfRun,"");
		}
		
		
		return res;
		
	}
	
		
	
	public static boolean OrderItemStatusFilevalidationInS3(WebDriver driver, WebDriverWait wait, String searchKey, String FileName, String tcid,String ResultPath,XWPFRun xwpfRun)
	{
		boolean res=false;
		try
		{
			driver.findElement(By.xpath("//input[@placeholder='Search for buckets' and @type='text']")).sendKeys(searchKey);
			utilityFileWriteOP.writeToLog(tcid, "Search field is populated with ", "Keyword "+searchKey,ResultPath,xwpfRun,"");
			
			String awsDynamoDB_sc1=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc1, driver);
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(@class,'list-view-item-name') and text()='"+searchKey+"']")));
			utilityFileWriteOP.writeToLog(tcid, "Link related to keyword "+searchKey, "Appeared!!!",ResultPath,xwpfRun);
			
			String awsDynamoDB_sc2=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc2, driver);
			
			driver.findElement(By.xpath("//a[contains(@class,'list-view-item-name') and text()='"+searchKey+"']")).click();
			utilityFileWriteOP.writeToLog(tcid, "Link related to keyword "+searchKey, "Clicked!!!",ResultPath,xwpfRun);
			
			
			driver.findElement(By.xpath("//a[contains(text(), 'inbound')]")).click();
			 
			 driver.findElement(By.xpath("//a[contains(text(), 'procurement')]")).click();
			 WebElement element =  driver.findElement(By.xpath("//a[contains(text(), 'order')]"));
			 ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
			 element.click();
			 driver.findElement(By.xpath("//a[contains(text(), 'pend')]")).click();
			 if(driver.findElement(By.xpath("//a[contains(@class,'list-view-item-name object-name ng-binding') and contains(text(),'"+FileName+"')]")).isDisplayed())
				{
					String filename=driver.findElement(By.xpath("//a[contains(@class,'list-view-item-name object-name ng-binding') and contains(text(),'"+FileName+"')]")).getText();
					res=true;
					String awsDynamoDB_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
					TakeScreenShot.saveScreenShot(awsDynamoDB_sc3, driver);
					utilityFileWriteOP.writeToLog(tcid, "The file with name "+filename+"Exists in pending folder!!!","PASS",ResultPath,xwpfRun);
				}
			driver.findElement(By.xpath("//*[@id='nav-usernameMenu']/div[2]")).click();
			driver.findElement(By.xpath("//*[@id='aws-console-logout']")).click();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			String awsDynamoDB_sc=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc, driver);
			res=false;
			utilityFileWriteOP.writeToLog(tcid, "A problem occurred while navigating through AWS S3 bucket", "Due to "+e,ResultPath,xwpfRun);
		}
		
		
		return res;
		
	}
	
	
	
	
	
	public static boolean GapScanFilevalidationInS3(WebDriver driver, WebDriverWait wait, String searchKey, String FileName, String tcid,String ResultPath, XWPFRun xwpfRun)
	{
		boolean res=false;
		try
		{
			driver.findElement(By.xpath("//input[@placeholder='Search for buckets' and @type='text']")).sendKeys(searchKey);
			String awsDynamoDB_sc1=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc1, driver);
			utilityFileWriteOP.writeToLog(tcid, "Search field is populated with ", "Keyword "+searchKey,ResultPath,xwpfRun);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(@class,'list-view-item-name') and text()='"+searchKey+"']")));
			
			
			String awsDynamoDB_sc2=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc2, driver);
			utilityFileWriteOP.writeToLog(tcid, "Link related to keyword "+searchKey, "Appeared!!!",ResultPath,xwpfRun);
			
			driver.findElement(By.xpath("//a[contains(@class,'list-view-item-name') and text()='"+searchKey+"']")).click();
			utilityFileWriteOP.writeToLog(tcid, "Link related to keyword "+searchKey, "Clicked!!!",ResultPath,xwpfRun,"");
			
			
			driver.findElement(By.xpath("//a[contains(text(), 'inbound')]")).click();
			 
			 driver.findElement(By.xpath("//a[contains(text(), 'transactions')]")).click();
			 WebElement element =  driver.findElement(By.xpath("//a[contains(text(), 'gapscan')]"));
			 element.click();
			 driver.findElement(By.xpath("//a[contains(text(), 'stocks')]")).click();
			 driver.findElement(By.xpath("//a[contains(text(), 'pend')]")).click();
			 if(driver.findElement(By.xpath("//a[contains(@class,'list-view-item-name object-name ng-binding') and contains(text(),'"+FileName+"')]")).isDisplayed())
				{
					String filename=driver.findElement(By.xpath("//a[contains(@class,'list-view-item-name object-name ng-binding') and contains(text(),'"+FileName+"')]")).getText();
					res=true;
					String awsDynamoDB_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
					TakeScreenShot.saveScreenShot(awsDynamoDB_sc3, driver);
					utilityFileWriteOP.writeToLog(tcid, "The file with name "+filename+"Exists in pending folder!!!","PASS",ResultPath,xwpfRun);
				}
			driver.findElement(By.xpath("//*[@id='nav-usernameMenu']/div[2]")).click();
			driver.findElement(By.xpath("//*[@id='aws-console-logout']")).click();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			String awsDynamoDB_sc=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc, driver);
			res=false;
			utilityFileWriteOP.writeToLog(tcid, "A problem occurred while navigating through AWS S3 bucket", "Due to "+e,ResultPath,xwpfRun);
		}
		
		
		return res;
		
	}
	
		
	
	public static boolean OneDayOrderFilevalidationInS3(WebDriver driver, WebDriverWait wait, String searchKey, String FileName, String tcid,String ResultPath,XWPFRun xwpfRun)
	{
		boolean res=false;
		try
		{
			driver.findElement(By.xpath("//input[@placeholder='Search for buckets' and @type='text']")).sendKeys(searchKey);
			String awsDynamoDB_sc1=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc1, driver);
			utilityFileWriteOP.writeToLog(tcid, "Search field is populated with ", "Keyword "+searchKey,ResultPath,xwpfRun);
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(@class,'list-view-item-name') and text()='"+searchKey+"']")));
			String awsDynamoDB_sc2=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc2, driver);
			utilityFileWriteOP.writeToLog(tcid, "Link related to keyword "+searchKey, "Appeared!!!",ResultPath,xwpfRun);
			driver.findElement(By.xpath("//a[contains(@class,'list-view-item-name') and text()='"+searchKey+"']")).click();
			utilityFileWriteOP.writeToLog(tcid, "Link related to keyword "+searchKey, "Clicked!!!",ResultPath,xwpfRun,"");
			
			
			driver.findElement(By.xpath("//a[contains(text(), 'inbound')]")).click();
			 
			 driver.findElement(By.xpath("//a[contains(text(), 'retrieved')]")).click();
			 WebElement element =  driver.findElement(By.xpath("//a[contains(text(), 'orderproposa')]"));
			 ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
			 element.click();
			 driver.findElement(By.xpath("//a[contains(text(), 'oneday')]")).click();
			 driver.findElement(By.xpath("//a[contains(text(), 'pend')]")).click();
			 if(driver.findElement(By.xpath("//a[contains(@class,'list-view-item-name object-name ng-binding') and contains(text(),'"+FileName+"')]")).isDisplayed())
				{
					String filename=driver.findElement(By.xpath("//a[contains(@class,'list-view-item-name object-name ng-binding') and contains(text(),'"+FileName+"')]")).getText();
					res=true;
					String awsDynamoDB_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
					TakeScreenShot.saveScreenShot(awsDynamoDB_sc3, driver);
					utilityFileWriteOP.writeToLog(tcid, "The file with name "+filename+"Exists in pending folder!!!","PASS",ResultPath,xwpfRun);
				}
			driver.findElement(By.xpath("//*[@id='nav-usernameMenu']/div[2]")).click();
			driver.findElement(By.xpath("//*[@id='aws-console-logout']")).click();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			String awsDynamoDB_sc=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc, driver);
			res=false;
			utilityFileWriteOP.writeToLog(tcid, "A problem occurred while navigating through AWS S3 bucket", "Due to "+e,ResultPath,xwpfRun);
		}
		
		
		return res;
		
	}
	
	
	
	
	public static boolean ADSRangeFilevalidationInS3(WebDriver driver, WebDriverWait wait, String searchKey, String FileName, String tcid,String ResultPath,XWPFRun xwpfRun)
	{
		boolean res=false;
		try
		{
			driver.findElement(By.xpath("//input[@placeholder='Search for buckets' and @type='text']")).sendKeys(searchKey);
			String awsDynamoDB_sc1=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc1, driver);
			utilityFileWriteOP.writeToLog(tcid, "Search field is populated with ", "Keyword "+searchKey,ResultPath,xwpfRun);
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(@class,'list-view-item-name') and text()='"+searchKey+"']")));
			String awsDynamoDB_sc2=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc2, driver);
			utilityFileWriteOP.writeToLog(tcid, "Link related to keyword "+searchKey, "Appeared!!!",ResultPath,xwpfRun);
			driver.findElement(By.xpath("//a[contains(@class,'list-view-item-name') and text()='"+searchKey+"']")).click();
			utilityFileWriteOP.writeToLog(tcid, "Link related to keyword "+searchKey, "Clicked!!!",ResultPath,xwpfRun,"");
			
			
			driver.findElement(By.xpath("//a[contains(text(), 'inbound')]")).click();
			 
			 driver.findElement(By.xpath("//a[contains(text(), 'masterdata')]")).click();
			 WebElement element =  driver.findElement(By.xpath("//a[contains(text(), 'adsrange')]"));
			 ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
			 element.click();
			// driver.findElement(By.xpath("//a[contains(text(), 'stocks')]")).click();
			 driver.findElement(By.xpath("//a[contains(text(), 'pend')]")).click();
			 if(driver.findElement(By.xpath("//a[contains(@class,'list-view-item-name object-name ng-binding') and contains(text(),'"+FileName+"')]")).isDisplayed())
				{
					String filename=driver.findElement(By.xpath("//a[contains(@class,'list-view-item-name object-name ng-binding') and contains(text(),'"+FileName+"')]")).getText();
					res=true;
					utilityFileWriteOP.writeToLog(tcid, "The file with name "+filename+"Exists in pending folder!!!","PASS",ResultPath,xwpfRun,"");
				}
			driver.findElement(By.xpath("//*[@id='nav-usernameMenu']/div[2]")).click();
			driver.findElement(By.xpath("//*[@id='aws-console-logout']")).click();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			String awsDynamoDB_sc=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc, driver);
			res=false;
			Utilities.utilityFileWriteOP.writeToLog(tcid, "A problem occurred while navigating through AWS S3 bucket", "Due to "+e,ResultPath);
		}
		
		
		return res;
		
	}
	
	
	
	
}

	
	
