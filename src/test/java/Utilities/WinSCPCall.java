package Utilities;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;


import java.util.concurrent.TimeUnit;

import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.relevantcodes.extentreports.ExtentTest;

import Utilities.*;


public class WinSCPCall {
	
	public static boolean LoginRackspace (WebDriverWait wait, WebDriver driver, String username, String PASSwd , String tcid,String ResultPath) {
		
		
		boolean res= false;	
		try{
		
			String AfterLogin=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(AfterLogin, driver);
		//wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//*[@id='ember606']"))));
		driver.findElement(By.xpath("//*[@id='ember606']")).sendKeys(username);
		driver.findElement(By.xpath("//*[@id='ember622']")).sendKeys(PASSwd);
		
		Thread.sleep(2000);
		
		driver.findElement(By.xpath("//*[@id='ember634']/label")).click();
		////wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(text(), 'Testing')]")));
		utilityFileWriteOP.writeToLog(tcid, "Login page displayed", "PASS",ResultPath);
		String relex_sc2=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(relex_sc2, driver);
		////System.out.println("Found");
		////wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//span[contains(text(), 'Testing')]"))));
		
		
		//*[@id="ember3394"]/span
			//driver.findElement(By.xpath("//*[@id='ember3394']/span")).click();
			////System.out.println("Clicked",ResultPath);
			
			
			//wait = new WebDriverWait(driver, 90);
			
			////wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//h1[contains(text(),'Loading data...']")));
			
			
			//wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class='ember-view flame-modal-pane flame-view dim-background']")));
			
			
			
			
			
			utilityFileWriteOP.writeToLog(tcid, "Login Successful", "PASS",ResultPath);
			
			
			
		
			
			List<WebElement> popuplist = driver.findElements(By.xpath("//div[@class = 'close-button' and text() = 'x']"));
			
			int popupcount = popuplist.size();
			
			for(int c = 0 ; c<popupcount ; c++){
				driver.findElement(By.xpath("//div[@class = 'close-button' and text() = 'x']")).click();
				
				
			}
				
				
				driver.findElement(By.xpath("//label[@class='flame-button-label' and text() = 'Optimizer']")).click();
				
				utilityFileWriteOP.writeToLog(tcid, "Optimizer button clicked", "PASS",ResultPath);
				String relex_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(relex_sc3, driver);
				//int tagnameIndex=0;
				/*for(int i=1;i<=10;i++)
				{
					WebElement element=driver.findElement(By.xpath("//div[contains(text(),'ember-view scroll-view flame-view')]/div[contains(text(),'ember-view flame-collection-view')]/div["+i+"]/span"));
					String tagName=element.getText();
					//System.out.println("Now in loop number "+i);
					if(tagName.contains("Testing"))
					{
						tagnameIndex=i;
						//System.out.println("Found the testing");
						break;
					}
				
				
				}*/
				
				////wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[contains(text(),'ember-view scroll-view flame-view')]/div[contains(text(),'ember-view flame-collection-view')]/div["+tagnameIndex+"]/span")));
				
				//wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//span[contains(text(),'Testing')])[2]")));
			
				//Thread.sleep(1000);
				//driver.findElement(By.xpath("//span[contains(text(),'Testing (13) ')]")).click();
				
				
				//*[@id="ember11645"]/span
				
				

				
				driver.findElement(By.xpath("(//span[contains(text(),'Testing')])[2]")).click();
				
				utilityFileWriteOP.writeToLog(tcid, "Testing Clicked", "PASS",ResultPath);
				TakeScreenShot.saveScreenShot(relex_sc3, driver);
				//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(text(), 'Read in data to Relex')]")));
				
				driver.findElement(By.xpath("//span[contains(text(), 'Read in data to Relex')]")).click();
				
				utilityFileWriteOP.writeToLog(tcid, "Read in data to Relex Clicked", "PASS",ResultPath);
				TakeScreenShot.saveScreenShot(relex_sc3, driver);
				
				driver.findElement(By.xpath("//label[@class='flame-button-label' and text()='Start']")).click();
				
				utilityFileWriteOP.writeToLog(tcid, "Start button Clicked", "PASS",ResultPath);
				TakeScreenShot.saveScreenShot(relex_sc3, driver);
				
				wait= new WebDriverWait(driver, 300);
				
				
				
				driver.findElement(By.xpath("//div[@class = 'close-button' and text() = 'x']")).click();
				
				wait= new WebDriverWait(driver, 60);
				Thread.sleep(100);
				driver.findElement(By.xpath("//label[@class='flame-button-label' and text() = 'Workspace']")).click();
				
				utilityFileWriteOP.writeToLog(tcid, "Workspace button Clicked", "PASS",ResultPath);
				TakeScreenShot.saveScreenShot(relex_sc3, driver);
				//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='ember-view flame-tree-item-view-content' and contains(text() , 'Pending optimization runs')]")));
				
				driver.findElement(By.xpath("//div[@class='ember-view flame-tree-item-view-content' and contains(text() , 'Pending optimization runs')]")).click();
				
				
				Robot rb3 = new Robot();
				
				rb3.keyPress(KeyEvent.VK_PAGE_DOWN);
			
			
			driver.findElement(By.xpath("//span[contains(text(), 'Testing')]")).click();
			utilityFileWriteOP.writeToLog(tcid, "Testing option", "Clicked",ResultPath);
			
			TakeScreenShot.saveScreenShot(relex_sc3, driver);
			
		
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Problem occured in Login and batch run", "Fail",ResultPath);
			String relex_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";
			TakeScreenShot.saveScreenShot(relex_sc3, driver);
			res = false;
			return res;
		}
		
		return res;
	}

	
	
	
public static boolean LogoutRackspace (WebDriverWait wait, WebDriver driver,String tcid,String ResultPath, XWPFRun xwpfRun) {
		
		
		boolean res= false;	
		try{
		
			driver.findElement(By.xpath("//div[@class='ember-view _1RBXF']")).click();
			
			utilityFileWriteOP.writeToLog(tcid, "Clicked on username", "Clicked",ResultPath, xwpfRun,"");
			Robot rb = new Robot();
			for(int i=0; i<5; i++)
			{
			rb.keyPress(KeyEvent.VK_DOWN);
			rb.keyRelease(KeyEvent.VK_DOWN);
			}
			rb.keyPress(KeyEvent.VK_ENTER);
			rb.keyPress(KeyEvent.VK_ENTER);
			
			String AfterLogOut=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
			TakeScreenShot.saveScreenShot(AfterLogOut, driver);
			
			utilityFileWriteOP.writeToLog(tcid, "Log Out option", "Clicked",ResultPath,xwpfRun);
			
			
			
		res=true;
			
			
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res = false;
			return res;
		}
		
		return res;
	}
	
	
	
	
	
	
public static boolean LoginRackspaceOutbound (WebDriverWait wait, WebDriver driver, String username, String PASSwd , String tcid,String ResultPath, XWPFRun xwpfRun) {
	
	
	boolean res= false;	
	try{
	
		String AfterLogin=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin, driver);
		
	   	
		driver.findElement(By.xpath("//*[@id='ember674']")).sendKeys(username);
		utilityFileWriteOP.writeToLog(tcid, "Username is entered", "PASS",ResultPath,xwpfRun);
		driver.findElement(By.xpath("//*[@id='ember678']")).sendKeys(PASSwd);
		utilityFileWriteOP.writeToLog(tcid, "Password is entered", "PASS",ResultPath,xwpfRun,"");
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='ember689']/label")).click();
		utilityFileWriteOP.writeToLog(tcid, "Clicked on Login Button", "PASS",ResultPath,xwpfRun,"");
		
		//driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		Thread.sleep(10000);
		
		
		//wait.until(ExpectedConditions.invisibilityOfElementLocated((By.xpath("//h1[@class='ember-view flame-label-view _2WnQx flame-view _2sUV5']"))));
		
		
		wait.until(ExpectedConditions.invisibilityOfElementLocated((By.xpath("//h1[@id='ember3705']"))));
		
		
		
		
		String AfterLogin1=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin1, driver);
		utilityFileWriteOP.writeToLog(tcid, "Home page displayed", "PASS",ResultPath,xwpfRun);
		utilityFileWriteOP.writeToLog(tcid, "Login Successful", "PASS",ResultPath,xwpfRun,"");
		
		
		List<WebElement> popuplist = driver.findElements(By.xpath("//div[@class = '_2DqB0 KSjA4 YEzHN']"));
		
		
		
	
		
		
		int popupcount = popuplist.size();
		
		
		for(int c = 0 ; c<popupcount ; c++)
		{
			
			WebElement element=driver.findElement(By.xpath("//div[@class = '_2DqB0 KSjA4 YEzHN']"));
			
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			
			
			executor.executeScript("arguments[0].click();", element);
			
			Thread.sleep(300);
			
			
			System.out.println("Popup closed!!!");
		
		}
		
		
		/*Thread.sleep(30000);
		
		System.out.println("Before clicking on workspace");
		
		
	//	WebElement workspace=driver.findElement(By.xpath("//div[@id='ember3272'][@class='ember-view flame-button-view _1uLtH _1EE3o flame-view _1jvVF']"));
		
	//	WebElement workspace=driver.findElement(By.xpath(" //*[@id='ember3272']/label"));
		
		
		
	//	Thread.sleep(30000);
		
		
	//	workspace.click();	
		
		
		//*[@id="ember3272"]
		
		
		String AfterLogin2=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin2, driver);
		utilityFileWriteOP.writeToLog(tcid, "Workspace button Clicked", "PASS",ResultPath,xwpfRun);
		
		driver.findElement(By.xpath("//span[contains(text(),'REPORTING')]")).click();
		
		
		int i=0;
		
		System.out.println("Reporting Clicked!!");
		
		//driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);	
	
		
		while(i==0)
		{
			Robot rb3 = new Robot();			
			rb3.keyPress(KeyEvent.VK_PAGE_DOWN);
			Thread.sleep(300);
			i=driver.findElements(By.xpath("//span[contains(text(), 'JSON')]")).size();
			
		}
		
		
		
		Robot rb3 = new Robot();			
		rb3.keyPress(KeyEvent.VK_PAGE_DOWN);
		Thread.sleep(3000);
		
		
		driver.findElement(By.xpath("//span[contains(text(), 'JSON')]")).click();
		

		Thread.sleep(3000);
		
		
		//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);	
		
		
		//driver.findElement(By.xpath("//span[contains(text(), 'JSON')]")).click();
		
		
		String AfterLogin3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin3, driver);
		
		
		utilityFileWriteOP.writeToLog(tcid, "JSON option", "Clicked",ResultPath,xwpfRun);*/
		
		
		res = true;
		
		
	} catch (Exception e) {
		
		e.printStackTrace();
		
		String AfterLogin=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin, driver);
		utilityFileWriteOP.writeToLog(tcid, "Problem occured in Login", "Fail",ResultPath,xwpfRun);
		
		System.out.println("Error is "+e);
		
		res = false;
		return res;
	}
	
	return res;
}



public static boolean LoginRackspaceAmend (WebDriverWait wait, WebDriver driver, String username, String PASSwd , String tcid,String ResultPath,XWPFRun xwpfRun) {
	
	
	boolean res= false;	
	try{
	
		String AfterLogin=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin, driver);
		utilityFileWriteOP.writeToLog(tcid, "Login page displayed", "PASS",ResultPath,xwpfRun);
	   	
		driver.findElement(By.xpath("//*[@id='ember674']")).sendKeys(username);
		driver.findElement(By.xpath("//*[@id='ember678']")).sendKeys(PASSwd);

		Thread.sleep(2000);
	
		driver.findElement(By.xpath("//*[@id='ember689']/label")).click();
		
		
		wait.until(ExpectedConditions.invisibilityOfElementLocated((By.xpath("//h1[@class='ember-view flame-label-view _2WnQx flame-view _2sUV5']"))));
		String AfterLogin1=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin1, driver);
		utilityFileWriteOP.writeToLog(tcid, "Home page displayed", "PASS",ResultPath,xwpfRun);
		utilityFileWriteOP.writeToLog(tcid, "Login Successful", "PASS",ResultPath,xwpfRun,"");
		
		
		List<WebElement> popuplist = driver.findElements(By.xpath("//div[@class = '_2DqB0 KSjA4 YEzHN']"));
		int popupcount = popuplist.size();
		
		
		for(int c = 0 ; c<popupcount ; c++)
		{
			
			WebElement element=driver.findElement(By.xpath("//div[@class = '_2DqB0 KSjA4 YEzHN']"));
			
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", element);
		
		}
		
		WebElement workspace=driver.findElement(By.xpath("//div[@id='ember3272'][@class='ember-view flame-button-view _1uLtH _1EE3o flame-view _1jvVF']"));
		
		workspace.click();			
		String AfterLogin2=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin2, driver);
		utilityFileWriteOP.writeToLog(tcid, "Workspace button Clicked", "PASS",ResultPath,xwpfRun,"");
		driver.findElement(By.xpath("//span[contains(text(),'Pending optimization runs')]")).click();
		int i=0;
		
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);	
		while(i==0)
		{
			Robot rb3 = new Robot();			
			rb3.keyPress(KeyEvent.VK_PAGE_DOWN);
			Thread.sleep(2000);
			i=driver.findElements(By.xpath("//span[contains(text(), 'Testing')]")).size();
		}
		
		driver.findElement(By.xpath("//span[contains(text(), 'Testing')]")).click();
		int j=0;
		while(j==0)
		{
			Robot rb3 = new Robot();			
			rb3.keyPress(KeyEvent.VK_PAGE_DOWN);
			Thread.sleep(2000);
			j=driver.findElements(By.xpath("//div[contains(text(), 'po response')]")).size();
		}
		
		
		driver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);	
		driver.findElement(By.xpath("//div[contains(text(), 'po response')]")).click();
		String AfterLogin3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin3, driver);
		utilityFileWriteOP.writeToLog(tcid, "po response from testing option", "Clicked",ResultPath,xwpfRun);
		res = true;
		
		
	} catch (Exception e) {
		
		e.printStackTrace();
		String AfterLogin=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin, driver);
		utilityFileWriteOP.writeToLog(tcid, "Problem occured in Login", "Fail",ResultPath,xwpfRun);
		res = false;
		return res;
	}
	
return res;
}



public static boolean RelexorderProcessedValidation (WebDriverWait wait, WebDriver driver, String username, String PASSwd , String tcid, int NoOfOrderToBeCreated ,int r, int nextRow, String DriverSheetPath,String SheetName,String ResultPath, String OrderNos,XWPFRun xwpfRun) {
	
	
	boolean res= false;	
	try{
	
		WebElement workspace=driver.findElement(By.xpath("//div[@id='ember3272'][@class='ember-view flame-button-view _1uLtH _1EE3o flame-view _1jvVF']"));
		workspace.click();			
		String AfterLogin2=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin2, driver);
		utilityFileWriteOP.writeToLog(tcid, "Workspace button Clicked", "PASS",ResultPath,xwpfRun);
		
		driver.findElement(By.xpath("//span[contains(text(),'Pending optimization runs')]")).click();
		int i=0;
		
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);	
		while(i==0)
		{
			Robot rb3 = new Robot();			
			rb3.keyPress(KeyEvent.VK_PAGE_DOWN);
			Thread.sleep(2000);
			i=driver.findElements(By.xpath("//span[contains(text(), 'Testing')]")).size();
		}
		
		driver.findElement(By.xpath("//span[contains(text(), 'Testing')]")).click();
		int j=0;
		while(j==0)
		{
			Robot rb3 = new Robot();			
			rb3.keyPress(KeyEvent.VK_PAGE_DOWN);
			Thread.sleep(2000);
			j=driver.findElements(By.xpath("//div[contains(text(), 'po response')]")).size();
		}
		
		
		driver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);	
		driver.findElement(By.xpath("//div[contains(text(), 'po response')]")).click();
		
		String AfterLogin3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin3, driver);
		utilityFileWriteOP.writeToLog(tcid, "po response from testing option", "Clicked",ResultPath,xwpfRun);

		wait.until(ExpectedConditions.visibilityOfAllElements(driver.findElements(By.xpath("//table[@class='flame-table']/tbody/tr"))));
		driver.findElement(By.xpath("//table[@class='flame-table']/tbody/tr[1]/td[1]")).click();
		driver.findElement(By.xpath("//table[@class='flame-table']/tbody/tr[1]/td[11]")).click();

		driver.findElement(By.xpath("(//div[@class='header-container'])[1]//table/tbody/tr/td[11]")).click();
		Thread.sleep(5000);

		driver.findElement(By.xpath("//div[contains(text(),'Sort descending...')]")).click();
		Thread.sleep(1000);
		//wait.until(ExpectedConditions.visibilityOfAllElements(driver.findElements(By.xpath("//table[@class='flame-table']/tbody/tr"))));
		int rowCount=driver.findElements(By.xpath("//table[@class='flame-table']/tbody/tr")).size();
		System.out.println(rowCount);
		
		
		ArrayList<String> Oid = new ArrayList<String>(Arrays.asList(OrderNos.split(",")));
		ArrayList<String> Status = new ArrayList<String>();
		Thread.sleep(5000);
		
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		for(int n=0;n<Oid.size(); n++)
		{
			for(int m=2; m<rowCount;m++)
			{
				String orderNo=driver.findElement(By.xpath("//table[@class='flame-table']/tbody/tr["+m+"]/td[1]")).getText();
				if(Oid.get(n).equalsIgnoreCase(orderNo))
				{
					String status=driver.findElement(By.xpath("//table[@class='flame-table']/tbody/tr["+m+"]/td[11]")).getText();
					Status.add(status);
					System.out.println(status);
					utilityFileWriteOP.writeToLog(tcid, "Status of Order No "+ orderNo +" is "+status, "Clicked",ResultPath,xwpfRun);
					break;
				}
				else
				{
					continue;
				}
				
			}
		}

		String final_status = "";
		
		for (String St:Status)
		{
			
				String st = St.replaceAll(",", "");
			
			
				final_status =final_status + st + ",";
			
											
		}
		
		excelCellValueWrite.writeValueToCell(final_status, r, 25, DriverSheetPath, SheetName);
		
		res=true;
		
	
	
	
	} catch (Exception e) {
		
		e.printStackTrace();
		String AfterLogin=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin, driver);
		utilityFileWriteOP.writeToLog(tcid, "Problem occured in Order Validation", "Fail",ResultPath,xwpfRun);
		return res;
	}
		return res;
	}

public static boolean RelexRackspaceAmendOrder (WebDriverWait wait, WebDriver driver, String username, String PASSwd , String tcid, int NoOfOrderToBeCreated ,int r, int nextRow, String DriverSheetPath,String SheetName,String ResultPath, XWPFRun xwpfRun) {
	
	
	boolean res= false;	
	try{

		wait.until(ExpectedConditions.visibilityOfAllElements(driver.findElements(By.xpath("//table[@class='flame-table']/tbody/tr"))));
		driver.findElement(By.xpath("//table[@class='flame-table']/tbody/tr[1]/td[1]")).click();
		driver.findElement(By.xpath("//table[@class='flame-table']/tbody/tr[1]/td[11]")).click();

		driver.findElement(By.xpath("(//div[@class='header-container'])[1]//table/tbody/tr/td[11]")).click();
		Thread.sleep(5000);

		driver.findElement(By.xpath("//div[contains(text(),'Sort descending...')]")).click();
		Thread.sleep(1000);
		//wait.until(ExpectedConditions.visibilityOfAllElements(driver.findElements(By.xpath("//table[@class='flame-table']/tbody/tr"))));
		int rowCount=driver.findElements(By.xpath("//table[@class='flame-table']/tbody/tr")).size();
		System.out.println(rowCount);
		ArrayList<String> Oid = new ArrayList<String>();
		Thread.sleep(5000);
		int count=0;
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		
		for(int m=2; m<rowCount;m++)
		{
			
			String status=driver.findElement(By.xpath("//table[@class='flame-table']/tbody/tr["+m+"]/td[11]")).getText();
			System.out.println(status);
			if((status.contains("Processed okay")))
			{
				
				String orderNo=driver.findElement(By.xpath("//table[@class='flame-table']/tbody/tr["+m+"]/td[1]")).getText();
				Oid.add(orderNo);	
				System.out.println(orderNo);
				count++;
				int[] x={1,4,7,10};
				int[] y={2,5,8,11};
				int[] z={3,6,9,12};
				int index1=Arrays.binarySearch(x, count);
				int index2=Arrays.binarySearch(y, count);
				int index3=Arrays.binarySearch(z, count);
				
				if(index1>=0)
				{
					
					driver.findElement(By.xpath("//table[@class='flame-table']/tbody/tr["+m+"]/td[1]")).click();
					String Text=driver.findElement(By.xpath("//table[@class='flame-table']/tbody/tr["+m+"]/td[13]")).getText();
					Thread.sleep(2000);
					DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
					Date ReceivedDate = sdf.parse(Text);
					
					Calendar c = Calendar.getInstance();
					c.setTime(ReceivedDate); // Now use today date.
					c.add(Calendar.DATE, 2); // Adding 5 days
					String output = sdf.format(c.getTime());
					System.out.println(output);
				
					driver.findElement(By.xpath("//table[@class='flame-table']/tbody/tr["+m+"]/td[14]")).click();
				
					Robot rb2=new Robot();
					rb2.keyPress(KeyEvent.VK_ENTER);
					rb2.keyRelease(KeyEvent.VK_ENTER);
					
					driver.findElement(By.xpath("//input[@placeholder='dd/mm/yyyy']")).sendKeys(output);
					
					rb2.keyPress(KeyEvent.VK_ENTER);
					rb2.keyRelease(KeyEvent.VK_ENTER);
				}
				else if(index2>=0)
				{
					Thread.sleep(5000);
					
					driver.findElement(By.xpath("//table[@class='flame-table']/tbody/tr["+m+"]/td[1]")).click();
					String Cquantity ="4";
					driver.findElement(By.xpath("//table[@class='flame-table']/tbody/tr["+m+"]/td[15]")).click();
					Actions action=new Actions(driver).doubleClick();
					action.build().perform();
					driver.findElement(By.xpath("//input[@class='table-edit-field']")).clear();
					driver.findElement(By.xpath("//input[@class='table-edit-field']")).sendKeys(Cquantity);
					
					
				}
				else if(index3>=0)
				{
					Thread.sleep(5000);
					driver.findElement(By.xpath("//table[@class='flame-table']/tbody/tr["+m+"]/td[1]")).click();
					driver.findElement(By.xpath("//table[@class='flame-table']/tbody/tr["+m+"]/td[16]")).click();
					driver.findElement(By.xpath("//table[@class='flame-table']/tbody/tr["+m+"]/td[16]")).click();
				
					
				
					Robot rb1=new Robot();
					rb1.keyPress(KeyEvent.VK_DOWN);
					rb1.keyRelease(KeyEvent.VK_DOWN);
					
					rb1.keyPress(KeyEvent.VK_DOWN);
					rb1.keyRelease(KeyEvent.VK_DOWN);
					
					rb1.keyPress(KeyEvent.VK_ENTER);
					rb1.keyRelease(KeyEvent.VK_ENTER);
					Thread.sleep(3000);
				}
				
	
				
							
				String AfterLogin1=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(AfterLogin1, driver);
				utilityFileWriteOP.writeToLog(tcid, "Order number accepted", orderNo, ResultPath,xwpfRun);	
				if(count==NoOfOrderToBeCreated)
				{
					break;
				}
			}
		}
		Thread.sleep(10000);
		
		
		driver.findElement(By.xpath("//span[@class='working-copy']")).click();
		
		
		String AfterLogin1=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin1, driver);
		utilityFileWriteOP.writeToLog(tcid, "Clicked on Accepts changes", "Pass" ,ResultPath,xwpfRun);
		
		driver.findElement(By.xpath("//label[contains(text(), 'Accept All')]")).click();
		
		
		String AfterLogin11=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin11, driver);
		utilityFileWriteOP.writeToLog(tcid, "Clicked on Accept All button", "Pass" ,ResultPath,xwpfRun);
		Thread.sleep(5000);
		
		
		
		driver.findElement(By.xpath("//label[@class='_3tCQF' and text() = 'Optimizer']")).click();
		String AfterLogin2=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin2, driver);
		utilityFileWriteOP.writeToLog(tcid, "Optimizer button clicked", "PASS",ResultPath,xwpfRun);
		Thread.sleep(6000);		

		//driver.findElement(By.xpath("(//span[@class='folder-name' and text()='JSON'])[2]")).click();	
		
		driver.findElement(By.xpath("(//span[@class='folder-name' and text()='Testing'])[1]")).click();					
		String AfterLogin3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin3, driver);
		utilityFileWriteOP.writeToLog(tcid, "Testing Clicked", "PASS",ResultPath,xwpfRun);

		driver.findElement(By.xpath("//div[contains(text(), 'Send out amendments (JSON)')]")).click();		
		String AfterLogin4=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin4, driver);
		utilityFileWriteOP.writeToLog(tcid, " Clicked Send out amendments (JSON)", "PASS",ResultPath,xwpfRun);
		driver.findElement(By.xpath("//label[@class='_3tCQF' and text()='Start']")).click();			
		String AfterLogin5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin5, driver);
		utilityFileWriteOP.writeToLog(tcid, "Start button Clicked", "PASS",ResultPath,xwpfRun);

		
		String jobid=driver.findElement(By.xpath(".//div[@class = 'bubble-id']")).getText();
		
		System.out.println("job id is: "+ jobid);
		
		excelCellValueWrite.writeValueToCell(jobid, r, 30, DriverSheetPath, SheetName);
		//excelCellValueWrite.writeValueToCell(jobid, 2, 30, DriverSheetPath, SheetName);
		
	
		
		utilityFileWriteOP.writeToLog(tcid, "Job finished", "PASS",ResultPath,xwpfRun,"");
		String AfterLogin6=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin6, driver);
		utilityFileWriteOP.writeToLog(tcid, "jobid", jobid,ResultPath,xwpfRun);
		
		String finaljobid= jobid.substring(1);
		int i=0;
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		while(i==0)
		{
			Thread.sleep(3000);
			System.out.println("Waiting for job to complete");
			i=driver.findElements(By.xpath("//div[@class = '_2DqB0 KSjA4 YEzHN']")).size();
		}
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//div[@class='_2DqB0 KSjA4 YEzHN']")).click();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		driver.findElement(By.xpath("//label[@class='_3tCQF' and text() = 'Monitoring']")).click();				
		Thread.sleep(3000);	
		String AfterLogin7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin7, driver);
		utilityFileWriteOP.writeToLog(tcid, "Clicked on Monitoring button", "PASS",ResultPath,xwpfRun);
		driver.findElement(By.xpath("//a[contains(text(),'job.log."+finaljobid+"')]")).click();	
		String AfterLogin8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin8, driver);
		utilityFileWriteOP.writeToLog(tcid, "Clicked on Job Log", "PASS",ResultPath,xwpfRun);
		ArrayList<String> alltabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(alltabs.get(1));	
		Thread.sleep(3000);
		String BodyText=driver.findElement(By.xpath("/html/body/pre")).getText();
		//String BodyText = driver.findElement(By.xpath("//pre[contains(text(), 'with job ID')]")).getText();
		String JsonJobID = BodyText.substring(BodyText.indexOf("with job ID") + 12, BodyText.indexOf("with job ID") + 48);
		System.out.println(JsonJobID);
		String BodyText2 =driver.findElement(By.xpath("/html/body/pre")).getText();
		String Filename =JsonJobID + "." + BodyText2.substring(BodyText.indexOf("Sent file OrderAmend") + 10,BodyText2.indexOf("Sent file OrderAmend") + 52);
		System.out.println(Filename);
		excelCellValueWrite.writeValueToCell(JsonJobID, r, 24, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(Filename, r, 31, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(JsonJobID,2 , 24, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(Filename, 2, 31, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(JsonJobID,1 , 24, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(Filename, 1, 31, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(JsonJobID,4 , 24, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(Filename, 4, 31, DriverSheetPath, SheetName);
		utilityFileWriteOP.writeToLog(tcid, "JsonJobID ", JsonJobID,ResultPath,xwpfRun,"");
		utilityFileWriteOP.writeToLog(tcid, "Filename ", Filename,ResultPath,xwpfRun,"");
		driver.switchTo().window(alltabs.get(0));
		
		
		
		
		
		
		
		String final_order = "";
		
		for (String Order:Oid)
		{
			
				String orderid = Order.replaceAll(",", "");
			
			
				final_order =final_order + orderid + ",";
			
											
		}
		
		
		
		excelCellValueWrite.writeValueToCell(final_order, nextRow, 25, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(final_order, nextRow, 25, DriverSheetPath, SheetName);
		
		res=true;
		
	
	
	
	} catch (Exception e) {
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Problem occured in Order Creation", "Fail",ResultPath);
		String AfterLogin=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin, driver);
		
		return res;
	}
	
	
	
	return res;
}




public static boolean RelexRackspaceOutboundCreateOrder (WebDriverWait wait, WebDriver driver, String username, String PASSwd , String tcid, int NoOfOrderToBeCreated ,int r, int nextRow, String DriverSheetPath,String SheetName,String ResultPath, XWPFRun xwpfRun) {
	
	
	boolean res= false;	
	try{
		driver.findElement(By.xpath("//div[contains(text(), 'Order proposal sending JSON [NT 23-8]')]")).click();		
		
		String relex_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";
		TakeScreenShot.saveScreenShot(relex_sc3, driver);
		utilityFileWriteOP.writeToLog(tcid, "Order proposal sending JSON clicked", "PASS",ResultPath,xwpfRun);
	
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);

		wait.until(ExpectedConditions.visibilityOfAllElements(driver.findElements(By.xpath("//table[@class='flame-table']/tbody/tr"))));
		int rowCount=driver.findElements(By.xpath("//table[@class='flame-table']/tbody/tr")).size();
		System.out.println(rowCount);
		ArrayList<String> Oid = new ArrayList<String>();
		Thread.sleep(2000);
		
	
		int count=0;
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		
		for(int m=2; m<rowCount;m++)
		{
			
			String status=driver.findElement(By.xpath("//table[@class='flame-table']/tbody/tr["+m+"]/td[8]")).getText();
			System.out.println(status);
			if(status.contains("No"))
			{
				
				driver.findElement(By.xpath("//table[@class='flame-table']/tbody/tr["+m+"]/td[1]")).click();
				String orderNo=driver.findElement(By.xpath("//table[@class='flame-table']/tbody/tr["+m+"]/td[1]")).getText();
				Oid.add(orderNo);	
				driver.findElement(By.xpath("//table[@class='flame-table']/tbody/tr["+m+"]/td[8]")).click();

				Robot rb=new Robot();
				rb.keyPress(KeyEvent.VK_ENTER);
				rb.keyRelease(KeyEvent.VK_ENTER);
				
				rb.keyPress(KeyEvent.VK_DOWN);
				rb.keyRelease(KeyEvent.VK_DOWN);
				
				rb.keyPress(KeyEvent.VK_DOWN);
				rb.keyRelease(KeyEvent.VK_DOWN);
				
				rb.keyPress(KeyEvent.VK_ENTER);
				rb.keyRelease(KeyEvent.VK_ENTER);
						
				Thread.sleep(2000);
				count++;
				
				utilityFileWriteOP.writeToLog(tcid, "Order number accepted", orderNo, ResultPath,xwpfRun,"");				
				String AfterLogin1=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(AfterLogin1, driver);

				if(count==NoOfOrderToBeCreated)
				{
					break;
				}
			}
		}
		
		
		
		driver.findElement(By.xpath("//span[@class='working-copy']")).click();
		
		
		String AfterLogin1=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin1, driver);
		utilityFileWriteOP.writeToLog(tcid, "no of changes clicked", "PASS",ResultPath,xwpfRun);
		
		driver.findElement(By.xpath("//label[contains(text(), 'Accept All')]")).click();
		String AfterLogin11=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin11, driver);
		utilityFileWriteOP.writeToLog(tcid, "All changes are accepted", "PASS",ResultPath,xwpfRun);
		Thread.sleep(3000);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@class='_3tCQF' and text() = 'Optimizer']")));
		
		driver.findElement(By.xpath("//label[@class='_3tCQF' and text() = 'Optimizer']")).click();
		
		String AfterLogin2=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin2, driver);
		utilityFileWriteOP.writeToLog(tcid, "Optimizer button clicked", "PASS",ResultPath, xwpfRun,"");
		Thread.sleep(1000);		

		//driver.findElement(By.xpath("(//span[@class='folder-name' and text()='JSON'])[2]")).click();	
		
		driver.findElement(By.xpath("(//span[@class='folder-name' and text()='Testing'])[2]")).click();
		String AfterLogin3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin3, driver);
		utilityFileWriteOP.writeToLog(tcid, "Testing Clicked", "PASS",ResultPath,xwpfRun);
		driver.findElement(By.xpath("//div[contains(text(), 'Order proposal writer (JSON)')]")).click();
		
		
		
		String AfterLogin4=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin4, driver);
		utilityFileWriteOP.writeToLog(tcid, "Order proposal writer (JSON) Clicked", "PASS",ResultPath,xwpfRun);
		driver.findElement(By.xpath("//label[@class='_3tCQF' and text()='Start']")).click();		
		

		String AfterLogin5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin5, driver);
		utilityFileWriteOP.writeToLog(tcid, "Start button Clicked", "PASS",ResultPath,xwpfRun);
		
		
		
		String jobid=driver.findElement(By.xpath(".//div[@class = 'bubble-id']")).getText();
		
		System.out.println("job id is: "+ jobid);
		
		excelCellValueWrite.writeValueToCell(jobid, r, 30, DriverSheetPath, SheetName);
		
		excelCellValueWrite.writeValueToCell(jobid, nextRow, 30, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(jobid, nextRow+2, 30, DriverSheetPath, SheetName);
		
		utilityFileWriteOP.writeToLog(tcid, "Job finished", "PASS",ResultPath,xwpfRun,"");
		utilityFileWriteOP.writeToLog(tcid, "jobid", jobid,ResultPath,xwpfRun,"");
	
		
		String finaljobid= jobid.substring(1);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[contains(text(), 'Queued')]")));
		
		int i=0;
		Thread.sleep(1000);
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);	
		while(i==0)
		{
			Thread.sleep(3000);
			System.out.println("Waiting for job to complete");
			i=driver.findElements(By.xpath("//div[@class = '_2DqB0 KSjA4 YEzHN']")).size();
		}
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);	
		driver.findElement(By.xpath("//div[@class = '_2DqB0 KSjA4 YEzHN']")).click();
		

		
		driver.findElement(By.xpath("//label[@class='_3tCQF' and text() = 'Monitoring']")).click();				
		Thread.sleep(2000);	
		
		String AfterLogin6=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin6, driver);
		utilityFileWriteOP.writeToLog(tcid, "Clicked on Monitoring", "PASS",ResultPath,xwpfRun);
		driver.findElement(By.xpath("//a[contains(text(),'job.log."+finaljobid+"')]")).click();	
		
		ArrayList<String> alltabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(alltabs.get(1));	
		
		String BodyText = driver.findElement(By.xpath("//pre[contains(text(), 'with job ID')]")).getText();
		//String BodyText = driver.findElement(By.xpath("/html/body/pre")).getText();
		String JsonJobID = BodyText.substring(BodyText.indexOf("with job ID") + 12, BodyText.indexOf("with job ID") + 48);
		System.out.println(JsonJobID);
		String BodyText2 =driver.findElement(By.xpath("//pre[contains(text(), 'with job ID')]")).getText();
		String Filename =JsonJobID + "." + BodyText2.substring(BodyText.indexOf("Sent file OrderProposal") + 10,BodyText2.indexOf("Sent file OrderProposal") + 55);
		System.out.println(Filename);
		String AfterLogin7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin7, driver);
		utilityFileWriteOP.writeToLog(tcid, "Job log clicked and found json job id which is "+ JsonJobID, "PASS",ResultPath,xwpfRun);
		utilityFileWriteOP.writeToLog(tcid, "Found file name which is "+ Filename, "PASS",ResultPath,xwpfRun);
		excelCellValueWrite.writeValueToCell(JsonJobID, r, 24, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(Filename, r, 31, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(JsonJobID, nextRow, 24, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(Filename, nextRow, 31, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(JsonJobID, nextRow+2, 24, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(Filename, nextRow+2, 31, DriverSheetPath, SheetName);
		driver.switchTo().window(alltabs.get(0));
		
		
		String final_order = "";
		
		for (String Order:Oid)
		{			String orderid = Order.replaceAll(",", "");
						final_order =final_order + orderid + ",";
											
		}
		
		excelCellValueWrite.writeValueToCell(final_order, r, 25, DriverSheetPath, SheetName);
	
		
		res=true;
		
	
		return res;
	
	} catch (Exception e) {
		
		res=false;
		e.printStackTrace();
		
		String AfterLogin=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin, driver);
		utilityFileWriteOP.writeToLog(tcid, "Problem occured in Order Creation", "Fail",ResultPath,xwpfRun);
		return res;
	}
	
	
	
	
}







public static boolean RelexS3validationOutboundCreateOrder (WebDriverWait wait, WebDriver driver, String username, String PASSwd , String tcid, int NoOfOrderToBeCreated ,int r, String DriverSheetPath,String ResultPath) {
	
	
	boolean res= false;	
	try{
		
		
		
	
	} catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Problem occured in Order Creation", "Fail",ResultPath);
		String relex_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";
		TakeScreenShot.saveScreenShot(relex_sc3, driver);
		res = false;
		return res;
	}
	
	
	
	return res;
}











public static boolean PullRecordsfromS3 (WebDriverWait wait, WebDriver driver, String username, String PASSwd , String tcid, int r, String DriverSheetPath,String SheetName,String ResultPath,XWPFRun xwpfRun) {
	
	
	boolean res= false;	
	try
	{
		String AfterLogin=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin, driver);
		utilityFileWriteOP.writeToLog(tcid, "Login page displayed", "PASS",ResultPath,xwpfRun);
	   	
		driver.findElement(By.xpath("//*[@id='ember674']")).sendKeys(username);
		driver.findElement(By.xpath("//*[@id='ember678']")).sendKeys(PASSwd);

		Thread.sleep(2000);
	
		driver.findElement(By.xpath("//*[@id='ember689']/label")).click();
		
		
		wait.until(ExpectedConditions.invisibilityOfElementLocated((By.xpath("//h1[@class='ember-view flame-label-view _2WnQx flame-view _2sUV5']"))));
		utilityFileWriteOP.writeToLog(tcid, "Home page displayed", "PASS",ResultPath,xwpfRun,"");
		String AfterLogin1=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin1, driver);
		utilityFileWriteOP.writeToLog(tcid, "Login Successful", "PASS",ResultPath,xwpfRun,"");
		
		
		List<WebElement> popuplist = driver.findElements(By.xpath("//div[@class = '_2DqB0 KSjA4 YEzHN']"));
		int popupcount = popuplist.size();
		
		
		for(int c = 0 ; c<popupcount ; c++)
		{
			
			WebElement element=driver.findElement(By.xpath("//div[@class = '_2DqB0 KSjA4 YEzHN']"));
			
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", element);
		
		}
	
		
	
		
		Thread.sleep(100);
		String relex_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";
		
		Thread.sleep(6000);	
	driver.findElement(By.xpath("//label[@class='_3tCQF' and text() = 'Optimizer']")).click();		
	String AfterLogin3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
	TakeScreenShot.saveScreenShot(AfterLogin3, driver);
	utilityFileWriteOP.writeToLog(tcid, "Optimizer button clicked", "PASS",ResultPath,xwpfRun,"");
	Thread.sleep(6000);		
	////wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//span[contains(text(),'Testing')])[2]")));
	driver.findElement(By.xpath("(//span[@class='folder-name' and text()='JSON'])[1]")).click();	
	//System.out.println("1");
	String AfterLogin2=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
	TakeScreenShot.saveScreenShot(AfterLogin2, driver);
	utilityFileWriteOP.writeToLog(tcid, "Scheduled runs Clicked", "PASS",ResultPath,xwpfRun,"");
	//wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'Job status fetcher')]")));	
	
	//System.out.println("2");
	driver.findElement(By.xpath("//div[contains(text(),'Job status fetcher')]")).click();
	
	//System.out.println("3");
	String AfterLogin4=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
	TakeScreenShot.saveScreenShot(AfterLogin4, driver);
	utilityFileWriteOP.writeToLog(tcid, "job status fetcher Clicked", "PASS",ResultPath,xwpfRun);
	driver.findElement(By.xpath("//label[contains(text(),'Start')]")).click();	
	//System.out.println("4");
	
	String AfterLogin5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
	TakeScreenShot.saveScreenShot(AfterLogin5, driver);
	utilityFileWriteOP.writeToLog(tcid, "Start button Clicked", "PASS",ResultPath,xwpfRun);

	wait= new WebDriverWait(driver, 1000);		
	//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='bubble-view-status completed' and text()='Finished']")));
	//<div class="bubble-id"><!-- react-text: 23 -->#<!-- /react-text --><!-- react-text: 24 -->21474836588<!-- /react-text --></div>
	String jobid=driver.findElement(By.xpath("//div[@class = 'bubble-id']")).getText();
	excelCellValueWrite.writeValueToCell(jobid, r, 30, DriverSheetPath, SheetName);
	utilityFileWriteOP.writeToLog(tcid, "Job finished", "PASS",ResultPath,xwpfRun,"");
	utilityFileWriteOP.writeToLog(tcid, "jobid" + jobid, "PASS",ResultPath,xwpfRun,"" );
	
	int i=0;
	driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);	
	while(i==0)
	{
		Thread.sleep(3000);
		System.out.println("Waiting for job to complete");
		i=driver.findElements(By.xpath("//div[@class = '_2DqB0 KSjA4 YEzHN']")).size();
	}
	driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
	
	driver.findElement(By.xpath("//div[@class = '_2DqB0 KSjA4 YEzHN']")).click();
	String AfterLogin6=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
	TakeScreenShot.saveScreenShot(AfterLogin5, driver);				
	res=true;

} catch (Exception e) {
	// TODO: handle exception
	e.printStackTrace();
	
	String relex_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";
	utilityFileWriteOP.writeToLog(tcid, "Problem occured in job run", "Fail",ResultPath,xwpfRun);
	TakeScreenShot.saveScreenShot(relex_sc3, driver);
	res = false;
	return res;
}



return res;
}

public static boolean RelexTroposOrderCreate (WebDriverWait wait, WebDriver driver, String username, String PASSwd , String tcid, int NoOfOrderToBeCreated ,int r, String DriverSheetPath,String ResultPath) {
	
	
	boolean res= false;	
	try{
	
		String AfterLogin=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin, driver);
		
		
		
		driver.findElement(By.xpath("//span[contains(text(), 'Order proposal sending JSON [NT 23-8]')]")).click();
		utilityFileWriteOP.writeToLog(tcid, "Order proposal sending JSON clicked", "PASS",ResultPath);
		String relex_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";
		TakeScreenShot.saveScreenShot(relex_sc3, driver);
		//wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class='ember-view flame-modal-pane flame-view dim-background']")));
		
		driver.findElement(By.xpath("(//input[@class='ember-view ember-text-field is-editable-label'])[5]")).sendKeys("S-8800");
		
		driver.findElement(By.xpath("//label[@class='flame-button-label' and text()='Search']")).click();
		Thread.sleep(5000);
		
		//wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class='ember-view flame-modal-pane flame-view dim-background']")));
		List<WebElement> rows = driver.findElements(By.xpath("//table[@class = 'flame-table']/tbody/tr/td[8]"));
		List<WebElement> rowsOrder = driver.findElements(By.xpath("//table[@class = 'flame-table']/tbody/tr/td[1]"));		
		//System.out.println(rowsOrder.get(0) + " "  + rowsOrder.get(1));
		
		
		/*
		
					
			
				rowsOrder.get(0).click();		
				rows.get(0).click();				
				Robot rb1 = new Robot();			
				rb1.keyPress(KeyEvent.VK_ENTER);
				rb1.keyPress(KeyEvent.VK_DOWN);
				rb1.keyPress(KeyEvent.VK_DOWN);
				rb1.keyPress(KeyEvent.VK_ENTER);
				


				
				Thread.sleep(5000);
				//wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class='ember-view flame-modal-pane flame-view']")));
				
				utilityFileWriteOP.writeToLog(tcid, "Order number accepted", rowsOrder.get(0).getText());				
				TakeScreenShot.saveScreenShot(relex_sc3, driver);
				
				
				
				
				
			
		
		
		
		
		
		
		//<div id="ember2908" class="ember-view flame-label-view changes-count flame-view has-changes toggle-property" style="line-height: 22px; right: 33px; top: 2px; height: 22px;"><span class="working-copy"><script id="metamorph-22-start" type="text/x-placeholder"></script>Abhinandan Ghosh (AG...<script id="metamorph-22-end" type="text/x-placeholder"></script>:</span> <script id="metamorph-23-start" type="text/x-placeholder"></script>13664 changes to accept &gt;<script id="metamorph-23-end" type="text/x-placeholder"></script></div>
		//driver.findElement(By.xpath("//div[contains(text(), 'AGhosh')]")).click();
		driver.findElement(By.xpath("//span[contains(text(), 'SRoy')]")).click();
		utilityFileWriteOP.writeToLog(tcid, "SRoy link clicked", "PASS",ResultPath);				
		TakeScreenShot.saveScreenShot(relex_sc3, driver);
		driver.findElement(By.xpath("//label[contains(text(), 'Accept All')]")).click();
		utilityFileWriteOP.writeToLog(tcid, "Accept all clicked", "PASS",ResultPath);				
		TakeScreenShot.saveScreenShot(relex_sc3, driver);
		Thread.sleep(5000);
		////wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='ember-view flame-button-view flame-view is-default']")));
		//driver.findElement(By.xpath("//div[@class='ember-view flame-button-view flame-view is-default']")).click();
		
		
		
		
		
		driver.findElement(By.xpath("//label[@class='flame-button-label' and text() = 'Optimizer']")).click();		
		utilityFileWriteOP.writeToLog(tcid, "Optimizer button clicked", "PASS",ResultPath);
		TakeScreenShot.saveScreenShot(relex_sc3, driver);
		
		
		
		
		
	
		Thread.sleep(6000);		
		////wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//span[contains(text(),'Testing')])[2]")));
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div[2]/div[2]/div[1]/div[3]/div[2]/div[3]/div[*]/span[contains(text(),'Testing')]")).click();		
		utilityFileWriteOP.writeToLog(tcid, "Testing Clicked", "PASS",ResultPath);
		TakeScreenShot.saveScreenShot(relex_sc3, driver);
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(text(), 'Order proposal writer (JSON)')]")));		
		driver.findElement(By.xpath("//span[contains(text(), 'Order proposal writer (JSON)')]")).click();		
		utilityFileWriteOP.writeToLog(tcid, "Order proposal writer (JSON) Clicked", "PASS",ResultPath);
		TakeScreenShot.saveScreenShot(relex_sc3, driver);		
		driver.findElement(By.xpath("//label[@class='flame-button-label' and text()='Start']")).click();		
		utilityFileWriteOP.writeToLog(tcid, "Start button Clicked", "PASS",ResultPath);
		TakeScreenShot.saveScreenShot(relex_sc3, driver);		
		wait= new WebDriverWait(driver, 1000);		
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='bubble-view-status completed' and text()='Finished']")));
		//<div class="bubble-id"><!-- react-text: 23 -->#<!-- /react-text --><!-- react-text: 24 -->21474836588<!-- /react-text --></div>
		String jobid=driver.findElement(By.xpath("//div[@class = 'bubble-id']")).getText();
		excelCellValueWrite.writeValueToCell(jobid, r, 30, DriverSheetPath, SheetName);
		utilityFileWriteOP.writeToLog(tcid, "Job finished", "PASS",ResultPath);
		utilityFileWriteOP.writeToLog(tcid, "jobid", jobid);
		TakeScreenShot.saveScreenShot(relex_sc3, driver);
		String finaljobid= jobid.substring(1);
		//System.out.println(finaljobid);
		driver.findElement(By.xpath("//div[@class = 'close-button' and text() = 'x']")).click();
		
		
		
		
		
		
		
		
		driver.findElement(By.xpath("//label[@class='flame-button-label' and text() = 'Logs']")).click();				
		Thread.sleep(3000);	
		driver.findElement(By.xpath("//a[contains(text(),'job.log."+finaljobid+"')]")).click();	
		ArrayList<String> alltabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(alltabs.get(1));		
		String BodyText = driver.findElement(By.xpath("//pre[contains(text(), 'with job ID')]")).getText();
		String JsonJobID = BodyText.substring(BodyText.indexOf("with job ID") + 12, BodyText.indexOf("with job ID") + 48);
		
		String BodyText2 =driver.findElement(By.xpath("//pre[contains(text(), 'Sent file OrderProposal')]")).getText();
		String Filename =JsonJobID + "." + BodyText2.substring(BodyText.indexOf("Sent file OrderProposal") + 10,BodyText2.indexOf("Sent file OrderProposal") + 55);
			
		excelCellValueWrite.writeValueToCell(JsonJobID, r, 24, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(Filename, r, 31, DriverSheetPath, SheetName);
		driver.switchTo().window(alltabs.get(0));
		*/
		
		
		
		
		
		
		
		res=true;
		
	
	
	
	} catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Problem occured in Order Creation", "Fail",ResultPath);
		String relex_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";
		TakeScreenShot.saveScreenShot(relex_sc3, driver);
		res = false;
		return res;
	}
	
	
	
	return res;
}


public static boolean RelexProjectedOrders (WebDriverWait wait, WebDriver driver, String username, String PASSwd , String tcid ,int r, int nextRow, String DriverSheetPath,String SheetName,String ResultPath,XWPFRun xwpfRun) {
	
	
	boolean res= false;	
	try{
	
		String relex_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(relex_sc3, driver);
		utilityFileWriteOP.writeToLog(tcid, "Login page displayed", "PASS",ResultPath,xwpfRun);
	   	
		driver.findElement(By.xpath("//*[@id='ember674']")).sendKeys(username);
		driver.findElement(By.xpath("//*[@id='ember678']")).sendKeys(PASSwd);

		Thread.sleep(2000);
	
		driver.findElement(By.xpath("//*[@id='ember689']/label")).click();
		
		
		wait.until(ExpectedConditions.invisibilityOfElementLocated((By.xpath("//h1[@class='ember-view flame-label-view _2WnQx flame-view _2sUV5']"))));
		String AfterLogin1=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin1, driver);
		utilityFileWriteOP.writeToLog(tcid, "Home page displayed", "PASS",ResultPath,xwpfRun);
		utilityFileWriteOP.writeToLog(tcid, "Login Successful", "PASS",ResultPath);
		
		
		List<WebElement> popuplist = driver.findElements(By.xpath("//div[@class = '_2DqB0 KSjA4 YEzHN']"));
		int popupcount = popuplist.size();
		
		
		for(int c = 0 ; c<popupcount ; c++)
		{
			
			WebElement element=driver.findElement(By.xpath("//div[@class = '_2DqB0 KSjA4 YEzHN']"));
			
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", element);
		
		}
		
		driver.findElement(By.xpath("//label[@class='_3tCQF' and text() = 'Optimizer']")).click();		
		TakeScreenShot.saveScreenShot(relex_sc3, driver);
		utilityFileWriteOP.writeToLog(tcid, "Optimizer button clicked", "PASS",ResultPath,xwpfRun);
		Thread.sleep(6000);		
		
		driver.findElement(By.xpath("(//span[@class='folder-name' and text()='JSON'])[2]")).click();	
		String AfterLogin2=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin2, driver);
		utilityFileWriteOP.writeToLog(tcid, "JSON Clicked", "PASS",ResultPath,xwpfRun);
		
		driver.findElement(By.xpath("//div[contains(text(),'Write out projected orders (API)')]")).click();
		String AfterLogin3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin3, driver);
		utilityFileWriteOP.writeToLog(tcid, "Write out projected orders (API) Clicked", "PASS",ResultPath,xwpfRun);
		
		driver.findElement(By.xpath("//label[@class='_3tCQF' and text()='Start']")).click();		
		String AfterLogin5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin5, driver);
		utilityFileWriteOP.writeToLog(tcid, "Start button Clicked", "PASS",ResultPath,xwpfRun);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='bubble-view-status completed' and text()='Finished']")));
		String jobid=driver.findElement(By.xpath("//div[@class = 'bubble-id']")).getText();
	
		
	
		
		utilityFileWriteOP.writeToLog(tcid, "Job finished", "PASS",ResultPath,xwpfRun,"");
		
		
		String AfterLogin=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin, driver);	
		utilityFileWriteOP.writeToLog(tcid, "jobid"+jobid,"PASS",ResultPath,xwpfRun);
		res=true;
		
		
		System.out.println("job id is: "+ jobid);
		
		excelCellValueWrite.writeValueToCell(jobid, r, 30, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(jobid, r+1, 30, DriverSheetPath, SheetName);
		
		driver.findElement(By.xpath("//div[@class = '_2DqB0 KSjA4 YEzHN']")).click();		
		String finaljobid= jobid.substring(1);
		Thread.sleep(5000);

		
		driver.findElement(By.xpath("//label[@class='_3tCQF' and text() = 'Monitoring']")).click();				
		Thread.sleep(3000);	
		
		driver.findElement(By.xpath("//a[contains(text(),'job.log."+finaljobid+"')]")).click();	
		
		ArrayList<String> alltabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(alltabs.get(1));	
		
		String BodyText = driver.findElement(By.xpath("//pre[contains(text(), 'with job ID')]")).getText();
		String JsonJobID = BodyText.substring(BodyText.indexOf("with job ID") + 12, BodyText.indexOf("with job ID") + 48);
		System.out.println(JsonJobID);
		String BodyText2 =driver.findElement(By.xpath("//pre[contains(text(), 'Sent file projectedorder')]")).getText();
		String Filename =JsonJobID + "." + BodyText2.substring(BodyText.indexOf("Sent file projectedorder") + 10,BodyText2.indexOf("Sent file projectedorder") + 55);
		System.out.println(Filename);
		utilityFileWriteOP.writeToLog(tcid, "Job log clicked and found json job id which is "+ JsonJobID, "PASS",ResultPath,xwpfRun,"");
		utilityFileWriteOP.writeToLog(tcid, "Found file name which is "+ Filename, "PASS",ResultPath,xwpfRun,"");
		
		excelCellValueWrite.writeValueToCell(JsonJobID, r, 24, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(Filename, r, 31, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(JsonJobID, nextRow, 24, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(Filename, nextRow, 31, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(JsonJobID, 1, 24, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(Filename, 1, 31, DriverSheetPath, SheetName);
		driver.switchTo().window(alltabs.get(0));
	}
	catch (Exception e){
		e.printStackTrace();
		res=false;
		
	}
	return res;
	}




public static boolean RelexRackspaceOutboundCreateOrder (WebDriverWait wait, WebDriver driver, String WorkspaceUrl, String OptimizerUrl, String arr[],String username, String PASSwd , String tcid, int NoOfOrderToBeCreated ,int r, int nextRow, String DriverSheetPath,String SheetName,String ResultPath, XWPFRun xwpfRun) {
	
	
	boolean res= false;	
	try{
		
		
		
		/*
		driver.findElement(By.xpath("//div[contains(text(), 'Order proposal sending JSON [NT 23-8]')]")).click();		
		
		String relex_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";
		TakeScreenShot.saveScreenShot(relex_sc3, driver);
		utilityFileWriteOP.writeToLog(tcid, "Order proposal sending JSON clicked", "PASS",ResultPath,xwpfRun);*/
		driver.navigate().to(WorkspaceUrl);
		//driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		wait.until(ExpectedConditions.visibilityOfAllElements(driver.findElements(By.xpath("//table[@class='flame-table']/tbody/tr"))));
		int rowCount=driver.findElements(By.xpath("//table[@class='flame-table']/tbody/tr")).size();
		System.out.println(rowCount);
		ArrayList<String> Oid = new ArrayList<String>();
		Thread.sleep(2000);
		
	
		int count=0;
		
		
		//driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		
		for(int m=2; m<rowCount;m++)
		{
			
			String status=driver.findElement(By.xpath("//table[@class='flame-table']/tbody/tr["+m+"]/td[8]")).getText();
			System.out.println(status);
			if(status.contains("No"))
			{
				
				driver.findElement(By.xpath("//table[@class='flame-table']/tbody/tr["+m+"]/td[1]")).click();
				String orderNo=driver.findElement(By.xpath("//table[@class='flame-table']/tbody/tr["+m+"]/td[1]")).getText();
				Oid.add(orderNo);	
				driver.findElement(By.xpath("//table[@class='flame-table']/tbody/tr["+m+"]/td[8]")).click();

				Robot rb=new Robot();
				rb.keyPress(KeyEvent.VK_ENTER);
				rb.keyRelease(KeyEvent.VK_ENTER);
				
				rb.keyPress(KeyEvent.VK_DOWN);
				rb.keyRelease(KeyEvent.VK_DOWN);
				
				rb.keyPress(KeyEvent.VK_DOWN);
				rb.keyRelease(KeyEvent.VK_DOWN);
				
				rb.keyPress(KeyEvent.VK_ENTER);
				rb.keyRelease(KeyEvent.VK_ENTER);
						
				Thread.sleep(2000);
				count++;
				
				utilityFileWriteOP.writeToLog(tcid, "Order number accepted", orderNo, ResultPath,xwpfRun,"");				
				String AfterLogin1=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(AfterLogin1, driver);

				if(count==NoOfOrderToBeCreated)
				{
					break;
				}
			}
		}
		
		
		
		driver.findElement(By.xpath("//span[@class='working-copy']")).click();
		
		
		String AfterLogin1=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin1, driver);
		utilityFileWriteOP.writeToLog(tcid, "no of changes clicked", "PASS",ResultPath,xwpfRun);
		
		driver.findElement(By.xpath("//label[contains(text(), 'Accept All')]")).click();
		String AfterLogin11=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin11, driver);
		utilityFileWriteOP.writeToLog(tcid, "All changes are accepted", "PASS",ResultPath,xwpfRun);
		Thread.sleep(3000);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		/*wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@class='_3tCQF' and text() = 'Optimizer']")));
		
		driver.findElement(By.xpath("//label[@class='_3tCQF' and text() = 'Optimizer']")).click();
		
		String AfterLogin2=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin2, driver);
		utilityFileWriteOP.writeToLog(tcid, "Optimizer button clicked", "PASS",ResultPath, xwpfRun,"");
		Thread.sleep(1000);		*/

		//driver.findElement(By.xpath("(//span[@class='folder-name' and text()='JSON'])[2]")).click();	
		driver.navigate().to(OptimizerUrl);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@class='_3tCQF' and text()='Start']")));
/*//		driver.findElement(By.xpath("(//span[@class='folder-name' and text()='Testing'])[2]")).click();
		String AfterLogin3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin3, driver);
		utilityFileWriteOP.writeToLog(tcid, "Testing Clicked", "PASS",ResultPath,xwpfRun);
//		driver.findElement(By.xpath("//div[contains(text(), 'Order proposal writer (JSON)')]")).click();
		
		
		
		String AfterLogin4=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin4, driver);
		utilityFileWriteOP.writeToLog(tcid, "Order proposal writer (JSON) Clicked", "PASS",ResultPath,xwpfRun);*/
		driver.findElement(By.xpath("//label[@class='_3tCQF' and text()='Start']")).click();		
		

		String AfterLogin5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin5, driver);
		utilityFileWriteOP.writeToLog(tcid, "Start button Clicked", "PASS",ResultPath,xwpfRun);
		
		
		
		String jobid=driver.findElement(By.xpath(".//div[@class = 'bubble-id']")).getText();
		
		System.out.println("job id is: "+ jobid);
		arr[0]=jobid;
		excelCellValueWrite.writeValueToCell(jobid, r, 30, DriverSheetPath, SheetName);
		
		excelCellValueWrite.writeValueToCell(jobid, nextRow, 30, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(jobid, nextRow+1, 30, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(jobid, nextRow+2, 30, DriverSheetPath, SheetName);
		
		excelCellValueWrite.writeValueToCell(jobid, nextRow+3, 30, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(jobid, nextRow+4, 30, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(jobid, nextRow+5, 30, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(jobid, nextRow+6, 30, DriverSheetPath, SheetName);
		
		
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[contains(text(), 'Queued')]")));
		
		int i=0;
		Thread.sleep(1000);
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);	
		int j=0;
		while(i==0)
		{
			j++;
			Thread.sleep(3000);
			System.out.println("Waiting for job to complete");
			i=driver.findElements(By.xpath("//div[@class = '_2DqB0 KSjA4 YEzHN']")).size();
			if (j==200)
			{
				throw new MyException("Test Stopped Because Job status is not Finished. Job is running for more than 10 minutes.");
			}
		}
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);	
		driver.findElement(By.xpath("//div[@class = '_2DqB0 KSjA4 YEzHN']")).click();
		
		utilityFileWriteOP.writeToLog(tcid, "Job finished", "PASS",ResultPath,xwpfRun,"");
		utilityFileWriteOP.writeToLog(tcid, "jobid", jobid,ResultPath,xwpfRun,"");
	
		
		String finaljobid= jobid.substring(1);
		
		driver.findElement(By.xpath("//label[@class='_3tCQF' and text() = 'Monitoring']")).click();				
		Thread.sleep(2000);	
		
		String AfterLogin6=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin6, driver);
		utilityFileWriteOP.writeToLog(tcid, "Clicked on Monitoring", "PASS",ResultPath,xwpfRun);
		driver.findElement(By.xpath("//a[contains(text(),'job.log."+finaljobid+"')]")).click();	
		
		ArrayList<String> alltabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(alltabs.get(1));	
		
		String BodyText = driver.findElement(By.xpath("//pre[contains(text(), 'with job ID')]")).getText();
		//String BodyText = driver.findElement(By.xpath("/html/body/pre")).getText();
		String JsonJobID = BodyText.substring(BodyText.indexOf("with job ID") + 12, BodyText.indexOf("with job ID") + 48);
		System.out.println(JsonJobID);
		String BodyText2 =driver.findElement(By.xpath("//pre[contains(text(), 'with job ID')]")).getText();
		String Filename =JsonJobID + "." + BodyText2.substring(BodyText.indexOf("Sent file OrderProposal") + 10,BodyText2.indexOf("Sent file OrderProposal") + 55);
		System.out.println(Filename);
		String AfterLogin7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin7, driver);
		utilityFileWriteOP.writeToLog(tcid, "Job log clicked and found json job id which is "+ JsonJobID, "PASS",ResultPath,xwpfRun);
		utilityFileWriteOP.writeToLog(tcid, "Found file name which is "+ Filename, "PASS",ResultPath,xwpfRun);
		excelCellValueWrite.writeValueToCell(JsonJobID, r, 24, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(Filename, r, 31, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(JsonJobID, nextRow, 24, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(Filename, nextRow, 31, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(JsonJobID, nextRow+2, 24, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(Filename, nextRow+2, 31, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(JsonJobID, nextRow+5, 24, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(Filename, nextRow+5, 31, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(JsonJobID, nextRow+6, 24, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(Filename, nextRow+6, 31, DriverSheetPath, SheetName);
		driver.switchTo().window(alltabs.get(0));
		
		
		String final_order = "";
		
		for (String Order:Oid)
		{			String orderid = Order.replaceAll(",", "");
						final_order =final_order + orderid + ",";
											
		}
		
		excelCellValueWrite.writeValueToCell(final_order, r, 25, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(final_order, nextRow, 25, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(final_order, nextRow+2, 25, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(final_order, nextRow+5, 25, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(final_order, nextRow+6, 25, DriverSheetPath, SheetName);
		
		res=true;
		
	
		return res;
	
	} catch (Exception e) {
		
		res=false;
		e.printStackTrace();
		
		String AfterLogin=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin, driver);
		utilityFileWriteOP.writeToLog(tcid, "Problem occured in Order Creation", "Fail",ResultPath,xwpfRun);
		return res;
	}
	
	
	
	
}




public static boolean ValidateOrderIDinRelex(WebDriverWait wait, WebDriver driver, String URL, String AllOrderIds , String tcid, int r, String DriverSheetPath,String SheetName,String ResultPath,XWPFRun xwpfRun) {
	
	
	boolean res= false;	
	
	String[] OrderID = AllOrderIds.split(",");
	
	
	
	try
	{
		
		
	driver.findElement(By.xpath("//label[contains(text(),'Workspace')]")).click();
	

	/*Actions actions = new Actions(driver);
	WebElement leftpanel = driver.findElement(By.xpath("//div[@class='ember-view scroll-view flame-view']"));
	actions.moveToElement(leftpanel);
	
	actions.click().build().perform();*/
	
	//driver.findElement(By.xpath("//div[@class='ember-view scroll-view flame-view']")).sendKeys(Keys.END);
	
	Thread.sleep(2000);
	
	//driver.findElement(By.xpath("//span[@class='folder-name' and contains(text(),'Testing')]")).click();
	
	driver.get(URL+"#workspace/po-response");
	
	/*JavascriptExecutor executor = (JavascriptExecutor)driver;
	executor.executeScript("arguments[0].click();", driver.findElement(By.xpath("//span[@class='folder-name' and contains(text(),'Testing')]")));
	
	//driver.findElement(By.xpath("//span[@class='folder-name' and contains(text(),'Testing')]")).sendKeys(Keys.END);
	
	Thread.sleep(2000);
	driver.findElement(By.xpath("//div[contains(text(),'po response')]")).click();
	
	JavascriptExecutor executor2 = (JavascriptExecutor)driver;
	executor2.executeScript("arguments[0].click();", driver.findElement(By.xpath("//div[contains(text(),'po response')]")));
	
*/	
	//System.out.println("3");
	
	//wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//label[contains(text(),'Search')]"))));
	
	Thread.sleep(7000);
	
	String AfterLogin4=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
	TakeScreenShot.saveScreenShot(AfterLogin4, driver);
	utilityFileWriteOP.writeToLog(tcid, "PO Response Clicked", "PASS",ResultPath,xwpfRun);
	
	int passflag = 0;
	
	for(int j=0; j<(OrderID.length); j++) {
		
		//System.out.println(OrderID[j]);
		
		String OrderIDwithoutblank = OrderID[j].replaceAll(" ", "");
		//System.out.println(OrderIDwithoutblank);
		
		driver.findElement(By.xpath("(//input[contains(@class,'ember-view ember-text-field')])[4]")).clear();
		Thread.sleep(2000);
		driver.findElement(By.xpath("(//input[contains(@class,'ember-view ember-text-field')])[4]")).sendKeys(OrderIDwithoutblank);
		Thread.sleep(2000);
		driver.findElement(By.xpath("(//input[contains(@class,'ember-view ember-text-field')])[4]")).sendKeys(Keys.RETURN);

		
		
		//driver.findElement(By.xpath("//label[contains(text(),'Search')]")).click();
		//driver.findElement(By.xpath("//div[contains(@class,'ember-view flame-button-view') and contains(text(),'Search')]"));
		
		Thread.sleep(3000);
		
		ArrayList<WebElement> allrowsoftable = (ArrayList<WebElement>) driver.findElements(By.xpath("//table[@class='flame-table']/tbody/tr"));
		
		
		if(allrowsoftable.size()<=1) {
			Reporting_Utilities.writeStepToHTMLLog(tcid, "2", "Validate PO Response for Order", "Order ID "+ OrderIDwithoutblank +" not found ", "FAIL", ResultPath);
			passflag = -1;
		}
		
		
		int allrowsforoneOrderFlag = 0;

		
		
		ArrayList<WebElement> allcolumnsofrow = null;
		
		//System.out.println("size of table rows "+allrowsoftable.size());
		
		for(int k=1; k<allrowsoftable.size(); k++) {
			//System.out.println("inside loop");
			try {
				allcolumnsofrow = (ArrayList<WebElement>) driver.findElements(By.xpath("(//table[@class='flame-table']/tbody/tr)["+(k+1)+"]/td"));

				
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				String ResponseMessege=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(ResponseMessege, driver);
				utilityFileWriteOP.writeToLog(tcid, "Order ID "+OrderIDwithoutblank+" not found", "FAIL",ResultPath,xwpfRun);
				Reporting_Utilities.writeStepToHTMLLog(tcid, "2", "Validate PO Response for Order", "Order ID "+ OrderIDwithoutblank +" not found ", "FAIL", ResultPath);

			}
			
			//System.out.println(allcolumnsofrow.get(0).getText().trim().equals(OrderIDwithoutblank));
			//System.out.println(allcolumnsofrow.get(10).getText().trim().equals("Processed okay"));
			
			if( (allcolumnsofrow.get(0).getText().trim().equals(OrderIDwithoutblank)) &&  (allcolumnsofrow.get(10).getText().trim().equals("Processed okay")) ) {
				
				String ResponseMessege=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(ResponseMessege, driver);
				utilityFileWriteOP.writeToLog(tcid, "Order ID "+OrderIDwithoutblank+" Processed "
						+ ""
						+ "okay", "PASS",ResultPath,xwpfRun);
				
				//System.out.println("passed for row "+k+" order "+OrderID[j]);
				allrowsforoneOrderFlag++;
				
			}else {
				
				String ResponseMessege=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(ResponseMessege, driver);
				utilityFileWriteOP.writeToLog(tcid, "Order ID "+OrderIDwithoutblank+" not Processed okay. Response is "+allcolumnsofrow.get(10).getText().trim(), "FAIL",ResultPath,xwpfRun);
				
				Reporting_Utilities.writeStepToHTMLLog(tcid, "2", "Validate PO Response for Order", "Response Received as "+allcolumnsofrow.get(10).getText().trim()+" for Order ID "+OrderIDwithoutblank, "FAIL", ResultPath);

				
			}
			
			
		}
		
		//System.out.println(allrowsforoneOrderFlag+ " "+allrowsoftable.size());
		if(allrowsforoneOrderFlag==allrowsoftable.size()-1) {
			passflag++;
		}
		
	}
	
	
	//System.out.println("passflag "+passflag+" and "+OrderID.length);
	
	if(passflag==OrderID.length) {
		
		 Reporting_Utilities.writeStepToHTMLLog(tcid, "2", "Validate PO Response for Order", "Response Received as Processed Okay for orders "+AllOrderIds.replaceAll(" ", ""), "PASS", ResultPath);

		 res=true;
	}
	
	
	//System.out.println("4");
	
	
	
				
	

} catch (Exception e) {
	// TODO: handle exception
	e.printStackTrace();
	
	String relex_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";
	utilityFileWriteOP.writeToLog(tcid, "Problem occured in Searching for PO Response", "Fail",ResultPath,xwpfRun);
	TakeScreenShot.saveScreenShot(relex_sc3, driver);
	res = false;
	return res;
}



return res;
}




public static boolean ValidateOrderIDinRelexOrdAmend(WebDriverWait wait, WebDriver driver, String URL, String AllOrderIds , String tcid, int r, String DriverSheetPath,String SheetName,String ResultPath,XWPFRun xwpfRun) {
	
	
	boolean res= false;	
	
	String[] OrderID = AllOrderIds.split(",");
	
	
	
	try
	{
		
		
	driver.findElement(By.xpath("//label[contains(text(),'Workspace')]")).click();
	

	/*Actions actions = new Actions(driver);
	WebElement leftpanel = driver.findElement(By.xpath("//div[@class='ember-view scroll-view flame-view']"));
	actions.moveToElement(leftpanel);
	
	actions.click().build().perform();*/
	
	//driver.findElement(By.xpath("//div[@class='ember-view scroll-view flame-view']")).sendKeys(Keys.END);
	
	Thread.sleep(2000);
	
	//driver.findElement(By.xpath("//span[@class='folder-name' and contains(text(),'Testing')]")).click();
	
	driver.get(URL+"#workspace/po-response");
	
	/*JavascriptExecutor executor = (JavascriptExecutor)driver;
	executor.executeScript("arguments[0].click();", driver.findElement(By.xpath("//span[@class='folder-name' and contains(text(),'Testing')]")));
	
	//driver.findElement(By.xpath("//span[@class='folder-name' and contains(text(),'Testing')]")).sendKeys(Keys.END);
	
	Thread.sleep(2000);
	driver.findElement(By.xpath("//div[contains(text(),'po response')]")).click();
	
	JavascriptExecutor executor2 = (JavascriptExecutor)driver;
	executor2.executeScript("arguments[0].click();", driver.findElement(By.xpath("//div[contains(text(),'po response')]")));
	
*/	
	//System.out.println("3");
	
	//wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//label[contains(text(),'Search')]"))));
	
	Thread.sleep(7000);
	
	String AfterLogin4=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
	TakeScreenShot.saveScreenShot(AfterLogin4, driver);
	utilityFileWriteOP.writeToLog(tcid, "PO Response Clicked", "PASS",ResultPath,xwpfRun);
	
	int passflag = 0;
	
	for(int j=0; j<(OrderID.length); j++) {
		
		//System.out.println(OrderID[j]);
		
		String OrderIDwithoutblank = OrderID[j].replaceAll(" ", "");
		//System.out.println(OrderIDwithoutblank);
		
		driver.findElement(By.xpath("(//input[contains(@class,'ember-view ember-text-field')])[4]")).clear();
		Thread.sleep(2000);
		driver.findElement(By.xpath("(//input[contains(@class,'ember-view ember-text-field')])[4]")).sendKeys(OrderIDwithoutblank);
		Thread.sleep(2000);
		driver.findElement(By.xpath("(//input[contains(@class,'ember-view ember-text-field')])[4]")).sendKeys(Keys.RETURN);

		
		
		//driver.findElement(By.xpath("//label[contains(text(),'Search')]")).click();
		//driver.findElement(By.xpath("//div[contains(@class,'ember-view flame-button-view') and contains(text(),'Search')]"));
		
		Thread.sleep(3000);
		
		ArrayList<WebElement> allrowsoftable = (ArrayList<WebElement>) driver.findElements(By.xpath("//table[@class='flame-table']/tbody/tr"));
		
		
		if(allrowsoftable.size()<=1) {
			Reporting_Utilities.writeStepToHTMLLog(tcid, "2", "Validate PO Response for Order", "Order ID "+ OrderIDwithoutblank +" not found ", "FAIL", ResultPath);
			passflag = -1;
		}
		
		
		int allrowsforoneOrderFlag = 0;

		
		
		ArrayList<WebElement> allcolumnsofrow = null;
		
		//System.out.println("size of table rows "+allrowsoftable.size());
		
		for(int k=1; k<allrowsoftable.size(); k++) {
			//System.out.println("inside loop");
			try {
				allcolumnsofrow = (ArrayList<WebElement>) driver.findElements(By.xpath("(//table[@class='flame-table']/tbody/tr)["+(k+1)+"]/td"));

				
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				String ResponseMessege=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(ResponseMessege, driver);
				utilityFileWriteOP.writeToLog(tcid, "Order ID "+OrderIDwithoutblank+" not found", "FAIL",ResultPath,xwpfRun);
				Reporting_Utilities.writeStepToHTMLLog(tcid, "2", "Validate PO Response for Order", "Order ID "+ OrderIDwithoutblank +" not found ", "FAIL", ResultPath);

			}
			
			//Amendment sent
			if( (allcolumnsofrow.get(0).getText().trim().equals(OrderIDwithoutblank)) &&  (allcolumnsofrow.get(10).getText().trim().toUpperCase().contains("PROCESSED OK")) ) {
				
				String ResponseMessege=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(ResponseMessege, driver);
				utilityFileWriteOP.writeToLog(tcid, "Order IDs "+OrderIDwithoutblank+" are "+allcolumnsofrow.get(10).getText().trim(), "PASS",ResultPath,xwpfRun);
				
				//System.out.println("passed for row "+k+" order "+OrderID[j]);
				allrowsforoneOrderFlag++;
				
			}else {
				
				String ResponseMessege=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(ResponseMessege, driver);
				utilityFileWriteOP.writeToLog(tcid, "Order ID "+OrderIDwithoutblank+" not PROCESSED OK/Processed okay. Response is "+allcolumnsofrow.get(10).getText().trim(), "FAIL",ResultPath,xwpfRun);
				
				Reporting_Utilities.writeStepToHTMLLog(tcid, "2", "Validate PO Response for Order", "Response Received as "+allcolumnsofrow.get(10).getText().trim()+" for Order ID "+OrderIDwithoutblank, "FAIL", ResultPath);

				
			}
			
			
		}
		
		//System.out.println(allrowsforoneOrderFlag+ " "+allrowsoftable.size());
		if(allrowsforoneOrderFlag==allrowsoftable.size()-1) {
			passflag++;
		}
		
	}
	
	
	
	if(passflag==OrderID.length) {
		
		 Reporting_Utilities.writeStepToHTMLLog(tcid, "2", "Validate PO Response for Order", "Response Received as PROCESSED OK/Processd okay for orders "+AllOrderIds.replaceAll(" ", ""), "PASS", ResultPath);

		 res=true;
	}
	
	
	//System.out.println("4");
	
	
	
				
	

} catch (Exception e) {
	// TODO: handle exception
	e.printStackTrace();
	
	String relex_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";
	utilityFileWriteOP.writeToLog(tcid, "Problem occured in Searching for PO Response", "Fail",ResultPath,xwpfRun);
	TakeScreenShot.saveScreenShot(relex_sc3, driver);
	res = false;
	return res;
}



return res;
}




public static boolean RelexRackspaceAmendOrder (WebDriverWait wait, WebDriver driver,String arr[],String username, String PASSwd , String tcid, int NoOfOrderToBeCreated ,int r, int nextRow, String DriverSheetPath,String SheetName,String ResultPath, XWPFRun xwpfRun) {
	
	
	boolean res= false;	
	try{

		wait.until(ExpectedConditions.visibilityOfAllElements(driver.findElements(By.xpath("//table[@class='flame-table']/tbody/tr"))));
		driver.findElement(By.xpath("//table[@class='flame-table']/tbody/tr[1]/td[1]")).click();
		driver.findElement(By.xpath("//table[@class='flame-table']/tbody/tr[1]/td[11]")).click();

		driver.findElement(By.xpath("(//div[@class='header-container'])[1]//table/tbody/tr/td[11]")).click();
		Thread.sleep(5000);

		driver.findElement(By.xpath("//div[contains(text(),'Sort descending...')]")).click();
		Thread.sleep(1000);
		//wait.until(ExpectedConditions.visibilityOfAllElements(driver.findElements(By.xpath("//table[@class='flame-table']/tbody/tr"))));
		int rowCount=driver.findElements(By.xpath("//table[@class='flame-table']/tbody/tr")).size();
		System.out.println(rowCount);
		ArrayList<String> Oid = new ArrayList<String>();
		Thread.sleep(5000);
		int count=0;
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		//String TempOrderNos="";  //tanmay
		for(int m=2; m<rowCount;m++)
		{
			
			String status=driver.findElement(By.xpath("//table[@class='flame-table']/tbody/tr["+m+"]/td[11]")).getText();
			System.out.println(status);
			if((status.contains("Processed okay")))
			{
				
				String orderNo=driver.findElement(By.xpath("//table[@class='flame-table']/tbody/tr["+m+"]/td[1]")).getText();
				Oid.add(orderNo);	
				System.out.println(orderNo);
				count++;
				int[] x={1,4,7,10};
				int[] y={2,5,8,11};
				int[] z={3,6,9,12};
				int index1=Arrays.binarySearch(x, count);
				int index2=Arrays.binarySearch(y, count);
				int index3=Arrays.binarySearch(z, count);
				
				if(index1>=0)
				{
					
					driver.findElement(By.xpath("//table[@class='flame-table']/tbody/tr["+m+"]/td[1]")).click();
					String Text=driver.findElement(By.xpath("//table[@class='flame-table']/tbody/tr["+m+"]/td[13]")).getText();
					Thread.sleep(2000);
					DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
					Date ReceivedDate = sdf.parse(Text);
					
					Calendar c = Calendar.getInstance();
					c.setTime(ReceivedDate); // Now use today date.
					c.add(Calendar.DATE, 2); // Adding 5 days
					String output = sdf.format(c.getTime());
					System.out.println(output);
				
					driver.findElement(By.xpath("//table[@class='flame-table']/tbody/tr["+m+"]/td[14]")).click();
				
					Robot rb2=new Robot();
					rb2.keyPress(KeyEvent.VK_ENTER);
					rb2.keyRelease(KeyEvent.VK_ENTER);
					
					driver.findElement(By.xpath("//input[@placeholder='dd/mm/yyyy']")).sendKeys(output);
					
					rb2.keyPress(KeyEvent.VK_ENTER);
					rb2.keyRelease(KeyEvent.VK_ENTER);
				}
				else if(index2>=0)
				{
					Thread.sleep(5000);
					
					driver.findElement(By.xpath("//table[@class='flame-table']/tbody/tr["+m+"]/td[1]")).click();
					String Cquantity ="4";
					driver.findElement(By.xpath("//table[@class='flame-table']/tbody/tr["+m+"]/td[15]")).click();
					Actions action=new Actions(driver).doubleClick();
					action.build().perform();
					driver.findElement(By.xpath("//input[@class='table-edit-field']")).clear();
					driver.findElement(By.xpath("//input[@class='table-edit-field']")).sendKeys(Cquantity);
					
					
				}
				else if(index3>=0)
				{
					Thread.sleep(5000);
					driver.findElement(By.xpath("//table[@class='flame-table']/tbody/tr["+m+"]/td[1]")).click();
					driver.findElement(By.xpath("//table[@class='flame-table']/tbody/tr["+m+"]/td[16]")).click();
					driver.findElement(By.xpath("//table[@class='flame-table']/tbody/tr["+m+"]/td[16]")).click();
				
					
				
					Robot rb1=new Robot();
					rb1.keyPress(KeyEvent.VK_DOWN);
					rb1.keyRelease(KeyEvent.VK_DOWN);
					
					rb1.keyPress(KeyEvent.VK_DOWN);
					rb1.keyRelease(KeyEvent.VK_DOWN);
					
					rb1.keyPress(KeyEvent.VK_ENTER);
					rb1.keyRelease(KeyEvent.VK_ENTER);
					Thread.sleep(3000);
				}
				
	
				
							
				String AfterLogin1=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
				TakeScreenShot.saveScreenShot(AfterLogin1, driver);
				utilityFileWriteOP.writeToLog(tcid, "Order number accepted", orderNo, ResultPath,xwpfRun);	
				if(count==NoOfOrderToBeCreated)
				{
					break;
				}
			}
		}
		Thread.sleep(10000);
		
		
		driver.findElement(By.xpath("//span[@class='working-copy']")).click();
		
		
		String AfterLogin1=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin1, driver);
		utilityFileWriteOP.writeToLog(tcid, "Clicked on Accepts changes", "Pass" ,ResultPath,xwpfRun);
		
		driver.findElement(By.xpath("//label[contains(text(), 'Accept All')]")).click();
		
		
		String AfterLogin11=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin11, driver);
		utilityFileWriteOP.writeToLog(tcid, "Clicked on Accept All button", "Pass" ,ResultPath,xwpfRun);
		Thread.sleep(5000);
		
		
		
		driver.findElement(By.xpath("//label[@class='_3tCQF' and text() = 'Optimizer']")).click();
		String AfterLogin2=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin2, driver);
		utilityFileWriteOP.writeToLog(tcid, "Optimizer button clicked", "PASS",ResultPath,xwpfRun);
		Thread.sleep(6000);	
		
		System.out.println("testing to click");

		//driver.findElement(By.xpath("(//span[@class='folder-name' and text()='JSON'])[2]")).click();	
		
		/*driver.findElement(By.xpath("(//span[@class='folder-name' and text()='Testing'])[1]")).click();					
		String AfterLogin3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin3, driver);*/
		utilityFileWriteOP.writeToLog(tcid, "Testing Clicked", "PASS",ResultPath,xwpfRun);
		
		driver.get(ProjectConfigurations.LoadProperties("Relex_ProjectURL")+"#optimizer/4294974415"); //tanmay
		System.out.println("Testing clicked");

		driver.findElement(By.xpath("//div[contains(text(), 'Send out amendments (JSON)')]")).click();		
		String AfterLogin4=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin4, driver);
		utilityFileWriteOP.writeToLog(tcid, " Clicked Send out amendments (JSON)", "PASS",ResultPath,xwpfRun);
		driver.findElement(By.xpath("//label[@class='_3tCQF' and text()='Start']")).click();			
		String AfterLogin5=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin5, driver);
		utilityFileWriteOP.writeToLog(tcid, "Start button Clicked", "PASS",ResultPath,xwpfRun);

		
		String jobid=driver.findElement(By.xpath(".//div[@class = 'bubble-id']")).getText();
		
		System.out.println("job id is: "+ jobid);
		arr[0]=jobid;
		
		//--start added by Tanmay to write OrderID----
		String stringToWrite="";
		for(int kk=0;kk<Oid.size();kk++)
		{
			String s=Oid.get(kk);
			if(kk==0)
			{
				stringToWrite=s;
			}
			else
			{
				stringToWrite=stringToWrite+","+s;
			}
			
		}
		excelCellValueWrite.writeValueToCell(stringToWrite, r, 31, DriverSheetPath, SheetName);		
		excelCellValueWrite.writeValueToCell(stringToWrite, nextRow, 31, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(stringToWrite, nextRow+1, 31, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(stringToWrite, nextRow+2, 31, DriverSheetPath, SheetName);		
		excelCellValueWrite.writeValueToCell(stringToWrite, nextRow+3, 31, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(stringToWrite, nextRow+4, 31, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(stringToWrite, nextRow+5, 31, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(stringToWrite, nextRow+6, 31, DriverSheetPath, SheetName);	
		
		//--end added by Tanmay to write OrderID----
		excelCellValueWrite.writeValueToCell(jobid, r, 30, DriverSheetPath, SheetName);
		//excelCellValueWrite.writeValueToCell(jobid, 2, 30, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(jobid, nextRow, 30, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(jobid, nextRow+1, 30, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(jobid, nextRow+2, 30, DriverSheetPath, SheetName);
		
		excelCellValueWrite.writeValueToCell(jobid, nextRow+3, 30, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(jobid, nextRow+4, 30, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(jobid, nextRow+5, 30, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(jobid, nextRow+6, 30, DriverSheetPath, SheetName);
	
		
		utilityFileWriteOP.writeToLog(tcid, "Job finished", "PASS",ResultPath,xwpfRun,"");
		String AfterLogin6=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin6, driver);
		utilityFileWriteOP.writeToLog(tcid, "jobid", jobid,ResultPath,xwpfRun);
		
		String finaljobid= jobid.substring(1);
		int i=0;
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		while(i==0)
		{
			Thread.sleep(3000);
			System.out.println("Waiting for job to complete");
			i=driver.findElements(By.xpath("//div[@class = '_2DqB0 KSjA4 YEzHN']")).size();
		}
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//div[@class='_2DqB0 KSjA4 YEzHN']")).click();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		driver.findElement(By.xpath("//label[@class='_3tCQF' and text() = 'Monitoring']")).click();				
		Thread.sleep(3000);	
		String AfterLogin7=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin7, driver);
		utilityFileWriteOP.writeToLog(tcid, "Clicked on Monitoring button", "PASS",ResultPath,xwpfRun);
		driver.findElement(By.xpath("//a[contains(text(),'job.log."+finaljobid+"')]")).click();	
		String AfterLogin8=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin8, driver);
		utilityFileWriteOP.writeToLog(tcid, "Clicked on Job Log", "PASS",ResultPath,xwpfRun);
		ArrayList<String> alltabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(alltabs.get(1));	
		Thread.sleep(3000);
		String BodyText=driver.findElement(By.xpath("/html/body/pre")).getText();
		//String BodyText = driver.findElement(By.xpath("//pre[contains(text(), 'with job ID')]")).getText();
		String JsonJobID = BodyText.substring(BodyText.indexOf("with job ID") + 12, BodyText.indexOf("with job ID") + 48);
		System.out.println(JsonJobID);
		String BodyText2 =driver.findElement(By.xpath("/html/body/pre")).getText();
		String Filename =JsonJobID + "." + BodyText2.substring(BodyText.indexOf("Sent file OrderAmend") + 10,BodyText2.indexOf("Sent file OrderAmend") + 52);
		System.out.println(Filename);
		excelCellValueWrite.writeValueToCell(JsonJobID, r, 24, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(Filename, r, 31, DriverSheetPath, SheetName);
		/*excelCellValueWrite.writeValueToCell(JsonJobID,2 , 24, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(Filename, 2, 31, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(JsonJobID,1 , 24, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(Filename, 1, 31, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(JsonJobID,4 , 24, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(Filename, 4, 31, DriverSheetPath, SheetName);*/
		excelCellValueWrite.writeValueToCell(JsonJobID, nextRow, 24, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(Filename, nextRow, 31, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(JsonJobID, nextRow+2, 24, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(Filename, nextRow+2, 31, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(JsonJobID, nextRow+5, 24, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(Filename, nextRow+5, 31, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(JsonJobID, nextRow+6, 24, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(Filename, nextRow+6, 31, DriverSheetPath, SheetName);
		utilityFileWriteOP.writeToLog(tcid, "JsonJobID ", JsonJobID,ResultPath,xwpfRun,"");
		utilityFileWriteOP.writeToLog(tcid, "Filename ", Filename,ResultPath,xwpfRun,"");
		driver.switchTo().window(alltabs.get(0));
		
		
		
		
		
		
		
		String final_order = "";
		
		for (String Order:Oid)
		{
			
				String orderid = Order.replaceAll(",", "");
			
			
				final_order =final_order + orderid + ",";
			
											
		}
		
		
		
		excelCellValueWrite.writeValueToCell(final_order, nextRow, 25, DriverSheetPath, SheetName);
		//excelCellValueWrite.writeValueToCell(final_order, nextRow, 25, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(final_order, nextRow+2, 25, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(final_order, nextRow+5, 25, DriverSheetPath, SheetName);
		excelCellValueWrite.writeValueToCell(final_order, nextRow+6, 25, DriverSheetPath, SheetName);
		
		res=true;
		
	
	
	
	} catch (Exception e) {
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Problem occured in Order Creation", "Fail",ResultPath);
		String AfterLogin=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
		TakeScreenShot.saveScreenShot(AfterLogin, driver);
		
		return res;
	}
	
	
	
	return res;
}


//******To get the latest file extract generated after batch within 20 mins(latest file )


		public static String latestS3FileExtract_SFTP(String OrderId,String us, String pw,String oretailpath,String host, String port, String tcid,String ResultPath,XWPFRun xwpfRun,ExtentTest logger) throws IOException {

			String res="";
			JSch jsch = new JSch();
			Session session = null;
			BufferedReader br = null;
			String Result = null;
			int flag = 0;

			long lastmodifiedfiletime = 0;
			String lastmodifiedfilename = null;

			try {

				session = jsch.getSession(us, host, Integer.parseInt(port));
				session.setConfig("StrictHostKeyChecking", "no");
				session.setPassword(pw);
				session.connect();
				System.out.println("Connection successful");
				Channel channel = session.openChannel("sftp");
				channel.connect();
				System.out.println("Channel Connection successful");
				ChannelSftp sftpchannel = (ChannelSftp) channel;


				sftpchannel.cd(oretailpath);

				System.out.println("The Current path is " + sftpchannel.pwd());

				//w-mcc-OrderID.csv

				Vector<ChannelSftp.LsEntry> list = sftpchannel.ls("w-mcc-"+OrderId+"*.csv");
				System.out.println("List"+list.size());
				String filename="";
				
				int p=0;
				
				for(ChannelSftp.LsEntry entry: list) {

					 filename = entry.getFilename();
					
					System.out.println("The current file is " + filename);
					
					//Getting the current date
					Date time=new Date();
					 //This method returns the time in millis
					long timeMilli = time.getTime();
					
					System.out.println("The current time in miliseconds:"+timeMilli);
					
					Calendar calendar = Calendar.getInstance();
					System.out.println("Current Date = " + calendar.getTime());
					System.out.println("Time in milliseconds before= "+(long)calendar.getTimeInMillis());
					
					
				  // Subtract 15 minutes from current date
					Calendar calendar1 = Calendar.getInstance();
				      calendar1.add(Calendar.MINUTE, -105);
				      System.out.println("Updated Date = " + calendar1.getTime());
				      System.out.println("Time in milliseconds after = "+(long)calendar1.getTimeInMillis());

					
		long currentfilelastmodifiedmilliseconds =(long) entry.getAttrs().getMTime()*1000L;
					
					
					System.out.println("The current file time is:"+currentfilelastmodifiedmilliseconds);

					
					
		if(currentfilelastmodifiedmilliseconds < calendar.getTimeInMillis() && calendar1.getTimeInMillis()<currentfilelastmodifiedmilliseconds){
						
						
						lastmodifiedfiletime = currentfilelastmodifiedmilliseconds;
						lastmodifiedfilename = filename;
						res=filename;
					//	utilityFileWriteOP.writeToLog(tcid, "The target file name in "+oretailpath, " is "+lastmodifiedfilename, ResultPath, xwpfRun,"");
						
						System.out.println("The file is :"+filename);
						
						break;
					}
					else{
						
						System.out.println("There are no file generated recently");
						
						res=null;
					}
				}
						
				//br.close();
				sftpchannel.exit();

				session.disconnect();

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				System.out.println(e);
			//	utilityFileWriteOP.writeToLog(tcid, "Error occurred during File Extraction ", "Due to :"+e);
				return res;
			}


			return res;
		}
		


		public static String latestS3FileExtract_SFTP_harvest(String OrderId,String us, String pw,String oretailpath,String host, String port, String tcid,String ResultPath,XWPFRun xwpfRun,ExtentTest logger) throws IOException {

			String res="";
			JSch jsch = new JSch();
			Session session = null;
			BufferedReader br = null;
			String Result = null;
			int flag = 0;

			long lastmodifiedfiletime = 0;
			String lastmodifiedfilename = null;

			try {

				session = jsch.getSession(us, host, Integer.parseInt(port));
				session.setConfig("StrictHostKeyChecking", "no");
				session.setPassword(pw);
				session.connect();
				System.out.println("Connection successful");
				Channel channel = session.openChannel("sftp");
				channel.connect();
				System.out.println("Channel Connection successful");
				ChannelSftp sftpchannel = (ChannelSftp) channel;


				sftpchannel.cd(oretailpath);

				System.out.println("The Current path is " + sftpchannel.pwd());

				//w-mcc-OrderID.csv

				Vector<ChannelSftp.LsEntry> list = sftpchannel.ls("w-har-"+OrderId+"*.csv");
				System.out.println("List"+list.size());
				String filename="";
				
				int p=0;
				
				for(ChannelSftp.LsEntry entry: list) {

					 filename = entry.getFilename();
					
					System.out.println("The current file is " + filename);
					
					//Getting the current date
					Date time=new Date();
					 //This method returns the time in millis
					long timeMilli = time.getTime();
					
					System.out.println("The current time in miliseconds:"+timeMilli);
					
					Calendar calendar = Calendar.getInstance();
					System.out.println("Current Date = " + calendar.getTime());
					System.out.println("Time in milliseconds before= "+(long)calendar.getTimeInMillis());
					
					
				  // Subtract 15 minutes from current date
					Calendar calendar1 = Calendar.getInstance();
				      calendar1.add(Calendar.MINUTE, -105);
				      System.out.println("Updated Date = " + calendar1.getTime());
				      System.out.println("Time in milliseconds after = "+(long)calendar1.getTimeInMillis());

					
		long currentfilelastmodifiedmilliseconds =(long) entry.getAttrs().getMTime()*1000L;
					
					
					System.out.println("The current file time is:"+currentfilelastmodifiedmilliseconds);

					
					
		if(currentfilelastmodifiedmilliseconds < calendar.getTimeInMillis() && calendar1.getTimeInMillis()<currentfilelastmodifiedmilliseconds){
						
						
						lastmodifiedfiletime = currentfilelastmodifiedmilliseconds;
						lastmodifiedfilename = filename;
						res=filename;
					//	utilityFileWriteOP.writeToLog(tcid, "The target file name in "+oretailpath, " is "+lastmodifiedfilename, ResultPath, xwpfRun,"");
						
						System.out.println("The file is :"+filename);
						
						break;
					}
					else{
						
						System.out.println("There are no file generated recently");
						
						res=null;
					}
				}
						
				//br.close();
				sftpchannel.exit();

				session.disconnect();

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				System.out.println(e);
			//	utilityFileWriteOP.writeToLog(tcid, "Error occurred during File Extraction ", "Due to :"+e);
				return res;
			}


			return res;
		}
		
		

		public static String latestS3FileExtract_SFTP_rontec(String OrderId,String us, String pw,String oretailpath,String host, String port, String tcid,String ResultPath,XWPFRun xwpfRun,ExtentTest logger) throws IOException {

			String res="";
			JSch jsch = new JSch();
			Session session = null;
			BufferedReader br = null;
			String Result = null;
			int flag = 0;

			long lastmodifiedfiletime = 0;
			String lastmodifiedfilename = null;

			try {

				session = jsch.getSession(us, host, Integer.parseInt(port));
				session.setConfig("StrictHostKeyChecking", "no");
				session.setPassword(pw);
				session.connect();
				System.out.println("Connection successful");
				Channel channel = session.openChannel("sftp");
				channel.connect();
				System.out.println("Channel Connection successful");
				ChannelSftp sftpchannel = (ChannelSftp) channel;


				sftpchannel.cd(oretailpath);

				System.out.println("The Current path is " + sftpchannel.pwd());

				//w-mcc-OrderID.csv

				Vector<ChannelSftp.LsEntry> list = sftpchannel.ls("w-ron-"+OrderId+"*.csv");
				System.out.println("List"+list.size());
				String filename="";
				
				int p=0;
				
				for(ChannelSftp.LsEntry entry: list) {

					 filename = entry.getFilename();
					
					System.out.println("The current file is " + filename);
					
					//Getting the current date
					Date time=new Date();
					 //This method returns the time in millis
					long timeMilli = time.getTime();
					
					System.out.println("The current time in miliseconds:"+timeMilli);
					
					Calendar calendar = Calendar.getInstance();
					System.out.println("Current Date = " + calendar.getTime());
					System.out.println("Time in milliseconds before= "+(long)calendar.getTimeInMillis());
					
					
				  // Subtract 15 minutes from current date
					Calendar calendar1 = Calendar.getInstance();
				      calendar1.add(Calendar.MINUTE, -105);
				      System.out.println("Updated Date = " + calendar1.getTime());
				      System.out.println("Time in milliseconds after = "+(long)calendar1.getTimeInMillis());

					
		long currentfilelastmodifiedmilliseconds =(long) entry.getAttrs().getMTime()*1000L;
					
					
					System.out.println("The current file time is:"+currentfilelastmodifiedmilliseconds);

					
					
		if(currentfilelastmodifiedmilliseconds < calendar.getTimeInMillis() && calendar1.getTimeInMillis()<currentfilelastmodifiedmilliseconds){
						
						
						lastmodifiedfiletime = currentfilelastmodifiedmilliseconds;
						lastmodifiedfilename = filename;
						res=filename;
					//	utilityFileWriteOP.writeToLog(tcid, "The target file name in "+oretailpath, " is "+lastmodifiedfilename, ResultPath, xwpfRun,"");
						
						System.out.println("The file is :"+filename);
						
						break;
					}
					else{
						
						System.out.println("There are no file generated recently");
						
						res=null;
					}
				}
						
				//br.close();
				sftpchannel.exit();

				session.disconnect();

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				System.out.println(e);
			//	utilityFileWriteOP.writeToLog(tcid, "Error occurred during File Extraction ", "Due to :"+e);
				return res;
			}


			return res;
		}
		

		public static String latestS3FileExtract_SFTP_sandpiper(String OrderId,String us, String pw,String oretailpath,String host, String port, String tcid,String ResultPath,XWPFRun xwpfRun,ExtentTest logger) throws IOException {

			String res="";
			JSch jsch = new JSch();
			Session session = null;
			BufferedReader br = null;
			String Result = null;
			int flag = 0;

			long lastmodifiedfiletime = 0;
			String lastmodifiedfilename = null;

			try {

				session = jsch.getSession(us, host, Integer.parseInt(port));
				session.setConfig("StrictHostKeyChecking", "no");
				session.setPassword(pw);
				session.connect();
				System.out.println("Connection successful");
				Channel channel = session.openChannel("sftp");
				channel.connect();
				System.out.println("Channel Connection successful");
				ChannelSftp sftpchannel = (ChannelSftp) channel;


				sftpchannel.cd(oretailpath);

				System.out.println("The Current path is " + sftpchannel.pwd());

				//w-mcc-OrderID.csv

				Vector<ChannelSftp.LsEntry> list = sftpchannel.ls("w-san-"+OrderId+"*.csv");
				System.out.println("List"+list.size());
				String filename="";
				
				int p=0;
				
				for(ChannelSftp.LsEntry entry: list) {

					 filename = entry.getFilename();
					
					System.out.println("The current file is " + filename);
					
					//Getting the current date
					Date time=new Date();
					 //This method returns the time in millis
					long timeMilli = time.getTime();
					
					System.out.println("The current time in miliseconds:"+timeMilli);
					
					Calendar calendar = Calendar.getInstance();
					System.out.println("Current Date = " + calendar.getTime());
					System.out.println("Time in milliseconds before= "+(long)calendar.getTimeInMillis());
					
					
				  // Subtract 15 minutes from current date
					Calendar calendar1 = Calendar.getInstance();
				      calendar1.add(Calendar.MINUTE, -105);
				      System.out.println("Updated Date = " + calendar1.getTime());
				      System.out.println("Time in milliseconds after = "+(long)calendar1.getTimeInMillis());

					
		long currentfilelastmodifiedmilliseconds =(long) entry.getAttrs().getMTime()*1000L;
					
					
					System.out.println("The current file time is:"+currentfilelastmodifiedmilliseconds);

					
					
		if(currentfilelastmodifiedmilliseconds < calendar.getTimeInMillis() && calendar1.getTimeInMillis()<currentfilelastmodifiedmilliseconds){
						
						
						lastmodifiedfiletime = currentfilelastmodifiedmilliseconds;
						lastmodifiedfilename = filename;
						res=filename;
					//	utilityFileWriteOP.writeToLog(tcid, "The target file name in "+oretailpath, " is "+lastmodifiedfilename, ResultPath, xwpfRun,"");
						
						System.out.println("The file is :"+filename);
						
						break;
					}
					else{
						
						System.out.println("There are no file generated recently");
						
						res=null;
					}
				}
						
				//br.close();
				sftpchannel.exit();

				session.disconnect();

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				System.out.println(e);
			//	utilityFileWriteOP.writeToLog(tcid, "Error occurred during File Extraction ", "Due to :"+e);
				return res;
			}


			return res;
		}
		

		public static String latestS3FileExtract_SFTP_rontecb(String OrderId,String us, String pw,String oretailpath,String host, String port, String tcid,String ResultPath,XWPFRun xwpfRun,ExtentTest logger) throws IOException {

			String res="";
			JSch jsch = new JSch();
			Session session = null;
			BufferedReader br = null;
			String Result = null;
			int flag = 0;

			long lastmodifiedfiletime = 0;
			String lastmodifiedfilename = null;

			try {

				session = jsch.getSession(us, host, Integer.parseInt(port));
				session.setConfig("StrictHostKeyChecking", "no");
				session.setPassword(pw);
				session.connect();
				System.out.println("Connection successful");
				Channel channel = session.openChannel("sftp");
				channel.connect();
				System.out.println("Channel Connection successful");
				ChannelSftp sftpchannel = (ChannelSftp) channel;


				sftpchannel.cd(oretailpath);

				System.out.println("The Current path is " + sftpchannel.pwd());

				//w-mcc-OrderID.csv

				Vector<ChannelSftp.LsEntry> list = sftpchannel.ls("w-rtb-"+OrderId+"*.csv");
				System.out.println("List"+list.size());
				String filename="";
				
				int p=0;
				
				for(ChannelSftp.LsEntry entry: list) {

					 filename = entry.getFilename();
					
					System.out.println("The current file is " + filename);
					
					//Getting the current date
					Date time=new Date();
					 //This method returns the time in millis
					long timeMilli = time.getTime();
					
					System.out.println("The current time in miliseconds:"+timeMilli);
					
					Calendar calendar = Calendar.getInstance();
					System.out.println("Current Date = " + calendar.getTime());
					System.out.println("Time in milliseconds before= "+(long)calendar.getTimeInMillis());
					
					
				  // Subtract 15 minutes from current date
					Calendar calendar1 = Calendar.getInstance();
				      calendar1.add(Calendar.MINUTE, -105);
				      System.out.println("Updated Date = " + calendar1.getTime());
				      System.out.println("Time in milliseconds after = "+(long)calendar1.getTimeInMillis());

					
		long currentfilelastmodifiedmilliseconds =(long) entry.getAttrs().getMTime()*1000L;
					
					
					System.out.println("The current file time is:"+currentfilelastmodifiedmilliseconds);

					
					
		if(currentfilelastmodifiedmilliseconds < calendar.getTimeInMillis() && calendar1.getTimeInMillis()<currentfilelastmodifiedmilliseconds){
						
						
						lastmodifiedfiletime = currentfilelastmodifiedmilliseconds;
						lastmodifiedfilename = filename;
						res=filename;
					//	utilityFileWriteOP.writeToLog(tcid, "The target file name in "+oretailpath, " is "+lastmodifiedfilename, ResultPath, xwpfRun,"");
						
						System.out.println("The file is :"+filename);
						
						break;
					}
					else{
						
						System.out.println("There are no file generated recently");
						
						res=null;
					}
				}
						
				//br.close();
				sftpchannel.exit();

				session.disconnect();

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				System.out.println(e);
			//	utilityFileWriteOP.writeToLog(tcid, "Error occurred during File Extraction ", "Due to :"+e);
				return res;
			}


			return res;
		}
		

		public static String latestS3FileExtract_SFTP_mcdly(String OrderId,String us, String pw,String oretailpath,String host, String port, String tcid,String ResultPath,XWPFRun xwpfRun,ExtentTest logger) throws IOException {

			String res="";
			JSch jsch = new JSch();
			Session session = null;
			BufferedReader br = null;
			String Result = null;
			int flag = 0;

			long lastmodifiedfiletime = 0;
			String lastmodifiedfilename = null;

			try {

				session = jsch.getSession(us, host, Integer.parseInt(port));
				session.setConfig("StrictHostKeyChecking", "no");
				session.setPassword(pw);
				session.connect();
				System.out.println("Connection successful");
				Channel channel = session.openChannel("sftp");
				channel.connect();
				System.out.println("Channel Connection successful");
				ChannelSftp sftpchannel = (ChannelSftp) channel;


				sftpchannel.cd(oretailpath);

				System.out.println("The Current path is " + sftpchannel.pwd());

				//w-mcc-OrderID.csv

				Vector<ChannelSftp.LsEntry> list = sftpchannel.ls("w-mcd-"+OrderId+"*.csv");
				System.out.println("List"+list.size());
				String filename="";
				
				int p=0;
				
				for(ChannelSftp.LsEntry entry: list) {

					 filename = entry.getFilename();
					
					System.out.println("The current file is " + filename);
					
					//Getting the current date
					Date time=new Date();
					 //This method returns the time in millis
					long timeMilli = time.getTime();
					
					System.out.println("The current time in miliseconds:"+timeMilli);
					
					Calendar calendar = Calendar.getInstance();
					System.out.println("Current Date = " + calendar.getTime());
					System.out.println("Time in milliseconds before= "+(long)calendar.getTimeInMillis());
					
					
				  // Subtract 15 minutes from current date
					Calendar calendar1 = Calendar.getInstance();
				      calendar1.add(Calendar.MINUTE, -105);
				      System.out.println("Updated Date = " + calendar1.getTime());
				      System.out.println("Time in milliseconds after = "+(long)calendar1.getTimeInMillis());

					
		long currentfilelastmodifiedmilliseconds =(long) entry.getAttrs().getMTime()*1000L;
					
					
					System.out.println("The current file time is:"+currentfilelastmodifiedmilliseconds);

					
					
		if(currentfilelastmodifiedmilliseconds < calendar.getTimeInMillis() && calendar1.getTimeInMillis()<currentfilelastmodifiedmilliseconds){
						
						
						lastmodifiedfiletime = currentfilelastmodifiedmilliseconds;
						lastmodifiedfilename = filename;
						res=filename;
					//	utilityFileWriteOP.writeToLog(tcid, "The target file name in "+oretailpath, " is "+lastmodifiedfilename, ResultPath, xwpfRun,"");
						
						System.out.println("The file is :"+filename);
						
						break;
					}
					else{
						
						System.out.println("There are no file generated recently");
						
						res=null;
					}
				}
						
				//br.close();
				sftpchannel.exit();

				session.disconnect();

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				System.out.println(e);
			//	utilityFileWriteOP.writeToLog(tcid, "Error occurred during File Extraction ", "Due to :"+e);
				return res;
			}


			return res;
		}
		
		

		public static String latestS3FileExtract_SFTP_mpk(String OrderId,String us, String pw,String oretailpath,String host, String port, String tcid,String ResultPath,XWPFRun xwpfRun,ExtentTest logger) throws IOException {

			String res="";
			JSch jsch = new JSch();
			Session session = null;
			BufferedReader br = null;
			String Result = null;
			int flag = 0;

			long lastmodifiedfiletime = 0;
			String lastmodifiedfilename = null;

			try {

				session = jsch.getSession(us, host, Integer.parseInt(port));
				session.setConfig("StrictHostKeyChecking", "no");
				session.setPassword(pw);
				session.connect();
				System.out.println("Connection successful");
				Channel channel = session.openChannel("sftp");
				channel.connect();
				System.out.println("Channel Connection successful");
				ChannelSftp sftpchannel = (ChannelSftp) channel;


				sftpchannel.cd(oretailpath);

				System.out.println("The Current path is " + sftpchannel.pwd());

				//w-mcc-OrderID.csv

				Vector<ChannelSftp.LsEntry> list = sftpchannel.ls("w-mpk-"+OrderId+"*.csv");
				System.out.println("List"+list.size());
				String filename="";
				
				int p=0;
				
				for(ChannelSftp.LsEntry entry: list) {

					 filename = entry.getFilename();
					
					System.out.println("The current file is " + filename);
					
					//Getting the current date
					Date time=new Date();
					 //This method returns the time in millis
					long timeMilli = time.getTime();
					
					System.out.println("The current time in miliseconds:"+timeMilli);
					
					Calendar calendar = Calendar.getInstance();
					System.out.println("Current Date = " + calendar.getTime());
					System.out.println("Time in milliseconds before= "+(long)calendar.getTimeInMillis());
					
					
				  // Subtract 15 minutes from current date
					Calendar calendar1 = Calendar.getInstance();
				      calendar1.add(Calendar.MINUTE, -105);
				      System.out.println("Updated Date = " + calendar1.getTime());
				      System.out.println("Time in milliseconds after = "+(long)calendar1.getTimeInMillis());

					
		long currentfilelastmodifiedmilliseconds =(long) entry.getAttrs().getMTime()*1000L;
					
					
					System.out.println("The current file time is:"+currentfilelastmodifiedmilliseconds);

					
					
		if(currentfilelastmodifiedmilliseconds < calendar.getTimeInMillis() && calendar1.getTimeInMillis()<currentfilelastmodifiedmilliseconds){
						
						
						lastmodifiedfiletime = currentfilelastmodifiedmilliseconds;
						lastmodifiedfilename = filename;
						res=filename;
					//	utilityFileWriteOP.writeToLog(tcid, "The target file name in "+oretailpath, " is "+lastmodifiedfilename, ResultPath, xwpfRun,"");
						
						System.out.println("The file is :"+filename);
						
						break;
					}
					else{
						
						System.out.println("There are no file generated recently");
						
						res=null;
					}
				}
						
				//br.close();
				sftpchannel.exit();

				session.disconnect();

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				System.out.println(e);
			//	utilityFileWriteOP.writeToLog(tcid, "Error occurred during File Extraction ", "Due to :"+e);
				return res;
			}


			return res;
		}
		
		//*********To Fetch the Invoice ID from the Shipment file
		
		
				public static String fetchInvoiceId(String OrderId,String us, String pw,String host, String port, String tcid,String TempDownloadedOrderFilePath,String ResultPath,XWPFRun xwpfRun,ExtentTest logger) throws IOException {
					
					JSch jsch = new JSch();
					Session session = null;
					BufferedReader br = null;
					
					String InvoiceID = null;
					
					String Result = null;
					int flag = 0;
					
					try {
						
						session = jsch.getSession(us,host , Integer.parseInt(port));
						session.setConfig("StrictHostKeyChecking", "no");
						session.setPassword(pw);
						session.connect();
						
						Channel channel = session.openChannel("sftp");
						channel.connect();
						
						ChannelSftp sftpchannel = (ChannelSftp) channel;
						
					/*	sftpchannel.cd("cd..;");
						sftpchannel.cd("cd..;");*/
						
						
						sftpchannel.cd("/data/nas/sit/wholesale_amazon/wholesale_ebs/out/pending/");
						
						System.out.println("The Current path is " + sftpchannel.pwd());
						
						Vector<ChannelSftp.LsEntry> list = sftpchannel.ls("w-mcc-*.csv");
						
						for(ChannelSftp.LsEntry entry: list) {
							
							String filename = entry.getFilename();
							
							System.out.println("The current file is " + filename);
							sftpchannel.get(filename, TempDownloadedOrderFilePath+"/" + filename);//Copy file from WinSCP to local
							
							//Read from local
							br = new BufferedReader(new FileReader(TempDownloadedOrderFilePath+"/" + filename));
							
							int count = 0;
							String CurrentLine = null;
							
							while((CurrentLine = br.readLine()) != null) {
								
								//Split the Current line in comma seperated values
								String[] strarr = CurrentLine.split(",");
							
								if(strarr[1].equals(OrderId)) {
									
									InvoiceID = strarr[11];
									System.out.println("The Invoice id is " + InvoiceID);
//									utilityFileWriteOP.writeToLog(tcid, "The Invoice for OrderID "+OrderId+"is ", InvoiceID);
									
									Result = InvoiceID;
									flag = 1;
									break;
								}
							}
							
							if(flag == 1) {
								
								br.close();
								break;
							}
						}
						
						br.close();
						sftpchannel.exit();
						
						session.disconnect();
						
					} catch (Exception e) {
						// TODO: handle exception
						System.out.println(e);
//						utilityFileWriteOP.writeToLog(tcid, "Error occurred during Invoice Extraction ", "Due to :"+e);
						return Result;
					}
					
					
					return Result;
				}

				
public static String fetchInvoiceId_harvest(String OrderId,String us, String pw,String host, String port, String tcid,String TempDownloadedOrderFilePath,String ResultPath,XWPFRun xwpfRun,ExtentTest logger) throws IOException {
					
					JSch jsch = new JSch();
					Session session = null;
					BufferedReader br = null;
					
					String InvoiceID = null;
					
					String Result = null;
					int flag = 0;
					
					try {
						
						session = jsch.getSession(us,host , Integer.parseInt(port));
						session.setConfig("StrictHostKeyChecking", "no");
						session.setPassword(pw);
						session.connect();
						
						Channel channel = session.openChannel("sftp");
						channel.connect();
						
						ChannelSftp sftpchannel = (ChannelSftp) channel;
						
					/*	sftpchannel.cd("cd..;");
						sftpchannel.cd("cd..;");*/
						
						
						sftpchannel.cd("/data/nas/sit/wholesale_amazon/wholesale_ebs/out/pending/");
						
						System.out.println("The Current path is " + sftpchannel.pwd());
						
						Vector<ChannelSftp.LsEntry> list = sftpchannel.ls("w-har-*.csv");
						
						for(ChannelSftp.LsEntry entry: list) {
							
							String filename = entry.getFilename();
							
							System.out.println("The current file is " + filename);
							sftpchannel.get(filename, TempDownloadedOrderFilePath+"/" + filename);//Copy file from WinSCP to local
							
							//Read from local
							br = new BufferedReader(new FileReader(TempDownloadedOrderFilePath+"/" + filename));
							
							int count = 0;
							String CurrentLine = null;
							
							while((CurrentLine = br.readLine()) != null) {
								
								//Split the Current line in comma seperated values
								String[] strarr = CurrentLine.split(",");
							
								if(strarr[1].equals(OrderId)) {
									
									InvoiceID = strarr[11];
									System.out.println("The Invoice id is " + InvoiceID);
//									utilityFileWriteOP.writeToLog(tcid, "The Invoice for OrderID "+OrderId+"is ", InvoiceID);
									
									Result = InvoiceID;
									flag = 1;
									break;
								}
							}
							
							if(flag == 1) {
								
								br.close();
								break;
							}
						}
						
						br.close();
						sftpchannel.exit();
						
						session.disconnect();
						
					} catch (Exception e) {
						// TODO: handle exception
						System.out.println(e);
//						utilityFileWriteOP.writeToLog(tcid, "Error occurred during Invoice Extraction ", "Due to :"+e);
						return Result;
					}
					
					
					return Result;
				}



public static String fetchInvoiceId_rontecb(String OrderId,String us, String pw,String host, String port, String tcid,String TempDownloadedOrderFilePath,String ResultPath,XWPFRun xwpfRun,ExtentTest logger) throws IOException {
	
	JSch jsch = new JSch();
	Session session = null;
	BufferedReader br = null;
	
	String InvoiceID = null;
	
	String Result = null;
	int flag = 0;
	
	try {
		
		session = jsch.getSession(us,host , Integer.parseInt(port));
		session.setConfig("StrictHostKeyChecking", "no");
		session.setPassword(pw);
		session.connect();
		
		Channel channel = session.openChannel("sftp");
		channel.connect();
		
		ChannelSftp sftpchannel = (ChannelSftp) channel;
		
	/*	sftpchannel.cd("cd..;");
		sftpchannel.cd("cd..;");*/
		
		
		sftpchannel.cd("/data/nas/sit/wholesale_amazon/wholesale_ebs/out/pending/");
		
		System.out.println("The Current path is " + sftpchannel.pwd());
		
		Vector<ChannelSftp.LsEntry> list = sftpchannel.ls("w-rtb-*.csv");
		
		for(ChannelSftp.LsEntry entry: list) {
			
			String filename = entry.getFilename();
			
			System.out.println("The current file is " + filename);
			sftpchannel.get(filename, TempDownloadedOrderFilePath+"/" + filename);//Copy file from WinSCP to local
			
			//Read from local
			br = new BufferedReader(new FileReader(TempDownloadedOrderFilePath+"/" + filename));
			
			int count = 0;
			String CurrentLine = null;
			
			while((CurrentLine = br.readLine()) != null) {
				
				//Split the Current line in comma seperated values
				String[] strarr = CurrentLine.split(",");
			
				if(strarr[1].equals(OrderId)) {
					
					InvoiceID = strarr[11];
					System.out.println("The Invoice id is " + InvoiceID);
//					utilityFileWriteOP.writeToLog(tcid, "The Invoice for OrderID "+OrderId+"is ", InvoiceID);
					
					Result = InvoiceID;
					flag = 1;
					break;
				}
			}
			
			if(flag == 1) {
				
				br.close();
				break;
			}
		}
		
		br.close();
		sftpchannel.exit();
		
		session.disconnect();
		
	} catch (Exception e) {
		// TODO: handle exception
		System.out.println(e);
//		utilityFileWriteOP.writeToLog(tcid, "Error occurred during Invoice Extraction ", "Due to :"+e);
		return Result;
	}
	
	
	return Result;
}


public static String fetchInvoiceId_rontec(String OrderId,String us, String pw,String host, String port, String tcid,String TempDownloadedOrderFilePath,String ResultPath,XWPFRun xwpfRun,ExtentTest logger) throws IOException {
	
	JSch jsch = new JSch();
	Session session = null;
	BufferedReader br = null;
	
	String InvoiceID = null;
	
	String Result = null;
	int flag = 0;
	
	try {
		
		session = jsch.getSession(us,host , Integer.parseInt(port));
		session.setConfig("StrictHostKeyChecking", "no");
		session.setPassword(pw);
		session.connect();
		
		Channel channel = session.openChannel("sftp");
		channel.connect();
		
		ChannelSftp sftpchannel = (ChannelSftp) channel;
		
	/*	sftpchannel.cd("cd..;");
		sftpchannel.cd("cd..;");*/
		
		
		sftpchannel.cd("/data/nas/sit/wholesale_amazon/wholesale_ebs/out/pending/");
		
		System.out.println("The Current path is " + sftpchannel.pwd());
		
		Vector<ChannelSftp.LsEntry> list = sftpchannel.ls("w-ron-*.csv");
		
		for(ChannelSftp.LsEntry entry: list) {
			
			String filename = entry.getFilename();
			
			System.out.println("The current file is " + filename);
			sftpchannel.get(filename, TempDownloadedOrderFilePath+"/" + filename);//Copy file from WinSCP to local
			
			//Read from local
			br = new BufferedReader(new FileReader(TempDownloadedOrderFilePath+"/" + filename));
			
			int count = 0;
			String CurrentLine = null;
			
			while((CurrentLine = br.readLine()) != null) {
				
				//Split the Current line in comma seperated values
				String[] strarr = CurrentLine.split(",");
			
				if(strarr[1].equals(OrderId)) {
					
					InvoiceID = strarr[11];
					System.out.println("The Invoice id is " + InvoiceID);
//					utilityFileWriteOP.writeToLog(tcid, "The Invoice for OrderID "+OrderId+"is ", InvoiceID);
					
					Result = InvoiceID;
					flag = 1;
					break;
				}
			}
			
			if(flag == 1) {
				
				br.close();
				break;
			}
		}
		
		br.close();
		sftpchannel.exit();
		
		session.disconnect();
		
	} catch (Exception e) {
		// TODO: handle exception
		System.out.println(e);
//		utilityFileWriteOP.writeToLog(tcid, "Error occurred during Invoice Extraction ", "Due to :"+e);
		return Result;
	}
	
	
	return Result;
}


public static String fetchInvoiceId_sandpiper(String OrderId,String us, String pw,String host, String port, String tcid,String TempDownloadedOrderFilePath,String ResultPath,XWPFRun xwpfRun,ExtentTest logger) throws IOException {
	
	JSch jsch = new JSch();
	Session session = null;
	BufferedReader br = null;
	
	String InvoiceID = null;
	
	String Result = null;
	int flag = 0;
	
	try {
		
		session = jsch.getSession(us,host , Integer.parseInt(port));
		session.setConfig("StrictHostKeyChecking", "no");
		session.setPassword(pw);
		session.connect();
		
		Channel channel = session.openChannel("sftp");
		channel.connect();
		
		ChannelSftp sftpchannel = (ChannelSftp) channel;
		
	/*	sftpchannel.cd("cd..;");
		sftpchannel.cd("cd..;");*/
		
		
		sftpchannel.cd("/data/nas/sit/wholesale_amazon/wholesale_ebs/out/pending/");
		
		System.out.println("The Current path is " + sftpchannel.pwd());
		
		Vector<ChannelSftp.LsEntry> list = sftpchannel.ls("w-san-*.csv");
		
		for(ChannelSftp.LsEntry entry: list) {
			
			String filename = entry.getFilename();
			
			System.out.println("The current file is " + filename);
			sftpchannel.get(filename, TempDownloadedOrderFilePath+"/" + filename);//Copy file from WinSCP to local
			
			//Read from local
			br = new BufferedReader(new FileReader(TempDownloadedOrderFilePath+"/" + filename));
			
			int count = 0;
			String CurrentLine = null;
			
			while((CurrentLine = br.readLine()) != null) {
				
				//Split the Current line in comma seperated values
				String[] strarr = CurrentLine.split(",");
			
				if(strarr[1].equals(OrderId)) {
					
					InvoiceID = strarr[11];
					System.out.println("The Invoice id is " + InvoiceID);
//					utilityFileWriteOP.writeToLog(tcid, "The Invoice for OrderID "+OrderId+"is ", InvoiceID);
					
					Result = InvoiceID;
					flag = 1;
					break;
				}
			}
			
			if(flag == 1) {
				
				br.close();
				break;
			}
		}
		
		br.close();
		sftpchannel.exit();
		
		session.disconnect();
		
	} catch (Exception e) {
		// TODO: handle exception
		System.out.println(e);
//		utilityFileWriteOP.writeToLog(tcid, "Error occurred during Invoice Extraction ", "Due to :"+e);
		return Result;
	}
	
	
	return Result;
}


public static String fetchInvoiceId_mcdly(String OrderId,String us, String pw,String host, String port, String tcid,String TempDownloadedOrderFilePath,String ResultPath,XWPFRun xwpfRun,ExtentTest logger) throws IOException {
	
	JSch jsch = new JSch();
	Session session = null;
	BufferedReader br = null;
	
	String InvoiceID = null;
	
	String Result = null;
	int flag = 0;
	
	try {
		
		session = jsch.getSession(us,host , Integer.parseInt(port));
		session.setConfig("StrictHostKeyChecking", "no");
		session.setPassword(pw);
		session.connect();
		
		Channel channel = session.openChannel("sftp");
		channel.connect();
		
		ChannelSftp sftpchannel = (ChannelSftp) channel;
		
	/*	sftpchannel.cd("cd..;");
		sftpchannel.cd("cd..;");*/
		
		
		sftpchannel.cd("/data/nas/sit/wholesale_amazon/wholesale_ebs/out/pending/");
		
		System.out.println("The Current path is " + sftpchannel.pwd());
		
		Vector<ChannelSftp.LsEntry> list = sftpchannel.ls("w-mcd-*.csv");
		
		for(ChannelSftp.LsEntry entry: list) {
			
			String filename = entry.getFilename();
			
			System.out.println("The current file is " + filename);
			sftpchannel.get(filename, TempDownloadedOrderFilePath+"/" + filename);//Copy file from WinSCP to local
			
			//Read from local
			br = new BufferedReader(new FileReader(TempDownloadedOrderFilePath+"/" + filename));
			
			int count = 0;
			String CurrentLine = null;
			
			while((CurrentLine = br.readLine()) != null) {
				
				//Split the Current line in comma seperated values
				String[] strarr = CurrentLine.split(",");
			
				if(strarr[1].equals(OrderId)) {
					
					InvoiceID = strarr[11];
					System.out.println("The Invoice id is " + InvoiceID);
//					utilityFileWriteOP.writeToLog(tcid, "The Invoice for OrderID "+OrderId+"is ", InvoiceID);
					
					Result = InvoiceID;
					flag = 1;
					break;
				}
			}
			
			if(flag == 1) {
				
				br.close();
				break;
			}
		}
		
		br.close();
		sftpchannel.exit();
		
		session.disconnect();
		
	} catch (Exception e) {
		// TODO: handle exception
		System.out.println(e);
//		utilityFileWriteOP.writeToLog(tcid, "Error occurred during Invoice Extraction ", "Due to :"+e);
		return Result;
	}
	
	
	return Result;
}



public static String fetchInvoiceId_mpk(String OrderId,String us, String pw,String host, String port, String tcid,String TempDownloadedOrderFilePath,String ResultPath,XWPFRun xwpfRun,ExtentTest logger) throws IOException {
	
	JSch jsch = new JSch();
	Session session = null;
	BufferedReader br = null;
	
	String InvoiceID = null;
	
	String Result = null;
	int flag = 0;
	
	try {
		
		session = jsch.getSession(us,host , Integer.parseInt(port));
		session.setConfig("StrictHostKeyChecking", "no");
		session.setPassword(pw);
		session.connect();
		
		Channel channel = session.openChannel("sftp");
		channel.connect();
		
		ChannelSftp sftpchannel = (ChannelSftp) channel;
		
	/*	sftpchannel.cd("cd..;");
		sftpchannel.cd("cd..;");*/
		
		
		sftpchannel.cd("/data/nas/sit/wholesale_amazon/wholesale_ebs/out/pending/");
		
		System.out.println("The Current path is " + sftpchannel.pwd());
		
		Vector<ChannelSftp.LsEntry> list = sftpchannel.ls("w-mpk-*.csv");
		
		for(ChannelSftp.LsEntry entry: list) {
			
			String filename = entry.getFilename();
			
			System.out.println("The current file is " + filename);
			sftpchannel.get(filename, TempDownloadedOrderFilePath+"/" + filename);//Copy file from WinSCP to local
			
			//Read from local
			br = new BufferedReader(new FileReader(TempDownloadedOrderFilePath+"/" + filename));
			
			int count = 0;
			String CurrentLine = null;
			
			while((CurrentLine = br.readLine()) != null) {
				
				//Split the Current line in comma seperated values
				String[] strarr = CurrentLine.split(",");
			
				if(strarr[1].equals(OrderId)) {
					
					InvoiceID = strarr[11];
					System.out.println("The Invoice id is " + InvoiceID);
//					utilityFileWriteOP.writeToLog(tcid, "The Invoice for OrderID "+OrderId+"is ", InvoiceID);
					
					Result = InvoiceID;
					flag = 1;
					break;
				}
			}
			
			if(flag == 1) {
				
				br.close();
				break;
			}
		}
		
		br.close();
		sftpchannel.exit();
		
		session.disconnect();
		
	} catch (Exception e) {
		// TODO: handle exception
		System.out.println(e);
//		utilityFileWriteOP.writeToLog(tcid, "Error occurred during Invoice Extraction ", "Due to :"+e);
		return Result;
	}
	
	
	return Result;
}



				public static String fetchVATamt(String OrderId,String us, String pw,String host, String port, String tcid,String TempDownloadedOrderFilePath,String ResultPath,XWPFRun xwpfRun,ExtentTest logger) throws IOException {
					
					JSch jsch = new JSch();
					Session session = null;
					BufferedReader br = null;
					
					String VATamt = null;
					
					String Result = null;
					int flag = 0;
					
					try {
						
						session = jsch.getSession(us,host , Integer.parseInt(port));
						session.setConfig("StrictHostKeyChecking", "no");
						session.setPassword(pw);
						session.connect();
						
						Channel channel = session.openChannel("sftp");
						channel.connect();
						
						ChannelSftp sftpchannel = (ChannelSftp) channel;
						
					/*	sftpchannel.cd("cd..;");
						sftpchannel.cd("cd..;");*/
						
						
						sftpchannel.cd("/data/nas/sit/wholesale_amazon/wholesale_ebs/out/pending/");
						
						System.out.println("The Current path is " + sftpchannel.pwd());
						
						Vector<ChannelSftp.LsEntry> list = sftpchannel.ls("w-mcc-*.csv");
						
						for(ChannelSftp.LsEntry entry: list) {
							
							String filename = entry.getFilename();
							
							System.out.println("The current file is " + filename);
							sftpchannel.get(filename, TempDownloadedOrderFilePath+"/" + filename);//Copy file from WinSCP to local
							
							//Read from local
							br = new BufferedReader(new FileReader(TempDownloadedOrderFilePath+"/" + filename));
							
							int count = 0;
							String CurrentLine = null;
							
							while((CurrentLine = br.readLine()) != null) {
								
								//Split the Current line in comma seperated values
								String[] strarr = CurrentLine.split(",");
							
								if(strarr[1].equals(OrderId)) {
									
									VATamt = strarr[13];
									System.out.println("The VAT amount is " + VATamt);
//									utilityFileWriteOP.writeToLog(tcid, "The Invoice for OrderID "+OrderId+"is ", InvoiceID);
									
									Result = VATamt;
									flag = 1;
									break;
								}
							}
							
							if(flag == 1) {
								
								br.close();
								break;
							}
						}
						
						br.close();
						sftpchannel.exit();
						
						session.disconnect();
						
					} catch (Exception e) {
						// TODO: handle exception
						System.out.println(e);
//						utilityFileWriteOP.writeToLog(tcid, "Error occurred during Invoice Extraction ", "Due to :"+e);
						return Result;
					}
					
					
					return Result;
				}


				
public static ArrayList<HashMap<String, String>> fetchInvoiceDetails(String OrderId,String us, String pw,String host, String port, String tcid,String TempDownloadedOrderFilePath,String ResultPath,XWPFRun xwpfRun,ExtentTest logger) throws IOException {
					
					JSch jsch = new JSch();
					Session session = null;
					BufferedReader br = null;
					
					String InvoiceID = null;
					
					String Result = null;
					
					HashMap<String, String> invoiceFileDetails = null;
					ArrayList<HashMap<String, String>> listofInvoiceDetails = new ArrayList<HashMap<String,String>>();
					int flag = 0;
					
					try {
						
						session = jsch.getSession(us,host , Integer.parseInt(port));
						session.setConfig("StrictHostKeyChecking", "no");
						session.setPassword(pw);
						session.connect();
						
						Channel channel = session.openChannel("sftp");
						channel.connect();
						
						ChannelSftp sftpchannel = (ChannelSftp) channel;
						
					/*	sftpchannel.cd("cd..;");
						sftpchannel.cd("cd..;");*/
						
						
						sftpchannel.cd("/data/nas/sit/wholesale_amazon/wholesale_ebs/out/pending/");
						
						System.out.println("The Current path is " + sftpchannel.pwd());
						
						Vector<ChannelSftp.LsEntry> list = sftpchannel.ls("w-mcc-*.csv");
						
						for(ChannelSftp.LsEntry entry: list) {
							
							String filename = entry.getFilename();
							
							System.out.println("The current file is " + filename);
							sftpchannel.get(filename, TempDownloadedOrderFilePath+"/" + filename);//Copy file from WinSCP to local
							
							//Read from local
							br = new BufferedReader(new FileReader(TempDownloadedOrderFilePath+"/" + filename));
							
							int count = 0;
							String CurrentLine = null;
							
							while((CurrentLine = br.readLine()) != null) {
								
								//Split the Current line in comma seperated values
								String[] strarr = CurrentLine.split(",");
							
								if(strarr[1].equals(OrderId)) {
									
									invoiceFileDetails = new HashMap<String, String>();
									
									invoiceFileDetails.put("customername", strarr[0]);
									invoiceFileDetails.put("orderid", strarr[1]);
									invoiceFileDetails.put("itemid", strarr[2]);
									invoiceFileDetails.put("itemlineid", strarr[3]);
									invoiceFileDetails.put("description", strarr[4]);
									invoiceFileDetails.put("ordRefCode", strarr[5]);
									invoiceFileDetails.put("QtyShipped", strarr[6]);
									invoiceFileDetails.put("currency", strarr[7]);
									invoiceFileDetails.put("unitPrice", strarr[8]);
									invoiceFileDetails.put("VAT", strarr[9]);
									invoiceFileDetails.put("fileRefCode", strarr[10]);
									invoiceFileDetails.put("InvoiceId", strarr[11]);
									invoiceFileDetails.put("taxableAmt", strarr[12]);
									invoiceFileDetails.put("tax", strarr[11]);
									invoiceFileDetails.put("totalAmt", strarr[12]);
									invoiceFileDetails.put("category", strarr[12]);
									
									System.out.println("The Invoice id is " + InvoiceID);
//									utilityFileWriteOP.writeToLog(tcid, "The Invoice for OrderID "+OrderId+"is ", InvoiceID);
									
									//Result = InvoiceID;
									//flag = 1;
									//break;
									
									listofInvoiceDetails.add(invoiceFileDetails);
									
								}
							}
							
							if(flag == 1) {
								
								br.close();
								break;
							}
						}
						
						br.close();
						sftpchannel.exit();
						
						session.disconnect();
						
					} catch (Exception e) {
						// TODO: handle exception
						System.out.println(e);
//						utilityFileWriteOP.writeToLog(tcid, "Error occurred during Invoice Extraction ", "Due to :"+e);
						return listofInvoiceDetails;
					}
					
					
					return listofInvoiceDetails;
				}





public static boolean ValidateConsolidationFile_At_Nas(String us, String pw,String filename,String OretailPath, String host, String port, String tcid,String ResultPath,XWPFRun xwpfRun, ExtentTest logger)
{
	boolean res=false;
	String extension = null;
	
	String matchpattern=filename;
	
	
	JSch jsch = new JSch();
	Session session = null;
	Channel channel = null;
	try {


		session = jsch.getSession(us, host, Integer.parseInt(port));
		session.setConfig("StrictHostKeyChecking", "no");
		session.setPassword(pw);
		session.connect();

		channel = session.openChannel("sftp");
		channel.connect();

		System.out.println("Connection Successful");

		ChannelSftp sftpchannel = (ChannelSftp) channel;

		sftpchannel.cd(OretailPath);
		System.out.println("The Current path is -> " + OretailPath);

		Vector<ChannelSftp.LsEntry> list = null;
		
		try {
			list = sftpchannel.ls(matchpattern);
			System.out.println("File "+filename+" found in "+OretailPath );
			res=true;
		
		} 

		catch (Exception e) {
			// TODO: handle exception
			System.out.println("File "+filename+" not found in "+OretailPath );
			utilityFileWriteOP.writeToLog(tcid, "File "+filename+" is not found in  ", OretailPath+ " Download Failed", ResultPath,xwpfRun,"");

			res = false;
			
			return res;
			
		}

		
	} catch (Exception e) {

		System.out.println("File Transfer Failed "+e);
		
		utilityFileWriteOP.writeToLog(tcid, "Exception occurred while downloading  file "+filename," from  "+OretailPath, ResultPath,xwpfRun,"");

		e.printStackTrace();
		res = false;
		
		
	}
	finally
	{
		channel.disconnect();
		session.disconnect();
		
		return res;

	}
}


}


