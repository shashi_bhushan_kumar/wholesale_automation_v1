package Utilities;

import java.io.*;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class excelCellValueWrite {
	

	public static String readValueFromCell(String keyword, String columnname,String resultFilePath, String sheetName ) throws IOException {
		
		
		//System.out.println("keyword"+keyword);
		//System.out.println("columnname"+columnname);
		FileInputStream input_document=null;
		String ress=null;
		String finalres=null;
		//System.out.println("Result path is "+resultFilePath);
		try { //Access the workbook
			
			input_document = new FileInputStream(new File(resultFilePath));
		HSSFWorkbook my_xls_workbook = new HSSFWorkbook(input_document); 
		 //Access the worksheet, so that we can update / modify it.
		 HSSFSheet my_worksheet = my_xls_workbook.getSheet(sheetName); 	
		Row toprow=my_worksheet.getRow(0);
		 Cell cell = null; 
		 
		 
		 int rows=my_worksheet.getPhysicalNumberOfRows();
		 int cols=toprow.getPhysicalNumberOfCells();
		 // Access the cell first to update the value
		 //System.out.println("Hi"+my_worksheet.getPhysicalNumberOfRows());
		 
		outerloop: for(int key=0;key<rows;key++)
		 {
			//System.out.println("loop"+key);
			 Cell cellkey = my_worksheet.getRow(key).getCell(2);
			 String kw=cellkey.getStringCellValue();
			 //System.out.println("kw "+kw);
			 if(kw.equalsIgnoreCase(keyword))
			 {
			 for(int col=0;col<cols;col++)
			 {
				 Cell cellcol=my_worksheet.getRow(0).getCell(col);
				 String colnm=cellcol.getStringCellValue();
				 //System.out.println("colnm "+colnm);
				 if(colnm.equalsIgnoreCase(columnname))
				 {
					 //System.out.println("Column "+col);
					 //System.out.println("Row"+key);
					 Cell newcell=my_worksheet.getRow(key).getCell(col);
					 //System.out.println("1");
					 DataFormatter df=new DataFormatter();
					 finalres=df.formatCellValue(newcell);
					 //finalres=String.valueOf(newcell.getNumericCellValue());
					 //System.out.println("finalres"+finalres);
					 break outerloop;
				 }
			 }
			 
			 }
		 }
		 
		 //Get the value of the cell
		 ress=finalres;
		 
	} catch (Exception e) {
		// TODO Auto-generated catch block
		//System.out.println("Catch of ExcelCellRead");
		//System.out.println("Exception "+e);
		//e.printStackTrace();
		
	}
     //close the stream
    finally
    {
    	input_document.close(); 
    	return ress;
    	
    }


	}

	
	
public static void writeValueToCell_sheet (String res, int sheet_no, int rowNo, int columnNo,String resultFilePath ) throws IOException
{
		
		FileInputStream input_document = new FileInputStream(new File(resultFilePath));
       
		HSSFWorkbook my_xls_workbook = new HSSFWorkbook(input_document); 
      
         HSSFSheet my_worksheet = my_xls_workbook.getSheetAt(sheet_no); 	
         
         Cell cell = null; 
       
         
         cell = my_worksheet.getRow(rowNo).getCell(columnNo);
       
         if (cell == null)
         
        	 cell = my_worksheet.getRow(rowNo).createCell((short)columnNo);
        
         
         cell.setCellType(HSSFCell.CELL_TYPE_STRING);
       
         cell.setCellValue(res);
        
         input_document.close();
         
         FileOutputStream output_file =new FileOutputStream(new File(resultFilePath));
         
         my_xls_workbook.write(output_file);
         
         output_file.close(); 
         my_xls_workbook.close();    
}
	

	public static void writeValueToCell(String res,int i,int columnNo,String resultFilePath ) throws IOException {
		
		//public static String resultFilePath="C:\\MorderingAutomation\\TestCase.xlsx"; 
		
		FileInputStream input_document = new FileInputStream(new File(resultFilePath));
         //Access the workbook
		HSSFWorkbook my_xls_workbook = new HSSFWorkbook(input_document); 
         //Access the worksheet, so that we can update / modify it.
         HSSFSheet my_worksheet = my_xls_workbook.getSheetAt(0); 	
		
         Cell cell = null; 
         // Access the cell first to update the value
         cell = my_worksheet.getRow(i).getCell(columnNo);
         // Get current value and then add 5 to it 
         //cell.setCellValue(cell.getNumericCellValue() + 5);
         
         if (cell == null)
         cell = my_worksheet.getRow(i).createCell((short)columnNo);
         cell.setCellType(HSSFCell.CELL_TYPE_STRING);
         //cell.setCellValue("a test");
         cell.setCellValue(res);
         
         input_document.close();
         
         FileOutputStream output_file =new FileOutputStream(new File(resultFilePath));
         //write changes
         my_xls_workbook.write(output_file);
         //close the stream
         output_file.close(); 
         my_xls_workbook.close();     
	}
	public static void writeValueToCell(String res,int i,int columnNo,String resultFilePath, String sheetName ) throws IOException {
		
		//public static String resultFilePath="C:\\MorderingAutomation\\TestCase.xlsx"; 
		
		FileInputStream input_document = new FileInputStream(new File(resultFilePath));
         //Access the workbook
		HSSFWorkbook my_xls_workbook = new HSSFWorkbook(input_document); 
         //Access the worksheet, so that we can update / modify it.
         HSSFSheet my_worksheet = my_xls_workbook.getSheet(sheetName); 	
		
         Cell cell = null; 
         // Access the cell first to update the value
         cell = my_worksheet.getRow(i).getCell(columnNo);
         // Get current value and then add 5 to it 
         //cell.setCellValue(cell.getNumericCellValue() + 5);
         
         if (cell == null)
         cell = my_worksheet.getRow(i).createCell((short)columnNo);
         cell.setCellType(HSSFCell.CELL_TYPE_STRING);
         //cell.setCellValue("a test");
         cell.setCellValue(res);
         
         input_document.close();
         
         FileOutputStream output_file =new FileOutputStream(new File(resultFilePath));
         //write changes
         my_xls_workbook.write(output_file);
         //close the stream
         output_file.close(); 
         my_xls_workbook.close();      
	}
	
public static void writeValueToCellMacro(String res,int i,int columnNo,String resultFilePath, String sheetName) throws IOException {
		
		//public static String resultFilePath="C:\\MorderingAutomation\\TestCase.xlsx"; 
		
		FileInputStream input_document = new FileInputStream(new File(resultFilePath));
         //Access the workbook
		XSSFWorkbook my_xls_workbook = new XSSFWorkbook(input_document); 
         //Access the worksheet, so that we can update / modify it.
         XSSFSheet my_worksheet = my_xls_workbook.getSheet(sheetName); 	
		
         Cell cell = null; 
         // Access the cell first to update the value
         cell = my_worksheet.getRow(i).getCell(columnNo);
         // Get current value and then add 5 to it 
         //cell.setCellValue(cell.getNumericCellValue() + 5);
         
         if (cell == null)
         cell = my_worksheet.getRow(i).createCell((short)columnNo);
         cell.setCellType(XSSFCell.CELL_TYPE_STRING);
         //cell.setCellValue("a test");
         cell.setCellValue(res);
         
         input_document.close();
         
         FileOutputStream output_file =new FileOutputStream(new File(resultFilePath));
         //write changes
         my_xls_workbook.write(output_file);
         //close the stream
         output_file.close(); 
         my_xls_workbook.close();     
	}
public static void appendValueToCell(String res,int i,int columnNo,String resultFilePath, String sheetName ) throws IOException {
	
	//public static String resultFilePath="C:\\MorderingAutomation\\TestCase.xlsx"; 
	
	FileInputStream input_document = new FileInputStream(new File(resultFilePath));
     //Access the workbook
	HSSFWorkbook my_xls_workbook = new HSSFWorkbook(input_document); 
     //Access the worksheet, so that we can update / modify it.
     HSSFSheet my_worksheet = my_xls_workbook.getSheet(sheetName); 	
	
     Cell cell = null; 
     // Access the cell first to update the value
     cell = my_worksheet.getRow(i).getCell(columnNo);
     // Get current value and then add 5 to it 
     //cell.setCellValue(cell.getNumericCellValue() + 5);
     
//     System.out.println("cell value"+cell);
     
     
     
     
     if (cell == null)
     cell = my_worksheet.getRow(i).createCell((short)columnNo);
     cell.setCellType(HSSFCell.CELL_TYPE_STRING);
     //cell.setCellValue("a test");
     if(cell.getStringCellValue().trim().length()<1){
    	 cell.setCellValue(res);
     }else
    	 cell.setCellValue(cell.getStringCellValue()+","+res);
     
     input_document.close();
     
     FileOutputStream output_file =new FileOutputStream(new File(resultFilePath));
     //write changes
     my_xls_workbook.write(output_file);
     //close the stream
     output_file.close(); 
             
}
	
}
