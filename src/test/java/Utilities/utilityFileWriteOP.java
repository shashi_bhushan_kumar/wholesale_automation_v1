package Utilities;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFRun;

public class utilityFileWriteOP {
	
	
	
	
	public static String ReadResultPathServer1()
	

	  {      
		String line = null;
		
		try{
		String Server1_TempFilePath="Server1/TempFile.txt";
		//String Server1_TempFilePath="S:/RemoteRun_Selenium/Server1/TempFile.txt";
		
		File fin = new File(Server1_TempFilePath);
		
		FileInputStream fis = new FileInputStream(fin);
		 
		//Construct BufferedReader from InputStreamReader
		BufferedReader br = new BufferedReader(new InputStreamReader(fis));
	 
		line=br.readLine().trim();

		br.close();

		}
			
		catch(Exception e){

		}
		
		
		finally{
		
		return line;
						
		}
	
	
	  }
	
	
	
	public static String ReadResultPathServer2()
	

	  {      
		String line = null;
		
		try{
		String Server1_TempFilePath="S:/RemoteRun_Selenium/Server2/TempFile.txt";
		
		File fin = new File(Server1_TempFilePath);
		
		FileInputStream fis = new FileInputStream(fin);
		 
		//Construct BufferedReader from InputStreamReader
		BufferedReader br = new BufferedReader(new InputStreamReader(fis));
	 
		
		
		line=br.readLine().trim();
		
		
	 
		br.close();
	
		
		}
		
		
		catch(Exception e){
	
			
		}
		
		
		finally{
		
		return line;
						
		}
	
	
	  }
	
	//need to change...

	public static void writeToLog(String tcid,String Comment,String result)
	

	  {      
		
		
//		String ResPath=utilityFileWriteOP.ReadResultPathServer1();
		
		String FilePath=System.getProperty("user.dir")+"/"+System.getProperty("resultpath")+"TestLog.txt";
		
		System.out.println("Log path is "+FilePath);
	
		//	String FilePath="S:/AutomationResultsSelenium/TestData/TestLog.txt";
		
		
		 try { 
				File file = new File(FilePath);
	 
				// if file doesn't exists, then create it
				if (!file.exists()) 
				{
					file.createNewFile();
				}
	 
				FileWriter fw = new FileWriter(file.getAbsoluteFile(),true);
				BufferedWriter bw = new BufferedWriter(fw);
				
				String content=tcid+" :::::  "+Comment+":::"+result;
				
				bw.write(content);			
				bw.newLine();
				bw.close();
				
			
				
		} 
		 
		 
		catch (IOException e) 
		 {
			 e.printStackTrace();
		 }
						
		}
	

	
	public static void writeToLog(String tcid,String Comment,String result,String ResultPath)
	

	  {      
		
		
		
		String FilePath=System.getProperty("user.dir")+ResultPath+"TestLog.txt";
		FilePath=FilePath.replace(System.getProperty("user.dir")+System.getProperty("user.dir"), System.getProperty("user.dir"));
		//System.out.println("File Path is "+FilePath);
		
		
		 try { 
				File file = new File(FilePath);
	 
				// if file doesn't exists, then create it
				if (!file.exists()) 
				{
					System.out.println("FilePath:"+FilePath);
					file.createNewFile();
				}
	 
				FileWriter fw = new FileWriter(file.getAbsoluteFile(),true);
				BufferedWriter bw = new BufferedWriter(fw);
				
				String content=tcid+" :::::  "+Comment+":::"+result;
				
				bw.write(content);			
				bw.newLine();
				bw.close();
				
		} 
		 
		 
		catch (IOException e) 
		 {
			 e.printStackTrace();
		 }
						
		}
	
	
	
	
public static void copyFileUsingApacheCommonsIO(File source, File dest) throws IOException {
	  
		
		FileUtils.copyFile(source, dest);
	
	
	}
	

	
	public static void writeToLog(String str,String ResultPath)
	

	  {      
		
		
		
		String FilePath=System.getProperty("user.dir")+"/"+ResultPath+"TestLog.txt";
		
		
		 try { 
				File file = new File(FilePath);
	 
				// if file doesn't exists, then create it
				if (!file.exists()) 
				{
					file.createNewFile();
				}
	 
				FileWriter fw = new FileWriter(file.getAbsoluteFile(),true);
				BufferedWriter bw = new BufferedWriter(fw);
				
				String content=str;
				
				bw.write(content);			
				bw.newLine();
				bw.close();
				
		} 
		 
		 
		catch (IOException e) 
		 {
			 e.printStackTrace();
		 }
						
		}
	
	
	
	
	public static void writeToLog(String str)
	

	  {      
		
		String FilePath="Results/TestLog.txt";
		
		
		 try { 
				File file = new File(FilePath);
	 
				// if file doesn't exists, then create it
				if (!file.exists()) 
				{
					file.createNewFile();
				}
	 
				FileWriter fw = new FileWriter(file.getAbsoluteFile(),true);
				BufferedWriter bw = new BufferedWriter(fw);
				
				String content=str;
				
				bw.write(content);			
				bw.newLine();
				bw.newLine();
				bw.close();
				
		} 
		 
		 
		catch (IOException e) 
		 {
			 e.printStackTrace();
		 }
						
		}
	
	

	
	
	public static void CreateTempResultPath(String ResFilePath)
	

	  {      
		
		
		
		String FilePath="S:/RemoteRun_Selenium/Server1/TempFile.txt";
		
		
		 try { 
				File file = new File(FilePath);
	 
				// if file doesn't exists, then create it
				if (!file.exists()) 
				{
					file.createNewFile();
				}
	 
				FileWriter fw = new FileWriter(file.getAbsoluteFile(),false);
				BufferedWriter bw = new BufferedWriter(fw);
				
				String content=ResFilePath;
				
				bw.write(content);			
				
				bw.close();
				
		} 
		 
		 
		catch (IOException e) 
		 {
			 e.printStackTrace();
		 }
						
		}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
public static void writeResultsToLog(String tcid,String tcname,String result)
	

{
	 try {
		    String log_file_path="S:/AutomationResultsSelenium/TestData/TestResultsLog.csv";
			File file = new File(log_file_path);

		// if file doesn't exists, then create it
			if (!file.exists()) 
			{
				file.createNewFile();
			}

			FileWriter fw = new FileWriter(file.getAbsoluteFile(),true);
			BufferedWriter bw = new BufferedWriter(fw);
			
			String content=tcid+","+tcname+","+result;
			
			bw.write(content);			
			bw.newLine();
			bw.close();
			
	} 
	 
	 
	catch (IOException e) 
	 {
		 e.printStackTrace();
	 }
					
	}
	
	
	public static void TestExecutionLog(String content)
	{
	 try {
		 
		 
		 File file = new File("C:/MorderingAutomation/Results/Results.txt");

			// if file doesn't exists, then create it
			if (!file.exists()) 
			{
				file.createNewFile();
			}

			FileWriter fw = new FileWriter(file.getAbsoluteFile(),true);
			BufferedWriter bw = new BufferedWriter(fw);
			
			bw.write(content);			
			bw.newLine();
			bw.close();
			
			} 
	 
	 catch (IOException e) 
	 {
		 e.printStackTrace();
	 }
					
	
	}	
	
	
	public static void writeToLog(String tcid,String Comment,String result,String ResultPath,XWPFRun xwpfRun)


	{      
		
		String FilePath=ResultPath+"TestLog.txt";
		FilePath=FilePath.replace("//", "/");
		System.out.println("File Path is "+FilePath);
		 try { 
				File file = new File(FilePath);

				// if file doesn't exists, then create it
				if (!file.exists()) 
				{
					file.createNewFile();
				}

				FileWriter fw = new FileWriter(file.getAbsoluteFile(),true);
				BufferedWriter bw = new BufferedWriter(fw);
				
				String content=tcid+" :::::  "+Comment+":::"+result;
				String screenshotcontent=Comment+" : "+result;
				
				int format=XWPFDocument.PICTURE_TYPE_PNG;
				String latestFileName=Reporting_Utilities.lastFileNameModified(ResultPath+"Screenshot/"+tcid);
				String imgFile=ResultPath+"Screenshot/"+tcid+"/"+latestFileName;
//				System.out.println("imgFile "+imgFile);
				
	          try {
	          	xwpfRun.setText(screenshotcontent);
	          	xwpfRun.addBreak();
					xwpfRun.addPicture (new FileInputStream(imgFile), format, imgFile, Units.toEMU(475), Units.toEMU(280));
				} catch (InvalidFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				bw.write(content);			
				bw.newLine();
				bw.close();
				
		} 
		 
		 
		catch (IOException e) 
		 {
			 e.printStackTrace();
		 }
						
		}
	public static void writeToLog(String tcid,String Comment,String result,String ResultPath,XWPFRun xwpfRun, String report)


	{      
		
		
		
		String FilePath=System.getProperty("user.dir")+"/"+ResultPath+"TestLog.txt";
		
		
		 try { 
				File file = new File(FilePath);

				// if file doesn't exists, then create it
				if (!file.exists()) 
				{
					System.out.println("FilePath:"+FilePath);
					file.createNewFile();
				}

				FileWriter fw = new FileWriter(file.getAbsoluteFile(),true);
				BufferedWriter bw = new BufferedWriter(fw);
				
				String content=tcid+" :::::  "+Comment+":::"+result;
				String screenshotcontent=Comment+" : "+result;
				
				
	          try {
	          	xwpfRun.setText(screenshotcontent);
	          	xwpfRun.addBreak();
	          	xwpfRun.setText(report);
	          	xwpfRun.addBreak();	
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				bw.write(content);			
				bw.newLine();
				bw.close();
				
		} 
		 
		 
		catch (IOException e) 
		 {
			 e.printStackTrace();
		 }
						
		}
public static void deleteFile(String filePath){
		  
	File file = new File(filePath);  
		  
	file.delete();	  
		   
	  }
public static String ReadBatchResult(String Path)


{      
	String line = null;
	
	try{
		
		//String Server1_TempFilePath=server_name+"/TempFile.txt";
	//String Server1_TempFilePath="Server1/TempFile.txt";
	String Server1_TempFilePath=Path;
	
	File fin = new File(Server1_TempFilePath);
	
	FileInputStream fis = new FileInputStream(fin);
	 
	//Construct BufferedReader from InputStreamReader
	BufferedReader br = new BufferedReader(new InputStreamReader(fis));

	line=br.readLine().trim();

	br.close();

	}
		
	catch(Exception e){

	}
	
	
	finally{
	
	return line;
					
	}


}
public static void writeToLogUFT(String str,String ResultPath)


{      
	
	
	
	String FilePath=ResultPath+"UFTTestLog.txt";
	
	
	 try { 
			File file = new File(FilePath);

			// if file doesn't exists, then create it
			if (!file.exists()) 
			{
				file.createNewFile();
			}

			FileWriter fw = new FileWriter(file.getAbsoluteFile(),true);
			BufferedWriter bw = new BufferedWriter(fw);
			
			String content=str;
			
			bw.write(content);			
			bw.newLine();
			bw.close();
			
	} 
	 
	 
	catch (IOException e) 
	 {
		 e.printStackTrace();
	 }
					
	}


public static void appendValueToCell(String res,int i,int columnNo,String resultFilePath, String sheetName ) throws IOException {
	
	//public static String resultFilePath="C:\\MorderingAutomation\\TestCase.xlsx"; 
	
	FileInputStream input_document = new FileInputStream(new File(resultFilePath));
     //Access the workbook
	HSSFWorkbook my_xls_workbook = new HSSFWorkbook(input_document); 
     //Access the worksheet, so that we can update / modify it.
     HSSFSheet my_worksheet = my_xls_workbook.getSheet(sheetName); 	
	
     Cell cell = null; 
     // Access the cell first to update the value
     cell = my_worksheet.getRow(i).getCell(columnNo);
     // Get current value and then add 5 to it 
     //cell.setCellValue(cell.getNumericCellValue() + 5);
     
     System.out.println("cell value"+cell);
     
     
     
     
     if (cell == null)
     cell = my_worksheet.getRow(i).createCell((short)columnNo);
     cell.setCellType(HSSFCell.CELL_TYPE_STRING);
     //cell.setCellValue("a test");
     cell.setCellValue(cell.getStringCellValue()+res);
     
     input_document.close();
     
     FileOutputStream output_file =new FileOutputStream(new File(resultFilePath));
     //write changes
     my_xls_workbook.write(output_file);
     //close the stream
     output_file.close(); 
             
}

public static String ReadUFTResult()
{      
	String line = null;		
	try{					
		FileInputStream fis = new FileInputStream("S:/MTEBS/Automation/Selenium_Project_McColls/New_Wholesale_EBS_Project/TempRes.txt");		 
		//Construct BufferedReader from InputStreamReader
		BufferedReader br = new BufferedReader(new InputStreamReader(fis));		
		line=br.readLine().trim();	 
		br.close();		
	}
			
	catch(Exception e){			
	}
			
	finally{		
	return line;						
	}	
}

public static String ReadUFTResultPath()
{      
	String line = null;		
	try{					
		FileInputStream fis = new FileInputStream("S:/MTEBS/Automation/Selenium_Project_McColls/New_Wholesale_EBS_Project/ResultPathRes.txt");		 
		//Construct BufferedReader from InputStreamReader
		BufferedReader br = new BufferedReader(new InputStreamReader(fis));		
		line=br.readLine().trim();	 
		br.close();		
	}				
	catch(Exception e){			
	}				
	finally{		
	return line;						
	}	
}

}
